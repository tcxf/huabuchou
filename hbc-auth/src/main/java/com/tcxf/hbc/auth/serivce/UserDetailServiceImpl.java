package com.tcxf.hbc.auth.serivce;

import com.tcxf.hbc.auth.feign.UserService;
import com.tcxf.hbc.auth.util.UserDetailsImpl;
import com.tcxf.hbc.common.vo.SysRole;
import com.tcxf.hbc.common.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhouyj
 * @date 2018/3/10
 * <p>
 */
@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

    //@Autowired
   // private UserService userService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        UserVo userVo = new UserVo();
        userVo.setUsername(username);
        userVo.setUserId(1);
        String pwd =   ENCODER.encode("123456");
        userVo.setPassword(pwd);
        userVo.setDelFlag("0");
        List<SysRole> roleList = new ArrayList<>();
        SysRole role = new SysRole();
        role.setRoleId(1);
        role.setRoleCode("10000");
        role.setRoleName("roleName");
        role.setDelFlag("0");
        role.setCreateTime(new Date());
        roleList.add(role);
        userVo.setRoleList(roleList);
                //userService.findUserByUsername(username);
        return new UserDetailsImpl(userVo);
    }

//    public static void main(String[] args) {
//        String pwd =   ENCODER.encode("123456");
//        System.out.println("pwd is :"+pwd);
//        boolean res =  ENCODER.matches("123456",pwd);
//        System.out.println("res is :"+res);
//    }
}
