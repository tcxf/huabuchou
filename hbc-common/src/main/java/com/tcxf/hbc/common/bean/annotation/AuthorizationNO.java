package com.tcxf.hbc.common.bean.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * 在Controller的方法上使用此注解，如果加上这个注解 则表明该方法不需要登录状态 其他无该注解都需要登录状态
 */ 
@Target(ElementType.METHOD) 
@Retention(RetentionPolicy.RUNTIME) 
public @interface AuthorizationNO {

}
