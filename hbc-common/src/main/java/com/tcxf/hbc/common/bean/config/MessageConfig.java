package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 是否支持短信验证码 888888
 *
 */
@Configuration
@ConditionalOnProperty(prefix = "message", name = "messageType")
@ConfigurationProperties(prefix = "message")
public class MessageConfig {
    /**
     * 是否支持验证码88888  0 为不支持 1 为支持
     * @return
     */
    private String messageType;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}
