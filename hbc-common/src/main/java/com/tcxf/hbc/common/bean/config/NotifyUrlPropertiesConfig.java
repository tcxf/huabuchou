package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 支付通知回调路径配置
 */
@Configuration
@ConditionalOnProperty(prefix = "notifyUrl", name = {"rj_notify_url","notify_before_pay_url","notify_normal_pay_url"})
@ConfigurationProperties(prefix = "notifyUrl")
public class NotifyUrlPropertiesConfig {

    /**
     * 支付通知回调路径URL
     */
    private String rj_notify_url;
    /**
     * 提前还款回调路径
     */
    private String notify_before_pay_url;

    /**
     * 正常（已出账）还款回调路径
     */
    private String notify_normal_pay_url;

    public String getRj_notify_url() {
        return rj_notify_url;
    }

    public void setRj_notify_url(String rj_notify_url) {
        this.rj_notify_url = rj_notify_url;
    }

    public String getNotify_before_pay_url() {
        return notify_before_pay_url;
    }

    public void setNotify_before_pay_url(String notify_before_pay_url) {
        this.notify_before_pay_url = notify_before_pay_url;
    }

    public String getNotify_normal_pay_url() {
        return notify_normal_pay_url;
    }

    public void setNotify_normal_pay_url(String notify_normal_pay_url) {
        this.notify_normal_pay_url = notify_normal_pay_url;
    }
}
