package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 运营商二级域名配置
 */
@Configuration
@ConditionalOnProperty(prefix = "osn", name = "osn_url")
@ConfigurationProperties(prefix = "osn")
public class OsnPropertiesConfig {

   private String osn_url;

    public String getOsn_url() {
        return osn_url;
    }

    public void setOsn_url(String osn_url) {
        this.osn_url = osn_url;
    }
}