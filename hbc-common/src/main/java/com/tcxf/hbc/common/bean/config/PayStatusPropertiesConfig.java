package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 支付金额设置
 */
@Configuration
@ConditionalOnProperty(prefix = "paystatus", name = "status")
@ConfigurationProperties(prefix = "paystatus")
public class PayStatusPropertiesConfig {

    /**
     * 支付金额设置 0 为1分钱 1 为正常支付
     */
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}