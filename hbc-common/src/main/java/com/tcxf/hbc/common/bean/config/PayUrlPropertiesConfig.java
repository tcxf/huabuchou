package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 支付服务路径
 */
@Configuration
@ConditionalOnProperty(prefix = "payurl", name = {"gotopayurl","gotoRepayment"})
@ConfigurationProperties(prefix = "payurl")
public class PayUrlPropertiesConfig {

    /**
     * 支付界面URL
     */
    private String gotopayurl;

    /**
     * consumer还款界面URL
     */
    private String gotoRepayment;

    public String getGotopayurl() {
        return gotopayurl;
    }

    public void setGotopayurl(String gotopayurl) {
        this.gotopayurl = gotopayurl;
    }
    public String getGotoRepayment() {
        return gotoRepayment;
    }

    public void setGotoRepayment(String gotoRepayment) {
        this.gotoRepayment = gotoRepayment;
    }
}
