package com.tcxf.hbc.common.bean.config;

import com.xiaoleilu.hutool.system.UserInfo;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author zhouyj
 * @date 2018/3/15
 * 增强redis
 */
@Configuration
@EnableCaching
public class RedisCacheConfig extends CachingConfigurerSupport {
    @Value("${redis.cache.expiration:3600}")
    private Long expiration;

    @Bean
    public CacheManager cacheManager(RedisTemplate redisTemplate) {
        RedisCacheManager rcm = new RedisCacheManager(redisTemplate);
        rcm.setDefaultExpiration(expiration);
        return rcm;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        return template;
    }

    /**
     * SetListByCache
     * @param redisTemplate
     * @param list
     * @param keyName
     */
    public void saveCacheList(RedisTemplate redisTemplate, List<T> list,String keyName,Long timeSeconds)
    {
        redisTemplate.opsForList().leftPushAll(keyName,Collections.singleton(list));
        redisTemplate.opsForList().set(keyName,timeSeconds,list);
    }

    /**
     * getListByCache
     * @param redisTemplate
     * @param t
     * @param keyName
     * @return
     */
    public List<T> getCacheList(RedisTemplate redisTemplate, T t,String keyName)
    {
        return (List<T>) redisTemplate.opsForList().leftPop(keyName);
    }
}
