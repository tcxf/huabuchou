package com.tcxf.hbc.common.bean.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 是否支持微信支付useWxPay:
 *   wxPay: 0
 */
@Configuration
@ConditionalOnProperty(prefix = "useWxPay", name = "wxPay")
@ConfigurationProperties(prefix = "useWxPay")
public class WxPayPropertiesConfig {

    /**
     * 是否支持微信支付 0 为不支持 1 为支持
     */
    private String wxPay;

    public String getWxPay() {
        return wxPay;
    }

    public void setWxPay(String wxPay) {
        this.wxPay = wxPay;
    }

}
