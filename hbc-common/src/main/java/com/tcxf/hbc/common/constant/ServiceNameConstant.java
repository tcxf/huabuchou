package com.tcxf.hbc.common.constant;

/**
 * @author zhouyj
 * @date 2018/3/15
 * 服务名称
 */
public interface ServiceNameConstant {
    /**
     * 认证服务的SERVICEID（zuul 配置的对应）
     */
    String AUTH_SERVICE = "hbc-auth";

    /**
     * UMPS模块
     */
    String UMPS_SERVICE = "hbc-upms-service";
}
