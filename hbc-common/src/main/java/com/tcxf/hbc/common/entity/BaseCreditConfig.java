package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
@TableName("base_credit_config")
public class BaseCreditConfig extends Model<BaseCreditConfig> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String value;
    private String parentId;
    private String type;
    private String content;
    private List<BaseCreditConfig> subList;

    public List<BaseCreditConfig> getSubList() {
        return subList;
    }

    public void setSubList(List<BaseCreditConfig> subList) {
        this.subList = subList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BaseCreditConfig{" +
        ", id=" + id +
        ", name=" + name +
        ", value=" + value +
        ", parentId=" + parentId +
        ", type=" + type +
        ", content=" + content +
        "}";
    }
}
