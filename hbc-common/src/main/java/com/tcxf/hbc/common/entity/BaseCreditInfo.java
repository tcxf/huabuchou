package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 授信四要素表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-16
 */
@TableName("base_credit_info")
public class BaseCreditInfo extends Model<BaseCreditInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * @Description: 已授信
     * @Param:
     * @return:
     * @Author: JinPeng
     * @Date: 2018/9/13
    */
    public final static String status_2="2";

    /**
     * 主键id
     */
    private String id;
    /**
     * 授信手机号码
     */
    private String mobile;
    /**
     * 授信用户名
     */
    private String username;
    /**
     * 授信身份证号
     */
    private String idcard;
    /**
     * 授信状态
     */
    private String status;
    /**
     * 授信用户id
     */
    private String uid;
    /**
     * 授信银行卡号
     */
    private String accountno;
    @TableField("create_date")
    private Date createDate;

    /**
     * 评分
     */
    @TableField("grade")
    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BaseCreditInfo{" +
                ", id=" + id +
                ", mobile=" + mobile +
                ", username=" + username +
                ", idcard=" + idcard +
                ", accountno=" + accountno +
                ", createDate=" + createDate +
                "}";
    }
}
