package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@TableName("base_interface_info")
public class BaseInterfaceInfo extends Model<BaseInterfaceInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String value;
    private String type;
    private String company;
    private String content;
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BaseInterfaceInfo{" +
        ", id=" + id +
        ", name=" + name +
        ", value=" + value +
        ", type=" + type +
        ", company=" + company +
        ", content=" + content +
        ", price=" + price +
        "}";
    }
}
