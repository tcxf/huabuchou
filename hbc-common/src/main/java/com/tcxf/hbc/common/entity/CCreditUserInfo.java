package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
@TableName("c_credit_user_info")
public class CCreditUserInfo extends Model<CCreditUserInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 类型
     */
    private String type;
    /**
     * 类型名称
     */
    @TableField("typeName")
    private String typeName;
    /**
     * json数据
     */
    @TableField("jsonStr")
    private String jsonStr;
    /**
     * 用户id
     */
    private String uid;
    @TableField("createTime")
    private Date createTime;
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 运营商类型 0:移动,1:联通,2:电信
     */
    @TableField("operator_type")
    private String operatorType;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CCreditUserInfo{" +
        ", id=" + id +
        ", type=" + type +
        ", typeName=" + typeName +
        ", jsonStr=" + jsonStr +
        ", uid=" + uid +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", operatorType=" + operatorType +
        "}";
    }
}
