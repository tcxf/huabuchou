package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 运营商开关表
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
@TableName("o_opera_switch")
public class COperaSwitch extends Model<COperaSwitch> {

    private static final long serialVersionUID = 1L;

    public static final String phoneState_NO = "0";

    public static final String phoneState_YES = "1";

    public static final String ipState_NO = "0";
    public static final String ipState_YES = "1";
    public static final String idcard_state_NO = "0";
    public static final String idcard_state_YES = "1";
    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 手机归属地状态是否与运营商一致（0.关，1.开）
     */
    @TableField("phone_state")
    private String phoneState;
    /**
     * ip归属地状态是否与运营商一致（0.关，1.开）
     */
    @TableField("ip_state")
    private String ipState;
    /**
     * 身份证归属地状态是否与运营商一致（0.关，1.开）
     */
    @TableField("idcard_state")
    private String idcardState;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPhoneState() {
        return phoneState;
    }

    public void setPhoneState(String phoneState) {
        this.phoneState = phoneState;
    }

    public String getIpState() {
        return ipState;
    }

    public void setIpState(String ipState) {
        this.ipState = ipState;
    }

    public String getIdcardState() {
        return idcardState;
    }

    public void setIdcardState(String idcardState) {
        this.idcardState = idcardState;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "COperaSwitch{" +
        ", id=" + id +
        ", oid=" + oid +
        ", phoneState=" + phoneState +
        ", ipState=" + ipState +
        ", idcardState=" + idcardState +
        "}";
    }
}
