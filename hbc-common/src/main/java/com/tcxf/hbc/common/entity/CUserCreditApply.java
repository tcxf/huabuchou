package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 提额申请审核记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("c_user_credit_apply")
public class CUserCreditApply extends Model<CUserCreditApply> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户姓名
     */
    private String uname;
    /**
     * 授信金额
     */
    @TableField("auth_amount")
    private BigDecimal authAmount;
    /**
     * 审核状态
     */
    @TableField("auth_status")
    private String authStatus;
    /**
     * 审批人
     */
    @TableField("opera_name")
    private String operaName;
    /**
     * 审核时间
     */
    @TableField("auth_time")
    private Date authTime;
    /**
     * 反馈意见
     */
    @TableField("examine_fail_reason")
    private String examineFailReason;
    /**
     * 申请时间
     */
    @TableField("application_date")
    private Date applicationDate;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public BigDecimal getAuthAmount() {
        return authAmount;
    }

    public void setAuthAmount(BigDecimal authAmount) {
        this.authAmount = authAmount;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getOperaName() {
        return operaName;
    }

    public void setOperaName(String operaName) {
        this.operaName = operaName;
    }

    public Date getAuthTime() {
        return authTime;
    }

    public void setAuthTime(Date authTime) {
        this.authTime = authTime;
    }

    public String getExamineFailReason() {
        return examineFailReason;
    }

    public void setExamineFailReason(String examineFailReason) {
        this.examineFailReason = examineFailReason;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CUserCreditApply{" +
        ", id=" + id +
        ", uid=" + uid +
        ", uname=" + uname +
        ", authAmount=" + authAmount +
        ", authStatus=" + authStatus +
        ", operaName=" + operaName +
        ", authTime=" + authTime +
        ", examineFailReason=" + examineFailReason +
        ", applicationDate=" + applicationDate +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
