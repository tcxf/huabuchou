package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消费者用户表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("c_user_info")
public class CUserInfo extends Model<CUserInfo> {

    private static final long serialVersionUID = 1L;


    /**
     * 运营商类型
     */
    public static final  String USERATTRIBUTE_YYS="3";
 
    /**
     * 用户启用
     */
    public static  final String STATUS_Y="0";

    /**
     * 默认为普通用户
     */
    public static final String USERATTRIBUTE_USER = "1";
    /**
     * 默认为商户用户
     */
    public static final String USERATTRIBUTE_MERCHANT = "2";
	
    /**
     * 主键id
     */
    private String id;
    /**
     * 用户姓名
     */
    @TableField("real_name")
    private String realName;
    /**
     * 用户手机号码
     */
    private String mobile;
    /**
     * 状态（0-启用 1-禁用）
     */
    private String status;
    /**
     * 用户属性（1-正常用户 2-商户用户）
     */
    @TableField("user_attribute")
    private String userAttribute;
    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;
    /**
     * 最后登录ip
     */
    @TableField("login_ip")
    private String loginIp;
    /**
     * 用户头像资源访问路径
     */
    @TableField("head_img")
    private String headImg;
    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 微信公众号openid
     */
    @TableField("wx_openid")
    private String wxOpenid;
    /**
     * 微信唯一id
     */
    @TableField("wx_unionid")
    private String wxUnionid;
    /**
     * 性别 1-男 2-女
     */
    private String sex;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 邀请码
     */
    @TableField("invate_code")
    private String invateCode;

    public String getInvateCode() {
        return invateCode;
    }

    public void setInvateCode(String invateCode) {
        this.invateCode = invateCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getWxOpenid() {
        return wxOpenid;
    }

    public void setWxOpenid(String wxOpenid) {
        this.wxOpenid = wxOpenid;
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getUserAttribute() {return userAttribute; }

    public void setUserAttribute(String userAttribute) { this.userAttribute = userAttribute; }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CUserInfo{" +
                "id='" + id + '\'' +
                ", realName='" + realName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", status='" + status + '\'' +
                ", userAttribute='" + userAttribute + '\'' +
                ", lastLoginTime=" + lastLoginTime +
                ", loginIp='" + loginIp + '\'' +
                ", headImg='" + headImg + '\'' +
                ", idCard='" + idCard + '\'' +
                ", wxOpenid='" + wxOpenid + '\'' +
                ", wxUnionid='" + wxUnionid + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", invateCode='" + invateCode + '\'' +
                '}';
    }
}
