package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消费者用户其他信息表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("c_user_other_info")
public class CUserOtherInfo extends Model<CUserOtherInfo> {

    private static final long serialVersionUID = 1L;
    /**
     * 默认为未绑卡
     */
    public static final String BINDCARDSTATUS_N = "0";
    /**
     * 默认为已绑卡
     */
    public static final String BINDCARDSTATUS_Y = "1";
    /**
     * 默认为个人
     */
    public static final String TYPE_N = "1";
    /**
     * 默认为企业
     */
    public static final String TYPE_Y = "2";
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 用户主键id
     */
    private String uid;
    /**
     * 绑卡状态（0-未绑卡 1-已绑卡）
     */
    @TableField("bind_card_status")
    private String bindCardStatus;
    /**
     * 账户类型 （1-个人 2-企业）
     */
    private String type;
    /**
     * 是否结婚
     */
    @TableField("is_marry")
    private String isMarry;
    /**
     * 地区
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 紧急联系人
     */
    @TableField("ugren_name")
    private String ugrenName;
    /**
     * 紧急联系人电话
     */
    @TableField("ugren_mobile")
    private String ugrenMobile;
    /**
     * 公司名称
     */
    @TableField("company_name")
    private String companyName;
    /**
     * 工作时长
     */
    @TableField("work_time")
    private Integer workTime;
    /**
     * 工资
     */
    private Integer wages;
    /**
     * 公司电话
     */
    @TableField("company_tel")
    private String companyTel;
    /**
     * 公司职位
     */
    @TableField("company_position")
    private String companyPosition;
    /**
     * 公司性质
     */
    @TableField("company_type")
    private String companyType;
    /**
     * 公司地址
     */
    @TableField("company_address")
    private String companyAddress;
    /**
     * 公司所在位置（如：湖南省长沙市岳麓区）
     */
    @TableField("company_area")
    private String companyArea;
    /**
     *  积分数
     */
    private Integer score;
    /**
     * 0
     */
    @TableField("version_code")
    private Integer versionCode;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBindCardStatus() {
        return bindCardStatus;
    }

    public void setBindCardStatus(String bindCardStatus) {
        this.bindCardStatus = bindCardStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(String isMarry) {
        this.isMarry = isMarry;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUgrenName() {
        return ugrenName;
    }

    public void setUgrenName(String ugrenName) {
        this.ugrenName = ugrenName;
    }

    public String getUgrenMobile() {
        return ugrenMobile;
    }

    public void setUgrenMobile(String ugrenMobile) {
        this.ugrenMobile = ugrenMobile;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    public Integer getWages() {
        return wages;
    }

    public void setWages(Integer wages) {
        this.wages = wages;
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    public String getCompanyPosition() {
        return companyPosition;
    }

    public void setCompanyPosition(String companyPosition) {
        this.companyPosition = companyPosition;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyArea() {
        return companyArea;
    }

    public void setCompanyArea(String companyArea) {
        this.companyArea = companyArea;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CUserOtherInfo{" +
        ", id=" + id +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", uid=" + uid +
        ", bindCardStatus=" + bindCardStatus +
        ", type=" + type +
        ", isMarry=" + isMarry +
        ", area=" + area +
        ", address=" + address +
        ", ugrenName=" + ugrenName +
        ", ugrenMobile=" + ugrenMobile +
        ", companyName=" + companyName +
        ", workTime=" + workTime +
        ", wages=" + wages +
        ", companyTel=" + companyTel +
        ", companyPosition=" + companyPosition +
        ", companyType=" + companyType +
        ", companyAddress=" + companyAddress +
        ", companyArea=" + companyArea +
        ", score=" + score +
        ", versionCode=" + versionCode +
        "}";
    }
}
