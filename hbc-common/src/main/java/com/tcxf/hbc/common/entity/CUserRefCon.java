package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端)
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("c_user_ref_con")
public class CUserRefCon extends Model<CUserRefCon> {

    private static final long serialVersionUID = 1L;
    /**
     * 为未授信
     */
    public static final String AUTHSTATUS_N = "0";
    /**
     * 为已授信
     */
    public static final String AUTHSTATUS_Y = "1";

    /**
     * 拒绝授信
     */
    public static final String AUTHSTATUS_JJ = "3";

    /**
     * 授信初始状态 0-初始状态 1-快速授信成功 2- 快速授信失败 3-提额申请中
     */
    public static final String AUTHEXAMINE_N = "0";

    public static final String AUTHEXAMINE_0 = "0";

    public static final String AUTHEXAMINE_1 = "1";

    public static final String AUTHEXAMINE_2 = "2";

    public static final String AUTHEXAMINE_3 = "3";


    //是否同意二次申请 0-不同意 1-同意
    public static final String TWOAPPLICATIONS_N = "0";

    public static final String TWOAPPLICATIONS_Y = "1";

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 绑定商户id
     */
    private String mid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 授信状态（0-未授信 1-已授信）
     */
    @TableField("auth_status")
    private String authStatus;
    /**
     * 授信额度
     */
    @TableField("auth_max")
    private BigDecimal authMax;

    /**
     * 提额额度
     */
    @TableField("up_Money")
    private BigDecimal upMoney;


    /**
     * 授信日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("auth_date")
    private Date authDate;
    /**
     * 出账日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("ooa_date")
    private String ooaDate;
    /**
     * 还款日期
     */
    @TableField("repay_date")
    private String repayDate;
    /**
     * 0-初始状态 1-快速授信成功 2- 快速授信失败 3-提额申请中
     */
    @TableField("auth_examine")
    private String authExamine;
    /**
     * 最近一次授信审核失败原因（包括快速授信和提额）
     */
    @TableField("examine_fail_reason")
    private String examineFailReason;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 快速授信时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("credit_date")
    private Date creditDate;

    /**
     * 授信提额时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("upmoney_date")
    private Date upmoneyDate;

    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("nextauth_date")
    private Date nextauthDate;

    /**
     * 用户id
     */
    private String accid;
    /**
     * 授信计数
     */
    private String creditCount;

    /**
     * 剩余授信额度
     */
    @TableField("residue_Money")
    private BigDecimal residueMoney;

    /**
     * 拒绝授信code
     */
    @TableField("refusing_code")
    private String refusingCode;

    /**
     * 是否同意二次申请
     */
    @TableField("twoapplications")
    private String twoapplications;

    @TableField("version_id")
    private String versionId;

    public BigDecimal getResidueMoney() {
        return residueMoney;
    }

    public void setResidueMoney(BigDecimal residueMoney) {
        this.residueMoney = residueMoney;
    }

    public String getRefusingCode() {
        return refusingCode;
    }

    public void setRefusingCode(String refusingCode) {
        this.refusingCode = refusingCode;
    }

    public String getTwoapplications() {
        return twoapplications;
    }

    public void setTwoapplications(String twoapplications) {
        this.twoapplications = twoapplications;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BigDecimal getUpMoney() {
        return upMoney;
    }

    public void setUpMoney(BigDecimal upMoney) {
        this.upMoney = upMoney;
    }

    public Date getCreditDate() {
        return creditDate;
    }

    public void setCreditDate(Date creditDate) {
        this.creditDate = creditDate;
    }

    public Date getUpmoneyDate() {
        return upmoneyDate;
    }

    public void setUpmoneyDate(Date upmoneyDate) {
        this.upmoneyDate = upmoneyDate;
    }

    public String getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(String creditCount) {
        this.creditCount = creditCount;
    }

    public String getAccid() {
        return accid;
    }

    public void setAccid(String accid) {
        this.accid = accid;
    }

    public Date getNextauthDate() {
        return nextauthDate;
    }

    public void setNextauthDate(Date nextauthDate) {
        this.nextauthDate = nextauthDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public BigDecimal getAuthMax() {
        return authMax;
    }

    public void setAuthMax(BigDecimal authMax) {
        this.authMax = authMax;
    }

    public Date getAuthDate() {
        return authDate;
    }

    public void setAuthDate(Date authDate) {
        this.authDate = authDate;
    }

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }

    public String getExamineFailReason() {
        return examineFailReason;
    }

    public void setExamineFailReason(String examineFailReason) {
        this.examineFailReason = examineFailReason;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    @Override
    public String toString() {
        return "CUserRefCon{" +
        ", id=" + id +
        ", uid=" + uid +
        ", oid=" + oid +
        ", mid=" + mid +
        ", fid=" + fid +
        ", authStatus=" + authStatus +
        ", authMax=" + authMax +
        ", authDate=" + authDate +
        ", ooaDate=" + ooaDate +
        ", repayDate=" + repayDate +
        ", authExamine=" + authExamine +
        ", examineFailReason=" + examineFailReason +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
