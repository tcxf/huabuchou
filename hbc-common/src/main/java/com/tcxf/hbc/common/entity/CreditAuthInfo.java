package com.tcxf.hbc.common.entity;

/**
 * 银行卡信息
 */
public class CreditAuthInfo {
    /**
     * 名称
     */
    private String name;
    /**
     * 身份证号码
     */
    private String identityCard;
    /**
     * SAME 匹配
     * DIFFERENT 不匹配
     * ACCOUNTNO_UNABLE_VERIFY 不能证实
     */
    private String checkStatus;

    /**
     * 返回结果
     */
    private String result;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
