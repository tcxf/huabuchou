package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 授信日报表
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
@TableName("credit_daily")
public class CreditDaily extends Model<CreditDaily> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 授信人数
     */
    @TableField("sx_number_people")
    private String sxNumberPeople;
    /**
     * 申请提额人数
     */
    @TableField("upmoney_number_people")
    private String upmoneyNumberPeople;
    /**
     * 人均提额金额
     */
    @TableField("percapita_up_money")
    private BigDecimal percapitaUpMoney;
    /**
     * 人均授信金额
     */
    @TableField("percapita_sx_money")
    private BigDecimal percapitaSxMoney;
    /**
     * 授信额度
     */
    @TableField("credit_line")
    private BigDecimal creditLine;
    /**
     * 通过率
     */
    @TableField("passing_rate")
    private String passingRate;
    /**
     * 创建时间
     */
    @JsonFormat(pattern ="yyyy-MM-dd" ,timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getSxNumberPeople() {
        return sxNumberPeople;
    }

    public void setSxNumberPeople(String sxNumberPeople) {
        this.sxNumberPeople = sxNumberPeople;
    }

    public String getUpmoneyNumberPeople() {
        return upmoneyNumberPeople;
    }

    public void setUpmoneyNumberPeople(String upmoneyNumberPeople) {
        this.upmoneyNumberPeople = upmoneyNumberPeople;
    }

    public BigDecimal getPercapitaUpMoney() {
        return percapitaUpMoney;
    }

    public void setPercapitaUpMoney(BigDecimal percapitaUpMoney) {
        this.percapitaUpMoney = percapitaUpMoney;
    }

    public BigDecimal getPercapitaSxMoney() {
        return percapitaSxMoney;
    }

    public void setPercapitaSxMoney(BigDecimal percapitaSxMoney) {
        this.percapitaSxMoney = percapitaSxMoney;
    }

    public BigDecimal getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(BigDecimal creditLine) {
        this.creditLine = creditLine;
    }

    public String getPassingRate() {
        return passingRate;
    }

    public void setPassingRate(String passingRate) {
        this.passingRate = passingRate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CreditDaily{" +
        ", id=" + id +
        ", oid=" + oid +
        ", fid=" + fid +
        ", sxNumberPeople=" + sxNumberPeople +
        ", upmoneyNumberPeople=" + upmoneyNumberPeople +
        ", percapitaUpMoney=" + percapitaUpMoney +
        ", percapitaSxMoney=" + percapitaSxMoney +
        ", creditLine=" + creditLine +
        ", passingRate=" + passingRate +
        ", createTime=" + createTime +
        "}";
    }
}
