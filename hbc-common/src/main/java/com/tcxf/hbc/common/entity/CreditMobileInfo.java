package com.tcxf.hbc.common.entity;

public class CreditMobileInfo {

    private String  name;    //姓名
    private String  identityCard;    //身份证号
    private String  mobile;    //手机号
    private String compareStatus;    //状态码 （SAME、DIFFERENT、NO_DATA）
    private String compareStatusDesc;    //状态码描述 （一致、不一致、无数据）

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCompareStatus() {
        return compareStatus;
    }

    public void setCompareStatus(String compareStatus) {
        this.compareStatus = compareStatus;
    }

    public String getCompareStatusDesc() {
        return compareStatusDesc;
    }

    public void setCompareStatusDesc(String compareStatusDesc) {
        this.compareStatusDesc = compareStatusDesc;
    }
}
