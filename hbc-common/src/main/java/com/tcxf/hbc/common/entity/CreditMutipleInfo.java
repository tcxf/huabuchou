package com.tcxf.hbc.common.entity;

import java.util.List;
import java.util.Map;

public class CreditMutipleInfo {

  private String  phone;    //电话
    private List<Map<String,Object>> creditPlatformRegistrationDetails;//信贷平台注册详情
    private List<Map<String,Object>>  loanApplicationDetails;//贷款申请详情
    private List<Map<String,Object>>  loanDetails;//贷款放款详情
    private List<Map<String,Object>>  loanRejectDetails;//贷款驳回详情
    private List<Map<String,Object>>  overduePlatformDetails;//逾期平台详情
    private List<Map<String,Object>>  arrearsInquiry;//欠款查询详情
    private String status;// 返回状态;（SUCCESS，FAIL）
    private String statusDesc;//  返回状态描述（查询正常，查询异常）

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Map<String, Object>> getCreditPlatformRegistrationDetails() {
        return creditPlatformRegistrationDetails;
    }

    public void setCreditPlatformRegistrationDetails(List<Map<String, Object>> creditPlatformRegistrationDetails) {
        this.creditPlatformRegistrationDetails = creditPlatformRegistrationDetails;
    }

    public List<Map<String, Object>> getLoanApplicationDetails() {
        return loanApplicationDetails;
    }

    public void setLoanApplicationDetails(List<Map<String, Object>> loanApplicationDetails) {
        this.loanApplicationDetails = loanApplicationDetails;
    }

    public List<Map<String, Object>> getLoanDetails() {
        return loanDetails;
    }

    public void setLoanDetails(List<Map<String, Object>> loanDetails) {
        this.loanDetails = loanDetails;
    }

    public List<Map<String, Object>> getLoanRejectDetails() {
        return loanRejectDetails;
    }

    public void setLoanRejectDetails(List<Map<String, Object>> loanRejectDetails) {
        this.loanRejectDetails = loanRejectDetails;
    }

    public List<Map<String, Object>> getOverduePlatformDetails() {
        return overduePlatformDetails;
    }

    public void setOverduePlatformDetails(List<Map<String, Object>> overduePlatformDetails) {
        this.overduePlatformDetails = overduePlatformDetails;
    }

    public List<Map<String, Object>> getArrearsInquiry() {
        return arrearsInquiry;
    }

    public void setArrearsInquiry(List<Map<String, Object>> arrearsInquiry) {
        this.arrearsInquiry = arrearsInquiry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
}
