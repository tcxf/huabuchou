package com.tcxf.hbc.common.entity;

public class CreditPoliceInfo {
    /**
     * 身份证
     */
    private String idCard;
    /**
     * 姓名
     */
    private String name;
    /**
     * 是否在逃
     */
    private String escape;
    /**
     * 是否有前科
     */
    private String crime;
    /**
     * 是否吸毒
     */
    private String drug;
    /**
     * drugRelated
     */
    private String drugRelated;
    /**
     * 在逃比对结果
     */
    private String escapeCompared;
    /**
     * 前科比对结果
     */
    private String crimeCompared;
    /**
     * 吸毒比对结果
     */
    private String drugCompared;
    /**
     * 涉毒比对结果
     */
    private String drugRelatedCompared;
    /**
     * 前科事件数量
     */
    private String checkCount;
    /**
     * 案发时间（[a,b）的时间区间，代表案发距今
     * a-b 年（不含 b）
     */
    private String caseTime;
    /**
     * 事件类别
     */
    private String caseType;
    /**
     * 查询数据状态（EXIST、NO_DATA）
     */
    private String status;

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEscape() {
        return escape;
    }

    public void setEscape(String escape) {
        this.escape = escape;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getDrugRelated() {
        return drugRelated;
    }

    public void setDrugRelated(String drugRelated) {
        this.drugRelated = drugRelated;
    }

    public String getEscapeCompared() {
        return escapeCompared;
    }

    public void setEscapeCompared(String escapeCompared) {
        this.escapeCompared = escapeCompared;
    }

    public String getCrimeCompared() {
        return crimeCompared;
    }

    public void setCrimeCompared(String crimeCompared) {
        this.crimeCompared = crimeCompared;
    }

    public String getDrugCompared() {
        return drugCompared;
    }

    public void setDrugCompared(String drugCompared) {
        this.drugCompared = drugCompared;
    }

    public String getDrugRelatedCompared() {
        return drugRelatedCompared;
    }

    public void setDrugRelatedCompared(String drugRelatedCompared) {
        this.drugRelatedCompared = drugRelatedCompared;
    }

    public String getCheckCount() {
        return checkCount;
    }

    public void setCheckCount(String checkCount) {
        this.checkCount = checkCount;
    }

    public String getCaseTime() {
        return caseTime;
    }

    public void setCaseTime(String caseTime) {
        this.caseTime = caseTime;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
