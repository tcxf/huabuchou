package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 授信提额综合分值
 * @Auther: liuxu
 * @Date: 2018/9/18 09:47
 * @Description:
 */
@TableName("rm_credit_score")
public class CreditScore extends Model<CreditScore> {

    private String id;
    /**
     * 最小分值
     */
    private BigDecimal minScore;

    /**
     * 最大分值
     */
    private BigDecimal maxScore;

    /**
     * 额度
     */
    private BigDecimal amount;

    /**
     * 版本号ID
     */
    private String  versionId;

    /**
     * 序号
     */
    private  Integer seq;

    /**
     * 大版本Id
     */
    @TableField(exist = false)
    private String relationshipId;

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    public BigDecimal getMinScore() {
        return minScore;
    }

    public void setMinScore(BigDecimal minScore) {
        this.minScore = minScore;
    }

    public BigDecimal getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(BigDecimal maxScore) {
        this.maxScore = maxScore;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getVersionId() {
        return versionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }
}

