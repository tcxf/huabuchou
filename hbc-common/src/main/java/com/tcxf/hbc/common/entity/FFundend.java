package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 资金端信息表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("f_fundend")
public class FFundend extends Model<FFundend> {

    private static final long serialVersionUID = 1L;

    /**
     * 资金端id
     */
    private String id;
    /**
     * 资金端名称
     */
    private String fname;
    /**
     * 资金端法人姓名
     */
    private String name;
    /**
     * 资金端手机号
     */
    private String mobile;
    /**
     * 资金端所在地址
     */
    private String address;
    /**
     * 资金端账号
     */
    private String account;
    /**
     * 资金端登录密码
     */
    private String pwd;
    /**
     * 资金端法人身份证
     */
    private String cardid;
    /**
     * 资金端银行卡
     */
    private String bankcard;
    /**
     * 状态 1：可用，0：不可用
     */
    private String status;
    /**
     * 记录创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 地址详情信息
     */
    @TableField("address_info")
    private String addressInfo;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public String getBankcard() {
        return bankcard;
    }

    public void setBankcard(String bankcard) {
        this.bankcard = bankcard;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }

    @Override
    public String toString() {
        return "FFundend{" +
        ", id=" + id +
        ", fname=" + fname +
        ", name=" + name +
        ", mobile=" + mobile +
        ", address=" + address +
        ", account=" + account +
        ", pwd=" + pwd +
        ", cardid=" + cardid +
        ", bankcard=" + bankcard +
        ", status=" + status +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
