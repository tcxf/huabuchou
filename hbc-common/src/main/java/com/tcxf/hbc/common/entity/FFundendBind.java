package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 资金端、运营商绑定关系表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("f_fundend_bind")
public class FFundendBind extends Model<FFundendBind> {

    private static final long serialVersionUID = 1L;

    /**
     * 资金端绑定表id
     */
    private String id;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 启用状态 （1：启用，0：不启用 ）
     */
    private String status;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FFundendBind{" +
        ", id=" + id +
        ", fid=" + fid +
        ", oid=" + oid +
        ", status=" + status +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
