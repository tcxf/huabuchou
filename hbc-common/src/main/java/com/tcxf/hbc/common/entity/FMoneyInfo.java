package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 资金端授信上限表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@TableName("f_money_info")
public class FMoneyInfo extends Model<FMoneyInfo> {

    private static final long serialVersionUID = 1L;

    public static final String STATUS_NO = "0";

    public static final String STATUS_YES = "1";

    /**
     * 主键id
     */
    private String id;
    /**
     * 资金方id
     */
    private String fid;
    /**
     * 总金额
     */
    private BigDecimal money;
    /**
     * 每天授信金额
     */
    @TableField("day_money")
    private BigDecimal dayMoney;
    /**
     * 每天授信金额
     */
    @TableField("residue_money")
    private BigDecimal residueMoney;
    /**
     * 状态（0.未开启，1.开启）
     */
    private String status;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    public BigDecimal getResidueMoney() {
        return residueMoney;
    }

    public void setResidueMoney(BigDecimal residueMoney) {
        this.residueMoney = residueMoney;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getDayMoney() {
        return dayMoney;
    }

    public void setDayMoney(BigDecimal dayMoney) {
        this.dayMoney = dayMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FMoneyInfo{" +
        ", id=" + id +
        ", fid=" + fid +
        ", money=" + money +
        ", dayMoney=" + dayMoney +
        ", status=" + status +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
