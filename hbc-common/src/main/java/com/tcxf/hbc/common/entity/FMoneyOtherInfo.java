package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 授信总金额出入明细表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@TableName("f_money_other_info")
public class FMoneyOtherInfo extends Model<FMoneyOtherInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 资金方授信上限表id
     */
    private String fmid;
    /**
     * 资金方id
     */
    private String fid;
    /**
     * 总金额
     */
    private BigDecimal money;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 类型（0.出金，1.入金）
     */
    private String type;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFmid() {
        return fmid;
    }

    public void setFmid(String fmid) {
        this.fmid = fmid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "FMoneyOtherInfo{" +
        ", id=" + id +
        ", fmid=" + fmid +
        ", fid=" + fid +
        ", money=" + money +
        ", uid=" + uid +
        ", type=" + type +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
