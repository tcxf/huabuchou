package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 首页轮播图表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("index_banner")
public class IndexBanner extends Model<IndexBanner> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 排序号
     */
    private Integer sort;
    /**
     * banner图
     */
    private String img;
    /**
     * 链接地址
     */
    private String href;
    /**
     * 图片类型 1-商户banner 2-商品banner  3-酒店banner 其他为定
     */
    private String type;
    /**
     * 链接名字
     */
    @TableField("link_name")
    private String linkName;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "IndexBanner{" +
        ", id=" + id +
        ", oid=" + oid +
        ", sort=" + sort +
        ", img=" + img +
        ", href=" + href +
        ", type=" + type +
        ", linkName=" + linkName +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
