package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 热搜店铺表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_hot_search")
public class MHotSearch extends Model<MHotSearch> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 热门搜索名称
     */
    private String name;
    /**
     * 类型 0-商家 1-商品
     */
    private String type;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 排序编号 ，越小越排前，order by orderlist asc
     */
    @TableField("order_list")
    private Integer orderList;
    /**
     * 资源id，可为商品id也可为商家id，根据type类型而定
     */
    private String rid;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getOrderList() {
        return orderList;
    }

    public void setOrderList(Integer orderList) {
        this.orderList = orderList;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MHotSearch{" +
        ", id=" + id +
        ", name=" + name +
        ", type=" + type +
        ", oid=" + oid +
        ", orderList=" + orderList +
        ", rid=" + rid +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
