package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 商户行业分类表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_merchant_category")
public class MMerchantCategory extends Model<MMerchantCategory> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 行业名称
     */
    private String name;
    /**
     * 分类父id，顶层为0
     */
    @TableField("parent_id")
    private Long parentId;
    /**
     * 层级
     */
    private Integer grade;
    /**
     * 排序id
     */
    @TableField("order_list")
    private Integer orderList;
    /**
     * 运营商id （保留字段，暂时不用）
     */
    private String oid;
    /**
     * 前端界面用户饼图对应颜色
     */
    @TableField("char_color")
    private String charColor;
    /**
     * 图标
     */
    private String icon;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getOrderList() {
        return orderList;
    }

    public void setOrderList(Integer orderList) {
        this.orderList = orderList;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getCharColor() {
        return charColor;
    }

    public void setCharColor(String charColor) {
        this.charColor = charColor;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MMerchantCategory{" +
        ", id=" + id +
        ", name=" + name +
        ", parentId=" + parentId +
        ", grade=" + grade +
        ", orderList=" + orderList +
        ", oid=" + oid +
        ", charColor=" + charColor +
        ", icon=" + icon +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
