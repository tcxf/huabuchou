package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商户信息表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_merchant_info")
public class MMerchantInfo extends Model<MMerchantInfo> {

    private static final long serialVersionUID = 1L;
    /**
     * 商户为启用状态
     */
    public static final String status_Y = "1";
    /**
     * 商户为未审核状态
     */
    public static final String status_C = "2";
    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商主键id
     */
    private String oid;
    /**
     * 商户名称
     */
    private String name;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 商户简称
     */
    @TableField("simple_name")
    private String simpleName;
    /**
     * 法人名称
     */
    @TableField("legal_name")
    private String legalName;
    /**
     * 法人身份证
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 地区
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 商户登录密码
     */
    private String pwd;
    /**
     * 商户状态0-禁用 1-启用 2-待审核
     */
    private String status;
    /**
     * 审核时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("examine_date")
    private Date examineDate;
    /**
     * 行业类型
     */
    @TableField("mic_id")
    private String micId;
    /**
     * 商家经纬度
     */
    private String lat;
    /**
     * 商家经纬度
     */
    private String lng;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getExamineDate() {
        return examineDate;
    }

    public void setExamineDate(Date examineDate) {
        this.examineDate = examineDate;
    }

    public String getMicId() {
        return micId;
    }

    public void setMicId(String micId) {
        this.micId = micId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MMerchantInfo{" +
        ", id=" + id +
        ", oid=" + oid +
        ", name=" + name +
        ", mobile=" + mobile +
        ", simpleName=" + simpleName +
        ", legalName=" + legalName +
        ", idCard=" + idCard +
        ", area=" + area +
        ", address=" + address +
        ", pwd=" + pwd +
        ", status=" + status +
        ", examineDate=" + examineDate +
        ", micId=" + micId +
        ", lat=" + lat +
        ", lng=" + lng +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
