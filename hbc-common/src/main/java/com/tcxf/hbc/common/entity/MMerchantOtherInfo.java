package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商户其他信息
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_merchant_other_info")
public class MMerchantOtherInfo extends Model<MMerchantOtherInfo> {

    private static final long serialVersionUID = 1L;
    /**
     * 信息填写未完成
     */
    public static final String ZLSTATUS_N = "1";
    /**
     * 信息填写已完成
     */
    public static final String ZLSTATUS_Y = "2";
    /**
     * 主键id
     */
    private String id;
    /**
     * 商户主键id，唯一
     */
    private String mid;
    /**
     * 商户性质
     */
    private String nature;
    /**
     * 品牌经营年限
     */
    @TableField("oper_year")
    private Integer operYear;
    /**
     * 经营面积（㎡）
     */
    @TableField("oper_area")
    private Integer operArea;
    /**
     * 注册资金（万）
     */
    @TableField("reg_money")
    private Integer regMoney;
    /**
     * 场地照片资源路径（c端显示图片）
     */
    @TableField("local_photo")
    private String localPhoto;
    /**
     * 营业执照图片资源路径
     */
    @TableField("license_pic")
    private String licensePic;
    /**
     * 法人身份照片资源路径
     */
    @TableField("legal_photo")
    private String legalPhoto;
    /**
     * 借记卡照片资源路径
     */
    @TableField("normal_card")
    private String normalCard;
    /**
     * 信用卡照片资源路径
     */
    @TableField("credit_card")
    private String creditCard;
    /**
     * 法人正面照资源路径
     */
    @TableField("lagal_face_photo")
    private String lagalFacePhoto;
    /**
     * 结算方式 0- t+1 , 1 - t+0
     */
    @TableField("settlement_type")
    private String settlementType;
    /**
     * 授信是否有上限
     */
    @TableField("auth_max_flag")
    private String authMaxFlag;
    /**
     * 授信上限值
     */
    @TableField("auth_max")
    private Long authMax;
    /**
     * 结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年）
     */
    @TableField("settlement_loop")
    private String settlementLoop;
    /**
     * 清算日期（结算日）0 开启 1 关闭
     */
    @TableField("clear_day")
    private Integer clearDay;
    /**
     * 商户让利费率（百分之）
     */
    @TableField("share_fee")
    private BigDecimal shareFee;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 开店时间（运营商审核的时间）
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @TableField("open_date")
    private String openDate;
    /**
     * 0-未提交 1-待审核 2-审核失败 3-审核成功
     */
    @TableField("auth_examine")
    private String authExamine;
    /**
     * 审核失败原因
     */
    @TableField("examine_fail_reason")
    private String examineFailReason;
    /**
     * 商家电话
     */
    @TableField("phone_number")
    private String phoneNumber;
    /**
     * 店铺推荐商品简介
     */
    @TableField("recommend_goods")
    private String recommendGoods;
    /**
     *  授信状态 0-未提交 1-待审核 2-审核中 3-审核成功 4审核失败5线下审核
     */
    @TableField("syauth_examine")
    private String syauthExamine;
    /**
     * 授信审核失败原因
     */
    @TableField("syexamine_fail_reason")
    private String syexamineFailReason;
    /**
     * 申请授信时间
     */
    @TableField("sy_date")
    private Date syDate;
    /**
     * 商家资料填写状态 0.未完成 1.已完成
     */
    @TableField("zl_status")
    private String zlStatus;
    /**
     *  出账日期
     */
    @TableField("ooa_date")
    private String ooaDate;
    /**
     * 还款日期
     */
    @TableField("repay_date")
    private String repayDate;
    /**
     * 多头借贷接口json
     */
    @TableField("borrowing_multi_json")
    private String borrowingMultiJson;
    /**
     * 多头借贷接口状态（0失败 1成功）
     */
    @TableField("borrowing_multi_status")
    private String borrowingMultiStatus;
    /**
     * 个人投资任职接口json
     */
    @TableField("investment_tenure_json")
    private String investmentTenureJson;
    /**
     * 个人投资任职接口状态（0失败 1成功）
     */
    @TableField("investment_tenure_status")
    private String investmentTenureStatus;
    /**
     * 银行卡接口json
     */
    @TableField("bank_card_json")
    private String bankCardJson;
    /**
     * 银行卡接口状态（0失败 1成功）
     */
    @TableField("bank_card_status")
    private String bankCardStatus;

    /**
     * 最低提现金额 (元)
     */
    @TableField("minmoney")
    private BigDecimal minmoney;

    /**
     * 最迟提现时间 (天)
     */
    @TableField("latestday")
    private String latestday;

    public BigDecimal getMinmoney() {
        return minmoney;
    }

    public void setMinmoney(BigDecimal minmoney) {
        this.minmoney = minmoney;
    }

    public String getLatestday() {
        return latestday;
    }

    public void setLatestday(String latestday) {
        this.latestday = latestday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public Integer getOperArea() {
        return operArea;
    }

    public void setOperArea(Integer operArea) {
        this.operArea = operArea;
    }

    public Integer getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Integer regMoney) {
        this.regMoney = regMoney;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getAuthMaxFlag() {
        return authMaxFlag;
    }

    public void setAuthMaxFlag(String authMaxFlag) {
        this.authMaxFlag = authMaxFlag;
    }

    public Long getAuthMax() {
        return authMax;
    }

    public void setAuthMax(Long authMax) {
        this.authMax = authMax;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public Integer getClearDay() {
        return clearDay;
    }

    public void setClearDay(Integer clearDay) {
        this.clearDay = clearDay;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }

    public String getExamineFailReason() {
        return examineFailReason;
    }

    public void setExamineFailReason(String examineFailReason) {
        this.examineFailReason = examineFailReason;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRecommendGoods() {
        return recommendGoods;
    }

    public void setRecommendGoods(String recommendGoods) {
        this.recommendGoods = recommendGoods;
    }

    public String getSyauthExamine() {
        return syauthExamine;
    }

    public void setSyauthExamine(String syauthExamine) {
        this.syauthExamine = syauthExamine;
    }

    public String getSyexamineFailReason() {
        return syexamineFailReason;
    }

    public void setSyexamineFailReason(String syexamineFailReason) {
        this.syexamineFailReason = syexamineFailReason;
    }

    public Date getSyDate() {
        return syDate;
    }

    public void setSyDate(Date syDate) {
        this.syDate = syDate;
    }

    public String getZlStatus() {
        return zlStatus;
    }

    public void setZlStatus(String zlStatus) {
        this.zlStatus = zlStatus;
    }

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getBorrowingMultiJson() {
        return borrowingMultiJson;
    }

    public void setBorrowingMultiJson(String borrowingMultiJson) {
        this.borrowingMultiJson = borrowingMultiJson;
    }

    public String getBorrowingMultiStatus() {
        return borrowingMultiStatus;
    }

    public void setBorrowingMultiStatus(String borrowingMultiStatus) {
        this.borrowingMultiStatus = borrowingMultiStatus;
    }

    public String getInvestmentTenureJson() {
        return investmentTenureJson;
    }

    public void setInvestmentTenureJson(String investmentTenureJson) {
        this.investmentTenureJson = investmentTenureJson;
    }

    public String getInvestmentTenureStatus() {
        return investmentTenureStatus;
    }

    public void setInvestmentTenureStatus(String investmentTenureStatus) {
        this.investmentTenureStatus = investmentTenureStatus;
    }

    public String getBankCardJson() {
        return bankCardJson;
    }

    public void setBankCardJson(String bankCardJson) {
        this.bankCardJson = bankCardJson;
    }

    public String getBankCardStatus() {
        return bankCardStatus;
    }

    public void setBankCardStatus(String bankCardStatus) {
        this.bankCardStatus = bankCardStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MMerchantOtherInfo{" +
        ", id=" + id +
        ", mid=" + mid +
        ", nature=" + nature +
        ", operYear=" + operYear +
        ", operArea=" + operArea +
        ", regMoney=" + regMoney +
        ", localPhoto=" + localPhoto +
        ", licensePic=" + licensePic +
        ", legalPhoto=" + legalPhoto +
        ", normalCard=" + normalCard +
        ", creditCard=" + creditCard +
        ", lagalFacePhoto=" + lagalFacePhoto +
        ", settlementType=" + settlementType +
        ", authMaxFlag=" + authMaxFlag +
        ", authMax=" + authMax +
        ", settlementLoop=" + settlementLoop +
        ", clearDay=" + clearDay +
        ", shareFee=" + shareFee +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", openDate=" + openDate +
        ", authExamine=" + authExamine +
        ", examineFailReason=" + examineFailReason +
        ", phoneNumber=" + phoneNumber +
        ", recommendGoods=" + recommendGoods +
        ", syauthExamine=" + syauthExamine +
        ", syexamineFailReason=" + syexamineFailReason +
        ", syDate=" + syDate +
        ", zlStatus=" + zlStatus +
        ", ooaDate=" + ooaDate +
        ", repayDate=" + repayDate +
        ", borrowingMultiJson=" + borrowingMultiJson +
        ", borrowingMultiStatus=" + borrowingMultiStatus +
        ", investmentTenureJson=" + investmentTenureJson +
        ", investmentTenureStatus=" + investmentTenureStatus +
        ", bankCardJson=" + bankCardJson +
        ", bankCardStatus=" + bankCardStatus +
        "}";
    }
}
