package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户领取优惠券记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_red_packet")
public class MRedPacket extends Model<MRedPacket> {

    private static final long serialVersionUID = 1L;
    /**
     * 已使用
     */
    public static final String IS_USE_YES = "1";
    /**
     * 未使用
     */
    public static final String IS_USE_NO = "0";

    /**
     * 主键id
     */
    private String id;
    /**
     * 优惠券名称
     */
    private String name;
    /**
     * 所属商户id
     */
    @TableField("mi_id")
    private String miId;
    /**
     * 优惠金额
     */
    private BigDecimal money;
    /**
     * 优惠券副标题
     */
    @TableField("sub_title")
    private String subTitle;
    /**
     * 订单金额需要达到该金额才可以使用
     */
    @TableField("full_money")
    private BigDecimal fullMoney;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;
    /**
     * 是否使用
     */
    @TableField("is_use")
    private String isUse;
    /**
     * 有效期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    @TableField("end_date")
    private Date endDate;
    /**
     * 所属运营商id
     */
    private String oid;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    @TableField("modify_date")
    private Date modifyDate;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    @TableField("create_date")
    private Date createDate;
    /**
     * 优惠券配置信息
     */
    @TableField("rps_id")
    private String rpsId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public BigDecimal getFullMoney() {
        return fullMoney;
    }

    public void setFullMoney(BigDecimal fullMoney) {
        this.fullMoney = fullMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsUse() {
        return isUse;
    }

    public void setIsUse(String isUse) {
        this.isUse = isUse;
    }

    public Date getEndDate() {
        return endDate;
    }



    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRpsId() {
        return rpsId;
    }

    public void setRpsId(String rpsId) {
        this.rpsId = rpsId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRedPacket{" +
        ", id=" + id +
        ", name=" + name +
        ", miId=" + miId +
        ", money=" + money +
        ", subTitle=" + subTitle +
        ", fullMoney=" + fullMoney +
        ", userId=" + userId +
        ", isUse=" + isUse +
        ", endDate=" + endDate +
        ", oid=" + oid +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        ", rpsId=" + rpsId +
        "}";
    }
}
