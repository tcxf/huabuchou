package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 优惠券表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("m_red_packet_setting")
public class MRedPacketSetting extends Model<MRedPacketSetting> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 优惠券名称
     */
    private String name;
    /**
     * 所属商户id
     */
    @TableField("mi_id")
    private String miId;
    /**
     * 商户设置的原始优惠金额
     */
    private BigDecimal money;
    /**
     * 实际优惠金额（运营商抽成后的优惠金额）
     */
    @TableField("actual_money")
    private BigDecimal actualMoney;
    /**
     * 运营商抽取优惠金额(用于三级分销分润)
     */
    @TableField("op_money")
    private BigDecimal opMoney;
    /**
     * 优惠券副标题
     */
    @TableField("sub_title")
    private String subTitle;
    /**
     * 订单金额需要达到该金额才可以使用
     */
    @TableField("full_money")
    private BigDecimal fullMoney;
    /**
     * 每次领取的数量
     */
    private Integer num;
    /**
     * 类型 1-固定截止时间 2-可用天数
     */
    @TableField("time_type")
    private Integer timeType;
    /**
     * 有效期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd", timezone = "GMT+8")
    @TableField("end_date")
    private Date endDate;
    /**
     * 有效期时间
     */
    private Integer days;
    /**
     * 所属运营商id
     */
    private String oid;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 1 审核中，2：审核通过，3：失效，4：审核不通过
     */
    private String status;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getActualMoney() {
        return actualMoney;
    }

    public void setActualMoney(BigDecimal actualMoney) {
        this.actualMoney = actualMoney;
    }

    public BigDecimal getOpMoney() {
        return opMoney;
    }

    public void setOpMoney(BigDecimal opMoney) {
        this.opMoney = opMoney;
    }

    public BigDecimal getFullMoney() {
        return fullMoney;
    }

    public void setFullMoney(BigDecimal fullMoney) {
        this.fullMoney = fullMoney;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MRedPacketSetting{" +
        ", id=" + id +
        ", name=" + name +
        ", miId=" + miId +
        ", money=" + money +
        ", actualMoney=" + actualMoney +
        ", opMoney=" + opMoney +
        ", subTitle=" + subTitle +
        ", fullMoney=" + fullMoney +
        ", num=" + num +
        ", timeType=" + timeType +
        ", endDate=" + endDate +
        ", days=" + days +
        ", oid=" + oid +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        ", status=" + status +
        "}";
    }
}
