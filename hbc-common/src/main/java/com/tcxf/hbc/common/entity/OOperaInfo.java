package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 运营商信息表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("o_opera_info")
public class OOperaInfo extends Model<OOperaInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商名称
     */
    private String name;
    /**
     * 运营商密码
     */
    private String pwd;
    /**
     * 运营商编码 即二级域名
     */
    private String osn;
    /**
     * 运营商微信公众号appid
     */
    private String appid;
    /**
     * 运营商微信公众号appsecret
     */
    private String appsecret;
    /**
     * 运营商状态0-未开通 1-可用 2-禁用
     */
    private String status;
    /**
     * 手机号码（默认作为登录帐号）
     */
    private String mobile;
    /**
     * 账号（默认用手机号）
     */
    private String acount;
    /**
     * 运营商简称
     */
    @TableField("simple_name")
    private String simpleName;
    /**
     * 法人名称
     */
    @TableField("legal_name")
    private String legalName;
    /**
     * 法人身份证
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 运营商地区
     */
    private String area;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-ddHH:mm:ss" )
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-ddHH:mm:ss" )
    @TableField("modify_date")
    private Date modifyDate;


    /**
     * 是否开通结算单功能
     * @return
     */
    @TableField("settlement_status")
    private String settlementStatus;


    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getOsn() {
        return osn;
    }

    public void setOsn(String osn) {
        this.osn = osn;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAcount() {
        return acount;
    }

    public void setAcount(String acount) {
        this.acount = acount;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    //@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" , locale = "zh" , timezone="GMT+8" )
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    //@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
   @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8" )
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OOperaInfo{" +
        ", id=" + id +
        ", name=" + name +
        ", pwd=" + pwd +
        ", osn=" + osn +
        ", appid=" + appid +
        ", appsecret=" + appsecret +
        ", status=" + status +
        ", mobile=" + mobile +
        ", acount=" + acount +
        ", simpleName=" + simpleName +
        ", legalName=" + legalName +
        ", idCard=" + idCard +
        ", address=" + address +
        ", area=" + area +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
