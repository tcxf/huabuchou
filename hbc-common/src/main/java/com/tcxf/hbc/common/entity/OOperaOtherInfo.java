package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 运营商其他信息表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("o_opera_other_info")
public class OOperaOtherInfo extends Model<OOperaOtherInfo> {

    private static final long serialVersionUID = 1L;

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    @TableField("settlement_loop")
    private String settlementLoop;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商主键id，唯一

     */
    private String oid;
    /**
     * 品牌经营年限
     */
    @TableField("oper_year")
    private Integer operYear;
    /**
     *  经营面积（㎡）
     */
    @TableField("oper_area")
    private String operArea;
    /**
     * 营业执照图片资源路径
     */
    @TableField("license_pic")
    private String licensePic;
    /**
     * 借记卡照片资源路径
     */
    @TableField("normal_card")
    private String normalCard;


    /**
     * 平台抽取运营商服务费率（百分之）
     */
    @TableField("platfrom_share_ratio")
    private BigDecimal platfromShareRatio;
    /**
     * 场地照片资源路径
     */
    @TableField("legal_photo")
    private String legalPhoto;
    /**
     *  信用卡照片资源路径
     */
    @TableField("credit_card")
    private String creditCard;
    /**
     * 运营商性质
     */
    private String nature;
    /**
     * 场地照片资源路径
     */
    @TableField("local_photo")
    private String localPhoto;
    /**
     * 注册资金（万）
     */
    @TableField("reg_money")
    private Long regMoney;
    /**
     * 法人正面照资源路径
     */
    @TableField("lagal_face_photo")
    private String lagalFacePhoto;
    /**
     * 短信费用/条
     */
    @TableField("message_fee")
    private Long messageFee;
    /**
     * 积分返利比例 %（根据消费金额获取积分）
     */
    @TableField("score_percent")
    private BigDecimal scorePercent;
    /**
     * 记录修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-ddHH:mm:ss" )
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 记录创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-ddHH:mm:ss" )
    @TableField("create_date")
    private Date createDate;

    /**
     * 出账日期
     */
    private String ooaDate;

    /**
     * 还款日期
     */
    private String repayDate;

    /**
     * 修改出账日期 和还款日起的值 0可以修改 1不可修改
     */
    private Integer type;

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public String getOperArea() {
        return operArea;
    }

    public void setOperArea(String operArea) {
        this.operArea = operArea;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }


    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public Long getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Long regMoney) {
        this.regMoney = regMoney;
    }

    public Long getMessageFee() {
        return messageFee;
    }

    public void setMessageFee(Long messageFee) {
        this.messageFee = messageFee;
    }

    public BigDecimal getScorePercent() {
        return scorePercent;
    }

    public void setScorePercent(BigDecimal scorePercent) {
        this.scorePercent = scorePercent;
    }

    //@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" , locale = "zh" , timezone="GMT+8" )
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

   // @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" , locale = "zh" , timezone="GMT+8" )
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }


    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OOperaOtherInfo{" +
        ", id=" + id +
        ", oid=" + oid +
        ", operYear=" + operYear +
        ", operArea=" + operArea +
        ", licensePic=" + licensePic +
        ", normalCard=" + normalCard +
        ", platfromShareRatio=" + platfromShareRatio +
        ", legalPhoto=" + legalPhoto +
        ", creditCard=" + creditCard +
        ", nature=" + nature +
        ", localPhoto=" + localPhoto +
        ", regMoney=" + regMoney +
        ", lagalFacePhoto=" + lagalFacePhoto +
        ", messageFee=" + messageFee +
        ", scorePercent=" + scorePercent +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
