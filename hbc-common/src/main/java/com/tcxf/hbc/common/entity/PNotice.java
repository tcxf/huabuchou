package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
@TableName("p_notice")
public class PNotice extends Model<PNotice> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 还款计划表id
     */
    @TableField("plan_id")
    private String planId;
    /**
     * 前三日类型(0:未通知,1:已通知,2:占线,3:空号,4:停机)
     */
    @TableField("three_day")
    private String threeDay;
    /**
     * 前一日类型(0:未通知,1:已通知,2:占线,3:空号,4:停机)
     */
    @TableField("one_day")
    private String oneDay;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("update_time")
    private Date updateTime;
    /**
     * 预计还款时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("plan_time")
    private Date planTime;
    /**
     * 备注
     */
    private String content;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getThreeDay() {
        return threeDay;
    }

    public void setThreeDay(String threeDay) {
        this.threeDay = threeDay;
    }

    public String getOneDay() {
        return oneDay;
    }

    public void setOneDay(String oneDay) {
        this.oneDay = oneDay;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PNotice{" +
        ", id=" + id +
        ", planId=" + planId +
        ", threeDay=" + threeDay +
        ", oneDay=" + oneDay +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", planTime=" + planTime +
        ", content=" + content +
        "}";
    }
}
