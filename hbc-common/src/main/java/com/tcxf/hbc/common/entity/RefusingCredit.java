package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.Date;

/**
 * 拒绝授信
 * @Auther: liuxu
 * @Date: 2018/9/18 09:47
 * @Description:
 */
@TableName("rm_refusing_credit")
public class RefusingCredit extends Model<RefusingCredit> {
    /**
     * 启用
     */
    public final static String status_1="1";

    /**
     *  是否允许二次授信 1 允许  0  不允许
     */
    public final static String TWOAPPLICATIONS_Y="1";

    public final static String TWOAPPLICATIONS_N="0";
    /**
     * 主键id
     */
    private String id;

    /**
     * 条件代码
     */
    private String code;

    /**
     * 条件代码描述  逾期平台   单平台逾期次数  欺诈风险等等
     */
    private String name;

    /**
     * 条件个数
     */
    private Integer number;

    /**
     * 条件描述
     */
    private String content;
    /**
     * 是否允许二次授信 1 允许  0  不允许
     */
    private String twoapplications;

    /**
     * 状态 1 启用  0 禁用
     */
    private String status;
    /**
     * 版本号ID
     */
    @TableField("version_id")
    private String  versionId;

    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 大版本Id
     */
    @TableField(exist = false)
    private String relationshipId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTwoapplications() {
        return twoapplications;
    }

    public void setTwoapplications(String twoapplications) {
        this.twoapplications = twoapplications;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
