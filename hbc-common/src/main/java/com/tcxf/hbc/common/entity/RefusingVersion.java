package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 授信版本
 * @Auther: liuxu
 * @Date: 2018/9/18 14:35
 * @Description:
 */
@TableName("rm_refusing_version")
public class RefusingVersion extends Model<RefusingVersion> {

    /**
     * 有效
     */
    public final static String STATUS_YES="1";
    /**
     * 过期
     */
    public final static String STATUS_NO="0";

    /**
     * 拒绝授信
     */
    public final static String TYPE_1="1";

    /**
     * 初始授信
     */
    public final static String TYPE_2="2";

    /**
     * 提额授信
     */
    public final static String TYPE_3="3";

    /**
     * 授信提额综合分值
     */
    public final static String TYPE_4="4";

    private String id;

    /**
     * 资金端ID
     */
    private String fid;

    /**
     * 授信类型 拒绝授信 初始授信 授信提额 授信提额综合分值
     */
    private String type;

    /**
     * 版本号
     */
    private String version;

    /**
     * 创建时间
     */
    @TableField("createDate")
    private Date createDate;


    @Override
    protected Serializable pkVal() {
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

}
