package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 初始授信条件模板表
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
@TableName("rm_base_credit_initial")
public class RmBaseCreditInitial extends Model<RmBaseCreditInitial> {

    private static final long serialVersionUID = 1L;

    /**
     * 有效
     */
    public final static String STATUS_YES="1";
    /**
     * 无效
     */
    public final static String STATUS_NO="0";

    /**
     * 主键id
     */
    private String id;
    /**
     * 条件描述
     */
    private String name;
    /**
     * 条件代码
     */
    private String code;
    /**
     * 条件状态（0.禁用,1.启用）
     */
    private String status;
    /**
     * 权重
     */
    private String value;

    /**
     * 条件个数
     */
    private Integer number;


    /**
     * 类别id
     */
    @TableField("typeId")
    private String typeId;
    /**
     * 版本id
     */
    @TableField("versionId")
    private String versionId;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 最小分值
     */
    @TableField("minScore")
    private BigDecimal minScore;

    /**
     * 最大分值
     */
    @TableField("maxScore")
    private BigDecimal maxScore;

    /**
     * 大版本Id
     */
    @TableField(exist = false)
    private String relationshipId;

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getMinScore() {
        return minScore;
    }

    public void setMinScore(BigDecimal minScore) {
        this.minScore = minScore;
    }

    public BigDecimal getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(BigDecimal maxScore) {
        this.maxScore = maxScore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RmBaseCreditInitial{" +
        ", id=" + id +
        ", name=" + name +
        ", code=" + code +
        ", status=" + status +
        ", value=" + value +
        ", typeId=" + typeId +
        ", versionId=" + versionId +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
