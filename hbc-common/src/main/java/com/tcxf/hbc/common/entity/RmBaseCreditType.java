package com.tcxf.hbc.common.entity;

import java.util.ArrayList;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 授信条件类别表
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
@TableName("rm_base_credit_type")
public class RmBaseCreditType extends Model<RmBaseCreditType> {

    private static final long serialVersionUID = 1L;

    /**
     * 初始授信类别
     */
    public final static String TYPE_0="0";

    /**
     * 授信提额类别
     */
    public final static String TYPE_1="1";

    /**
     * 初始授信内置类别
     */
    public final static String TYPE_2="2";

    /**
     * 主键id
     */
    private String id;
    /**
     * 条件描述
     */
    private String name;
    /**
     * 条件代码
     */
    private String code;
    /**
     * 条件状态（0.初始授信类别,1.授信提额类别,2.初始授信内置类别）
     */
    private String status;
    /**
     * 权重
     */
    private String value;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    @TableField(exist = false)
    private List<RmBaseCreditInitial> rbcilist = new ArrayList<>();

    @TableField(exist = false)
    private List<RmBaseCreditUpmoney> rbculist = new ArrayList<>();

    public List<RmBaseCreditUpmoney> getRbculist() {
        return rbculist;
    }

    public void setRbculist(List<RmBaseCreditUpmoney> rbculist) {
        this.rbculist = rbculist;
    }

    public List<RmBaseCreditInitial> getRbcilist() {
        return rbcilist;
    }

    public void setRbcilist(List<RmBaseCreditInitial> rbcilist) {
        this.rbcilist = rbcilist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RmBaseCreditType{" +
        ", id=" + id +
        ", name=" + name +
        ", code=" + code +
        ", status=" + status +
        ", value=" + value +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
