package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 授信月份统计表
 * </p>
 *
 * @author lengleng
 * @since 2018-10-10
 */
@TableName("rm_creditmonth_statistical")
public class RmCreditmonthStatistical extends Model<RmCreditmonthStatistical> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 月份
     */
    private String month;
    /**
     * 授信人数
     */
    @TableField("sx_people")
    private String sxPeople;
    /**
     * 授信额度(元)
     */
    @TableField("sx_money")
    private BigDecimal sxMoney;
    /**
     * 平均授信额度(元)
     */
    @TableField("avg_sx_money")
    private BigDecimal avgSxMoney;
    /**
     * 通过率
     */
    @TableField("pass_rate")
    private String passRate;
    /**
     * 额度使用率
     */
    @TableField("using_money_rate")
    private String usingMoneyRate;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getSxPeople() {
        return sxPeople;
    }

    public void setSxPeople(String sxPeople) {
        this.sxPeople = sxPeople;
    }

    public BigDecimal getSxMoney() {
        return sxMoney;
    }

    public void setSxMoney(BigDecimal sxMoney) {
        this.sxMoney = sxMoney;
    }

    public BigDecimal getAvgSxMoney() {
        return avgSxMoney;
    }

    public void setAvgSxMoney(BigDecimal avgSxMoney) {
        this.avgSxMoney = avgSxMoney;
    }

    public String getPassRate() {
        return passRate;
    }

    public void setPassRate(String passRate) {
        this.passRate = passRate;
    }

    public String getUsingMoneyRate() {
        return usingMoneyRate;
    }

    public void setUsingMoneyRate(String usingMoneyRate) {
        this.usingMoneyRate = usingMoneyRate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RmCreditmonthStatistical{" +
                ", id=" + id +
                ", oid=" + oid +
                ", fid=" + fid +
                ", month=" + month +
                ", sxPeople=" + sxPeople +
                ", sxMoney=" + sxMoney +
                ", avgSxMoney=" + avgSxMoney +
                ", passRate=" + passRate +
                ", usingMoneyRate=" + usingMoneyRate +
                ", createTime=" + createTime +
                "}";
    }
}
