package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 还款情况表
 * </p>
 *
 * @author lengleng
 * @since 2018-10-10
 */
@TableName("rm_repayment_situation")
public class RmRepaymentSituation extends Model<RmRepaymentSituation> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 违约率
     */
    @TableField("break_contract_rate")
    private String breakContractRate;
    /**
     * 不良率
     */
    @TableField("unhealthy_rate")
    private String unhealthyRate;
    /**
     * 坏账率
     */
    @TableField("bad_debt_rate")
    private String badDebtRate;
    /**
     * 还款周期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    @TableField("repayment_time")
    private Date repaymentTime;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getBreakContractRate() {
        return breakContractRate;
    }

    public void setBreakContractRate(String breakContractRate) {
        this.breakContractRate = breakContractRate;
    }

    public String getUnhealthyRate() {
        return unhealthyRate;
    }

    public void setUnhealthyRate(String unhealthyRate) {
        this.unhealthyRate = unhealthyRate;
    }

    public String getBadDebtRate() {
        return badDebtRate;
    }

    public void setBadDebtRate(String badDebtRate) {
        this.badDebtRate = badDebtRate;
    }

    public Date getRepaymentTime() {
        return repaymentTime;
    }

    public void setRepaymentTime(Date repaymentTime) {
        this.repaymentTime = repaymentTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RmRepaymentSituation{" +
        ", id=" + id +
        ", oid=" + oid +
        ", fid=" + fid +
        ", breakContractRate=" + breakContractRate +
        ", unhealthyRate=" + unhealthyRate +
        ", badDebtRate=" + badDebtRate +
        ", repaymentTime=" + repaymentTime +
        ", createTime=" + createTime +
        "}";
    }
}
