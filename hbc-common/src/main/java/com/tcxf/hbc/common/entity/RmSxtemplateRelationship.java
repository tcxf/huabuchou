package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 授信模板关系表
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
@TableName("rm_sxtemplate_relationship")
public class RmSxtemplateRelationship extends Model<RmSxtemplateRelationship> {

    private static final long serialVersionUID = 1L;

    /**
     * 是否当前版本 1:是 0:否
     */
    public static final String STATUS_YES = "1";

    public static final String STATUS_NO = "0";

    /**
     * 是否发布 1:是 0:否
     */
    public static final String ISPUBLISH_YES = "1";

    public static final String ISPUBLISH_NO = "0";

    /**
     * 主键id
     */
    private String id;
    /**
     * 版本号
     */
    private String version;
    /**
     * 资金端ID
     */
    private String fid;
    /**
     * 拒绝授信版本id
     */
    @TableField("sx_refusing_id")
    private String sxRefusingId;
    /**
     * 初始授信版本id
     */
    @TableField("sx_init_id")
    private String sxInitId;
    /**
     * 授信提额版本id
     */
    @TableField("sx_upmoney_id")
    private String sxUpmoneyId;
    /**
     * 授信提额综合分值版本id
     */
    @TableField("sx_upmoney_focus_id")
    private String sxUpmoneyFocusId;
    /**
     * 更新模块 json字符串
     */
    @TableField("update_module")
    private String updateModule;
    /**
     * 是否当前版本 1:是 0:否
     */
    private String status;
    /**
     * 是否发布 1:是 0:否
     */
    @TableField("is_publish")
    private String isPublish;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 发布时间
     */
    @TableField("publish_date")
    private Date publishDate;

    public String getSxUpmoneyId() {
        return sxUpmoneyId;
    }

    public void setSxUpmoneyId(String sxUpmoneyId) {
        this.sxUpmoneyId = sxUpmoneyId;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSxRefusingId() {
        return sxRefusingId;
    }

    public void setSxRefusingId(String sxRefusingId) {
        this.sxRefusingId = sxRefusingId;
    }

    public String getSxInitId() {
        return sxInitId;
    }

    public void setSxInitId(String sxInitId) {
        this.sxInitId = sxInitId;
    }

    public String getSxUpmoneyFocusId() {
        return sxUpmoneyFocusId;
    }

    public void setSxUpmoneyFocusId(String sxUpmoneyFocusId) {
        this.sxUpmoneyFocusId = sxUpmoneyFocusId;
    }

    public String getUpdateModule() {
        return updateModule;
    }

    public void setUpdateModule(String updateModule) {
        this.updateModule = updateModule;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(String isPublish) {
        this.isPublish = isPublish;
    }
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }


    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RmSxtemplateRelationship{" +
        ", id=" + id +
        ", fid=" + fid +
        ", version=" + version +
        ", sxRefusingId=" + sxRefusingId +
        ", sxInitId=" + sxInitId +
        ", sxUpmoneyId=" + sxUpmoneyId +
        ", sxUpmoneyFocusId=" + sxUpmoneyFocusId +
        ", updateModule=" + updateModule +
        ", status=" + status +
        ", isPublish=" + isPublish +
        ", createDate=" + createDate +
        ", updateDate=" + updateDate +
        ", publishDate=" + publishDate +
        "}";
    }
}
