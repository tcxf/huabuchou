package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 行政区划
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-28
 */
@TableName("sys_area")
public class SysArea extends Model<SysArea> {

    private static final long serialVersionUID = 1L;

    /**
     * @Description: 市级
     * @Param:
     * @return:
     * @Author: JinPeng
     * @Date: 2018/9/13
    */
    public final static String  TreeLevel_1="1";

    /**
     * @Description: 省级
     * @Param:
     * @return:
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    public final static String  TreeLevel_0="0";
    /**
     * @Description: 县级

     */
    public final static String  TreeLevel_2="2";



    /**
     * 区域编码
     */
    @TableId("area_code")
    private String areaCode;
    /**
     * 父级编号
     */
    @TableField("parent_code")
    private String parentCode;
    /**
     * 所有父级编号
     */
    @TableField("parent_codes")
    private String parentCodes;
    /**
     * 本级排序号（升序）
     */
    @TableField("tree_sort")
    private BigDecimal treeSort;
    /**
     * 所有级别排序号
     */
    @TableField("tree_sorts")
    private String treeSorts;
    /**
     * 是否最末级
     */
    @TableField("tree_leaf")
    private String treeLeaf;
    /**
     * 层次级别
     */
    @TableField("tree_level")
    private BigDecimal treeLevel;
    /**
     * 全节点名
     */
    @TableField("tree_names")
    private String treeNames;
    /**
     * 区域名称
     */
    @TableField("area_name")
    private String areaName;
    /**
     * 区域类型
     */
    @TableField("area_type")
    private String areaType;
    /**
     * 状态（0正常 1删除 2停用）
     */
    private String status;
    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 城市级别
     */
    @TableField("city_level")
    private String cityLevel;


    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentCodes() {
        return parentCodes;
    }

    public void setParentCodes(String parentCodes) {
        this.parentCodes = parentCodes;
    }

    public BigDecimal getTreeSort() {
        return treeSort;
    }

    public void setTreeSort(BigDecimal treeSort) {
        this.treeSort = treeSort;
    }

    public String getTreeSorts() {
        return treeSorts;
    }

    public void setTreeSorts(String treeSorts) {
        this.treeSorts = treeSorts;
    }

    public String getTreeLeaf() {
        return treeLeaf;
    }

    public void setTreeLeaf(String treeLeaf) {
        this.treeLeaf = treeLeaf;
    }

    public BigDecimal getTreeLevel() {
        return treeLevel;
    }

    public void setTreeLevel(BigDecimal treeLevel) {
        this.treeLevel = treeLevel;
    }

    public String getTreeNames() {
        return treeNames;
    }

    public void setTreeNames(String treeNames) {
        this.treeNames = treeNames;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaType() {
        return areaType;
    }

    public void setAreaType(String areaType) {
        this.areaType = areaType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCityLevel() {
        return cityLevel;
    }

    public void setCityLevel(String cityLevel) {
        this.cityLevel = cityLevel;
    }

    @Override
    protected Serializable pkVal() {
        return this.areaCode;
    }

    @Override
    public String toString() {
        return "SysArea{" +
                ", areaCode=" + areaCode +
                ", parentCode=" + parentCode +
                ", parentCodes=" + parentCodes +
                ", treeSort=" + treeSort +
                ", treeSorts=" + treeSorts +
                ", treeLeaf=" + treeLeaf +
                ", treeLevel=" + treeLevel +
                ", treeNames=" + treeNames +
                ", areaName=" + areaName +
                ", areaType=" + areaType +
                ", status=" + status +
                ", createBy=" + createBy +
                ", createDate=" + createDate +
                ", updateBy=" + updateBy +
                ", updateDate=" + updateDate +
                ", remarks=" + remarks +
                ", cityLevel=" + cityLevel +
                "}";
    }
}
