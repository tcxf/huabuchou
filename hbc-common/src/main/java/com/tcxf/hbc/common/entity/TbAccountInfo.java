package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户授信资金额度表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_account_info")
public class TbAccountInfo extends Model<TbAccountInfo> {

    private static final long serialVersionUID = 1L;

    //已冻结
    public static final String IS_FREEZE_YES="1";


    /**
     * 主键id
     */
    private String id;
    /**
     * 资源主键id（用户id或者商户id）
     */
    private String oid;
    /**
     * 所属用户类型（1-用户 2-商户）
     */
    private String utype;
    /**
     * 授信类型（1-消费者授信 2-商户授信）
     */
    @TableField("auth_type")
    private String authType;
    /**
     * 授信可用余额（des256加密）
     */
    private String balance;
    /**
     * 冻结金额（des256加密）
     */
    @TableField("frezz_money")
    private String frezzMoney;
    /**
     * 资金版本号（乐观锁）
     */
    @TableField("version_code")
    private Integer versionCode;
    /**
     * 授信最大限额（des256加密）
     */
    @TableField("max_money")
    private String maxMoney;
    /**
     * 授信是否冻结
     */
    @TableField("is_freeze")
    private String isFreeze;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 评分
     */
    @TableField("grade")
    private String grade;

    /**
     * 快速授信
     */
    @TableField("rapid_Money")
    private String rapidMoney;

    /**
     * 授信额度
     */
    @TableField("up_Money")
    private String upMoney;

    /**
     * 授信总额
     */
    @TableField("total_Money")
    private String totalMoney;


    public String getRapidMoney() {
        return rapidMoney;
    }

    public void setRapidMoney(String rapidMoney) {
        this.rapidMoney = rapidMoney;
    }

    public String getUpMoney() {
        return upMoney;
    }

    public void setUpMoney(String upMoney) {
        this.upMoney = upMoney;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFrezzMoney() {
        return frezzMoney;
    }

    public void setFrezzMoney(String frezzMoney) {
        this.frezzMoney = frezzMoney;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(String maxMoney) {
        this.maxMoney = maxMoney;
    }

    public String getIsFreeze() {
        return isFreeze;
    }

    public void setIsFreeze(String isFreeze) {
        this.isFreeze = isFreeze;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbAccountInfo{" +
        ", id=" + id +
        ", oid=" + oid +
        ", utype=" + utype +
        ", authType=" + authType +
        ", balance=" + balance +
        ", frezzMoney=" + frezzMoney +
        ", versionCode=" + versionCode +
        ", maxMoney=" + maxMoney +
        ", isFreeze=" + isFreeze +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
