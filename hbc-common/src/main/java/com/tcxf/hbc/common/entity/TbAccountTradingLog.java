package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 授信资金流水
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_account_trading_log")
public class TbAccountTradingLog extends Model<TbAccountTradingLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 资金支出
     */
    public static Integer DIRECTION_0=0;

    /**
     * 主键id
     */
    private String id;
    /**
     * 资金账户主键id
     */
    private String aid;
    /**
     * 收支类型（0-支出 1-收入）
     */
    private Integer direction;
    /**
     * 业务类型编码
     */
    @TableField("biz_type")
    private String bizType;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * 账户结余
     */
    private BigDecimal balance;
    /**
     * 资源id（交易明细id、还款计划id等）
     */
    @TableField("res_id")
    private String resId;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbAccountTradingLog{" +
        ", id=" + id +
        ", aid=" + aid +
        ", direction=" + direction +
        ", bizType=" + bizType +
        ", amount=" + amount +
        ", balance=" + balance +
        ", resId=" + resId +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
