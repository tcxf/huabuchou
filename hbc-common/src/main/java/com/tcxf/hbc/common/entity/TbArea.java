package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地区表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_area")
public class TbArea extends Model<TbArea> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 地区名称
     */
    private String name;
    /**
     * 层级路径
     */
    private String path;
    /**
     * 地区完整名称
     */
    @TableField("display_name")
    private String displayName;
    /**
     * 上级地区id
     */
    @TableField("parent_id")
    private String parentId;
    /**
     * 层级
     */
    private Integer grade;
    /**
     * 排序id
     */
    @TableField("order_list")
    private Integer orderList;
    /**
     * 记录创建时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 记录修改时间
     */
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getOrderList() {
        return orderList;
    }

    public void setOrderList(Integer orderList) {
        this.orderList = orderList;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbArea{" +
        ", id=" + id +
        ", name=" + name +
        ", path=" + path +
        ", displayName=" + displayName +
        ", parentId=" + parentId +
        ", grade=" + grade +
        ", orderList=" + orderList +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
