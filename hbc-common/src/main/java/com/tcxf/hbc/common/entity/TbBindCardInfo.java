package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 绑卡信息
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_bind_card_info")
public class TbBindCardInfo extends Model<TbBindCardInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 提现
     */
    public static final String CONTRACTTYPE_1="1";
    /**
     * 支付
     */
    public static final String CONTRACTTYPE_2="2";
    /**
     * 提现 and 支付
     */
    public static final String CONTRACTTYPE_3="3";

    /**
     * 主键id
     */
    private String id;
    /**
     * 资源主键id（用户id或者商户id或运营商id或资金端id）
     */
    private String oid;
    /**
     * 所属用户类型（1-用户 2-商户 3-运营商 4-资金端）
     */
    private String utype;
    /**
     * 卡片类型（0-储蓄卡 ）
     */
    @TableField("card_type")
    private String cardType;
    /**
     * 银行卡编码
     */
    @TableField("bank_sn")
    private String bankSn;
    /**
     * 持卡人手机号
     */
    private String mobile;
    /**
     * 持卡人姓名
     */
    private String name;
    /**
     * 持卡人证件号
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 银行卡号
     */
    @TableField("bank_card")
    private String bankCard;
    /**
     * 有效期
     */
    @TableField("vali_date")
    private String valiDate;
    /**
     * cvv码
     */
    private String cvv;
    /**
     * 是否为主卡
     */
    @TableField("is_default")
    private String isDefault;
    /**
     * 开户银行
     */
    @TableField("bank_name")
    private String bankName;
    /**
     * 支付通道协议号
     */
    @TableField("contract_id")
    private String contractId;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 支付银行卡状态 1:提现 2:支付 3:提现加支付
     */
    @TableField("contract_type")
    private String contractType;

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBankSn() {
        return bankSn;
    }

    public void setBankSn(String bankSn) {
        this.bankSn = bankSn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbBindCardInfo{" +
        ", id=" + id +
        ", oid=" + oid +
        ", utype=" + utype +
        ", cardType=" + cardType +
        ", bankSn=" + bankSn +
        ", mobile=" + mobile +
        ", name=" + name +
        ", idCard=" + idCard +
        ", bankCard=" + bankCard +
        ", valiDate=" + valiDate +
        ", cvv=" + cvv +
        ", isDefault=" + isDefault +
        ", bankName=" + bankName +
        ", contractId=" + contractId +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
