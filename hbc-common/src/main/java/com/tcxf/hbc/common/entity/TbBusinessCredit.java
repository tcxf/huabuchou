package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商业授信表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
@TableName("tb_business_credit")
public class TbBusinessCredit extends Model<TbBusinessCredit> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 绑定商户id
     */
    private String mid;
    /**
     * 状态（0，未通过，1，已通过，2，已报名）
     */
    private String status;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 授信额度
     */
    @TableField("auth_max")
    private String authMax;
    /**
     * 分期数
     */
    @TableField("total_times")
    private String totalTimes;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getAuthMax() {
        return authMax;
    }

    public void setAuthMax(String authMax) {
        this.authMax = authMax;
    }

    public String getTotalTimes() {
        return totalTimes;
    }

    public void setTotalTimes(String totalTimes) {
        this.totalTimes = totalTimes;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbBusinessCredit{" +
        ", id=" + id +
        ", uid=" + uid +
        ", oid=" + oid +
        ", status=" + status +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", authMax=" + authMax +
        ", totalTimes=" + totalTimes +
        "}";
    }
}
