package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商业授信审核表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
@TableName("tb_business_credit_checklist")
public class TbBusinessCreditChecklist extends Model<TbBusinessCreditChecklist> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 资源主键id（商业授信ID）
     */
    private String bid;
    /**
     * 审核结果 0审核失败 1审核成功
     */
    private String status;
    /**
     * 审批人（当前登录运营商姓名）
     */
    @TableField("approver_name")
    private String approverName;
    /**
     * 审批人手机号码（当前登录运营商手机号码）
     */
    private String mobile;
    /**
     * 最后给予授信额度（不能大于授信额度）
     */
    @TableField("end_moeny")
    private String endMoeny;
    /**
     * 纪录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 纪录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEndMoeny() {
        return endMoeny;
    }

    public void setEndMoeny(String endMoeny) {
        this.endMoeny = endMoeny;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbBusinessCreditChecklist{" +
        ", id=" + id +
        ", bid=" + bid +
        ", status=" + status +
        ", approverName=" + approverName +
        ", mobile=" + mobile +
        ", endMoeny=" + endMoeny +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
