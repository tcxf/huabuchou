package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 我的收藏表
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@TableName("tb_collect")
public class TbCollect extends Model<TbCollect> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 当前登陆的id
     */
    private String uid;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbCollect{" +
        ", id=" + id +
        ", uid=" + uid +
        ", mid=" + mid +
        ", createDate=" + createDate +
        "}";
    }
}
