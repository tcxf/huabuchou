package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 快速授信评分项
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
@TableName("tb_credit_grade")
public class TbCreditGrade extends Model<TbCreditGrade> {

    private static final long serialVersionUID = 1L;

    /**
     *  是否有效 0:无效 1:有效
     */
    public final static String STATUS_Y="1";

    public final static String STATUS_N="0";


    private String id;
    /**
     * 详情
     */
    private String details;
    /**
     * 名称
     */
    private String name;
    /**
     * 英文标识
     */
    private String code;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 评分
     */
    private String value;
    /**
     * 是否有效 0:无效 1:有效
     */
    private String status;

    /**
     * 版本id
     */
    @TableField("version_id")
    private String versionId;
    /**
     * 运营商id
     */
    private String oid;


    @TableField("sx_state")
    private String sxState;


    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSxState() {
        return sxState;
    }

    public void setSxState(String sxState) {
        this.sxState = sxState;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbCreditGrade{" +
                "id='" + id + '\'' +
                ", details='" + details + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", uid='" + uid + '\'' +
                ", value='" + value + '\'' +
                ", status='" + status + '\'' +
                ", versionId='" + versionId + '\'' +
                ", oid='" + oid + '\'' +
                ", sxState='" + sxState + '\'' +
                '}';
    }
}
