package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 消费者用户提额授信结果表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_improve_quota")
public class TbImproveQuota extends Model<TbImproveQuota> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String uid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 芝麻信用接口信息
     */
    @TableField("sesame_credit_json")
    private String sesameCreditJson;
    /**
     * 芝麻信用接口状态
     */
    @TableField("sesame_credit_status")
    private String sesameCreditStatus;
    /**
     * 信用卡接口信息
     */
    @TableField("credit_card_json")
    private String creditCardJson;
    /**
     * 信用卡接口状态
     */
    @TableField("credit_card_status")
    private String creditCardStatus;
    /**
     * 学信接口信息
     */
    @TableField("student_info_json")
    private String studentInfoJson;
    /**
     * 学信接口状态
     */
    @TableField("student_info_status")
    private String studentInfoStatus;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 电话运营商接口（6个月同行记录信息）
     */
    @TableField("yys_json")
    private String yysJson;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSesameCreditJson() {
        return sesameCreditJson;
    }

    public void setSesameCreditJson(String sesameCreditJson) {
        this.sesameCreditJson = sesameCreditJson;
    }

    public String getSesameCreditStatus() {
        return sesameCreditStatus;
    }

    public void setSesameCreditStatus(String sesameCreditStatus) {
        this.sesameCreditStatus = sesameCreditStatus;
    }

    public String getCreditCardJson() {
        return creditCardJson;
    }

    public void setCreditCardJson(String creditCardJson) {
        this.creditCardJson = creditCardJson;
    }

    public String getCreditCardStatus() {
        return creditCardStatus;
    }

    public void setCreditCardStatus(String creditCardStatus) {
        this.creditCardStatus = creditCardStatus;
    }

    public String getStudentInfoJson() {
        return studentInfoJson;
    }

    public void setStudentInfoJson(String studentInfoJson) {
        this.studentInfoJson = studentInfoJson;
    }

    public String getStudentInfoStatus() {
        return studentInfoStatus;
    }

    public void setStudentInfoStatus(String studentInfoStatus) {
        this.studentInfoStatus = studentInfoStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getYysJson() {
        return yysJson;
    }

    public void setYysJson(String yysJson) {
        this.yysJson = yysJson;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbImproveQuota{" +
        ", id=" + id +
        ", uid=" + uid +
        ", oid=" + oid +
        ", sesameCreditJson=" + sesameCreditJson +
        ", sesameCreditStatus=" + sesameCreditStatus +
        ", creditCardJson=" + creditCardJson +
        ", creditCardStatus=" + creditCardStatus +
        ", studentInfoJson=" + studentInfoJson +
        ", studentInfoStatus=" + studentInfoStatus +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", yysJson=" + yysJson +
        "}";
    }
}
