package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 短信记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_message_log")
public class TbMessageLog extends Model<TbMessageLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 商户id所属运营商(短信归属运营商)
     */
    private String oid;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 接收消息的商户id
     */
    private String mid;
    /**
     * 接收消息的用户id
     */
    private String uid;
    /**
     * 1-短信 2-站内信
     */
    private String type;
    /**
     * 短信业务类型（如注册、登录、体现、还款、授信结果等）
     */
    @TableField("biz_type")
    private String bizType;
    /**
     * 短信费用
     */
    private Long fee;
    /**
     * 请求发送的ip地址
     */
    private String ip;
    /**
     * 用户目标电话
     */
    private String mobile;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbMessageLog{" +
        ", id=" + id +
        ", oid=" + oid +
        ", content=" + content +
        ", mid=" + mid +
        ", uid=" + uid +
        ", type=" + type +
        ", bizType=" + bizType +
        ", fee=" + fee +
        ", ip=" + ip +
        ", mobile=" + mobile +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
