package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;


/**
 *  短信模板表
 *  liaozeyong
 */
@TableName("tb_message_template")
public  class TbMessageTemplate extends Model<TbMessageTemplate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 内容
     */
    private String content;
    /**
     * 字段说明(开头大写字母 提前还款)
     * 提前还款     TQHK
     * 逾期七天     YQ
     * 正常还款成功ZCHK
     * 授信未通过SXWTG
     * 授信已动过SXYTG
     * 待结算账单已生成DJXZD
     * 授信金成功支付SXJZF
     * 收款授信金SXJSK
     * 本月账单和还款日ZD
     * 本月账单成功还款ZDHK
     * 运营商完成商户订单DDJS
     * 验证码YZM
     * 成功消费订单CGXF
     */
    private String utype;
    /**
     * 描述
     */
    private String explan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getExplan() {
        return explan;
    }

    public void setExplan(String explan) {
        this.explan = explan;
    }

    @Override
    public String toString() {
        return "TbMessageTemplate{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", utype='" + utype + '\'' +
                ", explan='" + explan + '\'' +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
