package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_operation_log")
public class TbOperationLog extends Model<TbOperationLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商id
     */
    private Long oid;
    /**
     * 操作内容
     */
    @TableField("desc_info")
    private String descInfo;
    /**
     * 操作类型
     */
    private String type;
    /**
     * 操作用户（商户id、用户id、管理员id、运营商管理员id）
     */
    @TableField("operation_user")
    private String operationUser;
    /**
     * 操作用户类型（0-商户 1-用户 2-平台管理员 3-运营商管理员）
     */
    @TableField("user_type")
    private String userType;
    /**
     * 操作ip地址
     */
    @TableField("operation_ip")
    private String operationIp;
    /**
     * 操作地区名称
     */
    @TableField("operation_area")
    private String operationArea;
    /**
     * 请求方法
     */
    private String method;
    /**
     * 请求参数
     */
    @TableField("request_param")
    private String requestParam;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getDescInfo() {
        return descInfo;
    }

    public void setDescInfo(String descInfo) {
        this.descInfo = descInfo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperationUser() {
        return operationUser;
    }

    public void setOperationUser(String operationUser) {
        this.operationUser = operationUser;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getOperationIp() {
        return operationIp;
    }

    public void setOperationIp(String operationIp) {
        this.operationIp = operationIp;
    }

    public String getOperationArea() {
        return operationArea;
    }

    public void setOperationArea(String operationArea) {
        this.operationArea = operationArea;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbOperationLog{" +
        ", id=" + id +
        ", oid=" + oid +
        ", descInfo=" + descInfo +
        ", type=" + type +
        ", operationUser=" + operationUser +
        ", userType=" + userType +
        ", operationIp=" + operationIp +
        ", operationArea=" + operationArea +
        ", method=" + method +
        ", requestParam=" + requestParam +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
