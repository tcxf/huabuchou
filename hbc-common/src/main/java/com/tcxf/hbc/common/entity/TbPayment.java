package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 支付记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_payment")
public class TbPayment extends Model<TbPayment> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    private String type;
    /**
     * 所属商户id
     */
    @TableField("mi_id")
    private String miId;
    /**
     * 支付状态 0-待支付 1-支付成功
     */
    private String status;
    /**
     *  订单编号
     */
    @TableField("oi_id")
    private String oiId;
    /**
     * 支付编号
     */
    @TableField("payment_sn")
    private String paymentSn;
    /**
     * 支付金额
     */
    private BigDecimal money;
    /**
     * 所属运营商id
     */
    private String oid;
    @TableField("modify_date")
    private Date modifyDate;
    @TableField("create_date")
    private Date createDate;
    private String ip;
    /**
     * 支付产品类型
     */
    @TableField("payment_type")
    private String paymentType;
    /**
     * 支付时间
     */
    @TableField("pay_date")
    private Date payDate;
    private String openid;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOiId() {
        return oiId;
    }

    public void setOiId(String oiId) {
        this.oiId = oiId;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbPayment{" +
        ", id=" + id +
        ", type=" + type +
        ", miId=" + miId +
        ", status=" + status +
        ", oiId=" + oiId +
        ", paymentSn=" + paymentSn +
        ", money=" + money +
        ", oid=" + oid +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        ", ip=" + ip +
        ", paymentType=" + paymentType +
        ", payDate=" + payDate +
        ", openid=" + openid +
        "}";
    }
}
