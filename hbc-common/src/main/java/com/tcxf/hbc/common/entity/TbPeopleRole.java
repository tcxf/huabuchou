package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 后台用户角色关系表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_people_role")
public class TbPeopleRole extends Model<TbPeopleRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户类型1：商户，2：运营商，3：平台
     */
    @TableField("people_user_type")
    private String peopleUserType;
    /**
     * 用户id,用户表主键
     */
    @TableField("people_user_id")
    private String peopleUserId;
    /**
     * 角色id,角色表主键
     */
    @TableField("role_id")
    private String roleId;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeopleUserType() {
        return peopleUserType;
    }

    public void setPeopleUserType(String peopleUserType) {
        this.peopleUserType = peopleUserType;
    }

    public String getPeopleUserId() {
        return peopleUserId;
    }

    public void setPeopleUserId(String peopleUserId) {
        this.peopleUserId = peopleUserId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbPeopleRole{" +
        ", id=" + id +
        ", peopleUserType=" + peopleUserType +
        ", peopleUserId=" + peopleUserId +
        ", roleId=" + roleId +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
