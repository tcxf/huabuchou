package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
@TableName("tb_repay")
public class TbRepay extends Model<TbRepay> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 还款-支付状态 默认为0:待支付  1:已支付
     */
    private String status;
    /**
     * 生成还款支付单号
     */
    @TableField("repay_sn")
    private String repaySn;
    /**
     * 已支付返回支付单号
     */
    @TableField("payment_sn")
    private String paymentSn;
    /**
     * 还款-支付金额
     */
    @TableField("totalMoney")
    private BigDecimal totalMoney;
    /**
     * 支付时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("pay_date")
    private Date payDate;
    /**
     * 用户id
     */
    private String uid;

    /**
     * 运营商id
     */
    private String oid;


    /**
     * 资金端id
     */
    private String fid;

    /**
     * 当存在未分期还款是的还款表主键id
     */
    private String rdid;

    /**
     * 当存在未分期还款是的还款计划表中的serialNo
     */
    @TableField("serial_no")
    private String serialNo;

    /**
     *'还款类型 1-账单还款 2-提前还款'
     */
    @TableField("pay_type")
    private String payType;

    /**
     *'还款-支付类型 网银支付 默认为1'
     */
    @TableField("payment_type")
    private String paymentType;

    /**
     *创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRdid() {
        return rdid;
    }

    public void setRdid(String rdid) {
        this.rdid = rdid;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRepaySn() {
        return repaySn;
    }

    public void setRepaySn(String repaySn) {
        this.repaySn = repaySn;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepay{" +
        ", id=" + id +
        ", status=" + status +
        ", repaySn=" + repaySn +
        ", paymentSn=" + paymentSn +
        ", totalMoney=" + totalMoney +
        ", payDate=" + payDate +
        ", uid=" + uid +
        "}";
    }
}
