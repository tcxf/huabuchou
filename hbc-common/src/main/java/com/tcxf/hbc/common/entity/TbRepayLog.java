package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 还款记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_repay_log")
public class TbRepayLog extends Model<TbRepayLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 还款-支付类型
     */
    @TableField("payment_type")
    private String paymentType;
    /**
     * 还款-支付金额
     */
    private BigDecimal amount;
    /**
     * 还款-支付状态
     */
    private String status;
    private String uid;
    /**
     * 用户类型：1-消费者，2-商户
     */
    private String utype;
    /**
     * 还款-成功支付时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("pay_date")
    private Date payDate;
    /**
     * 还款-支付编号(对应还款计划表中的流水号)
     */
    @TableField("repay_sn")
    private String repaySn;
    /**
     * 还款-账单年月
     */
    @TableField("bill_date")
    private String billDate;
    /**
     * 如果是提前还款，此处存储所有的交易明细id
     */
    private String tids;
    /**
     * 还款类型 1-账单还款 2-提前还款
     */
    private String type;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;

    /**
     *分期planId( repaymentPlan表 主键id)
     */
    @TableField("planId")
    private String planId;

    /**
     *repayId(tb_repay表 主键id)
     */
    @TableField("repay_id")
    private String repayId;

    /**
     *运营商id
     */
    private String oid;

    /**
     *资金端id
     */
    private String fid;

    /**
     *还款计划表中的流水号(误删) 无需映射xml
     */
    @TableField(exist = false)
    private String serialNo;//还款计划表中的流水号 (误删)

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getRepayId() {
        return repayId;
    }
    public void setRepayId(String repayId) {
        this.repayId = repayId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getRepaySn() {
        return repaySn;
    }

    public void setRepaySn(String repaySn) {
        this.repaySn = repaySn;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getTids() {
        return tids;
    }

    public void setTids(String tids) {
        this.tids = tids;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepayLog{" +
                "id='" + id + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                ", uid='" + uid + '\'' +
                ", utype='" + utype + '\'' +
                ", payDate=" + payDate +
                ", repaySn='" + repaySn + '\'' +
                ", billDate='" + billDate + '\'' +
                ", tids='" + tids + '\'' +
                ", type='" + type + '\'' +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", planId='" + planId + '\'' +
                '}';
    }
}
