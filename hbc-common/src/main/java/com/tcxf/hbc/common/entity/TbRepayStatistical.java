package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 还款统计表
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
@TableName("tb_repay_statistical")
public class TbRepayStatistical extends Model<TbRepayStatistical> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 应还人数
     */
    @TableField("should_pay_people")
    private String shouldPayPeople;
    /**
     * 已还人数
     */
    @TableField("already_pay_people")
    private String alreadyPayPeople;
    /**
     * 按时还款率
     */
    @TableField("pay_rate")
    private String payRate;
    /**
     * 回款率
     */
    @TableField("repay_rate")
    private String repayRate;
    /**
     * 应还金额
     */
    @TableField("should_pay_money")
    private BigDecimal shouldPayMoney;
    /**
     * 实际回款金额
     */
    @TableField("actual_repay_money")
    private BigDecimal actualRepayMoney;
    /**
     * 中期异常人数
     */
    @TableField("unusual_number")
    private String unusualNumber;
    /**
     * 还款日期
     */
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" )
    @TableField("create_time")
    private Date createTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getShouldPayPeople() {
        return shouldPayPeople;
    }

    public void setShouldPayPeople(String shouldPayPeople) {
        this.shouldPayPeople = shouldPayPeople;
    }

    public String getAlreadyPayPeople() {
        return alreadyPayPeople;
    }

    public void setAlreadyPayPeople(String alreadyPayPeople) {
        this.alreadyPayPeople = alreadyPayPeople;
    }

    public String getPayRate() {
        return payRate;
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    public String getRepayRate() {
        return repayRate;
    }

    public void setRepayRate(String repayRate) {
        this.repayRate = repayRate;
    }

    public BigDecimal getShouldPayMoney() {
        return shouldPayMoney;
    }

    public void setShouldPayMoney(BigDecimal shouldPayMoney) {
        this.shouldPayMoney = shouldPayMoney;
    }

    public BigDecimal getActualRepayMoney() {
        return actualRepayMoney;
    }

    public void setActualRepayMoney(BigDecimal actualRepayMoney) {
        this.actualRepayMoney = actualRepayMoney;
    }

    public String getUnusualNumber() {
        return unusualNumber;
    }

    public void setUnusualNumber(String unusualNumber) {
        this.unusualNumber = unusualNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepayStatistical{" +
        ", id=" + id +
        ", oid=" + oid +
        ", fid=" + fid +
        ", shouldPayPeople=" + shouldPayPeople +
        ", alreadyPayPeople=" + alreadyPayPeople +
        ", payRate=" + payRate +
        ", repayRate=" + repayRate +
        ", shouldPayMoney=" + shouldPayMoney +
        ", actualRepayMoney=" + actualRepayMoney +
        ", unusualNumber=" + unusualNumber +
        ", createTime=" + createTime +
        "}";
    }
}
