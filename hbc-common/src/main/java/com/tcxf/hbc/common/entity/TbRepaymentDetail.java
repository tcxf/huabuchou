package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 还款表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_repayment_detail")
public class TbRepaymentDetail extends Model<TbRepaymentDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户类型：1-消费者，2-商户
     */
    private String utype;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 还款总期次
     */
    @TableField("total_times")
    private Integer totalTimes;
    /**
     * 当前还款期次
     */
    @TableField("current_times")
    private Integer currentTimes;
    /**
     * 还款总本金
     */
    @TableField("total_corpus")
    private BigDecimal totalCorpus;
    /**
     * 还款总利息
     */
    @TableField("total_fee")
    private BigDecimal totalFee;
    /**
     * 滞纳金总额（产生滞纳金后累计到此处）
     */
    @TableField("total_late_fee")
    private BigDecimal totalLateFee;
    /**
     * 还款详情流水号
     */
    @TableField("repayment_sn")
    private String repaymentSn;
    /**
     * 还款状态 0-待还 1-已还
     */
    private String status;
    /**
     * 已还本金（如果分期则累计）
     */
    @TableField("repay_corpus")
    private BigDecimal repayCorpus;
    /**
     * 已还利息（如果分期则累计）
     */
    @TableField("repay_fee")
    private BigDecimal repayFee;
    /**
     *  已还滞纳金（如果分期则累计）
     */
    @TableField("repay_late_fee")
    private BigDecimal repayLateFee;
    /**
     * 出账日（运营商指定的出账日）
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("out_date")
    private String outDate;
    /**
     * 还款日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("repay_date")
    private Date repayDate;
    /**
     * 账期开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("loop_start")
    private Date loopStart;
    /**
     * 账期结束时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("loop_end")
    private Date loopEnd;
    /**
     * 月利率（根据月利率算利息）
     */
    private BigDecimal rate;
    /**
     * 是否分期（当用收到账单后，选择是否分期，绝对此字段）
     */
    @TableField("is_split")
    private String isSplit;
    /**
     * 分期手续费利率
     */
    @TableField("service_fee_rate")
    private Integer serviceFeeRate;
    /**
     * 分期手续费
     */
    @TableField("service_fee")
    private BigDecimal serviceFee;
    /**
     * 操作分期时候的ip地址
     */
    private String ip;
    /**
     * 分期日期
     */
    @TableField("split_date")
    private Date splitDate;
    /**
     * 在账单中显示的时间
     */
    @TableField("show_date")
    private Date showDate;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Integer getTotalTimes() {
        return totalTimes;
    }

    public void setTotalTimes(Integer totalTimes) {
        this.totalTimes = totalTimes;
    }

    public Integer getCurrentTimes() {
        return currentTimes;
    }

    public void setCurrentTimes(Integer currentTimes) {
        this.currentTimes = currentTimes;
    }

    public String getRepaymentSn() {
        return repaymentSn;
    }

    public void setRepaymentSn(String repaymentSn) {
        this.repaymentSn = repaymentSn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public Date getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(Date repayDate) {
        this.repayDate = repayDate;
    }

    public Date getLoopStart() {
        return loopStart;
    }

    public void setLoopStart(Date loopStart) {
        this.loopStart = loopStart;
    }

    public Date getLoopEnd() {
        return loopEnd;
    }

    public void setLoopEnd(Date loopEnd) {
        this.loopEnd = loopEnd;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getIsSplit() {
        return isSplit;
    }

    public void setIsSplit(String isSplit) {
        this.isSplit = isSplit;
    }

    public Integer getServiceFeeRate() {
        return serviceFeeRate;
    }

    public void setServiceFeeRate(Integer serviceFeeRate) {
        this.serviceFeeRate = serviceFeeRate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getSplitDate() {
        return splitDate;
    }

    public void setSplitDate(Date splitDate) {
        this.splitDate = splitDate;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public BigDecimal getTotalCorpus() {
        return totalCorpus;
    }

    public void setTotalCorpus(BigDecimal totalCorpus) {
        this.totalCorpus = totalCorpus;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getTotalLateFee() {
        return totalLateFee;
    }

    public void setTotalLateFee(BigDecimal totalLateFee) {
        this.totalLateFee = totalLateFee;
    }

    public BigDecimal getRepayCorpus() {
        return repayCorpus;
    }

    public void setRepayCorpus(BigDecimal repayCorpus) {
        this.repayCorpus = repayCorpus;
    }

    public BigDecimal getRepayFee() {
        return repayFee;
    }

    public void setRepayFee(BigDecimal repayFee) {
        this.repayFee = repayFee;
    }

    public BigDecimal getRepayLateFee() {
        return repayLateFee;
    }

    public void setRepayLateFee(BigDecimal repayLateFee) {
        this.repayLateFee = repayLateFee;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepaymentDetail{" +
        ", id=" + id +
        ", mid=" + mid +
        ", oid=" + oid +
        ", uid=" + uid +
        ", utype=" + utype +
        ", fid=" + fid +
        ", totalTimes=" + totalTimes +
        ", currentTimes=" + currentTimes +
        ", totalCorpus=" + totalCorpus +
        ", totalFee=" + totalFee +
        ", totalLateFee=" + totalLateFee +
        ", repaymentSn=" + repaymentSn +
        ", status=" + status +
        ", repayCorpus=" + repayCorpus +
        ", repayFee=" + repayFee +
        ", repayLateFee=" + repayLateFee +
        ", outDate=" + outDate +
        ", repayDate=" + repayDate +
        ", loopStart=" + loopStart +
        ", loopEnd=" + loopEnd +
        ", rate=" + rate +
        ", isSplit=" + isSplit +
        ", serviceFeeRate=" + serviceFeeRate +
        ", serviceFee=" + serviceFee +
        ", ip=" + ip +
        ", splitDate=" + splitDate +
        ", showDate=" + showDate +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
