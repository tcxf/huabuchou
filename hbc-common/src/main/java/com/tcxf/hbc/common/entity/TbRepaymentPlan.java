package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 还款计划表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_repayment_plan")
public class TbRepaymentPlan extends Model<TbRepaymentPlan> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户类型：1-消费者，2-商户
     */
    private String utype;
    /**
     * 还款总期次
     */
    @TableField("total_times")
    private Integer totalTimes;
    /**
     * 当前还款期次
     */
    @TableField("current_times")
    private Integer currentTimes;
    /**
     * 还款总本金
     */
    @TableField("total_corpus")
    private BigDecimal totalCorpus;
    /**
     * 还款总利息
     */
    @TableField("total_fee")
    private BigDecimal totalFee;
    /**
     * 剩余还款本金
     */
    @TableField("rest_corpus")
    private BigDecimal restCorpus;
    /**
     * 剩余还款利息
     */
    @TableField("rest_fee")
    private BigDecimal restFee;
    /**
     * 当期还款本金
     */
    @TableField("current_corpus")
    private BigDecimal currentCorpus;
    /**
     * 当期还款利息
     */
    @TableField("current_fee")
    private BigDecimal currentFee;
    /**
     * 当期逾期滞纳金  每日逾期滞纳金 = （当期还款本金+当期还款利息+逾期滞纳金）* 滞纳金费率
     */
    @TableField("late_fee")
    private BigDecimal lateFee;
    /**
     * 还款日
     */
    @DateTimeFormat(pattern="yyyy-MM-ddHH:mm:ss")
    @JsonFormat(pattern ="yyyy-MM-ddHH:mm:ss" )
    @TableField("repayment_date")
    private Date repaymentDate;
    /**
     * 用户实际进行还款的日期
     */
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" )
    @TableField("process_date")
    private Date processDate;
    /**
     * 还款详情id
     */
    @TableField("rd_id")
    private String rdId;
    /**
     * 当前分期还款流水号（r+还款日期（8位）+2位运营商码+4位随机码+主机编号（4位） 长度 19位）
     */
    @TableField("serial_no")
    private String serialNo;
    /**
     * 状态 1-已还 2-未还
     */
    private String status;
    /**
     * 还款方式 1-网银 2-微信 3-支付宝
     */
    @TableField("repayment_type")
    private String repaymentType;
    /**
     * 逾期利率
     */
    @TableField("yq_rate")
    private BigDecimal yqRate;
    @TableField("loop_start")
    private Date loopStart;
    @TableField("loop_end")
    private Date loopEnd;
    /**
     * 分期纪录，在账单显示的日期
     */
    @TableField("show_date")
    private Date showDate;
    /**
     * 催收狀態 -1 不催收 0-短信催收 1-电话催收 2-上门催收
     */
    @TableField("call_repay_status")
    private String callRepayStatus;
    /**
     * 纪录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 纪录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public Integer getTotalTimes() {
        return totalTimes;
    }

    public void setTotalTimes(Integer totalTimes) {
        this.totalTimes = totalTimes;
    }

    public Integer getCurrentTimes() {
        return currentTimes;
    }

    public void setCurrentTimes(Integer currentTimes) {
        this.currentTimes = currentTimes;
    }

    public BigDecimal getTotalCorpus() {
        return totalCorpus;
    }

    public void setTotalCorpus(BigDecimal totalCorpus) {
        this.totalCorpus = totalCorpus;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getRestCorpus() {
        return restCorpus;
    }

    public void setRestCorpus(BigDecimal restCorpus) {
        this.restCorpus = restCorpus;
    }

    public BigDecimal getRestFee() {
        return restFee;
    }

    public void setRestFee(BigDecimal restFee) {
        this.restFee = restFee;
    }

    public BigDecimal getCurrentCorpus() {
        return currentCorpus;
    }

    public void setCurrentCorpus(BigDecimal currentCorpus) {
        this.currentCorpus = currentCorpus;
    }

    public BigDecimal getCurrentFee() {
        return currentFee;
    }

    public void setCurrentFee(BigDecimal currentFee) {
        this.currentFee = currentFee;
    }

    public BigDecimal getLateFee() {
        return lateFee;
    }

    public void setLateFee(BigDecimal lateFee) {
        this.lateFee = lateFee;
    }

    public Date getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(Date repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getRdId() {
        return rdId;
    }

    public void setRdId(String rdId) {
        this.rdId = rdId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRepaymentType() {
        return repaymentType;
    }

    public void setRepaymentType(String repaymentType) {
        this.repaymentType = repaymentType;
    }

    public BigDecimal getYqRate() {
        return yqRate;
    }

    public void setYqRate(BigDecimal yqRate) {
        this.yqRate = yqRate;
    }

    public Date getLoopStart() {
        return loopStart;
    }

    public void setLoopStart(Date loopStart) {
        this.loopStart = loopStart;
    }

    public Date getLoopEnd() {
        return loopEnd;
    }

    public void setLoopEnd(Date loopEnd) {
        this.loopEnd = loopEnd;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public String getCallRepayStatus() {
        return callRepayStatus;
    }

    public void setCallRepayStatus(String callRepayStatus) {
        this.callRepayStatus = callRepayStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepaymentPlan{" +
        ", id=" + id +
        ", uid=" + uid +
        ", utype=" + utype +
        ", totalTimes=" + totalTimes +
        ", currentTimes=" + currentTimes +
        ", totalCorpus=" + totalCorpus +
        ", totalFee=" + totalFee +
        ", restCorpus=" + restCorpus +
        ", restFee=" + restFee +
        ", currentCorpus=" + currentCorpus +
        ", currentFee=" + currentFee +
        ", lateFee=" + lateFee +
        ", repaymentDate=" + repaymentDate +
        ", processDate=" + processDate +
        ", rdId=" + rdId +
        ", serialNo=" + serialNo +
        ", status=" + status +
        ", repaymentType=" + repaymentType +
        ", yqRate=" + yqRate +
        ", loopStart=" + loopStart +
        ", loopEnd=" + loopEnd +
        ", showDate=" + showDate +
        ", callRepayStatus=" + callRepayStatus +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
