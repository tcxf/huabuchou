package com.tcxf.hbc.common.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 还款比率字典表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_repayment_ratio_dict")
public class TbRepaymentRatioDict extends Model<TbRepaymentRatioDict> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营端id
     */
    private String oid;
    /**
     * 资金端id
     */
    private String fid;
    /**
     * 逾期还款费率（百分比）
     */
    @TableField("yq_rate")
    private BigDecimal yqRate;
    /**
     * 手续费利率（分期才有手续费）
     */
    @TableField("service_fee")
    private BigDecimal serviceFee;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 记录修改时间
     */
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public BigDecimal getYqRate() {
        return yqRate;
    }

    public void setYqRate(BigDecimal yqRate) {
        this.yqRate = yqRate;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRepaymentRatioDict{" +
        ", id=" + id +
        ", oid=" + oid +
        ", fid=" + fid +
        ", yqRate=" + yqRate +
        ", serviceFee=" + serviceFee +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
