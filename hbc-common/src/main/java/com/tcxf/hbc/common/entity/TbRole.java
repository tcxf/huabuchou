package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 后台用户角色表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_role")
public class TbRole extends Model<TbRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户类型1：商户，2：运营商，3：平台
     */
    private String utype;
    /**
     * 角色名
     */
    @TableField("role_name")
    private String roleName;
    /**
     * 备注
     */
    private String remark;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 默认管理员id
     */
    @TableField("uid")
    private  String uid;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbRole{" +
        ", id=" + id +
        ", utype=" + utype +
        ", roleName=" + roleName +
        ", remark=" + remark +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
