package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户积分明细表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_score_log")
public class TbScoreLog extends Model<TbScoreLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 业务类型
     */
    @TableField("biz_type")
    private String bizType;
    /**
     * 收支类型 0-支付 1-收入
     */
    private String direction;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 收支类型 0-支付 1-收入
     */
    private String descinfo;
    /**
     * 获得积分
     */
    private Integer score;

    @TableField("modify_date")
    private Date modifyDate;

    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" , locale = "zh" , timezone="GMT+8" )
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDescinfo() {
        return descinfo;
    }

    public void setDescinfo(String descinfo) {
        this.descinfo = descinfo;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbScoreLog{" +
        ", id=" + id +
        ", oid=" + oid +
        ", bizType=" + bizType +
        ", direction=" + direction +
        ", uid=" + uid +
        ", descinfo=" + descinfo +
        ", score=" + score +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
