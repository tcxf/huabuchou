package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *  交易结算表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_settlement_detail")
public class TbSettlementDetail extends Model<TbSettlementDetail> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private String tid;

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * 主键id
     */
    private String id;
    /**
     * 结算对应商户主键id
     */
    private String mid;
    /**
     * 结算商户对应运营商id
     */
    private String oid;
    /**
     * 资金方id
     */
    private String fid;
    /**
     * 结算时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("settlement_time")
    private Date settlementTime;
    /**
     * 结算状态 （0-待确认 1-待结算 2-已结算）
     */
    private String status;
    /**
     * 实际结算总金额
     */
    private BigDecimal amount;
    /**
     * 运营商收取商户让利总费用+三级分销费用（根据分润表统计）
     */
    @TableField("share_fee")
    private BigDecimal shareFee;
    /**
     * 结算单号即流水号
     */
    @TableField("serial_no")
    private String serialNo;
    /**
     * 结算单出账时间(对应商户的清算日期)
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("out_settlement_date")
    private Date outSettlementDate;
    /**
     * 结算的交易记录开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("loop_start")
    private Date loopStart;
    /**
     * 结算的交易记录结束时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("loop_end")
    private Date loopEnd;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("role_stime")
    private Date roleStime;
    /**
     * 平台收取运营商通道总费用（根据分润表统计）
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("platform_share_fee")
    private BigDecimal platformShareFee;
    /**
     * 运营商抽取的折扣总费用（根据分润表统计）
     */
    @TableField("discount_fee")
    private BigDecimal discountFee;
    /**
     * 运营商结算状态 （0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）
     */
    @TableField("oper_status")
    private String operStatus;
    /**
     * 平台结算状态 （0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）
     */
    @TableField("plat_status")
    private String platStatus;
    /**
     * 记录创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 运营商收取商户通道费用
     */
    private BigDecimal operaShareFee;


    /**
     * 是否已还款
     * @return
     */
    @TableField("is_pay")
    private String isPay;

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getSettlementTime() {
        return settlementTime;
    }

    public void setSettlementTime(Date settlementTime) {
        this.settlementTime = settlementTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Date getOutSettlementDate() {
        return outSettlementDate;
    }

    public void setOutSettlementDate(Date outSettlementDate) {
        this.outSettlementDate = outSettlementDate;
    }

    public Date getLoopStart() {
        return loopStart;
    }

    public void setLoopStart(Date loopStart) {
        this.loopStart = loopStart;
    }

    public Date getLoopEnd() {
        return loopEnd;
    }

    public void setLoopEnd(Date loopEnd) {
        this.loopEnd = loopEnd;
    }

    public Date getRoleStime() {
        return roleStime;
    }

    public void setRoleStime(Date roleStime) {
        this.roleStime = roleStime;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public String getPlatStatus() {
        return platStatus;
    }

    public void setPlatStatus(String platStatus) {
        this.platStatus = platStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public BigDecimal getPlatformShareFee() {
        return platformShareFee;
    }

    public void setPlatformShareFee(BigDecimal platformShareFee) {
        this.platformShareFee = platformShareFee;
    }

    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    public BigDecimal getOperaShareFee() {
        return operaShareFee;
    }

    public void setOperaShareFee(BigDecimal operaShareFee) {
        this.operaShareFee = operaShareFee;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbSettlementDetail{" +
        ", id=" + id +
        ", mid=" + mid +
        ", oid=" + oid +
        ", fid=" + fid +
        ", settlementTime=" + settlementTime +
        ", status=" + status +
        ", amount=" + amount +
        ", shareFee=" + shareFee +
        ", serialNo=" + serialNo +
        ", outSettlementDate=" + outSettlementDate +
        ", loopStart=" + loopStart +
        ", loopEnd=" + loopEnd +
        ", roleStime=" + roleStime +
        ", platformShareFee=" + platformShareFee +
        ", discountFee=" + discountFee +
        ", operStatus=" + operStatus +
        ", platStatus=" + platStatus +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
