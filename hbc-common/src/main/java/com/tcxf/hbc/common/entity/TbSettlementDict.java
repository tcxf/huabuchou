package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 结算方式比例字典表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_settlement_dict")
public class TbSettlementDict extends Model<TbSettlementDict> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 结算关系 1：商户对运营商，2：运营商对资金端，3：运营商对平台'
     */
    @TableField("settlement_ref")
    private String settlementRef;
    /**
     * 结算周期（1-日 2-周 3-月 4-季 5-半年 6-一年）
     */
    @TableField("settlement_cycle")
    private String settlementCycle;
    /**
     * 结算方式 0- T+1 , 1 - T+0
     */
    @TableField("settlement_type")
    private String settlementType;
    /**
     * 运营商抽取商户服务费率（百分之）
     */
    @TableField("share_ratio")
    private Integer shareRatio;
    /**
     * 平台抽取运营商服务费率（百分之）
     */
    @TableField("platfrom_share_ratio")
    private Integer platfromShareRatio;
    /**
     * 描述
     */
    private String direction;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSettlementRef() {
        return settlementRef;
    }

    public void setSettlementRef(String settlementRef) {
        this.settlementRef = settlementRef;
    }

    public String getSettlementCycle() {
        return settlementCycle;
    }

    public void setSettlementCycle(String settlementCycle) {
        this.settlementCycle = settlementCycle;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public Integer getShareRatio() {
        return shareRatio;
    }

    public void setShareRatio(Integer shareRatio) {
        this.shareRatio = shareRatio;
    }

    public Integer getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(Integer platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbSettlementDict{" +
        ", id=" + id +
        ", oid=" + oid +
        ", settlementRef=" + settlementRef +
        ", settlementCycle=" + settlementCycle +
        ", settlementType=" + settlementType +
        ", shareRatio=" + shareRatio +
        ", platfromShareRatio=" + platfromShareRatio +
        ", direction=" + direction +
        ", modifyDate=" + modifyDate +
        ", createDate=" + createDate +
        "}";
    }
}
