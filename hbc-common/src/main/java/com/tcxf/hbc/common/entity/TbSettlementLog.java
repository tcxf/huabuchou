package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *  结算支付记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_settlement_log")
public class TbSettlementLog extends Model<TbSettlementLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 结算记录-支付类型
     */
    @TableField("payment_type")
    private Long paymentType;
    /**
     * 结算记录-支付编号
     */
    @TableField("settlement_sn")
    private String settlementSn;
    /**
     * 结算记录-运营商id
     */
    private String oid;
    /**
     * 结算记录-结算金额
     */
    private BigDecimal amount;
    /**
     * 结算记录-支付状态
     */
    private String status;
    /**
     *  结算记录-成功支付时间
     */
    @TableField("pay_date")
    private Date payDate;
    /**
     * 结算记录-结算单id
     */
    private String sid;
    /**
     * 结算记录-支付编号
     */
    @TableField("repay_sn")
    private String repaySn;
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    public String getSettlementSn() {
        return settlementSn;
    }

    public void setSettlementSn(String settlementSn) {
        this.settlementSn = settlementSn;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getRepaySn() {
        return repaySn;
    }

    public void setRepaySn(String repaySn) {
        this.repaySn = repaySn;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbSettlementLog{" +
        ", id=" + id +
        ", paymentType=" + paymentType +
        ", settlementSn=" + settlementSn +
        ", oid=" + oid +
        ", amount=" + amount +
        ", status=" + status +
        ", payDate=" + payDate +
        ", sid=" + sid +
        ", repaySn=" + repaySn +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
