package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 三级分销利润分配表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_three_sale_profit_share")
public class TbThreeSaleProfitShare extends Model<TbThreeSaleProfitShare> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 直接上级userId
     */
    @TableField("redirect_user_id")
    private String redirectUserId;
    /**
     * 直接上级userType
     */
    @TableField("redirect_user_type")
    private String redirectUserType;
    /**
     * 直接上级利润分配金额
     */
    @TableField("redirect_share_amount")
    private BigDecimal redirectShareAmount;
    /**
     * 间接上级userid
     */
    @TableField("inderect_user_id")
    private String inderectUserId;
    /**
     * 间接上级userType
     */
    @TableField("inderect_user_type")
    private String inderectUserType;
    /**
     * 间接上级利润分配金额
     */
    @TableField("inderect_share_amount")
    private BigDecimal inderectShareAmount;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 运营商抽取三级分销利润金额
     */
    @TableField("opa_share_amount")
    private BigDecimal opaShareAmount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRedirectUserType() {
        return redirectUserType;
    }

    public void setRedirectUserType(String redirectUserType) {
        this.redirectUserType = redirectUserType;
    }
    public String getInderectUserType() {
        return inderectUserType;
    }

    public void setInderectUserType(String inderectUserType) {
        this.inderectUserType = inderectUserType;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public BigDecimal getRedirectShareAmount() {
        return redirectShareAmount;
    }

    public void setRedirectShareAmount(BigDecimal redirectShareAmount) {
        this.redirectShareAmount = redirectShareAmount;
    }

    public BigDecimal getInderectShareAmount() {
        return inderectShareAmount;
    }

    public void setInderectShareAmount(BigDecimal inderectShareAmount) {
        this.inderectShareAmount = inderectShareAmount;
    }

    public BigDecimal getOpaShareAmount() {
        return opaShareAmount;
    }

    public void setOpaShareAmount(BigDecimal opaShareAmount) {
        this.opaShareAmount = opaShareAmount;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getRedirectUserId() {
        return redirectUserId;
    }

    public void setRedirectUserId(String redirectUserId) {
        this.redirectUserId = redirectUserId;
    }

    public String getInderectUserId() {
        return inderectUserId;
    }

    public void setInderectUserId(String inderectUserId) {
        this.inderectUserId = inderectUserId;
    }

    @Override
    public String toString() {
        return "TbThreeSaleProfitShare{" +
        ", id=" + id +
        ", redirectUserId=" + redirectUserId +
        ", redirectUserType=" + redirectUserType +
        ", redirectShareAmount=" + redirectShareAmount +
        ", inderectUserId=" + inderectUserId +
        ", inderectUserType=" + inderectUserType +
        ", inderectShareAmount=" + inderectShareAmount +
        ", oid=" + oid +
        ", opaShareAmount=" + opaShareAmount +
        "}";
    }
}
