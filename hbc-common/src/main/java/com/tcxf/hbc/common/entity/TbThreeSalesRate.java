package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 三级分销利润分配比例表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_three_sales_rate")
public class TbThreeSalesRate extends Model<TbThreeSalesRate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 运营商ID
     */
    private String oid;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 直接上级分润比率
     */
    @TableField("redirect_rate")
    private Integer redirectRate;
    /**
     * 间接上级分润比率
     */
    @TableField("indirect_rate")
    private Integer indirectRate;
    /**
     * 运营商抽取三级分销分润比例（redirect_rate、indirect_rate、opa_rate三者之和为100%）
     */
    @TableField("opa_rate")
    private Integer opaRate;
    /**
     * 创建时间
     */
    @TableField("CREATE_DATE")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("MODIFY_DATE")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Integer getRedirectRate() {
        return redirectRate;
    }

    public void setRedirectRate(Integer redirectRate) {
        this.redirectRate = redirectRate;
    }

    public Integer getIndirectRate() {
        return indirectRate;
    }

    public void setIndirectRate(Integer indirectRate) {
        this.indirectRate = indirectRate;
    }

    public Integer getOpaRate() {
        return opaRate;
    }

    public void setOpaRate(Integer opaRate) {
        this.opaRate = opaRate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbThreeSalesRate{" +
        ", id=" + id +
        ", oid=" + oid +
        ", mid=" + mid +
        ", redirectRate=" + redirectRate +
        ", indirectRate=" + indirectRate +
        ", opaRate=" + opaRate +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
