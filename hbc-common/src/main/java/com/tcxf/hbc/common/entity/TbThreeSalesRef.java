package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 三级分销用户关系绑定表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_three_sales_ref")
public class TbThreeSalesRef extends Model<TbThreeSalesRef> {

    private static final long serialVersionUID = 1L;
    /**
     * 用户类型为商户
     */
    public static final String USERTYPE_MERCHANT = "1";
    /**
     * 用户类型为消费者
     */
    public static final String USERTYPE_USER = "2";

    /**
     * 上级为商户
     */
    public static final String PARENT_USERTYPE_MERCHANT = "1";
    /**
     * 上级为商户用户 或者 用户
     */
    public static final String PARENT_USERTYPE_USER = "0";


    /**
     * 主键id
     */
    private String id;
    /**
     * 三级用户id
     */
    @TableField("user_id")
    private String userId;
    /**
     *  用户类型（1-商户，2-消费者）
     */
    @TableField("user_type")
    private String userType;

    /**
     *  上级用户类型（0 (用户,商户用户 对应的c_user_info表），1为商户（对应的 m_merchant_info表））
     */
    @TableField("parent_user_type")
    private String parentUserType;
    /**
     * 上级用户id
     */
    @TableField("parent_user_id")
    private String parentUserId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(String parentUserId) {
        this.parentUserId = parentUserId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getParentUserType() {
        return parentUserType;
    }

    public void setParentUserType(String parentUserType) {
        this.parentUserType = parentUserType;
    }

    @Override
    public String toString() {
        return "TbThreeSalesRef{" +
        ", id=" + id +
        ", userId=" + userId +
        ", userType=" + userType +
        ", parentUserId=" + parentUserId +
        "}";
    }
}
