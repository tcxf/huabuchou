package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 交易利润分配表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_trade_profit_share")
public class TbTradeProfitShare extends Model<TbTradeProfitShare> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 消费交易id
     */
    @TableField("trade_id")
    private String tradeId;
    /**
     * 交易类型（0-授信消费 1-非授信消费）
     */
    @TableField("trade_type")
    private String tradeType;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 交易总金额
     */
    @TableField("total_trade_amount")
    private BigDecimal totalTradeAmount;
    /**
     * 交易商户入账金额
     */
    @TableField("merchant_inhome_amount")
    private BigDecimal merchantInhomeAmount;
    /**
     * 运营商交易抽成费
     */
    @TableField("operator_share_fee")
    private BigDecimal operatorShareFee;
    /**
     * 三级分销总金额
     */
    @TableField("operator_discount_fee")
    private BigDecimal operatorDiscountFee;
    /**
     * 用于三级分销的商户让利折扣比例
     */
    @TableField("operator_discount_ratio")
    private Long operatorDiscountRatio;
    /**
     * 是否使用优惠券
     */
    @TableField("is_use_discout")
    private String isUseDiscout;
    /**
     * 三级分销利润分配id
     */
    @TableField("three_sale_share_id")
    private String threeSaleShareId;
    /**
     * 平台抽成运营商交易费用（按交易总金额百分比抽成）
     */
    @TableField("platfrom_share_fee")
    private BigDecimal platfromShareFee;
    /**
     * 平台抽成运营商交易费用比率（按交易总金额百分比）
     */
    @TableField("platfrom_share_ratio")
    private BigDecimal platfromShareRatio;
    /**
     * 授信消费是否已结算 0:未结算，1：已结算
     */
    @TableField("is_settlement")
    private String isSettlement;
    /**
     * 交易记录时间
     */
    @TableField("trading_date")
    private Date tradingDate;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 结算时间
     */
    @TableField("modify_date")
    private Date modifyDate;

    /**
     * 运营商收取商户通道费用
     */
    @TableField("opera_share_fee")
    private BigDecimal operaShareFee;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public BigDecimal getTotalTradeAmount() {
        return totalTradeAmount;
    }

    public void setTotalTradeAmount(BigDecimal totalTradeAmount) {
        this.totalTradeAmount = totalTradeAmount;
    }

    public BigDecimal getMerchantInhomeAmount() {
        return merchantInhomeAmount;
    }

    public void setMerchantInhomeAmount(BigDecimal merchantInhomeAmount) {
        this.merchantInhomeAmount = merchantInhomeAmount;
    }

    public BigDecimal getOperatorShareFee() {
        return operatorShareFee;
    }

    public void setOperatorShareFee(BigDecimal operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public BigDecimal getOperatorDiscountFee() {
        return operatorDiscountFee;
    }

    public void setOperatorDiscountFee(BigDecimal operatorDiscountFee) {
        this.operatorDiscountFee = operatorDiscountFee;
    }

    public void setPlatfromShareFee(BigDecimal platfromShareFee) {
        this.platfromShareFee = platfromShareFee;
    }


    public String getIsUseDiscout() {
        return isUseDiscout;
    }

    public void setIsUseDiscout(String isUseDiscout) {
        this.isUseDiscout = isUseDiscout;
    }

    public Long getOperatorDiscountRatio() {
        return operatorDiscountRatio;
    }

    public void setOperatorDiscountRatio(Long operatorDiscountRatio) {
        this.operatorDiscountRatio = operatorDiscountRatio;
    }

    public BigDecimal getPlatfromShareFee() {
        return platfromShareFee;
    }

    public String getThreeSaleShareId() {
        return threeSaleShareId;
    }

    public void setThreeSaleShareId(String threeSaleShareId) {
        this.threeSaleShareId = threeSaleShareId;
    }

    public String getIsSettlement() {
        return isSettlement;
    }

    public void setIsSettlement(String isSettlement) {
        this.isSettlement = isSettlement;
    }

    public Date getTradingDate() {
        return tradingDate;
    }

    public void setTradingDate(Date tradingDate) {
        this.tradingDate = tradingDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    public BigDecimal getOperaShareFee() {
        return operaShareFee;
    }

    public void setOperaShareFee(BigDecimal operaShareFee) {
        this.operaShareFee = operaShareFee;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbTradeProfitShare{" +
        ", id=" + id +
        ", tradeId=" + tradeId +
        ", tradeType=" + tradeType +
        ", oid=" + oid +
        ", mid=" + mid +
        ", totalTradeAmount=" + totalTradeAmount +
        ", merchantInhomeAmount=" + merchantInhomeAmount +
        ", operatorShareFee=" + operatorShareFee +
        ", operatorDiscountFee=" + operatorDiscountFee +
        ", operatorDiscountRatio=" + operatorDiscountRatio +
        ", isUseDiscout=" + isUseDiscout +
        ", threeSaleShareId=" + threeSaleShareId +
        ", platfromShareFee=" + platfromShareFee +
        ", platfromShareRatio=" + platfromShareRatio +
        ", isSettlement=" + isSettlement +
        ", tradingDate=" + tradingDate +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
