package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 用户交易记录表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_trading_detail")
public class TbTradingDetail extends Model<TbTradingDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 消费类型 授信支付
     */
    public static final  String TRADINGTYPE_SX_YES="0";

    /**
     * 消费类型 非授信支付
     */
    public static final  String TRADINGTYPE_SX_NO="1";

    /**
     * 支付状态 未支付
     */
    public static final  String PAYMENT_STATUS_NO="0";

    /**
     * 支付状态 已支付
     */
    public static final  String PAYMENT_STATUS_YES="1";
    /**
     * 是否使用了优惠券 1 已使用
     */
    public static final  String ISUSEDISCOUT_YES="1";

    /**
     * 是否使用了优惠券 0 未使用
     */
    public static final  String ISUSEDISCOUT_NO="0";


    /**
     * 是否已结算 1 已结算
     */
    public static final  String ISSETTLEMENT_YES="1";

    /**
     * 是否已结算 0 未结算
     */
    public static final  String ISSETTLEMENT_NO="0";
    /**
     * 主键id
     */
    private String id;
    /**
     * 业务单号 (t+年月日时分秒（14位）+2位运营商码+4位随机码+主机编号（4位） 长度 25位)
     */
    @TableField("serial_no")
    private String serialNo;
    /**
     * 商户id
     */
    private String mid;
    /**
     * 运营商id
     */
    private String oid;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户类型：1-消费者，2-商户
     */
    private String utype;
    /**
     * 交易类型（0-授信消费 1-非授信消费）
     */
    @TableField("trading_type")
    private String tradingType;
    /**
     * 支付状态（0-未支付 1-已支付）
     */
    @TableField("payment_status")
    private String paymentStatus;
    /**
     * 交易原始总金额
     */
    @TableField("trade_amount")
    private BigDecimal tradeAmount;
    /**
     * 优惠金额（消费者使用优惠券）
     */
    @TableField("discount_amount")
    private BigDecimal discountAmount;
    /**
     * 实际交易总金额
     */
    @TableField("actual_amount")
    private BigDecimal actualAmount;
    /**
     * 交易时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("trading_date")
    private Date tradingDate;
    /**
     *  所属还款主键id
     */
    private String rdid;
    /**
     * 交易地点ip
     */
    private String ip;
    /**
     * 是否使用优惠券
     */
    @TableField("is_use_discout")
    private String isUseDiscout;
    /**
     * 优惠券id
     */
    @TableField("rp_id")
    private String rpId;
    /**
     * 提前还款纪录id
     */
    private String rid;
    /**
     * 授信消费是否已结算 0:未结算，1：已结算
     */
    @TableField("is_settlement")
    private String isSettlement;
    /**
     * 结算单id（生成结算单时插入此字段）
     */
    private String sid;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" )
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" )
    @TableField("modify_date")
    private Date modifyDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getTradingType() {
        return tradingType;
    }

    public void setTradingType(String tradingType) {
        this.tradingType = tradingType;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public Date getTradingDate() {
        return tradingDate;
    }

    public void setTradingDate(Date tradingDate) {
        this.tradingDate = tradingDate;
    }

    public String getRdid() {
        return rdid;
    }

    public void setRdid(String rdid) {
        this.rdid = rdid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIsUseDiscout() {
        return isUseDiscout;
    }

    public void setIsUseDiscout(String isUseDiscout) {
        this.isUseDiscout = isUseDiscout;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getIsSettlement() {
        return isSettlement;
    }

    public void setIsSettlement(String isSettlement) {
        this.isSettlement = isSettlement;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbTradingDetail{" +
        ", id=" + id +
        ", serialNo=" + serialNo +
        ", mid=" + mid +
        ", oid=" + oid +
        ", uid=" + uid +
        ", utype=" + utype +
        ", tradingType=" + tradingType +
        ", paymentStatus=" + paymentStatus +
        ", tradeAmount=" + tradeAmount +
        ", discountAmount=" + discountAmount +
        ", actualAmount=" + actualAmount +
        ", tradingDate=" + tradingDate +
        ", rdid=" + rdid +
        ", ip=" + ip +
        ", isUseDiscout=" + isUseDiscout +
        ", rpId=" + rpId +
        ", rid=" + rid +
        ", isSettlement=" + isSettlement +
        ", sid=" + sid +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
