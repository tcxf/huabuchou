package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 授信提额表
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
@TableName("tb_up_money")
public class TbUpMoney extends Model<TbUpMoney> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 父id
     */
    @TableField("parent_id")
    private String parentId;
    /**
     * 级别
     */
    private String grade;
    /**
     * 一级名称/二级信息
     */
    private String name;
    /**
     * 英文标识
     */
    private String code;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 二级信息
     */
    private String value;

    /**
     * 状态
     */
    private String status;

    /**
     * token
     */
    private String token;
    /**
     * 运营商id
     */
    private String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUpMoney{" +
        ", id=" + id +
        ", parentId=" + parentId +
        ", grade=" + grade +
        ", name=" + name +
        ", code=" + code +
        ", uid=" + uid +
        ", value=" + value +
        "}";
    }
}
