package com.tcxf.hbc.common.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户账户表（包含消费者、商户）
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_user_account")
public class TbUserAccount extends Model<TbUserAccount> {

    private static final long serialVersionUID = 1L;

    /**
     * 账户表默认用户的类型
     */
    public static final String USERTYPEID_USER = "0";

    /**
     * 账户表默认商户的类型
     */
    public static final String USERTYPEID_MERCHANT = "1";
    /**
     * 主键id
     */
    private String id;
    /**
     * 登录账号（手机号）
     */
    private String account;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;
    /**
     * 支付密码
     */
    @TableField("tran_pwd")
    private String tranPwd;
    /**
     * 用户类型id(0为用户，1为商户)
     */
    @TableField("user_type_id")
    private String userTypeId;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTranPwd() {
        return tranPwd;
    }

    public void setTranPwd(String tranPwd) {
        this.tranPwd = tranPwd;
    }

    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUserAccount{" +
        ", id=" + id +
        ", account=" + account +
        ", mobile=" + mobile +
        ", password=" + password +
        ", tranPwd=" + tranPwd +
        ", userTypeId=" + userTypeId +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
