package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户类型表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_user_account_type_con")
public class TbUserAccountTypeCon extends Model<TbUserAccountTypeCon> {

    private static final long serialVersionUID = 1L;
    /**
     * 用户类型为消费者
     */
    public static final String USERTYPE_USER = "1";
    /**
     * 用户类型为商户
     */
    public static final String USERTYPE_MERCHANT = "2";
    /**
     * 主键id
     */
    private String id;
    /**
     * 账户id
     */
    @TableField("acct_id")
    private String acctId;
    /**
     * 用户类型（1：消费者，2商户）
     */
    @TableField("user_type")
    private String userType;
    /**
     *  关联id （如果user_type=1,则为c_user_info表的id ，否则为m_merchant_info的id）
     */
    @TableField("con_id")
    private String conId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAcctId() {
        return acctId;
    }

    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getConId() {
        return conId;
    }

    public void setConId(String conId) {
        this.conId = conId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUserAccountTypeCon{" +
        ", id=" + id +
        ", acctId=" + acctId +
        ", userType=" + userType +
        ", conId=" + conId +
        "}";
    }
}
