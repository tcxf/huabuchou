package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户分润明细表
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@TableName("tb_user_sharefee_details")
public class TbUserSharefeeDetails extends Model<TbUserSharefeeDetails> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 交易id
     */
    @TableField("trade_id")
    private String tradeId;
    /**
     * 分润金额
     */
    @TableField("share_money")
    private Long shareMoney;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField("modify_time")
    private Date modifyTime;
    /**
     * 是否结算
     */
    @TableField("is_settlement")
    private String isSettlement;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Long getShareMoney() {
        return shareMoney;
    }

    public void setShareMoney(Long shareMoney) {
        this.shareMoney = shareMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getIsSettlement() {
        return isSettlement;
    }

    public void setIsSettlement(String isSettlement) {
        this.isSettlement = isSettlement;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUserSharefeeDetails{" +
                ", id=" + id +
                ", uid=" + uid +
                ", tradeId=" + tradeId +
                ", shareMoney=" + shareMoney +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                ", isSettlement=" + isSettlement +
                "}";
    }
}
