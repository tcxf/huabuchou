package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 用户分润结算表
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@TableName("tb_user_sharefee_settlement")
public class TbUserSharefeeSettlement extends Model<TbUserSharefeeSettlement> {

    private static final long serialVersionUID = 1L;

    public String getuType() {
        return uType;
    }

    public void setuType(String uType) {
        this.uType = uType;
    }

    private String uType;


    /**
     * 主键id
     */
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 结算id
     */
    @TableField("settlement_id")
    private String settlementId;
    /**
     * 结算分润总金额
     */
    @TableField("total_money")
    private BigDecimal totalMoney;

    /**
     * 是否已结算
     */
    private String isSettlement;

    public String getIsSettlement() {
        return isSettlement;
    }

    public void setIsSettlement(String isSettlement) {
        this.isSettlement = isSettlement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbUserSharefeeSettlement{" +
                ", id=" + id +
                ", uid=" + uid +
                ", settlementId=" + settlementId +
                ", totalMoney=" + totalMoney +
                "}";
    }
}
