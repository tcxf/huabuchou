package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 资金钱包表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_wallet")
public class TbWallet extends Model<TbWallet> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 用户id（如果用户是消费者或商户，则此id为tb_user_account表对应的主键id）
     */
    private String uid;
    /**
     * 用户钱包卡号
     */
    @TableField("card_num")
    private String cardNum;
    /**
     * 总金额
     */
    @TableField("total_amount")
    private BigDecimal totalAmount;
    /**
     * 用户类型1：用户，2：商户，3：运营商，4：资金端
     */
    private String utype;
    /**
     * 用户名字
     */
    private String uname;
    /**
     * 用户手机
     */
    private String umobil;
    /**
     * 用户状态
     */
    private Integer stuta;
    /**
     * 提现续费开关
     */
    @TableField("withdraw_fee_flag")
    private String withdrawFeeFlag;
    /**
     * 手续费
     */
    private BigDecimal sxje;
    /**
     * 钱包名称(用户姓名or商户名称or运营商or资金端)
     */
    private String wname;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("create_date")
    private Date createDate;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("modify_date")
    private Date modifyDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUmobil() {
        return umobil;
    }

    public void setUmobil(String umobil) {
        this.umobil = umobil;
    }

    public Integer getStuta() {
        return stuta;
    }

    public void setStuta(Integer stuta) {
        this.stuta = stuta;
    }

    public String getWithdrawFeeFlag() {
        return withdrawFeeFlag;
    }

    public void setWithdrawFeeFlag(String withdrawFeeFlag) {
        this.withdrawFeeFlag = withdrawFeeFlag;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getSxje() {
        return sxje;
    }

    public void setSxje(BigDecimal sxje) {
        this.sxje = sxje;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbWallet{" +
        ", id=" + id +
        ", uid=" + uid +
        ", cardNum=" + cardNum +
        ", totalAmount=" + totalAmount +
        ", utype=" + utype +
        ", uname=" + uname +
        ", umobil=" + umobil +
        ", stuta=" + stuta +
        ", withdrawFeeFlag=" + withdrawFeeFlag +
        ", sxje=" + sxje +
        ", wname=" + wname +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        "}";
    }
}
