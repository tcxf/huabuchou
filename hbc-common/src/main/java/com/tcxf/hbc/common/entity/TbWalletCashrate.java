package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
@TableName("tb_wallet_cashrate")
public class TbWalletCashrate extends Model<TbWalletCashrate> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 用户类型：1用户 2商户 3运营商 4资金端
     */
    private String utype;
    /**
     * 提现费率类型 1为百分比 2.元
     */
    @TableField("present_type")
    private String presentType;
    /**
     * 提现类型费率
     */
    @TableField("present_content")
    private String presentContent;
    /**
     * 提现阶梯等级
     */
    @TableField("ladder_class")
    private String ladderClass;
    /**
     * 阶梯区间值最低
     */
    @TableField("minimum_interval_value")
    private String minimumIntervalValue;
    /**
     * 阶梯区间值最高
     */
    @TableField("maximum_interval_value")
    private String maximumIntervalValue;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getPresentType() {
        return presentType;
    }

    public void setPresentType(String presentType) {
        this.presentType = presentType;
    }

    public String getPresentContent() {
        return presentContent;
    }

    public void setPresentContent(String presentContent) {
        this.presentContent = presentContent;
    }

    public String getLadderClass() {
        return ladderClass;
    }

    public void setLadderClass(String ladderClass) {
        this.ladderClass = ladderClass;
    }

    public String getMinimumIntervalValue() {
        return minimumIntervalValue;
    }

    public void setMinimumIntervalValue(String minimumIntervalValue) {
        this.minimumIntervalValue = minimumIntervalValue;
    }

    public String getMaximumIntervalValue() {
        return maximumIntervalValue;
    }

    public void setMaximumIntervalValue(String maximumIntervalValue) {
        this.maximumIntervalValue = maximumIntervalValue;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbWalletCashrate{" +
        ", id=" + id +
        ", utype=" + utype +
        ", presentType=" + presentType +
        ", presentContent=" + presentContent +
        ", ladderClass=" + ladderClass +
        ", minimumIntervalValue=" + minimumIntervalValue +
        ", maximumIntervalValue=" + maximumIntervalValue +
        "}";
    }
}
