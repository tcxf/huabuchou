package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 资金钱包收支明细表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_wallet_income")
public class TbWalletIncome extends Model<TbWalletIncome> {

    private static final long serialVersionUID = 1L;
    public static  final String TYPE_1="1";
    public static  final String TYPE_2="2";
    public static  final String TYPE_3="3";
    public static  final String TYPE_4="4";
    public static  final String TYPE_5="5";
    public static  final String TYPE_6="6";
    public static  final String TYPE_7="7";
    //运营商扣除平台抽成费
    public static  final String TYPE_8="8";


    private String id;
    /**
     * 钱包id（收入、支出）
     */
    private String wid;
    /**
     * 资金来源用户id（收入、支出）
     */
    private String playid;
    /**
     * 用户类型1：消费者用户，2：商户，3：运营商，4：资金端
     */
    private String ptepy;
    /**
     * 收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出
     */
    private String type;
    /**
     * 用户名字
     */
    private String pname;
    /**
     * 用户手机
     */
    private String pmobile;
    /**
     * 交易总金额
     */
    @TableField("trade_total_amount")
    private BigDecimal tradeTotalAmount;
    /**
     * 出、入金总额
     */
    private BigDecimal amount;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    @TableField("create_date")
    private Date createDate;
    @TableField("modify_date")
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    private Date modifyDate;
    /**
     * 交易记录id
     */
    private String tid;

    private String settlementId;
    private String oid;
    @TableField("settlement_status")
    private String  settlementStatus;

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }


    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWid() {
        return wid;
    }

    public void setWid(String wid) {
        this.wid = wid;
    }

    public String getPlayid() {
        return playid;
    }

    public void setPlayid(String playid) {
        this.playid = playid;
    }

    public String getPtepy() {
        return ptepy;
    }

    public void setPtepy(String ptepy) {
        this.ptepy = ptepy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPmobile() {
        return pmobile;
    }

    public void setPmobile(String pmobile) {
        this.pmobile = pmobile;
    }

    public BigDecimal getTradeTotalAmount() {
        return tradeTotalAmount;
    }

    public void setTradeTotalAmount(BigDecimal tradeTotalAmount) {
        this.tradeTotalAmount = tradeTotalAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbWalletIncome{" +
        ", id=" + id +
        ", wid=" + wid +
        ", playid=" + playid +
        ", ptepy=" + ptepy +
        ", type=" + type +
        ", pname=" + pname +
        ", pmobile=" + pmobile +
        ", tradeTotalAmount=" + tradeTotalAmount +
        ", amount=" + amount +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", tid=" + tid +
        "}";
    }
}
