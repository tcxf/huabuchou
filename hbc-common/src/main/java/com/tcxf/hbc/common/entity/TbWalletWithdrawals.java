package com.tcxf.hbc.common.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 资金钱包提现表
 * </p>
 *
 * @author lengleng
 * @since 2018-06-14
 */
@TableName("tb_wallet_withdrawals")
public class TbWalletWithdrawals extends Model<TbWalletWithdrawals> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 提现用户id
     */
    private String uid;
    /**
     * 提现用户姓名
     */
    private String uname;
    /**
     * 提现用户手机
     */
    private String umobile;
    /**
     * 提现用户开户行
     */
    private String khh;
    /**
     * 提现用户银行卡号
     */
    @TableField("bank_card_no")
    private String bankCardNo;
    /**
     * 申请时间
     */
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("apply_time")
    private Date applyTime;
    /**
     * 到账时间
     */
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("arrive_time")
    private Date arriveTime;
    /**
     * 提现金额
     */
    @TableField("tx_amount")
    private BigDecimal txAmount;
    /**
     * 到账金额
     */
    @TableField("dz_amount")
    private BigDecimal dzAmount;
    /**
     * 手续费
     */
    @TableField("service_fee")
    private BigDecimal serviceFee;
    /**
     * 钱包余额
     */
    @TableField("wallet_balance")
    private BigDecimal walletBalance;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("create_date")
    private Date createDate;
    @JsonFormat(pattern ="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8" )
    @TableField("modify_date")
    private Date modifyDate;
    /**
     * 交易状态（1审核中 2提现成功 3提现失败）
     */
    private String state;
    /**
     * 提现钱包名称
     */
    private String wname;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    public String getKhh() {
        return khh;
    }

    public void setKhh(String khh) {
        this.khh = khh;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Date getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(Date arriveTime) {
        this.arriveTime = arriveTime;
    }

    public BigDecimal getTxAmount() {
        return txAmount;
    }

    public void setTxAmount(BigDecimal txAmount) {
        this.txAmount = txAmount;
    }

    public BigDecimal getDzAmount() {
        return dzAmount;
    }

    public void setDzAmount(BigDecimal dzAmount) {
        this.dzAmount = dzAmount;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TbWalletWithdrawals{" +
        ", id=" + id +
        ", uid=" + uid +
        ", uname=" + uname +
        ", umobile=" + umobile +
        ", khh=" + khh +
        ", bankCardNo=" + bankCardNo +
        ", applyTime=" + applyTime +
        ", arriveTime=" + arriveTime +
        ", txAmount=" + txAmount +
        ", dzAmount=" + dzAmount +
        ", serviceFee=" + serviceFee +
        ", walletBalance=" + walletBalance +
        ", createDate=" + createDate +
        ", modifyDate=" + modifyDate +
        ", state=" + state +
        ", wname=" + wname +
        "}";
    }
}
