package com.tcxf.hbc.common.message;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.bean.config.MessageConfig;
import com.tcxf.hbc.common.entity.TbMessageLog;
import com.tcxf.hbc.common.entity.TbMessageTemplate;
import com.tcxf.hbc.common.util.SmsClSupport;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class MessageUtil {

    public static final String  CODE_SEND_TIME = "code_send_time";
    public static final String  CODE_SEND      = "code_send";
    public static final String YZM="YZM";

    @Autowired
    public MessageConfig messageConfig;


//    public

  /*  @Resource(name = "redisTemplate")
    private ValueOperations<String, Object> vOps;*/

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> template;


    /**
     * 获取验证码
     */
    public String Getmsmcode() {
        String code = StringUtil.RadomCode(6, "number");
        return code;
    }

    /**
     * 组装短信模板
     * @param mobile 发送的手机号
     * @param uid 接收短信的用户ID
     * @param smsTempateId 业务编码ID
     * @param args 短信内容（模板内插入的内容）
     */
    public  void sendSms(String mobile,String uid,String smsTempateId, String[] args)
    {
        TbMessageTemplate temp = new TbMessageTemplate();
        TbMessageTemplate t = temp.selectOne(new EntityWrapper().eq("utype",smsTempateId));
        String tempStr =  t.getContent() ;
        String content = MessageFormat.format(tempStr,args);
        //把短信模板放在缓存里
        Long sendTime = null;
        if ( template.opsForValue().get(CODE_SEND_TIME + mobile + smsTempateId)!= null){
            sendTime = Long.parseLong(template.opsForValue().get(CODE_SEND_TIME + mobile + smsTempateId).toString());
        }
        System.out.println(sendTime);
        Long now = new Date().getTime();
        if (sendTime != null && (now - sendTime) < 1000 * 60) {
            throw new CheckedException("请等待" + (60l - ((now - sendTime) / 1000)) + "秒后再次发送");
        }

        // 5分钟过期
        template.opsForValue().set((CODE_SEND_TIME + mobile + smsTempateId), (new Date().getTime() + ""),300,TimeUnit.SECONDS);
        template.opsForValue().set((CODE_SEND + mobile + smsTempateId), args[0],300,TimeUnit.SECONDS);
        this.sendSms(mobile,smsTempateId,content,uid);
    }

    public  void sendSms(String mobile,String smsTempateId,String content,String uid)
    {
        //把短息内容插入短信记录表
        TbMessageLog m = new TbMessageLog();
        m.setBizType(smsTempateId);
        m.setContent(content);
        m.setMobile(mobile);
        m.setUid(uid);
        m.setCreateDate(new Date());
        m.setModifyDate(new Date());
        m.setType(smsTempateId);
        m.insert();
        //调用短信通道发送
        try {
            SmsClSupport.send(mobile, m.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 短信校验
     * @param mobile  手机号
     * @param valiCode 验证码
     * @param smsTempateIde 业务编码
     * @return
     */

    public boolean valiCode(String mobile, String valiCode, String smsTempateIde)  {
        boolean result = true;
            // 用户名/验证码未填写
            if (StringUtil.isEmpty(mobile) || StringUtil.isEmpty(valiCode)) {
                return false;
            }
            if (valiCode.equals("888888")&&messageConfig.getMessageType().equals("1")) {
                return true;
            } else {

                boolean returnVal = false;
                String value =  template.opsForValue().get(CODE_SEND + mobile + smsTempateIde);
                if (StringUtil.isEmpty(value)) {
                    returnVal = false;
                } else {
                    result = value.equals(valiCode);
                    if (result) { // 校验通过，同时删除redis缓存
                        template.delete(CODE_SEND + mobile + smsTempateIde);
                        returnVal = true;
                    }
                }

                return returnVal;
            }

    }

}
