package com.tcxf.hbc.common.util;

import sun.applet.Main;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BankUtil {
    private static Map<String, String> map = new HashMap<String, String>();
    static {

        map.put("工商银行","ICBC");
        map.put("农业银行","ABC");
        map.put("中国银行","BOC");
        map.put("建设银行","CCB");
        map.put("交通银行","BOCOM");
        map.put("兴业银行","CIB");
        map.put("中信银行","CNCB");
        map.put("平安银行","PAB");
        map.put("光大银行","CEB");
        map.put("浦发银行","SPDB");
        map.put("招商银行","CMB");
        map.put("民生银行","CMBC");
        map.put("华夏银行","HXB");
        map.put("广发银行","GDB");
        map.put("邮储银行","PSBC");
        map.put("北京银行","BCCB");
        map.put("上海银行","BOS");
    }

    public static String getBanksn(String key){
        return ValidateUtil.isEmpty(map.get(key))? "" : map.get(key);
    }
}
