package com.tcxf.hbc.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculate {
	
	// 除运算的精度
	public static int SCALE_6 = 6;


	private BigDecimal amount;

	public Calculate(String amount){
		this.amount = new BigDecimal(amount);
	}	
	public Calculate(BigDecimal amount){
		this.amount = amount;
	}

	public static Calculate division_100(BigDecimal left) {
		return division(left,new BigDecimal(100));
	}

	public static Calculate multiply_100(BigDecimal bigDecimal) {
		return multiply(bigDecimal,new BigDecimal(100));
	}

	public static Calculate multiply_10000(BigDecimal bigDecimal) {
		return multiply(bigDecimal,new BigDecimal(10000));
	}

	public String toString() {
		return this.amount.toString();
	}

	public static void main(String[] args) {
		System.out.println(lessOrEquals(new BigDecimal("123"),new BigDecimal("2")));
	}

	/**
	 * 取模 返回数组 0 是商 1 是余 比如 100%8 Calculate[0]=12 Calculate[1]=4
	 * @param left
	 * @param right
	 * @return
	 */
	public static Calculate[] divideAndRemainder(BigDecimal left,BigDecimal right) {
		BigDecimal[] bigDecimals = left.divideAndRemainder(right);
		Calculate[] calculates = new Calculate[2];
		calculates[0] = new Calculate(bigDecimals[0]);
		calculates[1] = new Calculate(bigDecimals[1]);
		return calculates;
	}

	/**
	 * 格式化
	 * @param scale 小数位精度 四舍五入
	 * @return
	 */
	public BigDecimal format(int scale) {
		return format(scale, RoundingMode.HALF_UP);
	}

	/**
	 * 格式化
	 * @param scale 小数位精度
	 * @param roundingMode 小数位规则 例如 四舍五入 ,RoundingMode.DOWN向左取整
	 * @return
	 */
	public  BigDecimal format(int scale, RoundingMode roundingMode) {
		BigDecimal decimal = this.amount.setScale(scale, roundingMode);
		return decimal;
	}

	/**
	 * 加
	 * @param left
	 * @param right
	 * @return
	 */
	public static Calculate add(BigDecimal left, BigDecimal right){
		return new Calculate(left.add(right));
	}

	/**
	 * 减
	 * @param left
	 * @param right
	 * @return
	 */
	public static Calculate subtract(BigDecimal left, BigDecimal right){
		return new Calculate(left.subtract(right));
	}


	/**
	 * 乘
	 * @param left
	 * @param right
	 * @return
	 */
	public static Calculate multiply(BigDecimal left, BigDecimal right){
		return new Calculate(left.multiply(right).toString());
	}
	/**
	 * 除
	 * @param left
	 * @param right
	 * @return
	 */
	public static Calculate division(BigDecimal left, BigDecimal right){
		return Calculate.division(left, right, SCALE_6);
	}

	/**
	 * 除
	 * @param left
	 * @param right
	 * @param scale
	 * @return
	 */
	public static Calculate division(BigDecimal left, BigDecimal right, int scale){
		BigDecimal bd=left.divide(right, scale ,BigDecimal.ROUND_HALF_UP);
		return new Calculate(bd);
	}


	/**
	 * 
	 * left 大于 right 返回1
	 * 等于返回0
	 * 小于返回-1
	 * @param
	 */
	public static int compareTo(BigDecimal left, BigDecimal right){
		return left.compareTo(right);
	}

	/**
	 * left 大于等于 right
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean greaterOrEquals(BigDecimal left, BigDecimal right) {
		return greaterThan(left, right) || equals(left, right);
	}
	/**
	 * left 小于等于 right
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean lessOrEquals(BigDecimal left, BigDecimal right) {
		return lessThan(left, right) || equals(left, right);
	}

	/**
	 * 是否小于0
	 * @param left
	 * @return
	 */
	public static boolean lessThanZero(BigDecimal left) {
		return lessThan(left, new BigDecimal("0"));
	}

	/**
	 * 是否大于0
	 * @param left
	 * @return
	 */
	public static boolean greaterThanZero(BigDecimal left) {
		return greaterThan(left, new BigDecimal("0"));
	}

	/**
	 * 小于等于0
	 * @param left
	 * @return
	 */
	public static boolean lessOrEqualsZero(BigDecimal left) {
		return lessThanZero(left) || equalsZero(left);
	}

	/**
	 * 大于等于0
	 * @param left
	 * @return
	 */
	public static boolean greaterOrEqualsZero(BigDecimal left) {
		return greaterThanZero(left) || equalsZero(left);
	}

	/**
	 * 等于0
	 * @param left
	 * @return
	 */
	public static boolean equalsZero(BigDecimal left) {
		return equals(left, new BigDecimal("0"));
	}	
	
	//left 大于 right
	public static boolean greaterThan(BigDecimal left, BigDecimal right){
		return (compareTo(left, right) == 1);
	}
	
	//left 小于  right
	public static boolean lessThan(BigDecimal left, BigDecimal right){
		return (compareTo(left, right) == -1);
	}

	/**
	 * left 等于 rigth
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean equals(BigDecimal left, BigDecimal right) {
		return (compareTo(left, right) == 0);
	}

	/**
	 * left 等于 right
	 * @param left
	 * @param right
	 * @param scale
	 * @return
	 */
	public static boolean equals(BigDecimal left, BigDecimal right, int scale) {
		return (compareTo(new Calculate(left).format(scale), new Calculate(right).format(scale)) == 0);
	}

	/**
	 * 格式化
	 * @param money
	 * @param scale
	 * @return
	 */
	public String format(BigDecimal money,int scale) {
		return format(money,scale, RoundingMode.HALF_UP);
	}

	/**
	 * 重写equals方法
	 * @param other
	 * @return
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Calculate)) {
			return false;
		}
		return Calculate.equals(this.getAmount(), (BigDecimal)other);
	}




	/**
	 * 格式化
	 * @param money
	 * @param scale
	 * @param roundingMode
	 * @return
	 */
	private String format(BigDecimal money,int scale, RoundingMode roundingMode) {
		
		BigDecimal decimal = money.setScale(scale, roundingMode);
		return decimal.toString();
	}

	/**
	 * 取最大值
	 * @param a
	 * @param b
	 * @return
	 */
	static public BigDecimal max(BigDecimal a, BigDecimal b) {
		return Calculate.greaterOrEquals(a, b) ? a : b;
	}

	/**
	 * 取最小值
	 * @param a
	 * @param b
	 * @return
	 */
	static public BigDecimal min(BigDecimal a, BigDecimal b) {
		return Calculate.lessOrEquals(a, b) ? a : b;
	}

	/**
	 * 是否可以整除
	 * @param
	 * @return
	 */
	public boolean isDivisible(Calculate left, Calculate right){
		BigDecimal remainder = left.amount.remainder(right.amount); 
		return remainder.compareTo(new BigDecimal("0.00"))==0;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
}
