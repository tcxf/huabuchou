
package com.tcxf.hbc.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

/**
 * 日期工具类
 *
 * @author hKF13242
 * @version [版本号, 2009-7-6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DateUtil
{
    /**
     * 标准时间格式
     */
    public static final String FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";

    /**
     * 标准时间格式
     */
    public static final String FORMAT_STANDARD_MIN = "yyyy-MM-dd HH:mm";

    /** 格式化类型:YYYY-MM-dd */
    public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    /** 日期格式yyyy-MM字符串常量 */
    public static final String MONTH_FORMAT = "yyyy-MM";

    /** 日期格式yyyy-MM字符串常量 */
    public static final String SINGLE_MONTH_FORMAT = "MM";

    /** 日期格式yyyy字符串常量 */
    public static final String YEAR_FORMAT = "yyyy";

    /** 格式化类型:YYYYMMdd */
    public static final String FORMAT_YYYYMMDD = "yyyyMMdd";

    /** 格式化类型:yyyyMMddHHmmss */
    public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    /** 格式化类型:yyyy-MM-dd HH:mm:ss */
    public static final String FORMAT_YYYY_MM_DD_SECONDS = "yyyy-MM-dd HH:mm:ss";

    public static final String HOUR_FORMAT = "HH:mm:ss";

    private static ThreadLocal<SimpleDateFormat> dateFormatLocal = new ThreadLocal<SimpleDateFormat>();

    private static final Logger logger = Logger.getLogger(DateUtil.class);

    /** yyyyMMdd格式的 正则判断器 */
    private static final Pattern YYYYMMDD_PATTERN = Pattern.compile("[1-9]{1}[0-9]{3}-((0?[1-9])|(1[0-2]))-((0?[1-9])|([1-2][0-9])|([3][0-1]))");

    /** yyyy-MM-dd HH:mm:ss格式的 正则判断器 */
    private static final Pattern YYYYMMDDHHMMSS_PATTERN = Pattern.compile("[1-9]{1}[0-9]{3}-((0?[1-9])|(1[0-2]))-((0?[1-9])|([1-2][0-9])|([3][0-1]))\\s+(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9]):([0-5]?[0-9])");

    private static ThreadLocal<SimpleDateFormat> dateFormat = new ThreadLocal<SimpleDateFormat>();
    public static SimpleDateFormat sdf_datetime_format = new SimpleDateFormat(FORMAT_YYYY_MM_DD_SECONDS);
    public static SimpleDateFormat sdf_date_format = new SimpleDateFormat(FORMAT_YYYY_MM_DD);
    public static SimpleDateFormat sdf_hour_format = new SimpleDateFormat(HOUR_FORMAT);

    public static SimpleDateFormat sdf_month_format = new SimpleDateFormat(MONTH_FORMAT);
    public static SimpleDateFormat sdf_single_month_format = new SimpleDateFormat(SINGLE_MONTH_FORMAT);
    public static SimpleDateFormat sdf_year_format = new SimpleDateFormat(YEAR_FORMAT);

    public final  static Long DATE_NUMBER =86400000L;

    private DateUtil()
    {

    }

    /**
     * 得到当前时间的字符格式
     *
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String getCurrentTime()
    {
        SimpleDateFormat standardFormate = getStandardFormate();

        return standardFormate.format(new Date());
    }

    /**
     * 得到当前时间
     *
     * @return Timestamp
     * @see [类、类#方法、类#成员]
     */
    public static Timestamp getCurrentDatetime()
    {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 格式化成系统常用日期格式：yyyyMMddHHmmss
     *
     * @param date date
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String format(Date date)
    {
        if (date == null)
        {
            return null;
        }

        SimpleDateFormat standardFormate = getStandardFormate();
        return standardFormate.format(date);
    }

    /**
     * 获取标准日期格式
     *
     * @return SimpleDateFormat
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static SimpleDateFormat getStandardFormate()
    {
        if (null == dateFormatLocal.get())
        {
            dateFormatLocal.set(new SimpleDateFormat(FORMAT_STANDARD));
        }
        return dateFormatLocal.get();
    }

    private static SimpleDateFormat getDateFormate(String formate)
    {
        return new SimpleDateFormat(formate);
    }

    /**
     * 格式化日期
     *
     * @param date date
     * @param formatStr formatStr
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String format(Date date, String formatStr)
    {
        if (date == null)
        {
            return null;
        }

        SimpleDateFormat sf = new SimpleDateFormat(formatStr);
        return sf.format(date);
    }

    /**
     * 判断两个日期是否为同年同月
     *
     * @param date1 date1
     * @param date2 date2
     * @return boolean
     * @see [类、类#方法、类#成员]
     */
    public static boolean isSameYYYYMM(Date date1, Date date2)
    {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);

        return (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) && (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH));
    }

    /**
     * 把字符串格式化日期
     *
     * @param dateStr dateStr
     * @param formater formater
     * @return Date
     * @see [类、类#方法、类#成员]
     */
    public static Date formdate(String dateStr, String formater)
    {
        formater = (null == formater) ? FORMAT_STANDARD : formater;
        DateFormat formatter = new SimpleDateFormat(formater);
        Date date = null;
        try
        {
            date = formatter.parse(dateStr);
        }
        catch (ParseException e)
        {
            logger.error("ParseException error, Exception = " + e);
        }
        return date;
    }

    /**
     * 本年度第几周
     *
     * @param date
     * @return int
     * @exception throws
     */
    public static int getWeekNumOfYear(Date date)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        return c.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * 转换时间格式为MMdd
     *
     * @param dateString dateString
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String transDateMMdd(String dateString)
    {
        try
        {
            Date date = StringTools.timeStr2Date(dateString, FORMAT_YYYYMMDDHHMMSS);
            return StringTools.date2TimeStr(date, "MMdd");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * 把其他格式的时间字符串转换为yyyMMddHHmmss的格式
     *
     * @param dateString dateString
     * @param format format
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String transDateString(String dateString, String format)
    {
        try
        {
            Date date = StringTools.timeStr2Date(dateString, format);
            return StringTools.date2TimeStr(date, FORMAT_YYYYMMDDHHMMSS);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * 日期转换
     *
     * @param noFormat noFormat
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String transDate(String noFormat)
    {
        try
        {
            Date date = StringTools.timeStr2Date(noFormat, FORMAT_YYYYMMDDHHMMSS);
            return StringTools.date2TimeStr(date, FORMAT_STANDARD);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * 日期转换成“yyyy-MM-dd”格式
     *
     * @param noFormat noFormat
     * @return String
     * @see [类、类#方法、类#成员]
     */
    public static String transDate2(String noFormat)
    {
        try
        {
            Date date = StringTools.timeStr2Date(noFormat, FORMAT_YYYYMMDDHHMMSS);

            return StringTools.date2TimeStr(date, "yyMMdd");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static Date stringDate(String value){
        if(value ==null ||("").equals(value.trim())){
            return  null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try{
            return sdf.parse(value);
        }catch (ParseException e){
            return null;
        }
    }

    /**
     * 判断一个字符串是否满足给定的日期格式<一句话功能简述> <功能详细描述>
     *
     * @param str String
     * @param dateFormat dateFormat
     * @return boolean dateFormat
     * @see [类、类#方法、类#成员]
     */
    public static boolean judgeDateFormat(String str, String dateFormat)
    {
        if (str != null)
        {
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            formatter.setLenient(false);

            try
            {
                formatter.format(formatter.parse(str));
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * getOffsetNewDate
     *
     * @param date date
     * @param offset offset
     * @return Date
     * @see [类、类#方法、类#成员]
     */
    public static Date getOffsetNewDate(Date date, int offset)
    {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.add(Calendar.DAY_OF_MONTH, offset);
        Date newDate = gc.getTime();
        return newDate;
    }

    /**
     * date转timestamp类型 maintian changcheng author changcheng edit changcheng
     */
    public static Timestamp convertDate2TStamp(Date date)
    {
        if (null == date)
        {
            return null;
        }
        return new Timestamp(date.getTime());
    }

    /**
     * 以分钟为单位对时间进行调整
     *
     * @param time 要调整的时间
     * @param minite 增加的分钟数
     *
     * @return String 调整后的时间
     */
    public static String addMinute(String time, int minite)
    {
        if (StringUtils.isEmpty(time))
        {
            return "";
        }
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat(FORMAT_STANDARD);
        Date date = null;
        try
        {
            date = dateFormat.parse(time);
        }
        catch (ParseException e)
        {
            return "";
        }
        calendar.setTimeInMillis(date.getTime());
        calendar.add(Calendar.MINUTE, minite);
        return dateFormat.format(calendar.getTime());
    }

    /**
     * 以分钟为单位对时间进行调整
     *
     * @param minite 增加的分钟数
     *
     * @return String 调整后的时间
     */
    public static Date addMinute(Date requestTime, int minite)
    {
        if (null == requestTime)
        {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(requestTime.getTime());
        calendar.add(Calendar.MINUTE, minite);
        return calendar.getTime();
    }

    /**
     * 获取几天前的日期
     *
     * @param days
     *
     * @return Date
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static Date getBeforeDateInDay(int days)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -days);
        return calendar.getTime();
    }

    /**
     * 得到当前时间的字符格式
     *
     * @param formate
     *
     * @return String [返回类型说明]
     * @see [类、类#方法、类#成员]
     */
    public static String getCurrentTime(String formate)
    {
        SimpleDateFormat standardFormate = getDateFormate(formate);
        return standardFormate.format(new Date());
    }

    /**
     * <一句话功能简述>验证日期 字符串的有效性 格式为:yyyy-MM-dd HH:mm:ss <功能详细描述>
     *
     * @param dateStr
     *
     * @return boolean
     * @see [类、类#方法、类#成员]
     */
    public static boolean validateYYYYMMDDHHMMSSDateStr(String dateStr)
    {
        if (StringUtils.isEmpty(dateStr) || (!YYYYMMDDHHMMSS_PATTERN.matcher(dateStr).matches()))
        {
            return false;
        }

        return true;
    }

    /**
     * 验证日期格式是否为yyyy-MM-dd样式
     *
     * @param date Date
     * @return String
     * @throws ParseException
     * @see [类、类#方法、类#成员]
     */
    public static Boolean isYYYYMMDD(String date)
    {
        if (date == null)
        {
            return false;
        }

        if (YYYYMMDD_PATTERN.matcher(date).matches())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 格式化为 Timestamp
     *
     * @param dateStr String
     * @param formater String
     * @return Timestamp
     * @see [类、类#方法、类#成员]
     */
    public static Timestamp formTimestamp(String dateStr, String formater)
    {
        return new Timestamp(formdate(dateStr, formater).getTime());
    }

    /**
     * 传入时间是否在当前时间之前
     *
     *
     * @return boolean [返回类型说明]
     * @see [类、类#方法、类#成员]
     */
    public static boolean isBeforeNowTime(Date compareTime)
    {
        Date nowTime = new Date();
        return nowTime.before(compareTime);
    }

    /**
     * 转换时间字符串为日期 <功能详细描述>格式为yyyy-MM-dd
     *
     * @param date Date
     * @param formatStr String 日期转换类型
     * @return String
     * @throws ParseException
     * @see [类、类#方法、类#成员]
     */
    public static Date parseNormalDate(String date, String formatStr)
            throws Exception
    {
        if (date == null)
        {
            return null;
        }
        if (YYYYMMDD_PATTERN.matcher(date).matches())
        {
            SimpleDateFormat sdf = getSimpleDateFormat(FORMAT_YYYY_MM_DD);
            sdf.setLenient(false);
            return sdf.parse(date);
        }
        else
        {
            throw new Exception("日期格式不正确");
        }
    }

    /**
     * 从ThreadLocal中获取日期格式器
     *
     * @param format
     * @return [参数说明]
     *
     * @return SimpleDateFormat [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    private static SimpleDateFormat getSimpleDateFormat(String format)
    {
        SimpleDateFormat sdf = dateFormat.get();
        if (null != sdf)
        {
            if (StringUtils.isNotEmpty(format))
            {
                sdf.applyPattern(format);
            }
            return sdf;
        }
        else
        {
            if (StringUtils.isEmpty(format))
            {
                format = FORMAT_YYYY_MM_DD_SECONDS;
            }
            sdf = new SimpleDateFormat(format);
            dateFormat.set(sdf);
        }
        return sdf;
    }

    /**
     * Gets date.
     *
     * @param dateStr the date str
     * @return the date
     */
    public static Date getDate(String dateStr)
    {
        if(null!=dateStr)
        {
            try {
                if(dateStr.length()==19)
                {
                    if(dateStr.indexOf("/")>0)
                    {
                        return sdf_datetime_format.parse(dateStr.replaceAll("/", "-"));
                    }
                    return sdf_datetime_format.parse(dateStr);
                }else if(dateStr.length()==10)
                {
                    return sdf_date_format.parse(dateStr);
                }else if(dateStr.length()==8){
                    return sdf_hour_format.parse(dateStr);
                }
            } catch (Exception e) {
                logger.error("格式化时间错误", e);
            }
        }
        return null;
    }

    /**
     * 传入时间是否是今天
     *
     * @return
     */
    public static boolean inDay(Date date)
    {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());

        return (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) && (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH))
                && (c1.get(Calendar.DATE) == c2.get(Calendar.DATE));
    }

    /**
     * 返回与指定日期相差days天的日期
     * @param date
     * @param days
     * @return
     */
    public static Date getDaydiffDate(Date date,int days){
        Date result = new Date();
        result.setTime(date.getTime()+DATE_NUMBER*days);
        return  result;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:39 2018/7/13
    */
    public static String getDateTimeforMonth(Date date) {
        try {
            return sdf_month_format.format(date);
        } catch (Exception e) {
            logger.debug("DateUtil.getDateTime():" + e.getMessage());
            return "";
        }
    }

    public static String getDateTimeforSingleMonth(Date date) {
        try {
            return sdf_single_month_format.format(date);
        } catch (Exception e) {
            logger.debug("DateUtil.getDateTime():" + e.getMessage());
            return "";
        }
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:39 2018/7/13
    */
    public static String getDateTimeforYear(Date date) {
        try {
            return sdf_year_format.format(date);
        } catch (Exception e) {
            logger.debug("DateUtil.getDateTime():" + e.getMessage());
            return "";
        }
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 12:00 2018/7/16
     * 比较日期相差天数
    */
    public static int getMargin(String date1, String date2) {
        int margin;
        try {
            ParsePosition pos = new ParsePosition(0);
            ParsePosition pos1 = new ParsePosition(0);
            Date dt1 = sdf_date_format.parse(date1, pos);
            Date dt2 = sdf_date_format.parse(date2, pos1);
            long l = dt1.getTime() - dt2.getTime();
            margin = (int) (l / (24 * 60 * 60 * 1000));
            return margin;
        } catch (Exception e) {
            logger.debug("DateUtil.getMargin():" + e.toString());
            return 0;
        }
    }


    public static String getDateTime() {
        try {
            return sdf_datetime_format.format(getCurrCalendar().getTime());
        } catch (Exception e) {
            logger.debug("DateUtil.getDateTime():" + e.getMessage());
            return "";
        }
    }

    public static Calendar getCurrCalendar()
    {
        return  Calendar.getInstance();
    }


    /**
     * 返回日期加X月后的日期
     *
     * @param date the date
     * @param i    the
     * @return string
     * @date Mar 11, 2012
     */
    public static String addMonth(String date, int i) {
        try {
            GregorianCalendar gCal = new GregorianCalendar(
                    Integer.parseInt(date.substring(0, 4)),
                    Integer.parseInt(date.substring(5, 7)) - 1,
                    Integer.parseInt(date.substring(8, 10)));
            gCal.add(GregorianCalendar.MONTH, i);
            return new SimpleDateFormat(FORMAT_STANDARD).format(gCal.getTime());
        } catch (Exception e) {
            logger.debug("DateUtil.addMonth():" + e.toString());
            return null;
        }

    }

    /**
     * 得到本月的第一天
     *
     * @return
     */
    public static String getMonthFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String result = sdf.format(calendar.getTime());
        return result;
    }

}

