/**   
* @version V1.0   
* @author	chw
*/
package com.tcxf.hbc.common.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportUtil {
	private static Logger log = Logger.getLogger(ExportUtil.class);

	public static void sendIOStream(Workbook workbook,
			HttpServletResponse response, String filename) {
		OutputStream out = null;
		try {
			if (workbook == null) {
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html;charset=utf-8");
				response.setHeader("Pragma", "No-cache");
				response.setHeader("Cache-Control", "no-cache");
				out = response.getOutputStream();
			} else {
				response.setHeader("Content-disposition","attachment;filename=\""
								+ new String(filename.getBytes(),"ISO8859-1") + "\"");
				out = response.getOutputStream();
				workbook.write(out);
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
				out = null;
			}
		}
	}

	/**
	 * workbook
	 * 
	 * @param list
	 * @param title
	 * @return
	 */
	public static void createExcel(List<String[]> list, String[] title,
			HttpServletResponse response, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		CellStyle style = createTitleStyle(workbook);
		Row rowTittle = sheet.createRow(0);
		for (int i = 0; i < title.length; i++) {
			createCell(rowTittle, i, style, Cell.CELL_TYPE_STRING, title[i]);
		}
		Row row = null;
		for (int j = 0; j < list.size(); j++) {
			String[] strs = list.get(j);
			row = sheet.createRow(j + 1);
			for (int z = 0; z < strs.length; z++) {
				createCell(row, z, style, Cell.CELL_TYPE_STRING, strs[z]);
			}
		}
		sendIOStream(workbook, response, filename);
	}
	
	public static void createExcel(List<String[]> list,HttpServletResponse response, String filename) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		CellStyle style = createTitleStyle(workbook);
		Row row = null;
		for (int j = 0; j < list.size(); j++) {
			String[] strs = list.get(j);
			row = sheet.createRow(j);
			for (int z = 0; z < strs.length; z++) {
				createCell(row, z, style, Cell.CELL_TYPE_STRING, strs[z]);
			}
		}
		sendIOStream(workbook, response, filename);
	}

	/**
	 * 设置excel的title样式
	 * 
	 * @param wb
	 * @return
	 */
	private static CellStyle createTitleStyle(Workbook wb) {
		Font boldFont = wb.createFont();
		boldFont.setFontHeight((short) 200);
		CellStyle style = wb.createCellStyle();
		style.setFont(boldFont);
		return style;
	}

	/**
	 * 创建Excel单元格
	 * @param row
	 * @param column
	 * @param style
	 * @param cellType
	 * @param value
	 */
	private static void createCell(Row row, int column, CellStyle style,
			int cellType, Object value) {
		Cell cell = row.createCell(column);
		if (style != null) {
			cell.setCellStyle(style);
		}
		switch (cellType) {
		case Cell.CELL_TYPE_BLANK: {
		}
			break;
		case Cell.CELL_TYPE_STRING: {
			if (value != null) {
				cell.setCellValue(value.toString());
			} else {
				cell.setCellType(Cell.CELL_TYPE_BLANK);
			}

		}
			break;
		case Cell.CELL_TYPE_NUMERIC: {
			if (value != null) {
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				cell.setCellValue(Double.parseDouble(value.toString()));
			} else {
				cell.setCellType(Cell.CELL_TYPE_BLANK);
			}
		}
			break;
		default:
			break;
		}
	}
}
