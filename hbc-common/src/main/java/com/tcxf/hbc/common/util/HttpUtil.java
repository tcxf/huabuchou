package com.tcxf.hbc.common.util;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 微信接口调用工具
 * @author liuxu
 * @creation 2018年7月4日
 */
@Component
public class HttpUtil {
	/**
	 * 花不愁支付公众号appid
	 */
	public static final String APP_ID="wx11450c903038e6b2";

	/**
	 * 花不愁支付公众号secret
	 */
	public static final String SECRET="a36bdfb839294f754ab316d70746166f";

	private static final int TIMEOUT = 30000;

	/**
	 * redis 缓存有限时间 7200秒
	 */
	private static final int REDIS_TIMEOUT=7200;
	/**
	 * redis 缓存key access_token前缀
	 */
	private static final String ACCESS_TOKEN="access_token";
	/**
	 * redis 缓存key ticket前缀
	 */
	private static final String TICKET="ticket";
	private static final Logger logger = LogManager.getLogger(HttpUtil.class);

	private  static RedisTemplate<String, String> template;
	/**
	 * 获取token
	 * access_token是公众号的全局唯一票据，公众号调用各接口时都需使用access_token。
	 * 开发者需要进行妥善保存。access_token的存储至少要保留512个字符空间。
	 * access_token的有效期目前为2个小时，需定时刷新，重复获取将导致上次获取的access_token失效。
	 * @return
	 */
	private static String getWeixinToken(String appId,String secret) {
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
		url = url.replace("APPID", appId);
		url = url.replace("APPSECRET", secret);
		String result_data = HttpUtil.httpRequest(url, "GET", "");
		Map<String, Object> m_result = JsonTools.parseJSON2Map(result_data);
		String access_token = (String) m_result.get(ACCESS_TOKEN);
		template.opsForValue().set(ACCESS_TOKEN+"_"+appId,access_token,REDIS_TIMEOUT,TimeUnit.SECONDS);
		return access_token;
	}


	/**
	 * 获取Jsapi_ticket
	 * 生成签名之前必须先了解一下jsapi_ticket，jsapi_ticket是公众号用于调用微信JS接口的临时票据。
	 * 正常情况下，jsapi_ticket的有效期为7200秒，通过access_token来获取。
	 * 由于获取jsapi_ticket的api调用次数非常有限，
	 * 频繁刷新jsapi_ticket会导致api调用受限，影响自身业务，开发者必须在自己的服务全局缓存jsapi_ticket 。
	 * */
	private static String getJsapi_ticket(String appId,String secret){
		String access_token=getRedisToken(appId,secret);
		if(ValidateUtil.isEmpty(access_token)){
			return null;
		}
		String url ="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
		url = url.replace("ACCESS_TOKEN", access_token);
		String result_data = HttpUtil.httpRequest(url, "GET", "");
		Map<String, Object> m_result = JsonTools.parseJSON2Map(result_data);
		String ticket = (String) m_result.get(TICKET);
		template.opsForValue().set(TICKET+"_"+appId,ticket,REDIS_TIMEOUT,TimeUnit.SECONDS);
		return ticket;
	}

	public static void main(String[] args) {

//		String appId="wx78a723c6fdf123c8";
//		String secret="b8aef7e6d2670e287d3579057c27cdef";
//		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
//		url = url.replace("APPID", appId);
//		url = url.replace("APPSECRET", secret);
//		String result_data = HttpUtil.httpRequest(url, "GET", "");
//		System.out.println(result_data);
//		Map<String, Object> m_result = JsonTools.parseJSON2Map(result_data);
//		String access_token = (String) m_result.get(ACCESS_TOKEN);
//
//
//		String url2 ="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
//		url2 = url2.replace("ACCESS_TOKEN", access_token);
//		String result_data2 = HttpUtil.httpRequest(url2, "GET", "");
//		Map<String, Object> m_result2 = JsonTools.parseJSON2Map(result_data2);
//		String ticket = (String) m_result2.get(TICKET);
//		System.out.println(ticket);


		String jsapi_ticket = "LIKLckvwlJT9cWIhEQTwfHgSS0LoVEsvFyzqfgxGq6Ui-Dw1nUPadMFNXIn_UOFBoFDqsqa1fbAdN7P2VB_YpQ";
		//32位随机字符
		String noncestr = "T3q5CasDjmKI0PFaZ6EaDRCyWEtidemj";//StringUtil.RadomCode(32,StringUtil.RADOMCODE_TYPE_OTHER);
		//时间戳
		String timestamp = "1538989487"; //String.valueOf(System.currentTimeMillis()/1000);
		//网页地址
		String url = "http://yy.huabuchou.com/consumer/user/index/Goindex";
		url = url.split("#")[0];
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("noncestr",noncestr);
		map.put("jsapi_ticket",jsapi_ticket);
		map.put("url",url);
		map.put("timestamp",timestamp);
		String sign = getSign_SHA1(map);

		System.out.println(noncestr);
		System.out.println(url);
		System.out.println(timestamp);
		System.out.println(sign);
	}


	/**
	 * 获取 ticketConfig
	 * @return
	 */
	public static Map<String,Object> ticketConfig(String pageUrl,String appId,String secret){
		String jsapi_ticket = getRedisJsapi_ticket(appId,secret);
		//32位随机字符
		String noncestr = StringUtil.RadomCode(32,StringUtil.RADOMCODE_TYPE_OTHER);
		//时间戳
		String timestamp = String.valueOf(System.currentTimeMillis()/1000);
		//网页地址
		String url = pageUrl.split("#")[0];
		url = url.replace(":80", "");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("noncestr",noncestr);
		map.put("jsapi_ticket",jsapi_ticket);
		map.put("url",url);
		map.put("timestamp",timestamp);
		String sign = getSign_SHA1(map);
		map.put("sign",sign);
		map.put("appId",appId);
		return map;
	}

	/**
	 *SHA1签名
	 * @param map
	 * @return
	 */
	private static String getSign_SHA1(Map<String,Object> map){
		ArrayList<String> list = new ArrayList<String>();
		for(Map.Entry<String,Object> entry:map.entrySet()){
			if(entry.getValue()!=""){
				String key=entry.getKey();
				list.add(key + "=" + entry.getValue() + "&");
			}
		}
		int size = list.size();
		String [] arrayToSort = list.toArray(new String[size]);
		Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < size; i ++) {
			sb.append(arrayToSort[i]);
		}
		String result = sb.toString().substring(0,sb.length()-1);
		System.out.println(result);
		return DigestUtils.sha1Hex(result);
	}

	/**
	 * 从Redis缓存中获取微信token 如果缓存中不存在 则从微信官网服务器上获取
	 * @return
	 */
	public static String getRedisToken(String appId,String secret){
		String wx_access_token =  template.opsForValue().get(ACCESS_TOKEN+"_"+appId);
		if(ValidateUtil.isEmpty(wx_access_token)){
			return getWeixinToken(appId,secret);
		}
		return wx_access_token;
	}
	/**
	 * 从Redis缓存中获取微信ticket 如果缓存中不存在 则从维修官网服务器上获取
	 * @return
	 */
	public static String getRedisJsapi_ticket(String appId,String secret){
		String ticket =  template.opsForValue().get(TICKET+"_"+appId);
		if(ValidateUtil.isEmpty(ticket)){
			return getJsapi_ticket(appId,secret);
		}
		return ticket;
	}

	/**
	 * 通过code换取网页授权access_token（与基础支持中的access_token不同）
	 * 1、微信网页授权是通过OAuth2.0机制实现的，在用户授权给公众号后，公众号可以获取到一个网页授权特有的接口调用凭证（网页授权access_token），
	 * 通过网页授权access_token可以进行授权后接口调用，如获取用户基本信息；
	 * 2、其他微信接口，需要通过基础支持中的“获取access_token”接口来获取到的普通access_token调用。
	 * @param code
	 * @return
	 */
	public static Map<String, Object> getAccesstokenByCode(String code,String appid,String secret) {
		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
		url = url.replace("APPID", appid);
		url = url.replace("SECRET",secret);
		url = url.replace("CODE", code);
		String result_data = HttpUtil.httpRequest(url, "GET", "");
		return JsonTools.parseJSON2Map(result_data);
	}

	/**
	 * 根据code获取openId
	 * @param code
	 * @return
	 */
	public static String getOpenId(String code , String appId , String secret)
	{
		if(ValidateUtil.isEmpty(code)|| ValidateUtil.isEmpty(appId) || ValidateUtil.isEmpty(secret)){
			return null;
		}
		Map<String, Object> userToken = HttpUtil.getAccesstokenByCode(code , appId , secret);
		for (String key : userToken.keySet()) {
			if ("openid".equalsIgnoreCase(key)) {
				return userToken.get(key).toString();
			}
		}
		return null;
	}

	/**
	 * 获取微信openId snsapi_base（静默方式，用户无感知，只能获取openID,无法获取具体用户信息 如：头像等）
	 * 方式 第一步操作，引导用户进入授权页面同意授权，获取code
	 * @param baseUrl
	 */
	public static String buildSendRedUrl(String baseUrl,String appid){
		StringBuilder url = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
		url.append(appid).append("&redirect_uri=").append(baseUrl.replace(":80", ""));
		url.append("redirectUrlValue");
		url.append("&response_type=code&scope=snsapi_base&state=statecode");
		url.append("#wechat_redirect");
		return url.toString();
	}


	/**
	 * 发起https请求并获取结果
	 * @param requestUrl 请求地址
	 * @param requestMethod 请求方式（GET、POST）
	 * @param outputStr 提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static String httpRequest(String requestUrl, String requestMethod,String outputStr) {
		StringBuffer buffer = new StringBuffer();
		HttpsURLConnection httpUrlConn = null;
		try {
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			URL url = new URL(requestUrl);
			httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);
			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			httpUrlConn.setReadTimeout(TIMEOUT);
			httpUrlConn.setRequestMethod(requestMethod);
			if ("GET".equalsIgnoreCase(requestMethod)) {
				httpUrlConn.connect();
			}
			if (null != outputStr && !"".equals(outputStr)) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			return buffer.toString();
		} catch (ConnectException ce) {
			   StringWriter sw = new StringWriter();   
	            PrintWriter pw = new PrintWriter(sw, true);   
	            ce.printStackTrace(pw);   
	            pw.flush();   
	            sw.flush();   
			logger.error(sw.toString());
			if (httpUrlConn != null) {
				httpUrlConn.disconnect();
				httpUrlConn = null;
			}
		} catch (Exception e) {
			   StringWriter sw = new StringWriter();   
	            PrintWriter pw = new PrintWriter(sw, true);   
	            e.printStackTrace(pw);   
	            pw.flush();   
	            sw.flush();   
			logger.error(sw.toString());
		}
		logger.error("{errcode:'ERR9999',errmsg:'请求微信服务器失败'}");
		return "{errcode:'ERR9999',errmsg:'请求微信服务器失败'}";
	}

	public  RedisTemplate<String, String> getTemplate() {
		return template;
	}

	@Resource(name = "redisTemplate")
	public void setTemplate(RedisTemplate<String, String> template){
		HttpUtil.template = template;
	}

}
