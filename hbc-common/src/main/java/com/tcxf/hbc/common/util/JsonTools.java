package com.tcxf.hbc.common.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import sun.reflect.generics.tree.Tree;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * java对象json序列化类
 */
public class JsonTools
{
    private static final SerializeConfig serializeConfig = new SerializeConfig();

    static
    {
        serializeConfig.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
        serializeConfig.put(Timestamp.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
    }

    public static String getJsonFromObject(Object req)
    {
        return JSON.toJSONString(req, serializeConfig, SerializerFeature.DisableCircularReferenceDetect);
    }

    public static String toJson(Object obj)
    {
        return JSON.toJSONString(obj, serializeConfig, SerializerFeature.DisableCircularReferenceDetect);
    }

    public static <T> T fromJson(String str, Class<T> clazz)
    {
        return JSON.parseObject(str, clazz);
    }

    public static <T> T fromResourceJson(String str, Class<T> clazz)
    {
        return JSON.parseObject(str, clazz);
    }

    public static <T> T parseObject(String str, Class<T> clazz)
    {
        return JSON.parseObject(str, clazz);
    }

    public static <T> T parseObjectToObject(Object srcObj, Class<T> clazz)
    {
        return parseObject(toJson(srcObj), clazz);
    }

    public static Map parseJSON2Map(String json){
        return fromJson(json,Map.class);
    }

    public static TreeMap parseJSON2TreeMap(String json){
        return fromJson(json,TreeMap.class);
    }
}
