package com.tcxf.hbc.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

/**
* @Author:YWT_tai
* @Description
* @Date: 14:34 2018/6/15
*/
public class JsonUtils {

	
	
	private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);

	private final static  ObjectMapper objectMapper = new ObjectMapper();

	/**
	* 默认非空不输出，时间格式
	*/
	static {
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true); 
	}


	
    
    /**
     * 将 Java 对象转为 JSON 字符串
     */
    public static <T> String toJSON(T obj) {
        String jsonStr;
        try {
            jsonStr = objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            logger.error("Java 转 JSON 出错！",e );
            throw new CheckedException("Json转对象出错");
        }
        return jsonStr;
    }

    /**
     * 将 JSON 字符串转为 Java 对象
     * @param json
     * @param type
     * @return T
     */
    public static <T> T fromJSON(String json, Class<T> type) {
        if(!"".equals(json) && json!=null)
        { 
        	try {
                return objectMapper.readValue(json, type);
        	} catch (Exception e) {
        		logger.error("JSON 转 Java 出错！", e);
        		throw new CheckedException("Json转对象出错");
        	}
        }  
        return null; 
    }

    
 
      
    
    /** 
     * 反序列化复杂Collection如List<Bean>, 先使用createCollectionType构造类型,然后调用本函数. 
     * @see #createCollectionType(Class, Class...) 
     * @param jsonString
     * @param javaType
     * @return T
     */
    @SuppressWarnings("unchecked")  
    public static <T> T fromJson(String jsonString, JavaType javaType) {  
        if (!"".equals(jsonString) && jsonString!=null)
        {  
        	try {  
        		return (T) objectMapper.readValue(jsonString, javaType);  
        	} catch (IOException e) {  
        		logger.error("JSON 转 Java 出错！", e);
        		throw new CheckedException("Json转对象出错");
        	}  
        }  
  
        return null; 
    }  
      
    /**
     * 构造的Collection Type如: 
     * ArrayList<Bean>, 则调用constructCollectionType(ArrayList.class,Bean.class) 
     * HashMap<String,Bean>, 则调用(HashMap.class,String.class, Bean.class)   
     * @param collectionClass
     * @param elementClasses
     * @return JavaType
     */
    public static JavaType createCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {  
        return objectMapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);  
    }  
      
    
  
    /**
     * json的字节流转换为对象  
     * @param request
     * @param clazz
     * @return T
     */
    public static <T> T getRequestFromObject(HttpServletRequest request,  
            Class<T> clazz) {  
        try {  
            return fromJSON(request.getInputStream(), clazz);  
        } catch (IOException e) {  
        	logger.error("json的字节流转换为对象   出错！", e);
    		throw new CheckedException("Json转对象出错");
        } 
    }  
  
    /**
     * json的字节流转换为对象   
     * @param json
     * @param clazz
     * @return T
     */
    public static <T> T fromJSON(InputStream json, Class<T> clazz) {  
        try {  
            return objectMapper.readValue(json, clazz);  
        } catch (Exception e) {  
        	logger.error("json的字节流转换为对象出错！", e);
    		throw new CheckedException("字节流转换为对象出错");
        }  
    }  
  
    /***
     * 
     * @param jstr
     * @param li
     * @return List<Object>
     */
    public static List<Object> getJsonList(String jstr, List<Object> li) {  
        char[] cstr = jstr.toCharArray();  
        boolean bend = false;  
        int istart = 0;  
        int iend = 0;  
        for (int i = 0; i < cstr.length; i++) {  
            if ((cstr[i] == '{') && !bend) {  
                istart = i;  
            }  
            if (cstr[i] == '}' && !bend) {  
                iend = i;  
                bend = true;  
            }  
        }  
  
        if (istart != 0) {  
            String substr = jstr.substring(istart, iend + 1);  
            jstr = jstr.substring(0, istart - 1)  
                    + jstr.substring(iend + 1, jstr.length());  
            substr = substr.replace(",\"children\":", "");  
            substr = substr.replace("]", "");  
            substr = substr.replace("[", "");  
            li.add(substr);  
            getJsonList(jstr, li);  
        }  
        return li;  
    }    
  
}
