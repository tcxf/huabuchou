package com.tcxf.hbc.common.util;

import com.tcxf.hbc.common.util.exception.CheckedException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Map对象操作工具
* @Author:YWT_tai
* @Description
* @Date: 17:31 2018/6/9
*/
public class MapUtil extends MapUtils {

	private static final Logger logger = LoggerFactory.getLogger(MapUtil.class);
    /***
     * 从请求中获取Map对象
     * @param request
     * @return Map<String,Object>
     */
    public static Map<String,Object> getMapForRequest(HttpServletRequest request)
    {
    	Map<String,Object> map=new HashMap<String,Object>();
    	
    	Enumeration<String>  enumeration=request.getParameterNames();
    	while(enumeration.hasMoreElements())
    	{
    		String key=enumeration.nextElement();
    		map.put(key,request.getParameter(key));
    	}
    	return map;
    }

	/***
	 * 通过Map获取Bean对象
	 * @param object  bean类对象
	 * @param map		map对象
	 * @return Object		bean对象
	 */
	public static synchronized Object getBeanForMap(Object object,Map<String,Object> map)
	{
		try {
			ConvertUtils.deregister();
			ConvertUtils.register(new Converter() {
				@Override
				public Object convert(Class aClass, Object o) {
					return  DateUtil.getDate(o.toString());
				}
			},Date.class);
			ConvertUtils.register(new Converter() {
				@Override
				public Object convert(Class aClass, Object o) {
					if(o!=null && isBlank(o.toString()))
					{
						return Integer.valueOf(0);
					}
					if(!NumberUtils.isDigits(o.toString()))
					{
						return Integer.valueOf(0);
					}
					return  Integer.valueOf(o.toString());
				}
			},Integer.class);
			BeanUtils.populate(object, map);
		} catch (Exception e) {
			logger.error("getBeanForMap error.", e);
		}
		return object;
	}

	/**
	* @Author:YWT_tai
	* @Description
	* @Date: 9:07 2018/6/23
	*/
	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	/**
	* @Author:YWT_tai
	* @Description
	* @Date: 9:07 2018/6/23
	*/
	public static  String getStringForMapObj(Map<String,Object> map,String key,boolean errorFlag)
	{
		if(!isBlank(key))
		{
			Object obj=map.get(key);
			if(obj!=null)
			{
				String str=String.valueOf(obj);
				if(errorFlag)
				{
					if(!isBlank(str))
					{
						return str;
					}
				}else
				{
					return str;
				}
			}
			if(errorFlag)
			{
				System.out.println("Thread:"+Thread.currentThread().getId());
				throw new CheckedException("从Map中通过%s获取对象为空");
			}
		}
		if(errorFlag)
		{
			throw new CheckedException("从Map中通过%s获取对象出错");
		}else
		{
			return null;
		}
	}

	/**
	* @Author:YWT_tai
	* @Description
	* @Date: 15:58 2018/6/21
	 * 获取运营商登陆的oid
	*/
	public static  Map<String, Object> getSessionOid(Map<String,Object> paramMap,HttpServletRequest request)
	{
		String oid = (String) request.getSession().getAttribute("session_opera_id");
		if(oid!=null && !"".equals(oid)){
			paramMap.put("oid",oid);
		}else{
			throw new CheckedException("非法请求!");
		}
		return paramMap;
	}



	/**
	* @Author:YWT_tai
	* @Description
	* @Date: 13:38 2018/6/25
	*/
	public static  Map<String, Object> getSessionFid(Map<String,Object> paramMap,HttpServletRequest request)
	{
		String fid = (String) request.getSession().getAttribute("session_fund_id");
		if(fid!=null && !"".equals(fid)){
			paramMap.put("fid",fid);
		}else{
			throw new CheckedException("非法请求!");
		}
		return paramMap;
	}


}
