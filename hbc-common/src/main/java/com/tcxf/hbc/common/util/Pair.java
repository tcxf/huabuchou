package com.tcxf.hbc.common.util;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * pair组合工具类
 */
public class Pair<L, R> {
	private final L left;
	private R right;
	
	/**
	 * 创建parir
	 * @param u 左边对象
	 * @param v 右边对象
	 * @return
	 */
	static public<U,V> Pair<U,V> create(U u, V v) {
		return new Pair<U,V>(u,v);
	}
	
	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	public L getLeft() {
		return left;
	}

	public R getRight() {
		return right;
	}

	public void setRight(R right) {
		this.right = right;
	}
	
	/**
	 * 重写equals方法
	 */
	@Override
	public boolean equals(Object other) {
		if (other == this){
			return true;
		}
		if (!(other instanceof Pair))
		{
			return false;
		}
		Pair<?, ?> that = (Pair<?, ?>) other;
		return new EqualsBuilder().append(this.left, that.left).append(
				this.right, that.right).isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.left).append(this.right)
				.toHashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		sb.append(null==left?"":left.toString());
		sb.append(",");
		sb.append(null==right?"":right.toString());
		sb.append(")");
		return sb.toString();
	}
}