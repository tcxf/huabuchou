package com.tcxf.hbc.common.util;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @param <T>
 * @author zhouyj
 */
public class R<T> implements Serializable {

    /**
     * 成功描述
     */
    public static final String OK_MESSAGE = "操作成功";
    public static final String ERROR_MESSAGE = "操作失败";


    private static final long serialVersionUID = 1L;

    public static final int NO_LOGIN = -1;

    public static final int SUCCESS = 0;

    public static final int FAIL = 1;

    public static final int NO_PERMISSION = 2;

    private String msg = "success";

    private int code = SUCCESS;

    private  T data;

    public R() {
        super();
    }

    public static<T> R newOK(){
        return newOK(null);
    }
    public static<T> R newOK(T data){
        return newOK(SUCCESS, data);
    }
    public static<T> R newOK(String msg,T data){
        return new R(SUCCESS, msg, data);
    }

    public static<T> R newOK(int code ,T data){
        return new R(SUCCESS, OK_MESSAGE, data);
    }
    public static<T> R newError(){
        return newError(null);
    }
    public static<T> R newError(T data){
        return newError(ERROR_MESSAGE,data);
    }

    public static<T> R newErrorMsg(String message){
        return newError(message, null);
    }

    public static<T> R newError(String message,T data){
        return new R(FAIL, message, data);
    }
    public R(int code,String msg,T data){
        this.code = code;
        this.msg = msg;
        if(!ValidateUtil.isEmpty(data)){
            this.data = data;
        }
    }

    public R(T data) {
        super();
        this.data = data;
    }
    public R(T data, String msg) {
        super();
        this.data = data;
        this.msg = msg;
    }

    public R(Throwable e) {
        super();
        this.msg = e.getMessage();
        this.code = FAIL;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
