package com.tcxf.hbc.common.util;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 字符串工具类
 * 
 * @author programtic
 * @version [版本号, 2013-9-3]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class StringTools
{
    /**
     * yyyy-MM-dd HH:mm:ss 格式
     */
    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * yyyy-MM-dd 格式
     */
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    
    private StringTools()
    {
        
    }
    
    /**
     * 获取长度
     * 
     * @param src
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static int getLength(String src)
    {
        return ((null == src) || ("".equals(src))) ? 0 : src.getBytes().length;
    }
    
    /**
     * 判断是否是空对象
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String nvl(Object o)
    {
        return (null == o) ? "" : o.toString().trim();
    }
    
    /**
     * 替换为$符号
     * 
     * @param instr
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String replaceDollarMark(String instr)
    {
        StringBuffer sb = new StringBuffer(instr);
        int place = sb.indexOf("$");
        while (place >= 0)
        {
            sb.replace(place, place + 1, "$$");
            place = sb.indexOf("$", place + 2);
        }
        return sb.toString();
    }
    
    /**
     * 格式化
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String getMsg(String value, Object[] params)
    {
        try
        {
            return MessageFormat.format(value, params);
        }
        catch (Exception ex)
        {
            return value;
        }
    }
    
    /**
     * 转为为long
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static Long toLong(String s)
    {
        Long l = null;
        if (StringUtils.isNotEmpty(s))
        {
            l = Long.valueOf(s);
        }
        return l;
    }
    
    /**
     * 转为为String
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String toString(Object obj)
    {
        if (obj == null)
        {
            return "";
        }
        else
        {
            String s = obj.toString().trim();
            return s;
        }
    }
    
    /**
     * 转为为Float
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static float toFloat(String s, float def)
    {
        float f = def;
        
        try
        {
            f = Float.parseFloat(s);
        }
        catch (Exception e)
        {
            f = def;
        }
        return f;
    }
    
    /**
     * 把字符串转化为整数
     * 
     * @param s 待转化为整数的字符串
     * @param def 转化为整数出错时的，设置的整数值
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static int toInt(String s, int def)
    {
        int value = def;
        
        try
        {
            value = Integer.parseInt(s);
        }
        catch (Exception e)
        {
            value = def;
        }
        
        return value;
    }
    
    /**
     * 判断字符串是否为整数
     * 
     * @param s
     * @see [类、类#方法、类#成员]
     */
    public static boolean isInt(String s)
    {
        boolean result = true;
        if (StringTools.isEmpty(s))
        {
            result = false;
            return result;
        }
        try
        {
            Integer.parseInt(s);
        }
        catch (Exception e)
        {
            result = false;
        }
        
        return result;
    }
    
    /**
     * 判断字符串是否为Long类型
     * 
     * @param s
     * @see [类、类#方法、类#成员]
     */
    public static boolean isLong(String s)
    {
        boolean result = true;
        if (StringTools.isEmpty(s))
        {
            result = false;
            return result;
        }
        try
        {
            Long.parseLong(s);
        }
        catch (Exception e)
        {
            result = false;
        }
        
        return result;
    }
    

    
    /**
     * 用指定的字符串拼接一个集合
     * 
     * @param list
     * @param joinStr
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String join(List<?> list, String joinStr)
    {
        if (CollectionUtils.isEmpty(list))
        {
            return null;
        }
        if (list.size() == 1)
        {
            if (list.get(0) == null)
            {
                return null;
            }
            else
            {
                return list.get(0).toString();
            }
        }
        joinStr = joinStr == null ? "" : joinStr;
        StringBuilder builder = new StringBuilder();
        for (Object obj : list)
        {
            builder.append(obj).append(joinStr);
        }
        builder.replace(builder.length() - joinStr.length(), builder.length(), "");
        return builder.toString();
    }
    
    /**
     * '',''
     * 
     * @param list
     * @param joinStr
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String joinSqlIn(List<String> list, String joinStr)
    {
        if (CollectionUtils.isEmpty(list))
        {
            return null;
        }
        if (list.size() == 1)
        {
            if (list.get(0) == null)
            {
                return null;
            }
            else
            {
                return "'" + list.get(0).toString() + "'";
            }
        }
        joinStr = joinStr == null ? "" : joinStr;
        StringBuilder builder = new StringBuilder();
        for (String obj : list)
        {
            builder.append("'").append(obj).append("'").append(joinStr);
        }
        builder.replace(builder.length() - joinStr.length(), builder.length(), "");
        return builder.toString();
    }
    
    /**
     * 日期转换工具
     * 
     * @param time
     * @param pattern
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static Date timeStr2Date(String time, String pattern)
    {
        if (null == time)
        {
            throw new IllegalArgumentException("time parameter can not be null");
        }
        if (null == pattern)
        {
            throw new IllegalArgumentException("pattern parameter can not be null");
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try
        {
            return sdf.parse(time);
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("using [" + pattern + "] parse [" + time + "] failed");
        }
    }
    
    /**
     * 日期转换工具
     * 
     * @param time
     * @param pattern
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String date2TimeStr(Date time, String pattern)
    {
        if (null == pattern)
        {
            throw new IllegalArgumentException("pattern parameter can not be null");
        }
        if (null == time)
        {
            throw new IllegalArgumentException("time parameter can not be null");
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(time);
    }
    
    /**
     * 元转为分
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static int yuanToFen(String s, int def)
    {
        float yuan = StringTools.toFloat(s, def);
        int fen = (int)(yuan * 100);
        
        return fen;
    }
    
    /**
     * 计算折扣
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String convertDiscount(String discount)
    {
        if (discount == null || "".equals(discount.trim()))
        {
            return "";
        }
        else if (discount.endsWith("0"))
        {
            return discount.substring(0, discount.length() - 1);
        }
        else
        {
            return discount;
        }
    }
    
    /**
     * 设置默认值
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String asDefault(String s, String def)
    {
        if ((s == null) || "".equals(s.trim()))
        {
            return def;
        }
        else
        {
            return s;
        }
    }
    

    
    /**
     * trim字符串
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String trim(String str)
    {
        return str == null ? null : str.trim();
    }
    
    /**
     * 判断字符串是否为空
     * 
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmpty(String str)
    {
        return (str == null) || (str.trim().length() == 0);
    }
    
    /**
     * 判断是否为数字 支持小数的验证
     * 
     * @param str
     * @return [参数说明]
     * 
     * @see [类、类#方法、类#成员]
     */
    public static boolean isNumeric(String str)
    {
        if (StringUtils.isEmpty(str))
        {
            return false;
        }
        
        int x = str.indexOf(".");
        if (x > 0 && x != (str.length() - 1))
        {
            StringBuilder strb = new StringBuilder(str);
            return StringUtils.isNumeric(strb.deleteCharAt(x).toString());
        }
        
        return StringUtils.isNumeric(str);
    }
    
    /**
     * 判断字符串是否以特定字符结束
     * 
     * @param str 字符串
     * @param endChar 是否以这个字符结束
     * @return
     */
    public static boolean endsWith(String str, char endChar)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return str.charAt(str.length() - 1) == endChar;
    }
    
    /**
     * 处理路径
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String fixPathForWindows(String str)
    {
        if (isEmpty(str))
        {
            return "";
        }
        
        return str.replaceAll("/", "\\\\");
    }
    
    /**
     * 修正路径，把\替换成/
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String fixPathForWeb(String str)
    {
        if (isEmpty(str))
        {
            return "";
        }
        
        return str.replaceAll("\\\\", "/");
    }
    
    /**
     * 传入基础文件地址与相对地址，生成一个绝对路径，基础路径中一定要带有文件名
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String processDir(String strBasePath, String strRelativePath)
    {
        // 删除基础路径中的文件名
        int iPos = strBasePath.lastIndexOf("\\");
        strBasePath = strBasePath.substring(0, iPos + 1);
        
        // 计算基础路径有几级
        String[] strTemps = strBasePath.split("\\\\");
        if ((strTemps == null) || (strTemps.length == 0))
        {
            return "";
        }
        int iBasePathLevel = strTemps.length - 1;
        
        // 计算相对路径需要退几级
        int iRelativeLevel = 0;
        iPos = strRelativePath.indexOf("..", 0);
        while (iPos != -1)
        {
            iRelativeLevel++;
            iPos = strRelativePath.indexOf("..", ++iPos);
        }
        
        // 如果相对路径需要回退，判断基础路径的最后一个字符是不是'\'，如果是，则需要删除
        if (iRelativeLevel > 0)
        {
            if (strBasePath.lastIndexOf("\\") == (strBasePath.length() - 1))
            {
                strBasePath = strBasePath.substring(0, strBasePath.length() - 1);
            }
        }
        
        // 如果需要退的层数大于基础地址的层数，说明传入的内容不正确
        if (iRelativeLevel >= iBasePathLevel)
        {
            return "";
        }
        
        // 按照相对路径回退的层数，把基础地址多余的部分截掉
        for (int i = 0; i < iRelativeLevel; i++)
        {
            iPos = strBasePath.lastIndexOf("\\");
            strBasePath = strBasePath.substring(0, iPos);
        }
        
        // 把相对路径多余的部分截掉
        iPos = strRelativePath.lastIndexOf("..");
        if (iPos != -1)
        {
            strRelativePath = strRelativePath.substring(iPos + 2);
        }
        
        return strBasePath + strRelativePath;
    }
    
    /**
     * 将非String类型转换成String类型
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String trans2String(Object o)
    {
        if (o == null)
        {
            return "";
        }
        
        return o.toString();
    }
    
    /**
     * 将String类型转换成int类型
     *
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static int transString2Int(String str)
    {
        return transString2Int(str, 0);
    }
    
    /**
     * 将String类型转换成int类型
     * 
     * @param str 需要转换的字符串
     * @param defaultValue 如果转换失败则返回默认值
     */
    public static int transString2Int(String str, int defaultValue)
    {
        if (isEmpty(str))
        {
            return defaultValue;
        }
        try
        {
            return Integer.valueOf(str);
        }
        catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }
    
    /**
     * 将String类型转换成Long类型
     * 
     * @param str 需要转换的字符串
     */
    public static Long transString2Long(String str)
    {
        return (long)transString2Int(str);
    }
    
    /**
     * 截取字符串
     * 
     * @param str 需要截取的字符串
     * @param num 截取位数
     * @return String
     */
    public static String substring(String str, int num)
    {
        if (str != null && str.length() > 0)
        {
            if (num > 0 && num < str.length())
            {
                return str.substring(0, num) + "...";
            }
            else
            {
                return str;
            }
        }
        else
        {
            return str;
        }
    }
    
    /**
     * 截取字符串
     * 
     * @param str
     * @param separator
     * @return [参数说明]
     * 
     * @exception throws
     * @see [类、类#方法、类#成员]
     */
    public static String subStringLocalPath(String str, String separator)
    {
        if (StringUtils.isEmpty(str))
        {
            return str;
        }
        if (StringUtils.isEmpty(separator))
        {
            return "";
        }
        
        int pos = str.lastIndexOf(separator);
        if (pos == -1 || pos == str.length() - separator.length())
        {
            return "";
        }
        else
        {
            str = str.substring(0, pos);
            System.out.println(str);
            if (StringUtils.isEmpty(str))
            {
                return null;
            }
            else
            {
                pos = str.lastIndexOf(separator);
                if (pos == -1 || pos == str.length() - separator.length())
                {
                    return "";
                }
                else
                {
                    return str.substring(pos + separator.length());
                }
            }
        }
        
    }
}
