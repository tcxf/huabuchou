package com.tcxf.hbc.common.util;

/**
 * 验证帮助类
 */
public class ValidateUtil {

	/**
	 * 校验是否为null或空字符串
	 * 或执行trim再进行比较
	 * @return true为空，false为不空
	 */
	public static boolean isEmpty(Object target) {
		return target == null || target.toString().trim().equals("");
	}

}

