package com.tcxf.hbc.common.util.exception;

/**
 * @author zhouyj
 * @date 2018/3/15
 */
public class CheckedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String errorMsg ;

    public CheckedException() {
    }

    public CheckedException(String message) {
        super(message);
        this.errorMsg = message;
    }

    public CheckedException(Throwable cause) {
        super(cause);
    }

    public CheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
