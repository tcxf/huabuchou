package com.tcxf.hbc.common.util.exception;

/**
 * @author zhouyj
 * @date 2018/3/15
 * 403 授权拒绝
 */
public class PigDeniedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PigDeniedException() {
    }

    public PigDeniedException(String message) {
        super(message);
    }

    public PigDeniedException(Throwable cause) {
        super(cause);
    }

    public PigDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public PigDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
