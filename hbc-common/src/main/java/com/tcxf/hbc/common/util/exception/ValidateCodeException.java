package com.tcxf.hbc.common.util.exception;

/**
 * @author zhouyj
 * @date 2018/3/15
 */
public class ValidateCodeException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -7285211528095468156L;

    public ValidateCodeException(String msg) {
        super(msg);
    }

}
