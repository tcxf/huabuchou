package com.tcxf.hbc.common.vo;

/**
 * @program: hbc
 * @description: 额度返回对象
 * @author: JinPeng
 * @create: 2018-07-23 19:01
 **/
public class BalanceVo {

    String subGrade;
    String sumbalance;

    public String getSubGrade() {
        return subGrade;
    }

    public void setSubGrade(String subGrade) {
        this.subGrade = subGrade;
    }

    public String getSumbalance() {
        return sumbalance;
    }

    public void setSumbalance(String sumbalance) {
        this.sumbalance = sumbalance;
    }
}


