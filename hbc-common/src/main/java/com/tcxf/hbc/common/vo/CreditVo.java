package com.tcxf.hbc.common.vo;

import java.io.Serializable;

public class CreditVo implements Serializable {

    private String mobile;
    private String token;
    private String idCard;
    private String userName;
    private String accountNO;
    //调运营商接口和银行卡验证次数
    private String count;
    //运营商类型
    private String serviceType;
    //短信验证码
    private String randomPassword;
    //图片验证码
    private String code;
    //服务密码
    private String serviceCode;
    //额度
    private String totalScore;
    //用户类型
    private String utype;
    //用户id
    private String uid;
    //账户id
    private String accountid;
    //运营商id
    private String oid;
    //商户id
    private String mid;
    //资金端id
    private String fid;
    //授信类型（1-消费者授信 2-商户授信）
    private String authType;
    //商业授信额度
    private String authMax;
    //商业授信分期
    private String totaltimes;
    //运营商登录标识，第一次登录参数为空，第二次登录值为：loginResultStatus
    private String loginResultStatus;

    //设备IP
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getLoginResultStatus() {
        return loginResultStatus;
    }

    public void setLoginResultStatus(String loginResultStatus) {
        this.loginResultStatus = loginResultStatus;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getAuthMax() {
        return authMax;
    }

    public void setAuthMax(String authMax) {
        this.authMax = authMax;
    }

    public String getTotaltimes() {
        return totaltimes;
    }

    public void setTotaltimes(String totaltimes) {
        this.totaltimes = totaltimes;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {

        return oid;

    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getUtype() {
        return utype;

    }

    public String getAuthType() {
        return authType;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setAccountNO(String accountNO) {
        this.accountNO = accountNO;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setRandomPassword(String randomPassword) {
        this.randomPassword = randomPassword;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getMobile() {
        return mobile;
    }

    public String getToken() {
        return token;
    }

    public String getIdCard() {
        return idCard;
    }

    public String getUserName() {
        return userName;
    }

    public String getAccountNO() {
        return accountNO;
    }

    public String getCount() {
        return count;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getRandomPassword() {
        return randomPassword;
    }

    public String getCode() {
        return code;
    }

    public String getServiceCode() {
        return serviceCode;
    }
}
