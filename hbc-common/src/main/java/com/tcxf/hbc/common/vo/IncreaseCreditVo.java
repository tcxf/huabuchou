package com.tcxf.hbc.common.vo;

import com.tcxf.hbc.common.entity.TbUpMoney;

import java.io.Serializable;
import java.util.List;

public class IncreaseCreditVo implements Serializable {

    public List<TbUpMoney> increaseCreditlist;

    //用户id
    public String uid;

    public String parentId;

    public String type;

    //姓名
    public String name;

    //手机号
    public String phone;

    //身份证
    public String idcard;

    //运营商id

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    //两种：1.emergencyContact（紧急联系人）2.qqOrWeChat（QQ/微信）
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<TbUpMoney> getIncreaseCreditlist() {
        return increaseCreditlist;
    }

    public void setIncreaseCreditlist(List<TbUpMoney> increaseCreditlist) {
        this.increaseCreditlist = increaseCreditlist;
    }
}
