package com.tcxf.hbc.common.vo;

import com.tcxf.hbc.common.entity.SysLog;

import java.io.Serializable;

/**
 * @author zhouyj
 * @date 2018/3/15
 */
public class LogVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private SysLog sysLog;
    private String token;

    public SysLog getSysLog() {
        return sysLog;
    }

    public void setSysLog(SysLog sysLog) {
        this.sysLog = sysLog;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
