package com.tcxf.hbc.common.vo;

/**
 * @Auther: liuxu
 * @Date: 2018/7/5 17:11
 * @Description: 快捷支付确认模块DTO
 */
public class PayagreeconfirmDto {

    /**
     * 订单号
     */
    private String orderid;

    /**
     * 快捷支付银行卡协议号
     */
    private String agreeid;

    /**
     * 短信验证
     */
    private String smscode;

    /**
     * 申请信息
     */
    private String thpinfo;


    /**
     * 用户id  ywt 新添加的参数  2018 8-11
     */
    private String uid;

    /**
     * 资金端id  ywt 新添加的参数 2018 8-11
     */
    private String fid;

    /**
     * 运营商id  ywt 新添加的参数 2018 8-11
     */
    private String oid;


    /**
     * 用户类型  ywt 新添加的参数 2018 8-11
     */
    private String utype;

    /**
     * 正常还款存在:未分期还款中tb_repayment_plan表中serialNo  ywt 新添加的参数 2018 8-11
     */
    private String serialNo;

    /**
     * 正常还款存在:未分期还款中tb_repayment_plan表中rd_id  ywt 新添加的参数 2018 8-11
     */
    private String rdId;


    /**
     * 通联返回的支付编号
     */
    private String trxid;


    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getRdId() {
        return rdId;
    }

    public void setRdId(String rdId) {
        this.rdId = rdId;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getSmscode() {
        return smscode;
    }

    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}
