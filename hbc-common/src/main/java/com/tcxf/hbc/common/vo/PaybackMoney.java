package com.tcxf.hbc.common.vo;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 14:16 2018/7/30
 */
public class PaybackMoney {
    private String planId;
    private String repaymentDate;
    private BigDecimal totalMoney;
    private String waitpay;
    private String splitType;

    public String getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(String repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getWaitpay() {
        return waitpay;
    }

    public void setWaitpay(String waitpay) {
        this.waitpay = waitpay;
    }

    public String getSplitType() {
        return splitType;
    }

    public void setSplitType(String splitType) {
        this.splitType = splitType;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }
}
