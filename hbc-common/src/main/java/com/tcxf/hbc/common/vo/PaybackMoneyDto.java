package com.tcxf.hbc.common.vo;

import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.common.entity.TbTradingDetail;

import java.util.List;

/**
 * @author YWT_tai
 * @Date :Created in 17:00 2018/7/30
 */
public class PaybackMoneyDto {
    private List<TbRepayLog>  PaybackMoney;
    private List<TbTradingDetail> TradingDetails;
    private String resultStr;
    private String rdid;
    private String uid;
    private String oid;
    private String fid;
    private String payWay;//支付方式 1:快捷支付 2;微信支付
    private String uType; //用户类型 1:消费者 2：商户
    private String contractId;//银行协议号

    public String getContractId() {
        return contractId;
    }
    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
    public List<TbTradingDetail> getTradingDetails() {
        return TradingDetails;
    }
    public void setTradingDetails(List<TbTradingDetail> tradingDetails) {
        TradingDetails = tradingDetails;
    }
    public String getFid() {
        return fid;
    }
    public void setFid(String fid) {
        this.fid = fid;
    }
    public String getuType() {
        return uType;
    }
    public void setuType(String uType) {
        this.uType = uType;
    }

    public String getPayWay() {
        return payWay;
    }
    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public List<TbRepayLog> getPaybackMoney() {
        return PaybackMoney;
    }

    public void setPaybackMoney(List<TbRepayLog> paybackMoney) {
        PaybackMoney = paybackMoney;
    }

    public String getRdid() {
        return rdid;
    }

    public void setRdid(String rdid) {
        this.rdid = rdid;
    }

    public String getResultStr() {
        return resultStr;
    }

    public void setResultStr(String resultStr) {
        this.resultStr = resultStr;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }
}
