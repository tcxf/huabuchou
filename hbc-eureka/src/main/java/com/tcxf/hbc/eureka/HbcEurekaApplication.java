package com.tcxf.hbc.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author zhouyj
 */
@EnableEurekaServer
@SpringBootApplication
public class HbcEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HbcEurekaApplication.class, args);
	}
}
