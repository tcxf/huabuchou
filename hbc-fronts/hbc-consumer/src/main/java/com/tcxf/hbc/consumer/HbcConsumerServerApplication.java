package com.tcxf.hbc.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhouyj
 *         EnableResourceServer
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(basePackages = {"com.tcxf.hbc.consumer", "com.tcxf.hbc.common"})
public class HbcConsumerServerApplication {

    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(HbcConsumerServerApplication.class, args);

    }
}
