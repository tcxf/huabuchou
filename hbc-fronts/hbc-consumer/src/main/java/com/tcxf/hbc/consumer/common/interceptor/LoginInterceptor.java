package com.tcxf.hbc.consumer.common.interceptor;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.interceptor.BaseInterceptor;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.HttpUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.consumer.common.token.TokenManager;
import com.tcxf.hbc.consumer.common.token.TokenModel;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.BaseService;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.util.ReturnUrlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(BaseInterceptor.class);


    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断方法是否需要进行登录验证
        BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        TokenManager  tokenManager = (TokenManager) factory.getBean("redisTokenManager");
        BaseService loin  = (BaseService)factory.getBean("baseService");
        LoginService loginService = loin.getLoginService();
        //如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();


        //方法级上是否需要登录验证，如果不需要登录验证,直接通过
        if (!ValidateUtil.isEmpty(method.getAnnotation(AuthorizationNO.class))) {
            return true;
        }
        //1 判断cookie 如果有cookie 则拿cooleie值进行Redis判断 否则需要获取微信openId 进行登录
        String redisKey = getCookieValue(request);
        if (ValidateUtil.isEmpty(redisKey)||!tokenManager.checkToken(new TokenModel(redisKey)))
        {
            //2、通过微信跳转 获取openId
            String basePath = request.getScheme() + "://"+request.getServerName() +
                    ":" + request.getServerPort() + request.getContextPath() + "/";
            //根据二级域名获取OOperaInfo
            String osn = request.getServerName();
            R<OOperaInfo> or = loginService.getOperaId(osn);
            if(or.getCode()== R.FAIL || ValidateUtil.isEmpty(or.getData())){
                throw  new CheckedException("未获取到运营商信息");
            }
            String queryurl = request.getQueryString();
            String parameter="";
            if (!ValidateUtil.isEmpty(queryurl)) {
                parameter += queryurl;
            }
            OOperaInfo oOperaInfo = or.getData();
            String redirectUrl = HttpUtil.buildSendRedUrl(basePath , oOperaInfo.getAppid());
            redirectUrl=redirectUrl.replace("redirectUrlValue", "cuser/interceptor");
            String returnUrl = getUrl(request);
            logger.error("获取到转发路径为="+returnUrl+" key="+ReturnUrlUtil.getLoginAfterKey(returnUrl.toString()));
            redirectUrl=redirectUrl.replace("statecode", ReturnUrlUtil.getLoginAfterKey(returnUrl.toString())+"_"+parameter);
            reDirect(request,response,redirectUrl);
            return false;
        }
        return true;
    }

    private String getUrl(HttpServletRequest request){
        String base = request.getContextPath()+"/";
        String url = request.getRequestURI();
        url = url.replaceFirst(base, "");
        return url;
    }

    private void reDirect(HttpServletRequest request, HttpServletResponse response,String redirectUrl) throws IOException {
        //获取当前请求的路径
        //如果request.getHeader("X-Requested-With") 返回的是"XMLHttpRequest"说明就是ajax请求，需要特殊处理
        if("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
            //告诉ajax我是重定向
            response.setHeader("REDIRECT", "REDIRECT");
            //告诉ajax我重定向的路径
            response.setHeader("CONTENTPATH", redirectUrl);
            //设置错误代码
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }else{
            response.sendRedirect(redirectUrl);
        }
    }

    /**
     * 获取cookie 值
     * @param request
     * @return
     */
    private String getCookieValue(HttpServletRequest request){
        Map<String,Cookie> map = ReadCookieMap(request);
        Cookie cookieValue = map.get(SessionDto.COOKIENAME);
        if(ValidateUtil.isEmpty(cookieValue)){
            return null;
        }
        return cookieValue.getValue();
    }

    private Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
        Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
        Cookie[] cookies = request.getCookies();
        if(null!=cookies){
            for(Cookie cookie : cookies){
                cookieMap.put(cookie.getName(), cookie);
            }
        }
        return cookieMap;
    }
}
