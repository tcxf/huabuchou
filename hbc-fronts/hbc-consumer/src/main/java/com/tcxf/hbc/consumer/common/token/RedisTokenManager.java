package com.tcxf.hbc.consumer.common.token;

import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.dto.SessionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RedisTokenManager implements TokenManager {

	/**
	 * 24小时
	 */
	public static final Long TIME = 86400L;

	/**
	 * 15分钟
	 */
	public static final Long TIME2 = 900L;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public TokenModel createToken(String userId,SessionDto sessionDto) {
		TokenModel model = new TokenModel(userId, sessionDto);
		//存储到redis并设置过期时间
		redisUtil.set(userId, sessionDto, TIME);
		return model;
	}

	@Override
	public TokenModel getToken(String userId) {
		if (ValidateUtil.isEmpty(userId)) {
			return null;
		}
		Object object = redisUtil.get(userId);
		if(ValidateUtil.isEmpty(object)){
			return null;
		}
		return new TokenModel(userId, (SessionDto) object);
	}

	@Override
	public boolean checkToken(TokenModel model) {
		if (ValidateUtil.isEmpty(model)) {
			return false;
		}
		Object value = redisUtil.get(model.getUserId());
		if (ValidateUtil.isEmpty(value)) {
			return false;
		}
		// 如果验证成功，说明此用户进行了一次有效操作，延长token的过期时间
		redisUtil.set(model.getUserId(),value, TIME);
		SessionDto sessionDto = (SessionDto) value;
		redisUtil.set(sessionDto.getSession_user_id(),model.getUserId(), TIME);
		return true;
	}

	@Override
	public void deleteToken(String userId) {
		redisUtil.remove(userId);
	}

	@Override
	public void refToken(String key,String value,Long expireTime){
		redisUtil.set(key,value,expireTime);
	}
}
