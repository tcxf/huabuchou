package com.tcxf.hbc.consumer.common.token;

import com.tcxf.hbc.consumer.dto.SessionDto;

public interface TokenManager {

	 /**
     * 创建一个token关联上指定用户
     */
    public TokenModel createToken(String userId, SessionDto sessionDto);

    /**
     * 检查token是否有效
     */
    public boolean checkToken(TokenModel model);

    /**
     * 从字符串中解析token
     */
    public TokenModel getToken(String userId);

    /**
     * 清除token
     */
    public void deleteToken(String userId);

    /**
     * 刷新token
     * @param key
     * @param value
     */
    public void refToken(String key ,String value,Long expireTime);

}

