package com.tcxf.hbc.consumer.common.token;

import com.tcxf.hbc.consumer.dto.SessionDto;

/**
 * Token的Model类，可以增加字段提高安全性，例如时间戳、url签名
 *
 */
public class TokenModel {

    //用户userId
    private String  userId;

    //存储value
    private SessionDto sessionDto;
    
    public TokenModel(String userId,SessionDto sessionDto) {
        this.sessionDto = sessionDto;
        this.userId = userId;
    }
    public TokenModel(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public SessionDto getSessionDto() {
        return sessionDto;
    }

    public void setSessionDto(SessionDto sessionDto) {
        this.sessionDto = sessionDto;
    }
}