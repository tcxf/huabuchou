package com.tcxf.hbc.consumer.controller.VipController;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.VipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/Vips")
public class VipController extends BaseController {
    @Autowired
    private VipService vipService;


    @Autowired
    private LoginService loginService;

    @RequestMapping("/findVip")
    public R findVip(HttpServletRequest request){
        R r=new R();
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String mid = userInfoDto.getmMerchantInfo().getId();
        String realName = request.getParameter("realName");
        if(realName==null || realName .equals("")){
            realName ="null";
        }
        Page vip = vipService.findVip(mid, realName);
        r.setData(vip);
        return r;
    }

    /**
     *
     * @return
     *
     */
    @RequestMapping("/qu")
    public ModelAndView quhy(){
        return  new ModelAndView("ftl/merchant/associator");
    }
}
