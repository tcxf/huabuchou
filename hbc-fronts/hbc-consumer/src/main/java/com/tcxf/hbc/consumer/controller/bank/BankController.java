package com.tcxf.hbc.consumer.controller.bank;

import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.BankSercice;
import com.tcxf.hbc.consumer.feign.BillAdvanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/bank")
public class BankController extends BaseController {

    @Autowired
    private BankSercice bankSercice;

    @Autowired
    private BillAdvanceService billAdvanceService;

    /**
     * 跳转银行卡页面
     * @return
     */
    @GetMapping("/gobank")
    public ModelAndView gobank(){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/cardList");
        return m;
    }
    /**
     * 查询所有绑卡记录
     * @param request
     * @return
     */
    @RequestMapping("/findbank")
    public R findbank(HttpServletRequest request){
        R r = new R<>();
        SessionDto sessionDto = getUserSession();
        R findbank = bankSercice.findbank(sessionDto.getSession_account_id(),TbBindCardInfo.CONTRACTTYPE_1);
        return findbank;
    }

    /**
     * 通过绑卡表ID删除绑卡
     * @param request
     * @return
     */
    @RequestMapping("/dbank")
    public R dbank(HttpServletRequest request){
        R r = bankSercice.delbank(request.getParameter("bindid"));
        return r;
    }

    /**
     * 跳转绑卡页面
     * @return
     */
    @GetMapping("/savebank")
    public ModelAndView savebank(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        String bk = request.getParameter("bk");
        if (bk == null){
            m.addObject("bk","");
        }else {
            m.addObject("bk",bk);
        }
        m.setViewName("ftl/newCard");
        return m;
    }

    /**
     * 绑卡
     * @return
     */
    @RequestMapping("/save")
    public R save(HttpServletRequest request){
        R<Object> r = new R<>();
        SessionDto sessionDto = getUserSession();
        TbBindCardInfo info = new TbBindCardInfo();
        info.setOid(sessionDto.getSession_account_id());
        info.setMobile(request.getParameter("mobile"));
        info.setBankName( request.getParameter("bankName"));
        info.setBankCard(request.getParameter("cardNo"));
        info.setIdCard(request.getParameter("idCard"));
        info.setName(request.getParameter("realName"));
        info.setContractType(TbBindCardInfo.CONTRACTTYPE_1);
       r = bankSercice.savebank(info);
        return r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:15 2018/8/8
     * 查询该用户的所有快键银行卡
    */
    @RequestMapping("/findQuickBankCard")
    public R findQuickBankCard(){
        SessionDto sessionDto = getUserSession();
        R findbank = bankSercice.findbank(sessionDto.getSession_account_id(),TbBindCardInfo.CONTRACTTYPE_2);
        return findbank;
    }





}
