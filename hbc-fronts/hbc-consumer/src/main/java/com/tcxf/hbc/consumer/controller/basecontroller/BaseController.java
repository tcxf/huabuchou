package com.tcxf.hbc.consumer.controller.basecontroller;

import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.common.token.RedisTokenManager;
import com.tcxf.hbc.consumer.common.token.RedisUtil;
import com.tcxf.hbc.consumer.common.token.TokenManager;
import com.tcxf.hbc.consumer.common.token.TokenModel;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseController {

	protected Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());


	@Autowired
	TokenManager tokenManager;
	@Autowired
	private RedisUtil redisUtil;

	protected  void createSession(UserInfoDto userInfoDto,String operId){
	    //设置redis缓存
        SessionDto sessionDto = new SessionDto();
        sessionDto.setSession_account_id(userInfoDto.getTbUserAccount().getId());
        sessionDto.setSession_user_id(userInfoDto.getcUserInfo().getId());
        sessionDto.setSession_openId(userInfoDto.getcUserInfo().getWxOpenid());
        sessionDto.setSession_opera_id(operId);
        sessionDto.setSession_m_id(!ValidateUtil.isEmpty(userInfoDto.getmMerchantInfo())?userInfoDto.getmMerchantInfo().getId():null);
        //用户ID key记录用户是否在线
		String key = userInfoDto.getcUserInfo().getId();
		//用户登录sessionID key记录是否已经登录
		String sessionKey = "login_"+StringUtil.RadomCode(10)+"_"+key;
		//检查是否存在该用户的登录，如果存在则剔除掉
		Object value = redisUtil.get(key);
		if(!ValidateUtil.isEmpty(value)){
			//删除该用户对应的登录token
			redisUtil.remove(value.toString());
			//剔除在线
			redisUtil.remove(key);
		}
		//创建登录token
        tokenManager.createToken(sessionKey,sessionDto);
		//创建在线记录
		redisUtil.set(key,sessionKey,RedisTokenManager.TIME);
        //添加cookie
		Cookie cookie = new Cookie(SessionDto.COOKIENAME,sessionKey);
		cookie.setMaxAge(30 * 60);// 设置为30min
		cookie.setPath("/");
		getResponse().addCookie(cookie);
	}

	protected void deleteToken(){
		//设置redis缓存
		Map<String,Cookie> map = readCookieMap();
		Cookie cookieValue = map.get(SessionDto.COOKIENAME);
		if(ValidateUtil.isEmpty(cookieValue)){
			return;
		}
		tokenManager.deleteToken(cookieValue.getValue());

		Object value = redisUtil.get(cookieValue.getValue());
		if (ValidateUtil.isEmpty(value)) {
			return;
		}
		SessionDto sessionDto = (SessionDto) value;
		redisUtil.remove(sessionDto.getSession_user_id());
	}

	/**
	 * @Description:  刷新登录token（立即开店时 需要将MId放入session中）
	 * @Param: []
	 * @return: void
	 * @Author: JinPeng
	 * @Date: 2018/8/22
	*/
	protected void refreshToken(SessionDto sessionDto){
		Map<String,Cookie> map = readCookieMap();
		Cookie cookieValue = map.get(SessionDto.COOKIENAME);
		if(ValidateUtil.isEmpty(cookieValue)){
			return;
		}
		tokenManager.createToken(cookieValue.getValue(),sessionDto);
	}

	private  Map<String,Cookie> readCookieMap(){
		Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
		Cookie[] cookies = getRequest().getCookies();
		if(null!=cookies){
			for(Cookie cookie : cookies){
				cookieMap.put(cookie.getName(), cookie);
			}
		}
		return cookieMap;
	}

	protected  SessionDto getUserSession(){
		Map<String,Cookie> map = readCookieMap();
		Cookie cookieValue = map.get(SessionDto.COOKIENAME);
		if(ValidateUtil.isEmpty(cookieValue)){
			return null;
		}
		TokenModel tokenModel = tokenManager.getToken(cookieValue.getValue());
		if(ValidateUtil.isEmpty(tokenModel)){
			return null;
		}
		return tokenModel.getSessionDto();
	}

	protected  String getSessionId() {
		return getRequest().getSession().getId();
	}
	/**
	 * 获得respgeonse
	 * @return
	 */
	protected  HttpServletResponse getResponse(){
		ServletRequestAttributes  requestAttributes  = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(ValidateUtil.isEmpty(requestAttributes)){
			return null;
		}
		return requestAttributes.getResponse();
	}

	/**
	 * 获得request
	 * @return
	 */
	protected  HttpServletRequest getRequest(){
		ServletRequestAttributes  requestAttributes  = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(ValidateUtil.isEmpty(requestAttributes)){
			return null;
		}
		return requestAttributes.getRequest();  
	}
}
