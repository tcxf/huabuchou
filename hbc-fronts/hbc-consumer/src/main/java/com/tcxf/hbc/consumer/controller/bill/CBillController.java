package com.tcxf.hbc.consumer.controller.bill;

import com.fasterxml.jackson.databind.JavaType;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.config.PayUrlPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonUtils;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.secure.Base64;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.vo.MycreditDetailsDto;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.consumer.common.token.RedisTokenManager;
import com.tcxf.hbc.consumer.common.token.TokenManager;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.controller.index.IndexInfoController;
import com.tcxf.hbc.consumer.dto.QuickPayPayagreeconfirmResData;
import com.tcxf.hbc.consumer.dto.NearbyMerchantDto;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author YWT_tai
 * @Date :Created in 20:23 2018/7/12
 */

@RestController
@RequestMapping("consumer/bill")
public class CBillController extends BaseController {
    @Autowired
    PayUrlPropertiesConfig payUrlPropertiesConfig;
    @Autowired
    private CBillService cBillService;

    @Autowired
    private EmpoService empoService;

    @Autowired
    private BillAdvanceService billAdvanceService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private CMerchantInfoService cMerchantInfoService;

    @Autowired
    private IndexInfoController indexInfoController;


//    String uid = "4";
//    String oid = "10148090729809469455";


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 20:21 2018/7/12
     * 跳转授信额度页面
     */
    @RequestMapping("/jumpMyCredit")
//    @AuthorizationNO
    public ModelAndView jumpMyCredit(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();

        try {
            SessionDto sessionDto = getUserSession();
            CreditVo creditVo = new CreditVo();
            if (!ValidateUtil.isEmpty(sessionDto.getSession_user_id())){
                creditVo.setUid(sessionDto.getSession_user_id());
            }else if (!ValidateUtil.isEmpty(request.getParameter("uId"))){
                creditVo.setUid(request.getParameter("uId"));
            }
            if (!ValidateUtil.isEmpty(sessionDto.getSession_opera_id())){
                creditVo.setOid(sessionDto.getSession_opera_id());
            }else if (!ValidateUtil.isEmpty(request.getParameter("oId"))){
                creditVo.setOid(request.getParameter("oId"));
            }

            R<CUserRefCon> r = empoService.getTbAccountInfo(creditVo);
            R<CUserInfo> r1 = empoService.getUserInfo(creditVo);
            R<TbBusinessCredit> r2 = empoService.getTbBusinessCreditByUid(creditVo);
            if (!ValidateUtil.isEmpty(sessionDto.getSession_m_id())){
                R<NearbyMerchantDto> r3 = cMerchantInfoService.QueryMerchantInfo(sessionDto.getSession_m_id());
                mode.addObject("authExamine", r3.getData().getAuthExamine());
            }


            CUserRefCon cUserRefCon = r.getData();
            String authStatus = cUserRefCon.getAuthStatus();
            if("0".equals(authStatus)){
//            //此处是区分用户类型，1.用户 2.商户
//            if ("1".equals(r1.getData().getUserAttribute())){
//                mode.setViewName("ftl/empo/fastEmpo");
//            }else {

                //获取资金端的授信余额详情
                R<FMoneyInfo> r4 = empoService.getFMoneyInfoByFid(creditVo);

                if (!ValidateUtil.isEmpty(r4.getData()) && "0".equals(r4.getData().getStatus())) {              //资金端未采用限额授信
                    mode.setViewName("ftl/empo/fastEmpoProtocol");
                    mode.addObject("userAttribute", r1.getData().getUserAttribute());
                }else if (!ValidateUtil.isEmpty(r4.getData()) && "1".equals(r4.getData().getStatus())){         //资金端采用限额授信
                    //获取资金端已使用授信额度
                    R<BigDecimal> r3 = empoService.getFMoneyOtherInfoByFid(creditVo);

                    if (!ValidateUtil.isEmpty(r3.getData())) {
                        //当日授信额度减去已授信额度
                        BigDecimal b1 = r4.getData().getDayMoney().subtract(r3.getData());

                        int i1 = b1.compareTo(new BigDecimal("0"));
                        int i2 = b1.compareTo(new BigDecimal("1500"));

                        mode.addObject("type", request.getParameter("type"));
                        mode.addObject("state", request.getParameter("state"));
                        String payPath = payUrlPropertiesConfig.getGotopayurl() + "/payment/";
                        logger.info("****************payPath:{}", payPath);
                        mode.addObject("payPath", payPath);
                        mode.addObject("openId", request.getParameter("openId"));
                        //判断当日授信剩余额度是否满足授信条件
                        if ((i1 == 1 || i1 == 0) && i2 == -1) {          //当日剩余授信额度无法满足授信
                            mode.setViewName("ftl/empo/lackOfCredit");
                        } else {                                         //当日剩余授信额度可以满足授信
                            mode.setViewName("ftl/empo/fastEmpoProtocol");
                            mode.addObject("userAttribute", r1.getData().getUserAttribute());
                        }
                    }
                }
//            }
            }else {
                mode.setViewName("ftl/mycredit");
                mode.addObject("authStatus", authStatus);
                mode.addObject("userAttribute", r1.getData().getUserAttribute());
                if (ValidateUtil.isEmpty(r2.getData())){
                    mode.addObject("status", "3");
                }else {
                    mode.addObject("status", r2.getData().getStatus());
                }
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  mode;
    }



    /**
     * @Description:  进入授信额度页面
     * @Param: []
     * @return: org.springframework.web.servlet.ModelAndView
     * @Author: JinPeng
     * @Date: 2018/8/1
    */
    @RequestMapping("/jumpCreditView")
    public ModelAndView jumpCreditView(){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        CreditVo creditVo = new CreditVo();
        creditVo.setUid(uid);
        creditVo.setOid(oid);
        R<MycreditDetailsDto> r = empoService.getMycreditDetails(creditVo);
        MycreditDetailsDto mycreditDetailsDto = r.getData();
        mode.addObject("mycreditDetailsDto", mycreditDetailsDto);
        mode.setViewName("ftl/empo/mycreditDetails");
        return  mode;
    }

    /**
     * @Description:  进入商业授信页面
     * @Param: []
     * @return: org.springframework.web.servlet.ModelAndView
     * @Author: JinPeng
     * @Date: 2018/8/1
     */
    @RequestMapping("/jumpBusinessCreditView")
    public ModelAndView jumpBusinessCreditView(){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        CreditVo creditVo = new CreditVo();
        creditVo.setUid(uid);
        creditVo.setOid(oid);
        R<TbBusinessCredit> r2 = empoService.getTbBusinessCreditByUid(creditVo);
        if (ValidateUtil.isEmpty(r2.getData())){
            mode.setViewName("ftl/empo/businessCreditView");
        }else {
            if ("1".equals(r2.getData().getStatus())){
                R<HashMap<String, Object>> map = empoService.getTbBusinessCreditDetailsByUid(creditVo);
                if (!ValidateUtil.isEmpty(map.getData())){
                    mode.addObject("authmax", map.getData().get("authmax"));
                    mode.addObject("totaltimes", map.getData().get("totaltimes"));
                    mode.addObject("endmoeny", map.getData().get("endmoeny"));
                }
                mode.setViewName("ftl/empo/businessCreditDetails");
            }else if ("2".equals(r2.getData().getStatus())){
                mode.setViewName("ftl/empo/businessCreditAudit");
            }
        }
        return  mode;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 20:32 2018/7/12
     * 查询授信额度信息
     */
    @RequestMapping("/findMyCreditInfo")
//    @AuthorizationNO
    public R<HashMap<String, Object>> findMyCreditInfo(){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
//        CreditVo creditVo = new CreditVo();
//        creditVo.setUid(uid);
//        creditVo.setOid(oid);
        return cBillService.findAvailableCreditInfo(uid,oid);
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 20:21 2018/7/12
     * 跳转我的带还账单
     */
    @RequestMapping("/jumpMineWaitPay")
    public ModelAndView jumpMineWaitPay(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/bill/repayment");
        return  mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 20:32 2018/7/12
     * 查询待还账单信息
    */
    @RequestMapping("/findMineWaitPayInfo")
    public R findMineWaitPayInfo(){
      /*  String uid="2";
        String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findMineWaitPayInfo(uid,oid);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:29 2018/7/13
     * 跳转待还分期详情
     * (waitpay 0:未还 = 滞纳金+利息+本金 1:待还=利息+本金)
    */
    @GetMapping("/jumpPlanDetailInfo")
    public ModelAndView jumpPlanDetailInfo(String waitpay,String planId,String splitType){
        ModelAndView mode = new ModelAndView();
        mode.addObject("waitpay",waitpay);
        mode.addObject("planId",planId);
        mode.addObject("splitType",splitType);
        mode.setViewName("ftl/bill/repaymentDetail"); //跳转交易记录页面
        return  mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:32 2018/7/13
     * 跳转分期详情页面(待还/逾期)
    */
    @RequestMapping("/findPlanDetailInfo")
    public R<Map<String,Object>> findPlanDetailInfo(String planId,String waitpay){
        return cBillService.findPlanDetailInfo(planId,waitpay);
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 20:21 2018/7/12
     * 跳转未出账单页面
     */
    @RequestMapping("/jumpUnOutBill")
    public ModelAndView jumpUnOutBill(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/bill/unout");
        return  mode;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 19:19 2018/7/16
     *查询未出账单页面接口
     */
    @RequestMapping("/findUnOutBill")
    public R findUnOutBill(){
          /*  String uid="3";
            String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findUnOutBill(uid,oid);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:56 2018/7/17
     * 跳转分期明细  id:还款表主键id
    */
    @RequestMapping("/jumpDivideDetail")
    public ModelAndView jumpDivideDetail(String id){
        ModelAndView mode = new ModelAndView();
        mode.addObject("id",id);
        mode.setViewName("ftl/bill/devideDetail");
        return  mode;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 8:56 2018/7/17
     * 分期明细 渲染数据 id:还款表主键id
     */
    @RequestMapping("/findDivideDetail")
    public R findDivideDetail(String id){
        return cBillService.findDivideDetail(id);
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 16:32 2018/7/13
     * 查询交易记录详情
     */
    @RequestMapping("/findTradeDetailInfo")
    public R findTradeDetailInfo(String id){
        return cBillService.findTradeDetailInfo(id);
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:23 2018/7/19
    */
    @RequestMapping("/jumpPlanYears")
    public ModelAndView jumpPlanYears(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/bill/planbillList");
        return  mode;
    }



    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:23 2018/7/19
     * 跳转历史账单年分
     */
    @RequestMapping("/jumpHistoryYears")
    public ModelAndView jumpHistoryYears(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/bill/historybillList");
        return  mode;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:23 2018/7/19
     * 跳转历史账单详情
     */
    @RequestMapping("/jumpHistoryBillDetailInfo")
    public ModelAndView jumpHistoryBillDetailInfo(String id,String isSplit){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/bill/historybillListDetailInfo");
        mode.addObject("id",id);
        mode.addObject("isSplit",isSplit);
        return  mode;
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:57 2018/7/19
     * 查询分期账单的年份
    */
    @RequestMapping("/findPlanYears")
    public R findPlanYears(){
         /*   String uid="2";
            String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findPlanYears(uid,oid);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:29 2018/7/19
     * 根据年分查询每月的分期
    */
    @PostMapping("/findPlanBillByYear")
    public R findPlanBillByYear(String dateYear){
        /* String uid="2";
         String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findPlanBillByYear(dateYear,uid,oid);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:32 2018/7/19
     * 查询历史账单的年份
    */
    @RequestMapping("/findHistoryBillYears")
    public R findHistoryBillYears(){
         /*    String uid = "4";
        String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findHistoryBillYears(uid,oid);
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:29 2018/7/19
     * 根据年分查询历史账单每年的月份情况
     */
    @RequestMapping("/findHistoryBillByYear")
    public R findHistoryBillByYear(String dateYear){
      /*  String uid="4";
        String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findHistoryBillByYear(dateYear,uid,oid);
    }


    /**c_user_ref_con
    * @Author:YWT_tai
    * @Description
    * @Date: 14:25 2018/7/19
     * 查询历史账单详情(本月交易记录+分期+提前还款) id:还款表主键id 没有id则都是分期   分期 isSplit： 1 分期   0 未分期  "" :都是提前还款
    */
    @RequestMapping("/findHistoryBillDetailInfo")
    public R findHistoryBillDetailInfo(String isSplit,String id){
       /* String uid="4";
        String oid="1014809072980946945";*/
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findHistoryBillDetailInfo(isSplit,uid,id,oid);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:39 2018/7/31
    */
    @RequestMapping("/jumpPaybackMoney")
    public ModelAndView jumpPaybackMoney(String payList,String rdid){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        ModelAndView mode = new ModelAndView();
        List<TbRepayLog> listPaybacks = new ArrayList<TbRepayLog>();
        BigDecimal totalMoney = new BigDecimal("0");
        BigDecimal unPlanTotalMoney = new BigDecimal("0");
        BigDecimal planTotalMoney = new BigDecimal("0");
        if (!"0".equals(payList)){
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbRepayLog.class);
           listPaybacks = (List<TbRepayLog>) JsonUtils.fromJson(payList, javaType);
        }
        if(!"0".equals(rdid)){
            R<TbRepaymentPlan> r=billAdvanceService.findRepaymentPlanByRdId(rdid);
            unPlanTotalMoney = r.getData().getTotalCorpus();
        }
        if(listPaybacks.size()>0){
            for (TbRepayLog tbRepayLog:listPaybacks) {
                planTotalMoney = tbRepayLog.getAmount().add(planTotalMoney);
            }
        }
        totalMoney=planTotalMoney.add(unPlanTotalMoney);
        mode.addObject("payList",payList);
        mode.addObject("rdid",rdid);
        mode.addObject("uid",uid);
        mode.addObject("amount",totalMoney.toString());
        mode.setViewName("ftl/bill/selectPayWay");
        return mode;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:39 2018/7/31
     */
    @RequestMapping("/jumpJmPaybackMoney")
    public ModelAndView jumpJmPaybackMoney(String payList,String rdid){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        ModelAndView mode = new ModelAndView();
        List<TbRepayLog> listPaybacks = new ArrayList<TbRepayLog>();
        BigDecimal totalMoney = new BigDecimal("0");
        BigDecimal unPlanTotalMoney = new BigDecimal("0");
        BigDecimal planTotalMoney = new BigDecimal("0");
        payList = new String( Base64.decode(payList));
        if (!"0".equals(payList)){
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbRepayLog.class);
            listPaybacks = (List<TbRepayLog>) JsonUtils.fromJson(payList, javaType);
        }
        if(!"0".equals(rdid)){
            R<TbRepaymentPlan> r=billAdvanceService.findRepaymentPlanByRdId(rdid);
            unPlanTotalMoney = r.getData().getTotalCorpus();
        }
        if(listPaybacks.size()>0){
            for (TbRepayLog tbRepayLog:listPaybacks) {
                planTotalMoney = tbRepayLog.getAmount().add(planTotalMoney);
            }
        }
        totalMoney=planTotalMoney.add(unPlanTotalMoney);
        mode.addObject("payList",payList);
        mode.addObject("rdid",rdid);
        mode.addObject("uid",uid);
        mode.addObject("amount",totalMoney);
        mode.setViewName("ftl/bill/selectPayWay");
        return mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:39 2018/7/31
     * 跳转支付成功页面
    */
    @RequestMapping("/jumpPaySuccess")
    public ModelAndView jumpPaySuccess(String trxstatus){
        ModelAndView mode = new ModelAndView();
        if(trxstatus!=null && "0000".equals(trxstatus)){
            mode.setViewName("ftl/bill/paySuccess");
        }else{
            mode.setViewName("ftl/bill/waitDealpay");
        }
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:39 2018/7/31
     * 跳转提前还款支付成功页面
    */
    @RequestMapping("/jumpAdvancePaySuccess")
    public ModelAndView jumpAdvancePaySuccess(String trxstatus){
        ModelAndView mode = new ModelAndView();
        if(trxstatus!=null && "0000".equals(trxstatus)){
            mode.setViewName("ftl/bill/advancePaySuccess");
        }else{
            mode.setViewName("ftl/bill/advanceWaitDealPay");
        }
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:39 2018/7/31
     * 查询提前还款的总金额
    */
    @RequestMapping("/jumpAdvancePaybackMoney")
    public ModelAndView jumpAdvancePaybackMoney(){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        R<Map<String, Object>> r= cBillService.findAdvancePaybackTotalMoney(uid,oid);
        mode.addObject("totalMoney",r.getData().get("totalMoney"));
        mode.addObject("uid",uid);
        mode.setViewName("ftl/bill/AdvanceSelectPayWay");
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:44 2018/8/11
     * 跳转绑卡页面 旭
    */
    @RequestMapping("/goToBindCard")
    public ModelAndView goToBindCard(HttpServletRequest request, HttpServletResponse response){
        String userId = request.getParameter("uid");
        return new ModelAndView("redirect:" + payUrlPropertiesConfig.getGotopayurl() + "/payment/bindCard/goToBindCard?userId="+userId+"&urlType=2");
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:44 2018/8/11
     * 跳转绑卡页面 旭
     */
    @RequestMapping("/goToYcBindCard")
    public ModelAndView goToYcBindCard(HttpServletRequest request, HttpServletResponse response){
        String userId = request.getParameter("uid");
        String payList = request.getParameter("payList");
        String encode = Base64.encode(payList.getBytes());
        String rdid = request.getParameter("rdid");
        return new ModelAndView("redirect:" + payUrlPropertiesConfig.getGotopayurl() + "/payment/bindCard/goToBindCard?userId="+userId+"&urlType=3"+"&rdid="+rdid+"&payList="+encode);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 19:39 2018/8/10
     * 调支付输入验证码
    */
    @RequestMapping("/inputAdvancePayMsgCode")
    public ModelAndView inputAdvancePayMsgCode(String agreeid,String orderid,String thpinfo){
        //需要穿参数带到当前页面
        ModelAndView mode = new ModelAndView();
        mode.addObject("agreeid",agreeid);
        mode.addObject("orderid",orderid);
        mode.addObject("thpinfo",thpinfo);
        mode.setViewName("ftl/bill/confirmAdvanceAgreePay");
        return mode;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 19:39 2018/8/10
     * 正常还款调支付输入验证码
     */
    @RequestMapping("/inputNormalPayMsgCode")
    public ModelAndView inputNormalPayMsgCode(String agreeid,String orderid,String thpinfo,String rdId,String serialNo){
        //需要穿参数带到当前页面
        ModelAndView mode = new ModelAndView();
        mode.addObject("agreeid",agreeid);
        mode.addObject("orderid",orderid);
        mode.addObject("thpinfo",thpinfo);
        mode.addObject("rdId",rdId);
        mode.addObject("serialNo",serialNo);
        mode.setViewName("ftl/bill/confirmPay");
        return mode;
    }



    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:39 2018/7/31
     * 查询所有提前还款的交易记录
     */
    @RequestMapping("/findTradingList")
    public R<Map<String, Object>> findTradingList(){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        return cBillService.findAdvancePaybackTotalMoney(uid,oid);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:49 2018/7/30
     * 正常还款插入最初数据
    */
    @RequestMapping("/PaybackMoney")
    public R PaybackMoney(String payList,String rdid,String payWay,String contractId){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        String uType=""; //用户类型 1:消费者 2：商户
        List<TbRepayLog> listPaybacks = new ArrayList<>();
        if (!"0".equals(payList)){
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbRepayLog.class);
            listPaybacks = (List<TbRepayLog>) JsonUtils.fromJson(payList, javaType);
        }
        R<UserInfoDto> r = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = r.getData();
        if(ValidateUtil.isEmpty(userInfoDto.getmMerchantInfo())){
            uType="1"; //消费者
        }else{
            uType="2"; // 商户
        }
        String fid = userInfoDto.getcUserRefCon().getFid(); //资金端id
        PaybackMoneyDto paybackMoneyDto = new PaybackMoneyDto();
        paybackMoneyDto.setPaybackMoney(listPaybacks);
        paybackMoneyDto.setOid(oid);
        paybackMoneyDto.setUid(uid);
        paybackMoneyDto.setFid(fid);
        paybackMoneyDto.setRdid(rdid);
        paybackMoneyDto.setPayWay(payWay);
        paybackMoneyDto.setuType(uType);
        paybackMoneyDto.setContractId(contractId);
        return billAdvanceService.PaybackMoney(paybackMoneyDto);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:40 2018/8/11
     * 提前还款插入repay表最初数据
    */
    @RequestMapping("/AdvancePaybackMoney")
    public R AdvancePaybackMoney(String tradeList,String payWay,String contractId){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        String uType=""; //用户类型 1:消费者 2：商户
        JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbTradingDetail.class);
        List<TbTradingDetail>  listTradings = (List<TbTradingDetail>) JsonUtils.fromJson(tradeList, javaType);
        R<UserInfoDto> r = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = r.getData();
        if(ValidateUtil.isEmpty(userInfoDto.getmMerchantInfo())){
            uType="1"; //消费者
        }else{
            uType="2"; // 商户
        }
        String fid = userInfoDto.getcUserRefCon().getFid(); //资金端id
        PaybackMoneyDto paybackMoneyDto = new PaybackMoneyDto();
        paybackMoneyDto.setTradingDetails(listTradings);
        paybackMoneyDto.setOid(oid);
        paybackMoneyDto.setUid(uid);
        paybackMoneyDto.setFid(fid);
        paybackMoneyDto.setPayWay(payWay);
        paybackMoneyDto.setuType(uType);
        paybackMoneyDto.setContractId(contractId);
        return billAdvanceService.AdvancePaybackMoney(paybackMoneyDto);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 21:52 2018/8/10
     * 确认支付
    */
    @RequestMapping("/confirmAdvancePay")
    public R confirmAdvancePay(String agreeid,String orderid,String thpinfo,String smscode,String tokenKey) {
        PayagreeconfirmDto payagreeconfirmDto = new PayagreeconfirmDto();
        payagreeconfirmDto.setAgreeid(agreeid);
        payagreeconfirmDto.setOrderid(orderid);
        payagreeconfirmDto.setThpinfo(thpinfo);
        payagreeconfirmDto.setSmscode(smscode);
        R<QuickPayPayagreeconfirmResData> returnR = billAdvanceService.confirmAdvanceOrAlreadyBillPay(payagreeconfirmDto);
        if (returnR.getCode() == R.FAIL) {
            return returnR;
        }
        SessionDto sessionDto = getUserSession();
        QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData = returnR.getData();
        if (quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000) ||
                quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)) {
            tokenManager.refToken(tokenKey + sessionDto.getSession_user_id(), "tbRepay",RedisTokenManager.TIME2);
        }
        return returnR;
    }



    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:05 2018/8/11
     * 账单还款回调成功 修改记录
     */
    @RequestMapping("/updateNormalPay")
    @AuthorizationNO
    public void updateNormalPay(HttpServletRequest request,HttpServletResponse response){
        try {
            logger.info("正常还款回调进进入=====================>");
            request.setCharacterEncoding("gbk");//通知传输的编码为GBK
            response.setCharacterEncoding("gbk");
            TreeMap<String, String> paras =  getParams(request);
            R r = billAdvanceService.updateNormalPay(paras);
            if(r.getCode()==1){
                response.getOutputStream().write("success".getBytes());
                response.flushBuffer();
            }
        } catch (IOException e) {
            logger.error("支付错误!",e);
        }
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:05 2018/8/11
     * 提前还款回调更新记录
    */
    @RequestMapping("/updateTradingInfo")
    @AuthorizationNO
    public void updateTradingInfo(HttpServletRequest request,HttpServletResponse response){
        try {
            request.setCharacterEncoding("gbk");//通知传输的编码为GBK
            response.setCharacterEncoding("gbk");
            TreeMap<String, String> paras =  getParams(request);
            R r = billAdvanceService.updateTradingInfo(paras);
            if(r.getCode()==1){
                response.getOutputStream().write("success".getBytes());
                response.flushBuffer();
            }
        } catch (IOException e) {
            logger.error("支付错误!",e);
        }
    }

    /**
     * 动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
     *
     * @param request
     * @return
     */
    private TreeMap<String, String> getParams(HttpServletRequest request) {
        TreeMap<String, String> map = new TreeMap<String, String>();
        Map reqMap = request.getParameterMap();
        for (Object key : reqMap.keySet()) {
            String value = ((String[]) reqMap.get(key))[0];
            System.out.println(key + ";" + value);
            map.put(key.toString(), value);
        }
        return map;
    }


}
