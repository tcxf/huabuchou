package com.tcxf.hbc.consumer.controller.coupon;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.MCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/user/Mcoupon")
public class MCouponController extends BaseController {

    @Autowired
    MCouponService mCouponService;

    @Autowired
    LoginService loginService;


    /**
     * 跳转查看优惠券页面
     * @return
     */
    @RequestMapping(value = "/couponFind")
    public ModelAndView couponFind(){
        SessionDto sessionDto = getUserSession();
        ModelAndView mode = new ModelAndView();
        if(!ValidateUtil.isEmpty(sessionDto)){
            R<UserInfoDto> dto = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
            UserInfoDto d = dto.getData();
            d.getmMerchantInfo().getId();
            String type = mCouponService.mtype(d.getmMerchantInfo().getId());
            mode.addObject("type", type);
        }
        mode.setViewName("ftl/merchant/couponRedact");
        return  mode;
    }


    /**
     *查询商户下的优惠券
     * @return
     */
    @RequestMapping(value="/couponList",method ={RequestMethod.POST} )
    //@RequestMapping("/couponList")
    public R couponList(){
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
        UserInfoDto d =dto.getData();
        return mCouponService.couponList( d.getmMerchantInfo().getId());
    }

    /**
     * 跳转到添加优惠券页面
     * @return
     */
    @RequestMapping(value = "/couponAdd" ,method = RequestMethod.GET)
    public ModelAndView couponAdd(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/merchant/couponNew");
        return  mode;
    }

    /**
     * 创建优惠券
     * @param mRedPacketSetting
     * @return
     */
    @RequestMapping(value = "/couponInsert", method ={RequestMethod.POST})
    public R couponInsert(HttpServletRequest request, MRedPacketSetting mRedPacketSetting){
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
        UserInfoDto d =dto.getData();
        d.getmMerchantInfo().getId();
        mRedPacketSetting.setModifyDate(mRedPacketSetting.getEndDate());
        Date date = new Date();
        if(mRedPacketSetting.getTimeType()!=2) {
            if (mRedPacketSetting.getEndDate().getTime() - date.getTime() < 0) {
                R r = new R();
                r.setMsg("您添加的优惠券已过期，请重新选择结束时间");
                r.setCode(1);
                return r;
            }
        }

        //DateUtil.stringDate(mRedPacketSetting.getEndDate())
        mRedPacketSetting.setMiId( d.getmMerchantInfo().getId());
        return mCouponService.couponInsert(mRedPacketSetting);
    }



    /**
     * 跳转到编辑优惠券页面
     * @param id
     * @return
     */
    @AuthorizationNO
    @RequestMapping(value = "/couponUpdate",method = {RequestMethod.GET})
    public ModelAndView couponUpdate(String id){
        ModelAndView mode = new ModelAndView();
        R r = mCouponService.couponUpdate(id);
        mode.addObject("entity",r.getData());
        mode.setViewName("ftl/merchant/couponUpdate");
        return  mode;
    }

    /**
     *
     * 修改优惠券
     * @param mRedPacketSetting
     * @return
     */
    @RequestMapping(value = "/couponUpdateById",method = {RequestMethod.POST})
    public  R couponUpdateById( MRedPacketSetting mRedPacketSetting){
        mRedPacketSetting.setModifyDate(mRedPacketSetting.getEndDate());
        Date date = new Date();
        if(mRedPacketSetting.getTimeType()!=2) {
            if (mRedPacketSetting.getEndDate().getTime() - date.getTime() < 0) {
                R r = new R();
                r.setMsg("您添加的优惠券已过期，请重新选择结束时间");
                r.setCode(1);
                return r;
            }
        }
        return  mCouponService.couponUpdateById(mRedPacketSetting);
    }

    /**
     * 使优惠券失效
     * @param id
     * @return
     */
    @RequestMapping(value="/couponFailure",method ={RequestMethod.POST} )
    public  R couponFailure (String id){
        return mCouponService.couponFailure(id);
    }



}
