package com.tcxf.hbc.consumer.controller.coupon;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.UCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user/Ucoupon")
public class UCouponController extends BaseController {
    @Autowired
    UCouponService uCouponService;

    /**
     * 跳转到用户优惠券页面
     * @return
     */
    @RequestMapping("/coupon")
    public ModelAndView coupon(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/myRedPacket");
        return mode;
    }

    /**
     *查询用户领取的优惠券
     * @return
     */
    @RequestMapping(value="/couponList",method ={RequestMethod.POST} )
    public R couponList(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
       return uCouponService.findList(sessionDto.getSession_user_id());
    }

    /**
     * 用户领取优惠券
     * @param id
     * @return
     */
    @RequestMapping(value = "/getCoupon",method = {RequestMethod.POST,RequestMethod.GET})
    public R getCoupon(HttpServletRequest request,String id){
        SessionDto sessionDto = getUserSession();
        return uCouponService.getCoupon(id,sessionDto.getSession_user_id());
    }
}
