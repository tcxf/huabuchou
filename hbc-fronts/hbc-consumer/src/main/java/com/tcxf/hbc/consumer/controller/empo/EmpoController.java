package com.tcxf.hbc.consumer.controller.empo;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.config.PayUrlPropertiesConfig;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.BalanceVo;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.vo.IncreaseCreditVo;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.CBillService;
import com.tcxf.hbc.consumer.feign.EmpoService;
import com.tcxf.hbc.consumer.feign.IncreaseCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user/empo")
public class EmpoController extends BaseController {

    @Autowired
    PayUrlPropertiesConfig payUrlPropertiesConfig;

    @Autowired
    private EmpoService empoService;
    @Autowired
    private IncreaseCreditService increaseCreditService;

    @Autowired
    private CBillService cBillService;

    @Resource(name = "redisTemplate")
    private RedisTemplate redisTemplate;

//    String uid = "4";
//    String oid = "10148090729809469455";

    /**
     * 快速授信
     * @return
     */
    @RequestMapping("/fastEmpo")
//    @AuthorizationNO
    public R fastEmpo(HttpServletRequest request , CreditVo creditVo)
    {
        R<Double> r1 = new R<>();
        creditVo.setCount("1");
        SessionDto sessionDto = getUserSession();
        creditVo.setOid(sessionDto.getSession_opera_id());
        creditVo.setUid(sessionDto.getSession_user_id());
        creditVo.setAccountid(sessionDto.getSession_account_id());

//        creditVo.setUid(uid);
//        creditVo.setOid(oid);
        logger.info("**************idCard:" + creditVo.getIdCard() + "**************");
        logger.info("**************userName:" + creditVo.getUserName() + "**************");
        logger.info("**************mobile:" + creditVo.getMobile() + "**************");
        logger.info("**************accountNO:" + creditVo.getAccountNO() + "**************");

        creditVo.setIdCard(request.getParameter("idCard"));
        creditVo.setUserName(request.getParameter("userName"));
        creditVo.setMobile(request.getParameter("mobile"));
        creditVo.setAccountNO(request.getParameter("accountNO"));

        redisTemplate.opsForValue().set("credit_idCard_" + sessionDto.getSession_user_id() , creditVo.getIdCard());
        redisTemplate.opsForValue().set("credit_userName_" + sessionDto.getSession_user_id(), creditVo.getUserName());
        redisTemplate.opsForValue().set("credit_mobile_" + sessionDto.getSession_user_id(), creditVo.getMobile());
//        redisTemplate.opsForValue().set("credit_mobile_" + sessionDto.getSession_user_id(), "15675835863");
        redisTemplate.opsForValue().set("credit_accountNO_" + sessionDto.getSession_user_id(), creditVo.getAccountNO());

        redisTemplate.expire("credit_idCard_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
        redisTemplate.expire("credit_userName_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
        redisTemplate.expire("credit_mobile_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
        redisTemplate.expire("credit_accountNO_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);




        String ipAddress = request.getHeader("X-Forwarded-For");
        logger.error("**************X-Forwarded-For:ipAddress:{}", ipAddress);

        if (!ValidateUtil.isEmpty(ipAddress)){
            String[] ipAdd = ipAddress.split(",");
            ipAddress = ipAdd[0];
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            //X-Real-IP：nginx服务代理
            ipAddress = request.getHeader("X-Real-IP");
            logger.error("**************X-Real-IP:ipAddress:{}", ipAddress);
        }


        if (!ValidateUtil.isEmpty(ipAddress)){
            creditVo.setIp(ipAddress);
        }

        logger.error("**************X:ipAddress:{}", ipAddress);

        R r = empoService.fastEmpo(creditVo);

        redisTemplate.delete(sessionDto.getSession_account_id() + "_account_info");
        if (r.getCode() == 0){
            //额度
            double totalScore = r.getData() == null ? 0l : Double.parseDouble(r.getData() + "");
            logger.info("四要素额度：" + totalScore);

            redisTemplate.opsForValue().set("credit_totalScore_" + sessionDto.getSession_user_id(), totalScore);
            redisTemplate.expire("credit_totalScore_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
            r1.setCode(0);
            r1.setData(totalScore);
        }else {
            r1.setCode(1);
            r1.setMsg(r.getMsg());
        }
        return r1;
    }


    /**
     * 跳转四要素审核失败页面
     * @return
     */
    @RequestMapping("/fastEmpoFailed")
    public ModelAndView fastEmpoFailed()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/empo/fastEmpoFailed");
        return modelAndView;
    }

    /**
     * 跳转运营商审核界面
     * @return
     */
    @RequestMapping("/goOperatorEmpo")
//    @AuthorizationNO
    public ModelAndView goOperatorEmpo(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView();

        SessionDto sessionDto = getUserSession();

        String mobile = redisTemplate.opsForValue().get("credit_mobile_" + sessionDto.getSession_user_id()) + "";
        String totalScore = redisTemplate.opsForValue().get("credit_totalScore_" + sessionDto.getSession_user_id()) + "";

        CreditVo cv = new CreditVo();
        cv.setMobile(mobile);


        String idCard = redisTemplate.opsForValue().get("credit_idCard_" + sessionDto.getSession_user_id()) + "";
        String userName = redisTemplate.opsForValue().get("credit_userName_" + sessionDto.getSession_user_id()) + "";

        cv.setIdCard(idCard);
        cv.setUserName(userName);
        cv.setUid(sessionDto.getSession_user_id());

        R<HashMap<String,Object>> r1 = empoService.toOperatorView(cv);
        //获取传回来的值有没有图片验证码，如果没有，传一个0到前端做验证
        Map<String , Object> map = r1.getData();
        String icon = map.get("icon") + "";

        String token = map.get("token") + "";
        Boolean logintag = false;
        String pictag = "false";
        String smstag = "false";

        if (!ValidateUtil.isEmpty(map.get("logintag"))){
            logintag = Boolean.parseBoolean(map.get("logintag").toString());
        }
        if (!ValidateUtil.isEmpty(map.get("pictag"))){
            pictag = map.get("pictag").toString();
        }
        if (!ValidateUtil.isEmpty(map.get("smstag"))){
            smstag = map.get("smstag").toString();
        }
        if (!ValidateUtil.isEmpty(map.get("mobile"))){
            mobile = map.get("mobile").toString();
            totalScore = map.get("totalScore").toString();

            redisTemplate.opsForValue().set("credit_totalScore_" + sessionDto.getSession_user_id(), totalScore);
            redisTemplate.expire("credit_totalScore_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);


            redisTemplate.opsForValue().set("credit_mobile_" + sessionDto.getSession_user_id(), map.get("mobile"));
            redisTemplate.opsForValue().set("credit_idCard_" + sessionDto.getSession_user_id() , map.get("idCard"));
            redisTemplate.opsForValue().set("credit_userName_" + sessionDto.getSession_user_id(), map.get("userName"));
            redisTemplate.expire("credit_mobile_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
            redisTemplate.expire("credit_idCard_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
            redisTemplate.expire("credit_userName_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
        }


        redisTemplate.opsForValue().set("creditVoToken_" + sessionDto.getSession_user_id(), token);
        redisTemplate.expire("creditVoToken_" + sessionDto.getSession_user_id(), 1800, TimeUnit.SECONDS);
        logger.info("存入缓存的token:" + token);

        if (!"".equals(icon))
        {
            modelAndView.addObject("yzm","yzm");
            modelAndView.addObject("icon",icon);
        }else {
            modelAndView.addObject("yzm","");
            modelAndView.addObject("icon","");
        }
        CreditVo creditVo = new CreditVo();
        creditVo.setToken(token);
        logger.info("************goOperatorEmpo.totalScore:" + totalScore + "**************");
        creditVo.setTotalScore(totalScore);
        //验证四要素是否通过,0为已通过，2为未通过

        creditVo.setOid(sessionDto.getSession_opera_id());
        creditVo.setUid(sessionDto.getSession_user_id());
//        creditVo.setUid(uid);
//        creditVo.setOid(oid);
        R r2 = empoService.verifyCredit(creditVo);

        R r3 = cBillService.getOoperaInfoNameByid(sessionDto.getSession_opera_id());

        if (r2.getCode() == 0)
        {

            logger.info("****************2.type:{}", request.getSession().getAttribute("type"));
            logger.info("****************2.state:{}", request.getSession().getAttribute("state"));
            logger.info("****************2.openId:{}", request.getSession().getAttribute("openId"));
            if (!ValidateUtil.isEmpty(request.getSession().getAttribute("type"))){
                modelAndView.addObject("type", request.getSession().getAttribute("type"));
            }
            if (!ValidateUtil.isEmpty(request.getSession().getAttribute("state"))){
                modelAndView.addObject("state", request.getSession().getAttribute("state"));
                String payPath = payUrlPropertiesConfig.getGotopayurl() + "/payment/";
                logger.info("****************payPath:{}", payPath);
                modelAndView.addObject("payPath", payPath);
            }
            if (!ValidateUtil.isEmpty(request.getSession().getAttribute("openId"))){
                modelAndView.addObject("openId", request.getSession().getAttribute("openId"));
            }
            modelAndView.setViewName("ftl/empo/operatorEmpo");
            modelAndView.addObject("mobile",mobile);
            modelAndView.addObject("totalScore",totalScore);
            modelAndView.addObject("logintag",logintag.toString());
            modelAndView.addObject("pictag",pictag);
            modelAndView.addObject("smstag",smstag);
            if (!ValidateUtil.isEmpty(r3.getData())){
                modelAndView.addObject("oname",r3.getData());
            }
        }else {
            modelAndView.setViewName("ftl/empo/fastEmpoFailed");
        }
        return modelAndView;
    }


    /**
     * 刷新图片验证码
     * @param mobile 手机号
     * @return
     */
    @RequestMapping("/refreshLoginSmsCode")
    public R refreshLoginSmsCode(String mobile,HttpServletRequest request)
    {
        R r = new R();
        SessionDto sessionDto = getUserSession();
        String token = redisTemplate.opsForValue().get("creditVoToken_" + sessionDto.getSession_user_id()) + "";
        CreditVo cv = new CreditVo();
        cv.setMobile(mobile);
        cv.setToken(token);

        R<HashMap<String,Object>> r1 = empoService.toOperatorView(cv);
        //获取传回来的值有没有图片验证码，如果没有，传一个0到前端做验证
        Map<String , Object> map = r1.getData();
        String icon = map.get("icon") + "";

        if (r1.getCode() != 0){
            logger.info("***********图片验证码发送失败**********");
            r.setMsg("图片验证码发送失败");
        }else {
            logger.info("***********图片验证码发送成功**********");
            r.setMsg("图片验证码发送成功");
            r.setCode(0);
            r.setData(icon);
        }
        return r;
    }


    /**
     * 运营商验证发送验证码
     * @param mobile 手机号
     * @return
     */
    @RequestMapping("/sendVerificationCode")
//    @AuthorizationNO
    public R sendVerificationCode(String mobile,HttpServletRequest request)
    {
//        String token = (String) request.getSession().getAttribute("token");
        SessionDto sessionDto = getUserSession();
        String token = redisTemplate.opsForValue().get("creditVoToken_" + sessionDto.getSession_user_id()) + "";
        CreditVo creditVo = new CreditVo();
        creditVo.setToken(token);
        creditVo.setMobile(mobile);
        logger.info("************sendVerificationCode.mobile:" + mobile);
        R r = empoService.sendVerificationCode(creditVo);
        if (r.getCode() != 0){
            r.setMsg("短信验证码发送失败");
        }
        return r;
    }

    /**
     * 运营商验证方法
     * 1.serviceType    判断是图片验证码还是短信验证码，move为短信
     * 2.mobile     手机号
     * 3.serviceCode    服务密码
     * 4.randomPassword   短信验证码
     * 5.code   图片验证码
     * @return
     */
    @RequestMapping("/threeMethod")
//    @AuthorizationNO
    public R threeMethod(CreditVo creditVo , HttpServletRequest request)
    {

        SessionDto sessionDto = getUserSession();

        String randomPassword = request.getParameter("randomPassword");
        String code = request.getParameter("code");
        String totalScore = request.getParameter("totalScore");
        String serviceCode = request.getParameter("serviceCode");
        String mobile = request.getParameter("mobile");
        String loginResultStatus = ValidateUtil.isEmpty(request.getParameter("loginResultStatus")) ? "" : request.getParameter("loginResultStatus");
        //判断短信验证码为不为空，如果为空，设置serviceType为空，如果不为空，设置serviceType为move
//        creditVo.setToken((String) request.getSession().getAttribute("token"));
        String token = redisTemplate.opsForValue().get("creditVoToken_" + sessionDto.getSession_user_id()) + "";

        logger.info("***********token:" + token + "**************");
        logger.info("***********randomPassword:" + randomPassword + "**************");
        logger.info("***********code:" + code + "**************");
        logger.info("***********totalScore:" + totalScore + "**************");
        creditVo.setToken(token);
        creditVo.setTotalScore(totalScore);
        creditVo.setServiceCode(serviceCode);
        creditVo.setMobile(mobile);
        creditVo.setLoginResultStatus(loginResultStatus);
        if ("".equals(randomPassword) || randomPassword == null)
        {
            creditVo.setCode(code);
            creditVo.setServiceType("");
            creditVo.setRandomPassword("");
        }else {
            creditVo.setServiceType("move");
//            creditVo.setCode("");
            creditVo.setRandomPassword(randomPassword);
        }

        creditVo.setOid(sessionDto.getSession_opera_id());
        creditVo.setUid(sessionDto.getSession_user_id());
//        creditVo.setUid(uid);
//        creditVo.setOid(oid);

        String idCard = redisTemplate.opsForValue().get("credit_idCard_" + sessionDto.getSession_user_id()) + "";
        String userName = redisTemplate.opsForValue().get("credit_userName_" + sessionDto.getSession_user_id()) + "";
        String accountNO = redisTemplate.opsForValue().get("credit_accountNO_" + sessionDto.getSession_user_id()) + "";
        creditVo.setIdCard(idCard);
        creditVo.setUserName(userName);
        creditVo.setAccountNO(accountNO);

        R r = empoService.threeMethod(creditVo);
        logger.info("***************code:" + r.getCode());
        if (r.getCode() == 2){
//            r.setMsg("授权认证失败，请重新获取！");
        }else if (r.getCode() == 1){
//            r.setMsg("需要进行短信验证码二次认证，请输入最新验证码!");
        }
        return r;
    }

    /**
     * 进入提额界面
     * @param request
     * @return
     */
    @RequestMapping("/goEmpoPlus")
    public ModelAndView goEmpoPlus(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView("ftl/empo/empoPlus");
        return modelAndView;
    }

    /**
     * 获取提额数据
     * @return
     */
    @RequestMapping("/getEmpo")
    @AuthorizationNO
    public R getEmpo(){
        SessionDto sessionDto = getUserSession();
        TbUpMoney tbUpMoney = new TbUpMoney();
        tbUpMoney.setUid(sessionDto.getSession_user_id());
        tbUpMoney.setOid(sessionDto.getSession_opera_id());
        R<List<TbUpMoney>> r = increaseCreditService.toIncreaseCredit(tbUpMoney);
        return r;
    }

    /**
     * 跳转进入紧急联系人
     * @return
     */
    @RequestMapping("/emergencyContact")
    public ModelAndView emergencyContact(){
        ModelAndView modelAndView = new ModelAndView("ftl/empo/emergencyContact");
        return modelAndView;
    }

    /**
     * 保存紧急联系人
     * @return
     */
    @RequestMapping("/saveEmergencyContact")
    public R saveEmergencyContact(HttpServletRequest request){
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        TbUpMoney tbUpMoney1 = new TbUpMoney();
        tbUpMoney1.setName("紧急联系人姓名");
        tbUpMoney1.setCode("contactName");
        tbUpMoney1.setValue(request.getParameter("name"));
        TbUpMoney tbUpMoney2 = new TbUpMoney();
        tbUpMoney2.setCode("contactMobile");
        tbUpMoney2.setName("紧急联系人手机号码");
        tbUpMoney2.setValue(request.getParameter("mobile"));
        TbUpMoney tbUpMoney3 = new TbUpMoney();
        tbUpMoney3.setName("与本人关系");
        tbUpMoney3.setCode("relation");
        tbUpMoney3.setValue(request.getParameter("relation"));
        List<TbUpMoney> list = new ArrayList<>();
        list.add(tbUpMoney1);
        list.add(tbUpMoney2);
        list.add(tbUpMoney3);
        increaseCreditVo.setIncreaseCreditlist(list);
        increaseCreditVo.setType("emergencyContact");
        SessionDto sessionDto = getUserSession();
        String id = sessionDto.getSession_user_id();
        increaseCreditVo.setUid(id);
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        R r = increaseCreditService.saveEmergencyContact(increaseCreditVo);
        return r;
    }

    /**
     * 跳转进入微信界面
     * @return
     */
    @RequestMapping("/qqOrWeChat")
    public ModelAndView qqOrWeChat(){
        ModelAndView modelAndView = new ModelAndView("ftl/empo/qqAndWeChat");
        return modelAndView;
    }

    /**
     * 保存qq号/或者微信号
     * @param
     * @param
     * @return
     */
    @RequestMapping("/saveQQOrWeChat")
    public R saveQQOrWeChat(HttpServletRequest request){
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        List<TbUpMoney> list = new ArrayList<>();
        if (!ValidateUtil.isEmpty(request.getParameter("qq")))
        {
            TbUpMoney tbUpMoney1 = new TbUpMoney();
            tbUpMoney1.setName("QQ");
            tbUpMoney1.setCode("qq");
            tbUpMoney1.setValue(request.getParameter("qq"));
            list.add(tbUpMoney1);
        }
        if (!ValidateUtil.isEmpty(request.getParameter("weChat")))
        {
            TbUpMoney tbUpMoney2 = new TbUpMoney();
            tbUpMoney2.setCode("WeChat");
            tbUpMoney2.setName("weChat");
            tbUpMoney2.setValue(request.getParameter("weChat"));
            list.add(tbUpMoney2);
        }
        increaseCreditVo.setIncreaseCreditlist(list);
        SessionDto sessionDto = getUserSession();
        String id = sessionDto.getSession_user_id();
        increaseCreditVo.setUid(id);
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        increaseCreditVo.setType("qqOrWeChat");
        R r = increaseCreditService.saveEmergencyContact(increaseCreditVo);
        return r;
    }

    /**
     * 跳转到社保H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toSocialInsurance")
    public void toSocialInsurance(HttpServletRequest request , HttpServletResponse response) throws IOException {
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");

        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());

        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);

        R<String> r = increaseCreditService.toSocialInsurance(increaseCreditVo);
        String url = r.getData();
        response.sendRedirect(url);
    }

    /**
     * 跳转到公积金H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toAccumulationFund")
    public void toAccumulationFund(HttpServletRequest request , HttpServletResponse response) throws IOException {
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");

        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());

        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);

        R<String> r = increaseCreditService.toAccumulationFund(increaseCreditVo);
        String url = r.getData();
        response.sendRedirect(url);
    }

    /**
     * 跳转到学信验证H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toEducationApprove")
    public void toEducationApprove(HttpServletRequest request , HttpServletResponse response) throws IOException {
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");

        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);

        R<String> r = increaseCreditService.toEducationApprove(increaseCreditVo);
        String url = r.getData();
        response.sendRedirect(url);
    }

    /**
     * 跳转到淘宝H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toTaobaoApprove")
    public R toTaobaoApprove(HttpServletRequest request , HttpServletResponse response) throws IOException {
//        ModelAndView modelAndView = new ModelAndView();
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");

        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);

        R<String> r = increaseCreditService.toTaobaoApprove(increaseCreditVo);

//        String url = r.getData();

//        modelAndView.addObject("url", url);
//        modelAndView.addObject("type", "taobaoApprove");
//        modelAndView.setViewName("ftl/empo/guide");
//        return modelAndView;
        return r;
    }

    /**
     * 跳转到京东H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toJDcomApprove")
    public void toJDcomApprove(HttpServletRequest request , HttpServletResponse response) throws IOException {
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");

        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);

        R<String> r = increaseCreditService.toJDcomApprove(increaseCreditVo);
        String url = r.getData();
        response.sendRedirect(url);
    }


    /**
     * 跳转到填入基本信息界面
     */
    @RequestMapping("/toDidiApproveView")
    public ModelAndView toDidiApproveView(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        String type = request.getParameter("type");

        modelAndView.addObject("type", type);

        modelAndView.setViewName("ftl/empo/didiApprove");
        return modelAndView;
    }


    /**
     * 跳转到滴滴打车H5
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/toDidiDacheApprove")
    @AuthorizationNO
    public R toDidiDacheApprove(HttpServletRequest request , HttpServletResponse response) {

//        ModelAndView modelAndView = new ModelAndView();
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idcard = request.getParameter("idcard");
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());
        increaseCreditVo.setIdcard(idcard);
        increaseCreditVo.setName(name);
        increaseCreditVo.setPhone(phone);
        R<String> r = increaseCreditService.toDidiDacheApprove(increaseCreditVo);
//        String url = r.getData();

//        modelAndView.addObject("url", url);
//        modelAndView.addObject("type", "didiApprove");
//        modelAndView.setViewName("ftl/empo/guide");
//        return modelAndView;
//        response.sendRedirect(url);
        return r;
    }

    /**
     * 跳转到滴滴打车H5
     * @param request
     * @throws IOException
     */
    @GetMapping("/toDidiDacheJump")
    @AuthorizationNO
    public ModelAndView toDidiDacheJump(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView();
        String url = request.getParameter("urls");

        modelAndView.addObject("url", url);
        modelAndView.addObject("type", "didiApprove");
        modelAndView.setViewName("ftl/empo/confirmJump");
        return modelAndView;
    }

    /**
     * 跳转到淘宝H5
     * @param request
     * @throws IOException
     */
    @GetMapping("/toTaobaoJump")
    @AuthorizationNO
    public ModelAndView toTaobaoJump(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView();
        String url = request.getParameter("taobaourls");

        modelAndView.addObject("url", url);
        modelAndView.addObject("type", "taobaoApprove");
        modelAndView.setViewName("ftl/empo/confirmJump");
        return modelAndView;
    }

    /**
     * 最后一步
     * @return
     */
    @RequestMapping("/toSaveIncreaseCredit")
    public R toSaveIncreaseCredit(HttpServletRequest request){
        IncreaseCreditVo increaseCreditVo = new IncreaseCreditVo();
        SessionDto sessionDto = getUserSession();
        increaseCreditVo.setUid(sessionDto.getSession_user_id());
        increaseCreditVo.setOid(sessionDto.getSession_opera_id());

        CreditVo creditVo = new CreditVo();
        creditVo.setUid(sessionDto.getSession_user_id());
        creditVo.setOid(sessionDto.getSession_opera_id());

        R r = increaseCreditService.toSaveIncreaseCredit(increaseCreditVo);
        logger.info("*************toSaveIncreaseCredit：" + r.getMsg());

        return r;
    }

    /**
     * 跳转到显示提额界面
     */
    @RequestMapping("/amount")
    public ModelAndView goEmpoAmount(String subGrade,String sumbalance){
        ModelAndView modelAndView = new ModelAndView();
        logger.info("*************subGrade:" + subGrade);
        logger.info("*************sumbalance:" + sumbalance);
        modelAndView.addObject("subGrade",subGrade);
        modelAndView.addObject("sumbalance",sumbalance);
        modelAndView.setViewName("ftl/empo/increaseSuccess");
        return modelAndView;
    }

    /**
     * 跳转到商业授信界面
     */
    @RequestMapping("/businessCreditView")
    public ModelAndView businessCreditView(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ftl/empo/businessCredit");
        return modelAndView;
    }


    /**
     * 保存商业授信信息
     * @return
     */
    @RequestMapping("/toSaveBusinessCredit")
    public R toSaveBusinessCredit(CreditVo creditVo){
        SessionDto sessionDto = getUserSession();
        creditVo.setUid(sessionDto.getSession_user_id());
        creditVo.setOid(sessionDto.getSession_opera_id());
        creditVo.setMid(sessionDto.getSession_m_id());

        R r = empoService.saveBusinessCredit(creditVo);
        logger.info("*************toSaveBusinessCredit：" + r.getMsg());

        return r;
    }

    /**
     * 跳转到商业授信报名成功界面
     */
    @RequestMapping("/toApplySuccessView")
    public ModelAndView toApplySuccessView(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ftl/empo/applySuccess");
        return modelAndView;
    }

}
