package com.tcxf.hbc.consumer.controller.increasecredit;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.IncreaseCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user/increasecredit")
public class IncreaseCreditController extends BaseController {

    @Autowired
    private IncreaseCreditService increaseCreditService;


    /**
     * 进入提额界面-获取需输入项
     * @return
     */
    @RequestMapping("/fastEmpoFailed")
    public ModelAndView fastEmpoFailed()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/fastEmpoFailed");
        return modelAndView;
    }

    @RequestMapping("/callBack")
    @AuthorizationNO
    public String callBack(HttpServletRequest request){
        String authJson = request.getParameter("authJson");
        String token = request.getParameter("token");

        SessionDto sessionDto = getUserSession();

//        String authJson = "{\"chsiBaseInfo\":{\"country\":\"中国大陆\",\"credentialsNum\":\"4****************2\",\"credentialsType\":\"中华人民共和国居民身份证\",\"loginAccount\":\"13647300951\",\"mobilePhone\":\"136****0951\",\"name\":\"张剑锋\",\"status\":0},\"educationInfoList\":[],\"schoolInfoList\":[{\"birthday\":\"1993-08-18 00:00:00\",\"branch\":\"\",\"className\":\"2011通信工程\",\"credentialsNum\":\"430621199308181852\",\"degree\":\"本科\",\"degreeType\":\"普通高等教育\",\"educationStyle\":\"普通全日制\",\"enrolDate\":\"2011-09-17 00:00:00\",\"graduateDate\":\"2015-06-10 00:00:00\",\"graduationPhoto\":\"https://static.gxb.io/upload/chsi/29848573/e83643cee64fd7b0c3bfc3ae1c1fc0df.jpeg\",\"major\":\"通信工程\",\"nation\":\"汉族\",\"schoolYears\":4,\"sex\":1,\"status\":\"毕业\",\"studentNum\":\"2011091024\",\"university\":\"吉首大学张家界学院\"}]}";
//        logger.info("*****************authJson:" + authJson);
        logger.info("*****************token:" + token);

        increaseCreditService.callBack(authJson,token,sessionDto.getSession_openId());

        return "{\"retCode\":1,\"retMsg\":\"成功\"}";
    }

}
