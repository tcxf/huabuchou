package com.tcxf.hbc.consumer.controller.index;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.HttpUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.IndexBannerService;
import com.tcxf.hbc.consumer.feign.OoperainfoService;
import com.tcxf.hbc.consumer.feign.LoginService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.omg.PortableInterceptor.IORInfoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/user/index")
public class IndexInfoController extends BaseController {

    @Autowired
    private IndexBannerService indexBannerService;

    @Autowired
    private OoperainfoService ooperainfoService;

    private Log log = LogFactory.getLog(this.getClass());

    @RequestMapping("/Goindex")
    @AuthorizationNO
    public ModelAndView Goindex(HttpServletRequest request) throws IOException {
        ModelAndView modelAndView=new ModelAndView();
        /**
         * 获取ticket配置，用于界面微信JS验证调用
         */
        String osn = request.getServerName();
        String invateCode = request.getParameter("invateCode");
        OOperaInfo selectOOperaInfo = ooperainfoService.selectooperainfoByOsn(osn).getData();
        String url =  request.getRequestURL().toString();
        if(!ValidateUtil.isEmpty(selectOOperaInfo)){
            String queryurl = request.getQueryString();
            if (!ValidateUtil.isEmpty(queryurl)) {
                url += "?" + queryurl;
            }
            Map<String,Object> ticketMap = HttpUtil.ticketConfig(url,selectOOperaInfo.getAppid(),selectOOperaInfo.getAppsecret());
            modelAndView.addObject("ticketMap",ticketMap);
        }
        modelAndView.addObject("sessionId",getSessionId());
        modelAndView.addObject("invateCode",invateCode);
        modelAndView.setViewName("ftl/t_index");
        return  modelAndView;
    }

    /**
     * 查询banner图
     * @param request
     * @return
     */
    @RequestMapping("/QueryBanner")
    @AuthorizationNO
    public R QueryBanner(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        if(!ValidateUtil.isEmpty(sessionDto)){
            return indexBannerService.QueryBanner(sessionDto.getSession_user_id());
        }
        return  indexBannerService.QueryBanners();
    }
}
