package com.tcxf.hbc.consumer.controller.index;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.MerchantCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/merchants")
public class MerchantCategoryController {
    @Autowired
    public MerchantCategoryService merchantCategoryService;

    /**
     * 查询商户分类
     * @return
     */
    @RequestMapping("Querymerchantcategory")
    @AuthorizationNO
    public R QueryMerchantCategory(){
        R r=new R();
        return merchantCategoryService.QueryMerchantCategory();
    }


    /**
     * 查询商户分类
     * @return
     */
    @RequestMapping("Querymerchantcategorys")
    @AuthorizationNO
    public R QueryMerchantCategorys(){
        R r=new R();
        return merchantCategoryService.QueryMerchantCategorys();
    }

    /**
     * 商户分类更多跳转
     * @return
     */
    @RequestMapping("/findNature")
    @AuthorizationNO
    public ModelAndView findNatures(){
        return  new ModelAndView("ftl/findNature");
    }

    /**
     * 跳转附近商家
     * @return
     */
    @RequestMapping("/findNaturegoods")
    @AuthorizationNO
    public ModelAndView findNaturegoods(String nature){
        ModelAndView model = new ModelAndView();
        model.addObject("micId",nature);
        model.setViewName("ftl/findNaturegoods");
        return  model;
    }

}
