package com.tcxf.hbc.consumer.controller.index;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.MerchantQueryService;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/merchantQuery")
/**
 * 首页热门搜索
 * jiangyong
 * 2018年7月11日16:18:38
 */
public class MerchantQueryController {
    /**
     * 首页查询跳转  热门搜索界面
     * @return
     */
    @Autowired
    public MerchantQueryService merchantQueryService;

    @RequestMapping("/Queryhotsearch")
    @AuthorizationNO
    public ModelAndView Queryhotsearch(){
      return new ModelAndView("ftl/grabbleHandler");
    }

    /**
     * 加载热门搜索
     * @return
     */
    @RequestMapping("/hoRearedName")
    @AuthorizationNO
    public R hoRearedName(){
        R r=new R();
        return  merchantQueryService.QueryAllHotsearch();
    }

    /**
     * 更具热门搜索分页查询
     * @param name
     * @return
     */
    @RequestMapping("/grabbleHandlerName")
    @AuthorizationNO
    public R QueryAllHotsearchName(String name){
        R r=new R();
        return  merchantQueryService.QueryAllHotsearchName(name);
    }
}
