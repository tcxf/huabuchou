package com.tcxf.hbc.consumer.controller.index;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.MerchantCategoryService;
import com.tcxf.hbc.consumer.feign.PacketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 优惠券
 */
@RestController
@RequestMapping("/packet")
public class PacketController  extends BaseController{
    @Autowired
    public PacketService packetService;

    @Autowired
    public MerchantCategoryService merchantCategoryService;
    /**
     * 首页商户优惠券展示
     */
    @RequestMapping("/loadReadPcke")
    @AuthorizationNO
    public R loadReadPcke(HttpServletRequest request){
            return  packetService.NOpackets();
    }

    /**
     * 首页优惠卷更多跳转附近商家
     * @return
     */
    @RequestMapping("/coupons")
    @AuthorizationNO
    public ModelAndView coupon(){
        return  new ModelAndView("ftl/coupons");
    }

    /**
     * 查询商户分类
     * @return
     */
    @RequestMapping("Querymerchantcategory")
    @AuthorizationNO
    public R QueryMerchantCategory(){
        R r=new R();
        return merchantCategoryService.QueryMerchantCategory();
    }


    /*更多精选优惠券*/
    @RequestMapping("/queryloadReadPckess")
    @AuthorizationNO
    public R queryloadReadPckes(String micId,HttpServletRequest request,String page){
        R r=new R();
        SessionDto  sessionDto =  getUserSession();
        if(!ValidateUtil.isEmpty(sessionDto)){
            return  packetService.QueryPacketss(micId,sessionDto.getSession_user_id(),page);
        }
        return  packetService.QueryPacketss(micId,"null",page);
    }
}
