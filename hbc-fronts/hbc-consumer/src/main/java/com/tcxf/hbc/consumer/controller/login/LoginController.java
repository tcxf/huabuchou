package com.tcxf.hbc.consumer.controller.login;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.consumer.common.token.TokenManager;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.LoginDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.RegisterService;
import com.tcxf.hbc.consumer.util.ReturnUrlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@RestController
@RequestMapping("/cuser")
public class LoginController extends BaseController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private RegisterService registerService;

    @Autowired
    private TokenManager tokenManager;

    private final static String SPLIT_CHARACTER = "_";

    /**
     * 跳转登陆
     * @return ModelAndView
     */
    @GetMapping("/gologin")
    @AuthorizationNO
    public ModelAndView require(HttpServletRequest request)
    {
        String openId = request.getParameter("openId");
        ModelAndView modelAndView = new ModelAndView("ftl/login/loadLogin");
        modelAndView.addObject("openId",openId);
        return modelAndView;
    }

    /**
     * 生成验证码对照短信模板进行发送
     * @param request
     * @return
     */
    @RequestMapping("/getYzm")
    @AuthorizationNO
    public R getYzm(HttpServletRequest request)
    {
        R<Object> r = new R<>();
        //map获取request里面的值
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        //生成验证码
        String mobile = (String) paramMap.get("mobile");
        String type = (String) paramMap.get("type");
        if(ValidateUtil.isEmpty(mobile)||ValidateUtil.isEmpty(type)){
            return R.newErrorMsg("发送失败");
        }
        //调用发送验证码方法
        R yzm = registerService.getYzm(mobile, type);
        r.setCode(1);
        r.setMsg(yzm.getMsg());
        return r;

    }

    /**
     * 拦截器里拿openId和存session
     * @param request
     * @return
     */
    @RequestMapping("/interceptor")
    @AuthorizationNO
    public ModelAndView interceptor(HttpServletRequest request){
        ModelAndView modelAndView;
        //获取到code值拿到openId
        String code = request.getParameter("code");
        String states = request.getParameter("state");
        Pair<String, String> pair = getkeyAndParameter(states);
        String state = pair.getLeft();
        //根据二级域名获取OOperaInfo
        String osn = request.getServerName();
        R<OOperaInfo> or = loginService.getOperaId(osn);
        if(or.getCode()== R.FAIL || ValidateUtil.isEmpty(or.getData())){
            throw new CheckedException("未获取到运营商信息");
        }
        OOperaInfo oOperaInfo = or.getData();
        String openId = HttpUtil.getOpenId(code , oOperaInfo.getAppid() , oOperaInfo.getAppsecret());
        //如果openID 为空直接跳登陆界面
        if(ValidateUtil.isEmpty(openId)){
            if(!ValidateUtil.isEmpty(state)&&ReturnUrlUtil.URL_KEY_GOTOPAY.equals(state)){
                return new ModelAndView("ftl/pay/payment_opt");
            }
            return new ModelAndView("ftl/login/loadLogin");
        }
        //根据openId登陆
        CUserInfo cUserInfo = loginService.loginByOpenId(openId).getData();
        //不存在跳转到登陆页面
        if (ValidateUtil.isEmpty(cUserInfo))
        {
            if(!ValidateUtil.isEmpty(state)&&ReturnUrlUtil.URL_KEY_GOTOPAY.equals(state)){
                ModelAndView mv = new ModelAndView("ftl/pay/payment_opt");
                mv.addObject("openId",openId);
                return mv;
            }
            ModelAndView mv = new ModelAndView("ftl/login/loadLogin");
            mv.addObject("openId",openId);
            return mv;
        }
        //存在 进行登录session缓存
        R<UserInfoDto>  r = loginService.getUserAccountInfoByUserId(cUserInfo.getId());
        if(r.getCode()==R.FAIL || ValidateUtil.isEmpty(r.getData())){
            throw new CheckedException("登录异常，请稍后再试");
        }
        UserInfoDto userInfoDto = r.getData();
        userInfoDto.getcUserInfo().setWxOpenid(openId);
        createSession(userInfoDto,oOperaInfo.getId());
        //这里要根据不同的点击路径进行跳转
        String returnUrl = ReturnUrlUtil.getLoginAfterUrl(state);
        if(!ValidateUtil.isEmpty(returnUrl)){
            if(ReturnUrlUtil.URL_KEY_GOTOPAY.equals(state)){
                return new ModelAndView("redirect:/"+returnUrl+"?"+pair.getRight());
            }
            return new ModelAndView("redirect:/"+returnUrl+"");
        }
        return new ModelAndView("redirect:/user/index/Goindex");
    }


    /**
     * 登陆
     * @param request
     * @param loginDto
     * @return
     */
    @RequestMapping("/login")
    @AuthorizationNO
    public R login(HttpServletRequest request , @RequestBody LoginDto loginDto)
    {
        //为1的时候说明2的时候为用户登陆,为是商户登陆
        R r = loginService.login(loginDto);
        //code为0为success
        if (r.getCode() == R.FAIL ) {
            return r;
        }
        if(!ValidateUtil.isEmpty(loginDto.getOpenId())){
            loginService.addOpenId(loginDto.getOpenId() , loginDto.getMobile());
        }
        //把值存入session和cookie中
        String id = (String) r.getData();
        R<UserInfoDto> listR = loginService.userAccountInfo(id);
        if(listR.getCode()==R.FAIL){
            return listR;
        }
        UserInfoDto userInfoDto = listR.getData();
        String osn = request.getServerName();
        R<OOperaInfo> or = loginService.getOperaId(osn);
        if(or.getCode()==R.FAIL || ValidateUtil.isEmpty(or.getData())){
            throw new CheckedException("未获取到运营商信息，登录失败");
        }
        OOperaInfo oOperaInfo = or.getData();
        userInfoDto.getcUserInfo().setWxOpenid(loginDto.getOpenId());
        createSession(userInfoDto,oOperaInfo.getId());
        return R.newOK();
    }

    /**
     * @param state
     * @return Pair<String , String> left 为 mid  right 为 userId
     */
    private Pair<String, String> getkeyAndParameter(String state) {
        if (ValidateUtil.isEmpty(state)) {
            return null;
        }
        String[] list= StringUtil.splitString(state, SPLIT_CHARACTER);
        return new Pair<String, String>(list[0], list[1]);
    }


    /**
     * 获取openId
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/getCode")
    @AuthorizationNO
    public void getCode(HttpServletRequest request , HttpServletResponse response) throws IOException
    {
        //2、通过微信跳转 获取openId
        String basePath = request.getScheme() + "://"+request.getServerName() +
                ":" + request.getServerPort() + request.getContextPath() + "/";
        //根据二级域名获取OOperaInfo
        String osn = request.getServerName();
        OOperaInfo oOperaInfo = loginService.getOperaId(osn).getData();
        String redirectUrl = HttpUtil.buildSendRedUrl(basePath , oOperaInfo.getAppid());
        redirectUrl=redirectUrl.replace("redirectUrlValue", "cuser/gologin");
        response.sendRedirect(redirectUrl);
    }

    /**
     * 跳转快速授信页面
     * @return
     */
    @RequestMapping("/updateTransPwd1")
    public ModelAndView updateTransPwd1()
    {
        return new ModelAndView("ftl/empo/fastEmpo");
    }
}