package com.tcxf.hbc.consumer.controller.merchant;

import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.CMerchantInfoService;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.vo.MerchantVo;
import com.xiaoleilu.hutool.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cmerchant")
public class CMerchantController extends BaseController {


    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    @Autowired
    private  CMerchantInfoService cMerchantInfoService;

    @Autowired
    private LoginService loginService;

    protected org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());


    /**
     * 店铺资料跳转
     * @return
     */

    @RequestMapping("/QueryMerchantQu")
    public ModelAndView QueryMerchantQu(){
        ModelAndView modelAndView=new ModelAndView();
        SessionDto  sessionDto =  getUserSession();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String mid = userInfoDto.getmMerchantInfo().getId();
        R r = cMerchantInfoService.QueryMerchantInfo(mid);
        R r1 = cMerchantInfoService.QueryMerchantCatgory();
        modelAndView.addObject("m",r.getData());
        modelAndView.addObject("list",r1.getData());
        modelAndView.setViewName("ftl/merchant/merchantInfo");
        return  modelAndView;
    }

    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public Map<String, String> upload( MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
           logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }

    @RequestMapping("/updateStore")
    public R updateStore(@RequestBody MerchantVo vo , HttpServletRequest request) {
        try {
            String type = request.getParameter("type");
            vo.setUid(getUserSession().getSession_user_id());
            if (!ValidateUtil.isEmpty(type)) {
                R r = cMerchantInfoService.updateStoreinformation(vo);
                r.setCode(2);
                return r;
            }
            return cMerchantInfoService.updateStoreinformation(vo);
        } catch (Exception e) {
            if(e instanceof CheckedException){
                return  R.newErrorMsg(e.getMessage()) ;
            }
            return  R.newError("请确认手机号码，姓名，身份证是否正确！","");
        }
    }
}


