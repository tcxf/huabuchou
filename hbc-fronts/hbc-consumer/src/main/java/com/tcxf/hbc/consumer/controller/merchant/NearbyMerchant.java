package com.tcxf.hbc.consumer.controller.merchant;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.NearbyMerchantDto;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.NearbyMerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 附件商家
 */
/**
* @Author:HuangJun
* @Description
* @Date:
*/
@RestController
@RequestMapping("/nearby")
public class NearbyMerchant extends BaseController {
    @Autowired
    NearbyMerchantService nearbyMerchantService;

    @Autowired
    LoginService loginService;

    @RequestMapping("/index")
    public ModelAndView index(){ return new ModelAndView("ftl/t_index"); }
    /**
     * 附近商家
     * @return
     */
    @AuthorizationNO
    @RequestMapping(value = "/findList",method = {RequestMethod.POST})
    public R findList(String lat,String lng,String page,String micId){
        //传null mc服务会报错 改为字符串null
        if(micId==null || micId==""){
            micId="null";
        }
        return nearbyMerchantService.couponList(lat,lng,page,micId);
    }

    /**
     * 商户详情
     * @param id
     * @return
     */
    @AuthorizationNO
    @RequestMapping(value = "/findById",method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView findById(HttpServletRequest request, String id,String invateCode){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        String uid="null";
        if(sessionDto!=null){
         uid   = sessionDto.getSession_user_id();
        }
        R r = nearbyMerchantService.findById(id,uid);
         mode.addObject("entity",r.getData());
         mode.addObject("invateCode",invateCode);
         mode.addObject("id",id);
         mode.setViewName("ftl/merchantDetail1");
         return mode;
    }
    @RequestMapping(value = "/dt",method = {RequestMethod.GET})
    public  ModelAndView dt(String id){
        ModelAndView model = new ModelAndView();
        Map<String,Object> map = nearbyMerchantService.dt( id);
        model.addObject("address",map.get("address"));
        model.addObject("name",map.get("name"));
        model.setViewName("ftl/merchant/apply");
        return  model;
    }

    /**
     * 查询商户优惠券以及用户领取的状态
     * @param miId
     * @return
     */
    @AuthorizationNO
    @RequestMapping(value = "/findByUserId",method = RequestMethod.POST)
    public R findByUserId(HttpServletRequest request, String miId){
        SessionDto sessionDto = getUserSession();
        String userId="null";
        if(sessionDto!=null){
            userId   = sessionDto.getSession_user_id();
        }
        return nearbyMerchantService.findByUserId(userId, miId);
    }

}
