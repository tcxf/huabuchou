package com.tcxf.hbc.consumer.controller.mine;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.CCenterService;
import com.tcxf.hbc.consumer.feign.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * @author YWT_tai
 * @Date :Created in 10:53 2018/7/5
 */
@RestController
@RequestMapping("/consumer/center")
public class CCenterContrller extends BaseController {

    @Autowired
    private CCenterService cCenterService;
    @Autowired
    private LoginService loginService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:21 2018/7/5
     * 跳转我的收藏
    */
    @RequestMapping("/jumpMyCollect")
    public ModelAndView jumpMyCollect(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/collect");
        return  mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:36 2018/7/5
     * 加载我的收藏商铺
    */
    @RequestMapping("/findMineCollects")
    public R findMineCollects(){
        SessionDto sessionDto = getUserSession();
       return cCenterService.findMineCollects(sessionDto.getSession_user_id());
    }
    
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:39 2018/7/6
     * 取消收藏
    */
    @RequestMapping("/deleteCollect")
    public R deleteCollect(String id){
       return cCenterService.deleteCollect(id);
    }

    /**
    * @Author:HuangJun
    * @Description
     * @Date: 13:56 2018/7/11
     *根据用户id和商户id取消收藏
    */
    @RequestMapping("/deleteByMid")
    public R deleteByMid(String mid){
        SessionDto sessionDto = getUserSession();
        return  cCenterService.deleteByMid(sessionDto.getSession_user_id(),mid);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 19:53 2018/7/10
     *添加收藏
    */
    @RequestMapping("/addCollect")
    public R addCollect(TbCollect tbCollect){
        //到缓存里面拿uid
        SessionDto sessionDto = getUserSession();
        tbCollect.setUid(sessionDto.getSession_user_id());
        tbCollect.setCreateDate(new Date());
        return cCenterService.addCollect(tbCollect);
    }

    /*======================我的积分==============================*/

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:03 2018/7/6
     * 跳转我的积分页面
    */
    @RequestMapping("/jumpScore")
    public ModelAndView jumpScore(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/myScore");
        return  mode;
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:46 2018/7/6
     *查询我的积分(收入、支出)
    */
    @RequestMapping("/findScoreInfo")
    public R findScoreInfo(int limit,int page,String direction){
        SessionDto sessionDto = getUserSession();
        return cCenterService.findScoreInfo(limit,page,direction,sessionDto.getSession_user_id());
    }

    /*==========================我的还款记录============================================*/
    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:21 2018/7/5
     * 跳转我的还款
     */
    @RequestMapping("/jumpMyPayback")
    public ModelAndView jumpMyPayback(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/repayLog");
        return  mode;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:56 2018/7/9
     * 查询我的还款记录(提前还款/正常还款)
     */
    @RequestMapping("/findMyPaybackPageInfo")
    public R findMyPaybackPageInfo(int limit, int page){
        SessionDto sessionDto = getUserSession();
        return cCenterService.findMyPaybackPageInfo(limit,page,sessionDto.getSession_user_id());
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:21 2018/7/5
     * 跳转我的还款详情
     */
    @RequestMapping("/jumpMyRepayDetail")
    public ModelAndView jumpMyRepayDetail(String id){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/repayDetail");
        mode.addObject("id",id);
        return  mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:44 2018/7/9
     * 查询还款详情
    */
    @RequestMapping("/findMyRepayDetail")
    public R findMyRepayDetail(String id){
        SessionDto sessionDto = getUserSession();
        return cCenterService.findMyRepayDetail(id,sessionDto.getSession_user_id());
    }

    /*===========================关于我们===================================*/
    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:21 2018/7/5
     * 跳转关于我们页面
     */
    @RequestMapping("/jumpAboutUs")
    public ModelAndView jumpAboutUs(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/Company profile");
        return  mode;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:21 2018/7/5
     * 跳转帮助页面
     */
    @RequestMapping("/jumpHelp")
    public ModelAndView jumpHelp(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/help");
        return  mode;
    }

    /*====================首页========================*/
    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 16:41 2018/7/5
     */
    @RequestMapping("/jumpMine")
    public ModelAndView jumpMine(){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> r = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = r.getData();
        if(ValidateUtil.isEmpty(userInfoDto.getmMerchantInfo())){ //消费者
            mode.addObject("invateCode",userInfoDto.getcUserInfo().getInvateCode());
            mode.setViewName("ftl/mineCenter");
        }else{  //商户
            mode.addObject("invateCode",userInfoDto.getcUserInfo().getInvateCode());
            mode.setViewName("ftl/mineMerchantCenter");
        }
        return  mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:49 2018/7/10
     * 加载个人信息资料
    */
    @RequestMapping("/findUserInfo")
    public R findUserInfo(){
        SessionDto sessionDto = getUserSession();
        return cCenterService.findUserInfo(sessionDto.getSession_user_id());
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:03 2018/7/6
     * 跳转授信合同
     */
    @RequestMapping("/jumpSxHt")
    public ModelAndView jumpSxHt(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        String osn = request.getServerName();
        OOperaInfo oOperaInfo = loginService.getOperaId(osn).getData();
        mode.addObject("operaInfo",oOperaInfo);
        mode.setViewName("ftl/agreement/card_pay");
        return  mode;
    }
}
