package com.tcxf.hbc.consumer.controller.order;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.UorderService;
import com.tcxf.hbc.consumer.vo.UorderVo;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
* @Author:HuangJun
* @Description  我的订单
 * @Date: 11:58 2018/7/9

*/
@RestController
@RequestMapping("/user/order")
public class UorderController extends BaseController {
    @Autowired
    UorderService uorderService;

    @Autowired
    LoginService loginService;



    /**
    * @Author:HuangJun
    * @Description
     * @Date: 9:58 2018/7/12
     *用户端我的订单
    */
    @RequestMapping(value = "/order")
    public ModelAndView order(){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> r = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = r.getData();
        if(ValidateUtil.isEmpty(userInfoDto.getmMerchantInfo())){ //消费者
            mode.setViewName("ftl/myOrder");
        }else{  //商户
            mode.setViewName("ftl/merchant/myOrder");
        }


        return  mode;
    }

    /**
    * @Author:HuangJun
    * @Description
     * @Date: 10:00 2018/7/12
     *商户端我的订单
    */
    @RequestMapping("/morder")
    public ModelAndView morder(){
        return  new ModelAndView("ftl/merchant/myOrder");
    }
    /**
     * 商查询订单  1支付订单 2商户订单
     * @param vo
     * @return
     */
    @RequestMapping(value = "/findByMid",method = {RequestMethod.POST,RequestMethod.GET})
    public R findByMid(UorderVo vo, HttpServletRequest request){
        R r = new R();
        SessionDto sessionDto = getUserSession();
        String id = "";
        if("1".equals(vo.getType())){
            id =sessionDto.getSession_user_id();
        }else{
            R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
            UserInfoDto d =dto.getData();
            id = d.getmMerchantInfo().getId();
        }
        r.setData( uorderService.findByUid(vo.getPage(),id,vo.getType()));
        return  r;
    }

    /**
     * 用户查询订单
     * @param vo
     * @return
     */
    @RequestMapping(value = "/findByUid",method = {RequestMethod.POST,RequestMethod.GET})
    public R findByUid(UorderVo vo, HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        R r = new R();
        r.setData( uorderService.findByUid(vo.getPage(), sessionDto.getSession_user_id(),vo.getType()));
        return  r;
    }

    /**
     * 查询用户订单详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById",method ={ RequestMethod.POST,RequestMethod.GET})
    public ModelAndView findById(String id){
        ModelAndView mode = new ModelAndView();
        R r = uorderService.findById(id);
        mode.addObject("entity", r.getData());
        mode.setViewName("ftl/orderDetail");
        return mode;
    }
    /**
     * 查询商户订单详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/mfindById",method ={ RequestMethod.POST,RequestMethod.GET})
    public ModelAndView mfindById(String id){
        ModelAndView mode = new ModelAndView();
        R r = uorderService.findById(id);
        mode.addObject("entity", r.getData());
        mode.setViewName("ftl/merchant/morderDetail");
        return mode;
    }


    /**
     * 商户统计交易
     * @param
     * @return
     */
    @RequestMapping(value = "/findStati",method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView findStati(HttpServletRequest request ){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
       UserInfoDto d =dto.getData();
        R r = uorderService.findStati(d.getmMerchantInfo().getId());
        mode.addObject("entity",r.getData());
        mode.setViewName("ftl/merchant/statistics");
        return mode;
    }

    /**
     * 交易详情页面
     * @param sumMoney
     * @return
     */
    @RequestMapping(value = "/findTrading",method = RequestMethod.GET)
    public ModelAndView findTrading(String sumMoney){
        ModelAndView mode  = new ModelAndView();
        mode.addObject("sumMoney",sumMoney);
        mode.setViewName("ftl/merchant/tradingDetail");
        return mode;
    }

    @RequestMapping(value = "/findTradingDeta",method = RequestMethod.POST)
    public R findTradingDeta(HttpServletRequest request,UorderVo vo){
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
        UserInfoDto d =dto.getData();
        d.getmMerchantInfo().getId();
        String mid=d.getmMerchantInfo().getId();;
        if("null".equals(vo.getCreateDateStr()) || vo.getCreateDateStr() ==null){
            vo.setCreateDateStr("null");
        }else{
            vo.setCreateDateStr(vo.getCreateDateStr()+" 00:00:00");
        }
        if("null".equals(vo.getModifyDateStr())|| vo.getModifyDateStr()==null){
            vo.setModifyDateStr("null");
        }else{
            vo.setModifyDateStr(vo.getModifyDateStr()+" 23:59:59");
        }
        if(vo.getSid()==null) {
            vo.setSid("null");
        }
        R r = new R();
        Page page1 = uorderService.findTradingDeta(vo.getPage(),mid, vo.getCreateDateStr(),vo.getModifyDateStr(),vo.getSid());
        r.setData(page1);
        return  r;
    }

    /**
     * 跳转结算订单页面
     * @return
     */
    @RequestMapping(value = "/findSett",method = RequestMethod.GET)
    public ModelAndView findSett(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
        UserInfoDto d =dto.getData();
        d.getmMerchantInfo().getId();
        String mid= d.getmMerchantInfo().getId();
        R r = uorderService.findStati(mid);
        mode.addObject("entity",r.getData());
        mode.setViewName("ftl/merchant/settleOrder");
        return mode;
    }
    /**
     * 结算订单
     * @param vo
     * @return
     */
    @RequestMapping(value = "/findSettlementDeta",method = RequestMethod.POST)
    public R findSettlementDeta(UorderVo vo,HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> dto  = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id());
        UserInfoDto d =dto.getData();
        R r =new R();
        Page page1 = uorderService.findSettlementDeta(vo.getPage(),d.getmMerchantInfo().getId());
        r.setData(page1);
        return r;
    }

    /***
     * 订单详情页面
     * @return
     */
    @RequestMapping(value = "/findsettleOrderDetail",method = RequestMethod.GET)
    public ModelAndView findsettleOrderDetail(String id) {
        ModelAndView mode = new ModelAndView();
        mode.addObject("sid", id);
        mode.setViewName("ftl/merchant/settleOrderDetail");
        return mode;
    }
}
