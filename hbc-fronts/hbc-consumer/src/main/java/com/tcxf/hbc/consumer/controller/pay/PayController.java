package com.tcxf.hbc.consumer.controller.pay;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.config.PayUrlPropertiesConfig;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.OoperainfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/pay")
public class PayController extends BaseController {
    @Autowired
    PayUrlPropertiesConfig payUrlPropertiesConfig;

    @Autowired
    private OoperainfoService ooperainfoService;

    @Autowired
    private LoginService loginService;

    /**
     * 商户收款码的生成
     *
     * @return
     */
    @RequestMapping("/goToQRCodePage")
    public ModelAndView goToQRCodePage(HttpServletRequest request) {
        SessionDto sessionDto = getUserSession();
        String basePath = request.getScheme() + "://" + request.getServerName() +
                ":" + request.getServerPort() + request.getContextPath() + "/";
        ModelAndView modelAndView = new ModelAndView("ftl/pay/merchantqrcode");
        UserInfoDto r = loginService.getUserAccountInfoByUserId(sessionDto.getSession_user_id()).getData();
        modelAndView.addObject("mName",r.getmMerchantInfo().getSimpleName());
        modelAndView.addObject("gotoPayUrl", basePath + "pay/goToPay?mId=" + sessionDto.getSession_m_id());
        return modelAndView;
    }

    /**
     * 前往支付端
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/goToPay")
    public ModelAndView goToPay(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String mId = request.getParameter("mId");
        SessionDto sessionDto = getUserSession();
        return new ModelAndView("redirect:" + payUrlPropertiesConfig.getGotopayurl() + "/payment/pay/goTowxGetCode?userId=" + sessionDto.getSession_user_id() + "&mId=" + mId);
    }

    /**
     * 前往支付引导页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/goToPayBootpage")
    @AuthorizationNO
    public ModelAndView goToPayBootpage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return  new ModelAndView("ftl/pay/payBootpage");
    }

}

