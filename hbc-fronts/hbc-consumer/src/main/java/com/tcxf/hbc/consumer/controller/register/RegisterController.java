package com.tcxf.hbc.consumer.controller.register;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.HttpUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.consumer.common.token.TokenManager;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.controller.empo.EmpoController;
import com.tcxf.hbc.consumer.controller.index.IndexInfoController;
import com.tcxf.hbc.consumer.dto.RegisterDto;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.CMerchantInfoService;
import com.tcxf.hbc.consumer.feign.EmpoService;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.RegisterService;
import com.tcxf.hbc.consumer.vo.MerchantVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.Map;

@RestController
@RequestMapping("/register")
public class RegisterController extends BaseController {

    @Autowired
    private RegisterService registerService;
    @Autowired
    private CMerchantInfoService cMerchantInfoService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private EmpoService empoService;
    @Autowired
    private IndexInfoController indexInfoController;

    @Autowired
    private EmpoController empoController;

    /**
     * 跳转注册界面,获取到openId
     * @return
     */
    @RequestMapping("/goRegister")
    @AuthorizationNO
    public ModelAndView goRegister(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String openId = request.getParameter("openId");
        String invateCode = request.getParameter("invateCode");
        if(ValidateUtil.isEmpty(openId)){
            //2、通过微信跳转 获取openId
            String basePath = request.getScheme() + "://"+request.getServerName() +
                    ":" + request.getServerPort() + request.getContextPath() + "/";
            //根据二级域名获取OOperaInfo
            String osn = request.getServerName();
            OOperaInfo oOperaInfo= loginService.getOperaId(osn).getData();
            String redirectUrl = HttpUtil.buildSendRedUrl(basePath , oOperaInfo.getAppid());
            redirectUrl=redirectUrl.replace("redirectUrlValue", "register/goRegisterwxcode");
            redirectUrl=redirectUrl.replace("statecode", ValidateUtil.isEmpty(invateCode)?"":invateCode);
            response.sendRedirect(redirectUrl);
        }
        ModelAndView modelAndView =  new ModelAndView("ftl/register/loadRegister");
        modelAndView.addObject("openId",openId);
        modelAndView.addObject("invateCode",invateCode);
        return modelAndView;
    }

    /**
     * 跳转注册界面,获取到openId
     * @return
     */
    @RequestMapping("/goRegisterwxcode")
    @AuthorizationNO
    public ModelAndView goRegisterwxcode(HttpServletRequest request, HttpServletResponse response)
    {
        //获取到code值拿到openId
        String code = request.getParameter("code");
        //根据二级域名获取OOperaInfo
        String osn = request.getServerName();
        OOperaInfo oOperaInfo= loginService.getOperaId(osn).getData();
        String openId = HttpUtil.getOpenId(code , oOperaInfo.getAppid() , oOperaInfo.getAppsecret());
        String invateCode = request.getParameter("state");
        ModelAndView modelAndView =  new ModelAndView("ftl/register/loadRegister");
        modelAndView.addObject("openId",openId);
        modelAndView.addObject("invateCode",invateCode);
        return modelAndView;
    }

    /**
     * 发送验证码
     * @param request
     * @return
     */
    @RequestMapping("/getCode")
    @AuthorizationNO
    public R getYzm(HttpServletRequest request)
    {
        //map获取request里面的值
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String mobile = (String) paramMap.get("mobile");
        String type = (String) paramMap.get("type");
        //调用发送验证码方法
        return registerService.getYzm(mobile,type);
    }

    /**
     * 注册
     * @return
     */
    @RequestMapping("/register")
    @AuthorizationNO
    public R register(@RequestBody RegisterDto registerDto , HttpServletRequest request)
    {
        //获取二级域名
        String osn = request.getServerName();
		registerDto.setOpsn1(osn);
        R<UserInfoDto> r = registerService.register(registerDto);
        if(r.getCode()==1){
            return r;
        }
        UserInfoDto userInfoDto = r.getData();
        OOperaInfo operaInfo = loginService.getOperaId(osn).getData();
        userInfoDto.getcUserInfo().setWxOpenid(registerDto.getOpenId());
        createSession(userInfoDto,operaInfo.getId());
        return R.newOK();
    }

    /**
     * 跳转到输入支付密码界面
     * @return
     */
    @RequestMapping("/paymentCode")
    public ModelAndView paymentCode(String type)
    {
        ModelAndView modelAndView = new ModelAndView("ftl/register/paymentCode");
        modelAndView.addObject("type",type);
        return modelAndView;
    }

    /**
     * 添加账户表
     * 生成钱包表
     * 生成账户和用户信息关联表
     * @return
     */
    @RequestMapping("/addTbUserAccount")
    public R addTbUserAccount(RegisterDto registerDto)
    {
        R r = new R();
        //从session里把id拿出来查询userInfo
        SessionDto sessionDto = getUserSession();
        registerDto.setId(sessionDto.getSession_user_id());
        //填写支付密码添加关联表
        r = registerService.AddTbUserAccount(registerDto);
        return r;
    }

    /**
     * 跳转到快速授信界面
     * @return
     */
    @RequestMapping("/goFastEmpo")
//    @AuthorizationNO
    public ModelAndView fastEmpo(HttpServletRequest request) throws Exception
    {

        CreditVo creditVo = new CreditVo();
        SessionDto sessionDto = getUserSession();
        if (!ValidateUtil.isEmpty(sessionDto.getSession_user_id())){
            creditVo.setUid(sessionDto.getSession_user_id());
        }else if (!ValidateUtil.isEmpty(request.getParameter("uId"))){
            creditVo.setUid(request.getParameter("uId"));
        }
        if (!ValidateUtil.isEmpty(sessionDto.getSession_opera_id())){
            creditVo.setOid(sessionDto.getSession_opera_id());
        }else if (!ValidateUtil.isEmpty(request.getParameter("oId"))){
            creditVo.setOid(request.getParameter("oId"));
        }

//        creditVo.setUid("4");
//        creditVo.setUid("1017603975603572738");
        R<CUserInfo> r = empoService.getUserInfo(creditVo);

        //查询该用户授信记录
        R<BaseCreditInfo> r1 = empoService.getBaseCreditInfo(creditVo);

        ModelAndView modelAndView = null;

        //获取资金端的授信余额详情
        R<FMoneyInfo> r2 = empoService.getFMoneyInfoByFid(creditVo);

        if (!ValidateUtil.isEmpty(r2.getData()) && "0".equals(r2.getData().getStatus())){               //资金端未采用限额授信
            if (ValidateUtil.isEmpty(r1.getData())){
                //此处是区分用户类型，1.用户 2.商户
                if ("1".equals(r.getData().getUserAttribute())){
                    return new ModelAndView("ftl/empo/fastEmpo");
                }else {
                    modelAndView = new ModelAndView("ftl/empo/fastEmpoProtocol");
//                return modelAndView;
                }
            }else {
                modelAndView = empoController.goOperatorEmpo(request);
//                return empoController.goOperatorEmpo();
            }
        }else if (!ValidateUtil.isEmpty(r2.getData()) && "1".equals(r2.getData().getStatus())){         //资金端采用限额授信
            //获取资金端已使用授信额度
            R<BigDecimal> r3 = empoService.getFMoneyOtherInfoByFid(creditVo);

            if (!ValidateUtil.isEmpty(r3.getData())){
                //当日授信额度减去已授信额度
                BigDecimal b1 = r2.getData().getDayMoney().subtract(r3.getData());

                int i1 = b1.compareTo(new BigDecimal("0"));
                int i2 = b1.compareTo(new BigDecimal("1500"));

                //判断当日授信剩余额度是否满足授信条件
                if ((i1 == 1 || i1 == 0) && i2 == -1){          //当日剩余授信额度无法满足授信

                    modelAndView = indexInfoController.Goindex(request);
//                return indexInfoController.Goindex(request);
                }else {                                         //当日剩余授信额度可以满足授信
                    if (ValidateUtil.isEmpty(r1.getData())){
                        //此处是区分用户类型，1.用户 2.商户
                        if ("1".equals(r.getData().getUserAttribute())){
                            modelAndView = new ModelAndView("ftl/empo/fastEmpo");
//                        return new ModelAndView("ftl/empo/fastEmpo");
                        }else {
                            modelAndView = new ModelAndView("ftl/empo/fastEmpoProtocol");
//                    return modelAndView;
                        }
                    }else {
                        modelAndView = empoController.goOperatorEmpo(request);
//                    return empoController.goOperatorEmpo();
                    }
                }
            }
        }
        return modelAndView;
    }

    /**
     * 跳转到快速授信界面
     * @return
     */
    @RequestMapping("/goFastEmpoUser")
//    @AuthorizationNO
    public ModelAndView goFastEmpoUser(HttpServletRequest request)
    {
        CreditVo creditVo = new CreditVo();
        SessionDto sessionDto = getUserSession();
        creditVo.setUid(sessionDto.getSession_user_id());
        creditVo.setOid(sessionDto.getSession_opera_id());

        //查询该用户授信记录
        R<BaseCreditInfo> r1 = empoService.getBaseCreditInfo(creditVo);

        //当日授信额度是否满足授信条件
        Boolean ismeet = true;

        //获取资金端的授信余额详情
        R<FMoneyInfo> r2 = empoService.getFMoneyInfoByFid(creditVo);

        if (!ValidateUtil.isEmpty(r2.getData()) && "1".equals(r2.getData().getStatus())) {         //资金端采用限额授信
            R<BigDecimal> r3 = empoService.getFMoneyOtherInfoByFid(creditVo);

            //当日授信额度减去已授信额度
            BigDecimal b1 = r2.getData().getDayMoney().subtract(r3.getData());

            int i1 = b1.compareTo(new BigDecimal("0"));
            int i2 = b1.compareTo(new BigDecimal("1500"));


            //判断当日授信剩余额度是否满足授信条件
            if ((i1 == 1 || i1 == 0) && i2 == -1) {          //当日剩余授信额度无法满足授信
                ismeet = false;
            }

        }

        if (ismeet){
            if (ValidateUtil.isEmpty(r1.getData())){
                Boolean isjump = ValidateUtil.isEmpty(request.getParameter("isjump")) ? true : Boolean.parseBoolean(request.getParameter("isjump"));
                ModelAndView modelAndView = new ModelAndView("ftl/empo/fastEmpo");
                modelAndView.addObject("ismeet", ismeet);
                modelAndView.addObject("isjump", isjump);
                return modelAndView;
            }else {
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.addObject("ismeet", ismeet);
                return modelAndView = empoController.goOperatorEmpo(request);
            }
        }else {
            ModelAndView modelAndView = new ModelAndView("ftl/empo/lackOfCredit");
            return modelAndView;
        }




    }

    /**
     *  跳转到提交商户信息界面
     */
    @RequestMapping("/addMerchantInfo")
    @AuthorizationNO
    public ModelAndView addMerchantInfo()
    {
        ModelAndView modelAndView = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        UserInfoDto userInfoDto = loginService.userAccountInfo(sessionDto.getSession_account_id()).getData();
        sessionDto.setSession_m_id(userInfoDto.getmMerchantInfo().getId());
        refreshToken(sessionDto);
        R r2 = cMerchantInfoService.QueryMerchantInfo(userInfoDto.getmMerchantInfo().getId());
        R r1 = cMerchantInfoService.QueryMerchantCatgory();
        modelAndView.addObject("m",r2.getData());
        modelAndView.addObject("list",r1.getData());
        modelAndView.addObject("type" , "1");
        modelAndView.setViewName("ftl/merchant/merchantInfo");
        return modelAndView;
    }

    /**
     * 商户注册跳转到线上协议界面
     */
    @RequestMapping("/goMerchantAgreement")
    @AuthorizationNO
    public ModelAndView goMerchantAgreement(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("ftl/register/merchantAgreement");
        //获取商户信息
        SessionDto sessionDto = getUserSession();
        UserInfoDto userInfoDto = loginService.userAccountInfo(sessionDto.getSession_account_id()).getData();
        MMerchantInfo mMerchantInfo = userInfoDto.getmMerchantInfo();
        modelAndView.addObject("merchantDto" , mMerchantInfo);
        //获取运营商信息
        String osn = request.getServerName();
        OOperaInfo operaInfo = loginService.getOperaId(osn).getData();
        modelAndView.addObject("operaInfo" , operaInfo);
        TbBindCardInfo tbBindCardInfo = loginService.getTbBindCardInfoByOid(operaInfo.getId()).getData();
        modelAndView.addObject("tbBindCardInfo" , tbBindCardInfo);
        return modelAndView;
    }
}
