package com.tcxf.hbc.consumer.controller.spilt;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.SpiltService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 我要分期
 * jiangyong
 * 2018年7月21日17:13:41
 */
@RestController
@RequestMapping("/user/spilt")
public class spiltController extends BaseController {

    @Autowired
    private SpiltService spiltService;

    @Autowired
    private LoginService loginService;

    @RequestMapping("/qu")
    public ModelAndView qu(){
//        ModelAndView mode=new ModelAndView();
        return  new  ModelAndView("ftl/bill/divide");
    }


    /**
     * 当月分期账单
     * @return
     */
    @RequestMapping("/QuerySpilt")
    public R QuerySpilt(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String mid = userInfoDto.getmMerchantInfo().getId();
//        String uid="2";
//        String oid="1014809072980946945";
        return  spiltService.QuerySpilt(uid,oid,DateUtil.getDateTimeforMonth(new Date()));
    }

    /**
     * 369...添加分期
     */
    @RequestMapping("/updateByStages")
    public  R updateByStages(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String fid = userInfoDto.getcUserRefCon().getFid();
//        String uid="2";
//        String oid="1014809072980946945";
//        String fid="1014809990090682370";
        String splitMonth = request.getParameter("splitMonth");
        if(splitMonth==null || splitMonth.equals("")){
            splitMonth="3";
        }
        return  spiltService.ByStages(uid,oid,DateUtil.getDateTimeforMonth(new Date()),fid,splitMonth);

    }

    /**
     * 分期手续费
     * @param request
     * @return
     */
    @RequestMapping("/QuertFQLV")
    public  R QuertFQLV(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String mid = userInfoDto.getmMerchantInfo().getId();
        String fid = userInfoDto.getcUserRefCon().getFid();
//        String oid="1014809072980946945";
        //String fid="1014809990090682370";
        return spiltService.QueryFQ(oid,fid);
    }


    /**
     * 分期
     * @param request
     * @return
     */
    @RequestMapping("/QueryMon")
    public  R QueryMon(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        String uid = sessionDto.getSession_user_id();
        String oid = sessionDto.getSession_opera_id();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
      //  String mid = userInfoDto.getmMerchantInfo().getId();
        String fid = userInfoDto.getcUserRefCon().getFid();
//        String uid="2";
//        String oid="1014809072980946945";
//        String fid="1014809990090682370";
        String splitMonth = request.getParameter("splitMonth");
        if(splitMonth==null || splitMonth.equals("")){
            splitMonth="3";
        }
        return  spiltService.getDiffSplitAmount(uid,oid,fid,splitMonth);
    }


    /**
     * 校验密码 确认分期
     */
    @RequestMapping("/eckPassWords")
    public R eckPassWords(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        R<UserInfoDto> userInfoDtoR = loginService.userAccountInfo(sessionDto.getSession_account_id());
        UserInfoDto userInfoDto = userInfoDtoR.getData();
        String accountId  = userInfoDto.getTbUserAccount().getId();
//        String accountId = request.getParameter("accountId");
        String passWord = request.getParameter("passWord");
        return spiltService.eckPassWords(accountId ,passWord);
    }


}
