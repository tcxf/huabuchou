package com.tcxf.hbc.consumer.controller.userInfo;

import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.CUserInfoDto;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 我的页面的设置
 * liaozeyong
 * 2018年7月21日10:43:00
 */
@RestController
@RequestMapping("/user/userInfo")
public class userInfoController extends BaseController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private UserInfoService userInfoService;
    /**
     * 跳转到修改个人详情信息界面
     * @return
     */
    @RequestMapping("/selectUserInfo")
    public ModelAndView selectUserInfo()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/baseInfo");
        String sex = "男";
        SessionDto sessionDto = getUserSession();
        CUserInfoDto cUserInfoDto = userInfoService.selectUserInfoDto(sessionDto.getSession_user_id()).getData();
        if ("2".equals(cUserInfoDto.getSex()))
        {
            sex = "女";
        }
        modelAndView.addObject("sex",sex);
        modelAndView.addObject("cUserInfoDto",cUserInfoDto);
        return modelAndView;
    }

    /**
     * 修改个人详情
     * @param cUserInfoDto
     * @return
     */
    @RequestMapping("/updateUserInfo")
    public R updateUserInfo(CUserInfoDto cUserInfoDto)
    {
        SessionDto sessionDto = getUserSession();
        cUserInfoDto.setId(sessionDto.getSession_user_id());
        R r = userInfoService.updateUserInfo(sessionDto.getSession_account_id(),cUserInfoDto);
        return r;
    }

    /**
     * 跳转用户设置页面
     * @return
     */
    @RequestMapping("/userSetting")
    public ModelAndView userSetting()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/userSetting");
        return modelAndView;
    }

    /**
     * 跳转到商户设置界面
     * @return
     */
    @RequestMapping("/goMerchantSetting")
    public ModelAndView goMerchantSetting()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/merchantSetting");
        return modelAndView;
    }

    /**\
     * 验证手机
     * @return
     */
    @RequestMapping("/yzm")
    public R yzm(HttpServletRequest request){
        String mobile = request.getParameter("mobile");
        String yCode = request.getParameter("yCode");
        //判断手机号验证码
        R r = userInfoService.selectMobile(getUserSession().getSession_account_id() , mobile , yCode);
        if (r.getCode() != 0)
        {
            return r;
        }
        r.setData(mobile);
        return r;
    }

/*===============================修改支付密码=====================================*/

    /**
     * 跳转验证修改支付密码界面
     * @return
     */
    @RequestMapping("/goUpdatePaymentCode")
    public ModelAndView goUpdatePaymentCode()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/goUpdatePaymentCode");
        return modelAndView;
    }

    /**
     * 跳转到设置修改支付密码界面
     * @return
     */
    @RequestMapping("/setUpdatePaymentCode")
    public ModelAndView setUpdatePaymentCode()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/updatePaymentCode");
        return modelAndView;
    }

    /**
     * 修改支付密码方法
     * @return
     */
    @RequestMapping("/updatePaymentCode")
    public R updatePaymentCode(String paymentCode)
    {
        R r = userInfoService.updatePaymentCode(getUserSession().getSession_account_id(),paymentCode);
        return r;
    }

/*===============================修改登陆密码=====================================*/

    /**
     * 跳转到修改登录密码验证界面
     * @return
     */
    @RequestMapping("/goUpdatePassword")
    @AuthorizationNO
    public ModelAndView goUpdatePassword()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/goUpdatePassword");
        return modelAndView;
    }

    /**
     * 跳转到设置修改登陆密码界面
     * @return
     */
    @RequestMapping("/setUpdatePassword")
    public ModelAndView setUpdatePassword(String mobile)
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/updatePassword");
        modelAndView.addObject("mobile",mobile);
        return modelAndView;
    }
    /**
     * 修改登陆密码方法
     */
    @RequestMapping("/updatePassword")
    @AuthorizationNO
    public R updatePassword(String mobile , String password)
    {
        R r = userInfoService.updatePassword(mobile , password);
        return r;
    }

/*===============================忘记密码=====================================*/

    /**
     * 跳转到忘记密码短信验证界面
     * @return
     */
    @RequestMapping("/goForgetPassword")
    @AuthorizationNO
    public ModelAndView goForgetPassword()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/goForgetPassword");
        return modelAndView;
    }

    /**
     * 忘记密码短信验证
     * @param mobile
     * @param yCode
     * @return
     */
    @RequestMapping("/yzForgetPassword")
    @AuthorizationNO
    public R yzForgetPassword(String mobile , String yCode)
    {
        R r = userInfoService.yzForgetPassword( mobile , yCode);
        r.setData(mobile);
        return r;
    }

    /**
     * 跳转到忘记密码设置密码界面
     * @return
     */
    @RequestMapping("/setForgetPassword")
    @AuthorizationNO
    public ModelAndView setForgetPassword(String mobile)
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/updatePassword");
        modelAndView.addObject("mobile" , mobile);
        return modelAndView;
    }

/*===============================立即开店=====================================*/

    /**
     * 跳转用户设置立即开店界面
     * @return
     */
    @RequestMapping("/goUserSetPassword")
    public ModelAndView goUserSetPassword()
    {
        ModelAndView modelAndView = new ModelAndView("ftl/my/userSetPassword");
        return modelAndView;
    }

    /**
     * 用户设置登陆密码
     * @return
     */
    @RequestMapping("/userSetPassword")
    public R userSetPassword(String password)
    {
        SessionDto sessionDto = getUserSession();
        R r = userInfoService.userSetPassword(sessionDto.getSession_account_id() , password ,sessionDto.getSession_opera_id());
        return r;
    }

/*===============================退出登陆=====================================*/

    /**
     * 退出登陆
     * @return
     */
    @RequestMapping("/logout")
    public R logout(HttpServletRequest request){
        SessionDto sessionDto = getUserSession();
        R r = loginService.logout(sessionDto.getSession_account_id(),sessionDto.getSession_user_id());
        if (r.getCode() == 0)
        {
            //清空redis
            deleteToken();
            return R.newOK("退出成功",null);
        }
        return  R.newErrorMsg("退出失败");
    }
}
