package com.tcxf.hbc.consumer.controller.wallet;

import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.basecontroller.BaseController;
import com.tcxf.hbc.consumer.dto.SessionDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import com.tcxf.hbc.consumer.feign.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wallet")
public class WalletController extends BaseController {

    @Autowired
    private WalletService walletService;
    @Autowired
    public LoginService loginService;


    /**
     * 跳转钱包页面-用户，商户
     *
     * @return
     */
    @GetMapping("/findwallet")
    public ModelAndView findwallet(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<TbWallet> r = walletService.get(sessionDto.getSession_account_id());
        TbWallet tbWallet  = r.getData();
        tbWallet.setTotalAmount(new Calculate(tbWallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        m.addObject("w", tbWallet);
        m.setViewName("ftl/wallet");
        return m;
    }

    /**
     * 查询钱包的收入支出记录
     *
     * @param request
     * @return
     */
    @RequestMapping("/findWalletIncome")
    @ResponseBody
    public R<List<TbWalletIncome>> findWalletIncome(HttpServletRequest request) {
        R<List<TbWalletIncome>> r = new R<>();
        r = walletService.getfindWalletIncome(request.getParameter("utype"), request.getParameter("wid"));
        return r;
    }

    @RequestMapping("/findWalletwithdrawals")
    public R findWalletwithdrawalsList(HttpServletRequest request) {
        R<Object> r = new R<>();
        r = walletService.findWalletwithdrawalsList(request.getParameter("uid"));
        return r;
    }

    /**
     * 跳转钱包提现页面
     *
     * @return
     */
    @RequestMapping("/WalletTx")
    public ModelAndView WalletTx(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        SessionDto sessionDto = getUserSession();
        R<TbWallet> r = walletService.get(sessionDto.getSession_account_id());
        TbWallet tbWallet=  r.getData();
        tbWallet.setTotalAmount(new Calculate(tbWallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        TbUserAccount account = walletService.findaccount(sessionDto.getSession_account_id());
        if (account.getUserTypeId().equals("0")){
            HashMap<String, Object> map = new HashMap<>();
            map.put("utype", 1);
            R bYuser = walletService.findBYuser(map);
            m.addObject("c", bYuser.getData());
            m.addObject("m",null);
        }else if (account.getUserTypeId().equals("1")){
            HashMap<String, Object> map = new HashMap<>();
            map.put("utype", 2);
            R bYmerchant = walletService.findBYmerchant(map);
            List<TbWalletCashrate> merchant =(List<TbWalletCashrate>) bYmerchant.getData();
            m.addObject("m",JsonTools.toJson(merchant));
            TbWalletCashrate tbWalletCashrate = new TbWalletCashrate();
            tbWalletCashrate.setPresentType("3");
            tbWalletCashrate.setPresentContent("0");
            m.addObject("c",tbWalletCashrate);
        }
        R bindCardinfo = walletService.findBindCardinfo(sessionDto.getSession_account_id(),TbBindCardInfo.CONTRACTTYPE_1);
        m.addObject("bind", bindCardinfo.getData());
        m.addObject("w", tbWallet);
        m.addObject("a", account);
        m.setViewName("ftl/wallettx");
        return m;
    }

    /**
     * 跳转到发短信的页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/findtxdx")
    public ModelAndView findtxdx(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R bind = walletService.findBind(request.getParameter("bid"));
        m.addObject("sjtxje", request.getParameter("sjtxje"));
        m.addObject("txsxje", request.getParameter("txsxje"));
        m.addObject("txje", request.getParameter("txje"));
        m.addObject("b", bind.getData());
        m.setViewName("ftl/wallettxdx");
        return m;
    }

    /**
     * 发送验证码
     *
     * @param request
     * @return
     */
    @RequestMapping("/getsys")
    public R getsys(HttpServletRequest request) {
        R<Object> r = new R<>();
        r = walletService.getsys(request.getParameter("mobile"), request.getParameter("type"));
        return r;
    }

    /**
     * 钱包提现
     *
     * @param request
     * @return
     */
    @RequestMapping("/walletdoReg")
    public R walletdoReg(HttpServletRequest request) {
        R<Object> r = new R<>();
        SessionDto sessionDto = getUserSession();
        Map<String, Object> map = new HashMap<>();
        map.put("uid", sessionDto.getSession_account_id());
        map.put("type", request.getParameter("type"));
        map.put("yCode", request.getParameter("yCode"));
        map.put("bid", request.getParameter("bid"));
        map.put("txje", request.getParameter("txje"));
        map.put("sjtxje", request.getParameter("sjtxje"));
        map.put("txsxje", request.getParameter("txsxje"));
        r = walletService.walletdoReg(map);
        return r;
    }

    /**
     * 跳转提现成功-用户，商户
     *
     * @return
     */
    @GetMapping("/waleettxwc")
    public ModelAndView waleettxwc(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/walletwc");
        return m;
    }

}
