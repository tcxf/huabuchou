package com.tcxf.hbc.consumer.dto;

public class RegisterDto {

    private String mobile;//手机号
    private String openId;//openId
    private String ycode;//验证码
    private String type;//区分是商户注册还是会员注册
    private String invateCode;//邀请码
    private String password;//密码
    private String opsn1;//二级域名
    private String newpwd;//支付密码
    private String id;//用户表id

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getYcode() {
        return ycode;
    }

    public void setYcode(String ycode) {
        this.ycode = ycode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInvateCode() {
        return invateCode;
    }

    public void setInvateCode(String invateCode) {
        this.invateCode = invateCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOpsn1() {
        return opsn1;
    }

    public void setOpsn1(String opsn1) {
        this.opsn1 = opsn1;
    }

    public String getNewpwd() {
        return newpwd;
    }

    public void setNewpwd(String newpwd) {
        this.newpwd = newpwd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
