package com.tcxf.hbc.consumer.dto;

import java.io.Serializable;

public class SessionDto implements Serializable {
    public static final String COOKIENAME = "sessionDto";
    private String session_user_id;
    private String session_account_id;
    private String session_openId;
    private String session_opera_id;
    private String session_m_id;

    public String getSession_user_id() {
        return session_user_id;
    }

    public void setSession_user_id(String session_user_id) {
        this.session_user_id = session_user_id;
    }

    public String getSession_account_id() {
        return session_account_id;
    }

    public void setSession_account_id(String session_account_id) {
        this.session_account_id = session_account_id;
    }

    public String getSession_openId() {
        return session_openId;
    }

    public void setSession_openId(String session_openId) {
        this.session_openId = session_openId;
    }

    public String getSession_opera_id() {
        return session_opera_id;
    }

    public void setSession_opera_id(String session_opera_id) {
        this.session_opera_id = session_opera_id;
    }

    public String getSession_m_id() {
        return session_m_id;
    }

    public void setSession_m_id(String session_m_id) {
        this.session_m_id = session_m_id;
    }
}
