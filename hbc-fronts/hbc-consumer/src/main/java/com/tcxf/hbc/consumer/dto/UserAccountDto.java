package com.tcxf.hbc.consumer.dto;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;

public class UserAccountDto extends TbUserAccount {
    private MMerchantInfo mMerchantInfo;

    private CUserInfo cUserInfo;

    private String conId;

    private String userType;//用户类型

    public MMerchantInfo getmMerchantInfo() {
        return mMerchantInfo;
    }

    public void setmMerchantInfo(MMerchantInfo mMerchantInfo) {
        this.mMerchantInfo = mMerchantInfo;
    }

    public CUserInfo getcUserInfo() {
        return cUserInfo;
    }

    public void setcUserInfo(CUserInfo cUserInfo) {
        this.cUserInfo = cUserInfo;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getConId() {
        return conId;
    }

    public void setConId(String conId) {
        this.conId = conId;
    }
}
