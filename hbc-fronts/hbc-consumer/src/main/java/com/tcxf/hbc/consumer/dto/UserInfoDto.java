package com.tcxf.hbc.consumer.dto;

import com.tcxf.hbc.common.entity.*;

public class UserInfoDto {
    private TbUserAccount tbUserAccount;
    private TbUserAccountTypeCon tbUserAccountTypeCon;
    private CUserInfo cUserInfo;
    private MMerchantInfo mMerchantInfo;
    private MMerchantOtherInfo mMerchantOtherInfo;

    private CUserRefCon cUserRefCon;

    public CUserRefCon getcUserRefCon() {
        return cUserRefCon;
    }

    public void setcUserRefCon(CUserRefCon cUserRefCon) {
        this.cUserRefCon = cUserRefCon;
    }

    public TbUserAccount getTbUserAccount() {
        return tbUserAccount;
    }

    public void setTbUserAccount(TbUserAccount tbUserAccount) {
        this.tbUserAccount = tbUserAccount;
    }

    public TbUserAccountTypeCon getTbUserAccountTypeCon() {
        return tbUserAccountTypeCon;
    }

    public void setTbUserAccountTypeCon(TbUserAccountTypeCon tbUserAccountTypeCon) {
        this.tbUserAccountTypeCon = tbUserAccountTypeCon;
    }

    public CUserInfo getcUserInfo() {
        return cUserInfo;
    }

    public void setcUserInfo(CUserInfo cUserInfo) {
        this.cUserInfo = cUserInfo;
    }

    public MMerchantInfo getmMerchantInfo() {
        return mMerchantInfo;
    }

    public void setmMerchantInfo(MMerchantInfo mMerchantInfo) {
        this.mMerchantInfo = mMerchantInfo;
    }

    public MMerchantOtherInfo getmMerchantOtherInfo() {
        return mMerchantOtherInfo;
    }

    public void setmMerchantOtherInfo(MMerchantOtherInfo mMerchantOtherInfo) {
        this.mMerchantOtherInfo = mMerchantOtherInfo;
    }
}
