package com.tcxf.hbc.consumer.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.BankServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "hbc-mc-service", fallback = BankServiceFallbackImpl.class)
public interface BankSercice {

    /**
     * 通过账户表ID查询所有绑卡信息
     * @param oid
     * @return
     */
    @RequestMapping(value="bank/findUbank/{oid}/{type}",method ={RequestMethod.POST})
    R findbank(@PathVariable("oid") String oid,@PathVariable("type") String type);

    /**
     * 通过绑卡表ID删除绑卡
     * @param id
     * @return
     */
    @RequestMapping(value="bank/delbank/{id}",method ={RequestMethod.POST})
    R delbank(@PathVariable("id") String id);

    /**
     * 绑卡
     * @param id
     * @param mobile
     * @param bankName
     * @param bankCard
     * @param idCard
     * @param realName
     * @return
     */

    @RequestMapping(value="bank/savebank",method ={RequestMethod.POST})
    R savebank(@RequestBody TbBindCardInfo tbBindCardInfo);
}
