package com.tcxf.hbc.consumer.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/24 10:32
 * @Description:
 */
@Service
public class BaseService {

    @Autowired
    LoginService loginService;

    public LoginService getLoginService() {
        return loginService;
    }
}
