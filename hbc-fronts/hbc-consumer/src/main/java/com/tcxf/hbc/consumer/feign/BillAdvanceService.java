package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.consumer.dto.QuickPayPayagreeconfirmResData;
import com.tcxf.hbc.consumer.feign.fallback.BillAdvanceServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.TreeMap;


/**
 * @author YWT_tai
 * @Date :Created in 20:27 2018/7/12
 */

@FeignClient(name = "hbc-trans-service", fallback = BillAdvanceServiceImpl.class)
public interface BillAdvanceService {

    @RequestMapping(value="/billAndAdvance/PaybackMoney",method ={RequestMethod.POST} )
    R PaybackMoney(@RequestBody PaybackMoneyDto paybackMoneyDto);

    @RequestMapping(value="/billAndAdvance/AdvancePaybackMoney",method ={RequestMethod.POST} )
    R AdvancePaybackMoney(PaybackMoneyDto paybackMoneyDto);

    @RequestMapping(value="/billAndAdvance/findRepaymentPlanByRdId/{rdid}",method ={RequestMethod.POST} )
    R<TbRepaymentPlan> findRepaymentPlanByRdId(@PathVariable("rdid")String rdid);

    @RequestMapping(value="/pay/onPayagreeconfirm",method ={RequestMethod.POST} )
    R<QuickPayPayagreeconfirmResData> confirmAdvanceOrAlreadyBillPay(@RequestBody PayagreeconfirmDto payagreeconfirmDto);

    @RequestMapping(value="/billAndAdvance/updateTradingInfo",method ={RequestMethod.POST} )
    R updateTradingInfo(@RequestBody TreeMap<String, String> map);

    @RequestMapping(value="/billAndAdvance/updateNormalPay",method ={RequestMethod.POST} )
    R updateNormalPay(@RequestBody TreeMap<String, String> paras);
}
