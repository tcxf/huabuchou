package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.consumer.feign.fallback.CBillServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 20:27 2018/7/12
 */

@FeignClient(name = "hbc-mc-service", fallback = CBillServiceFallbackImpl.class)
public interface CBillService {

    @RequestMapping(value="/consumerBill/findAvailableCreditInfo/{uid}/{oid}",method ={RequestMethod.POST} )
    R<HashMap<String, Object>> findAvailableCreditInfo(@PathVariable("uid")String uid, @PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findMineWaitPayInfo/{uid}/{oid}",method ={RequestMethod.POST} )
    R findMineWaitPayInfo(@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findPlanDetailInfo/{planId}/{waitpay}",method ={RequestMethod.POST} )
    R<Map<String,Object>> findPlanDetailInfo(@PathVariable("planId")String planId,@PathVariable("waitpay") String waitpay);

    @RequestMapping(value="/consumerBill/findUnOutBill/{uid}/{oid}",method ={RequestMethod.POST} )
    R findUnOutBill(@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findTradeDetailInfo/{id}",method ={RequestMethod.POST} )
    R findTradeDetailInfo(@PathVariable("id")String id);

    @RequestMapping(value="/consumerBill/findDivideDetail/{id}",method ={RequestMethod.POST} )
    R findDivideDetail(@PathVariable("id")String id);

    @RequestMapping(value="/consumerBill/findPlanYears/{uid}/{oid}",method ={RequestMethod.POST} )
    R findPlanYears(@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findPlanBillByYear/{dateYear}/{uid}/{oid}",method ={RequestMethod.POST} )
    R findPlanBillByYear(@PathVariable("dateYear")String dateYear,@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findHistoryBillYears/{uid}/{oid}",method ={RequestMethod.POST} )
    R findHistoryBillYears(@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findHistoryBillByYear/{dateYear}/{uid}/{oid}",method ={RequestMethod.POST} )
    R findHistoryBillByYear(@PathVariable("dateYear")String dateYear,@PathVariable("uid")String uid,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/findHistoryBillDetailInfo/{isSplit}/{uid}/{id}/{oid}",method ={RequestMethod.POST} )
    R findHistoryBillDetailInfo(@PathVariable("isSplit")String isSplit, @PathVariable("uid")String uid, @PathVariable("id") String id,@PathVariable("oid")String oid);

    @RequestMapping(value="/consumerBill/PaybackMoney",method ={RequestMethod.POST} )
    R PaybackMoney(@RequestBody PaybackMoneyDto paybackMoneyDto);

    @RequestMapping(value="/consumerBill/findAdvancePaybackTotalMoney/{uid}/{oid}",method ={RequestMethod.POST} )
    R<Map<String, Object>> findAdvancePaybackTotalMoney(@PathVariable("uid")String uid,@PathVariable("oid") String oid);

    @RequestMapping(value="/consumerBill/getOoperaInfoNameByid/{oid}",method ={RequestMethod.POST} )
    R<String> getOoperaInfoNameByid(@PathVariable("oid") String oid);
}
