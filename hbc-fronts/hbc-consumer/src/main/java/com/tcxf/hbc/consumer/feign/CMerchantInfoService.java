package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.consumer.dto.NearbyMerchantDto;
import com.tcxf.hbc.consumer.feign.fallback.CMerchantInfoServiceImpl;
import com.tcxf.hbc.consumer.vo.MerchantVo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@FeignClient(name = "hbc-mc-service", fallback = CMerchantInfoServiceImpl.class)
public interface CMerchantInfoService{
    /**
     * 店铺资料信息
     * @param id
     * @return
     */
    @RequestMapping(value="/merchat/QueryMerchatInfo/{id}",method ={RequestMethod.POST} )
   R<NearbyMerchantDto> QueryMerchantInfo(@PathVariable("id") String id);

    /**
     * 行业类型
     * @return
     */
    @RequestMapping(value="/merchat/QueryCategory",method ={RequestMethod.POST} )
    R QueryMerchantCatgory();


    /**
     * 店铺资料修改
     * @param m
     * @return
     */
    @RequestMapping(value = "/merchat/updateStore",method ={RequestMethod.POST} )
    R updateStoreinformation(@RequestBody MerchantVo m);
}
