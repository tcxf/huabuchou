package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.vo.MycreditDetailsDto;
import com.tcxf.hbc.consumer.feign.fallback.EmpoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FeignClient(name = "hbc-ctu-service", fallback =EmpoServiceFallbackImpl.class)
public interface EmpoService {
    /**
     * 快速授信
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/checkUser")
    R fastEmpo(@RequestBody CreditVo creditVo);

    /**
     * 获取资金端剩余授信额度
     * @param creditVo
     * @return
     */
    @PostMapping("/fMoneyInfo/getFMoneyInfoByFid")
    R<FMoneyInfo> getFMoneyInfoByFid(@RequestBody CreditVo creditVo);

    /**
     * 获取资金端剩余授信额度
     * @param creditVo
     * @return
     */
    @PostMapping("/fMoneyOtherInfo/getFMoneyOtherInfoByFid")
    R<BigDecimal> getFMoneyOtherInfoByFid(@RequestBody CreditVo creditVo);

    /**
     * 查询用户授信信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getTbAccountInfo")
    R<CUserRefCon> getTbAccountInfo(@RequestBody CreditVo creditVo);

    /**
     * 进入运营商授信界面
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/toOperatorView")
    R<HashMap<String,Object>> toOperatorView(@RequestBody CreditVo creditVo);

    /**
     * 跳转运营商授信界面前的操作
     * @return
     */
    @PostMapping("/baseCreditConfig/verifyCredit")
    R verifyCredit(@RequestBody CreditVo creditVo);

    /**
     * 运营商授信获取验证码的方法
     * @return
     */
    @PostMapping("/baseCreditConfig/sendVerificationCode")
    R sendVerificationCode(@RequestBody  CreditVo creditVo);

    /**
     * 运营商验证方法
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/threeMethod")
    R threeMethod(@RequestBody  CreditVo creditVo);

    /**
     * 查询用户基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getUserInfo")
    R<CUserInfo> getUserInfo(@RequestBody CreditVo creditVo);

    /**
     * 查询用户基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getBaseCreditInfo")
    R<BaseCreditInfo> getBaseCreditInfo(@RequestBody CreditVo creditVo);

    /**
     * 查询用户授信基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getMycreditDetails")
    R<MycreditDetailsDto> getMycreditDetails(@RequestBody CreditVo creditVo);

    /**
     * 查询用户授信基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/saveBusinessCredit")
    R saveBusinessCredit(@RequestBody CreditVo creditVo);

    /**
     * 查询商业授信基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getTbBusinessCreditByUid")
    R<TbBusinessCredit> getTbBusinessCreditByUid(@RequestBody CreditVo creditVo);

    /**
     * 查询啥商业授信基本信息
     * @param creditVo
     * @return
     */
    @PostMapping("/baseCreditConfig/getTbBusinessCreditDetailsByUid")
    R<HashMap<String, Object>> getTbBusinessCreditDetailsByUid(@RequestBody CreditVo creditVo);

}
