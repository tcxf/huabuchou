package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.BalanceVo;
import com.tcxf.hbc.common.vo.IncreaseCreditVo;
import com.tcxf.hbc.consumer.feign.fallback.IncreaseCreditServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "hbc-ctu-service", fallback =IncreaseCreditServiceFallbackImpl.class)
public interface IncreaseCreditService {


    /**
     * 进入提额界面-获取需输入项
     * @param
     * @return
     */
    @PostMapping("/increaseCreditConfig/toIncreaseCredit")
    R<List<TbUpMoney>> toIncreaseCredit(@RequestBody TbUpMoney tbUpMoney);

    /**
     * 进入紧急联系人界面
     * @param tbUpMoney
     * @return
     */
    @PostMapping("/increaseCreditConfig/toEmergencyContact")
    R<List<TbUpMoney>> toEmergencyContact(@RequestBody TbUpMoney tbUpMoney);

    /**
     * 保存紧急联系人
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/saveEmergencyContact")
    R saveEmergencyContact(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入社保认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toSocialInsurance")
    R<String> toSocialInsurance(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入公积金认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toAccumulationFund")
    R<String> toAccumulationFund(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入学历认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toEducationApprove")
    R<String> toEducationApprove(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入淘宝认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toTaobaoApprove")
    R<String> toTaobaoApprove(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入京东认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toJDcomApprove")
    R<String> toJDcomApprove(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 进入滴滴认证界面
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toDidiDacheApprove")
    R<String> toDidiDacheApprove(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * 最后一步
     * @param increaseCreditVo
     * @return
     */
    @PostMapping("/increaseCreditConfig/toSaveIncreaseCredit")
    R<BalanceVo> toSaveIncreaseCredit(@RequestBody IncreaseCreditVo increaseCreditVo);

    /**
     * @Description:  提额回调
     * @Param: [authJson, token]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/7/31
    */
    @RequestMapping("/increaseCreditConfig/callBack")
    R callBack(@RequestParam("authJson") String authJson, @RequestParam("token") String token, @RequestParam("OId") String OId);
}
