package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.IndexBannerImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "hbc-mc-service", fallback =IndexBannerImpl.class)
public interface IndexBannerService {

    @RequestMapping(value = "/index/banner/{id}",method ={RequestMethod.POST} )
     public R QueryBanner(@PathVariable("id") String id);

    @RequestMapping(value = "/index/bannerss",method ={RequestMethod.POST} )
    public R QueryBanners();

}
