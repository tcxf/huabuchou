package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.LoginDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.fallback.LoginServiceFallBackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * liaozeyong
 * 2018年7月17日10:23:04
 */
@FeignClient(name = "hbc-mc-service", fallback = LoginServiceFallBackImpl.class)
public interface LoginService {

    /**
     * 登陆
     * @param loginDto
     * @return
     */
    @PostMapping("/clogin/login")
    R login(@RequestBody LoginDto loginDto);

    /**
     * 判断openId存不存在
     * @param openId
     * @return
     */
    @GetMapping("/clogin/getOpenId/{openId}")
    R<CUserInfo> loginByOpenId(@PathVariable("openId")String openId);

    /**
     * 登陆的时候添加opneId
     * @param openId
     * @param mobile
     * @return
     */
    @PostMapping("/clogin/addOpenId/{openId}/{mobile}")
    R addOpenId(@PathVariable("openId")String openId,@PathVariable("mobile")String mobile);

    /**
     * 退出登陆清空用户里面的openId
     * @param accountId
     * @return
     */
    @PostMapping("/clogin/logout/{accountId}/{userId}")
    R logout(@PathVariable("accountId")String accountId , @PathVariable("userId")String userId);
    /**
     * 获取TbUserAccount内容
     * @param id
     * @return
     */
    @PostMapping("/clogin/getUserAccountInfo/{id}")
    R<UserInfoDto> userAccountInfo(@PathVariable("id")String id);

    /**
     * 根据userId获取UserInfoDto内容
     * @param id
     * @return
     */
    @RequestMapping(value = "/clogin/getUserAccountInfoByUserId/{id}" , method = RequestMethod.POST)
    R<UserInfoDto> getUserAccountInfoByUserId(@PathVariable("id")String id);

    /**
     * 根据二级域名获取运营商id
     * @param osn
     * @return
     */
    @PostMapping("clogin/getOperaId/{osn}")
    R<OOperaInfo> getOperaId(@PathVariable("osn") String osn);

    @PostMapping("clogin/getTbBindCardInfoByOid/{id}")
    R<TbBindCardInfo> getTbBindCardInfoByOid(@PathVariable("id") String id);
}
