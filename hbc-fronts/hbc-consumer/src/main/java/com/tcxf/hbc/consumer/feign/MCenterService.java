package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.MCenterServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author YWT_tai
 * @Date :Created in 14:01 2018/7/5 */
@FeignClient(name = "hbc-mc-service", fallback = MCenterServiceFallbackImpl.class)
public interface MCenterService {

    @RequestMapping(value="/merchantCenter/findMineCollects/{userId}",method ={RequestMethod.GET} )
    R findMineCollects(@PathVariable("userId") String userId);

    @RequestMapping(value="/merchantCenter/deleteCollect/{id}",method ={RequestMethod.POST} )
    R deleteCollect(@PathVariable("id") String id);

    @RequestMapping(value="/merchantCenter/findScoreInfo/{limit}/{page}/{direction}/{userId}",method ={RequestMethod.POST} )
    R findScoreInfo(@PathVariable("limit") int limit, @PathVariable("page") int page, @PathVariable("direction") String direction, @PathVariable("userId") String userId);

    @RequestMapping(value="/merchantCenter/findMyPaybackPageInfo/{limit}/{page}/{userId}",method ={RequestMethod.POST} )
    R findMyPaybackPageInfo(@PathVariable("limit") int limit, @PathVariable("page") int page, @PathVariable("userId") String userId);

    @RequestMapping(value="/merchantCenter/findMyRepayDetail/{id}/{userId}",method ={RequestMethod.POST} )
    R findMyRepayDetail(@PathVariable("id") String id, @PathVariable("userId") String userId);

    @RequestMapping(value="/merchantCenter/findUserInfo/{accountId}",method ={RequestMethod.POST} )
    R findUserInfo(@PathVariable("accountId") String accountId);

    @RequestMapping(value="/merchantCenter/addCollect",method ={RequestMethod.POST} )
    R addCollect(TbCollect tbCollect);
}
