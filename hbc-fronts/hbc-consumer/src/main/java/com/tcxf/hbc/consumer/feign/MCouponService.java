package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.MCouponServiceFallBackImpl;
import com.tcxf.hbc.consumer.feign.fallback.UserServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author huangjun
 * @date 2018/7/04
 */
@FeignClient(name = "hbc-mc-service", fallback = MCouponServiceFallBackImpl.class)
public interface MCouponService {

    /**
     *查询商户下的优惠券
     * @return
            */
    @RequestMapping(value="/coupon/couponList/{mid}",method ={RequestMethod.POST} )
    public R couponList(@PathVariable("mid") String mid);

    /**
     * 创建优惠券
     * @param mRedPacketSetting
     * @return
     */
    @RequestMapping(value = "/coupon/couponInsert",method = {RequestMethod.POST})
    public R couponInsert(@RequestBody MRedPacketSetting mRedPacketSetting);

    /**
     * 编辑优惠券页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/coupon/couponUpdate/{id}",method = {RequestMethod.POST})
    public R couponUpdate(@PathVariable("id") String id);

    /**
     *
     * 修改优惠券
     * @param mRedPacketSetting
     * @return
     */
    @RequestMapping(value = "/coupon/couponUpdateById",method = RequestMethod.POST)
    public  R couponUpdateById(@RequestBody MRedPacketSetting mRedPacketSetting);

    /**
     * 使优惠券失效
     * @param id
     * @return
     */
    @RequestMapping(value="/coupon/couponFailure/{id}",method ={RequestMethod.POST} )
    public  R couponFailure(@PathVariable("id") String id);


    @RequestMapping(value="/coupon/mtype/{mid}",method ={RequestMethod.POST} )
    public  String mtype(@PathVariable("mid") String mid);

}
