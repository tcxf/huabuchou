package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.MerchantCategoryServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "hbc-mc-service", fallback =MerchantCategoryServiceImpl.class)
public interface MerchantCategoryService {

    @RequestMapping(value = "/merchantCategory/QueryMerchantCategory",method ={RequestMethod.POST} )
    public R QueryMerchantCategory();

    @RequestMapping(value = "/merchantCategory/QueryMerchantCategorys",method ={RequestMethod.POST} )
    public R QueryMerchantCategorys();
}
