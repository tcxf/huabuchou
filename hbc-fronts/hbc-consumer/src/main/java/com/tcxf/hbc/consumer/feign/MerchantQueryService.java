package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.MerchantQueryServiceImpl;
import com.tcxf.hbc.consumer.feign.fallback.PacketServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service", fallback =MerchantQueryServiceImpl.class)
public interface MerchantQueryService {
    /**
     *热门商户
     * @return
     */
    @RequestMapping(value="/hotsearch/QueryAllHotsearch",method ={RequestMethod.POST} )
    public R QueryAllHotsearch();

    /**
     * 热门搜索
     * @return
     */
    @RequestMapping(value="/hotsearch/QueryAllHotsearchName/{name}",method ={RequestMethod.POST} )
    public R QueryAllHotsearchName(@PathVariable("name")String name);


}
