package com.tcxf.hbc.consumer.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.merchant.NearbyMerchant;
import com.tcxf.hbc.consumer.dto.NearbyMerchantDto;
import com.tcxf.hbc.consumer.feign.fallback.NerabyMerchantServiceFallBackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * @author huangjun
 * @date 2018/7/04
 */
@FeignClient(name = "hbc-mc-service", fallback = NerabyMerchantServiceFallBackImpl.class)
public interface NearbyMerchantService {

    /**
     *查询商户下的优惠券
     * @return
     */
    @RequestMapping(value="/nearby/findList/{lat}/{lng}/{page}/{micId}",method ={RequestMethod.POST} )
    public R couponList(@PathVariable("lat") String lat, @PathVariable("lng") String lng ,@PathVariable("page") String page,@PathVariable("micId") String micId);


    @RequestMapping(value = "/nearby/findById/{id}/{uid}",method = RequestMethod.POST)
    public R findById(@PathVariable("id") String id,@PathVariable("uid") String uid);

    /**
     * 商户下优惠券券，已经领取状态
     * @param userId
     * @param miId
     * @return
     */
    @RequestMapping(value = "/nearby/findByUserId/{userId}/{miId}",method = RequestMethod.POST)
    public R findByUserId(@PathVariable("userId") String userId,@PathVariable("miId") String miId);

    @RequestMapping(value = "/nearby/dt/{mid}",method = RequestMethod.POST)
    public Map<String,Object> dt(@PathVariable("mid")String mid);
}
