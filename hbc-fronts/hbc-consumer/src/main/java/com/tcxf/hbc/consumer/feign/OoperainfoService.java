package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.OoperainfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:44
 * @Description: 运营商信息
 */
@FeignClient(name = "hbc-mc-service", fallback = OoperainfoServiceFallbackImpl.class)
public interface OoperainfoService {

    @RequestMapping("/operaInfo/selectooperainfoByOsn")
    public R<OOperaInfo> selectooperainfoByOsn(@RequestParam("osn") String osn);
    
}
