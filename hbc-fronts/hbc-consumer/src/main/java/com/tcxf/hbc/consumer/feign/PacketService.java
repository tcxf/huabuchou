package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.PacketServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service", fallback =PacketServiceImpl.class)
public interface PacketService {

    @RequestMapping(value = "/packets/loadReadPckes",method ={RequestMethod.POST} )
    public R QueryPacket();

    @RequestMapping(value = "/packets/queryReadPckess/{micId}/{userId}/{page}",method ={RequestMethod.POST} )
    public R QueryPacketss(@PathVariable("micId")String micId,@PathVariable("userId")String userId,@PathVariable("page") String page);

    /**
     * 未登陆优惠券
     */
    @RequestMapping(value = "/packets/NOpacket",method = {RequestMethod.POST})
    R NOpackets();


}
