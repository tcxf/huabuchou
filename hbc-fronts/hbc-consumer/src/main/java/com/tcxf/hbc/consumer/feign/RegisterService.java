package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.RegisterDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.fallback.RegisterServiceFallBackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 注册的consumerService
 * 2018年7月4日13:51:32
 * liaozeyong
 */
@FeignClient(name = "hbc-mc-service", fallback = RegisterServiceFallBackImpl.class)
public interface RegisterService {

    /**
     * 注册
     * @param registerDto
     * @return
     */
    @PostMapping("/register/register")
    R<UserInfoDto> register(@RequestBody RegisterDto registerDto);

    /**
     * 注册添加TbUserAccount
     * 添加账户表
     * 生成钱包表
     * 生成账户和用户信息关联表
     * @return
     */
    @PostMapping("/register/AddTbUserAccount")
    R AddTbUserAccount(@RequestBody RegisterDto registerDto);
    /**
     * 发送验证码
     * @param mobile
     * @param type
     * @return
     */

    @PostMapping("/register/getYzm/{mobile}/{type}")
    public R getYzm(@PathVariable("mobile") String mobile,@PathVariable("type") String type);

    /**
     * 修改支付密码
     * @param id
     * @param code
     * @return
     */
    @PostMapping("/register/updatePaymentCode/{id}/{paymentCode}")
    R updatePaymentCode(@PathVariable("id")String id ,@PathVariable("paymentCode") String code);
}
