package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.SpiltServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import retrofit2.http.POST;

/**
 * jiangyong
 * 2018年7月23日09:10:40
 */
@FeignClient(name = "hbc-mc-service", fallback = SpiltServiceImpl.class)
public interface SpiltService {
    /**
     * 查询当月分期账单
     * @param oid
     * @param uid
     * @param currentData
     * @return
     */
    @RequestMapping(value="/divide/Querydividemoeny/{uid}/{oid}/{currentData}",method ={RequestMethod.POST} )
    R QuerySpilt(@PathVariable("uid") String uid,@PathVariable("oid")String oid,@PathVariable("currentData")String currentData);

    @RequestMapping(value = "/divide/updateSplitRepamentDetail/{totalTime}/{totalFee}",method = {RequestMethod.POST})
    void updateSplitRepamentDetails(@PathVariable("totalTime") String totalTime,@PathVariable("totalFee")String totalFee);

    /**
     * 369...分期
     */
    @RequestMapping(value = "/divide/ByStages/{uid}/{oid}/{currentData}/{fid}/{splitMonth}",method ={RequestMethod.POST})
    R ByStages(@PathVariable("uid") String uid,@PathVariable("oid")
            String oid,@PathVariable("currentData") String currentData,@PathVariable("fid") String fid,@PathVariable("splitMonth") String splitMonth);

    /**
     * 查询分期利率
     * @param oid
     * @param fid
     * @return
     */
    @RequestMapping(value = "/divide/findDictMyOid/{oid}/{fid}",method = {RequestMethod.POST})
    R QueryFQ(@PathVariable("oid") String oid, @PathVariable("fid") String fid);


    /**
     * 本月分期
     */
    @RequestMapping(value = "/divide/getDiffSplitAmounts/{uid}/{oid}/{fid}/{spiltMon}",method = {RequestMethod.POST})
    R getDiffSplitAmount(@PathVariable("uid") String uid, @PathVariable("oid") String oid,@PathVariable("fid")String fid ,@PathVariable("spiltMon") String spiltMon);

    /**
     * 校验密码
     */
    @RequestMapping(value = "/cUserInfo/checkPassWord/{accountId}/{passWord}",method = {RequestMethod.POST})
    R eckPassWords(@PathVariable("accountId") String accountId,@PathVariable("passWord") String passWord);
}
