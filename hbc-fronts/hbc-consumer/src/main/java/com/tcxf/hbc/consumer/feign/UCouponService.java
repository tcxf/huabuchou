package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.UCouponServiceFallBacklmpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author huangjun
 * @date 2018/07/04
 */
@FeignClient(name = "hbc-mc-service", fallback = UCouponServiceFallBacklmpl.class)
public interface UCouponService {
    /**
     *查询用户领取的优惠券
     * @param id
     * @return
     */
    @RequestMapping(value="/Ucoupon/coupon/{id}",method ={RequestMethod.POST} )
    public R findList(@PathVariable("id") String id);

    /**
     * 用户领优惠券
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/Ucoupon/getCoupon/{id}/{userId}",method = {RequestMethod.POST})
    public R getCoupon(@PathVariable("id") String id,@PathVariable("userId") String userId);
}
