package com.tcxf.hbc.consumer.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.UorderServiceFallBacklmpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;
import java.util.Map;

@FeignClient(name = "hbc-mc-service" ,fallback = UorderServiceFallBacklmpl.class)
public interface UorderService {

    /**
     * 查询用户订单
     * @param uid
     * @return
     */
    @RequestMapping(value = "/uorder/findByUid/{page}/{uid}/{type}",method = RequestMethod.POST)
    public Page findByUid(@PathVariable("page") String page , @PathVariable("uid") String uid , @PathVariable("type") String type);

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/uorder/findById/{id}",method = RequestMethod.POST)
    public R findById(@PathVariable("id") String id);

    @RequestMapping(value = "/uorder/findStati/{mid}",method = RequestMethod.POST)
    public  R findStati(@PathVariable("mid") String mid);

    @RequestMapping(value = "/uorder/findTradingDeta/{page}/{mid}/{createDate}/{modifyDate}/{sid}",method = RequestMethod.POST)
    public Page findTradingDeta(@PathVariable("page") String page, @PathVariable("mid") String mid, @PathVariable("createDate") String createDate, @PathVariable("modifyDate") String modifyDate,@PathVariable("sid") String sid);

    @RequestMapping(value = "/uorder/findSettlementDeta/{page}/{mid}",method = RequestMethod.POST)
    public Page findSettlementDeta(@PathVariable("page") String page,@PathVariable("mid")String mid);

}
