package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.CUserInfoDto;
import com.tcxf.hbc.consumer.feign.fallback.UserInfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "hbc-mc-service", fallback = UserInfoServiceFallbackImpl.class)
public interface UserInfoService {

    /**
     * 修改用户信息详情
     * @param cUserInfoDto
     * @return
     */
    @PostMapping("/userInfo/updateUserInfo/{accountId}")
    R updateUserInfo(@PathVariable("accountId")String accountId , @RequestBody CUserInfoDto cUserInfoDto);

    /**
     * 查询用户信息详情
     * @param userId
     * @return
     */
    @PostMapping("/userInfo/getUserById/{id}")
    R<CUserInfoDto> selectUserInfoDto(@PathVariable("id") String userId);

    /**
     * 修改用户支付密码
     * @param id 根据accountId
     * @param paymentCode 支付密码
     * @return
     */
    @PostMapping("/userInfo/updatePaymentCode/{id}/{paymentCode}")
    R updatePaymentCode(@PathVariable("id") String id , @PathVariable("paymentCode") String paymentCode);

    /**
     * 修改支付密码短信验证码验证
     * @return
     */
    @PostMapping("/userInfo/selectMobile/{id}/{mobile}/{yCode}")
    R selectMobile(@PathVariable("id") String id , @PathVariable("mobile") String mobile,@PathVariable("yCode") String yCode);

    /**
     * 修改登陆密码
     * @param mobile
     * @param password
     * @return
     */
    @PostMapping("/userInfo/updatePassword/{mobile}/{password}")
    R updatePassword(@PathVariable("mobile") String mobile , @PathVariable("password") String password);

    /**
     * 用户立即开店设置登陆密码
     * @param id
     * @param password
     * @return
     */
    @PostMapping("/userInfo/userSetPassword/{id}/{password}/{oid}")
    R<String> userSetPassword(@PathVariable("id")String id , @PathVariable("password")String password , @PathVariable("oid") String oid);

    /**
     * 忘记密码验证
     */
    @PostMapping("/userInfo/yzForgetPassword/{mobile}/{yzm}")
    R yzForgetPassword(@PathVariable("mobile") String mobile , @PathVariable("yzm")String yzm);
}
