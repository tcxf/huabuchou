package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.UserServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.Map;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@FeignClient(name = "hbc-mc-service", fallback = UserServiceFallbackImpl.class)
public interface UserService {

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return CUserInfo
     */
    @GetMapping("/cUserInfo/{id}")
    public CUserInfo get(@PathVariable("id") Integer id);

    @GetMapping("/cUserInfo/getUserById/{id}")
    public CUserInfo getUserInfo(@PathVariable("id") String id);

    /**
     * 判断微信公众号openid
     * @param cUserInfo
     * @return
     */
    @RequestMapping("/")
    public CUserInfo getOne(CUserInfo cUserInfo);

    /**
     * 分页查询信息
     *
     * @param params 分页对象
     * @return 分页对象
     */
    @RequestMapping("/cUserInfo/page")
    public Page page(@RequestParam("params") Map<String, Object> params) ;

    /**
     * 添加
     * @param  cUserInfo  实体
     * @return success/false
     */
    @RequestMapping("/cUserInfo/add")
    public R<Boolean> add(@RequestBody CUserInfo cUserInfo);


    /**
     * 编辑
     * @param  cUserInfo  实体
     * @return success/false
     */
    @PutMapping("/cUserInfo")
    public Boolean edit(@RequestBody CUserInfo cUserInfo) ;


}
