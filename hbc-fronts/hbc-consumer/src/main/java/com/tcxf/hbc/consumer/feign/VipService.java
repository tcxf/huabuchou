package com.tcxf.hbc.consumer.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.MCouponServiceFallBackImpl;
import com.tcxf.hbc.consumer.feign.fallback.VipServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service", fallback = VipServiceImpl.class)
public interface VipService {

    /**
     * 查询商户下的Vip
     * @return
     */
    @RequestMapping( value = "/Vip/QueryVip/{mid}/{realName}",method = RequestMethod.POST)
    Page findVip(@PathVariable("mid")String mid, @PathVariable("realName") String realName);

}
