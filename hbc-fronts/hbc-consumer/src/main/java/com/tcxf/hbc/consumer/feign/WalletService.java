package com.tcxf.hbc.consumer.feign;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.fallback.WalletSweviceFallBackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@FeignClient(name = "hbc-mc-service", fallback = WalletSweviceFallBackImpl.class)
public interface WalletService {
    /**
     * 通过ID查询用户钱包
     * @param oid
     * @return
     */
    @RequestMapping(value="wallet/wallets/{oid}",method ={RequestMethod.POST})
     R<TbWallet> get(@PathVariable("oid") String oid);

    /**
     * 查询钱包的收入支出记录
     * @param type
     * @param wid
     * @return
     */
    @RequestMapping(value="wallet/findUWalletIncome/{utype}/{wid}",method ={RequestMethod.POST})
    R<List<TbWalletIncome>> getfindWalletIncome(@PathVariable("utype") String type, @PathVariable("wid") String wid);


    /**
     * 查询钱包的提现记录
     * @param uid
     * @return
     */
    @RequestMapping(value="wallet/findWalletwithdrawalsList/{uid}",method ={RequestMethod.POST})
    R findWalletwithdrawalsList(@PathVariable("uid") String uid);

    /**
     * 查询所有绑卡
     * @param oid
     * @return
     */
    @RequestMapping(value="wallet/findUBindCardinfo/{oid}/{type}",method ={RequestMethod.POST})
    R findBindCardinfo(@PathVariable("oid") String oid,@PathVariable("type") String type);

    /**
     * 查询提现卡
     * @param id
     * @return
     */
    @RequestMapping(value="wallet/findBind/{id}",method ={RequestMethod.POST})
    R findBind(@PathVariable("id") String id);

    /**
     * 获取发送验证码
     * @param mobile
     * @return
     */
    @RequestMapping(value="wallet/getsys/{mobile}/{type}",method ={RequestMethod.POST})
    R getsys(@PathVariable("mobile") String mobile,@PathVariable("type") String type);

    /**
     * 钱包提现申请
     * @param
     */
    @RequestMapping(value="/wallet/walletdoReg", method = RequestMethod.POST)
    R walletdoReg(@RequestBody Map<String, Object> map);

    /**
     * 查询用户提现费率
     * @param map
     * @return
     */
    @RequestMapping(value="/wallet/findBYuser", method = RequestMethod.POST)
    R findBYuser(@RequestBody Map<String, Object> map);
    /**
     * 查询用户账户表信息
     */
    @RequestMapping(value="/wallet/findaccount/{id}", method = RequestMethod.POST)
    TbUserAccount findaccount(@PathVariable("id") String id);

    /**
     * 查询商户提现费率
     * @param map
     * @return
     */
    @RequestMapping(value="/wallet/findBYmerchant", method = RequestMethod.POST)
    R findBYmerchant(@RequestBody Map<String, Object> map);
}
