package com.tcxf.hbc.consumer.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.BankSercice;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankServiceFallbackImpl implements BankSercice {


    @Override
    public R findbank(String oid,String type) {
        return null;
    }

    @Override
    public R delbank(String id) {
        return null;
    }

    @Override
    public R savebank(TbBindCardInfo tbBindCardInfo) {
        return null;
    }


}
