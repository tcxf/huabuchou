package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.consumer.feign.BillAdvanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.TreeMap;


/**
 * @author YWT_tai
 * @Date :Created in 20:28 2018/7/12
 */
@Service
public class BillAdvanceServiceImpl implements BillAdvanceService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:53 2018/7/30
     * 立即还款
    */
    @Override
    public R PaybackMoney(PaybackMoneyDto paybackMoneyDto) {
        return null;
    }

    @Override
    public R AdvancePaybackMoney(PaybackMoneyDto paybackMoneyDto) {
        return null;
    }

    @Override
    public R<TbRepaymentPlan> findRepaymentPlanByRdId(String rdid) {
        return null;
    }

    @Override
    public R confirmAdvanceOrAlreadyBillPay(PayagreeconfirmDto payagreeconfirmDto) {
        return null;
    }

    @Override
    public R updateTradingInfo(TreeMap<String,String> map) {
        return null;
    }

    @Override
    public R updateNormalPay(TreeMap<String, String> paras) {
        return null;
    }


}
