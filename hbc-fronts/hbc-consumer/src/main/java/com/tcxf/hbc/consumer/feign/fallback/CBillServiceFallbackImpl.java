package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.PaybackMoney;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.consumer.feign.CBillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 20:28 2018/7/12
 */
@Service
public class CBillServiceFallbackImpl implements CBillService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R<String> getOoperaInfoNameByid(String oid) {
        return null;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 20:34 2018/7/12
     * 查询代还款账单和未出帐账单所有信息
     */
    @Override
    public R<HashMap<String, Object>> findAvailableCreditInfo(String uid, String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 20:34 2018/7/12
     * 查询待还账单信息
    */
    @Override
    public R findMineWaitPayInfo(String uid,String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:44 2018/7/13
     * 查询分期或逾期的详情接口
    */
    @Override
    public R findPlanDetailInfo(String planId,String waitpay) {
        return null;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 19:29 2018/7/16
     * 查询未出账单
     */
    @Override
    public R findUnOutBill(String uid,String oid) {
        return null;
    }
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:44 2018/7/13
     * 查询交易记录的详情
    */
    @Override
    public R findTradeDetailInfo(String id) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:06 2018/7/17
     * 查询分期明细
    */
    @Override
    public R findDivideDetail(String id) {
        return null;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:42 2018/7/19
     * 查询分期账单年份
     */
    @Override
    public R findPlanYears(String uid,String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:42 2018/7/19
     * 分期账单根据年份查询
    */
    @Override
    public R findPlanBillByYear(String dateYear, String uid,String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:42 2018/7/19
     * 查询历史账单年份
    */
    @Override
    public R findHistoryBillYears(String uid,String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:55 2018/7/19
     *  根据年份查询历史账单
    */
    @Override
    public R findHistoryBillByYear(String dateYear, String uid,String oid) {
        return null;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:35 2018/7/19
     * 查询历史账单详情(分期 提前 交易记录)
    */
    @Override
    public R findHistoryBillDetailInfo(String isSplit, String uid, String id,String oid) {
        return null;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:53 2018/7/30
     * 立即还款
    */
    @Override
    public R PaybackMoney(PaybackMoneyDto paybackMoneyDto) {
        return null;
    }

    @Override
    public R<Map<String, Object>> findAdvancePaybackTotalMoney(String uid, String oid) {
        return null;
    }


}
