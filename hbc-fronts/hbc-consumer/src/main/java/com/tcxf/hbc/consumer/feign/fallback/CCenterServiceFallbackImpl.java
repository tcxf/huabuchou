package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.CCenterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * @author YWT_tai
 * @Date :Created in 14:05 2018/7/5
 */
@Service
public class CCenterServiceFallbackImpl implements CCenterService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R findMineCollects(String userId) {
        logger.error("查询信息为空!");
        return null;
    }

    @Override
    public R deleteCollect(String id) {
        return null;
    }

    @Override
    public R findScoreInfo(int limit, int page, String direction,String userId) {
        return null;
    }

    @Override
    public R findMyPaybackPageInfo(int limit, int page,String userId) {
        return null;
    }

    @Override
    public R findMyRepayDetail(String id,String userId) {
        return null;
    }

    @Override
    public R findUserInfo(String userId) {
        return null;
    }

    @Override
    public R addCollect(TbCollect tbCollect) {
        return null;
    }

    @Override
    public R deleteByMid(String uid, String mid) {
        return null;
    }


}
