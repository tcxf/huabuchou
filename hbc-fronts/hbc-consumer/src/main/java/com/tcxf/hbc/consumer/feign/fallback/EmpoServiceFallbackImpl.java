package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.BaseCreditInfo;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.vo.MycreditDetailsDto;
import com.tcxf.hbc.consumer.feign.EmpoService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@Service
public class EmpoServiceFallbackImpl implements EmpoService {
    @Override
    public R fastEmpo(CreditVo creditVo) {
        return null;
    }

    @Override
    public R toOperatorView(CreditVo creditVo) {
        return null;
    }

    @Override
    public R<FMoneyInfo> getFMoneyInfoByFid(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R<BigDecimal> getFMoneyOtherInfoByFid(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R verifyCredit(CreditVo creditVo) {
        return null;
    }

    @Override
    public R sendVerificationCode(CreditVo creditVo) {
        return null;
    }

    @Override
    public R threeMethod(CreditVo creditVo) {
        return null;
    }

    @Override
    public R getTbAccountInfo(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R<BaseCreditInfo> getBaseCreditInfo(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R<CUserInfo> getUserInfo(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R<MycreditDetailsDto> getMycreditDetails(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R saveBusinessCredit(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R getTbBusinessCreditByUid(@RequestBody CreditVo creditVo) {
        return null;
    }

    @Override
    public R<HashMap<String, Object>> getTbBusinessCreditDetailsByUid(@RequestBody CreditVo creditVo) {
        return null;
    }
}
