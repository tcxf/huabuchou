package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.BalanceVo;
import com.tcxf.hbc.common.vo.IncreaseCreditVo;
import com.tcxf.hbc.consumer.feign.IncreaseCreditService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class IncreaseCreditServiceFallbackImpl implements IncreaseCreditService {


    @Override
    public R<List<TbUpMoney>> toIncreaseCredit(TbUpMoney tbUpMoney) {
        return null;
    }

    @Override
    public R<List<TbUpMoney>> toEmergencyContact(TbUpMoney tbUpMoney) {
        return null;
    }

    @Override
    public R saveEmergencyContact(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R<String> toSocialInsurance(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R<String> toAccumulationFund(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R<String> toEducationApprove(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R<String> toTaobaoApprove(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R toJDcomApprove(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R toDidiDacheApprove(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R<BalanceVo> toSaveIncreaseCredit(IncreaseCreditVo increaseCreditVo) {
        return null;
    }

    @Override
    public R callBack(String authJson, String token, String OId) {
        return null;
    }
}
