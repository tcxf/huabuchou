package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.IndexBannerService;
import org.springframework.stereotype.Service;

@Service
public class IndexBannerImpl implements IndexBannerService {

    @Override
    public R QueryBanner(String id) {
        return null;
    }

    @Override
    public R QueryBanners() {
        return null;
    }
}
