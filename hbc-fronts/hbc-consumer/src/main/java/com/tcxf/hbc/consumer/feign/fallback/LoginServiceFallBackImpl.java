package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.LoginDto;
import com.tcxf.hbc.consumer.dto.UserInfoDto;
import com.tcxf.hbc.consumer.feign.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LoginServiceFallBackImpl implements LoginService{
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public R login(LoginDto loginDto) {
        return null;
    }

    @Override
    public R<CUserInfo> loginByOpenId(String openId) {
        return null;
    }

    @Override
    public R addOpenId(String openId, String mobile) {
        return null;
    }

    @Override
    public R logout(String accountId, String userId) {
        return null;
    }


    @Override
    public R<UserInfoDto> userAccountInfo(String id) {
        return null;
    }

    @Override
    public R<UserInfoDto> getUserAccountInfoByUserId(String id) {
        return null;
    }

    @Override
    public R<OOperaInfo> getOperaId(String osn) {
        return null;
    }

    @Override
    public R<TbBindCardInfo> getTbBindCardInfoByOid(String id) {
        return null;
    }

}
