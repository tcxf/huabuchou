package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.MCouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MCouponServiceFallBackImpl implements MCouponService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R couponList(String mid) {
        return null;
    }

    @Override
    public R couponInsert(MRedPacketSetting mRedPacketSetting) {
        return null;
    }

    @Override
    public R couponUpdate(String id) {
        return null;
    }

    @Override
    public R couponUpdateById(MRedPacketSetting mRedPacketSetting) {
        return null;
    }

    @Override
    public R couponFailure(String id) {
        return null;
    }

    @Override
    public String mtype(String mid) {
        return null;
    }
}
