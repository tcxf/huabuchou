package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.MerchantQueryService;
import org.springframework.stereotype.Service;

@Service
public class MerchantQueryServiceImpl implements MerchantQueryService {
    @Override
    public R QueryAllHotsearch() {
        return null;
    }

    @Override
    public R QueryAllHotsearchName(String name) {
        return null;
    }
}
