package com.tcxf.hbc.consumer.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.controller.merchant.NearbyMerchant;
import com.tcxf.hbc.consumer.dto.NearbyMerchantDto;
import com.tcxf.hbc.consumer.feign.NearbyMerchantService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class NerabyMerchantServiceFallBackImpl implements NearbyMerchantService {


    @Override
    public R couponList(String lat, String lng,String page,String micId) {
        return null;
    }

    @Override
    public R findById(String id,String uid) {
        return null;
    }

    @Override
    public R findByUserId(String userId, String miId) {
        return null;
    }

    @Override
    public Map<String, Object> dt(String mid) {
        return null;
    }
}