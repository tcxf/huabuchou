package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.consumer.feign.OoperainfoService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:46
 * @Description:
 */
@Service
public class OoperainfoServiceFallbackImpl implements OoperainfoService {

    @Override
    public R<OOperaInfo> selectooperainfoByOsn(String osn) {
        throw new CheckedException("网络异常，请稍后再试");
    }

}
