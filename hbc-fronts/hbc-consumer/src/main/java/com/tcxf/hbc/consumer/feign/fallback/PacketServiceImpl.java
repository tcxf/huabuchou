package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.PacketService;
import org.springframework.stereotype.Service;

@Service
public class PacketServiceImpl implements PacketService {

    @Override
    public R QueryPacket() {
        return null;
    }

    @Override
    public R QueryPacketss(String micId, String userId ,String page) {
        return null;
    }

    @Override
    public R NOpackets() {
        return null;
    }
}
