package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.RegisterDto;
import com.tcxf.hbc.consumer.feign.RegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RegisterServiceFallBackImpl implements RegisterService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R register(RegisterDto registerDto) {
        return null;
    }

    @Override
    public R AddTbUserAccount(RegisterDto registerDto) {
        return null;
    }

    @Override
    public R getYzm(String mobile, String type) {
        return null;
    }

    @Override
    public R updatePaymentCode(String id, String code) {
        return null;
    }
}
