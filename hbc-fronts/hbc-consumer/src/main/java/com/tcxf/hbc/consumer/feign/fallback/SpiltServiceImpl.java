package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.SpiltService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class SpiltServiceImpl implements SpiltService {
    @Override
    public R QuerySpilt(String uid, String oid, String currentData) {
        return null;
    }

    @Override
    public void updateSplitRepamentDetails(String totalTime, String totalFee) {
    }

    @Override
    public R ByStages(String uid, String oid, String currentData, String fid, String splitMonth) {
        return null;
    }

    @Override
    public R QueryFQ(String oid, String fid) {
        return null;
    }

    @Override
    public R getDiffSplitAmount(String uid, String oid, String fid, String spiltMon) {
        return null;
    }

    @Override
    public R eckPassWords(String accountId, String passWord) {
        return null;
    }
}
