package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.UCouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UCouponServiceFallBacklmpl implements UCouponService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public R findList(String id) {
        return null;
    }

    @Override
    public R getCoupon(String id, String userId) {
        return null;
    }
}
