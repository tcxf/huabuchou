package com.tcxf.hbc.consumer.feign.fallback;


import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.UCouponService;
import com.tcxf.hbc.consumer.feign.UorderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UorderServiceFallBacklmpl implements UorderService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public Page findByUid(String page, String uid, String type) {
        return null;
    }

    @Override
    public R findById(String id) {
        return null;
    }

    @Override
    public R findStati(String mid) {
        return null;
    }

    @Override
    public Page findTradingDeta(String page, String mid, String createDate, String modifyDate,String sid) {
        return null;
    }

    @Override
    public Page findSettlementDeta(String page, String mid) {
        return null;
    }
}
