package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.dto.CUserInfoDto;
import com.tcxf.hbc.consumer.feign.UserInfoService;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceFallbackImpl implements UserInfoService {

    @Override
    public R updateUserInfo(String accountId, CUserInfoDto cUserInfoDto) {
        return null;
    }

    @Override
    public R<CUserInfoDto> selectUserInfoDto(String userId) {
        return null;
    }

    @Override
    public R updatePaymentCode(String id, String paymentCode) {
        return null;
    }

    @Override
    public R selectMobile(String id, String mobile, String yCode) {
        return null;
    }

    @Override
    public R updatePassword(String mobile, String password) {
        return null;
    }

    @Override
    public R userSetPassword(String id, String password, String oid) {
        return null;
    }

    @Override
    public R yzForgetPassword(String mobile, String yzm) {
        return null;
    }
}
