package com.tcxf.hbc.consumer.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author zhouyj
 * @date 2018/3/10
 * 用户服务的fallback
 */
@Service
public class UserServiceFallbackImpl implements UserService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public CUserInfo get(Integer id) {
        logger.error("调用{}异常:{}", "get", id);
        return null;
    }

    @Override
    public CUserInfo getUserInfo(String id) {
        logger.error("调用{}异常:{}", "getUserInfo", id);
        return null;
    }

    @Override
    public CUserInfo getOne(CUserInfo cUserInfo) {
        return null;
    }

    @Override
    public Page page(Map<String, Object> params) {
        return null;
    }

    @Override
    public R<Boolean> add(CUserInfo cUserInfo) {
        logger.error("调用{}异常:{}", "add", cUserInfo);
        return null;
    }


    @Override
    public Boolean edit(CUserInfo cUserInfo) {
        logger.error("调用{}异常:{}", "edit", cUserInfo);
        return null;
    }
}
