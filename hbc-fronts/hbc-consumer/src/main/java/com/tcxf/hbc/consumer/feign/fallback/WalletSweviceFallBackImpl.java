package com.tcxf.hbc.consumer.feign.fallback;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.consumer.feign.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WalletSweviceFallBackImpl implements WalletService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R get(String oid)
    {
        logger.error("调用{}异常:{}", "R", oid);
        return null;
    }

    @Override
    public R getfindWalletIncome(String type, String wid) {
        return null;
    }

    @Override
    public R findWalletwithdrawalsList(String uid) {
        return null;
    }

    @Override
    public R findBindCardinfo(String oid, String type) {
        return null;
    }


    @Override
    public R findBind(String id) {
        return null;
    }


    @Override
    public R getsys(String mobile, String type) {
        return null;
    }

    @Override
    public R walletdoReg(Map<String, Object> map) {
        return null;
    }

    @Override
    public R findBYuser(Map<String, Object> map) {
        return null;
    }

    @Override
    public TbUserAccount findaccount(String id) {
        return null;
    }


    @Override
    public R findBYmerchant(Map<String, Object> map) {
        return null;
    }

}
