package com.tcxf.hbc.consumer.util;

import com.tcxf.hbc.common.util.ValidateUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回路径配置
 * @author liuxu
 *
 */
public class ReturnUrlUtil {
	
	private static Map<String, String> mapUrl = new HashMap<String, String>();
	private static Map<String, String> mapKey = new HashMap<String, String>();
	/**
	 * 我的中心
	 */
	public static String URL_KEY_USER_CENTER="userCenter";
	public static String URL_KEY_GOTOPAY="goToPay";
	public static String URL_KEY_ORDER="order";
	public static String URL_KEY_BILL="bill";



	static {
		mapUrl.put(URL_KEY_GOTOPAY, "pay/goToPay");
		mapUrl.put(URL_KEY_USER_CENTER, "consumer/center/jumpMine");
		mapUrl.put(URL_KEY_ORDER, "user/order/order");
		mapUrl.put(URL_KEY_BILL, "consumer/bill/jumpMyCredit");
		mapKey.put("pay/goToPay",URL_KEY_GOTOPAY);
		mapKey.put("consumer/center/jumpMine",URL_KEY_USER_CENTER);
		mapKey.put("user/order/order",URL_KEY_ORDER);
		mapKey.put("consumer/bill/jumpMyCredit",URL_KEY_BILL);
	}
	
	public static String getLoginAfterUrl(String key){
		return ValidateUtil.isEmpty(mapUrl.get(key))? "" : mapUrl.get(key);
	}
	public static String getLoginAfterKey(String key){
		return ValidateUtil.isEmpty(mapKey.get(key))? "" : mapKey.get(key);
	}
	
}
