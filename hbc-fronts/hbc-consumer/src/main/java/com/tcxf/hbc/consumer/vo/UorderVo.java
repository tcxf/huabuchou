package com.tcxf.hbc.consumer.vo;

import com.tcxf.hbc.common.entity.TbTradingDetail;

public class UorderVo  extends TbTradingDetail {
    private String page;

    private String type;

    private String mid;

    private String sid;

    private String createDateStr;

    private String modifyDateStr;

    public String getCreateDateStr() {
        return createDateStr;
    }

    public void setCreateDateStr(String createDateStr) {
        this.createDateStr = createDateStr;
    }

    public String getModifyDateStr() {
        return modifyDateStr;
    }

    public void setModifyDateStr(String modifyDateStr) {
        this.modifyDateStr = modifyDateStr;
    }

    @Override
    public String getMid() {
        return mid;
    }

    @Override
    public void setMid(String mid) {
        this.mid = mid;
    }

    @Override
    public String getSid() {
        return sid;
    }

    @Override
    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPage() {

        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
