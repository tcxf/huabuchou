var ajax = {
	timeout : 300000,
	type : "POST",
	dataType : "json",
    loadindex:"",
	initProperty : function() {
		ajax.timeout = 300000;
		ajax.type = "POST";
		ajax.dataType = "json";
	},
	loadService : function(url,data,success,error) {
		ajax.loadAjax(url,data,"json",true, success,error);
	},
	
	loadServiceAsync : function(url,data,success,error) {
		ajax.loadAjax(url,data,"json",false, success,error);
	},
	loadAjax : function(url,data,dataType,async,success,error){

		$.ajax( {
            beforeSend: function(){
                loadindex = layer.load();
            },
            complete: function(){
                layer.close(loadindex);
            },
			url : url,
			timeout : ajax.timeout,
			type : ajax.type,
			dataType : dataType,
            contentType:"application/json",
			async : async,
			data : JSON.stringify(data),
			success :success,
			error : error
		});
	}
};

$(function(){
	$(document).ajaxStop(function(e) {
		//util.hideLoading();
		//util.hideMainLoading();
	});
	$(document).ajaxError(function(event,jqXHR, textStatus, errorThrown) {
		//util.hideLoading();
		//util.hideMainLoading();
		//util.showErrorMessage("系统异常，请重新登录")
	});
});
