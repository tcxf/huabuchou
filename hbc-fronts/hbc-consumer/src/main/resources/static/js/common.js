var res_uri = "http://res.zoomyblion.com/";
//var context="http://localhost:4022/cfp";
$(function(){
	
	jQuery.fn.initBank = function() {
		var binfo = {};
		$.ajax({
			url:context2+"/js/bank/bank_info.json",
		    type:'GET', //GET
		    timeout:30000,    //超时时间
		    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		    async:false,
		    success:function(data){
		    	binfo = data;
		    }
		});
		
		return this.each(function() {
			var name = $(this).attr("input-name");
			var val = $(this).attr("input-val")==null?"":$(this).attr("input-val");
			var $select = $('<select name="'+name+'"></select>');
			for(var i = 0 ; i < binfo.length ; i++){
				selected = binfo[i].sn == val?"selected":"";
				$select.append('<option value="'+binfo[i].sn+'" '+selected+'>'+binfo[i].name+'</option>');
			}
			$(this).html($select);
			$select.select();
		});
	}

	/*平台端运营商下拉选项*/
    jQuery.fn.operaSelect = function(callback) {
        var operaInfo = [];
        $.ajax({
            url:context+"/p_payback/allopera",
            type:'POST', //GET
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            async:false,
            success:function(data){
                operaInfo = data.data;
            }
        });
        return this.each(function() {
            //获取所有运营商信息
            $this = $(this);
            var ele_id = $this.attr("ele-id");
            var ele_name = $this.attr("ele-name");
            var data_val = $this.attr("data-val");
            $select = $('<select id="'+ele_id+'" name="'+ele_name+'"></select>');
            $select.append('<option value="">选择运营商</option>');
            for(var i = 0 ; i < operaInfo.length ; i++){
                selected = "";
                if(data_val != null && data_val == operaInfo[i].id) selected = "selected";
                $select.append('<option value="'+operaInfo[i].id+'" '+selected+'>'+operaInfo[i].name+'</option>');
            }
            $select.change(function(){
                if(callback != null)
                    callback($(this));
            });
            $this.append($select);
            $select.select();
            if(data_val != null && data_val != ""){
                $select.trigger("change");
            }
        });
    };


	/*资金端运营商下拉选项*/
    jQuery.fn.operaSelectByfid = function(callback) {
        var operaInfo = [];
        $.ajax({
            url:context+"/f_payback/allopera",
            type:'POST', //GET
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            async:false,
            success:function(data){
                operaInfo = data.data;
            }
        });
        return this.each(function() {
            //获取所有运营商信息
            $this = $(this);
            var ele_id = $this.attr("ele-id");
            var ele_name = $this.attr("ele-name");
            var data_val = $this.attr("data-val");
            $select = $('<select id="'+ele_id+'" name="'+ele_name+'"></select>');
            $select.append('<option value="">选择运营商</option>');
            for(var i = 0 ; i < operaInfo.length ; i++){
                selected = "";
                if(data_val != null && data_val == operaInfo[i].id) selected = "selected";
                $select.append('<option value="'+operaInfo[i].id+'" '+selected+'>'+operaInfo[i].name+'</option>');
            }
            $select.change(function(){
                if(callback != null)
                    callback($(this));
            });
            $this.append($select);
            $select.select();
            if(data_val != null && data_val != ""){
                $select.trigger("change");
            }
        });
    };

	//加载行业类型
    jQuery.fn.loadIndustryType = function(callback) {
        var mechantData = [];
        $.ajax({
            url:context+"/p_merchant/loadIndustryType",
            type:'POST', //GET
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            async:false,
            success:function(data){
                mechantData = data.data;
            }
        });
        return this.each(function() {
            //获取所有行业类型信息
            $this = $(this);
            var ele_id = $this.attr("ele-id");
            var ele_name = $this.attr("ele-name");
            var data_val = $this.attr("data-val");
            $select = $('<select id="'+ele_id+'" name="'+ele_name+'"></select>');
            $select.append('<option value="">选择行业类型</option>');
            for(var i = 0 ; i < mechantData.length ; i++){
                selected = "";
                if(data_val != null && data_val == mechantData[i].id) selected = "selected";
                $select.append('<option value="'+mechantData[i].id+'" '+selected+'>'+mechantData[i].name+'</option>');
            }
            $select.change(function(){
                if(callback != null)
                    callback($(this));
            });
            $this.append($select);
            $select.select();
            if(data_val != null && data_val != ""){
                $select.trigger("change");
            }
        });
    };



	
	jQuery.fn.select = function() {
		return this.each(function() {
			var b = randomString(32);
			var option = $(this).find("option");
			var $_html = $('<div class="btn-group" style="width:auto;margin-right:10px;" id="'+b+'"></div>');
			if($(this).attr("_i") != null){
				$_html = $("#"+$(this).attr("_i"));
				$_html.html("");
				b = $(this).attr("_i");
			}
			var selVal = "";
			var selText = "";
			$_html.append('<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only"></span></button>');
			var $_sel = $('<ul class="dropdown-menu" style="max-height:200px;overflow-y: auto;"></ul>');
			option.each(function(i,e){
				if($(e).prop("selected")){
					selVal = $(e).val();
					selText = $(e).html();
				}
				$_sel.append('<li class="_hsj_sel_option_'+b+'" data="'+$(e).val()+'"><a href="javascript:void(0);">'+$(e).html()+'</a></li>');
			});
			$_html.append($_sel);
			$_html.prepend('<button type="button" class="_hsj_sel_text_'+b+' btn btn-default">'+selText+'</button>');
			$(this).attr("_i",b);
			$(this).hide();
			$(this).parent().append($_html);
			var $this = $(this);
			$("._hsj_sel_option_"+b).click(function(){
				$this.val($(this).attr("data"));
				$this.trigger("change");
				$("._hsj_sel_text_"+b).html($(this).find("a").html());
			});
		});
	};

    jQuery.fn.area = function(){
        this.each(function(){
            var data_name=$(this).attr("data-name")==null?randomString(5):$(this).attr("data-name");
            var data_value=$(this).attr("data-value")==null?"":$(this).attr("data-value");
            var path = "";
            if(data_value != null &&　data_value != ''){
                $.ajax({
                    url:context+"/p_merchant/loadArea?id="+data_value,
                    type:'POST', //GET
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    async:false,
                    success:function(data){
                        if(data != null && data.data != null)
                            path = data.data.path;
                    }
                });
            }

            var r = randomString(32);
            var $p = $('<select id="p'+r+'" data-random="'+r+'" style="width:33.33%"></select>&nbsp;');
            var $c = $('<select id="c'+r+'" data-random="'+r+'" style="width:33.33%"></select>&nbsp;');
            var $d = $('<select id="d'+r+'" data-random="'+r+'" style="width:33.33%"></select>&nbsp;');
            var $nn = $('<input type="hidden" name="'+data_name+'" id="'+r+data_name+'" value="'+data_value+'"/>');
            $d.attr("data-name",data_name);
            $(this).append($p);
            $(this).append($c);
            $(this).append($d);
            $(this).append($nn);


            l1 = null;
            l2 = null;
            l3 = null;
            if(path.split(",").length > 0){
                l1 = path.split(",")[0];
                l2 = path.split(",")[1];
                l3 = path.split(",")[2];
            }

            bindData("p",null,r);

            $p.change(function(){
                rr = $(this).attr("data-random");
                bindData("c",$(this).val(),rr);
            });

            $c.change(function(){
                rr = $(this).attr("data-random");
                bindData("d",$(this).val(),rr);
            });

            $d.change(function(){
                l1 = null;
                l2 = null;
                l3 = null;
                rr = $(this).attr("data-random");
                $("#"+rr+$("#d"+rr).attr("data-name")).val($(this).val());
            });

            function bindData(type,areaId,rr){
                if(type == "p"){
                    $("#p"+rr).empty();
                    $("#p"+rr).append('<option value="">选择省份</option>');
                    $("#c"+rr).empty();
                    $("#c"+rr).append('<option value="">选择市</option>');
                    $("#d"+rr).empty();
                    $("#d"+rr).append('<option value="">选择区</option>');
                    $("#"+rr+$("#d"+rr).attr("data-name")).val("");
                    $.post(context+"/p_merchant/loadPcd",{parentId:0},function(d){
                        for(var i = 0 ; i < d.data.length ; i++){
                            if(l1 == d.data[i].id)
                                $("#p"+rr).append('<option selected value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                            else
                                $("#p"+rr).append('<option value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                        }
                        $("#p"+rr).select();
                        $("#c"+rr).select();
                        $("#d"+rr).select();
                        if(l1 != null){
                            $("#p"+rr).trigger("change");
                        }
                    },"json");
                }else if(type == "c"){
                    $("#c"+rr).empty();
                    $("#c"+rr).append('<option value="">选择市</option>');
                    $("#d"+rr).empty();
                    $("#d"+rr).append('<option value="">选择区</option>');
                    $("#"+rr+$("#d"+rr).attr("data-name")).val("");
                    $.post(context+"/p_merchant/loadPcd",{parentId:areaId},function(d){
                        for(var i = 0 ; i < d.data.length ; i++){
                            if(l2 == d.data[i].id)
                                $("#c"+rr).append('<option selected value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                            else
                                $("#c"+rr).append('<option value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                        }
                        $("#c"+rr).select();
                        $("#d"+rr).select();

                        if(l2 != null){
                            $("#c"+rr).trigger("change");
                        }
                    },"json");
                }else if(type == "d"){
                    $("#d"+rr).empty();
                    $("#d"+rr).append('<option value="">选择区</option>');
                    $("#"+rr+$("#d"+rr).attr("data-name")).val("");
                    $.post(context+"/p_merchant/loadPcd",{parentId:areaId},function(d){
                        for(var i = 0 ; i < d.data.length ; i++){
                            if(l3 == d.data[i].id)
                                $("#d"+rr).append('<option selected value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                            else
                                $("#d"+rr).append('<option value="'+d.data[i].id+'">'+d.data[i].name+'</option>');
                        }
                        $("#d"+rr).select();
                        if(l3 != null){
                            $("#d"+rr).trigger("change");
                        }
                    },"json");
                }
            }
        });
    }

    jQuery.fn.uploadImg2 = function(option){
        var uploader = {};
        this.each(function(){
            var r = randomString(32);
            $div = $('<span></span>');
            fileId = $(this).attr("file-id");
            uploader[fileId] = {
                fileId:fileId
            }
            val = $(this).attr("file-value");
            var size = $(this).attr("file-size");
            width = 30;
            height = 30;
            if(size != null){
                width = size.split("\*")[0];
                height = size.split("\*")[1];
            }
            $div.append('<input id="'+fileId+'" name="'+fileId+'" type="hidden" value="'+val+'"/>');
            $div2 = $('<span></span>');
            $uploader = $('<span id="uploader_'+fileId+'"></span>');
            $fileList = $('<span id="fileList_'+fileId+'" class="uploader-list"></span>');
            if(val == ""){
                $fileList.append('<span"><img id="img_'+fileId+'" style="width:'+width+'px;height:'+height+'px;display:none;" /></span>');
            }else{
                $fileList.append('<span style="margin-bottom:10px;"><img id="img_'+fileId+'" style="width:'+width+'px;height:'+height+'px;" src="'+val+'"/></span>');
            }
            $uploader.append($fileList);
            $uploader.append('<span id="filePicker'+fileId+'"></span>');
            $div2.append($uploader);
            $div.append($div2);
            $(this).append($div);

            uploader[fileId].uploader = WebUploader.create({
                // 选完文件后，是否自动上传。
                auto: true,
                // swf文件路径
                swf: context+'/js/plugins/webuploader/Uploader.swf',
                // 文件接收服务端。
                server: context+'/cmerchant/upload',
                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: '#filePicker'+fileId,
                // 只允许选择图片文件。
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }
            });
        });
        for(var i in uploader){
            uploader[i].uploader.on( 'uploadSuccess', function( file,response  ) {
                var fileId = $(this)[0].options.pick.replace("#filePicker","");
                $("#"+fileId).val(response.filePath);
                $("#img_"+fileId).attr("src",response.filePath);
                $("#img_"+fileId).show();
            });
        }
    }

	jQuery.fn.uploadImg = function(option){
		var uploader = {};
		this.each(function(){
			var r = randomString(32);
			$div = $('<span></span>');
			fileId = $(this).attr("file-id");
			uploader[fileId] = {
				fileId:fileId
			}
			val = $(this).attr("file-value");
			var size = $(this).attr("file-size");
			width = 100;
			height = 100;
			if(size != null){
				width = size.split("\*")[0];
				height = size.split("\*")[1];
			}
			$div.append('<input id="'+fileId+'" name="'+fileId+'" type="hidden" value="'+val+'"/>');
			$div2 = $('<span></span>');
			$uploader = $('<span id="uploader_'+fileId+'"></span>');
			$fileList = $('<span id="fileList_'+fileId+'" class="uploader-list"></span>');
			if(val == ""){
				$fileList.append('<span style="margin-bottom:10px;"><img id="img_'+fileId+'" style="width:'+width+'px;height:'+height+'px;display:none;" /></span>');
			}else{
				$fileList.append('<span style="margin-bottom:10px;"><img id="img_'+fileId+'" style="width:'+width+'px;height:'+height+'px;" src="'+val+'"/></span>');
			}
			$uploader.append($fileList);
			$uploader.append('<span id="filePicker'+fileId+'" style="margin-left: 20px;">选择图片</span>');
			$div2.append($uploader);
			$div.append($div2);
			$(this).append($div);
			
			uploader[fileId].uploader = WebUploader.create({
        	    // 选完文件后，是否自动上传。
        	    auto: true,
        	    // swf文件路径
        	    swf: context+'/js/plugins/webuploader/Uploader.swf',
        	    // 文件接收服务端。
        	    server: context+'/cmerchant/upload',
        	    // 选择文件的按钮。可选。
        	    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        	    pick: '#filePicker'+fileId,
        	    // 只允许选择图片文件。
        	    accept: {
        	        title: 'Images',
        	        extensions: 'gif,jpg,jpeg,bmp,png',
        	        mimeTypes: 'image/*'
        	    }
        	});
		});
		for(var i in uploader){
			uploader[i].uploader.on( 'uploadSuccess', function( file,response  ) {
				var fileId = $(this)[0].options.pick.replace("#filePicker","");
        	    $("#"+fileId).val(response.filePath);
        	    $("#img_"+fileId).attr("src",response.filePath);
        	    $("#img_"+fileId).show();
        	});
		}
	}
	
	var _val_input = $(this).find("[validate-rule]");
	_val_input.each(function(i,e){
		$(this).bind("blur",function(){
			var $this = $(this);
			rules = null;
			if($this.is(":hidden")){return;} 
			if($this.attr("disabled") == "disabled"){return;} 
			try{rules = $(this).attr("validate-rule").split(",");}catch(e){}
			msgs = null;
			try{msgs = $(this).attr("validate-msg").split(",");}catch(e){}
			var _v = $this.val();
			var _r = "";
			var _p = "";
			var t = $('<div class="spmessage alert alert-danger" id="error-msg" style="display: block;margin-top:10px;margin-bottom:10px;"><span class="msgjg"></span><span id="error-msgbox"></span></div>');
			for(var i = 0 ; i < rules.length ; i++){
				_r = rules[i];
				_p = "";
				if(_r.indexOf("[") != -1){
					_p = _r.substring(_r.indexOf("[")+1,_r.length-1);
					_r = rules[i].substring(0, _r.indexOf("["));
				}
				if(_r == "") break;
				if(!validate[_r].validator(_v,_p)){
					_msg = validate[_r].message.replace("{p}", _p);
					if(msgs != null)
					for(var b = 0 ; b < msgs.length ; b++){
						if(msgs[b].split(":")[0] == _r){
							_msg = msgs[b].split(":")[1];
						}
					}
					layer.tips(_msg, $this);
					break;
				}
				$this.parent().find("#error-msg").remove();
			}
		});
	})
	
	var error_ = false;
	jQuery.fn.validate = function(){
		this.each(function() {
			var _val_input = $(this).find("[validate-rule]");
			var errorNum = 0;
			_val_input.each(function(i,e){
				var $this = $(this);
				if($this.is(":hidden")){return;} 
				if($this.attr("disabled") == "disabled"){return;} 
				var _v = $(this).val();
				rules = null;
				try{rules = $(this).attr("validate-rule").split(",");}catch(e){}
				msgs = null;
				try{msgs = $(this).attr("validate-msg").split(",");}catch(e){}
				var _r = "";
				var _p = "";
				var t = $('<div class="spmessage alert alert-danger" id="error-msg" style="display: block;margin-top:10px;margin-bottom:10px;"><span class="msgjg"></span><span id="error-msgbox"></span></div>');
				for(var i = 0 ; i < rules.length ; i++){
					_r = rules[i];
					_p = "";
					if(_r.indexOf("[") != -1){
						_p = _r.substring(_r.indexOf("[")+1,_r.length-1);
						_r = rules[i].substring(0, _r.indexOf("["));
					}
					if(_r == "") break;
					if(!validate[_r].validator(_v,_p)){
						_msg = validate[_r].message.replace("{p}", _p);
						if(msgs != null)
						for(var b = 0 ; b < msgs.length ; b++){
							if(msgs[b].split(":")[0] == _r){
								_msg = msgs[b].split(":")[1];
							}
						}
						layer.tips(_msg, $this);
						errorNum ++;
						break;
					}
					$this.parent().find("#error-msg").remove();
				}
			});
			if(errorNum > 0){
				error_ = false;
			}else{
				error_ = true;
			}
		});
		return error_;
	}
	
	validate = {
		required: {
	        validator: function (value,param) {
	            return $.trim(value) != "";
	        },
	        message: '请输入内容'
		},
		engAndNum:{
			validator: function (value,param) {
	            return /^[0-9a-zA-Z]+$/.test(value);
	        },
	        message: '只能输入英文字母或者汉字'
		},
		CHS: {
	        validator: function (value,param) {
	            return /^[\u0391-\uFFE5]+$/.test(value);
	        },
	        message: '请输入汉字'
	    },
	    ZIP: {
	        validator: function (value,param) {
	            return /^[1-9]\d{5}$/.test(value);
	        },
	        message: '邮政编码不存在'
	    },
	    QQ: {
	        validator: function (value,param) {
	            return /^[1-9]\d{4,10}$/.test(value);
	        },
	        message: 'QQ号码不正确'
	    },
	    email: {
	    	validator: function (value,param) {
	    		if(value == "") return true;
	            return /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/.test(value);
	        },
	        message: 'Email格式不正确'
	    },
	    mobile: {
	        validator: function (value,param) {
	            return /^((\(\d{2,3}\))|(\d{3}\-))?1\d{10}$/.test(value);
	        },
	        message: '手机号码不正确'
	    },
	    loginName: {
	        validator: function (value,param) {
	            return /^[\u0391-\uFFE5\w]+$/.test(value);
	        },
	        message: '登录名称只允许汉字、英文字母、数字及下划线。'
	    },
	    safepass: {
	        validator: function (value,param) {
	            return safePassword(value);
	        },
	        message: '密码由字母和数字组成，至少6位'
	    },
	    equalTo: {
	        validator: function (value,param) {
	            return value == $(param).val();
	        },
	        message: '两次输入的字符不一至'
	    },
	    number: {
	        validator: function (value,param) {
	            return /^\d+$/.test(value);
	        },
	        message: '请输入数字'
	    },
	    money:{
	        validator: function (value,param) {
	         	return (/^(([1-9]\d{0,9})|\d)(\.\d{1,2})?$/).test(value);
	         },
	         message:'请输入正确的金额'

	    },
	    idcard: {
	        validator: function (value,param) {
	            return idCard(value);
	        },
	        message:'请输入正确的身份证号码'
	    },
	    remote: {
	    	validator: function(value,param){
	    		var simNo = $("#simNo").val();
	    	    $.post(value,null,
	    	    function(data){
		    	    if(data=="unexist"){
		    	    	flag = true;
		    	    	$.data(document.body,"flag",flag);
		    	    }else if(data=="exist"){
		    	    	flag = false;
		    	    	$.data(document.body,"flag",flag);
		    	    }
	    	    });
	    	    return  $.data(document.body,"flag");
	    	},
	    	message: '卡号已经存在'
	    },
	    minLength: {
	        validator: function (value, param) {
	            return value.length >= param;
	        },
	        message:'字符长度不能小于{p}'
	    },
	    maxLength: {
	        validator: function (value,param) {
	            return value.length <= param;
	        },
	        message:'字符长度不能大于{p}'
	    },
	    min: {
	        validator: function (value,param) {
	            var b = parseFloat(value) >= parseFloat(param);
	            return b;
	        },
	        message:'不能小于{p}'
	    },
	    max: {
	        validator: function (value,param) {
	            var b = parseFloat(value) <= parseFloat(param);
	            return b;
	        },
	        message:'不能大于{p}'
	    },
	    pwdss: {
	        validator: function (value,param) {
	        	if(value == ""){
	        		return true;
	        	}else{
	        		return /^.{6,20}$/.test(value);	
	        	}
	        },
	        message:'密码长度不能少于6大于20'
	    },
	    pwdssa: {
	        validator: function (value,param) {
	        	if(value == ""){
	        		return true;
	        	}else{
	        		return /^[0-9]{6}$/.test(value);	
	        	}
	        },
	        message:'请输入长度为6的数字密码'
	    },
	    floatss: {
	        validator: function (value,param) {
	            return /^[0-9]+([.]{1}[0-9]+){0,1}$/.test(value);
	        },
	        message:'只能输入数字和小数'
	    },
	    num:{
	        validator: function (value,param) {
	        	if(value == "") return true;
	         	return (/^(([1-9]\d{0,9})|\d)(\.\d{1,2})?$/).test(value);
	         },
	         message:'最多只能输入2位小数的数字'

	    },
	    ffstring:{
	    	validator:function(value,param){
	    		var fa = (/[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im).test(value);
	    		if(fa == false){
	    			return true;
	    		}else{
	    			return false;
	    		}
	    	},
	    	message:'不能输入非法字符'
	    },
	    integer:{
	    	validator:function(value,param){
	    		if(isNaN(value)) return false;
	    		return true;
	    	},
	    	message:'请输入正整数'
	    }
	}
	
	
	var safePassword = function (value) {
	    return !(/^(([A-Z]*|[a-z]*|\d*|[-_\~!@#\$%\^&\*\.\(\)\[\]\{\}<>\?\\\/\'\"]*)|.{0,5})$|\s/.test(value));
	}

	var idCard = function (value) {
	    if (value.length == 18 && 18 != value.length) return false;
	    var number = value.toLowerCase();
	    var d, sum = 0, v = '10x98765432', w = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2], a = '11,12,13,14,15,21,22,23,31,32,33,34,35,36,37,41,42,43,44,45,46,50,51,52,53,54,61,62,63,64,65,71,81,82,91';
	    var re = number.match(/^(\d{2})\d{4}(((\d{2})(\d{2})(\d{2})(\d{3}))|((\d{4})(\d{2})(\d{2})(\d{3}[x\d])))$/);
	    if (re == null || a.indexOf(re[1]) < 0) return false;
	    if (re[2].length == 9) {
	        number = number.substr(0, 6) + '19' + number.substr(6);
	        d = ['19' + re[4], re[5], re[6]].join('-');
	    } else d = [re[9], re[10], re[11]].join('-');
	    if (!isDateTime.call(d, 'yyyy-MM-dd')) return false;
	    for (var i = 0; i < 17; i++) sum += number.charAt(i) * w[i];
	    return (re[2].length == 9 || number.charAt(17) == v.charAt(sum % 11));
	}

	var isDateTime = function (format, reObj) {
	    format = format || 'yyyy-MM-dd';
	    var input = this, o = {}, d = new Date();
	    var f1 = format.split(/[^a-z]+/gi), f2 = input.split(/\D+/g), f3 = format.split(/[a-z]+/gi), f4 = input.split(/\d+/g);
	    var len = f1.length, len1 = f3.length;
	    if (len != f2.length || len1 != f4.length) return false;
	    for (var i = 0; i < len1; i++) if (f3[i] != f4[i]) return false;
	    for (var i = 0; i < len; i++) o[f1[i]] = f2[i];
	    o.yyyy = s(o.yyyy, o.yy, d.getFullYear(), 9999, 4);
	    o.MM = s(o.MM, o.M, d.getMonth() + 1, 12);
	    o.dd = s(o.dd, o.d, d.getDate(), 31);
	    o.hh = s(o.hh, o.h, d.getHours(), 24);
	    o.mm = s(o.mm, o.m, d.getMinutes());
	    o.ss = s(o.ss, o.s, d.getSeconds());
	    o.ms = s(o.ms, o.ms, d.getMilliseconds(), 999, 3);
	    if (o.yyyy + o.MM + o.dd + o.hh + o.mm + o.ss + o.ms < 0) return false;
	    if (o.yyyy < 100) o.yyyy += (o.yyyy > 30 ? 1900 : 2000);
	    d = new Date(o.yyyy, o.MM - 1, o.dd, o.hh, o.mm, o.ss, o.ms);
	    var reVal = d.getFullYear() == o.yyyy && d.getMonth() + 1 == o.MM && d.getDate() == o.dd && d.getHours() == o.hh && d.getMinutes() == o.mm && d.getSeconds() == o.ss && d.getMilliseconds() == o.ms;
	    return reVal && reObj ? d : reVal;
	    function s(s1, s2, s3, s4, s5) {
	        s4 = s4 || 60, s5 = s5 || 2;
	        var reVal = s3;
	        if (s1 != undefined && s1 != '' || !isNaN(s1)) reVal = s1 * 1;
	        if (s2 != undefined && s2 != '' && !isNaN(s2)) reVal = s2 * 1;
	        return (reVal == s1 && s1.length != s5 || reVal > s4) ? -10000 : reVal;
	    }
	};
});

function randomString(len) {
	len = len || 32;
	var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
	var maxPos = $chars.length;
	var pwd = '';
	for (i = 0; i < len; i++) {
		pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
		return pwd;
	}
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
