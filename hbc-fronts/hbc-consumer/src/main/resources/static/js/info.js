var base="";
api = {
	tools : {
		ajax:function(option,callback,err){ //调用普惠服务器方法
			cover = false;
			async = true; //是否异步调用
			if(option.async != null) async = option.async;
			if(option.cover != null) cover = option.cover;
			l = null;
			$.ajax({
				url:option.url,
			    type:'POST', //GET
			    data:option.data,
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    async:async,	//异步请求
			    beforeSend:function(xhr){
			    	if(cover) l = layer.open({type: 2,content: '加载中'});
			    },
			    error:function(req,status,e){
                    var auth = req.getResponseHeader("REDIRECT");
                    var auth_url = req.getResponseHeader("CONTENTPATH");
                    if(auth=="REDIRECT" && auth_url){
                        window.location.href = auth_url;
                    }
                    if(cover) layer.close(l);
			    	if(err != null) err();

			    },
			    success:function(data){
			    	if(cover) layer.close(l);
			    	callback(data);
			    },

			});
		},
		bankInfo:{ //获取银行列表
			list: function(){//银行列表
				var binfo = {};
				$.ajax({
					url:base,
				    type:'GET', //GET
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    async:false,
                    success:function(b){
				    	binfo = b;
				    }
				});
				return binfo;
			},
			get:function(sn){//根据银行编码获取logo
				var bankInfo = api.tools.bankInfo.list();
				for(var i = 0 ; i < bankInfo.length ; i++){
					if(bankInfo[i].sn == sn){
                        return bankInfo[i];
					}
				}
				return null;
			}
		},
		toast:function(msg){ //显示提示层toast
			layer.open({
			    content: msg
			    ,skin: 'msg'
			    ,time: 2 //2秒后自动关闭
			  });
		},
		confirm:function(option){
			i = layer.open({
			    content: option.content
			    ,btn: ['确定', '取消']
			    ,yes: function(index){
			      option.callback();
			      layer.close(i);
			    }
			  });
		},
		moneyFormat:function(m){ //金额格式化
			return (parseFloat(m)).toFixed(2);
		},
		scroll:function(callback){ //页面滚动监听
			$(document).scroll(function() {
				var top = $(document).scrollTop();
				var wheight = $(window).height();
				var dheight = $(document).height();
				callback({
					top:top,
					wheight:wheight,
					dheight:dheight
				});
			});
		},
		isIos:function(){
			var u = navigator.userAgent;
		    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
		    return isiOS;
		},
	    goback:function(){
	    	if (/(iPhone|iPad|iPod)/i.test(navigator.userAgent)) {
	    		var replaceUrl = window.location.href+"?i="+new Date().getTime();
	    	    history.replaceState(null,"",replaceUrl);       
	            window.location.href = window.document.referrer;
	    	} else { 
	    		window.history.go("-1"); 	
	    	}
	    },
		initWxJsSdk:function(option,callback){
			api.tools.ajax({
				url:"/system/wxJsSignTicket",
				data:{
					url:location.href.split("#")[0]
				}
			},function(d){
				var data = d.data;
				try{
					wx.config({
					    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
					    appId: data.appid, // 必填，公众号的唯一标识
					    timestamp: data.timestamp, // 必填，生成签名的时间戳
					    nonceStr: data.nonceStr, // 必填，生成签名的随机串
					    signature: data.signature,// 必填，签名，见附录1
					    jsApiList: option // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
					});
					wx.ready(function(){
						callback();
					});
				}catch(e){}
			});
		},
		openPwdWin:function(merchantName,amt,okFun,noFun){
            PwdBox.show(function(res){
                okFun(res.password,this);
            });
		},
		qrCode:function(ele,text){ //生成二维码  ele：生成位置  text：二维码内容
//			$(ele).empty().qrcode({
//				background:"#ffffff",
//				ecLevel:"H",
//				fill:"#333333",
//				minVersion:6,
//				text:text
//			});
			var img = $('<img src="http://pan.baidu.com/share/qrcode?w=300&h=300&url='+text+'" style="max-width:100%;"/>');
			$(ele).html(img);
		},
		phone_type : function(phoneno){  
			var regex = /^(134|135|136|137|138|139|150|151|152|157|158|159|182|183|184|187|188|147|178)[0-9]{8}$/;  
	        if(regex.test(phoneno)){  //移动
	            return "10";  
	        }  
	        regex = /^(130|131|132|155|156|185|186|145|176|175)[0-9]{8}$/;  
	        if(regex.test(phoneno)){   //联通
	            return "20";  
	        }  
	        regex = /^(133|153|180|189|181|177|173|149)[0-9]{8}$/;  
	        if(regex.test(phoneno)){   //电信
	            return "30";  
	        }  
	        return "-1";  
	    },
	    toPay:function(mnum,oiid,amt,paymentType,openid,callback){
	    	api.tools.ajax({
				url:api.base+'orderInfo/createPayment.htm',
				data:{
					oiid:oiid,
					paymentType:paymentType,
					openid:openid
				}
			},function(d){
				var sn = d.data.paymentSn;
				if(d.resultCode != -1){
					if(d.data.paymentInfo == null && d.data.paymentUrl == null){ //授信余额支付
						api.tools.openPwdWin('订单支付',amt,function(d,box){
							api.tools.ajax({
								url:api.base+'orderInfo/pay.htm',
								data:{
									mnum:mnum,
									sn:sn,
									tranPwd:d
								}
							},function(d,box){
								callback(d,box);
							});
						});
					}else{
						if(d.data.redirectType == 0){
							var pinfo = d.data.paymentInfo;
							WeixinJSBridge.invoke('getBrandWCPayRequest',pinfo.pay_info,function(res){
									if(res.err_msg == "get_brand_wcpay_request:ok" ) {
										var d = {
											resultMsg:'支付成功',
											resultCode:1
										}
										callback(d);
									}else{
										var d = {
											resultMsg:'支付失败',
											resultCode:-1
										}
										callback(d);
									}
								});
						}else{
							api.tools.toast('正在跳转支付界面...');
							setTimeout(function(){
								if(d.data.redirectType == null || d.data.redirectType == 1){
									var uri = d.data.paymentUrl;
									if(d.data.paymentInfo != null)
										for(var k in d.data.paymentInfo){
											if(uri.indexOf('?') == -1)
												uri += "?"+k+"="+d.data.paymentInfo[k];
											else
												uri += "&"+k+"="+d.data.paymentInfo[k];
										}
									window.location = uri;
								}else{
									var $form = $('<form method="post"></form>');
									$form.attr("action",d.data.paymentUrl);
									for(var k in d.data.paymentInfo){
										console.log(k+"="+d.data.paymentInfo[k]);
										$form.append('<input type="hidden" value="'+d.data.paymentInfo[k]+'" name="'+k+'"/>');
									}
									$form.submit();
								}
							},1500);
						}
					}
				}else{
					api.tools.toast(d.resultMsg);
				}
			});
	    },
	    idCardCheck:function(idCard){
	    	var idCardValidate = function (value) {
			    if (value.length == 18 && 18 != value.length) return false;
			    var number = value.toLowerCase();
			    var d, sum = 0, v = '10x98765432', w = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2], a = '11,12,13,14,15,21,22,23,31,32,33,34,35,36,37,41,42,43,44,45,46,50,51,52,53,54,61,62,63,64,65,71,81,82,91';
			    var re = number.match(/^(\d{2})\d{4}(((\d{2})(\d{2})(\d{2})(\d{3}))|((\d{4})(\d{2})(\d{2})(\d{3}[x\d])))$/);
			    if (re == null || a.indexOf(re[1]) < 0) return false;
			    if (re[2].length == 9) {
			        number = number.substr(0, 6) + '19' + number.substr(6);
			        d = ['19' + re[4], re[5], re[6]].join('-');
			    } else d = [re[9], re[10], re[11]].join('-');
			    if (!isDateTime.call(d, 'yyyy-MM-dd')) return false;
			    for (var i = 0; i < 17; i++) sum += number.charAt(i) * w[i];
			    return (re[2].length == 9 || number.charAt(17) == v.charAt(sum % 11));
			}
	    	var isDateTime = function (format, reObj) {
	    	    format = format || 'yyyy-MM-dd';
	    	    var input = this, o = {}, d = new Date();
	    	    var f1 = format.split(/[^a-z]+/gi), f2 = input.split(/\D+/g), f3 = format.split(/[a-z]+/gi), f4 = input.split(/\d+/g);
	    	    var len = f1.length, len1 = f3.length;
	    	    if (len != f2.length || len1 != f4.length) return false;
	    	    for (var i = 0; i < len1; i++) if (f3[i] != f4[i]) return false;
	    	    for (var i = 0; i < len; i++) o[f1[i]] = f2[i];
	    	    o.yyyy = s(o.yyyy, o.yy, d.getFullYear(), 9999, 4);
	    	    o.MM = s(o.MM, o.M, d.getMonth() + 1, 12);
	    	    o.dd = s(o.dd, o.d, d.getDate(), 31);
	    	    o.hh = s(o.hh, o.h, d.getHours(), 24);
	    	    o.mm = s(o.mm, o.m, d.getMinutes());
	    	    o.ss = s(o.ss, o.s, d.getSeconds());
	    	    o.ms = s(o.ms, o.ms, d.getMilliseconds(), 999, 3);
	    	    if (o.yyyy + o.MM + o.dd + o.hh + o.mm + o.ss + o.ms < 0) return false;
	    	    if (o.yyyy < 100) o.yyyy += (o.yyyy > 30 ? 1900 : 2000);
	    	    d = new Date(o.yyyy, o.MM - 1, o.dd, o.hh, o.mm, o.ss, o.ms);
	    	    var reVal = d.getFullYear() == o.yyyy && d.getMonth() + 1 == o.MM && d.getDate() == o.dd && d.getHours() == o.hh && d.getMinutes() == o.mm && d.getSeconds() == o.ss && d.getMilliseconds() == o.ms;
	    	    return reVal && reObj ? d : reVal;
	    	    function s(s1, s2, s3, s4, s5) {
	    	        s4 = s4 || 60, s5 = s5 || 2;
	    	        var reVal = s3;
	    	        if (s1 != undefined && s1 != '' || !isNaN(s1)) reVal = s1 * 1;
	    	        if (s2 != undefined && s2 != '' && !isNaN(s2)) reVal = s2 * 1;
	    	        return (reVal == s1 && s1.length != s5 || reVal > s4) ? -10000 : reVal;
	    	    }
	    	};

	    	return idCardValidate(idCard);
	    },
	    location:function(success,err){
	    	if($.cookie("POSITION") != null){
	    		var data = JSON.parse($.cookie("POSITION"));
	    		success(data);
	    	}else{
	    		try{
			    	mapObj = new AMap.Map('iCenter');
					mapObj.plugin(['AMap.Geolocation','AMap.Geocoder'], function () {
					    geolocation = new AMap.Geolocation({
					        enableHighAccuracy: true,//是否使用高精度定位，默认:true
					        timeout: 10000,          //超过10秒后停止定位，默认：无穷大
					        maximumAge: 0,           //定位结果缓存0毫秒，默认：0
					        convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
					        showButton: true,        //显示定位按钮，默认：true
					        buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
					        buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
					        showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
					        showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
					        panToLocation: true,     //定位成功后将定位到的位置作为地图中心点，默认：true
					        zoomToAccuracy:true      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
					    });
					    mapObj.addControl(geolocation);
					    AMap.event.addListener(geolocation, 'complete', function(d){
					    	 lat = d.position.lat;
					    	 lng = d.position.lng;
					    	 var geocoder = new AMap.Geocoder({
					             radius: 1000,
					             extensions: "all"
					         });        
					    	 lnglatXY = [lng, lat]; //已知点坐标
					         geocoder.getAddress(lnglatXY, function(status, result) {
					        	 if (status === 'complete' && result.info === 'OK') {
					            	 var address = result.regeocode.addressComponent;
					                 var data = {};
					                 data.address = address;
					                 data.lat = lat;
					                 data.lng = lng;
					                 $.cookie("POSITION", JSON.stringify(data),{
					                	 path : '/',//cookie的作用域  
					                	 expires : 1
					                 }); 
					                 success(data);
					             }
					         });   
					   
					    });//返回定位信息
					    AMap.event.addListener(geolocation, 'error', function(d){
			                err();
					    });//返回定位出错信息
					    geolocation.getCurrentPosition();
					});
				}catch(e){
					alert(e);
				}
	    	}
	    },
	    platformAuth:function(){
	    	var currentUri = window.location.href;
	    	window.location = '/weixin_auth/platformAuth.htm?url='+encodeURIComponent(currentUri);
	    }
	}
};


$(document).ready(function() {
	try{PwdBox.init(null,'/js/PwdBox/img/pwd_keyboard.png','请输入支付密码','安全支付环境，请放心使用！');}catch(e){}
	//后退
	$("#hostory").bind("click", function() {
		if(window.history.length>1){
			window.history.go(-1);
		} else{
			window.location = "/index.htm";
		}
	});
	$(".redirect").click(function() {
        var data = $(this).attr("data");
        var url = $(this).attr("url");
        if (url != null) {
            window.location = url;
        }
    });
	var isPageHide = false;   
    window.addEventListener('pageshow', function () {   
        if (isPageHide) {   
          window.location.reload();   
        }   
      });   
      window.addEventListener('pagehide', function () {   
        isPageHide = true;   
      });  
});