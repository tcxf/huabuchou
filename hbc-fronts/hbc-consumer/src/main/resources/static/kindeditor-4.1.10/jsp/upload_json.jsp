<%@page import="com.hsj.grant.SystemSettingTools"%>
<%@page import="com.hsj.grant.UploadFileTools"%>
<%@page import="com.alibaba.fastjson.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page
	import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page
	import="org.springframework.web.multipart.MultipartHttpServletRequest"%>
<%@ page import="org.springframework.web.multipart.MultipartResolver"%>
<%@ page import="org.springframework.web.multipart.commons.CommonsMultipartResolver"%>
<%
	//定义允许上传的文件扩展名
	HashMap<String, String> extMap = new HashMap<String, String>();
	extMap.put("image", "gif,jpg,jpeg,png,bmp");
	extMap.put("flash",
			"swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
	extMap.put("media",
			"swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
	extMap.put("file",
			"doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

	//最大文件大小
	long maxSize = 1000000;

	session.setAttribute("ajax_upload", 1);
	response.setContentType("text/html; charset=UTF-8");
	if (!ServletFileUpload.isMultipartContent(request)) {
		out.println(getError("请选择文件。"));
		return;
	}

	MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
	MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
	//存储
	
	
 	String childrenFolder = "redeem";
	
	if(request.getParameter("f") != null) childrenFolder = request.getParameter("f");
	
	String allowExt = "jpg,png,jpeg,bmp".toUpperCase();

    String uri = UploadFileTools.uploadFile(multipartRequest.getFile("imgFile"), childrenFolder, allowExt, "2048");
    
	JSONObject obj = new JSONObject();
	obj.put("error", 0);
	obj.put("url", SystemSettingTools.getSystemSetting().getResSiteUri()+uri);
	out.println(obj.toJSONString());
%>
<%!private String getError(String message) {
		JSONObject obj = new JSONObject();
		obj.put("error", 1);
		obj.put("message", message);
		return obj.toJSONString();
	}%>