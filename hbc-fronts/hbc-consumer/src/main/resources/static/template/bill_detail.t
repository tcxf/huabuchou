<div class="webkit-box">
	<div class="wkt-flex">
		{name}
	</div>
	<div class="wkt-flex text-right">
		¥{money}
	</div>
</div>
<div class="main-date webkit-box" style="margin-top: 2px;">
	<div class="wkt-flex">
	{time}
	</div>
	<div class="wkt-flex text-right">
		{cal}
	</div>
</div>
<div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>