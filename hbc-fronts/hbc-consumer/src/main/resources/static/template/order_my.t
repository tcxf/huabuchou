<div class="webkit-box">
	<#list item="{orderItemList}">
    <div class="wkt-flex text-left">
    	<img src="{img}" style="width: 60px;height:60px;margin-top:0px" alt=""/>
	
      	<h4 style="font-size:1rem;margin-top: -4rem;margin-left: 5rem;white-space:nowrap;
             overflow:hidden;
             text-overflow:ellipsis;width:180px"> {name}</h4>	
        <p style="margin-left: 5rem">下单时间：{createDate}</p>
        <p style="margin-left: 5rem;line-height:10px">总价：￥{totalAmount} </p>
    </div>
    <div class="wkt-flex text-right" style="font-size: 0.8rem;color: #ff7a00">
    	{orderStatusStr}
	</div>
	</#list>
 </div>
 <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>