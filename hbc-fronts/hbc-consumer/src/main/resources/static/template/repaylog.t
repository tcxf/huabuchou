<div class="c-content" style="margin-bottom: 5px;margin-top:0px;padding: 10px 10px 2px 10px ">
	<div class="webkit-box">
		<div class="wkt-flex">
			{typeStr}
		</div>
	</div>
	<div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
	<div class="webkit-box">
		<div class="wkt-flex text-left">
			{totalMoney}
			<p>{createDate}</p>
		</div>
		<div class="wkt-flex text-right">
			<p>{paymentTypeStr}</p>
		</div>
	</div>
</div>