<div class="balance_content">
    <div class="content_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-9">
                    {serialNo}
                </div>
                <div class="col-xs-3">
                   {type}
                </div>
            </div>
        </div>
    </div>
    <div class="content_body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-9">
                    <h4>{simpleName}</h4>
                    <h6>结算周期:{loopStart}至{loopEnd}</h6>
                    <h6>生成时间:{createDate}</h6>
                </div>
                <div class="col-xs-3">
                    ￥{oamount}
                </div>
            </div>
        </div>
    </div>
</div>
