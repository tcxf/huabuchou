var template_catch =  {}; //缓存模板数据

var template = {
	load : function(options){
		var html; 
		if(template_catch[options.name] != null){ //从缓存中读取模板，如果已经读取过，直接返回模板
			html = processTemplate(template_catch[options.name],options.data);
		}else{
			$.ajax({
				url:options.host+"template/"+options.name+".t",
				success:function(d){
					template_catch[options.name]=d; //将模板存储到缓存中
					html = processTemplate(d,options.data);
				},
				async:false
			});
		}
		return html;
	}
}

function processTemplate(template,data){
	var list = template.match(/<#list([\w\W]*)<\/#list>/g);
	var l_c = [];
	if(list!= null && list.length > 0){
		for(var i = 0 ; i < list.length ; i++){
			listTag = list[i].match(/<#list item="{[a-zA-Z]*}">/g)[0];
			t = list[i].replace(listTag,'').replace('</#list>','');
			name = listTag.replace('<#list item="{','').replace('}">','');
			template = template.replace(list[i],'≮'+name+'≯')
			l_c[name] = {
				template:t
			}
		}
	}
	
	for(var k in data){
		if(template.indexOf('≮'+k+'≯') != -1){
			var t = l_c[k];
			var html = "";
			for(var i = 0 ; i < data[k].length ; i++){
				html += processTemplate(t.template,data[k][i]);
			}
			template = template.replace('≮'+k+'≯',html);
		}else{
			template = replaceAll(template,"{"+k+"}",data[k]);
		}
	}
	return template;
}

function replaceAll(str, sptr, sptr1){
    while (str.indexOf(sptr) >= 0){
       str = str.replace(sptr, sptr1);
    }
    return str;
}
