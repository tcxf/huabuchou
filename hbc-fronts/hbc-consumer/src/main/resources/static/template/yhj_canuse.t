<div style="margin-top:0.5rem;">
    <div class="wkt-flex" id="yhj">
        <div class="row">
            <div class="col-xs-5" style="margin-top:5px;padding-left: 25px;line-height: 25px">
                <h3 style="color: #ffffff;padding-left: 1.0rem;padding-top:0.6rem"> <span style="font-size: small">¥</span> {money}</h3>
                <div style="color: #ffffff;font-size: 12px;padding-left: 0.5rem">满{fullMoney}元使用</div>
            </div>
            <div class="col-xs-6" style="padding-top: 15px;margin-left:-27px;line-height:35px">
                <h5>{merchantName}</h5>
                <p style="font-size: 12px;width:120%">{validityDate}</p>
            </div>
            <div class="col-xs-2" style="padding-top: 25px;margin-left: -10%">
            	<a class="btn btn-default sms-btn get-red" data="{id}" style=" font-size: 0.8em; padding: 5px 5px 2px 4px; "href="#" role="button">可使用</a>
            </div>
        </div>
    </div>
</div>