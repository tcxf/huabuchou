<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="/css/Font-Awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/css/app.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<link rel="stylesheet" href="/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="/js/layer_mobile/need/layer.css" />

		<title></title>
		<style>
			body,
			html {
				background: #f5f5f5;
			}
			
			.layui-m-layer {
				z-index: 0 !important;
			}
		</style>
	</head>

	<body>
		<div class="c-content text-center success-img" style="margin-top: 0px; background: none; ">
			<img src="<%=path %>/img/images/error.png"  width="86"/>
			<div class="jy-error">500</div>
			<center>
				<div style="margin-top:40px;color:black;font-size:14px;width:80%;">
					<%=exception.getCause().getMessage() %>
				</div>
			</center>
		</div>
		
		
		<div class="webkit-box">
			<div class="wkt-flex">
				<div onclick="javascript:window.location='/p/index.htm'" class="yzm-btn tj" style="background: none; border:1px solid #999999; color: #999999; font-size: 17px; height: 40px; line-height: 40px; width: 40%; margin: 0 auto; margin-top: 40%">
					返回主页
				</div>
			</div>
		</div>
	</body>

	<script>
	</script>

</html>