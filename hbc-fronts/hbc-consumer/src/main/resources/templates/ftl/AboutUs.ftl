<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${base}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${base}/css/app.css" />
    <link rel="stylesheet" href="${base}/css/y.css"/>
    <link rel="stylesheet" href="${base}/js/mui/css/mui.min.css" />
    <title></title>
    <style>
        body,
        html {
            background: #eee;
        }
        .hk-money div {
            margin-top: 0;
            margin-left: 0;
            font-weight: 400;
        }
        .p-jbxx img{
            width: 23%;
        }
        .p-jbxx div{
            text-align: center;
            color: #333;
        }
        .row {
            margin-right: 0;

        }
        .row img{
            width: 20px;
            height: 20px;
            margin-top: 10%;
        }
    </style>
</head>
<body>
<div class="c-content" style="margin-top: 0;">
     <div class="webkit-box">
        <div class="wkt-flex">
            <div class="row">
                <a href="tel:4000000555" style="color: #333">
                    <div class="col-xs-3">
                        <img src="${base}/img/imgs/icon_shouxin_dianhuakefu.png" alt=""/>
                    </div>
                    <div class="col-xs-8" style="margin-top: 4px;margin-left:-3%">
                        我的客服
                    </div>
                </a>
            </div>
        </div>
        <div onclick="javascript:window.location='tel:4000000555'" class="wkt-flex text-right" style="color: #999;margin-top: 1%">
            400-0000555
            <img src="img/img/icon_enter.png" style="width: 6px;"alt=""/>
        </div>
    </div>
    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
     <div class="webkit-box">
        <div class="wkt-flex">
            <div class="row">
                <div class="col-xs-3">
                    <img src="${base}/img/imgs/Company profile.png" alt=""/>
                </div>
                <div class="col-xs-8" style="margin-top: 4px;margin-left:-8%">
                    公司介绍
                </div>
            </div>
        </div>
        <div class="wkt-flex text-right">
            <img src="img/img/icon_enter.png"style="width: 6px;" alt=""/>
        </div>
    </div>
    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
    <div class="webkit-box">
        <div class="wkt-flex">
            <div class="row">
                <div class="col-xs-3">
                    <img src="${base}/img/imgs/icon_my_jiameng.png" alt=""/>
                </div>
                <div class="col-xs-8" style="margin-top: 4px;margin-left:-8%">
                    招商加盟
                </div>
            </div>
        </div>
        <div class="wkt-flex text-right">
            <img src="${base}/img/img/icon_enter.png"style="width: 6px;" alt=""/>
        </div>
    </div>
</div>
   <div class="c-content" style="margin-top: 5px;">
    <div class="webkit-box redirect" url="<%=path%>/help.jsp">
        <div class="wkt-flex">
            <div class="row">
                <div class="col-xs-3">
                    <img src="${base}/img/imgs/heep.png" alt=""/>
                </div>
                <div class="col-xs-8" style="margin-top: 4px;margin-left:-6%">
                    帮助
                </div>
            </div>
        </div>
        <div class="wkt-flex text-right">
            <img src="${base}/img/img/icon_enter.png" style="width: 6px;"alt=""/>
        </div>
    </div>
   </div>
   
	<#include "common/footer.ftl">

	<script type="text/javascript" src="${base}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${base}/js/jquery.js"></script>
	<script type="text/javascript" src="${base}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${base}/js/info.js"></script>
	<script type="text/javascript" src="${base}/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="${base}/template/template.js"></script>
	<script type="text/javascript" src="${base}/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="${base}/js/picker/js/mui.poppicker.js"></script>
	<script>
		$(document).ready(function() {
			$(".base-info").click(function(){
				window.location = '<%=path%>/userInfoWx/baseInfo.htm'
			});
			
			api.tools.ajax({
				url:"<%=path%>/userInfoWx/loadUserInfo.htm"
			},function(d){
				if(d.data.headImg == null || d.data.headImg == "")
					$("#headImg").attr('src','/img/img/head_portrait.png');
				else
					$("#headImg").attr('src','${res!}'+d.data.headImg);
				var realName = d.data.realName;
				if(realName==null || realName=='') $('#name').html(d.data.mobile);
				else $('#name').html(realName);
			});
		});
		$("#dindan").click(function() {
			window.location = '${base}/orderInfo/orderList.htm';
		});
		$("#zhuye").click(function() {
			window.location = '${base}/p/index.htm';
		});
		
		$("#suoxin").click(function() {
			window.location = '${base}/b/bill.htm';
		});
		$("#dindan").click(function() {
			window.location = '${base}/orderSuper/orderCount.htm';
		});
		$("#my").click(function() {
			window.location = '${base}/Mysx.htm';
		});
	</script>

</body>
</html>