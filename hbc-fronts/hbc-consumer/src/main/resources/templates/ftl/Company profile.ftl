<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>关于我们</title>
    <style>
        body,
        html {
            background: #fff;
        }
        .help-top{
            width:100%;
            margin-top:20%;
            text-align:center
        }
        .help-top>img{
            width: 80px;
        }
        .company{
            margin-top:30px;
            padding: 20px 30px;
            color:#999;
        }
        .kefu_phone{
            width:100%;
            margin-top:10px;
            text-align:center;

        }
        .kefu_phone>img{
            width:20px
        }
        .kefu_phone a{
            color:#999;
            text-decoration: none;

        }
    </style>
</head>
<body>
<div class="help-top">
    <img src="${basePath!}/img/imgs/logo.png" alt=""/>
</div>
<div class="company">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp花不愁是面向全国的大型开放性消费服务平台,开创互联网生活消费服务生态圈,引入全国知名品牌商家,实力金融服务机构,建立全国消费服务中心,
    线上全国跨地域结合线下同城消费场景,为老百姓和商家提供多种消费场景,完善金融服务方案,资金解决服务和咨询服务。
</div>
<div class="kefu_phone">
    <img src="${basePath!}/img/imgs/kefu_phone.png">
    <span>
		<a href="tel:4000000555">客服:400-000-0555</a>
	</span>
</div>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<#include "./agreement/mune_btn.ftl">
</body>
<html>
