<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Title</title>
    <style>
        body{
            background: #fff;
        }
        *{
            padding: 0;
            margin: 0;
            font-size: 0.8rem;
        }
        .footer{
            width: 100%;
            height: 50px;
            background: #fff;
            box-shadow: 2px 2px 2px 5px #ededed;
            position: fixed;
            bottom: 0;
        }
        .footer li{
            width: 20%;
           float: left;
            text-align: center;
            padding-top: 5px;
            list-style: none;
        }
        .footer li>a{
            text-decoration: none;
            color: #999;
        }
        .footer li img{
            width: 20px;
            height: 20px;
        }
        .footer .active a{
            color: #f49110;
        }
        .footer li .a3{
            width: 60px;
            height: 60px;
            margin-top: -30px;
        }
        .footer li .shouxin{
            position: absolute;
            top:-10px;
            left:46%
        }
        .footer li .shouxin>img{
            width: 25px;
            height: 15px;
        }
    </style>
</head>
<body>
<div class="footer">
    <ul>
        <li>
            <a href="http://localhost:63342/%E9%A1%B9%E7%9B%AE/%E6%96%B0%E9%A6%96%E9%A1%B5.html">
                <img src="imgs/img/icon_bottom_shouye_grey.png" class="a1" alt="">
                <p>首页</p>
            </a>
        </li>
        <li>
            <a href="http://localhost:63342/%E9%A1%B9%E7%9B%AE/%E9%99%84%E8%BF%91%E5%95%86%E5%AE%B61.html?_ijt=881li2gdt1tstfm443kfu7bf2m">
                <img src="imgs/img/icon_bottom_shop_grey.png" class="a2" alt="">
                <p>附近商家</p>
            </a>
        </li>
        <li>
            <a href="http://localhost:63342/%E9%A1%B9%E7%9B%AE/%E6%8E%88%E4%BF%A11.html">
                <img src="imgs/img/home_credit.png" class="a3" alt="">
                <div class="shouxin">
                    <img src="imgs/logo.png" alt="">
                    <p>授信</p>
                </div>
            </a>
        </li>
        <li>
            <a href="http://localhost:63342/%E9%A1%B9%E7%9B%AE/%E8%AE%A2%E5%8D%95.html">
                <img src="imgs/img/icon_bottom_dd_grey.png" class="a4" alt="">
                <p>订单</p>
            </a>
        </li>
        <li>
            <a href="http://localhost:63342/%E9%A1%B9%E7%9B%AE/%E6%88%91%E7%9A%84.html">
                <img src="imgs/img/icon_bottom_my_grey.png" class="a5" alt="">
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
<script src="js/jquery.js"></script>
<script>
    $(".footer li").click(function(){
        $(this).children().css('color','#f49110');//点击的那个变黄色
        $(this).siblings().children().css('color','#adaeaa');//同胞的变灰色
        var n=$(this).index();//点击的index值
//分别是改变图片的地方，跟颜色一样，同胞变其他灰色图片，自个变黄色
        if(n=='0'){
            $('.a1').attr('src','imgs/mg/icon_bottom_shouye_yellow.png');
            $('.a2').attr('src','imgs/img/icon_bottom_shop_grey.png');
            $('.a3').attr('src','imgs/home_credit.png');
            $('.a4').attr('src','imgs/img/icon_bottom_dd_grey.png');
            $('.a5').attr('src','imgs/img/icon_bottom_my_grey.png');
        }
        if(n=='1'){
            $('.a1').attr('src','imgs/img/icon_bottom_shouye_grey.png');
            $('.a2').attr('src','imgs/img/icon_bottom_shop_yellow.png');
            $('.a3').attr('src','imgs/home_credit.png');
            $('.a4').attr('src','imgs/img/icon_bottom_dd_grey.png');
            $('.a5').attr('src','imgs/img/icon_bottom_my_grey.png');
        }
        if(n=='2'){
            $('.a1').attr('src','imgs/img/icon_bottom_shouye_grey.png');
            $('.a2').attr('src','imgs/img/icon_bottom_shop_grey.png');
            $('.a3').attr('src','imgs/home_credit.png');
            $('.a4').attr('src','imgs/img/icon_bottom_dd_grey.png');
            $('.a5').attr('src','imgs/img/icon_bottom_my_grey.png');
        }

        if(n=='3'){
            $('.a1').attr('src','imgs/img/icon_bottom_shouye_grey.png');
            $('.a2').attr('src','imgs/img/icon_bottom_shop_grey.png');
            $('.a3').attr('src','imgs/home_credit.png');
            $('.a4').attr('src','imgs/img/icon_bottom_dd_yellow.png');
            $('.a5').attr('src','imgs/img/icon_bottom_my_grey.png');
        }
        if(n=='4'){
            $('.a1').attr('src','imgs/img/icon_bottom_shouye_grey.png');
            $('.a2').attr('src','imgs/img/icon_bottom_shop_grey.png');
            $('.a3').attr('src','imgs/home_credit.png');
            $('.a4').attr('src','imgs/img/icon_bottom_dd_grey.png');
            $('.a5').attr('src','imgs/img/icon_bottom_my_yellow.png');
        }
    });
</script>
</body>
</html>