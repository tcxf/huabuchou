<div class="service">
    <h4>商 户 授 信 合 同</h4>
    <p>&nbsp&nbsp&nbsp&nbsp
        贷款人（甲方）：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        住所地：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        法定代表人：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
        邮编：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        电话：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p style="margin-top: 40px">&nbsp&nbsp&nbsp&nbsp
        借款人（乙方）：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        住所地：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        法定代表人：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        电话：<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
        邮编:<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        甲乙双方本着平等诚信的原则，经协商一致，于<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>年<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>月<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>日在<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>订立本合同，以兹共同遵照执行。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第一条  授信额度及类别</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        在本合同规定的条件下，甲方同意在授信额度有效期间内向乙方提供累计余额不得超过人民币<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>元整的授信额度。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第二条  授信期间</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、本合同项下授信额度的有效使用期间（即授信期间）为<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>
        个月，自<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>年<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>月
        日起算，但借款额度的有效使用期限应受借款合同的约定。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、甲方有权对本合同项下授信额度使用情况进行不定期审查，如出现本合同第四条阐述的情形，甲方有权调整授信期间。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第三条  授信额度的使用</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、在本合同约定的授信期间和授信额度内，乙方可一次或分次向甲方书面申请使用该授信额度。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、乙方申请使用的授信额度余额在任何时候都不得超过本合同第一条约定的授信额度，在授信期间内，乙方对已归还的授信额度可循环使用，授信期间内未使用的授信额度在授信期间届满后自动取消。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、乙方必须在本合同第二条约定的授信期间内申请使用授信额度，每笔授信项目的开始使用日期不得超过授信期间的截止日，该截止日包括调整后的授信期间的截止日。每笔授信项目的使用期限依所签的合同或协议约定。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、甲方与乙方就每一项具体授信所签订的合同或协议（以下简称借款合同）与本合同不一致的，以该具体借款合同为准，但乙方对本合同项下或本合同相关的具体借款合同项下的任何一切债务承担的连带责任不得因此无效或得以解除。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        5、对乙方在本协议及具体借款合同项下的到期未付应付款项，并且甲方有权对资金使用情况随时进行检查。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第四条  授信额度的调整</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        一、如发生以下条件之一即构成乙方违约：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、乙方没有按期支付到期的与甲方有关的未清偿债务，包括本合同规定或每笔具体借款合同的本金、利息和其他费用等。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、乙方没有按照约定用途使用授信资金。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、乙方没有充分履行本合同或与甲方签订的其他关合同或协议项下的任何义务或没有完全遵守其中的任一规定，且在接到甲方书面通知后没有采取甲方满意的补救措施；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、发生据甲方合理判断可能会实质性危及、损害甲方权益的与本合同有关的其他事件，如本合同的担保人的担保能力变得明显不足，与乙方经营相关的市场情况或国家政策发生重大变化并将对乙方经营状况产生实质性的不利影响等。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        二、违约事件发生后，甲方有权根据情节轻重调整、减少或终止本授信额度及授信期间，并有权采取以下部分或全部措施：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、宣布直接或间接源于本合同的一切债务提前到期，并要求乙方立即清偿；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、要求乙方承担甲方因实现债权而发生的各项合理费用（包括诉讼或仲裁、送达、执行、律师代理、差旅费用等）；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、要求乙方提供或追加担保，担保的方式包括保证、抵押和质押等；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、采取维护甲方在合同项下权益的符合有关法律规定的其他措施。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第五条  担保</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        为保证本合同项下形成的债权能得到清偿，<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>公司将与甲方签订保证/抵押/质押合同，（简称“担保合同”）为乙方履行本合同及与本合同相关的每笔具体业务合同或协议项下债务提供担保。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第六条  合同生效、变更和解除</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、本合同自双方法定代表人（授权代理人）签字或加盖公章之日起成立，与担保合同同时生效。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、本合同生效后，甲、乙双方任何一方不得擅自变更或提前解除本合同。需要变更或解除时，应经双方协商一致，并达成书面协议。担保合同终止或解除的，本合同同时终止或解除。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第七条  争议和解决</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        甲、乙双方在履行合同过程中发生争议时，双方可向甲方所在地人民法院提起诉讼。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>第八条  附则</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、本合同一式<u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>份，当事人各一份，具有同等法律效力。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、甲方与乙方依据本合同就每一项具体授信所签订的借款合同均为本合同的组成部分，并构成一个合同整体。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        甲方（公章）： <u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>乙方（公章）:
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        法定代表人或 <u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>法定代表人或
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        委托代理人（签章）： <u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>委托代理人（签章）：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>年
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp</u>月
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp</u>日
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</u>年
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp</u>月
        <u>&nbsp&nbsp&nbsp&nbsp&nbsp</u>日
    </p>
</div>