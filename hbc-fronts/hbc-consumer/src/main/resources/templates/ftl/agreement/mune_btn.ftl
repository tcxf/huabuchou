<link rel="stylesheet" href="${basePath!}/css/mune_btn.css">

<div class="menu_button">
    <ul>
        <li>
            <a href="${basePath!}/user/index/Goindex">首页</a>
        </li>
        <li>
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">附近商家</a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMyCredit">授信</a>
        </li>
        <li>
            <a href="${basePath!}/user/order/order">订单</a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMine">我的</a>
        </li>
    </ul>
</div>
<div class="menu_modal">
    <p>返回</p>
</div>
 <script>
     $(".menu_modal").click(function () {
         $(".menu_button").slideToggle();
     });
 </script>