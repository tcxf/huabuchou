<div class="service">
    <h4>用户注册协议</h4>
    <div> 编号：</div>
    <p>&nbsp&nbsp&nbsp&nbsp
        “花不愁”消费服务平台（以下简称“平台”）是由湖南同城消费网络科技有限公司开发的同城消费软件系统。
        根据本公司（即平台运营商<u id="use_name"></u>公司）与湖南同城消费网络科技有限公司之间的相关协议约定，本公司有权使用该平台开展业务，包括但不限于为用户线上、线下消费提供商家资信和商品信息等资讯，同时可在用户申请的情况下为合格用户提供消费金融中介服务，消费金融详细内容以平台及与平台合作的金融机构届时公布的规则为准。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>用户可通过本公司所运营的平台注册相应账号，享受本公司基于平台提供的相关服务。在贵方注册成为本公司用户前，请务必认真、仔细阅读并充分理解本协议的所有条款、内容。若贵方不接受本协议条款，请立即停止注册。一经贵方点击平台“用户注册”按钮，即表示贵方已完全阅读、理解并同意接受本协议全部内容，自愿成为平台用户，遵守平台规则，并根据本协议的约定享有权利、承担义务。</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>一、相关声明与承诺</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、本服务协议双方为本公司与用户。本服务协议包括协议正文及所有本公司通过平台已经发布的或将来可能发布的补充协议、各类规则、声明、通知及公告等文件，该类文件为协议不可分割的一部分，与协议正文具有同等法律效力。如用户对本公司今后发布的文件不认可、不接受，需立即停止接受本公司平台服务，并对相关账号予以注销。否则，视为贵方完全同意并认可本公司修改后的文件。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>2、本公司有权根据需要不时修改本协议。如本协议内容有任何变更，本公司将在平台上公告。一经公告，即自动生效，而无需另行单独通知，请贵方适时关注平台相关页面有关协议和规则的公告。如贵方对本公司修改后的协议或规则不认可、不接受，应立即停止接受平台服务，并注销注册账户。</b>
        否则，视为贵方完全同意并认可本公司修改后的协议和规则。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、如果本协议中任何一条被视为不生效、无效或因任何理由不可执行的，该条应视为可分的且并不影响任何其余条款的有效性和可执行性，其余条款依然对贵我双方具有约束力。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、注册用户可通过平台注销注册账户。但如用户因接受平台服务对本公司或与本公司合作的金融机构负有义务的，用户需履行完毕相关义务方可完成注销。否则，应就此承担全部责任和损失。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>二、平台注册用户的有关要求</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、平台采取实名注册方式，用户须是中华人民共和国大陆公民，年满18周岁，且具有完全民事权利能力和民事行为能力。否则，贵方应立即停止注册、停止使用本平台服务。用户一经注册成为平台用户，即视为符合上述条件，因此产生的损失、后果和责任由用户自行承担。为保障用户权益，本公司有权通过多种方式核查用户的身份情况，对不具备上述条件的用户进行清查，经查确不符合上述条件的，本公司有权强制注销贵方账户，随时终止提供平台服务，因此产生的全部后果、损失和责任由贵方自行承担，给本公司造成损失的，还应全额赔偿，对此，贵方监护人应承担连带责任。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、依本协议要求完成注册后，贵方应对注册账号、密码严格保密，只有贵方本人可以使用该账号、密码，不得将账号、密码泄露、转让、赠与、租借或以其他任何方式提供给第三方使用。
        <b>注册账号和密码为用户的唯一标识，任何使用用户注册账号和密码所行使的行为均为用户本人所为，用户均应承担相应义务和责任。</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、在注册成为平台用户时，贵方应提供贵方自身真实、有效的资料和信息，并保证自贵方注册之时起至贵方使用本服务的全部期间内，贵方所提交的所有资料和信息(包括但不限于电子邮件地址、联系电话、联系地址、个人身份信息、征信信息等)真实、准确、完整、有效，且是最新的，保证本公司及相关服务方能够通过上述联系方式与贵方进行联系。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、若有合理理由怀疑贵方提供的资料错误、不实、过时或不完整的，本公司有权暂停或终止向贵方提供部分或全部服务，包括但不限于封锁或冻结账号、强制注销账号、冻结直至取消贷款额度等。本公司对此不承担任何责任，贵方将承担因此产生的任何直接或间接支出，并赔偿因此给本公司及相关方造成的损失。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        5、如贵方提供的各项信息和资料一旦发生变更，贵方有义务及时更新相应的信息和资料或向本公司提出相应的更新申请，如因贵方未及时更新信息和资料导致本公司无法向贵方提供服务或因信息错误而发生错误服务的，由此导致的全部法律责任和后果将由贵方自行承担，因此给本公司及相关方造成损失的，贵方还应全额赔偿。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        6、如贵方使用他人信息和资料注册使用本服务或向本公司提供的信息和资料不符合上述规定，由此引起的一切责任和后果均由贵方本人承担，本公司不承担因此产生的任何法律责任，因而给本公司造成的任何损失，贵方应全额赔偿。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>三、平台服务内容</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、本公司基于平台提供的服务（在本协议中简称“平台服务”），包括但不限于向用户提供商家及商品信息、消费折扣及消费金融等服务。其中，消费金融服务由具有相应资质的第三方具体提供，本公司仅提供相关媒介服务。具体详情以平台届时提供的服务内容为准。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、为更好地为用户提供服务，本公司有权通过各种途径获得用户信息，并据此评定贵方的个人信用等级，进而对贵方提出的消费金融等申请进行初步审核，以决定是否转介给相关合作金融机构。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、贵方通过平台与商户或其他第三方达成的交易与本公司及平台开发者湖南同城消费网络科技有限公司无关，贵方应根据与交易对方的相关约定享有权利、承担义务，交易出现纠纷的，应妥善协商解决，或商请本公司协调解决。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、基于平台运行和交易安全的需要，本公司可以暂时停止提供或者限制提供平台部分服务功能，在此情况下，只要贵方仍然使用本平台，即表示贵方对此不存在任何异议，并自愿接受和承担相应的后果。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        5、除非另有规定，本公司在平台上推出的新产品、新功能、新服务，均受到本协议的规范。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>四、服务使用规范</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、贵方在使用本平台的过程中，不得发送、发布或展示任何违反中国法律法规规定、本协议约定或侵犯他人合法权益的内容或信息，不得从事任何有违法律法规规定、本协议约定或侵犯他人合法权益的任何活动。一经发现，本公司有权不经贵方同意采取屏蔽、删除等方式阻止该等行为，同时有权不经通知而立即暂停或停止贵方对本服务的全部或部分功能的使用。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、贵方不得在接受本服务过程中从事任何违反中国法律法规或侵犯他人权益的活动，如在本公司网站或其他应用端的论坛上散布非法言论，贬低他人损害他人合法权益等言行。本公司在发现贵方从事该类活动时，有权不经通知而立即停止贵方对本服务的全部或部分功能的使用。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、贵方不得以任何形式使用平台上的任何信息、数据和资料，不得转发平台信息用于商业用途，不得使用任何装置、软件或方法窃取平台数据或干预平台运行。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、贵方在接受平台服务过程中接触的所有内容，包括但不限于文字、数据、图片、音频、视频、源代码和其他所有信息，均由本公司或平台开发者湖南同城消费网络科技有限公司享有完全的知识产权。未经本公司及湖南同城消费网络科技有限公司事先书面同意，贵方或其他任何人不得复制、改编、传播、公布、展示或以任何其他方式侵犯本公司及湖南同城消费网络科技有限公司享有的知识产权及其他合法权益。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        5、发现有第三人冒用或盗用贵方账号及密码，或其他任何未经合法授权而使用贵方账号、密码之情形，贵方应立即通知本公司，本公司有权立即采取冻结账号、冻结贷款额度、限制或停止使用平台全部或部分服务等措施，本公司亦可基于合理怀疑，为防止可能损失的发生或进一步扩大而主动采取前述措施直至停止提供任何服务。在此情况下，如第三方冒用、盗用贵方账号、密码进行了相关操作，该等操作产生的后果和责任由贵方承担，本公司不承担任何责任。若贵方迟延通知，则迟延期间产生的损失，亦由贵方承担，本公司不承担任何责任。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>五、用户信息的保护和披露</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>1、为不断优化平台系统功能，用户在此不可撤销地授权平台开发者湖南同城消费网络科技有限公司有权搜集、保存、查询、整合、使用用户各类信息，包括但不限于用户自行提供、本公司提供及湖南同城消费网络科技有限公司取得的用户个人信息、消费订单信息、消费金融信息等，但不得用于非法用途。湖南同城消费网络科技有限公司仅可向本公司及与本公司合作的金融机构披露用户信息，并确保本公司及相关金融机构不将用户信息用于非法用途，法律法规另有规定或有权机关另有要求的除外。</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、为贵方违反法律法规规定、平台规则或本协议约定，本公司有权通过平台或其他公开渠道公布贵方相关违法违约信息。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、本公司将不断提高技术水平，尽力保护贵方信息，包括建立相应的信息保护制度、机制，配置相应设备等，但贵方应了解，上述保护措施可能受限于现有技术水平而不能确保贵方的信息不会因黑客攻击、网络病毒或其他类似方式被窃取、泄露，对此，本公司不承担相应损失或责任。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、贵方保证并承诺贵方在本公司网络中进行的操作行为合法。贵方同意，本公司有权按照包括但不限于公安机关、检察机关、法院等司法机关、行政机关的要求协助对贵方的账户进行核实等操作，并应要求向上述机关提供贵方的个人信息。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        5、为了保障贵方使用本服务的安全以及不断改进服务质量，本公司将记录并保存贵方登陆和使用本服务的相关信息，但本公司承诺不将此类信息提供给任何第三方(除双方另有约定或法律法规另有规定及本公司关联公司、与本公司合作为用户提供服务的第三方如商户、金融机构等外)。贵方在此同意，本公司有权将贵方注册及使用本服务过程中所提供、形成的信息提供给本公司、本公司关联公司及与本公司合作为用户提供服务的第三方（包括但不限于平台商户、金融机构），除本协议另有规定外，本公司不对外公开或向第三方提供贵方的信息。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>六、免责条款</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、贵方保证，不向其他任何人泄露贵方在注册时向本公司提交的电子邮箱、用户名、账号、密码及安全问题答案等信息，上述信息是贵方在本公司网络平台的唯一身份识别信息。贵方应妥善保管贵方的账号、密码及与账户有关的一切信息。如贵方账号、密码或其他相关信息泄露的，贵方应及时通知本公司，以减少可能发生的损失，因上述原因导致的损失需由贵方自行承担。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、如贵方发现有他人冒用或盗用贵方的手机、账户及密码或进行其他任何未经合法授权而冒用、盗用贵方账号、密码之情形时，应立即以书面或口头方式通知本公司并要求本公司暂停服务。本公司将积极响应贵方的要求；但贵方需理解，对贵方的要求所采取的行动需要合理期限，在此之前，本公司对已执行的指令及(或)所导致的贵方的损失不承担任何责任。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、本服务内容可能涉及或链接到由第三方所有、控制或者运营的其他网站(“第三方网站”)。本公司不能保证也没有义务保证第三方网站上的信息的真实性和有效性。贵方确认按照第三方网站的相关服务协议而非本协议使用第三方网站，第三方网站的内容、产品、广告和其他任何信息均由贵方自行判断并承担风险。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        4、本公司系统因下列状况无法正常运作，使贵方无法使用各项服务时，本公司不承担任何责任，该状况包括但不限于：(1)平台系统停机维护、升级的；(2)电信设备出现故障不能进行数据传输的；(3)因台风、地震、洪水、停电、战争、恐怖袭击等不可抗力之因素，造成本公司系统故障无法执行业务的；(4)由于黑客攻击、电信部门技术调整或故障等原因造成服务中断或者延迟的。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>七、违约责任</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        如果贵方违反了贵方在本协议中所作的陈述、保证、承诺或任何其他义务，致使本公司承受任何损失的，贵方应向本公司做出全额赔偿，包括但不限于本公司所受之直接损失、被第三方索赔或惩处、为维护自身合法权益所支出的全部费用（如公证费、诉讼费、财产保全费、执行费、仲裁费、律师代理费、差旅费、评估费、拍卖费、催收费用等）。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>八、法律适用与管辖</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        本协议之效力、解释、变更、执行与争议解决均适用中华人民共和国大陆地区法律。如发生本协议与适用的法律相抵触时，则这些条款将完全按法律规定重新解释，而其它有效条款继续有效。因本协议产生的争议，按照下述第 <u>（1） </u>种方式解决：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        （1）向本公司住所地人民法院提起诉讼；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        （2）向长沙仲裁委员会申请仲裁。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>九、协议终止</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、除非本公司终止本协议或者贵方申请终止本协议及注销相应用户账户（号）且经本公司同意，否则本协议始终有效。在贵方违反了本协议，或在相关法律法规、政府部门的要求下，本公司有权根据本协议的约定以通知的方式(包括但不限于电子邮件)告知贵方终止本协议、关闭贵方的账户（号）或者限制贵方使用本服务。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、当贵方不再符合本协议第二条所要求的注册用户身份条件(包括但不限于不再具备中国公民身份、因故身亡、丧失民事行为能力等)致使本协议或因接受平台服务所签订其他协议（如买卖合同、借款合同等）无法继续履行时，本公司有权依法终止本协议。贵方的合法授权代表、继承人、监护人、直系亲属在提交相关证明文件后，可向本公司申请查询贵方在本公司系统中注册的用户信息及其他交易信息，并办理资产的转移。同时，本公司有权关闭贵方的账户。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        <b>十、其他</b>
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1、若本协议的部分条款被认定为无效或者无法实施时，本协议中的其他条款仍然有效。对本协议的内容有任何异议，双方可通过订立补充协议的方式来解决。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        2、贵方认可以电子数据形式提交、确认或签署任何文件的方式及其法律效力，且认可本公司电子数据的有效性和证据效力。本公司相关系统及平台所生成和保留记载的相关电子数据及交易记录，以及本公司制作或保留的相关电子及纸质单据、凭证、记录等相关资料，均构成有效证明合同双方之间权利义务关系的确定证据。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        3、特别授权：对于贵方提供的资料及数据信息，贵方在此不可撤销地授权湖南同城消费网络科技有限公司独家的、全球通用的、永久的、免费的许可使用的权利 ，包括但不限于整合、使用、复制、修订、改写、发布、翻译、分发、执行和展示贵方的全部资料数据(包括但不限于注册资料、交易行为数据及全部展示本服务的各类信息)或制作其派生作品，并以现在已知或日后开发的任何形式、媒体或技术，将上述信息纳入其它作品内。
    </p>
</div>