<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/zy.css"/>
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
		<link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
		<title>我的</title>
	    <style>
	        body{
            background:url(/img/imgs/shouxin_backgroung1.png) no-repeat;background-size:100%;
        }
        .p-ck{
         text-align: center;
         margin-top:2%
        }
        .btn-01{
            display: table;
            vertical-align: middle;
            text-align: center;
        }
        .btn-01 p{
          margin-top: -10%;
          text-align: center;
            color: #ffe700;
            font-size: 1.2rem;
            font-weight: bold;
        }
        .container{
            text-align: center;
            margin-top: 119%;
            color: #73181c;
        }
         .m-tcxy h4{
            text-align: center;
            color: #333;
        }
          .m-tcxy h5{
            text-align: center;
            color: #333;
        }
        .m-tcxy p{
            font-size: 0.8rem;
            color: #666;
        }
        .shouce>img{
        	width:100%;
        }
	    </style>
	</head>

	<body>
	<div class="modal" id="mymodal1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
             <div class="m-tcxy">
    <h4>个人征信与信息使用授权书</h4>
    <p>&nbsp&nbsp&nbsp&nbsp您申请使用“佰联同城”平台授信服务，应与当地（${name! }      ）公司（简称城市运营商，下同）签订《会员授信服务合同》，您理解并同意本授权书中的所有内容，并遵照附件《会员授信服务合同》执行。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp一、本人同意并不可撤销地授权:城市运营商可以根据国家有关规定，通过中国人民银行个人信用信息基础数据库、其他依法设立的征信机构、公安部公民身份信息数据库、第三方数据库查询、打印、保存符合法律法规规定的本人个人信息和包括信贷信息在内的信用信息。用途如下:
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp(一)审核本人授信申请;</p>
    <p>&nbsp&nbsp&nbsp&nbsp(二)审核本人资信状况、担保情况;</p>
    <p>&nbsp&nbsp&nbsp&nbsp(三)贷中监控;</p>
    <p>&nbsp&nbsp&nbsp&nbsp(四)贷后管理、不良处置;</p>
    <p>&nbsp&nbsp&nbsp&nbsp(五)处理本人征信异议;</p>
    <p>&nbsp&nbsp&nbsp&nbsp(六)其他本人向贷款人（如小贷公司，下同）申请或办理的业务。</p>
    <p>&nbsp&nbsp&nbsp&nbsp二、本人同意并不可撤销地授权:为便于城市运营商及贷款人给本人授信，城市运营商及贷款人可以将本人的姓名、身份证号、联系方式、银行卡信息及其他相关必要信息传输使用。</p>
    <p>&nbsp&nbsp&nbsp&nbsp三、本人同意并不可撤销地授权:在本人授信资金逾期时，城市运营商及贷款人可将本人的其他联系方式和联系地址等信息传输给贷款人作催收贷款所用</p>
    <p>&nbsp&nbsp&nbsp&nbsp四、本人授权城市运营商及贷款人分享的信息、本人使用贷款人服务产生的信息，为便于向本人提供更加优质的产品和服务，本人同意并不可撤销地授权贷款人可将前述信息分享给必要的合作伙伴。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp声明:本授权书是本人向城市运营商及贷款人作出的单方承诺，效力具有独立性，不因相关合同的任何条款无效而无效。无论授信业务是否获批，本人的授权书、信用报告等资料一律可不退还本人。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp本授权书的有效期为:自本人作出本授权承诺之日起至本人在城市运营商及贷款人处所有业务终结之日止，本人在有授信额度但无余额的情况下，本人与城市运营商及贷款人的关系依然存续，城市运营商及贷款人仍有权向中国人民银行金融信用信息基础数据库、其他依法设立的征信机构查询、提供本人的信用信息。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp若本人因该授权与城市运营商及贷款人发生任何纠纷或争议的，双方首先应友好协商解决;协商不成的，本人同意将纠纷或争议提交城市运营商所在地有管辖权的人民法院管辖。本授权书的成立、生效、履行、解释及纠纷解决，适用我国法律</p>
    <p>&nbsp&nbsp&nbsp&nbsp本人知悉并理解本授权书中所有条款的意义以及由此产生的法律效力，自愿作出上述授权。本授权书内容均为本人真实意思表示、愿意承担由此带来的一切法律后果。</p>
    <p>&nbsp&nbsp&nbsp&nbsp附件：</p>
    <h5>会员授信服务合同</h5>
    <p>&nbsp&nbsp&nbsp&nbsp您理解并同意，在申请使用”佰联同城”平台授信服务时，应与当地（  ${name! }    ）公司（简称城市运营商，下同）签约，除遵守《个人征信与信息使用授权书》的相关承诺外，还适应本附件合同的相关约定：</p>
    <p>&nbsp&nbsp&nbsp&nbsp第一条 术语定义</p>
    <p>&nbsp&nbsp&nbsp&nbsp 1.1消费授信：指有资质贷款人（如小贷公司，简称贷款人，下同）向您提供的，限于消费用途的授信服务，您可以使用该笔授信额度在”佰联同城”平台购买认可的商品或服务。本附件合同中简称“本服务”。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp
        1.2指定账户：指您申请、签约、使用、终止本服务所必需的您本人的银行或其他有效账户。</p>
    <p>&nbsp&nbsp&nbsp&nbsp1.3指令：指您通过指定账户，输入支付密码或按照约定的方式，在”佰联同城”平台发出的各项指令（包括且不限于信息传递指令、授权性指令等），所有指令及其内容均不可撤销及变更，除非本附件合同或双方另有明确约定。</p>
    <p>&nbsp&nbsp&nbsp&nbsp1.4本金：指贷款人向您发放的授信资金的本金。</p>
    <p>&nbsp&nbsp&nbsp&nbsp 1.5应还款额：指您累计到期或已逾期在当月还款日应当偿还的授信资金的本金、利息、罚息、手续费及其他款项（如有）的金额总和。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp 1.6账单日：指每个月由城市运营商向您发出的，通知您本期应还款项的具体日期，您的应还款项以”佰联同城”平台的计算及提示为准。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp 1.7还款日：指您所获得的授信资金每月的固定还款时间。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp1.8期数：指您在使用分期还款服务时，您选择的分几次在还款日归还授信资金本金和手续费。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp1.9代扣：您授权在您开通本服务未主动按约偿还时，您理解并同意将贷款人增加为为您提供代扣服务的扣款资金方。</p>
    <p>&nbsp&nbsp&nbsp&nbsp● 消费授信服务</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1服务规则</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.1授信额度：指城市运营商联合贷款人根据您的申请和综合状况核定的、为您提供消费性授信的最高额度，在授信额度有效期内可循环使用，您可登陆”佰联同城”平台相关页面查询您的授信额度。您理解并同意，城市运营商及贷款人可根据对您的风险评估状况随时调整授信额度或停止授信，您也可主动向城市运营商及贷款人申请调整授信额度。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.2授信期限：指在授信额度内，您可向城市运营商申请授信资金的期限，以月为单位有3、6、9、12、18、24期。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.3授信资金发放和支付：您使用授信资金在入驻”佰联同城”平台的商户消费后，城市运营商发出指令由贷款人将您的消费授信资金支付给该商户在”佰联同城”平台的指定账户；您同意由城市运营商及贷款人按照交易所使用的服务形式完成上述支付。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.4授信资金的用途：本附件合同项下的授信资金仅能用于城市运营商认可的在”佰联同城”平台的消费之用，严禁使用本服务套现。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.5明细查阅：城市运营商将于每月初在”佰联同城”平台向您展示上月服务的详情，如消费明细等，您可主动到平台查询。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.1.6到期还款：您应在还款日24点前（含当日）清偿全部应还款额，逾期未清偿，将影响您的征信记录。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.2服务类型</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.2.1不分期服务（又称“不分期”）：指您在授信资金发放后的约定还款日须一次性全额归还本金及费用的服务类型。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.2.2分期还款服务：分期还款服务的期数和每期应还款金额以本服务平台客户端显示为准，您应于分期还款期间内的每期还款日按时归还相应本金及费用。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.2.3城市运营商可根据运营需要及对您的综合风险评估暂停直至取消您享受某种服务的资格。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.3费用计收规则</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.3.1基于本附件合同项下现有的服务类型，城市运营商将向您收取或代收以下费用：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.3.1.1手续费：在您使用分期还款服务的情况下，城市运营商将向您收取分期手续费，手续费按照授信资金本金的一定比例收取，分期按月偿还。根据分期还款的期数不同，手续费收取比例不同。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.3.1.2逾期利息：无论是不分期服务、还是分期还款服务，若您在还款日后仍未清偿当月应还的授信资金本金、手续费和利息的，城市运营商将按每日未偿还授信资金本金、手续费以及利息的合计余额的千分之一逐日计收逾期利息，直至您按照要求将授信资金本金、手续费和利息等清偿完毕。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.3.2您使用本服务须缴纳的手续费、利息以及逾期利息等收费标准详
        见”佰联同城”平台客户端页面或交易支付页面相应展示。城市运营商有权根据业务发展需要修改或新增费用项目或标准，费用项目或标准修改须在本服务网站进行公告。若您不同意修改或新增费用项目、标准，或不接受城市运营商向您展示的服务定价，您可部分或全部停止使用本服务。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4还款来源与还款方式</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4.1还款来源：您仅限于以银行借记卡账户余额等非信贷性资金作为还款资金来源。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4.2本服务的还款方式仅为：</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4.2.1主动还款：您可主动登录“佰联同城”平台使用合法的还款资金来源，根据城市运营商认可的还款规则，清偿相应的应还款额。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4.2.2提前还款：在征得城市运营商同意的前提下，您可自主选择微信、绑定的银行卡账户等途径提前归还全部或部分授信资金本金及利息费用（如有）。 </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.4.2.3到期自动还款：在双方另有约定前，您不可撤销地授权贷款人，在还款日自动从您指定银行账户扣划款项用于清偿应还款额。您知晓并同意，贷款人有权在还款日当天的任一时点向您发起到期自动还款操作。
        你理解并同意，到期自动还款功能是城市运营商联合贷款人为方便您还款而提供的一项便捷服务。特别的，如您在绑定的银行卡中存放金额不足以归还全部欠款的，平台将无法为您自动还款，
        所以您应密切注意还款提示及结果，若出现自动还款失败，您应及时履行主动还款的义务，若因您延误或拒绝还款导致的授信资金逾期损失由您自行承担。 </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.5还款顺序</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.5.1您的还款将先用于偿还逾期利息，再依次为手续费、本金；若还款资金仍有剩余，将用于偿还如诉讼费、律师代理费、公证费、催收费用等（如有）。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.5.2城市运营商联合贷款人有权根据风险政策和您的信用状况变更前述还款顺序，特别是在还款金额小于您的应还金额的情况下，而无需另行通知您。</p>
    <p>&nbsp&nbsp&nbsp&nbsp2.6特别授权</p>
    <p>&nbsp&nbsp&nbsp&nbsp本服务项下您的债务逾期未清偿，您不可撤销地同意：城市运营商可立即停止向您提供所有消费、授信等功能和服务，贷款人可执行止付、冻结、扣划资金等还款操作。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.7特别约定</p>
    <p>&nbsp&nbsp&nbsp&nbsp 2.7.1您承诺使用授信资金的交易背景真实合法。</p>
    <p>&nbsp&nbsp&nbsp&nbsp 2.7.2您应及时登录平台，查阅还款日、应还款额及其明细等信息。</p>
    <p>&nbsp&nbsp&nbsp&nbsp 2.7.3城市运营商可不时单方面调整可使用授信服务的商品类目和商户范围且无需另行通知您。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp2.7.4若发生交易退款，平台商户退回授信资金，该笔资金按照退款还款流程处理，您与城市运营商另有约定的情形除外。</p>
    <p>&nbsp&nbsp&nbsp&nbsp 2.7.5除本附件合同另有约定外，您可以随时终止本服务，但前提是本附件合同项下债务已全部清偿。</p>
    <p>&nbsp&nbsp&nbsp&nbsp第三条 权利和义务</p>
    <p>&nbsp&nbsp&nbsp&nbsp 3.1您有权要求对您提供的有关个人信息予以保密，但法律法规另有规定或本附件合同另有约定的除外。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp3.2您保证您在“佰联同城”平台信息系统中提供的个人信息、交易信息真实、合法、有效。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp3.3您应妥善保管您的账户名、密码、手机校验码等与账户有关的一切信息。您应确保不向其他任何人泄露您账户的密码。对于因密码泄露所致的损失，由您自行承担。如您发现有他人冒用或盗用您的账户及密码或任何其他未经合法授权之情形时，应立即以有效方式通知城市运营商，要求贷款人暂停本服务。同时，您理解您的请求采取行动需要合理期限，在此之前，城市运营商及贷款人对已执行的指令所导致的您的损失不承担任何责任。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp 3.4无论本附件合同是否生效或终止，城市运营商均有权保留有关资料并不予退回。</p>
    <p>&nbsp&nbsp&nbsp&nbsp 3.5您变更通讯地址、电子邮件地址、电话、银行账户等事项的，应于变更后立即通知城市运营商并办理变更手续。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp第四条 违约及违约处理</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1违约事件</p>
    <p>&nbsp&nbsp&nbsp&nbsp您发生下列任一情况，均构成违约：</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.1您未按本附件合同规定按时足额偿还应还款额；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.2您未按本附件合同约定用途使用授信资金，如利用本服务套现等；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.3您提供虚假的信息；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.4您被宣告失踪、处于限制民事行为能力或丧失民事行为能力状态、被
        刑事监禁、或您发生重大变故等可能影响您偿付能力的情况；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.5您卷入或即将卷入重大的诉讼或仲裁程序及其他法律纠纷，可能影响
        您的偿债能力；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.6您转移资产，以逃避债务的；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.7您未履行到期债务，或发现您有其他拖欠债务的行为的；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.1.8您的资信情况或清偿能力出现其他重大不利变化；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2 违约处理</p>
    <p>&nbsp&nbsp&nbsp&nbsp如您出现本条第一款所述之任何违约情形，或违背您在本附件合同项下其他任何义务、陈述、保证或承诺的，城市运营商还有权采取下列一项或多项措施：
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.1要求您限期纠正违约行为；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.2单方面停止向您提供任何服务；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.3宣布本附件合同项下未偿还的授信资金提前到期，并要求您提前全部
        清偿；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.4处分抵押/质押财产（如有）或要求担保人（如有）代偿，实现债权；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.5与贷款人对您执行止付、冻结、扣划资金等还款操作；</p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.6以合法手段追偿您应还款项（如委托债务催讨公司、律师事务所等其
        他第三方机构代为追讨，申请相关部门进行调查、向法院提起诉讼等），由此引起的费用（如公证费、诉讼费、财产保全费、执行费、仲裁费、律师代理费、差旅费、评估费、拍卖费、催收费用等）由您承担；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.7将您纳入“佰联同城”平台黑名单处理，并公布您的征信不良信息。
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.8自授信资金发放之日起向您收取日利率为0.1%的利息；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.9要求您支付授信资金本金、手续费、利息、逾期利息等总金额30%的
        违约金；
    </p>
    <p>&nbsp&nbsp&nbsp&nbsp4.2.10若您的行为涉嫌犯罪的，进一步追究您的刑事责任。
    </p>
</div>
            </div>
        
        </div>
    </div>
</div>
<div class="modal" id="mymodal2" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
               
            </div>
            <div class="modal-body shouce">
              <img src="${basePath!}/img/imgs/background_bangzhu.png">
            </div>
         
        </div>
    </div>
</div>
		<div class="container">
		    <input type="checkbox" id="agree" style="margin-top: -1%"/>
		    同意 <a href="#mymodal1" data-toggle="modal" style="color: #73181c;">《用户授信协议》</a>
		</div>
		<div class="btn-01 doApply">
		    <img src="${basePath!}/img/imgs/btn_ljshengqing.png"style="width: 61%" alt=""/>
		    <p>立即申请</p>
		</div>
        <div class="p-ck">
		    <a href="#mymodal2" data-toggle="modal"> <u style="color: #73181c">查看激活帮助手册</u></a>
		</div>
	</body>
	<script src="${basePath!}/js/jquery-1.11.3.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
	<script>
		$(document).ready(function() {
			$(".doApply").click(function(){
				if(!$("#agree").is(':checked')) api.tools.toast("请先同意《授信协议》");
				else{
					 window.location = "${basePath!}/cuser/updateTransPwd1";
				}
			});
		}); 
	</script>

</html>