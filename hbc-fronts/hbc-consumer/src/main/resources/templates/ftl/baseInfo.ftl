<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <#include "merchant/common.ftl"/>

		<title>基本信息</title>
	    <style>
            .sj-ul li {
                padding: 10px 0px 1px 10px;
            }
            .sj-ul .col-xs-4{
                margin-top: 5px;
                padding-left: 0;
            }
            .sj-ul .col-xs-8{
                text-align: right;
                margin-bottom: 10px;
            }
            .textright{
                text-align: right;
                margin-right: 4%;
                margin-top: 1%;
            }
            .textright>img{
                width:6px;
            }
            .btn-block{
                width: 94%;
                height: 40px;
                margin-left: 3%;
                background: #f49110;
                color: #fff;
                border-radius: 30px;
                outline: none;
                border: none;
                text-align: center;
                cursor: pointer
            }
            .webuploader-pick {
                position: relative;
                display: inline-block;
                cursor: pointer;
                background: url(${basePath!}/consumer/img/imgs/icon_enter.png) no-repeat right;
                background-size: 8px 10px!important;
                /* background: #00b7ee; */
                padding: 10px 15px;
                color: #fff;
                text-align: center;
                border-radius: 3px;
                overflow: hidden;
            }
            .webuploader-pick-hover {
                background-size: 8px 10px!important;
                background:url(${basePath!}/consumer/img/imgs/icon_enter.png) no-repeat right;;
            }
            .webuploader-container {
                position: relative;
                padding-right: 10px!important;
            }
	    </style>
	</head>

    <body>
    <div class="c-content" style="margin-top: 0px; padding:0px;">
        <ul class="sj-ul">
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-4">
                            头像
                        </div>
                        <div class="col-xs-8" style="padding: 0">
                            <#if cUserInfoDto.headImg == "images/uhead.png">
                                <div class="img" file-id="headImg" file-value="${basePath!}/${cUserInfoDto.headImg!}"></div>
                            </#if>
                           <#if cUserInfoDto.headImg != "images/uhead.png">
                            <div class="img" file-id="headImg" file-value="${cUserInfoDto.headImg!}"></div>
                           </#if>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box" id="realName-div">
                    <div class="input-left-name"> 真实姓名:</div>
                    <div class="wkt-flex">
                        <input type="text" id="realName" name="realName" value="${cUserInfoDto.realName!}" UNSELECTABLE='true' class="form-control reg-input" maxlength="4"  placeholder="您的真实姓名"
                               style=" background-size:3%;background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">绑定手机号：</div>
                    <div class="wkt-flex">
                        <input type="tel" class="form-control reg-input" id="mobile" readonly UNSELECTABLE='true' onfocus="this.blur()" value="${cUserInfoDto.mobile!}"
                               style="background-color:#fff;  background-size:3%;width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box" id="sex-name-div">
                    <div class="input-left-name">性别：</div>
                    <div class="wkt-flex">
                        <input type="hidden" id="sex" value="${cUserInfoDto.sex!}"/>
                        <input type="text" readonly UNSELECTABLE='true' onfocus="this.blur()" id="sex-name" value="${sex!}" name="sex" class="form-control reg-input" placeholder="请选择"
                               style="cursor: pointer;background-color:#fff;  background:url(${basePath!}/img/imgs/icon_enter.png) no-repeat right; background-size:3%;width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box" id="birthday-div">
                    <div class="input-left-name">出生年月：</div>
                    <div class="wkt-flex">
                        <input type="text" readonly UNSELECTABLE='true' onfocus="this.blur()" id="birthday" value="${cUserInfoDto.birthday!}" name="birthday" class="form-control reg-input" placeholder="请选择"
                               style="cursor: pointer;background-color:#fff;  background:url(${basePath!}/img/imgs/icon_enter.png) no-repeat right; background-size:3%;width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box" id="area-div">
                    <div class="input-left-name"> 位置：</div>
                    <div class="wkt-flex">
                        <input type="hidden" id="areaId" value="${cUserInfoDto.area!}"/>
                        <input type="tel" readonly UNSELECTABLE='true' onfocus="this.blur()" id="displayName" name="displayName" value="${cUserInfoDto.disPlayName!}" class="form-control reg-input" placeholder="请选择"
                               style="background-color:#fff;background:url(${basePath!}/img/imgs/icon_enter.png) no-repeat right; background-size:3%;width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">详细地址：</div>
                    <div class="wkt-flex">
                        <input type="text" id="address" name="address" value="${cUserInfoDto.address!}" class="form-control reg-input" value="" placeholder="请填写您所居住的街道，门，牌号"
                               style="background:#fff; background-size:3%;width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <button class="btn-block tj">保存</button>
    </body>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <#include "./agreement/mune_btn.ftl">
	<script>
		$(document).ready(function() {
            $(".img").uploadImg2();

			var n = new Date();
			var dtPicker = new mui.DtPicker({
				type: 'date',
				beginDate: new Date(1900, 1, 1),
				endDate: new Date(n.getFullYear(), n.getMonth(), n.getDate())
			});
			
			var pickerCity = new mui.PopPicker({
			    layer: 3
			});

            $.ajax({
                type:'GET',
                url:'${basePath!}/js/area.json',
                success: function (data)
                {
                    pickerCity.setData(data);
                },
                dataType: "json",//请求数据类型
            },function(d){
                console.log(d);
            });

			var sexPicker = new mui.PopPicker();
			sexPicker.setData([{value: '1', text: '男'},{value: '2', text: '女'}]);

			$("#birthday-div").click(function() {
				dtPicker.show(function(s) {
					$("#birthday").val(s.y.text + "-" + s.m.text + "-" + s.d.text);
				});
			});
			$("#sex-name-div").click(function() {
				sexPicker.show(function(s) {
					$("#sex-name").val(s[0].text);
					$("#sex").val(s[0].value);
				});
			});
			
			$("#area-div").click(function(){
				pickerCity.show(function(s) {
					$("#areaId").val(s[2].value);
					$("#displayName").val(s[0].text+s[1].text+s[2].text);
				});
			});

			$(".tj").click(function() {
			    if ($(this).hasClass('disabled')){
			        return;
                }
				var realName = $.trim($("#realName").val());
				var sex = $.trim($("#sex").val());
				var birthday = $.trim($("#birthday").val());
				var address = $.trim($("#address").val());
				var area = $.trim($("#areaId").val());
				var headImg = $("#img_headImg").attr("src");
				$(this).addClass('disabled');
				$(this).html('提交中...');
				$this = $(this);
				api.tools.ajax({
					url: "${basePath!}/user/userInfo/updateUserInfo",
					data: {
						realName: realName,
						address: address,
						sex: sex,
						birthday: birthday,
						headImg: headImg,
						area:area
					},
					cover:true
				},
				function(d) {
					api.tools.toast(d.msg);
					$this.html('保存');
					$this.removeClass('disabled');
					if(d.code != 1){
						setTimeout(function(){
							api.tools.goback();
						},1500);
					}
				});
			});
		});
	</script>
</html>