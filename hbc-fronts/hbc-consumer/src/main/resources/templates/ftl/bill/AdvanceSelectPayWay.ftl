<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <link rel="stylesheet" href="${basePath!}/css/zhifu.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>立即还款</title>
 	 <style>
         .addBankCard{
             margin-top: 20px;
             width: 100%;
             text-align: center;
             text-after-overflow: none;
         }
        .addBankCard>a{
            color: #f49110;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="Bills">
    <p>待还账单金额</p>
    <h3>${totalMoney!}</h3>
</div>
<div class="top_one">
        <li>
            <b>网银快捷</b>
        </li>
    <ul id="bankList" class="banklistcss">
        <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/img/ico_yinlianzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>长沙银行</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="wangyin" name="zhifu" value="4">
                    </div>
                </div>
            </div>
        </li>-->
    </ul>
</div>
<div class="addBankCard">
    <a href="#">添加银行卡</a>
</div>

<button class="btn-block submit_btn" onclick="login(1)">下一步</button>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>

    $(document).ready(function() {

        var tradeList;
        /*查询所有的交易记录*/
        api.tools.ajax({
            url:"${basePath!}/consumer/bill/findTradingList",
        },function (data) {
            if(data.code==0){
                tradeList=data.data.tradeList
            }
        });


        api.tools.ajax({
            url: "${basePath!}/bank/findQuickBankCard",
        }, function (d) {
            console.log(d);
            for (var i = 0; i < d.data.length; i++) {
                data = {
                    bankName: d.data[i].bankName,
                    contractId: d.data[i].contractId
                };
                data.path = '${basePath!}';
                var html = template.load({
                    host: '${basePath!}/',
                    name: "bankList",
                    data: data
                });
                $html = $(html);
              // $html.find(".container-fluid").attr("data1", d.data[i].id);
              //  $html.find(".container-fluid").click(function () {
              //      window.location = "${basePath!}/consumer/bill/jumpDivideDetail?id=" + $(this).attr("data1");
              //  });
                $("#bankList").append($html);
                $(".banklistcss input:first").attr("checked",true);
            }
        });


        $(".submit_btn").click(function() {
            var contractId= $("input[name='zhifu']:checked").val();
            if(!contractId){
                layer.msg("请选择您的银行卡！");
                return;
            }
            var index = layer.load();
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/AdvancePaybackMoney",
                data:{
                    tradeList:JSON.stringify(tradeList),
                    payWay:"1",
                    contractId:contractId
                }
            },function (data) {
                layer.close(index);
                console.log(data);
                if(data.code==0){
                    window.location ="${basePath!}/consumer/bill/inputAdvancePayMsgCode?agreeid="+data.data.agreeid+"&orderid="+data.data.orderid+"&thpinfo="+data.data.thpinfo;
                }else{
                    layer.msg(data.msg)
                }
            });
        });

        $(".addBankCard").click(function() {
          window.location = " ${basePath!}/consumer/bill/goToBindCard?uid=${uid!}";
        });

    });


</script>
</body>
</html>