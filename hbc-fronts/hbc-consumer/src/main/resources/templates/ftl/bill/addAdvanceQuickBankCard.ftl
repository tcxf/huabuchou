<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="${basePath!}/css/card_list.css" />
		<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
        <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
		<title>添加银行卡</title>
        <style>
            .container_gress{
                margin-top: 10px;
                color:#999;
                height: 20px;
                line-height: 20px;
            }
            .container_gress .col-xs-12{
                text-align: center;
            }
            .container_gress .col-xs-12>.col-xs-3{
                text-align: right;
                padding: 0;
                margin-top: -2px;
            }
            .container_gress .col-xs-12>.col-xs-9{
                text-align: left;
                padding: 0;
            }
            .container_gress .col-xs-12>.col-xs-9>a{
                text-decoration: none;
                font-size: 12px;
            }
		</style>
	</head>
	<body>
    <div class="card_content">
        <p>请绑定持卡人本人的银行卡</p>
        <div class="card_list">
            <ul>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                真实姓名
                            </div>
                            <div class="col-xs-8">
                                <input type="text" type="tel" id="realName" placeholder="请输入您的姓名">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                身份证号码
                            </div>
                            <div class="col-xs-8">
                                <input type="text" id="idCard" placeholder="请输入您的身份证号码">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                银行卡号
                            </div>
                            <div class="col-xs-8">
                                <input type="text" type="tel" id="bankCard" placeholder="请输入您的银行卡号">
                            </div>
                        </div>
                    </div>
                </li>
               <#-- <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                开户行
                            </div>
                            <div class="col-xs-8">
                                <input type="text" id="bankName"  placeholder="请输入您的银行名称">
                            </div>
                        </div>
                    </div>
                </li>-->

            </ul>
        </div>
    </div>

        <button class="btn-block tj" onclick="login(1)">同意并绑卡</button>

    <div class="container_gress">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-3">
                        <input type="checkbox" id="agree1"/>
                    </div>
                    <div class="col-xs-9">
                        同意
                        <a href="#mymodalSix" data-toggle="modal" style="color: #008fff;">《银行卡支付协议》</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	 <#include "../agreement/models.ftl">
	</body>
	 <script src="${basePath!}/js/jquery-1.11.3.js"></script>
     <script src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
    <#include "../agreement/mune_btn.ftl">
	<script>
		$(document).ready(function() {
            $("#bankCard").on("input",function(){
                var val=$(this).val().replace(/\s/g,"").replace(/(\d{4})/g,"$1 ");
                $(this).val(val);
            });
            $("#bankCard").on("keyup",function(){
                //获取当前光标的位置
                var caret = this.selectionStart;
                //获取当前的value
                var value = this.value;
                //从左边沿到坐标之间的空格数
                var sp =  (value.slice(0, caret).match(/\s/g) || []).length;
                //去掉所有空格
                var nospace = value.replace(/\s/g, '');
                //重新插入空格
                var curVal = this.value = nospace.replace(/\D+/g,"").replace(/(\d{4})/g, "$1 ").trim();
                //从左边沿到原坐标之间的空格数
                var curSp = (curVal.slice(0, caret).match(/\s/g) || []).length;
                //修正光标位置
                this.selectionEnd = this.selectionStart = caret + curSp - sp;
            });



			var isCheck = true;
			$("#check").click(function(){
				if($.trim($("#bankCard").val()).length >= 16){
					if($(this).find("img").attr("src").indexOf("checkbox.png") == -1){
						$(this).find("img").attr("src","<%=path%>/img/checkbox.png");
		//					$(".tj").addClass("disabled");
						isCheck = false;
					}else{
						$(this).find("img").attr("src","<%=path%>/img/check-reg.png");
		//					$(".tj").removeClass("disabled");
						isCheck = true;
					}
				}
			});
			env = 'keyup';
			if(api.tools.isIos()){
				env = 'input';
			}
			
			var cardType = 1;
			function check(){
				if($.trim($("#realName").val()) == ""){
					api.tools.toast("请输入持卡人姓名");
					return false;
				}
                if(!(/^[\u4e00-\u9fa5]{2,4}$/.test($("#realName").val()))){
                    api.tools.toast("姓名有误，请重填");
                    return false;
                }
				if($.trim($("#idCard").val()) == ""){
					api.tools.toast("请输入持卡人身份照");
					return false;
				}
                if(!(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test($("#idCard").val()))){
                    api.tools.toast("身份证号码有误，请重填");
                    return;
                }
				if($.trim($("#bankCard").val()) == ""){
					api.tools.toast("请输入银行卡号");
					$(".xyk").hide();
					cardType = 1;
					return false;
				}
				if($.trim($("#bankCard").val()).length < 16){
					$(".xyk").hide();
					cardType = 1;
					api.tools.toast("请输入正确的银行卡号");
					return false;
				}

				if(cardType == 2){
					if($.trim($("#valiDate").val()) == ""){
						api.tools.toast("请输入信用卡有效期");
						return false;
					}
					if($.trim($("#cvv").val()) == ""){
						api.tools.toast("请输入信用卡CVN码");
						return false;
					}
				}

                if(!$("#agree1").is(':checked')) {
                    api.tools.toast("请先同意《绑卡协议》");
                    return false;
                }
				return true;
			}
			
			$(".tj").click(function(){
				if(!check()) return;
				if($(this).hasClass("disabled")) return;
				$(this).html("处理中...");
				$(this).addClass("disabled");
				$this = $(this);
				api.tools.ajax({
					url:"${basePath!}/bank/agreeSaveQuickBankCard",
					data:{
                        acctno:$("#bankCard").val(),
                        mobile:$("#mobile").val(),
                        acctname:$("#realName").val(),
                        idno:$("#idCard").val()
					}
				},function(d){
					$this.html("绑定");
					$this.removeClass("disabled");
					if(d.code == 1){
                        api.tools.toast(d.msg);
					}else if(d.code == 0){

					    //?planId=" + $(this).attr("data1") + "&waitpay=" + $(this).attr("data2")+ "&splitType=" + $(this).attr("data3");
                        window.location = "${basePath!}/bank/jumpAdvanceInputMsgCode?acctno="$("#bankCard").val()+"&mobile="+$("#mobile").val()+"&acctname="+$("#realName").val()+"&idno="+$("#idCard").val()+"&thpinfo="+ d.data;
					}
				});
			});
		});
	</script>

</html>