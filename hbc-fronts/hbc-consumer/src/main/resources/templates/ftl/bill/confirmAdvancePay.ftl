<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>银行卡短信验证</title>
    <style>
        .message_post>p{
            font-size: 14px;
            color: #666;
        }
        .look_message{
            margin-top: 20px;
            width: 94%;
            height: 40px;
            padding-top: 10px;
            box-shadow: 1px 1px 1px 2px #ededed;
            margin-left: 3%;
        }
        .look_message>label{
            font-size: 14px;
            padding-left: 10px;
        }
        .look_message input{
            border: none;
            outline: none;
            padding-left: 10px;
        }
        ::-webkit-input-placeholder{
            color: #dedede;
        }
        .btn-block{
            margin-top: 50px;
            width: 80%;
            height: 40px;
            line-height: 40px;
            background: #f48e0e;
            text-align: center;
            margin-left: 10%;
            border-radius: 30px;
            color: #fff;
            outline: none;
            border: none;
        }
    </style>
</head>
<body>
  <div class="message_post">
     <#-- <p>短信将发送到您<span>188****1234</span>的手机上，请注意查收。</p>-->
      <div class="look_message">
          <label>短信验证码:</label>
          <input type="text" placeholder="请输入您的短信验证码" id="msgCode" value="123456">
      </div>
  </div>
  <button class="btn-block tj">确认支付</button>
</body>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>

    $(document).ready(function() {

        var tradeList;
        /*查询所有的交易记录*/
        api.tools.ajax({
            url:"${basePath!}/consumer/bill/findTradingList",
        },function (data) {
            if(data.code==0){
                 tradeList=data.data.tradeList
            }
        });

        $(".tj").click(function() {
            var index = layer.load();
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/AdvancePaybackMoney",
                data:{
                    tradeList:JSON.stringify(tradeList),
                    payWay:"1",     /*支付方式 1:快捷支付 2;微信支付*/
                }
            },function (data) {
                layer.close(index);
                if(data.code==0){
                    window.location = "${basePath!}/consumer/bill/jumpAdvancePaySuccess";
                }else{
                    layer.msg(data.msg)
                }
            });
        });

    });


</script>
</html>