<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>分期明细</title>
</head>
<body>
<div class="byStages">
    <p>剩余分期总额</p>
    <h2 id="RemainPlanMoney"></h2>
    <h6 id="CurrentMonthMoney"></h6>
</div>
<div class="stagingDetail">
    <ul id="fenqiList">
        <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        <p>1/6期</p>
                        <h6>2018-01-11</h6>
                    </div>
                    <div class="col-xs-6">
                        <p>￥1000.00</p>
                        <h6>已完成</h6>
                    </div>
                </div>
            </div>
        </li>-->
    </ul>
</div>
</body>

<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>

   $(document).ready(function() {

        loadTradingDetail();
        //加载交易记录
        function loadTradingDetail(){
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/findDivideDetail",
                data:{
                    id:"${id!}"
                }
            },function(d){
                $("#RemainPlanMoney").html(d.data.RemainPlanMoney)
                $("#CurrentMonthMoney").html("本期应还: ￥"+d.data.CurrentMonthMoney)
                for (var i = 0; i < d.data.planList.length; i++) {
                    var obj = d.data.planList[i];
                    data = {
                        currentTime: obj.currentTime,
                        totalTime: obj.totalTime,
                        time: obj.repaymentDate,
                        totalMoney: obj.totalMoney,
                        status: obj.status == 1 ? "已还款":"进行中",
                        type: obj.status == 1 ? "over":"ing"
                    };

                    var html = template.load({
                        host: '${basePath!}/',
                        name: "bill_fenqi_list",
                        data: data
                    });
                    $html = $(html);
                    $html.find(".container-fluid").attr("data1", d.data.planList[i].planId);
                    $html.find(".container-fluid").attr("data2", d.data.planList[i].waitpay);
                    $html.find(".container-fluid").attr("data3", d.data.planList[i].splitType);
                    $html.find(".container-fluid").click(function () {
                        /*跳转页面(逾期未还/待还)*/
                        window.location = "${basePath!}/consumer/bill/jumpPlanDetailInfo?planId=" + $(this).attr("data1") + "&waitpay=" + $(this).attr("data2")+ "&splitType=" + $(this).attr("data3");
                    });
                    $('#fenqiList').append($html);
                }

            });
        }

    });


</script>
</body>
</html>