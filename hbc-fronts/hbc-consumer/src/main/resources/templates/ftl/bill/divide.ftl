<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <link rel="stylesheet" href="${basePath!}/css/possword.css">
    <title>我要分期</title>
</head>
<body>
<div class="my_byStages">
    <p>分期金额</p>
    <h3 id="totalMoney"></h3>
</div>
<div class="choice_byStages">
    <div class="by_stages_num">
        <div class="qs">期数</div>
        <ul id="splitMonth">
            <li class="active" >
                <a href="#" value="3">3个月</a>
            </li>
            <li>
                <a href="#" value="6">6个月</a>
            </li>
            <li >
                <a href="#" value="9">9个月</a>
            </li>
            <li>
                <a href="#" value="12">12个月</a>
            </li>
        </ul>
    </div>
    <div class="by_stages_list">
        <ul>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-8">
                            总服务费
                        </div>
                        <div class="col-xs-4" id="totalFee">

                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-8">
                            总分期本金
                        </div>
                        <div class="col-xs-4" id="totalMoneys">

                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container-fluid">
                    <div class="row goClick">
                        <div class="col-xs-8">
                            总还款金额
                        </div>
                        <div class="col-xs-3" id="Summoney">

                        </div>
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/imgs/icon_enter.png" alt="">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="periodization_model">
    <div class="periodization_content">
        <div class="periodization_head">
            <span>分期明细</span>
            <img id="closed" src="${basePath!}/img/img/ico_close_img.png" alt="">
        </div>
        <div class="periodization_body">
            <ul id="plan_list">

            </ul>
        </div>
    </div>
</div>
<div class="ftc_wzsf">
    <div class="srzfmm_box">
        <div class="qsrzfmm_bt clear_wl" id="qxfq">
            <img src="${basePath!}/img/ico_cancel.png" class="tx close fl">
            <span class="fl">请输入支付密码</span></div>
        <div class="zfmmxx_shop">
            <div class="zhifu_price"></div>
        </div>
        <ul class="mm_box">
            <li></li><li></li><li></li><li></li><li></li><li></li>
        </ul>
    </div>
    <div class="numb_box">
        <ul class="nub_ggg">
            <li><a href="javascript:void(0);" class="zf_num">1</a></li>
            <li><a href="javascript:void(0);" class="zj_x zf_num">2</a></li>
            <li><a href="javascript:void(0);" class="zf_num">3</a></li>
            <li><a href="javascript:void(0);" class="zf_num">4</a></li>
            <li><a href="javascript:void(0);" class="zj_x zf_num">5</a></li>
            <li><a href="javascript:void(0);" class="zf_num">6</a></li>
            <li><a href="javascript:void(0);" class="zf_num">7</a></li>
            <li><a href="javascript:void(0);" class="zj_x zf_num">8</a></li>
            <li><a href="javascript:void(0);" class="zf_num">9</a></li>
            <li><a href="javascript:void(0);" class="zf_empty">清空</a></li>
            <li><a href="javascript:void(0);" class="zj_x zf_num">0</a></li>
            <li><a href="javascript:void(0);" class="zf_del">删除</a></li>
        </ul>
    </div>
    <div class="hbbj"></div>
</div>
<button class="btn-block repayment_btn">确认分期</button>
<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {
        /**
         * 分期利率，总额 ，本月应还
         */
        $.ajax({
            type: "POST",
            url: "${basePath!}/user/spilt/QueryMon",
            data: {},
            dataType: "json",
            success: function (dataf) {
                console.log(dataf);
                 $("#totalMoney").html("￥"+dataf.data[0].totalCorpus)    /*渲染分期金额*/
                  $("#totalMoneys").html("￥"+dataf.data[0].totalCorpus)    /*渲染分期金额*/
                  $("#totalFee").html("￥"+dataf.data[0].totalFee)    /*渲染总服务费金额*/
                  $("#Summoney").html("￥"+(dataf.data[0].totalFee+dataf.data[0].totalCorpus))    /*总还款金额*/
            }
        });

        //默认分期
        var splitMonth="3";
        loadfenqi(splitMonth);

        //点击369...分期
        $("#splitMonth li").click(function () {
                 splitMonth= $(this).children("a").attr("value");
                 loadfenqi(splitMonth);
        })

        function loadfenqi(splitMonth){
            $.ajax({
                type: "POST",
                url: "${basePath!}/user/spilt/QueryMon",
                data: {
                    splitMonth:splitMonth
                },
                dataType: "json",
                success: function (d) {
                    console.log(d);
                    $('#plan_list').html("");
                    $("#totalMoney").html("￥"+d.data[0].totalCorpus)    /*渲染分期金额*/
                    $("#totalMoneys").html("￥"+d.data[0].totalCorpus)    /*渲染分期金额*/
                    $("#totalFee").html("￥"+d.data[0].totalFee)    /*渲染总服务费金额*/
                    $("#Summoney").html("￥"+(d.data[0].totalFee+d.data[0].totalCorpus))    /*总还款金额*/
                    if (d.data != null && d.data.length > 0) {
                        for (var i = 0; i < d.data.length; i++) {
                            data = {
                                curentTime: d.data[i].currentTimes,
                                totalTime: d.data[i].totalTimes,
                                totalMoney: (d.data[i].currentCorpus+d.data[i].currentFee),
                                time: d.data[i].repaymentDate,
                                money: d.data[i].currentCorpus,
                                serviceMoney: d.data[i].currentFee
                            };
                            data.path = '${basePath!}';
                            var html = template.load({
                                host: '${basePath!}/',
                                name: "a_moeny_list",
                                data: data
                            });
                            $html = $(html);
                            $('#plan_list').append($html);
                        }
                    }
                }
            });
        }

        <#--//支付密码框 校验密码 完成分期-->
        <#--$(function(){-->
            <#--$("#payPwd").payPwd({-->
                <#--max:6,-->
                <#--type:"password",-->
                <#--callback:function(arr) {-->
                    <#--// var tt=123456 ;   //服务器密码-->
                    <#--$.ajax({-->
                        <#--type: "POST",-->
                        <#--url: "${basePath!}/user/spilt/eckPassWords",-->
                        <#--data: {passWord:arr},-->
                        <#--dataType: "json",-->
                        <#--success: function (d) {-->
                            <#--if(d.data){-->
                                <#--$.ajax({-->
                                    <#--type: "POST",-->
                                    <#--url: "${basePath!}/user/spilt/updateByStages",-->
                                    <#--data: {splitMonth:splitMonth},-->
                                    <#--dataType: "json",-->
                                    <#--success: function (d) {-->
                                        <#--console.log(d)-->
                                        <#--if(d.code==0){-->
                                               <#--window.location.href = "${basePath!}/consumer/bill/jumpMineWaitPay";-->
                                        <#--}else{-->
                                            <#--//分期失败-->
                                            <#--api.tools.toast(d.msg);-->
                                            <#--return;-->
                                        <#--}-->
                                    <#--}-->
                                <#--});-->
                            <#--}else{-->
                                    <#--//密码错误-->
                                <#--api.tools.toast(d.msg);-->
                            <#--}-->
                        <#--}-->
                    <#--});-->
                <#--}-->
            <#--})-->
        <#--});-->
        $(function(){
            //出现浮动层
            $(".repayment_btn").click(function(){
                $(".ftc_wzsf").show();
            });
            //关闭浮动
            $(".close").click(function(){
                $(".ftc_wzsf").hide();
                $(".mm_box li").removeClass("mmdd");
                $(".mm_box li").attr("data","");
                i = 0;
            });
            var i = 0;
            $(".nub_ggg li .zf_num").click(function(){
                if(i<6){
                    $(".mm_box li").eq(i).addClass("mmdd");
                    $(".mm_box li").eq(i).attr("data",$(this).text());
                    i++
                    if (i==6) {
                        var data = "";
                        $(".mm_box li").each(function(){
                            data += $(this).attr("data");
                        });
                        $.ajax({
                            type: "POST",
                            url: "${basePath!}/user/spilt/eckPassWords",
                            data: {passWord:data},
                            dataType: "json",
                            success: function (d) {
                                if(d.data){
                                    $.ajax({
                                        type: "POST",
                                        url: "${basePath!}/user/spilt/updateByStages",
                                        data: {splitMonth:splitMonth},
                                        dataType: "json",
                                        success: function (d) {
                                            console.log(d)
                                            if(d.code==0){
                                                window.location.href = "${basePath!}/consumer/bill/jumpMineWaitPay";
                                            }else{
                                                //分期失败
                                                api.tools.toast(d.msg);
                                                return;
                                            }
                                        }
                                    });
                                }else{
                                    //密码错误
                                    api.tools.toast(d.msg);
                                }
                            }
                        });

                    };
                }
            });
            $(".nub_ggg li .zf_del").click(function(){
                if(i>0){
                    i--
                    $(".mm_box li").eq(i).removeClass("mmdd");
                    $(".mm_box li").eq(i).attr("data","");
                }
            });

            $(".nub_ggg li .zf_empty").click(function(){
                $(".mm_box li").removeClass("mmdd");
                $(".mm_box li").attr("data","");
                i = 0;
            });
        });




        //期数选择
        $(".by_stages_num li").click(function () {
            $(".by_stages_num li").removeClass("active");
            $(this).addClass("active");
        });

        //点击按钮
        $(".repayment_btn").click(function () {
            $(".pwd-control").css("display","block");
            $(".repayment_btn").css("display","none")
        })
        $(".goClick").click(function () {
            $(".periodization_model").css("display","block");

        });
        $("#closed").click(function () {
            $(".periodization_model").css("display","none");
        });
        $("#qxfq").click(function () {
            window.location.reload();
        })
    });
</script>
</body>
</html>