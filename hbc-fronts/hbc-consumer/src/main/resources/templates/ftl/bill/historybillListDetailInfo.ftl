<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>账单详情</title>
    <style>

    </style>
</head>
<body>
 <#if isSplit=="1" >
        <div class="amortizationRecord">
            <div class="amortizationRecord_title">
                分期还款记录
            </div>
            <div class="amountRepaid">
                <div class="container-fluid">
                    <div class="row" id="plan_List">
                        <div class="col-xs-6">
                            <p>已还款金额</p>
                            <h6 id="alreadyPayMoney"></h6>
                        </div>
                        <div class="col-xs-6">
                            查看分期明细
                            <img src="${basePath!}/img/imgs/icon_enter.png" alt="" id="plan_id">
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </#if>

 <#if isSplit=="0" >
        <div class="amortizationRecord" id="unplan_div">
            <div class="amortizationRecord_title" id="activeLaber">
                未分期还款记录
            </div>
            <div class="amountRepaid">
                <div class="container-fluid" id="unplan_id">
                    <div class="row">
                        <div class="col-xs-6">
                            <p>还款总金额</p>
                            <h6 id="upPlanPayDate"></h6>
                        </div>
                        <div class="col-xs-6">
                            <h6 id="upPlanPayMoney"></h6>
                            <img src="${basePath!}/img/imgs/icon_enter.png" alt="" id="unPlan_button">
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </#if>

<div class="amortizationRecord" id="advanceTitle">
    <div class="amortizationRecord_title">
        提前还款记录
    </div>
   <div id="tq-adiv">
       <#--<div class="amountRepaid amountRepaid_money">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-xs-6">
                       <p>提前还款金额</p>
                       <h6>2018-05-15 08:20:20</h6>
                   </div>
                   <div class="col-xs-6">
                       <p>￥20.00</p>
                   </div>
               </div>
           </div>
       </div>-->
   </div>
</div>


<div class="byStages_time">
    <div class="amortizationRecord_title">交易明细</div>
    <ul id="trade_list">
       <#-- <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        <p>相约回家吃饭</p>
                        <h6>2018-07-06</h6>
                    </div>
                    <div class="col-xs-6">
                        231.00
                        <img src="img/right_enter.png" alt="">
                    </div>
                </div>
            </div>
        </li>-->
    </ul>
</div>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    loadTradingDetail();

    function loadTradingDetail(){
        api.tools.ajax({
            url:"${basePath!}/consumer/bill/findHistoryBillDetailInfo",
            data: {
                id:"${id!}",
                isSplit:"${isSplit!}"
            }
        },function(d){
            if(d.data.advancePayList!=null && d.data.advancePayList.length>0) {
                for (var i = 0; i < d.data.advancePayList.length; i++) {
                    var obj = d.data.advancePayList[i];

                    data = {
                        advanceMoney: obj.amount,
                        time: obj.payDate
                    };

                    var html = template.load({
                        host: '${basePath!}/',
                        name: "advnce_payback_list",
                        data: data
                    });
                    $html = $(html);
                    $('#tq-adiv').append($html);
                }
            }else{
               /* $("#tq-adiv").append('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');*/
                $("#advanceTitle").css("display","none");
            }

            if( d.data.tbRepaymentDetail.id!=null){
              $("#alreadyPayMoney").html("￥"+d.data.tbRepaymentDetail.repayCorpus);

                $("#plan_List").click(function () {
                    /*跳转页面(逾期未还/待还)*/
                    window.location = "${basePath!}/consumer/bill/jumpDivideDetail?id="+d.data.tbRepaymentDetail.id;
                });

            }

            if(d.data.unPlantbRepaymentDetail!=null){
                if(d.data.unPlantbRepaymentDetail.currentCorpus==null){
                    $("#unplan_div").css("display","none");
                }
              $("#upPlanPayMoney").html("￥"+d.data.unPlantbRepaymentDetail.currentCorpus);
              $("#upPlanPayDate").html(d.data.unPlantbRepaymentDetail.processDate);

              if(d.data.unPlantbRepaymentDetail.status == '0'){
                  $("#activeLaber").html("未分期逾期还款记录");
                  $("#unplan_id").click(function () {
                      window.location = "${basePath!}/consumer/bill/jumpPlanDetailInfo?planId=" +d.data.unPlantbRepaymentDetail.id+ "&waitpay=" + d.data.unPlantbRepaymentDetail.status+ "&splitType="+${isSplit!};
                  });
              }else{
                  $("#unPlan_button").css("display","none");
              }

            }else{
                $("#unPlan_button").css("display","none");
            }

            if(d.data.tradeList!=null && d.data.tradeList.length>0) {
                for (var i = 0; i < d.data.tradeList.length; i++) {

                    var obj = d.data.tradeList[i];

                    data = {
                        name: "交易记录",
                        money: obj.actualAmount,
                        time: obj.tradingDate
                    };

                    var html = template.load({
                        host: '${basePath!}/',
                        name: "bill_trading_detail",
                        data: data
                    });

                    $html = $(html);
                    $html.find(".container-fluid").attr("data1", d.data.tradeList[i].id);
                    $html.find(".container-fluid").click(function () {
                        /*跳转页面(逾期未还/待还)*/
                        window.location = "${basePath!}/user/order/findById?id=" + $(this).attr("data1");
                    });
                    $('#trade_list').append($html);
                }
            }else{
                $("#trade_list").append('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');
            }

        });
    }

</script>


</body>
</html>