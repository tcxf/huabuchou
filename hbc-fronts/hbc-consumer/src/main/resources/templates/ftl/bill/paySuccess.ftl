<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>还款成功</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 6rem;
        }
        .s_content{
            text-align: center;

        }
        .s_content>p{
            color: #999;
            margin-top: 10px;
            font-weight: 600;
            font-size: 12px;
        }
        .s_content h6{
            color: #999;
        }
        .tj{
            width: 94%;
            margin-left:3%;
            text-align: center;
            background:#f49110;
            margin-top: 10%;
            border-radius:30px;
            height: 40px;
            line-height: 40px;

        }
        .tj>a{
           text-decoration: none;
            color:#ffffff;
            font-size: 12px;
        }

    </style>
</head>
<body>
<div class="top">
    <img src="${basePath!}/img/img/pay_ok.png" alt=""/>
</div>
<div class="s_content">
    <p>还款成功！</p>
</div>
<div class="tj">
    <a href="${basePath!}/consumer/bill/jumpMyCredit">完成</a>
</div>
</body>
</html>