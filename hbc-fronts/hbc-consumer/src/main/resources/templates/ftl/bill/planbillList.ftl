<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>分期账单</title>
</head>
<body>
<div class="byStages">
    <p>剩余分期总额</p>
    <h2 id="totalMoney"></h2>
    <h6 id="currentMoney">应还总额 : </h6>
</div>
<div class="byStages_time_list" id="bills_list">


    <#--<div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="#cc2" data-toggle="collapse">2017</a>
            </h4>
        </div>
        <div class="panel-collapse collapse" id="cc2">
            <div class="panel-body">
                <ul id="">
                    <li>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6">
                                    <p>7月份账单</p>
                                    <h6>2345.67</h6>
                                </div>
                                <div class="col-xs-6 ing">
                                    进行中
                                    <img src="img/right_enter.png" alt="">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6">
                                    <p>5月份账单</p>
                                    <h6>2345.67</h6>
                                </div>
                                <div class="col-xs-6 over">
                                    已完成
                                    <img src="img/right_enter.png" alt="">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6">
                                    <p>4月份账单</p>
                                    <h6>2345.67</h6>
                                </div>
                                <div class="col-xs-6 over">
                                    已完成
                                    <img src="img/right_enter.png" alt="">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>-->

</div>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    loadTradingDetail();

    function loadTradingDetail(){
        api.tools.ajax({
            url:"${basePath!}/consumer/bill/findPlanYears",
        },function(d){
            if(d.data.mmp == null){
                $('.byStages').parent().html('<div class="content" align="center">'+
                        '<img src="${basePath!}/img/img/icon_zanwujilu.png" alt=""/>'+
                        '<h5>暂无记录...</h5>'+
                        '</div>');
                return;
            }
            $("#totalMoney").html(d.data.mmp == null ? 0:d.data.mmp.totalMoney);
            $("#currentMoney").html("应还总额 :"+(d.data.mmp.currentMoney == null ? 0:d.data.mmp.currentMoney) );

            if(d.data.list!=null && d.data.list.length > 0 ) {
                for (var i = 0; i < d.data.list.length; i++) {
                    data = {
                        dateYear: d.data.list[i].dateYear
                    };
                    data.path = '${basePath!}';
                    var html = template.load({
                        host: '${basePath!}/',
                        name: "parent_plan_bill_list",
                        data: data
                    });
                    $html = $(html);
                    $('#bills_list').append($html);

                     param=d.data.list[i].dateYear
                    // document.getElementById("Click2017").onclick =function () {

                }
            }else{
                $("#bills_list").append('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');
            }
        });
    }

    $(document).on('click',function(e){
        //console.log($(e.target).attr("id").substring(5));

        api.tools.ajax({
            url: "${basePath!}/consumer/bill/findPlanBillByYear",
            data: {
                dateYear:$(e.target).attr("id").substring(5)
            }
        }, function (d) {
            console.log(d);
            for (var i = 0; i < d.data.length; i++) {
                data = {
                    dateMonth: d.data[i].dateMonth+"月份账单",
                    isfinish: d.data[i].isfinish==1 ? "完成":"进行中",
                    type: d.data[i].isfinish==1 ? "over":"ing",
                    money: d.data[i].total_amount
                };
                data.path = '${basePath!}';
                var html = template.load({
                    host: '${basePath!}/',
                    name: "son_plan_bill_list",
                    data: data
                });
                $html = $(html);
                $html.find(".container-fluid").attr("data1", d.data[i].id);
                $html.find(".container-fluid").click(function () {
                    window.location = "${basePath!}/consumer/bill/jumpDivideDetail?id=" + $(this).attr("data1");
                });
                $("#" +$(e.target).attr("id").substring(5)).html("");
                $("#" +$(e.target).attr("id").substring(5)).append($html);

            }
        });
    })

</script>
</body>
</html>