<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>待还账单</title>
</head>
<body>
<div class="Bills">
    <p>待还款金额</p>
    <h2 id="totalMoney"></h2>
    <h6 id="lastPayDate"></h6>
</div>

<!--账单-->
<div class="bills_list" id="bills_list_tow">
    <ul id="bills_list">
    <#--<li>
            <div class="container-fluid">
                <div class="row">
                        <div class="col-xs-1">
                            <input type="checkbox" name='checkbox'>
                        </div>
                    <div class="col-xs-8">
                        <p>消费分期(每月还款+服务费)</p>
                        <h6>2018-06-12</h6>
                    </div>
                    <div class="col-xs-4">
                        1000.00
                    </div>
                </div>
            </div>
        </li>-->

    </ul>
</div>

<!--交易记录表-->
<div class="bills_list" id="bills_list_one">
    <ul id="trade_list">
    <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-8">
                        <p>芙蓉华天酒店消费</p>
                        <h6>2018-06-12</h6>
                    </div>
                    <div class="col-xs-4">
                        1000.00
                    </div>
                </div>
            </div>
        </li>-->
    </ul>

    <div class="nub_text">
        合计比数: <span id="totalSize"></span>
    </div>
</div>


<div class="my_btn">
    <ul>
        <li class="repayment">
            <a href="#">立即还款</a>
        </li>
        <li class="by_stages">
            <a href="#">我要分期</a>
        </li>
    </ul>
</div>

<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>

    $(document).ready(function() {

        loadTradingDetail();
        //加载交易记录
        function loadTradingDetail(){
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/findMineWaitPayInfo",
                /*data:{
                    page:page,
                    limit:rows
                }*/
            },function(d){
                $("#totalMoney").html(d.data.totalMoneyMap.totalMoney)    /*渲染待还金额*/
                $("#totalSize").html(d.data.totalSize)     /*渲染总数量*/
                $("#lastPayDate").html("最后还款日 "+d.data.lastPayDate)     /*渲染最后还款日*/

                if(d.data.totalMoneyMap.totalMoney==0 && d.data.totalSize==0){
                    $(".nub_text").css("display","none")
                    $('#bills_list').parent().html('<div class="content" align="center">'+
                            '<h5>赞！&nbsp本期全部还清了</h5>'+
                            '<img src="${basePath!}/img/img/xiaolian.png" alt=""/>'+
                            '</div>');
                }

                if(d.data.totalMoneyMap.totalMoney!=0 && d.data.totalSize==0){
                    $(".nub_text").css("display","none")
                    $('#bills_list').parent().html('<div class="content" align="center">'+
                            '<h5>赞！&nbsp本期全部还清了</h5>'+
                            '<img src="${basePath!}/img/img/xiaolian.png" alt=""/>'+
                            '</div>');
                }



                /*加载账单列表 逾期或者是待还*/
                if(d.data.payList!=null && d.data.payList.length > 0 ) {
                    for (var i = 0; i < d.data.payList.length; i++) {
                        var obj = d.data.payList[i];
                        if (obj.waitpay == 0 && obj.splitType == 1) { /1:分期 *1:待还 0:未还*/
                            data = {
                                name: "消费分期(每月还款+服务费+滞纳金)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate,
                                planId:obj.planId,
                                repaySn:obj.serialNo
                            };
                        }else if(obj.waitpay == 1 && obj.splitType == 1){
                            data = {
                                name: "消费分期(每月还款+服务费)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate,
                                planId:obj.planId,
                                repaySn:obj.serialNo
                            };
                        }else if(obj.waitpay == 0 && obj.splitType == 0){
                            data = {
                                name: "逾期总计(本金+滞纳金)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate,
                                planId:obj.planId,
                                repaySn:obj.serialNo
                            };
                        }
                        var html = template.load({
                            host: '${basePath!}/',
                            name: "bill_list",
                            data: data
                        });
                        $html = $(html);
                        $html.find(".col-xs-7").attr("data1", d.data.payList[i].planId);
                        $html.find(".col-xs-7").attr("data2", d.data.payList[i].waitpay);
                        $html.find(".col-xs-7").attr("data3", d.data.payList[i].splitType);
                        $html.find(".col-xs-7").click(function () {
                            /*跳转页面(逾期未还/待还)*/
                            window.location = "${basePath!}/consumer/bill/jumpPlanDetailInfo?planId=" + $(this).attr("data1") + "&waitpay=" + $(this).attr("data2")+ "&splitType=" + $(this).attr("data3");
                        });
                        $('#bills_list').append($html);
                        $("input[name='checkbox']").attr("checked","true");
                    }
                }
                /*加载本月已出账单交易记录 或者逾期已出账单交易记录*/
                var rdid='';
                if(d.data.totalTradeList != null && d.data.totalTradeList.length>0){
                    console.log(d.data.totalTradeList);
                    for (var i = 0; i < d.data.totalTradeList.length; i++) {
                        var obj = d.data.totalTradeList[i];
                        rdid=obj.rdid;
                        data = {
                            name: "交易记录",
                            money: obj.actualAmount,
                            time: obj.tradingDate
                        };

                        var html = template.load({
                            host: '${basePath!}/',
                            name: "bill_trading_detail",
                            data: data
                        });
                        $html = $(html);
                        $html.find(".container-fluid").attr("data1", d.data.totalTradeList[i].id);
                        $html.find(".container-fluid").click(function () {
                            /*跳转页面(逾期未还/待还)*/
                            window.location = "${basePath!}/user/order/findById?id=" + $(this).attr("data1");
                        });
                        $('#trade_list').append($html);
                    }
                }


                /*控制还款按钮 以及分期按钮*/
                if(d.data.payButton){
                    $(".repayment").css("display","block")
                }
                if(d.data.planButton){
                    $(".by_stages").css("display","block")
                }

                $(".by_stages").click(function () {
                    window.location.href="${basePath!}/user/spilt/qu";
                })

                $(".repayment").click(function () {
                    var index = layer.load();
                    var arr = [];
                    $('ul li input[type=checkbox]') .each(function(){
                        if($(this).prop('checked')){
                            var obj = {};
                            obj.planId = $(this).attr('id');    //分期id
                            obj.amount = $(this).attr('data_money') ; //账单金额
                            obj.billDate = $(this).attr('billDate') ;//账单年月 需处理
                            obj.repaySn = $(this).attr('serialNo') ;//计划表中的流水号
                            obj.type = "1" ; //账单还款
                            arr.push(obj);
                        }
                    });
                    console.log(JSON.stringify(arr));
                    if(rdid =='' && arr.length==0){
                        api.tools.toast("请至少选中一项!");
                        return;
                    }
                  window.location = "${basePath!}/consumer/bill/jumpPaybackMoney?payList="+(arr.length>0 ? JSON.stringify(arr) : "0" )+ "&rdid=" + (rdid == ''? "0" :rdid);

                })

            });
        }

    });


</script>
</body>
</html>