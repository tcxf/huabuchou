<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
    <title>
        <#if waitpay =="0">
        逾期详情
        </#if>
        <#if waitpay =="1">
        分期详情
        </#if>
    </title>
    <style>
    body,
            html {
        background: #eee;
    }

    .layui-m-layer {
        z-index: 0 !important;
    }
    .hk-money {

        background: #fff;
        color: #333;
        text-align:center;
        height: 120px;
        padding-top: 31px;
        line-height: 21px;
    }
    .hk-money div {
        margin-top: 10px;
        font-size: 14px;
        font-weight: 400;
        color: #f49110;
    }
    .foot-fq {
        background: #ffffff;
        margin-right: 1px;
        height: 55px;
        line-height: 55px;
        color: #8ed066;
        font-size: 19px;
    }

    </style>
    </head>
    <body>
    <div class="c-content" style="margin-top: 0px;">
            <div class="webkit-box">
            <div class="wkt-flex hk-hm" id="repaymentDate">
            应还
            </div>
            <div class="wkt-flex text-right hk-hm" id="currentTime_totalTime">
            期
            </div>
            </div>
            </div>

            <div class=" hk-money" style="-ms-text-align-last">
            <span style="font-size: 29px" id="totalMoney"> </span>
                     <#if splitType=="1">
                        <div id="totalAmount">
                                    分期总金额
                        </div>
                     </#if>
            </div>

            <div class="c-content" style="margin-top: 5px;">

            <div class="webkit-box">
            <div class="wkt-flex">
                            <#if waitpay=="0">逾期本金</#if>
                             <#if waitpay=="1">本金</#if>
    </div>
    <div class="wkt-flex text-right" id="currentCorpus"> </div>
            </div>

        <#if splitType=="1">
            <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>

                <div class="webkit-box">
                <div class="wkt-flex">
                分期时间
                </div>
                <div class="wkt-flex text-right" id="repaymentDate_fenqi"> </div>
            </div>
        </#if>

         <#if waitpay=="0">
                <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
                 <div class="webkit-box">
                 <div class="wkt-flex">
                 逾期滞纳金
                 </div>
                 <div class="wkt-flex text-right" id="lateFee"> </div>
                </div>
         </#if>

          <#if waitpay=="0"&& splitType=="0" >
                    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
                  <div class="webkit-box">
                  <div class="wkt-flex">
                  逾期天数
                  </div>
                  <div class="wkt-flex text-right" id="lateDay"> </div>
                    </div>
          </#if>


         <#if splitType=="1">
            <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
                 <div class="webkit-box redirect" id="plan_id" >
                 <div class="wkt-flex">
                 分期计划
                 </div>
                 <div class="wkt-flex">
                 <input type="tel" readonly UNSELECTABLE='true' onfocus="this.blur()" id="totalTime"  class="form-control reg-input"
                         style=" font-size: 14px;  background:url(${basePath!}/img/images/icon_enter.png) no-repeat right; background-size:3%;
                               width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
         </div>
         </div>


         <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
                 <div class="webkit-box">
                 <div class="wkt-flex">
                 每期利息
                 </div>
                 <div class="wkt-flex text-right" id="currentFee"></div>
            </div>
         </#if>

            </body>

            <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"> </script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"> </script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"> </script>
    <script type="text/javascript" src="${basePath!}/js/bootstrap.js"> </script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
    <script>
        $(document).ready(function() {
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/findPlanDetailInfo",
                data:{
                    planId:"${planId!}",
                    waitpay:"${waitpay!}"
                }
            },function(d){
                $("#repaymentDate").html(d.data.repaymentDate+"应还")
                $("#currentTime_totalTime").html(d.data.currentTime+"/"+d.data.totalTime+"期")
                $("#totalTime").val(d.data.totalTime+"期")
                $("#totalMoney").html("￥"+d.data.totalMoney)
                $("#totalAmount").html("分期总金额："+d.data.totalAmount)
                $("#currentCorpus").html("￥"+d.data.currentCorpus)
                $("#repaymentDate_fenqi").html(d.data.splitDate)
                $("#lateFee").html("￥"+d.data.lateFee)
                $("#currentFee").html("￥"+d.data.currentFee)
                $("#lateDay").html(d.data.lateDay==null ? 0:d.data.lateDay+"天")
                console.log(${splitType!})
                if(${splitType!}=="1"){
                    $("#plan_id").attr("url","${basePath!}/consumer/bill/jumpDivideDetail?id="+d.data.id);
                }
            });
        });
    </script>
</html>