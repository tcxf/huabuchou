<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <link rel="stylesheet" href="${basePath!}/css/zhifu.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>立即还款</title>
    <style>
        .addBankCard{
            margin-top: 20px;
            text-align: center;
        }
        .addBankCard>a{
            text-after-overflow: none;
            color: #f49110;
        }
    </style>
</head>
<body>
<div class="Bills">
    <p>待还账单金额</p>
    <h3 id="amount"></h3>
</div>
<div class="top_one">
    <li>
        <b>网银快捷</b>
    </li>
    <ul id="bankList" class="banklistcss">
    <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/img/ico_yinlianzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>长沙银行</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="wangyin" name="zhifu" value="4">
                    </div>
                </div>
            </div>
        </li>-->
    </ul>
</div>
<div class="addBankCard">
    <a href="#">添加银行卡</a>
</div>

<div class="submit_btn">
    <a href="#">下一步</a>
</div>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>

    $(document).ready(function() {

        $("#amount").html(${amount!});

        api.tools.ajax({
            url: "${basePath!}/bank/findQuickBankCard",
        }, function (d) {
            console.log(d);
            for (var i = 0; i < d.data.length; i++) {
                data = {
                    bankName: d.data[i].bankName,
                    contractId: d.data[i].contractId
                };
                data.path = '${basePath!}';
                var html = template.load({
                    host: '${basePath!}/',
                    name: "bankList",
                    data: data
                });
                $html = $(html);
                // $html.find(".container-fluid").attr("data1", d.data[i].id);
                //  $html.find(".container-fluid").click(function () {
                //      window.location = "${basePath!}/consumer/bill/jumpDivideDetail?id=" + $(this).attr("data1");
                //  });
                $("#bankList").append($html);
                $(".banklistcss input:first").attr("checked",true);
            }
        });


       //支付方式 1:快捷支付 2;微信支付
        $(".submit_btn").click(function() {
            var contractId= $("input[name='zhifu']:checked").val();
            if(!contractId){
                layer.msg("请选择您的银行卡！");
                return;
            }
            var index = layer.load();
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/PaybackMoney",
                data:{
                    payList:JSON.stringify(${payList!}),
                    rdid:"${rdid!}",
                    payWay:"1",
                    contractId:contractId
                }
            },function (data) {
                layer.close(index);
                if(data.code==0){
                    window.location ="${basePath!}/consumer/bill/inputNormalPayMsgCode?agreeid="+data.data.agreeid+"&orderid="+data.data.orderid+"&thpinfo="+data.data.thpinfo+"&rdId="+data.data.rdId+"&serialNo="+data.data.serialNo;
                }else{
                    layer.msg(data.msg)
                }
            });
        });

        $(".addBankCard").click(function() {
            window.location = " ${basePath!}/consumer/bill/goToYcBindCard?uid="+"${uid!}"+"&rdid="+"${rdid!}"+"&payList="+JSON.stringify(${payList!});
        });
    });


</script>
</body>
</html>