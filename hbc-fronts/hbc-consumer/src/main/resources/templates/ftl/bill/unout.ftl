<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>未出账单</title>
</head>
<body>
<div class="byStages">
    <p>未出账单金额</p>
    <h2 id="unoutMoney"></h2>
</div>
<div class="noBill">
    <div class="n_text">
        未出账单
    </div>
    <div class="noBill_list">
        <ul id="row-list">
        <#--<li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-8">
                            <p>芙蓉华天酒店消费</p>
                            <h6>2018-06-12</h6>
                        </div>
                        <div class="col-xs-4">
                            1000.00
                        </div>
                    </div>
                </div>
            </li>-->
        </ul>
    </div>
    <div class="total_size" style="display: none">
        <p>合计笔数: <span id="totalSize">2</span></p>
    </div>
</div>

<div class="noBill">
    <div class="n_text">
        未出帐还款账单
    </div>
    <div class="noBill_list">
        <ul id="tq-list">
        <#--<li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-8">
                            <p>芙蓉华天酒店消费</p>
                            <h6>2018-06-12</h6>
                        </div>
                        <div class="col-xs-4">
                            1000.00
                        </div>
                    </div>
                </div>
            </li>-->
        </ul>
    </div>
    <div class="total_num" style="display: none">
        <p>提前还款金额: <span id="advanceMoney"></span></p>
    </div>
</div>
<div class="repayment_btn">
    <a href="#">还款</a>
</div>
</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js">
</script>
<script type="text/javascript" src="${basePath!}/js/jquery.js">
</script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js">
</script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js">
</script>
<script type="text/javascript" src="${basePath!}/js/info.js">
</script>
<script type="text/javascript" src="${basePath!}/template/template.js">
</script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {

        //提前还款账单
        function loadTradingDetail() {
            api.tools.ajax({
                url: "${basePath!}/consumer/bill/findUnOutBill",
            }, function (d) {
                $("#unoutMoney").html(d.data.totalMoney);
                $("#advanceMoney").html(d.data.advanceMoney);

                if (d.data.unOutPageInfo!= null && d.data.unOutPageInfo.length == 0 && d.data.advancePageInfo != null && d.data.advancePageInfo.length ==0) {
                    $('.byStages').parent().html('<div class="content" align="center">'+
                            '<img src="${basePath!}/img/img/icon_zanwujilu.png" alt=""/>'+
                            '<h5>暂无记录...</h5>'+
                            '</div>');
                }

                if (d.data.unOutPageInfo!= null && d.data.unOutPageInfo.length == 0) {
                    $(".repayment_btn").css("display","none")
                }

                if (d.data.unOutPageInfo!= null && d.data.unOutPageInfo.length > 0) {
                    $(".total_size").css("display","block");
                    $("#totalSize").html(d.data.unOutPageInfo.length);

                    for (var i = 0; i < d.data.unOutPageInfo.length; i++) {
                        obj = d.data.unOutPageInfo[i];
                        data = {
                            name: "交易记录",
                            money: obj.actualAmount,
                            time: obj.tradingDate
                        };
                        var html = template.load({
                            host: "${basePath!}/",
                            name: "bill_trading_detail_tq",
                            data: data
                        });
                        var $html = $(html);
                        $html.find(".container-fluid").attr("data1", d.data.unOutPageInfo[i].id);
                        $html.find(".container-fluid").click(function () {
                            window.location = "${basePath!}/user/order/findById?id=" + $(this).attr("data1");
                        });
                        $("#row-list").append($html);
                    }
                }else{
                    $("#row-list").append('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');
                }

                if (d.data.advancePageInfo != null && d.data.advancePageInfo.length > 0) {
                    $(".total_num").css("display","block");
                    for (var i = 0; i < d.data.advancePageInfo.length; i++) {
                        obj = d.data.advancePageInfo[i];
                        console.log(obj)
                        data = {
                            name: "交易记录",
                            money: obj.actualAmount,
                            time: obj.tradingDate
                        };
                        var html = template.load({
                            host: "${basePath!}/",
                            name: "bill_trading_detail",
                            data: data
                        });
                        var $html = $(html);
                        $html.find(".container-fluid").attr("data1", d.data.advancePageInfo[i].id);
                        $html.find(".container-fluid").click(function () {
                            window.location = "${basePath!}/user/order/findById?id=" + $(this).attr("data1");
                        });
                        $("#tq-list").append($html);
                    }
                }else{
                    $("#tq-list").append('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');
                }

                $(".repayment_btn").click(function(){
                    window.location = "${basePath!}/consumer/bill/jumpAdvancePaybackMoney";
                   //window.location = "${basePath!}/consumer/bill/jumpAdvancePaybackMoney?tradeList="+JSON.stringify(d.data.unOutPageInfo);
                });
            });
        }
        loadTradingDetail();
    });
</script>

</html>