<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <#include "common/common.ftl">
		<title>我的银行卡</title>
		<style>
			body,html{
				background: #fff;
			}
			.bank_card{
                margin-top: 10px;
                width: 94%;
                height: 40px;
                line-height: 40px;
                border: 1px dashed #f49110;
                text-align: center;
                margin-left: 3%;
                border-radius: 30px;
                color: #f49110;
                background: transparent;
                padding: 0;
			}
			.container{
				margin-top: 5%;
				background: #3093f9;
				border-radius: 5px;
				background-size:95% ;
				color: #ffffff;
				width: 95%;
				line-height: 38px ;
			}
			.modal-header{
				text-align:center;
			}
			.modal-footer{
			text-align:center;
			letter-spacing:60px
			}
		</style>

	</head>

	<body>

    <button class="bank_card">+添加银行卡</button>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" >
						<h4 class="modal-title" id="myModalLabel">
							您确认要解绑这张卡吗？
						</h4>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" 
								data-dismiss="modal">取消
						</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">
							确认
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div id="bank-list"></div>

		<!-- 清除浮动 -->
		<div style="clear:both;"></div>

    <#include "./agreement/mune_btn.ftl">
        <script>
            $(document).ready(function() {
                $.ajax({
                    url:"${basePath!}/js/bank/bank_info-yb.json",
                    type:'GET', //GET
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    async:true,
                    success:function(data){
                        binfo = data;
                        $(".card").each(function(i,e){
                            for(var i = 0 ; i < data.length ; i++){
                                if(data[i].sn == $(e).attr("data")){
                                    $(e).html(data[i].name);
                                    if(data[i].color != null)
                                        $(e).parent().parent().css("background",data[i].color);
                                    break;
                                }
                            }
                        });

                    }
                });

                card();
                function card(){
                    $('#bank-list').empty();
                    api.tools.ajax({
                        url:'${basePath!}/bank/findbank',
                    },function(d){
                        if(d.data != null && d.data.length > 0){
                            for(var i = 0 ; i < d.data.length ; i++){
                                data = d.data[i];
                                html = template.load({
                                    host:'${basePath!}/',
                                    name:'bank_list',
                                    data:data
                                });

                                $html = $(html);
                                $("#bank-list").attr('data',data.id);
                                $html.find(".card").click(function(){
                                    $(function () {
                                        $('#myModal').modal({
                                            keyboard: true
                                        })});
                                    var bindid = data.id;
                                    $(".btn-primary").click(function(){
                                        api.tools.ajax({
                                            url:'${basePath!}/bank/dbank',
                                            data:{
                                                bindid:bindid
                                            }
                                        },function(d){
                                            api.tools.toast(d.msg);
                                            if(d.code!= 0){
                                                card();
                                            }
                                        });

                                    });
                                });
                                $('#redList').append($html);
                                bankInfo = api.tools.bankInfo.get(data.bankSn);

                                logo = '${basePath!}/img/img/ionic-yhk.png';
                                if(bankInfo != null){
                                    if(bankInfo.color != null) $html.css('background-color',bankInfo.color);
                                    if(bankInfo.logo != null && bankInfo.logo != "") logo = "${basePath!}"+bankInfo.logo;
                                }

                                $bankInfo = $('<img src="'+logo+'" alt="" style="width:30px;height:30px;margin-right:5px;"/>');
                                $html.find("#bank-info").append($bankInfo);
                                $html.find("#bank-info").append(data.bankName);
                                var c=data.bankCard;
                                var c1= c.substr(c.length-4);
                                $html.find("#cbank").append(c1);
                                $("#bank-list").append($html);
                            }
                        }else{
                            $html = '<div style="margin-top: 0px;width:100%;text-align:center;margin:5px auto;background:#fff;padding:10px;">暂无卡片</div>'
                            $("#bank-list").append($html);
                        }
                    });
                }

                $(".bank_card").click(function(){
                    window.location = "${basePath!}/bank/savebank";
                });
            });
        </script>
	</body>
</html>