<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zybl.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
	<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />

    <title>店铺收藏</title>
    <style>
        #top-yh h4{
            margin-top: 28px;
        }
        #top-yh p{
            padding-top: 9px;
            font-size: 12px;
        }
        .active>a{
            color: #f49110 !important;
        }
        .m_tab a{
            color: #333333;
        }
        .c_yhq{
            margin-top: 5px;
            width: 100%;
            background: #fff;
        }
        .c_yhq .col-xs-4{
            margin-top: 22px;
        }
        .c_thing .thing_left img{
            width:55px;
            height: 55px;

        }
        .c_thing .thing_left p{
            margin-bottom: 12%;
        }
        .c_thing .thing_right{
            padding-top: 0;
        }
        .c_toutu img{ border-radius:50px; width:10%;}
        .p-dingdan{
            margin-left: -32%;
        }
        .p-dingdan1 h4{
            margin-top: 0%;
            font-size: 1.1rem;
        }
        .p-dingdan1 p{
            margin-top: 6%;
            font-size: 0.8rem;
            line-height: 5px;
        }
    </style>
</head>
<body>
<!-- ————————————       商品收藏—————————————— -->
<div class="m_content tab-content ">
   <!--  <div class="c_thing tab-pane active" id="all">
        <ul id="sp">

      	</ul>
    </div> -->
    <!-- ————————————       商店收藏—————————————— -->
    <div class="c_yhq tab-pane active" id="dfk" style="margin-top: -7px">
        <ul id="sd">

        </ul>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.cookie.js"></script>
    <#include "./agreement/mune_btn.ftl">
	<script>
		$(document).ready(function() {
			loadRedPacket();

			function loadRedPacket(){
				$('#sd').empty();
				api.tools.ajax({
					url:'${basePath!}/consumer/center/findMineCollects',
				},function(d){
					if(d.data!= null && d.data.length > 0){
						for(var i = 0 ; i < d.data.length ; i++){
							var data =d.data[i];
                            data.path = '${basePath!}';
                            data.firstWord="满"
                            data.secondWord="减";

							var html = template.load({
								host : '${basePath!}/',
								name : "collect_sdlist",
								data : data
							});

							$html = $(html);
							$html.find(".start").attr("data1",data.id);
							$html.find(".start").click(function(){
								$this = $(this);
// 								$this.parent().parent().parent().parent().remove();
								api.tools.ajax({
									url:'${basePath!}/consumer/center/deleteCollect',
									data:{
										id: $(this).attr('data1')
									},
								},function(d){
                                       api.tools.toast('已取消收藏');
										$this.parent().parent().parent().parent().remove();
										if($("#sd").find("li").length == 0){
											$('#sd').parent().html('<div class="content" align="center" style=" margin-top: 5px;">'+
												    '<img src="${basePath!}/img/img/wushoucang.png" alt=""/>'+
												    '<h5>亲,你还没收藏哦...</h5>'+
												'</div>');
										}
								});
							});
                            $html.find(".container-fluid .col-xs-3").attr("data", data.mid);
							$html.find(".container-fluid .col-xs-3").click(function(){
                                window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');
							});
							$('#sd').append($html);

							if(data.status == 2){
                                $("#sd .col-xs-5 p:last-child").css("display","block")
                            }

						}
					}else{
                        $('#sd').parent().html('<div class="content" align="center" style=" margin-top: 5px;">'+
                                '<img src="${basePath!}/img/img/wushoucang.png" alt=""/>'+
                                '<h5>亲,你还没收藏哦...</h5>'+
                                '</div>');
					}
				});
			}
		});
	</script>
</html>
