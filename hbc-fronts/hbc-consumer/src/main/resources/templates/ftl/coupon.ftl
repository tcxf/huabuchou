<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zybl.css"/>
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <title>领券中心</title>
    <style>
    	  /*滚动水平导航栏 start*/
        .lr_nb{height:40px;line-height:40px;width:100%;position:absolute;background:#fff;padding: 0px 10px;box-sizing:border-box;z-index:1;max-width:1080px;opacity:1;top:0;-webkit-transition:.3s all;transition:.3s all}
        .lr_nb .slider_wrap.line{overflow:hidden;overflow-x:scroll;width:100%;-webkit-overflow-scrolling:touch;}
        .lr_nb .slider_wrap.line .item_cell{display:inline-block;margin: 0px 10px;;overflow:hidden;position:relative;}
        .lr_nb .slider_wrap.box{overflow:hidden;width:100%}
        .lr_nb .slider_wrap::-webkit-scrollbar{display:none}
        .lr_nb .wx_items{white-space:nowrap}
        .lr_nb .wx_items span{font-size: 15px; white-space:nowrap;display:block;-webkit-tap-highlight-color:rgba(0,0,0,0);text-align:center;cursor:pointer}
        .lr_nb_after{height:40px;display:block;clear:both;}
        .lr_nb .active{
             color: #ff7a00;
            border-bottom: 2px solid #ff7a00;
         	}
         	 .lr_nb .active a{
             color: #ff7a00;
         	}
        .lr_nb a{
            color: #333;
           text-decoration: none;
           font-size:1rem
        } 
       .lr_nb span a:hover{
       	color: #ff7a00;
       }
        .m-banner>img{
            width: 100%;
            margin-top: 44px;
        }
        .yhj{
            width: 100%;
           background:url(${basePath!}/img/imgs/background_youhuiquan.png) no-repeat;background-size:100%;
        	height: 92px;
        }
        .yhj .col-xs-5{
        	margin-top:5px;
        	padding-left: 25px;
        	line-height: 25px
        }
        .yhj .col-xs-6{
        	padding-top: 15px;
        	margin-left:-27px;
        	line-height:35px;
        }
        .yhj .col-xs-2{
        	padding-top: 25px;
        	margin-left: -10%
        }
        @media (min-width: 375px) and (max-width: 979px){
        	 .yhj{
            width: 100%;
           background:url(${basePath!}/img/imgs/background_youhuiquan.png) no-repeat;background-size:100%;
        	height: 100px;
        }
        }
         @media (min-width: 768px) and (max-width: 979px){
         	 .yhj{
            width: 100%;
           background:url(${basePath!}/img/imgs/background_youhuiquan.png) no-repeat;background-size:100%;
        	height: 190px;
        }
        .yhj .col-xs-5{
        	margin-top: 47px;
            padding-left: 87px;
        }
          .yhj .col-xs-6{
        	padding-top: 60px;
        	margin-left:-27px;
        	line-height:35px;
        }
         .yhj .col-xs-2{
        	padding-top: 72px;
        	margin-left: -10%
        }
         }
        .row{
            margin-right: 0 ;
        }
        .sms-btn1{
            border-color: #ff7a00;
            border-radius: 10px;
            color: #ff7a00;
            font-size: 0.8em;
            padding:3px 5px 1px 5px;
        }
         .active{
        	color:#ff7a00
        }
        .clearfix li a:hover{
        	color:#ff7a00;
        	border-bottom:1px solid #ff7a00;
        }
    </style>
</head>
<body>
<div class="wrapper wrapper03" id="wrapper03">
<div class="lr_nb">
    <div class="slider_wrap line">
        <div class="wx_items">
            <div class="item_cell active" onclick="redlist('null')">
                <a href="javascript:void(0)">精选</a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="m-banner">
    <img src="${basePath!}/img/imgs/banner1a.png" alt=""/>
</div>
<div  style="margin-top: 5px;">
<ul id="yhq"></ul>
</div>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script>

$(document).ready(function() {
    $(function(){
        $(".wx_items .item_cell").click(function(){
        	 $(".item_cell").removeClass('active');
             $(this).addClass('active');
        });
    });
    /**
	 * 商户导航栏
     */
		api.tools.ajax({
            url:'${basePath!}/merchants/Querymerchantcategory',
			data:{
			}
		},function(d){
				if(d.data!= null ){
					var html = template.load({
							host : '${basePath}/',
							name : "coupon_list",
							data:{
							    data : d.data
							}
						});
						$html = $(html);
						$('.wx_items').append($html);
						var nature=undefined;
						$(".wx_items div").click(function(){

					        	 $(".item_cell").removeClass('active');
					             $(this).addClass('active');

							nature=	$(this).children("span").html();
							if(nature ==undefined){
							    redlist('null')
                            }else {
                                redlist(nature);
                            }

						});
				}
			});

    redlist('null');


//精选优惠券
function redlist(micId){
    api.tools.ajax({
        url:'${basePath!}/packet/queryloadReadPckess',
        data:{
            micId:micId,
            // userId:'-1',
            rows:10,
            page:1,
            orderStatus:status
        }
    },function(d){
        $('#yhq').empty();
        var data=d.data.records;
        var rpsId=null;
        if(data!= null    && data.length > 0){
            for(var i = 0 ; i < data.length ; i++){
                var datas = data[i];
                data.img =datas.img;
                var html = template.load({
                    host : '${basePath!}/',
                    name : "shopType_list",
                    data : datas
                });
                $html = $(html);
                if(datas.utype!=0){
                    $html.find(".sms-btn1").html("已领取");
                }else{
                    $html.find(".sms-btn1").click(function(){
                         rpsId = $(this).attr("data");
                        // alert(rpsId)
                        api.tools.ajax({
                            url:'${basePath!}/user/Ucoupon/getCoupon',
                            data:{
                              id:rpsId
                            }
                        },function(d){
                            window.location.reload();
                            api.tools.toast(d.msg);
                            if(d.code != -1){
                                redlist();
                            }
                        });
                    });
                }
                $('#yhq').append($html);
            }
        }else{
            $('#yhq').html('<div class="content" align="center" style="position:absolute;margin-top:7px;background:none">'+
                    '<img src="${basePath!}/img/img/icon_zanwujilu.png" alt=""/>'+
                    '<h5>暂无记录...</h5>'+
                    '</div>');
        }
    });
}
});

</script>
</body>
</html>