<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/more_yhj.css" />
    <title>更多优惠券</title>
    <style>
        .nav_menu #merchantcategory>li a{
            color: #f49110!important;
            text-decoration: none;
            font-size: 12px!important;
        }
        .nav_menu #merchantcategory>.active a{
            padding: 3px 6px;
            border-radius: 30px;
            background: #f49110;
            color: #fff!important;
        }
        #merchantcategory>li:first-child{
            width: 15%;
        }
        .manjian .col-xs-6{
            margin-top:1px;
        }
        .manjian .col-xs-6:last-child{
            text-align: right;
            padding-right: 20px;
            color: #333;
            font-weight: 800;
            font-size: 12px;
            font-family: KaiTi;
        }
        .top_menu{
            width: 100%;
            height: 40px;
            line-height: 30px;
        }
        .top_menu .col-xs-3{
            padding: 0;
            text-align: center;
        }
        .top_menu .col-xs-4>img{
            width: 11px;
            position: absolute;
            top:10px;
        }
        .top_menu .search{
            width: 100%;
            height: 30px;
            background: #f5f5f5;
            border-radius: 30px;
            margin-top: 5px;
        }
        .top_menu a{
          color: #999;
        }
    </style>
</head>
<body>
<div class="top_menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <a href="${basePath!}/merchantQuery/Queryhotsearch">
                    <div class="search">
                        <div class="col-xs-4">
                            <img src="${basePath!}/img/imgs/icon_souyisou.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            <p>试试搜索</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="top_content">
    <div class="bg_img">
    </div>
</div>
<div class="nav_menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-10">
                <ul id="merchantcategory">
                    <li class="active">
                        <a href="">全部</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-2 onclick">
                <img src="${basePath!}/img/img/down_enter.png" alt="">
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mymodal">
    <div class="modal-dialog" id="modal-dialog">
        <div class="content">
            <div class="header">
                <p>为你推荐</p>
                <div class="up_enter">
                    <img src="${basePath!}/img/img/ico_up_enter.png" alt="">
                </div>
            </div>
            <div class="media-body">
                <ul>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="coupont_list">
    <ul>
    </ul>
</div>
<div style="text-align: center;  margin-bottom: 80px;" id="dj">
    <a href="javascript:void(0)" style="color: #999"  id="dianji">点击查看更多</a>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {
        var page= 1;
        var micId="null";

        $(".onclick").click(function () {
            document.getElementById("mymodal").style.display="block";
            document.getElementById("mymodal").style.background="rgba(0,0,0,.5)";
        });
        $(".up_enter").click(function () {
            $("#mymodal").css("display","none")
        })

        //商户导航排序5张
        Querymerchantcategory();
        //商户导航排序
        Querymerchantcategorys();
        function Querymerchantcategory(){
            $.ajax({
                type: "POST",
                url:'${basePath!}/merchants/Querymerchantcategorys',
                data: {},
                dataType: "json",
                success: function (d) {
                    console.log(d);
                    if (d.data != null && d.data.records!= null && d.data.records.length > 0) {
                        for (var i = 0; i < d.data.records.length; i++) {
                            data = {
                                names: d.data.records[i].name,
                                id: d.data.records[i].id
                            };
                            data.path = '${basePath!}';
                            var html = template.load({
                                host: '${basePath!}/',
                                name: "packet_list",
                                data: data,
                            });
                            $html = $(html);
                            $('#merchantcategory').append($html);
                            $("#merchantcategory .dj").click(function () {
                                $("#merchantcategory .dj").removeClass("active");
                                $(".nav_menu #merchantcategory>li").removeClass("active")
                                $(this).addClass("active");
                            });
                            var nature=undefined;
                            $html.find('.dj').click(function(){
                                nature=	$(this).children("span").html();
                                $('.coupont_list').empty();
                                page =1;
                                if(nature ==undefined){
                                    redlist('null',page)
                                }else {
                                    redlist(nature,page);
                                }
                                micId=nature;
                            })
                        }
                    }
                }
            });
        }

        function Querymerchantcategorys() {
            $.ajax({
                type: "POST",
                url: '${basePath!}/merchants/Querymerchantcategory',
                data: {},
                dataType: "json",
                success: function (d) {
                    console.log(d);
                    if (d.data != null && d.data.length > 0) {
                        for (var i = 0; i < d.data.length; i++) {
                            data = {
                                names: d.data[i].name,
                                id: d.data[i].id
                            };
                            data.path = '${basePath!}';
                            var html = template.load({
                                host: '${basePath!}/',
                                name: "packet_list",
                                data: data
                            });
                            $html = $(html);
                            $('.media-body>ul').append($html);
                            var nature=undefined;
                            $html.find('.dj').click(function(){
                                nature=	$(this).children("span").html();

                                $("#mymodal").css("display","none");
                                $('.coupont_list').empty();
                                page =1;
                                if(nature ==undefined){
                                    redlist('null',page)
                                }else {
                                    redlist(nature,page);
                                }
                                micId=nature;
                            })
                        }
                    }
                }
            });
        }

        redlist('null',page);
        //精选优惠券
        function redlist(micId,page){
            api.tools.ajax({
                url:'${basePath!}/packet/queryloadReadPckess',
                data:{
                    micId:micId,
                    // userId:'-1',
                    // rows:10,
                    // page:1,
                    page:page,
                    orderStatus:status
                }
            },function(d){
                var data=d.data.records;
                if(data.length<10){
                    $("#dj").hide();
                }
                if(data.length>=10){
                    $("#dj").show();
                }
                var rpsId=null;
                if(data!= null    && data.length > 0){
                    for(var i = 0 ; i < data.length ; i++){
                        var datas = data[i];
                        data.img =datas.img;
                        datas.path = '${basePath!}';
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "packets_list",
                            data : datas
                        });
                        $html = $(html);
                        if(datas.utype!=0){
                            $html.find(".sms-btn1").html("已领取");
                            $html.find(".sms-btn1").css("background","#999");
                        }else{
                            $html.find(".sms-btn1").click(function(){
                                rpsId = $(this).attr("data");
                                // alert(rpsId)
                                api.tools.ajax({
                                    url:'${basePath!}/user/Ucoupon/getCoupon',
                                    data:{
                                        id:rpsId
                                    }
                                },function(d){
                                    window.location.reload();
                                    api.tools.toast(d.msg);
                                    if(d.code != -1){
                                        redlist();
                                    }
                                });
                            });
                        }
                        $html.find(".coupont_content").click(function () {
                            window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');
                        })
                        $('.coupont_list').append($html);
                    }
                }else{
                    if(page==1){
                        $('.coupont_list').html('<div class="content" align="center" style="margin-top:7px;background:none">'+
                                '<img src="${basePath!}/img/img/icon_zanwujilu.png" style="width:50%" alt=""/>'+
                                '<h5>暂无记录...</h5>'+
                                '</div>');
                    }
                }
            });
        }

        $(".active").click(function () {
            redlist('null');
        })

        $("#dianji").click(function(){
            page = page+1;
            redlist(micId,page);
        });
    })

</script>
</body>
</html>