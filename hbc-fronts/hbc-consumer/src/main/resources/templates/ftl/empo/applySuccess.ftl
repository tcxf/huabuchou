<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>报名成功</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 10rem;
        }
        .s_content{
            padding: 20px 30px;

        }
        .s_content>p{
            color: #666;
        }
        .s_content span{
            color: #ee332c;
        }
        .tj{
            margin-top: 40%;
            background:#f49110;
            color:#ffffff;
            border-radius:30px;
            font-size: 12px;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            text-align: center;
        }
        .tj>a{
            text-decoration: none;
            color: #fff;
        }
    </style>
</head>
<body>
<div class="top">
    <img src="${basePath!}/img/img/img_is_ok.png" alt=""/>
</div>
<div class="s_content">
    <p>您的商业授信报名成功，我们的工作人员会在<span>1-3</span>个工作日内与您联系，
        请您耐心等待，谢谢!</p>
</div>
<div class="yzm-btn tj" onclick="goIndex()">
    <a href="${basePath!}/user/index/Goindex">返回首页</a>
</div>
</body>
</html>