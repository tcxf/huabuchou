<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>商业授信</title>
    <style>
        body{
            background: #fff;
        }
        .top_content{
            width: 100%;
            height: auto;
            background: #fff;
            box-shadow: 0 0 5px #ededed;
            text-align: center;
            padding: 20px 0;
            border-bottom: 3px solid #ededed;
        }
        .top_content>p{
            font-size: 14px
            margin-bottom: 5px;
        }
        .top_content .input_money>input{
            border: none;
            text-align: center;
            outline:none;
            width: 80px;
            font-size: 2rem;
            height: 30px;
        }
        .top_content .input_money #img_wirte img{
            width: 18px;
        }
        .choose_money{
            margin-top: 20px;
        }
        .choose_money .col-xs-3{
            padding: 0;
        }
        .choose_money .col-xs-3 p{
            margin-bottom: 0;
        }
        .choose_money .col-xs-3 h6{
            margin: 0;
        }
        .choose_money .col-xs-6{
            padding: 0;
        }
        .choose_money .col-xs-6>img{
            width: 100%;
            height: 8px;
        }
        .choose_fenqi{
            width: 100%;
            height: auto;
            box-shadow: 0 0 5px #ededed;
            padding: 10px 0;
        }
        input[type=checkbox], input[type=radio]{
            margin: 0;
            width: 20% !important;
        }
        .choose_fenqi .col-xs-5{
            text-align: right;
        }
        .choose_fenqi .col-xs-5>img{
            width: 10px;
            height: 12px;
        }
        .a-btn{
            margin-top: 40px;
            width: 94%;
            height: 40px;
            background:#f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }
        .container_gress{
            margin-top: 10px;
            color:#999;
            height: 20px;
            line-height: 20px;
        }
        .container_gress .col-xs-12{
            text-align: center;
        }
        .container_gress .col-xs-12>.col-xs-3{
            text-align: right;
            padding: 0;
        }
        .container_gress .col-xs-12>.col-xs-9{
            text-align: left;
            padding: 0;
        }
        .container_gress .col-xs-12>.col-xs-9>a{
            text-decoration: none;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            margin-top: 20px;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="top_content">
    <p>当前总额度(元)</p>
    <div class="input_money">
        <input type="text" value="20000" id="authmax"><span id="img_wirte"><img src="${basePath!}/img/wirte.png" alt=""></span>
    </div>
    <div class="choose_money">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3">
                    <p>最低</p>
                    <h6>2,0000</h6>
                </div>
                <div class="col-xs-6">
                    <img src="${basePath!}/img/a_line.png" alt="">
                </div>
                <div class="col-xs-3">
                    <p>最高</p>
                    <h6>50,0000</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="choose_fenqi">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                分期期数
            </div>
            <div class="col-xs-3 relationship">
            </div>
            <div class="col-xs-5" id="choice_linkman">
                <img src="${basePath!}/img/right_enter.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodalone" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <ul>
                    <li>12期</li>
                    <li>24期</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<button class="btn-block" id="logout">确认设置</button>
<div class="container_gress">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <input type="checkbox" id="agree"/>
                </div>
                <div class="col-xs-9">
                    同意
                    <a href="#mymodalFive" data-toggle="modal" style="color: #008fff;">《商户授信协议》</a>
                </div>
            </div>
        </div>
    </div>
</div>
  <#include "../agreement/models.ftl">
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.min.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script>
    $("#choice_linkman").click(function () {
        $("#mymodalone").css("display","block")
    });
    $(".modal-body li").click(function(){
        $(".modal-body").find('li').removeClass('active');
        $(this).addClass('active');
        var choice =$(this).html();
        $(".relationship").html(choice);
        $("#mymodalone").css("display","none")
    });
  $("#logout").on("touchend",function () {
      var totaltimes = $(".active").text();
      var authMax = parseInt($("#authmax").val());
      if (authMax == ''|| authMax == null)
      {
          layer.msg("请填写额度");
          return ;
      }
      if (totaltimes == ''|| totaltimes == null)
      {
          layer.msg("请选择分期期数");
          return ;
      }
      if(!$("#agree").is(':checked')) {
          layer.msg("请先同意《商户授信协议》");
          return;
      }
      if (authMax > 500000 || authMax < 20000){
          layer.msg("额度只能在20000-500000之间");
          return;
      }

      if(totaltimes == '12期'){
          totaltimes = 12;
      }else if (totaltimes == '24期') {
          totaltimes = 24;
      }

      $.ajax({
          url:"${basePath!}/user/empo/toSaveBusinessCredit",
          type:'POST', //GET
          data:{
              totaltimes: totaltimes,
              authMax: authMax
          },
          success:function(d){
              if (d.code == 0)
              {
                  layer.msg("保存成功");
                  window.location.href = "${basePath!}/user/empo/toApplySuccessView";
              }else{
                  layer.msg("保存失败");
              }
          }
      });
  });



</script>
</body>
</html>