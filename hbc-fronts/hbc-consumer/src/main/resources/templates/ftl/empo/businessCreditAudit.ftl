<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>商业授信(审核中)</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 20%;
        }
        .top>img{
            width: 35%;
        }
        .top p{
            margin-top: 10px;
            color: #666;
        }
        .s_content{
            padding: 20px 30px;
        }
        .s_content>p{
            color: #999;
        }
        .btn-block{
            margin-top: 10px;
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="top">
    <img src="${basePath!}/img/img/ico_sh.png" alt=""/>
    <p>正在审核中...</p>
</div>
<div class="s_content">
    <p>您提交的商业授信申请已收到，我们的工作人员正
        在努力审核中，请您耐心等待，如有疑问请拨打客服
        电话：0731-88888292
    </p>
</div>
<div>
    <button class="btn-block">返回首页</button>
</div>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script>
    $(".btn-block").click(function () {
        window.location.href="${basePath!}/user/index/Goindex";
    })
</script>


</body>

</html>