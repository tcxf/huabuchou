<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>审核通过额度</title>
    <style>
        .Auditing{
            margin-top: 20px;
            width: 90%;
            height: 400px;
            border-radius: 8px;
            margin-left: 7%;
            box-shadow: 1px 1px 10px 1px #ededed;
            position: relative;
        }
        .b_img1{
            position: absolute;
            top:0;
            left: 0;
            width: 60%;
        }
        .b_img2{
            position: absolute;
            bottom:0;
            right: 0;
            width: 60%;
        }
        .Auditing_content{
            padding-top: 50px;
            text-align: center;
        }
        .Auditing_content>p{
            color: #666;
        }
        .Auditing_content h2{
            color: #f49110;
            font-size: 4rem;
        }
        .Auditing_money{
            margin-top:50px;
        }
        .Auditing_money .col-xs-6{
            text-align: center;
            padding: 0;
        }
        .Auditing_money .col-xs-6>span{
            color: #666;
        }
    </style>
</head>
<body>
<div class="Auditing">
    <img src="${basePath!}/img/imgs/shangyeshouxin_upper_line.png" class="b_img1" alt="">
    <img src="${basePath!}/img/imgs/shangyeshouxin_lower_line.png" class="b_img2" alt="">
    <div class="Auditing_content">
        <p>审核通过额度</p>
        <h2>${endmoeny}</h2>
    </div>
    <div class="Auditing_money">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6">
                    <span>申请额度：</span>
                    <b>${authmax}</b>
                </div>
                <div class="col-xs-6">
                   <span>分期期数：</span>
                    <b>${totaltimes}期</b>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>