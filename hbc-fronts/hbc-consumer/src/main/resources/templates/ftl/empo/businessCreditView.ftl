<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
    <title>商业授信引导页</title>
    <style>
        body{
            background: url(${basePath!}/img/imgs/sysx_loding.png);
            background-size: cover;
        }
        .sx_text{
            width: 100%;
            position: fixed;
            bottom: 20%;
            text-align: center;
        }
        .sx_text>h3{
            color: #ff7a00;
            font-size: 1.8rem;
            font-weight: 600;
        }
        .sx_text p{
            margin-bottom: 0;
            color: #666;
        }
        .a_content{
            position: fixed;
            bottom:30px;
            width: 100%;
        }

        .container_gress{
            margin-top: 5px;
            color:#999;
            height: 20px;
            line-height: 20px;
        }
        .container_gress .col-xs-12{
            text-align: center;
        }
        .container_gress .col-xs-12>.col-xs-4{
            text-align: right;
            padding-right: 5px;
            margin-top: -3px;
        }
        .container_gress .col-xs-12>.col-xs-8{
            text-align: left;
            padding-left: 0;
        }
        .container_gress .col-xs-12>.col-xs-8>a{
            text-decoration: none;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer
        }
    </style>
</head>
<body>
<div class="sx_text">
    <h3>商业授信</h3>
    <p>&nbsp&nbsp&nbsp&nbsp会员--享受最高30天免息授信消费</p>
    <p>商家--获得高额，快捷商业授信</p>
</div>
<div class="a_content">
    <div>
        <button class="btn-block l_btn">授信报名</button>
    </div>
    <div class="container_gress">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-4">
                        <input type="checkbox" id="agree"/>
                    </div>
                    <div class="col-xs-8">
                        同意<a href="#mymodalFive" data-toggle="modal" style="color: #008fff;">《商户授信协议》</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <#include "../agreement/models.ftl">
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.min.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script>
    $(".l_btn").on("touchend",function () {
        if(!$("#agree").is(':checked')) {
            layer.msg("请先同意《商户授信协议》");
            return;
        }
        window.location.href = "${basePath!}/user/empo/businessCreditView";
    })


</script>
</body>
</html>