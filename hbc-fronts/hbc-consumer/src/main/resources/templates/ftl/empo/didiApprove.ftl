<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>
        <#if type == 'socialInsurance'>
            社保认证
        </#if>
        <#if type == 'accumulationFund'>
            公积金认证
        </#if>
        <#if type == 'educationApprove'>
            学信网认证
        </#if>
        <#if type == 'taobaoApprove'>
            淘宝认证
        </#if>
        <#if type == 'jDcomApprove'>
            京东认证
        </#if>
        <#if type == 'didiApprove'>
            滴滴认证
        </#if>
    </title>
    <style>
        body{
            background: #fff;
        }
        .content_list{
            width: 100%;
            height: auto;
            background:#fff;
            box-shadow: 0 0 5px #dedede;
        }

        .content_list .row{
            border-bottom: 1px solid #ededed;
            padding: 13px;
        }
        .content_list .row:last-child{
            border: none;
        }
        .content_list .col-xs-4{
            padding-left: 0;
        }
        .content_list .col-xs-8{
            padding-left: 0;
        }

        .content_list .col-xs-8>input{
            border: none;
            outline:medium;
        }
        .text_a{
            padding: 30px;
            color: #999;
        }
        .text_a>span{
            color: #f90003;
        }
        .a-btn{
            width: 94%;
            height: 40px;
            background:#f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="content_list">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                真实姓名
            </div>
            <div class="col-xs-8">
                <input type="text" id="realName" placeholder="请输入真实姓名"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                身份证号
            </div>
            <div class="col-xs-8">
                <input type="text" id="cardNo" placeholder="请输入您的身份证号码"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                手机号码
            </div>
            <div class="col-xs-8">
                <input id="mobile" type="text" placeholder="请输入您的手机号码"/>
            </div>
        </div>
    </div>
</div>

<form action="${basePath!}/user/empo/toDidiDacheJump" id="didijumpform" method="get">
    <input type="hidden" value="" id="urls" name="urls">
</form>
<form action="${basePath!}/user/empo/toTaobaoJump" id="taobaojumpform" method="get">
    <input type="hidden" value="" id="taobaourls" name="taobaourls">
</form>
<div class="a-btn" id="shouxin">
    <a href="#">认证</a>
</div>
</body>

<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/html5shiv.min.js"></script>
<script src="${basePath!}/js/respond.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
<script src="${basePath!}/js/layer/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<#include "../agreement/mune_btn.ftl">
</html>


<script >
    $(document).ready(function(){
        $("#shouxin").click(function(){
            var mobile = $("#mobile").val();
            var userName = $("#realName").val();
            var idCard =$("#cardNo").val();
            var type = '${type!}';
            var reg = /^[\u4e00-\u9fa5]{2,4}$/i;
            if (!reg.test(userName)) {
                layer.msg("请输入真实姓名，只能是2-4个汉字！");
                return ;
            }
            if(idCard==""){
                layer.msg("身份证不能为空，请重填");
                return;
            }
            var regs = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(!regs.test(idCard)){
                layer.msg("您输入的身份证号码不正确，请重填");
                return;
            }
            if (!(/^1\d{10}$/.test(mobile))){
                layer.msg("手机号码有误，请重填");
                return;
            }
            if (type == 'socialInsurance'){
                window.location.href = "${basePath!}/user/empo/toSocialInsurance?phone="+mobile+"&name="+userName+"&idcard="+idCard;
            }else if (type == 'accumulationFund'){
                window.location.href = "${basePath!}/user/empo/toAccumulationFund?phone="+mobile+"&name="+userName+"&idcard="+idCard;
            }else if (type == 'educationApprove'){
                window.location.href = "${basePath!}/user/empo/toEducationApprove?phone="+mobile+"&name="+userName+"&idcard="+idCard;
            }else if (type == 'taobaoApprove'){

                api.tools.ajax({
                    url:"${basePath!}/user/empo/toTaobaoApprove",
                    data:{
                        phone:mobile,
                        name:userName,
                        idcard:idCard
                    },
                },function(d){
                    if (d.code == 0){
                        $("#taobaourls").val(d.data);
                        $("#taobaojumpform").submit();
                    }
                });

                <#--window.location.href = "${basePath!}/user/empo/toTaobaoApprove?phone="+mobile+"&name="+userName+"&idcard="+idCard;-->
            }else if (type == 'jDcomApprove'){
                window.location.href = "${basePath!}/user/empo/toJDcomApprove?phone="+mobile+"&name="+userName+"&idcard="+idCard;
            }else if (type == 'didiApprove'){

                api.tools.ajax({
                    url:"${basePath!}/user/empo/toDidiDacheApprove",
                    data:{
                        phone:mobile,
                        name:userName,
                        idcard:idCard
                    },
                },function(d){
                    if (d.code == 0){
                        $("#urls").val(d.data);
                        $("#didijumpform").submit();
                    }
                });

                <#--window.location.href = "${basePath!}/user/empo/toDidiDacheApprove?phone="+mobile+"&name="+userName+"&idcard="+idCard;-->
            }
        });
    });
</script>
