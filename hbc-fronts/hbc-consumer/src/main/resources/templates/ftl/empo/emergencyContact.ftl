<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>紧急联系人</title>
</head>
<body>
    <div class="linkman">
        <ul>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-5">
                            联系人姓名
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="请输入联系人姓名" id="name">
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-5">
                            联系人手机号码
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="请输入联系人手机号码" id="mobile">
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container-fluid">
                    <div class="row" id="choice_linkman">
                        <div class="col-xs-5">
                           与本人关系
                        </div>
                        <div class="col-xs-6 relationship">
                            请选择与本人关系
                        </div>
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/right_enter.png" alt="">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="modal" id="mymodalone" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                   <ul>
                       <li class="active">父母</li>
                       <li>夫妻</li>
                       <li>亲戚</li>
                       <li>兄弟</li>
                       <li>姐妹</li>
                       <li>同事</li>
                       <li>朋友</li>
                   </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="submit_btn" id="submit">
        <a href="#">保存</a>
    </div>
    <script src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.min.js"></script>
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/template/template.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <#include "../agreement/mune_btn.ftl">
    <script>
        $("#choice_linkman").click(function () {
            $("#mymodalone").css("display","block")
        });
        $(".modal-body li").click(function(){
            $(".modal-body").find('li').removeClass('active');
            $(this).addClass('active');
            var choice =$(this).html();
            $(".relationship").html(choice);
            $("#mymodalone").css("display","none")
        });
        $(document).ready(function() {
            $("#submit").click(function() {
                var name = $("#name").val();
                var mobile = $("#mobile").val();
                var relation = $(".active").text();
                var regName =/^[\u4e00-\u9fa5]{2,4}$/;
                if (name == '' || name == null)
                {
                    api.tools.toast("请填写姓名");
                    return ;
                }
                if(!regName.test(name)){
                    api.tools.toast('真实姓名填写有误');
                    return ;
                }
                if (mobile == ''|| mobile == null)
                {
                    api.tools.toast("请填写电话号码");
                    return ;
                }
                if(!(/^1[23456789]\d{9}$/.test(mobile))){
                    api.tools.toast("请输入正确的手机号码");
                    return;
                }
                if (relation == ''|| relation == null)
                {
                    api.tools.toast("请填写关系");
                    return ;
                }
                $(this).html('提交中...');

                $.ajax({
                    url:"${basePath!}/user/empo/saveEmergencyContact",
                    type:'POST', //GET
                    data:{
                        name: name,
                        mobile: mobile,
                        relation:relation
                    },
                    success:function(d){
                        $(this).html('完成');
                        if (d.code == 0)
                        {
                            api.tools.toast("保存成功");
                            window.location.href = "${basePath!}/user/empo/goEmpoPlus";
                        }else{
                            api.tools.toast("保存失败");
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>