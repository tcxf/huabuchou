<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <#include "common.ftl" />
    <title>授信提额</title>
    <style>
        .btn-block{
            margin-top: 20px;
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            text-align: center;
            cursor: pointer
        }
    </style>
</head>
<body>
<div class="must_list">
    <p class="one_text">必填项</p>
    <ul>
        <li>
            <a href="${basePath!}/user/empo/emergencyContact" id="code_emergencyContact_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_emergencycontact.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            紧急联系人
                        </div>
                        <div class="col-xs-3 is_ok" id="code_emergencyContact" hidden>
                            已完善
                        </div>
                        <div class="col-xs-3 is_no" id="code_emergencyContact_1">
                            待完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/qqOrWeChat"  id="code_qqOrWeChat_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_qq.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            QQ/微信
                        </div>
                        <div class="col-xs-3 is_ok" id="code_qqOrWeChat" hidden>
                            已完善
                        </div>
                        <div class="col-xs-3 is_no" id="code_qqOrWeChat_1">
                            待完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <p class="tow_text">选填项
        <span>(至少完成一项方可进行提额，完善越多提额越高)</span>
    </p>
    <ul>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=socialInsurance" id="code_socialInsurance_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_shebao.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            社保认证
                        </div>
                        <div class="col-xs-3 is_no" id="code_socialInsurance_1" >
                            待完善
                        </div>
                        <div class="col-xs-3 is_ok" id="code_socialInsurance" hidden>
                            已完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=accumulationFund" id="code_accumulationFund_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_gongjij.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            公积金
                        </div>
                        <div class="col-xs-3 is_no" id="code_accumulationFund_1">
                            待完善
                        </div>
                        <div class="col-xs-3 is_ok" id="code_accumulationFund" hidden>
                            已完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=educationApprove" id="code_eeducationApprove_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_xueli.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            学历认证
                        </div>
                        <div class="col-xs-3 is_no" id="code_educationApprove_1">
                            待完善
                        </div>
                        <div class="col-xs-3 is_ok" id="code_educationApprove" hidden>
                            已完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=taobaoApprove" id="code_taobaoApprove_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_taobao.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            淘宝/支付宝
                        </div>
                        <div class="col-xs-3 is_no" id="code_taobaoApprove_1" >
                            待完善
                        </div>
                        <div class="col-xs-3 is_ok" id="code_taobaoApprove" hidden>
                            已完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=jDcomApprove" id="code_jDcomApprove_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_jindong.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            京东
                        </div>
                        <div class="col-xs-3 is_no" id="code_jDcomApprove_1">
                            待完善
                        </div>
                        <div class="col-xs-3 is_ok" id="code_jDcomApprove" hidden>
                            已完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/empo/toDidiApproveView?type=didiApprove" id="code_didiDacheApprove_2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/credit_didi.png" alt="">
                        </div>
                        <div class="col-xs-8">
                            滴滴打车
                        </div>
                        <div class="col-xs-3 is_ok" id="code_didiDacheApprove" hidden>
                            已完善
                        </div>
                        <div class="col-xs-3 is_no" id="code_didiDacheApprove_1">
                            待完善
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>
<button class="btn-block" id="submit">提交审核</button>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/html5shiv.min.js"></script>
<script src="${basePath!}/js/respond.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {
        api.tools.ajax({
            type:"post",
            url:"${basePath!}/user/empo/getEmpo"
        },function(d){
            var  data = d.data;
            for ( var i=0; i < data.length; i++){
                var code = data[i];
                $("#code_"+code.code+"_1").hide();
                $("#code_"+code.code).show();
                $("#code_"+code.code+"_2").attr('href', 'javascript:;');
            }
        });
        $("#submit").click(function() {
            api.tools.ajax({
                type:"post",
                url: "${basePath!}/user/empo/toSaveIncreaseCredit"
            },function (data) {
                if (data.code == 0)
                {
                    var subGrade = data.data.subGrade;
                    var sumbalance = data.data.sumbalance;
                    window.location.href = "${basePath!}/user/empo/amount?subGrade="+subGrade+"&sumbalance="+sumbalance;
                }else {
                    api.tools.toast(data.msg);
                }
            });
        });
    });
</script>
</body>
</html>