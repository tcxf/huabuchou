<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/loading.css">
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
    <title>快速授信</title>
    <style>
        body{
            background: #fff;
        }
        .content_list{
            width: 100%;
            height: auto;
            background:#fff;
        }

        .content_list .row{
            border-bottom: 1px solid #ededed;
            padding: 13px 5px;
        }

        .content_list .col-xs-5{
            padding: 0;
            text-align: left;
            width: 52%;
        }

        .content_list .col-xs-7{
            padding: 0 10px;
            width: 47%;
            text-align: left;
        }
        .content_list .col-xs-5>input{
            padding-left: 5px;
            width: 100%;
            border: none;
            outline: none;
        }
        .text_a{
            padding: 30px 30px 0 30px;

        }
        .text_a>img{
            width: 15px;
            height: 15px;
            float: left;
        }
        .text_a p{
            margin-left: 20px;
            letter-spacing: 1px;
            color: #ff5f3e;
            font-weight: 600;
            font-size: 12px;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            text-align: center;
            cursor: pointer
        }
        .cancel_btn{
            margin-top: 20px;
            width: 94%;
            height: 40px;
            border: 1px solid #f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .cancel_btn>a{
            color: #f49110;
            text-decoration: none;
        }
        #loading{
            background: transparent;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .bg_loading{
            width: 50%;
            margin-left: 25%;
            height: 100px;
            background: rgba(0,0,0,.5);
            padding-top: 7px;
            margin-top: 20%;
        }
    </style>
</head>
<body>
<div class="text_a">
    <img src="${basePath!}/img/sx_icon_tips.png" alt="">
    <p>尊敬的用户，信息错误两次将锁定账户，须三个月以后再次进行授信，请务必认真填写。</p>
</div>
<div class="content_list">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-7">
                本人真实银行卡号:
            </div>
            <div class="col-xs-5">
                <input type="text" id="bankcard" placeholder="请输入您的银行卡号"/>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-7">
                银行卡预留手机号:
            </div>
            <div class="col-xs-5">
                <input id="mobile" type="text" placeholder="请输入您的手机号码"/>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-7">
                本人真实姓名:
            </div>
            <div class="col-xs-5">
                <input type="text" id="realName" placeholder="请输入真实姓名"/>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-7">
                本人真实身份证号:
            </div>
            <div class="col-xs-5">
                <input type="text" id="cardNo" placeholder="请输入您的身份证号码"/>
            </div>
        </div>
    </div>
</div>
<button class="btn-block" id="shouxin">快速授信</button>
<div id="loading">
   <div class="bg_loading">
       <div class="m-load2">
           <div class="line">
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
               <div></div>
           </div>
           <div class="circlebg"></div>
       </div>
       <div class=jiazai>正在授信中，请耐心等待...</div>
   </div>

</div>


<#if isjump ??>
    <#else>
        <div class="cancel_btn">
            <a href="${basePath!}/user/index/Goindex">跳过</a>
        </div>
</#if>
</body>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
    <script src="${basePath!}/js/layer/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>

</html>


<script >
    $(document).ready(function(){
        $("#bankCard").on("input",function(){
            var val=$(this).val().replace(/\s/g,"").replace(/(\d{4})/g,"$1 ");
            $(this).val(val);
        });
        $("#bankCard").on("keyup",function(){
            //获取当前光标的位置
            var caret = this.selectionStart;
            //获取当前的value
            var value = this.value;
            //从左边沿到坐标之间的空格数
            var sp =  (value.slice(0, caret).match(/\s/g) || []).length;
            //去掉所有空格
            var nospace = value.replace(/\s/g, '');
            //重新插入空格
            var curVal = this.value = nospace.replace(/\D+/g,"").replace(/(\d{4})/g, "$1 ").trim();
            //从左边沿到原坐标之间的空格数
            var curSp = (curVal.slice(0, caret).match(/\s/g) || []).length;
            //修正光标位置
            this.selectionEnd = this.selectionStart = caret + curSp - sp;
        });

        $("#shouxin").click(function(){
            var mobile = $("#mobile").val().replace(/\s+/g, "");
            var userName = $("#realName").val().replace(/\s+/g, "");
            var accountNO=$("#bankcard").val().replace(/\s+/g, "");
            var idCard =$("#cardNo").val().replace(/\s+/g, "");
            var reg = /^[\u4e00-\u9fa5]{2,4}$/i;
            if (!reg.test(userName)) {
                layer.msg("请输入真实姓名，只能是2-4个汉字！");
                return ;
            }
            if(idCard==""){
                layer.msg("身份证不能为空，请重填");
                return;
            }
            var regs = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(!regs.test(idCard)){
                layer.msg("您输入的身份证号码不正确，请重填");
                return;
            }
            if (!(/^1[345789]\d{9}$/.test(mobile))){
                layer.msg("手机号码有误，请重填");
                return;
            }
            if(accountNO==""){
                layer.msg("银行卡号码不能为空，请重填");
                return;
            }
            if(!(/^\d{16,19}$/.test(accountNO))){
                layer.msg("银行卡号码输入格式不正确，请重填");
                return;
            }
           // var index = layer.load();
            $("#loading").css("display","block");
            setTimeout(hideDiv,30000);
            function hideDiv(){
                $("#loading").css("display","none");
            }
            api.tools.ajax({
                url:"${basePath!}/user/empo/fastEmpo",
                data:{
                    mobile:mobile,
                    userName:userName,
                    accountNO:accountNO,
                    idCard:idCard
                },
            },function(d){
               // layer.close(index);
                $("#loading").css("display","none");
                if(d.code == 0){
                    layer.msg("授权成功");
                    window.location.href = "${basePath!}/user/empo/goOperatorEmpo?mobile="+mobile+"&totalScore="+d.data;
                }else if (d.code != 0 && d.msg == '手机号及银行卡必须在本人自己名下') {
                    window.location.href = "${basePath!}/user/empo/fastEmpoFailed";
                }else {
                    layer.msg(d.msg);
                }
            });
        });
    });
</script>
