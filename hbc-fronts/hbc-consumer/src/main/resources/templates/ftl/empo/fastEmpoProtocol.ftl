<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
    <title>

        <#if userAttribute == '1'>
            会员授信引导页
        </#if>
        <#if userAttribute == '2'>
            商户授信引导页
        </#if>
    </title>
    <style>
        body{
            background: url(${basePath!}/img/imgs/sysx_loding.png);
            background-size: cover;
        }
        .sx_text{
            width: 100%;
            position: fixed;
            bottom: 20%;
            text-align: center;
        }
        .sx_text>h3{
            color: #f49110;
            font-size: 1.8rem;
            font-weight: 600;
        }
        .sx_text p{
            margin-bottom: 0;
            color: #666;
        }
        .a_content{
            position: fixed;
            bottom:30px;
            width: 100%;
        }
        .l_btn{
            width: 80%;
            height: 40px;
            background: #f49110;
            margin-left: 10%;
            border-radius: 30px;
            line-height: 40px;
            text-align: center;
        }
        .l_btn>a{
            text-decoration: none;
            color: #fff;
        }
        .container_gress{
            margin-top: 5px;
            color:#999;
            height: 20px;
            line-height: 20px;
        }
        .container_gress .col-xs-12{
            text-align: center;
        }
        .container_gress .col-xs-12>.col-xs-4{
            text-align: right;
            padding-right: 5px;
            margin-top: -2px;
        }
        .container_gress .col-xs-12>.col-xs-8{
            text-align: left;
            padding-left: 0;
        }
        .container_gress .col-xs-12>.col-xs-8>a{
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="sx_text">
    <h3>快速授信</h3>
    <p>&nbsp&nbsp&nbsp&nbsp会员--享受最高30天免息授信消费</p>
    <p>商家--获得高额，快捷商业授信</p>
</div>
<div class="a_content">
    <div class="l_btn" onclick="goFastEmpo()">
        <a href="#">立即授信</a>
    </div>
    <div class="container_gress">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-4">
                        <input type="checkbox" id="agree"/>
                    </div>
                    <div class="col-xs-8">
                        同意<a href="#mymodalFive" data-toggle="modal" style="color: #008fff;">《授信协议》</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "../agreement/models.ftl">
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer/layer.js"></script>

<script>
    var type = '${type!}';
    var state = '${state!}';
    var openId = '${openId!}';
    var payPath = '${payPath!}';
    window.sessionStorage.setItem("paygoTosx_type",type);
    window.sessionStorage.setItem("paygoTosx_state",state);
    window.sessionStorage.setItem("paygoTosx_openId",openId);
    window.sessionStorage.setItem("paygoTosx_payPath",payPath);


    function goFastEmpo(){
        if(!$("#agree").is(':checked')) {
            layer.msg("请先同意《授信协议》");
            return;
        }
        window.location.href = "${basePath!}/register/goFastEmpoUser?isjump=false";
    }

</script>
</body>
</html>