<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>
        <#if type == 'taobaoApprove'>
            淘宝认证
        </#if>
        <#if type == 'didiApprove'>
            滴滴认证
        </#if>
    </title>
    <style>
        body{
            background: #fff;
        }
        .title_text{
            width: 100%;
            padding: 40px 10px;
            text-align: center;
        }
        .title_text>a{
           font-size: 2rem;
        }
        textarea{
            resize:none;
            border: none;
            outline: none;
        }
    </style>
</head>

<body>
<div class="title_text">
    <#--<textarea name="" id="" cols="30" rows="10" readonly></textarea>-->
    <a href="#" class="btn btn-info" id="approve">滴滴认证</a>

    <form action="${basePath!}/user/empo/toDidiDacheJump" id="jumpform" method="get">
        <input type="hidden" value="${url!}" id="urls" name="urls">
    </form>

</div>
</body>

<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/html5shiv.min.js"></script>
<script src="${basePath!}/js/respond.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    <#--$(document).ready(function() {-->
        <#--is_neizhi();-->
    <#--});-->
    <#--function is_neizhi() {-->
        <#--var ua = navigator.userAgent.toLowerCase();-->
        <#--if (ua.match(/MicroMessenger/i) == "micromessenger") {-->
            <#--return "weixin";-->
        <#--} else if (ua.match(/QQ/i) == "qq") {-->
            <#--return "QQ";-->
        <#--} else if (ua.match(/Alipay/i) == "alipay" && payway == 2) {-->
            <#--return "alipay";-->
        <#--}-->
        <#--return false;-->
    <#--}-->
    <#--var isNeizhi = is_neizhi();  //调用上面js判断-->
    <#--var winHeight = typeof window.innerHeight != 'undefined' ? window.innerHeight : document.documentElement.clientHeight;  //网页可视区高度-->
    <#--var weixinTip = $('<div id="weixinTip" style="color:#fff;padding-left:20%;font-size:3rem"><p style="margin-left:30%"><img src="${basePath!}/img/jiantou.png"alt="微信打开"/></p>选择在浏览器中打开</div>');-->

    <#--if(isNeizhi){-->
        <#--$("body").append(weixinTip);-->
    <#--}-->
    <#--$("#weixinTip").css({-->
        <#--"position": "fixed",-->
        <#--"left": "0",-->
        <#--"top": "0",-->
        <#--"height": winHeight,-->
        <#--"width": "100%",-->
        <#--"z-index": "1000",-->
        <#--"background-color": "rgba(0,0,0,0.8)",-->
        <#--"filter": "alpha(opacity=80)",-->
    <#--});-->
    <#--$("#weixinTip p").css({-->
        <#--"text-align": "center",-->
        <#--"margin-top": "10%",-->
        <#--"padding-left": "5%",-->
        <#--"padding-right": "5%"-->
    <#--});-->
    <#--$("#weixinTip p img").css({-->
        <#--"max-width": "100%",-->
        <#--"height": "auto"-->
    <#--});-->

    var url = '${url!}';
    $("#approve").click(function () {
        $("#jumpform").submit();
        <#--window.location.href = "${basePath!}/user/empo/toDidiDacheJump?urls="+url;-->
    });
</script>
</html>