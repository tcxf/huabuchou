<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>授信提额成功</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 10rem;
        }
        .s_content{
            text-align: center;

        }
        .s_content>p{
            color: #333;
            margin-top: 10px;
            font-weight: 600;
        }
        .s_content h6{
            color: #999;
        }
        .tj{
            margin-top: 30%;
            background:#f49110;
            border-radius:30px;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            text-align: center;
        }
        .tj>a{
            text-decoration: none;
            color:#ffffff;
        }
    </style>
</head>
<body>
<div class="top">
    <img src="${basePath!}/img/imgs/pay_ok.png" alt=""/>
</div>
<div class="s_content">
    <p>恭喜您，您已授信提额成功</p>
    <h6>本次提额${subGrade}元，您的授信额度提升至${sumbalance}元</h6>
</div>
<div class="yzm-btn tj">
    <a href="${basePath!}/user/index/Goindex">返回首页</a>
</div>
</body>
</html>