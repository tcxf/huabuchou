<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>授信额度不足</title>
    <style>
        .sxbz{
            margin-top: 40px;
            width: 100%;
            text-align: center;
        }
        .sxbz>img{
            width: 50%;
        }
        .sxbz p{
            font-size: 14px;
            color: #999;
        }
        .ok_btn{
            width: 100%;
            text-align: center;
            position: fixed;
            bottom:50px;
        }
        .btn{
            padding: 10px 40px;
            border: none;
            background: #f49110;
            border-radius: 30px;
            color: #fff;
            outline: none;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="sxbz">
    <img src="${basePath!}/img/sx_sb_gray.png" alt="">
    <p>今日授信总额不足，请明天再试～</p>
</div>
<div class="ok_btn">
    <a class="btn" href="#">确定</a>
    <input type="hidden" id="type" name="type" value="${type!}">
    <input type="hidden" id="state" name="state" value="${state!}" />
    <input type="hidden" id="openId" name="openId" value="${openId!}" />
    <input type="hidden" id="payPath" name="payPath" value="${payPath!}" />
</div>

<script src="${basePath!}/js/jquery-1.11.3.js" type="text/javascript"></script>
<script>

    $(".btn").click(function(){
        var type = $("#type").val();
        if ('onPay' == type) {
            var state = $("#state").val();
            var openId = $("#openId").val();
            var payPath = $("#payPath").val();
            window.location.href= payPath + "pay/goToPaymentOptPage?state="+state+"&openId="+openId+"";
        }else {
            window.location.href= "${basePath!}/user/index/Goindex";
        }

    });
</script>
</body>
</html>