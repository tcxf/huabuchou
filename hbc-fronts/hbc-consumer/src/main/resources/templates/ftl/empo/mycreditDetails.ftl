<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/lettersA.css">
    <title>授信额度</title>
</head>
<body>
<div class="p-top">
    <div id="yuan" align="center">
        <div id="container">
            <div class="p-img">
                <img src="${basePath!}/img/bg_erduqiu3.png" alt=""/>
            </div>
            <div id="wave-one"></div>
            <div id="wave-two"></div>
            <div id="wave-three"></div>
        </div>
        <div class="p-p">
            <p >可用额度(元)</p>
            <h5>${mycreditDetailsDto.balance}</h5>
        </div>
    </div>
</div>
<div class="letters_list">
    <ul>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        授信额度
                    </div>
                    <div class="col-xs-6">
                        ￥${mycreditDetailsDto.rapidMoney}
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        出账时间
                    </div>
                    <div class="col-xs-6">
                        每月${mycreditDetailsDto.ooaDate}号
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        还款时间
                    </div>
                    <div class="col-xs-6">
                        每月${mycreditDetailsDto.repayDate}号
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    var waveOneinc = 0;
    var waveTwoinc = 15;
    var waveThreeinc = 25;
    setInterval(function(){
        $('#wave-one').css('transform', 'rotate('+waveOneinc+'deg)');
        waveOneinc+=1.5;
        waveTwoinc+=1.15;
        waveThreeinc+=1.25;
        $('#wave-two').css('transform', 'rotate('+waveTwoinc+'deg)');
        $('#wave-three').css('transform', 'rotate(-'+waveThreeinc+'deg)');
    }, 20);
</script>
</body>
</html>