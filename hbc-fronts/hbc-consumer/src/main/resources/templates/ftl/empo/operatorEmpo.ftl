<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/loading.css">
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
    <title>运营商授权</title>
    <style>
        body{
            background: #fff;
        }
        .top_head{
            width: 100%;
            height: 40px;
            background: #f49110;
            text-align: center;
            line-height: 40px;
            color: #fff;

        }
        .content_list{
            margin: 30px 0 0 0;
        }
        .content_list>ul{
            padding-left: 0;
        }
        .content_list li{
            list-style: none;
        }
        .content_list .row{
            margin: 10px 0 0 0;
            border-bottom: 1px solid #ededed;
            padding-bottom: 10px;
        }
        .content_list .col-xs-3{
            padding: 0;

        }
        .content_list .col-xs-8{
            padding-left: 0;
        }
        .content_list .col-xs-8>input{
            border: none;
            outline:medium;
            background: transparent;
        }
        .content_list .col-xs-6{
            padding: 0;
            border-right: 1px solid #ededed;
            width: 47%;
        }
        .content_list .col-xs-6>input{
            border: none;
            outline:medium;
            width: 100%;
        }
        .content_list :last-child .col-xs-3{
            padding-left: 10px;
        }
        .content_list .col-xs-3>a{
            font-size: 0.8rem;
            text-decoration: none;
            color: #ff7a00;
        }
        .content_list .row .yzn_a img{
            width: 60px;
            height: 20px;
        }
        .yzn_a{
            width: 70px;
            height: 24px;
            border: 1px solid #ededed;
            text-align: center;
            letter-spacing: 4px;
        }
        .yzn_a>I{
            font-size: 16px;
        }
        #yzm_img{
            display: none;
        }
        .remind_title_1{
            display: none;
        }
        #shuzi{
            display: none;
        }
        #dx_img{
            display: none;
        }
        .gouxuan{
            margin-top: 10px;
            width: 100%;
            padding: 10px;
            font-size: 1.2rem;
        }
        .gouxuan .col-xs-8 span{
            color: #ff7a00;
        }
        .gouxuan .col-xs-4{
            padding: 0;
        }
        .gouxuan .col-xs-4>a{
            color: #999;
            font-weight: 600;
            text-decoration: none;
        }
        input[type=radio],input[type=checkbox]  {
            display: inline-block;
            vertical-align: middle;
            width: 18px;
            height: 18px;
            margin-left: 5px;
            margin-top: -2px;
            -webkit-appearance: none;
            background-color: transparent;
            border: 0;
            outline: 0 !important;
            line-height: 20px;
            color: #d8d8d8;
        }
        input[type=checkbox]:after  {
            content: "";
            display:block;
            width: 18px;
            height: 18px;
            text-align: center;
            line-height: 12px;
            font-size: 14px;
            color: #fff;
            border: 2px solid #ddd;
            background-color: #fff;
            box-sizing:border-box;
        }
        input[type=checkbox]:checked:after  {
            border: 4px solid #ddd;
            background-color: #f49110;
        }

        input[type=checkbox]:checked:after  {
            content: "√";
            border-color: #f49110;
            background-color: #f49110;

        }
        .a-btn{
            width: 90%;
            height: 40px;
            background:#f49110;
            line-height: 40px;
            text-align: center;
            margin:5px auto;
            border-radius: 5px;
            cursor: pointer
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }
        .a-text{
            margin: 20px;
            border-top:1px solid #ededed;
            padding-top: 20px;
            color: #999;
        }
        .modal{
            background: rgba(0,0,0,0.6);


        }
        .modal .content{
            width: 90%;
            height: auto;
            margin: 45% auto;
            background:rgba(255,255,255,0.8);
            border-radius: 5px;
        }
        .header{
            text-align: right;
            padding: 5px
        }
        #closed{
            width: 20px;
        }
        .media-body{
            padding: 20px;
        }
        .media-body span{
            color: #f49110;
        }

        #hyzm-btn{
            border: 1px solid #f49110;
            outline: none;
            padding: 3px 5px;
            color: #f49110;
            background: transparent;
            border-radius: 30px;
            font-size: 12px;
            cursor: pointer;
        }
        .remind_title{
            padding: 5px 28px;
            color: #ff0000;
            font-size: 12px;
            font-weight: 600;
        }
        .remind_title_1{
            padding: 5px 28px;
            color: #ff0000;
            font-size: 12px;
            font-weight: 600;
        }
        .question>img{
            width: 15px;
            height: 15px;
        }
        #closed1{
            width: 20px;

        }
        #loading{
            background: transparent;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .bg_loading{
            width: 50%;
            margin-left: 25%;
            height: 100px;
            background: rgba(0,0,0,.5);
            padding-top: 7px;
            margin-top: 20%;
        }
        .dxfs{
            background: transparent;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .dxfs-con{
            width: 50%;
            margin-left: 25%;
            height: 60px;
            line-height: 60px;
            background: rgba(0,0,0,.5);
            margin-top: 50%;
            color: #fff;
            text-align: center;
        }
        .dxfs1{
            background: transparent;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .dxfs-con1{
            width: 50%;
            margin-left: 25%;
            height: 60px;
            line-height: 60px;
            background: rgba(0,0,0,.5);
            margin-top: 50%;
            color: #fff;
            text-align: center;
        }

    </style>
</head>
<body>
    <div class="top_head">
        运营商授权
    </div>
    <div class="content_list">
        <ul>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-3" style="width: 36%">
                            手&nbsp机&nbsp号&nbsp码&nbsp:
                        </div>
                        <div class="col-xs-8" style="width: 54%">
                            <input type="mobile" placeholder="13988888888" id="mobile" disabled="disabled" value="${mobile!}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3" style="width: 36%">
                            手机服务密码:
                        </div>
                        <div class="col-xs-8" style="width: 54%">
                            <input type="password" placeholder="请输入手机服务密码" id="serviceCode"/>
                        </div>
                        <div class="question col-xs-1" style="padding: 0">
                            <img src="${basePath!}/img/merchant_my_bz.png" alt="">
                        </div>
                    </div>
                    <div class="row"  id="dx_img">
                        <div class="col-xs-3" style="width: 34%">
                            短信验证码:
                        </div>
                        <div class="col-xs-6" id="smsyzm_id" style="width: 40%">
                            <input type="text" placeholder="请输入短信验证码" id="randomPassword"/>
                        </div>
                        <div class="col-xs-3" id="smsyzm">
                            <#--<a href="#" id="getYzm">获取验证码</a>-->
                            <input type="button" id="hyzm-btn" <#--onclick="getycode()"--> value="获取验证码"/>
                        </div>
                    </div>
                    <div class="row"  id="yzm_img">
                        <div class="col-xs-3" style="width: 34%">
                            图片验证码:
                        </div>
                        <div class="col-xs-6" style="width: 40%">
                            <input type="hidden" id="totalScore" name="totalScore" value="${totalScore!}" />
                            <input type="hidden" id="loginResultStatus" name="loginResultStatus" value="" />
                            <input type="hidden" id="type" name="type" value="${type!}" />
                            <input type="hidden" id="state" name="state" value="${state!}" />
                            <input type="hidden" id="openId" name="openId" value="${openId!}" />
                            <input type="hidden" id="payPath" name="payPath" value="${payPath!}" />
                            <input type="text" placeholder="请输入图片验证码" id="code"/>
                        </div>
                        <div class="col-xs-3">
                            <input type="hidden" value="${yzm}" id="yzm"/>
                            <img src="data:image/gif;base64,${icon!}" <#--onclick="refreshCode()"--> id="icon" class="yzn_a">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="remind_title"><span id="shuzi">1.</span>如不知道输入手机服务密码，请点击问号提示！</div>
    <div class="remind_title_1">2.系统已发送短信验证码，如长时间未收到，请点击获取验证码！</div>
    <div class="gouxuan">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8">
                    <input type="checkbox" checked>
                    同意<a href="#mymodalseven" data-toggle="modal" style="color: #008fff;">账户数据收集协议</a>
                </div>
                <div class="col-xs-4">
                    <a href="#" id="forget_password">忘记服务密码?</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal">
        <div class="modal-dialog" id="modal-dialog">
            <div class="content">
                <div class="header">
                    <img src="${basePath!}/img/imgs/icon_delete.png" id="closed" alt="">
                </div>
                <div class="media-body">
                    <p>
                        <b>移动</b>：发送“ <span>CZMM#入网证件号#新密码#新密码</span>”新密码为6位数字，到10086，请确认两次输入密码一致 。  
                    </p>
                    <p>
                        <b>联通</b>：发送“ <span>MMCZ#新密码#证件号后6位</span>”新密码为6为数字，到10010，请确认两次输入密码一致 。  
                    </p>
                    <p>
                        <b>电信</b>：用本机拨打 <span>10001按1-3-1键</span>获取服务密码。
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal1">
        <div class="modal-dialog" id="modal-dialog1">
            <div class="content">
                <div class="header">
                    <img src="${basePath!}/img/imgs/icon_delete.png" id="closed1" alt="">
                </div>
                <div class="media-body">
                    手机个人服务密码是您办理移动通信业务时用于识别您的身份的有效凭证之一。
                    凭个人服务密码，您可查询您的手机通话费、打印详细话单，办理手机挂失、报停等业务。
                    服务密码是客户重要的个人资料，凡使用服务密码办理的一切业务，均视为客户本人或客户本人授权的行为。
                    <span style="color: #f49110">如果您忘记了手机服务密码是什么?请点击下面忘记服务密码</span>
                </div>
            </div>
        </div>
    </div>
    <div class="a-btn" id="tj">
        <a href="#">授权认证</a>
    </div>
    <div class="a-text">
        <p>温馨提示</p>
        <h6>1.请授权本人实名认证手机号码</h6>
        <h6>2.登录成功之后会收到运营商短信,无需理会</h6>
    </div>

    <div id="loading">
        <div class="bg_loading">
            <div class="m-load2">
                <div class="line">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="circlebg"></div>
            </div>
            <div class=jiazai>正在授信中，请耐心等待...</div>
        </div>

    </div>
    <div class="dxfs">
        <div class="dxfs-con">
            短信验证码发送成功
        </div>
    </div>
    <div class="dxfs1">
        <div class="dxfs-con1">
            短信验证码发送失败
        </div>
    </div>


<#include "../agreement/models.ftl">
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <#--<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>-->
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script src="${basePath!}/js/layer/layer.js"></script>
    <#include "../agreement/mune_btn.ftl">
    <script>
        var i = 60;

        $(document).ready(function(){

            stay();

            $("#company").html("${oname!}");

            var yzm = $("#yzm").val();
            var logintag = "${logintag!}";

            var pictag = "${pictag!}";
            var smstag = "${smstag!}";

            if (logintag == 'true'){
                if (yzm.length == 0){
                    $("#dx_img").css("display","block");

                    $("#hyzm-btn").attr("disabled", true);

                    $("#shuzi").css("display","inline-block");

                    $(".remind_title_1").css("display","block");

                    // layer.msg("短信验证码发送成功");
                    $(".dxfs").css("display","block");
                    setTimeout(hideDiv,3000);
                    function hideDiv(){
                        $(".dxfs").css("display","none");
                    }
                    i = 60;
                    startTime();
                    // if (smstag == 'true') {
                    //     $("#smsyzm").css("display","none");
                    // }
                } else{
                    $("#yzm_img").css("display","block");
                    if (smstag == 'true') {
                        $("#dx_img").css("display","block");
                        $("#hyzm-btn").attr("disabled", true);
                        $("#shuzi").css("display","block");
                        $(".remind_title_1").css("display","block");
                        // layer.msg("短信验证码发送成功");
                        $(".dxfs").css("display","block");
                        setTimeout(hideDiv,3000);
                        function hideDiv(){
                            $(".dxfs").css("display","none");
                        }
                        i = 60;
                        startTime();
                        // $("#smsyzm").css("display","none");
                    }
                }
            }else {
                // layer.msg("短信验证码发送失败");
                $(".dxfs1").css("display","block");
                setTimeout(hideDiv,3000);
                function hideDiv(){
                    $(".dxfs1").css("display","none");
                }
                $("#hyzm-btn").val("获取验证码");
                $("#hyzm-btn").attr("disabled", false);
            }
            $("#tj").click(function(){
                var mobile = $("#mobile").val().replace(/\s+/g, "");
                var serviceCode = $("#serviceCode").val().replace(/\s+/g, "");
                var randomPassword = $("#randomPassword").val().replace(/\s+/g, "");
                var code = $("#code").val().replace(/\s+/g, "");
                var totalScore = $("#totalScore").val();
                var type = window.sessionStorage.getItem("paygoTosx_type");
               // var index = layer.load(2, {time: 60*1000});
                $("#loading").css("display","block");
                setTimeout(hideDiv,60000);
                function hideDiv(){
                    $("#loading").css("display","none");
                }
                var loginResultStatus = $("#loginResultStatus").val();
                api.tools.ajax({
                    url:"${basePath!}/user/empo/threeMethod",
                    data:{
                        mobile:mobile,
                        serviceCode:serviceCode,
                        randomPassword:randomPassword,
                        code:code,
                        totalScore:totalScore,
                        loginResultStatus:loginResultStatus
                    }
                },function(d){
               //     layer.close(index);
                    $("#loading").css("display","none");
                    if (d.code == 0)
                    {
                        if ('onPay' == type) {
                            layer.msg("授信成功");
                            var state = window.sessionStorage.getItem("paygoTosx_state");
                            var openId = window.sessionStorage.getItem("paygoTosx_openId");
                            var payPath = window.sessionStorage.getItem("paygoTosx_payPath");
                            window.location.href= payPath + "pay/goToPaymentOptPage?state="+state+"&openId="+openId+"";
                        }else {
                            layer.msg("授信成功");
                            window.location.href = "${basePath!}/consumer/bill/jumpMyCredit";
                        }
                    }else if (d.code == 2) {
                        layer.msg(d.msg);
                        $("#hyzm-btn").val("获取验证码");
                        $("#hyzm-btn").attr("disabled", false);
                        i = 0;
                    }else if (d.code == 1){
                        layer.msg(d.msg);
                        $("#hyzm-btn").val("获取验证码");
                        $("#hyzm-btn").attr("disabled", false);
                        $("#loginResultStatus").val("loginResultStatus");
                        if (d.msg == '需要进行短信验证码二次认证，请输入最新验证码') {
                            $("#dx_img").css("display","block");
                        }else {
                            $('#icon').attr('src', 'data:image/gif;base64,'+ d.data);
                        }
                        i = 0;
                    }else if (d.code == 3){
                        layer.msg(d.msg);
                    }
                });
            });
       });

        $("#hyzm-btn").click(function () {
            var mobile = $("#mobile").val();
            if(mobile==null||mobile==""){
                layer.msg("请输入手机号码");
                return;
            }
            var re = /^1\d{10}$/
            if (!re.test(mobile)) {
                layer.msg("请输入正确的手机号码");
                return;
            }
            $("#hyzm-btn").attr("disabled", true);
            var index = layer.load();
            api.tools.ajax({
                url:"${basePath!}/user/empo/sendVerificationCode",
                data:{
                    mobile:mobile
                },
            },function(d){
                layer.close(index);
                i = 60;
                if (d.code == 0){
                    // layer.msg("短信验证码发送成功");
                    $(".dxfs").css("display","block");
                    setTimeout(hideDiv,3000);
                    function hideDiv(){
                        $(".dxfs").css("display","none");
                    }
                    startTime();
                }else {
                    // layer.msg("短信验证码发送失败");
                    $(".dxfs1").css("display","block");
                    setTimeout(hideDiv,3000);
                    function hideDiv(){
                        $(".dxfs1").css("display","none");
                    }
                    $("#hyzm-btn").val("获取验证码");
                    $("#hyzm-btn").attr("disabled", false);
                }
            });
        });
        <#--function getycode() {-->
            <#--var mobile = $("#mobile").val();-->
            <#--if(mobile==null||mobile==""){-->
                <#--layer.msg("请输入手机号码");-->
                <#--return;-->
            <#--}-->
            <#--var re = /^1\d{10}$/-->
            <#--if (!re.test(mobile)) {-->
                <#--layer.msg("请输入正确的手机号码");-->
                <#--return;-->
            <#--}-->
            <#--$("#hyzm-btn").attr("disabled", true);-->
            <#--var index = layer.load();-->
            <#--api.tools.ajax({-->
                <#--url:"${basePath!}/user/empo/sendVerificationCode",-->
                <#--data:{-->
                    <#--mobile:mobile-->
                <#--},-->
            <#--},function(d){-->
                <#--layer.close(index);-->
                <#--i = 60;-->
                <#--if (d.code == 0){-->
                    <#--layer.msg("短信验证码发送成功");-->
                    <#--startTime();-->
                <#--}else {-->
                    <#--layer.msg("短信验证码发送失败");-->
                    <#--$("#hyzm-btn").val("获取验证码");-->
                    <#--$("#hyzm-btn").attr("disabled", false);-->
                <#--}-->
            <#--});-->
        <#--}-->

        function startTime(){
            $("#hyzm-btn").val("重新发送("+i+"秒)");
            if(i > 0){
                i--;
                setTimeout(function(){
                    startTime();
                },1000);
            }else{
                $("#hyzm-btn").val("获取验证码");
                $("#hyzm-btn").attr("disabled", false);
                i = 60;
            }
        }

        $("#forget_password").click(function () {
            document.getElementById("mymodal").style.display="block";
        });
        $("#closed").click(function () {
            document.getElementById("mymodal").style.display="none";
        });
        $(".question").click(function () {
            document.getElementById("mymodal1").style.display="block";

        });
        $("#closed1").click(function () {
            document.getElementById("mymodal1").style.display="none";
        });
        //刷新图片验证码

        $("#icon").click(function () {
            var mobile = $("#mobile").val();
            api.tools.ajax({
                url:"${basePath!}/user/empo/refreshLoginSmsCode",
                data:{
                    mobile:mobile
                },
            },function(d){
                if (d.code == 0){
                    $('#icon').attr('src', 'data:image/gif;base64,'+ d.data);
                }
            });
        });
        /*function refreshCode() {
            var mobile = $("#mobile").val();
            api.tools.ajax({
                url:"${basePath!}/user/empo/refreshLoginSmsCode",
                data:{
                    mobile:mobile
                },
            },function(d){
                if (d.code == 0){
                    $('#icon').attr('src', 'data:image/gif;base64,'+ d.data);
                }
            });
        }*/

        var pageOpen = 1800;

        function stay() {
            var mobile = $("#mobile").val();
            var totalScore = $("#totalScore").val();
            if(pageOpen > 0){
                pageOpen--;
                setTimeout(function(){
                    stay();
                },1000);
            }else{
                window.location.href = "${basePath!}/user/empo/goOperatorEmpo?mobile="+mobile+"&totalScore="+totalScore;
            }
        }
    </script>
</body>
</html>