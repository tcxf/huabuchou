<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>QQ-微信</title>
    <style>
        ::-webkit-input-placeholder{
            color: #dadada;
        }
    </style>
</head>
<body>
    <div class="linkman">
        <p class="tow_text">QQ/微信
            <span>(请填写其中一个)</span>
        </p>
        <ul>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-5">
                            QQ号
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="18181888" id="qq">
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-5">
                            微信号
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="请输入微信号码" id="weChat">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="submit_btn" id="submit">
        <a href="#">保存</a>
    </div>
    <script src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script type="text/javascript" src="${basePath!}/template/template.js"></script>
    <#include "../agreement/mune_btn.ftl">
    <script>
        $(document).ready(function() {
            $("#submit").click(function() {
                var qq = $("#qq").val();
                var weChat = $("#weChat").val();
                var regQq=/^[1-9]\d{4,9}$/;
                var regWx= /^[-_a-zA-Z0-9]{5,19}$/
                if ((qq == "" || qq == null)&&(weChat == ""|| weChat == null))
                {
                    api.tools.toast("请填写任意一项");
                    return;

                }
                if(!(regQq.test(qq)) && !(regWx.test(weChat))){
                    api.tools.toast("请输入正确的号码");
                    return;
                }

                $(this).html('提交中...');
                $.ajax({
                    url:"${basePath!}/user/empo/saveQQOrWeChat",
                    type:'POST', //GET
                    data:{
                        qq: qq,
                        weChat: weChat
                    },
                    success:function(d){
                        $(this).html('完成');
                        if (d.code == 0)
                        {
                            api.tools.toast("保存成功");
                            window.location.href = "${basePath!}/user/empo/goEmpoPlus";
                        }else{
                            api.tools.toast("保存失败");
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>