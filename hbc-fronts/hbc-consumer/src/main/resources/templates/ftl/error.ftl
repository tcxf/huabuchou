<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/404.css">
    <title>错误信息</title>
</head>
<body>
<div class="errors">
    <div class="errors_img">
        <img src="${basePath!}/img/404_img.png" alt="">
    </div>
    <div class="errors_content">${errmsg!}</div>
</div>
</body>
</html>