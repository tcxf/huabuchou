<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>快速授信</title>
    <style>
        body{
            background: #fff;
        }
        .content_list{
            width: 100%;
            height: auto;
            background:#fff;
            box-shadow: 0 0 5px #dedede;
        }
        .content_list .row{
            border-bottom: 1px solid #ededed;
            padding: 13px;
        }
        .content_list .row:last-child{
            border: none;
        }
        .content_list .col-xs-4{
            padding-left: 0;
        }
        .content_list .col-xs-8{
            padding-left: 0;
        }

        .content_list .col-xs-8>input{
            border: none;
            outline:medium;
        }
        .text_a{
            padding: 30px;
            color: #999;
        }
        .text_a>span{
            color: #f90003;
        }
        .a-btn{
            width: 94%;
            height: 40px;
            background:#f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="content_list">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                真实姓名
            </div>
            <div class="col-xs-8">
                <input type="text" id="realName" placeholder="请输入真实姓名" value="彭进"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                身份证号
            </div>
            <div class="col-xs-8">
                <input type="text" id="cardNo" placeholder="请输入您的身份证号码" value="360428199411185312"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                手机号码
            </div>
            <div class="col-xs-8">
                <input id="mobile" type="text" placeholder="请输入您的手机号码" value="15802653507"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                银行卡号
            </div>
            <div class="col-xs-8">
                <input type="text" id="bankcard" placeholder="请输入您的银行卡号" value="6222600640016972722"/>
            </div>
        </div>
    </div>
</div>
<div class="text_a">
    尊敬的用户,您正在进行授信个人信息填写,请您仔细核对信息是否准确无误,如信息有误系统将直接拒绝授信!
</div>
<div class="a-btn">
    <a href="#" id="shouxin">快速授信</a>
</div>
</body>

    <script src="${basePath!}/js/jquery-1.11.3.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>

<script >
	$(document).ready(function(){
		$("#shouxin").click(function(){
			var mobile = $("#mobile").val();
			var userName = $("#realName").val();
			var accountNO=$("#bankcard").val();
			var idCard =$("#cardNo").val();
			var reg = /^[\u4e00-\u9fa5]{2,4}$/i; 
		    if (!reg.test(userName)) {
		    	api.tools.toast("请输入真实姓名，只能是2-4个汉字！");
		    	return ;
		    }
		    if(idCard==""){
		    	api.tools.toast("身份证不能为空，请重填");  
		        return;
		    }
		    var regs = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
		    if(!regs.test(idCard)){
		    	api.tools.toast("您输入的身份证号码不正确，请重填");  
		        return;
		    } 
		    if (!(/^1[34578]\d{9}$/.test(mobile))){
		        api.tools.toast("手机号码有误，请重填");  
		        return; 
		    }
		    if(accountNO==""){
		    	api.tools.toast("银行卡号码不能为空，请重填");  
		        return;
		    }
			api.tools.ajax({
				url:"${basePath!}/user/empo/fastEmpo",
				data:{
					mobile:mobile,
                    userName:userName,
                    accountNO:accountNO,
                    idCard:idCard
				},
			},function(d){
				api.tools.toast(d.msg);
				if(d.code == 0){
                    window.location.href = "${basePath!}/user/empo/goOperatorEmpo?mobile="+mobile+"&&totalScore="+d.data;
				}else if (d.data == 1 ) {
                    api.tools.toast(d.msg);
				}else {
                    api.tools.toast(d.msg);
                    window.location.href = "${basePath!}/user/empo/fastEmpoFailed";
                }
			});
		});
	});
</script>
</html>