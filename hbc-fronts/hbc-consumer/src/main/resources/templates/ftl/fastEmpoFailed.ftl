<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title></title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 35%;
        }
         .top p{
             margin-top: 10px;
             color: #666;
            }
         .n_text{
             text-align: center;
             margin-top: 20%;
         }
        .n_text>p{
            color: #999;
        }
        .s_content>h3{
            color: #999999;
        }
        .tj{
            background: #f49110;
            color:#ffffff;
            border-radius:30px;
            font-size: 0.8rem;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            text-align: center;
            margin-top: 20px;
        }
    </style>

</head>
<body>
<div class="top">
    <img src="${basePath!}/img/imgs/ico_sh_no.png" alt=""/>
    <p>审核不通过...</p>
</div>
<div class="n_text">
    <p>信息不符合请重新填写提交</p>
</div>
<div class="yzm-btn tj" href="${basePath!}/user/index/Goindex">
    返回首页
</div>
</body>
</html>