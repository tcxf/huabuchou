<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
        <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
	    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
		<link rel="stylesheet" href="${basePath!}/css/y.css"/>
		<link rel="stylesheet" href="${basePath!}/css/zybl.css"/>
		<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
	    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>   
    <title>更多分类</title>
    <style>
           body{
            background: #eee;
        }
        .p-zjds{
            margin-top: 10%;
            margin-left: 3%;
        }
        .p-zjds>img{
            width: 20px;
            height: 20px;
        }
        .p-qbfl{
            margin-left: 5%;
            width: 90%;
            margin-top: 2%;
            background: #fff;
        }
        .p-qbfl li{
            float: left;
            width: 25%;
           padding: 10px 5px 10px 5px;
            text-align: center;
            border-bottom: 1px solid #dadada;
             border-right: 1px solid #dadada;
            background: #fff;
        }
        .p-qbfl a{
            color: #999;
        }
       .p-qbfl ul:last-child{
            border-bottom: 0;
        }
    
    </style>
</head>
<body>
    <div class="p-zjds">
        <img src="${basePath!}/img/imgs/housekeeping.png" alt=""/>
        <span>热门</span>
    </div>
    <div class="p-qbfl">
  <ul class="p-last-li">
            
        </ul>
      
    </div>
  
    
</body>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
   <#include "./agreement/mune_btn.ftl">
  <script >
	$(document).ready(function() {
		loadfindNatures();
		function loadfindNatures(){
			api.tools.ajax({
				url:'${basePath!}/merchants/Querymerchantcategory',
				data:{
				}
			},function(d){
				if(d.data!= null ){
					var html = template.load({
							host : '${basePath!}/',
							name : "a_findNatures_list",
							data:{
							    data : d.data
							}
						});
						$html = $(html);
						$('.p-last-li').append($html);
						var nature=null;
						$(".p-last-li li").click(function(){
							nature=	$(this).children("span").html();  
							name = $(this).children("a").html();
							//if(name=="酒店"){
								//window.location='/rummery/index.htm';
							//}else{
								window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature;
								
							//}
						
						});
				}
		});
		}
		
	
		
	});
  
  
  
  </script>
</html>
