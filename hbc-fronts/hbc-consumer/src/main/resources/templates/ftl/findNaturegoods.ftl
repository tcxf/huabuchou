
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css" />
    <link rel="stylesheet" href="${basePath!}/css/y.css" />
    <link rel="stylesheet" href="${basePath!}/css/zybl.css" />
    <link rel="stylesheet" href="${basePath!}/css/footer.css"/>

    <title>附近商家</title>
    <style>
        .lr_nb{height:40px;line-height:40px;width:100%;position:absolute;background:#fff;padding: 0px 10px;box-sizing:border-box;z-index:1;max-width:1080px;opacity:1;-webkit-transition:.3s all;transition:.3s all}
        .lr_nb .slider_wrap.line{overflow:hidden;overflow-x:scroll;width:100%;-webkit-overflow-scrolling:touch;}
        .lr_nb .slider_wrap.line .item_cell{display:inline-block;margin: 0px 10px;;overflow:hidden;position:relative;}
        .lr_nb .slider_wrap.box{overflow:hidden;width:100%}
        .lr_nb .slider_wrap::-webkit-scrollbar{display:none}
        .lr_nb .wx_items{white-space:nowrap}
        .lr_nb .wx_items span{color:#666;font-size: 15px; white-space:nowrap;display:block;-webkit-tap-highlight-color:rgba(0,0,0,0);text-align:center;cursor:pointer}
        .lr_nb .wx_items .current span,.lr_nb .wx_items .current a:visited,.lr_nb .wx_items .current a:link,.lr_nb .wx_items .current a:hover,.lr_nb .wx_items .current a:focus{color:#4fbeab;}
        .lr_nb_after{height:40px;display:block;clear:both;}
        .lr_nb .active{
            color: #ff7a00;
            border-bottom: 2px solid #ff7a00;
        }
        .lr_nb .active a{
            color: #ff7a00;
        }
        .lr_nb a{
            color: #333;
            text-decoration: none;
            font-size:1rem
        }

        .container .col-xs-4>img{
            width:11px
        }
        .m-sou>img {
            width: 8%;
            margin-left: 2%;
        }
        @media (min-width: 375px) and (max-width: 979px){
            .m-sou>img {
                width: 5%;
                margin-left: 2%;
            }
        }
        @media (min-width: 768px) and (max-width: 979px){
            .container .col-xs-4>img{
                width:9%
            }
            .m-sou>img {
                width: 4%;
                margin-left: 7%;
            }
        }
        @media (min-width:979px) and (max-width: 1200px){
            .container .col-xs-4>img{
                width:10px
            }
            .m-sou>img {
                width: 3%;
                margin-left: 7%;
            }
            #scan>img {
                width: 14%;
            }
        }
        .c_shop .shop_left p {
            font-size: 0.8em;
            color: #333;
            margin-top: 0;
        }
        .c_shop h3{
            margin-top: 5px;
        }
        .fjsj2 a{
            color: #999;
        }

        #ti-ul .active a{
            color:#333;
        }

        .content_top{
            background: url(${basePath!}/img/banner_05.png)no-repeat;
            background-size: cover;
            height: 120px;
            border-radius: 5px;
            width: 94%;
            margin-left: 3%;
            margin-top: 5px;
        }
        .m_content{
            padding-bottom: 50px;
        }
        #div{
            margin-bottom: 80px;
            width: 100%;
            text-align: center;
            clear: both;
        }
        #dianji{
            color: #999;
            cursor: pointer
        }
    </style>
</head>
<body>
<div class="content_top">
    <div class="container">
        <div class="row" style="padding: 5px;">
            <div class="col-xs-4">
                <img src="${basePath!}/img/imgs/map.png" alt="" /> <a
                    href="#" style="color: #ffffff;" id="location"></a>
            </div>
            <a href="${basePath!}/merchantQuery/Queryhotsearch" >
                <div class="col-xs-8 m-sou"
                     style="width:68%;margin-left: -1rem;
					    height: 1.7rem;
					    border:1px solid #ffffff;
					    border-radius: 16px;
					    background: #ffffff; ">
                    <img src="${basePath!}/img/imgs/search.png" alt="" />
                    <span  style="color: #999999">搜一搜...</span>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="lr_nb">
    <div class="slider_wrap line">
        <div class="wx_items" id="p-last-li" >
            <div class="item_cell active">
                <span><a href="#" id="span" data="${micId!}">全部</a></span>
            </div>

        </div>
    </div>
</div>

<div class="fjsj2" style="margin-top: 45px">
    <div class="m_list tab-pane active" id="foot">

    </div>
</div>
<div class="m_content tab-content">
    <div class="c_shop " id="shop">
        <ul>

        </ul>
    </div>


</div>
<div id="div">
    <a href="javascript:void(0)" id="dianji">点击查看更多</a>
</div>
<div class="footer" id="footer">
    <ul>
        <li>
            <a href="${basePath!}/user/index/Goindex">
                <div class="toOne"></div>
                <p>首页</p>
            </a>
        </li>
        <li class="active">
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                <div class="toTow"></div>
                <p>附近商家</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMyCredit">
                <div class="toThree"></div>
                <p>授信</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/order/order" class ="redirect">
                <div class="toFou"></div>
                <p>订单</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMine">
                <div class="toFive"></div>
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
</body>


<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.cookie.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        var page = 1;
        var lat='null';
        var lng='null';
        var types ='null';
        var nature ='null';
        var micId = $("#span").attr('data');
        if(micId !=null){
            nature=micId ;
        }
        function location(){
            api.tools.location(function(d){
                lat = d.lat;
                lng = d.lng;
                $("#location").html(d.address.city);
                loadfindNatures();
                loadMerchants(micId);
            },function(){
                $("#location").html('定位失败');
                loadfindNatures();
                loadMerchants(micId);
            });
        }
        $("#location").click(function(){
            api.tools.location(function(d){
                lat = d.lat;
                lng = d.lng;
                $("#location").html(d.address.city);
            },function(){
                $("#location").html('定位失败');
            });
        });

        $("#fenlei li").click(function(){
            nature=	$(this).children("span").html();
            loadMerchants();

        });

        $("#ti-ul li").click(function(){
            var it=$(this).index();
            if(it==0){
                types=null;
                page = 1;
                loadfindNatures();
            }
            if(it==1){
                types=it;
                page = 1;
                loadfindNatures();
            }
            if(it==2){

                types=it;
                page = 1;
                loadfindNatures();
            }if(it==3){

                types=it;
                page = 1;
                loadfindNatures();
            }
        });

        // $("#span").click(function () {
        //     $("#shop").find("ul").empty();
        //     loadMerchants('null');
        // })
        location();
        $("#location").click(function(){
            location();
        });

        function loadfindNatures(){
            api.tools.ajax({
                url:'${basePath!}/merchants/Querymerchantcategory',
                data:{
                }
            },function(d){
                if(d.data!= null ){
                    var html = template.load({
                        host : '${basePath!}/',
                        name : "findNatures_list",
                        data:{
                            data : d.data
                        }
                    });
                    $html = $(html);
                    $('#p-last-li').append($html);
                    var firstChild=$(".item_cell:first");//用选择器的方式获取第一个子元素
                    if(nature==null){
                        firstChild.addClass('active');
                    }
                    if(nature!=null){
                        var mm ='${micId!}';
                        if(mm!=null && mm!="null" ){
                            firstChild.removeClass('active');
                        }

                        $("#"+nature).addClass('active');
                    }

                    $("#p-last-li div").click(function(){
                        $(".item_cell").removeClass('active');
                        $(this).addClass('active');
                        micId = $(this).children("p").html();
                        $("#shop").find("ul").empty();
                        page = 1;
                        loadMerchants($(this).children("p").html());
                    });
                }
            });
        }
        function loadMerchants(micId){
            api.tools.ajax({
                url:'${basePath!}/nearby/findList',
                data:{
                    lat:lat,
                    lng:lng,
                    page:page,
                    micId:micId
                }
            },function(d){
                var data=d.data.records;
                if(data.length<10){
                    $("#div").hide();
                }
                if(data.length>=10){
                    $("#div").show();
                }
                if(data!= null    && data.length > 0){
                    for(var i = 0 ; i < data.length ; i++){
                        var data1 = data[i];
                        data1.path = '${basePath!}';
                        if(data1.fullMoney==0 && data1.money==0){
                            data1.coupon='';
                        }else {
                            data1.coupon = '满' + data1.fullMoney + '减' + data1.money + '元';
                        }
                        if(data1.localPhoto!=null) {
                            var img = data1.localPhoto.substring((data1.localPhoto.length) - 3);
                            if (img == 'jpg') {
                                data1.img = "_65x65.jpg";
                            } else if (img == 'gif') {
                                data1.img = "_65x65.gif";
                            } else if (img == 'bmp') {
                                data1.img = "_65x65.bmp";
                            } else if (img == 'png') {
                                data1.img = "_65x65.png";
                            } else if (img == 'peg') {
                                data1.img = "_65x65.peg";
                            } else {
                                var img1 = data1.localPhoto.substring((data1.localPhoto.length) - 4);
                                if (img1 == 'jpeg') {
                                    data1.img = "_65x65.jpeg";
                                } else {
                                    data1.img = "";
                                }
                            }
                        }else{
                            data1.img = "";
                        }
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "index_merchant_list",
                            data : data1
                        });
                        $html = $(html);
                        $('#shop').find('ul').append($html);
                        $html.find('.detail').click(function(){

                            window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');


                        });
                    }
                }else{
                    if(page==1) {
                        $('#shop').find("ul").html('<div class="content" align="center">' +
                                '<img src="${basePath!}/img/img/icon_zanwujilu.png" alt=""/>' +
                                '<h5>暂无记录...</h5>' +
                                '</div>');
                    }
                }


            });
        }

        $("#dianji").click(function(){
            page = page+1;
            loadMerchants(micId);
        });


    });


</script>
</html>