<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="<%=path %>css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=path %>css/Font-Awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<%=path %>css/app.css" />
		<link rel="stylesheet" href="<%=path %>js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path %>js/layer_mobile/need/layer.css" />
		
		<title>忘记密码</title>
	    <style>
	        body,
	        html {
	            background: #eee;
	            height: 100%;
	        }
	
	        .layui-m-layer {
	            z-index: 0 !important;
	        }
	        
	        .tj{
	        	background: #ff7a00; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 95%; margin: 0 auto; margin-top: 50px;
	        }
	        
	        .disabled{
	        	background:#ccc;
	        }
	    </style>
	</head>

	<body>
		<div class="c-content login-model" style="margin-top: 0px">
		    <div class="webkit-box ">
		        <div style="line-height: 30px; width: 11%;border-right: 1px solid #999999">
		            <img src="<%=path %>/img/images/icon_sj.png" />
		        </div>
		        <div class="wkt-flex">
		            <input type="tel" id="mobile" name="mobile" class="form-control reg-input" placeholder="请输入账号" style="width: 100%; height: 30px; padding-left: 10px; font-size: 14px;">
		        </div>
		        <div style="line-height: 30px;" class="clear-input">
		            <img src="<%=path %>/img/images/icon_delete.png" />
		        </div>
		    </div>
		
		    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
		    <div class="webkit-box ">
		        <div style="line-height: 30px; width: 11%;margin-top: 0.6em;border-right: 1px solid #999999">
		            <img src="<%=path %>/img/images/icon_messges.png" />
		        </div>
		        <div class="wkt-flex" style="padding-top: 5px">
		            <input type="tel" id="yCode" name="yCode" class="form-control reg-input" placeholder="请输入短信验证码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 14px;">
		        </div>
		        <div style="margin-top: 5%">
		            <a class="btn btn-default sms-btn" style=" font-size: 0.8em; padding: 5px;"href="#" role="button">发送验证码</a>
		        </div>
		    </div>
		</div>
		<div class="yzm-btn tj" style="">
		    下一步
		</div>
	</body>
	<script type="text/javascript" src="<%=path %>js/jquery.js"></script>
	<script type="text/javascript" src="<%=path %>js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path %>js/info.js"></script>
	<script>
		$(document).ready(function() {

			$(".clear-input").click(function(){
				$(this).parent().find("input").val("");
				$(this).parent().find("input").trigger("keyup");
				$(this).parent().find("input").focus();
			});
			
			$(".sms-btn").click(function(){
				if($("#mobile").val().trim().length==0){
					api.tools.toast("手机号码不能为空");
					 return;
				}
				if(!(/^1[34578]\d{9}$/.test($("#mobile").val()))){ 
			        api.tools.toast("手机号码有误，请重填");  
			        return ; 
			    } 
				
				if($(this).hasClass("disabled")) return;
				$(".sms-btn").addClass("disabled");
				
				
// 				发送短信验证码
				api.tools.ajax({
					url:"<%=path%>/system/getValiCode.htm",
					data:{
						mobile:$("#mobile").val(),
						type:1
					}
				},function(d){
					api.tools.toast(d.resultMsg);
					if(d.resultCode != 1){
						$(".sms-btn").removeClass("disabled");
					}else{
						startTime();
					}
				});
			});
			var i = 60;
			function startTime(){
				$(".sms-btn").html("获取验证码("+i+"秒)");
				if(i > 0){
					i--;
					setTimeout(function(){
						startTime();
					},1000);
				}else{
					$(".sms-btn").html("获取验证码");
					$(".sms-btn").removeClass("disabled");
					i = 60;
				}
			}
			
			$(".tj").click(function(){
				if($(this).hasClass("disabled")) return;
				if($("#mobile").val().trim().length==0){
					api.tools.toast("手机号码不能为空");
					 return;
				}
				if(!(/^1[34578]\d{9}$/.test($("#mobile").val()))){ 
			        api.tools.toast("手机号码有误，请重填");  
			        return ; 
			    } 
				if($("#yCode").val().trim().length==0){
					api.tools.toast("验证码不能为空");
					 return;
				}
				$this = $(this);
				$this.addClass("disabled");
				$this.html("处理中...");
				$.ajax({
					type:"post",
					url:"<%=path%>/userInfoWx/updatePwd.htm",
					data:"yCode="+$("#yCode").val()+"&mobile="+$("#mobile").val(),
					success:function(msg){
						if(msg.resultCode == "1"){
	           		     	window.location.href = "<%=path%>/updatePwd.htm?mobile="+$('#mobile').val();
                         	return;
	           			 }else{
	           				api.tools.toast(msg.resultMsg);
	           				$this.removeClass("disabled");
	           				$this.html("下一步");
	           				return;
	           			 }
					},dataType:"json"
				});
			});
		});
	</script>

</html>
