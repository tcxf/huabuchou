<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	    <link rel="stylesheet" href="<%=path %>/css/bootstrap/css/bootstrap.css" />
	    <link rel="stylesheet" href="<%=path %>/css/zy.css"/>
	    <link rel="stylesheet" href="<%=path %>/css/Font-Awesome/css/font-awesome.min.css" />
	    <link rel="stylesheet" href="<%=path %>/css/app.css" />
	    <link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
		

		<title>商品详情</title>
		   <style>
		        #xq-1{
		            background: #ffffff;
		            padding-top: 12px;
		            padding-left: 10px;
		        }
		        #top-yh-1{
		            padding-top: 20px;
		            line-height: 30px;
		        }
		        .modal {
		            position: fixed;
		            top: 10rem;
		            right: 0;
		            bottom: 0;
		            left: 0;
		            z-index: 1050;
		        }
		        .hk-money {
		            margin-top: 0px;
		            background: #ffffff;
		            color: #fff;
		            text-align: center;
		            height: 72px;
		            padding-top: 0;
		        }
		        .hk-money div {
		            margin-top: 1px;
		            font-size: 32px;
		            font-weight: 400;
		        }
		        .row{
		            margin-right: 0;
		        }
		        #btn-01{
		            margin-top: 10px;
		
		        }
	    </style>
	</head>

	<body>
		<div class="c-content" style="margin-top: 0px;padding: 0px">
		    <div class="wkt-flex">
		        <div>
		            <img src="${res}${g.img}" style="width: 100%;height:300px;" alt=""/>
		        </div>
		    </div>
		</div>
		<div class="row" id="xq-1">
		    <div class="col-xs-8">
		        <div style="font-size: 18px">${g.name}</div>
		        <p style="color: #fb4e44 ;margin-top: 19px;font-size: 18px">¥${g.salePrice}</p>
		        <s style="color: #666666;margin-top: 10px">原价：${g.marketPrice }</s>
		    </div>
		    <div class="text-right">
		        <p style="margin-top: 70px;margin-right: 3%">销量：${g.saleCount}</p>
		    </div>
		</div>
		
		<!--<div class="container">-->
	    <div class="modal" id="mymodal1" data-backdrop="static">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <div class=" hk-money">
	                        <div class="row" id="spt">
	                            <div class="col-xs-3">
	                                <img src="${res}${g.img}"  style="width: 70px;height: 70px"alt=""/>
	                            </div>
	                            <div class="col-xs-8">
	                                <p >${g.name}</p>
	                                <span style="color: #fe2504">￥${g.salePrice}</span><s style="font-size: 14px;color: #d0d0d0"> 原价￥${g.marketPrice}</s>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="modal-body">
	                    <div class="webkit-box">
	                        <div class="wkt-flex text-left" style="line-height:50px;">
								 数量：
	                        </div>
	                        <div class="text-right" id="btn-01">
	                            <div class="btn-toolbar">
	                                <div class="btn-group">
	                                    <button type="button" class="btn btn-default sub">-</button>
	                                    <button type="button" class="btn btn-default num">1</button>
	                                    <button type="button" class="btn btn-default add">+</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-warning btn-block sure-sel" data-dismiss="modal">确定</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="c-content" style="margin-top: 10px;">
	        <div class="webkit-box">
	            <div class="wkt-flex">
	                <a href="#mymodal1" data-toggle="modal" style="color: #333333">已选</a>
	            </div>
	            <div class="wkt-flex text-right">
	                <a href="#mymodal1" class="yxnum" data-toggle="modal" style="color: #333333">1件</a>
	            </div>
	        </div>
	    </div>
		<div style="margin-bottom:55px">
		    <div class="row" id="top-yh">
		        <div class="col-xs-3" style="margin-left: 5%">
		            <img src="${res}${m.localPhoto}" style="width: 60px;height: 60px " alt=""/>
		        </div>
		        <div class="col-xs-8" id="top-yh-1">
		            <div style="font-size: 16px">${m.simpleName}</div>
		            <p style="font-size: 12px">${m.address}</p>
		        </div>
		    </div>
		</div>
		
		<div class="yzm-btn tj" style="background: #ff800f; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 100%; margin:0px auto; position: fixed;bottom: 0">
		    立即购买
		</div>
	</body>
    <script type="text/javascript" src="<%=path %>/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
	<script type="text/javascript" src="<%=path %>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
	<script type="text/javascript" src="<%=path %>/template/template.js"></script>
	<script>
		$(document).ready(function() {
			var num = 1;
			$(".add").click(function(){
				num++;
				$('.num').html(num);
			});
			
			$(".sub").click(function(){
				if(num > 1) num--;
				$('.num').html(num);
			});
			
			$(".sure-sel").click(function(){
				$('.yxnum').html(num);
			});
			
			$(".tj").click(function(){
				<c:if test="${g.isOutStore}">api.tools.toast('抱歉，商品已售罄');</c:if>
				<c:if test="${!g.isOutStore}">window.location='<%=path%>/orderInfo/confirmOrder.htm?id=${g.id}&num='+num;</c:if>
			});
		});
	</script>

</html>