<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <link rel="stylesheet" href="${basePath}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath}/css/app.css" />
    <link rel="stylesheet" href="<%=path%>/css/zybl.css" />
    <link rel="stylesheet" href="${basePath}/js/mui/css/mui.min.css" />
    <script src="${basePath}/js/html5shiv.min.js"></script>
    <script src="${basePath}/js/respond.min.js"></script>
    <title>商品详情</title>
    <style>
        .c-content{
            margin-top: 0;
            padding: 10px 8px 10px 10px;
        }
        .webkit-box img {
            width: 38%;
        }
        .p-tex{
            background: #fff;
            padding-top: 3%;
        }
        .t-text{
            margin-left:3%;
        }
        .t-text>h4{
            font-size: 1.2rem;
        }
        .t-text p{
            color: red;
            padding-top: 13%;
        }
        .t-text img{
            margin-top: -1rem;
        }
        .text-right{
            margin-right: 3%;
        }
        .text-right p{
            margin-top: 1.9rem;
            font-size: 0.8rem;
        }
      
        .m_tab a:hover{
            color: #fd6c02 !important;
        }
        .m_tab a{
            color: #666666;
        }
        .m_tab .active {
            border-bottom: 2px solid #fd6c02;
            color: #fd6c02;
        }
        .thing_left p{
            line-height: 14px;
        }
        .c_thing ul li{
            padding: 10px 10px 2px 10px ;
        }
        .c_thing .thing_right {
            float: right;
            width: 30%;
            font-size: 0.9em;
            text-align: right;
            color: #666;
            padding-top: 12px;
        }
        .yhq_info .yhq_r h4 {
            color: #000;
            font-size: 1em;
        }
        .yhq_info .yhq_r p {
            color: #999;
            font-size: 0.7em;
            line-height: 32px;
        }

        .c_thing .thing_left img {
            float: left;
            margin-right: 10px;
            width: 60px;
            height: 60px;
        }
        .c_shop .shop_left img {
            float: left;
            margin-right: 10px;
            width: 60px;
            height: 60px;
        }
        .c_shop ul li {
            padding: 10px 10px 0 10px;
            border-bottom: 1px solid #eee;
            height: auto;
            overflow: hidden;
        }
        .c_yhq ul li {
            padding: 2px 0px;
            height: auto;
            overflow: hidden;
        }

        .hk-money div {
            margin-top: 1px;
            font-size: 32px;
            font-weight: 400;
        }
        .t-text1{
         margin-left:-10px;
     }
        .sj-ul li {
            padding: 4px 0px 0px 10px;
            position: relative;
        }
        .modal {
            position: fixed;
            top: 36%;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
        }
        .hk-money {
            margin-top: 0px;
            background: #ffffff;
            color: #fff;
            text-align: center;
            height: 72px;
            padding-top: 0;
        }
        .hk-money div {
            margin-top: 1px;
            font-size: 32px;
            font-weight: 400;
        }
           .scroll{
            position: relative;
            margin: 0 auto;
            overflow: hidden;

        }
        ul{
            margin: 0;
        }

        dl{
            background:rgba(0,0,0,0.7);
            position: fixed;
            right: 0;
            bottom: 50px;
            width: 300px;
            height: 70px;
            font-size: 0;
            transition: all 1s;
            border-radius: 8px;
        }
        dl.colse{
            right: -300px;
        }
        #bbb>img{
            width: 40px;
            height: 40px;
        }
        dl dt{
            position: absolute;
            top: 16px;
            left: -45px;
            width: 50px;
            height: 30px;
            line-height: 30px;
            font-size: 12px;
            text-align: center;
        }
        dl dd{
            display: inline-block;
            margin: 0;
            padding:0;
            width: 25%;
            font-size: 12px;
            text-align: center;
        }
        dl dd img{
            width: 25px;
            padding-top: 15px;
        }
        dl dd p{
            font-size: 0.8rem;
            color: #fff;
        }
    </style>
</head>
<body>
<div class="c-content" style="margin-top: 0px;padding: 0px;">
    <div class="wkt-flex">
        <div>
            <img src="#" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" style="width: 100%;height:250px" alt=""/>
        </div>
    </div>
</div>
<div class="webkit-box p-tex">
    <div class="wkt-flex t-text">
        <h4>${g.name}</h4>
        评分
        <div class="star star4" style="margin-left: 12%;margin-top: -1.4rem"></div>
        <p style="font-size: 1.4rem"> <span style="font-size: small">￥</span> ${g.salePrice}<span> <s style="color: #999;font-size: small">原价${g.marketPrice }</s></span></p>

    </div>
    <div class="wkt-flex text-right">
    	<c:if test="${co!=null}">
    		<img src="${basePath}/img/imgs/icon_star_yes.png" id="collect"  style="width: 20px; height:20px";alt=""/>
    		<p style="margin-top: 0rem;margin-right: -2%;color: #fe8a13;line-height:12px">取消<br/>收藏</p>
    	</c:if>
    	<c:if test="${co==null}">
    		<img src="${basePath}/img/imgs/icon_star_no.png" id="collect"  style="width: 20px; height:20px" alt=""/>
	        <p style="margin-top: 0rem;margin-right:-2%;color: #fe8a13">收藏</p>
    	</c:if>
        
        <p>已销：${g.saleCount}</p>
    </div>
</div>
<div class="c-content" style="margin-top: 2%">
   <ul class="sj-ul">
       <li style="padding-left:0">
           <div class="webkit-box">
               <div class="wkt-flex" style="font-size: 1.1rem">
                   商家信息
               </div>
           </div>
       </li>
       <li>
           <div class="webkit-box p-tex">
               <div class="wkt-flex t-text1">
                   <p style="font-size: 1.2rem;color: #000;">${m.simpleName}</p>
                   <div style="margin-top: 0.6rem">${m.address}</div>
                   <div style="margin-top:5px">
                   		 <img src="${basePath}/img/imgs/2.png" style="width: 10%;margin-top:-6px" alt=""/>
                   <span >${dis.distanceStr }</span>
                   </div>
               </div>
               <div class="wkt-flex text-right" style="margin-top: 1.3rem">
                  <a href="tel:${m.mobile }"> <img    src="${basePath}/img/imgs/phone.png"  style="width:25px;height:21px" alt=""/></a>
               </div>
           </div>
       </li>
   </ul>
</div>
<div class="modal" id="mymodal1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class=" hk-money">
                    <div class="wkt-flex text-left" id="spt">
                        	<img src="${res}${g.img}" onerror="this.src='/img/imgs/picture_zhanwutupian.png'" alt="" style="width: 60px;height:60px"/>
                            <p style="margin-top: -4.2rem;margin-left: 5rem;font-size: 1.1rem;color: #666">${g.name}</p>
                            <span style="color: #fe2504;margin-left: 5rem;">${g.salePrice}<s style="font-size: 14px;color: #d0d0d0">原价${g.marketPrice }</s></span>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="webkit-box">
                    <div class="wkt-flex text-left">
                        数量：
                    </div>
                    <div class="text-right" id="btn-01">
                        <div class="btn-toolbar">
                        	<div class="btn-group">
	                             <button type="button" class="btn btn-default sub">-</button>
	                             <button type="button" class="btn btn-default num">1</button>
	                             <button type="button" class="btn btn-default add">+</button>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-block">确定</button>
            </div>
        </div>
    </div>
</div>
<div class="yzm-btn tj" style="background: #ff800f;font-size: 17px; height: 50px; line-height: 50px; width: 100%; position: fixed;bottom: 0">
    <a href="#mymodal1" data-toggle="modal" style="color: #fff">立即购买</a>
</div>
	 <div class="scroll">
            <dl id="aaa" class="colse">
                <dt id="bbb"><img src="${basePath}/img/imgs/img_quick_no.png" id="imgpp" alt=""/></dt>
                <dd>
                    <a href="${basePath}/p/index.htm">
                        <img src="${basePath}/img/imgs/icon_quick_shouye.png" alt=""/>
                        <p>首页</p>
                    </a>
                </dd>
        <%--         <dd>
                    <a href="${basePath}/b/bill.htm"> <img src="${basePath}/img/imgs/icon_quick_shouxin.png"alt=""/>
                        <p>授信</p>
                    </a>
                </dd> --%>
                <dd>
                    <a href="${basePath}/orderSuper/orderCount.htm"> <img src="${basePath}/img/imgs/icon_quick_dingdan.png"alt=""/>
                        <p>订单</p>
                    </a>
                </dd>
                <dd>
                    <a href="${basePath}/Mysx.htm"> <img src="${basePath}/img/imgs/icon_quick_wode.png"alt=""/>
                        <p>我的</p>
                    </a>
                </dd>
            </dl>
        </div>
</body>
 <script type="text/javascript" src="${basePath}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath}/js/info.js"></script>
	<script type="text/javascript" src="${basePath}/template/template.js"></script>
	<script>
		$(document).ready(function() {
			var num = 1;
			$(".add").click(function(){
				num++;
				$('.num').html(num);
			});
			
			$(".sub").click(function(){
				if(num > 1) num--;
				$('.num').html(num);
			});
			
			$(".sure-sel").click(function(){
				$('.yxnum').html(num);
			});
			
			$(".btn-block").click(function(){
				<c:if test="${g.isOutStore}">api.tools.toast('抱歉，商品已售罄');</c:if>
				<c:if test="${!g.isOutStore}">window.location='<%=path%>/orderInfo/confirmOrder.htm?id=${g.id}&num='+num;</c:if>
			});
			$("#collect").click(function(){
				<c:if test="${co!=null}">api.tools.toast('已取消收藏'); </c:if>
				<c:if test="${co==null}"> api.tools.toast('收藏成功');</c:if>
				api.tools.ajax({
					url:'<%=path%>/goodsInfo/CollectAdd.htm',
					data:{
						mid:'${g.id}',
						co:'${co}',
					}
				},function(d){
					window.location.reload();
					
				});
				
			});
			function ShowDiv(obj){
				document.getElementById(obj).style.display = 'block';
				};
				 $(function() {
			            $("#bbb").on("click",function(){
			                $("#aaa").toggleClass("colse");
			                var imgObj=document.getElementById("imgpp");
			                if(imgObj.getAttribute("src",2)=="${basePath}/img/imgs/img_quick_yes.png"){
			                    imgObj.src="${basePath}/img/imgs/img_quick_no.png";
			                }else{
			                    imgObj.src="${basePath}/img/imgs/img_quick_yes.png";
			                }
			            })
			        })
		});
	</script>
</html>
