<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <meta name="format-detection" content="telephone=no" />
	    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
	    <link rel="stylesheet" href="${basePath!}/css/app.css" />
	    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
	    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
	    <link rel="stylesheet" href="${basePath!}/css/zybl.css"/>
	    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
	    <title></title>
	    <style>
	        .ipt{
	            height: 40px;
	            background: #fff;
	        }
	        .ipt>input{
	            border-radius: 18px;
	            font-size: 12px;
	            margin-left: 8%;
	            padding-left: 9%;
	
	        }
	        .glyphicon {
	            top: 4px;
	        }
	        input[type=search] {
	            width: 69%;
	            font-size: 14px;
	            -webkit-box-sizing: border-box;
	            box-sizing: border-box;
	            height: 34px;
	            text-align: left;
	            border: 0;
	            border-radius: 10px;
	            background-color: rgba(0,0,0,.1);
	        }
	        .p-a{
            margin-top: 5%;
	        }
	        .p-a>p a{
	            color: #666;
	            padding: 7px 7px 7px 7px;
	            background: rgba(0,0,0,0.1);
	            border-radius: 3px;
	        }
	    </style>
	</head>
	<body> 
	<div class="c-content" style="margin-top: 0">
		<div class="ipt">
            <input type="search"  name="name" placeholder="搜索..." id="name" class="ipt_control">
            <img src="${basePath!}/img/imgs/search.png"style="width: 5%;margin-left:-67%" alt=""/>
            <p id="dosearch" style="text-align: right;margin-top: -3rem;margin-right:10%;font-size: 1.1rem">
                <a href="javascript:void(0)" style="color: #999">搜索</a>
            </p>
        </div>
        <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
        <div id="id">
	        <div class="p-zjds">
		       	 热门搜索
		    </div>
 	    	<p id="p1"></p>
   	        <p id="p2"></p>
        </div>
		<div id="count-div">
		<div id="input-name" class="wkt-flex" style="margin-left:10%;margin-top: -0.5rem;font-size: 1.2rem">
         		   
        </div>
        <div class="wkt-flex text-right" style="margin-top: -1.5rem;color: #999" id="count">
          		 
        </div>
        </div>
       <div id="list">
       	
       </div>
  	</div>  
</body>
		<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script>
		$(document).ready(function() {

			var lat='';
			var lng='';
			api.tools.location(function(d){
				lat = d.lat;
				lng = d.lng;
				hoSearch();
			},function(){
				hoSearch();
			});

			//热门搜索hot
			function hoSearch(){
				api.tools.ajax({
					url:"${basePath!}/merchantQuery/hoRearedName",
					data:{
						name:name
					}

				},function (d){
					if(d.data!=null && d.data.length >0){
						for(var i = 0 ; i<d.data.length ; i++){
							var data=d.data[i];
							var html = template.load({
								
								host : '${basePath!}/',
								name : "ho_srarch_list",
								data : data
							});
							$html = $(html);
							$html.css('margin-right','10px');
							$html.attr('type',data.type);
							$html.attr('data',data.rid);
							$html.click(function(){
								var type=$(this).attr("type");
								var id = $(this).attr("data");
								if(type==0) {
                                    window.location = '${basePath!}/nearby/findById?id=' + $(this).attr('data');
                                }
								<#--}else {-->
									<#--window.location = '${basePath!}/goodsInfo/detail.htm?id='+$(this).attr('data')+'&lat='+lat+'&lng='+lng; -->
								<#--}-->
							});
							if(i>2){
								$("#p2").append($html);
							}else{
								$("#p1").append($html);
							}
						}
						
					}
					
				});
			};
			
			$('#dosearch').click(function(p){
				var name = $("#name").val();
				if(name == ''){
					$('#gwl').empty();
					$('#input-name').html('');
					$('#id').show();
					$('#list').empty();
						$('#count').empty();
				}else{
					$('#id').hide();
					$('#input-name').html(name);
					page = 1;
					rows = 10;
					loadData(name);
					
				}
			});
			$("#list").click(function(){
				$("#list").list();
			});
			
			
			var page = 1;
			var rows = 10;
			function loadData(name){
				api.tools.ajax({
					url:"${basePath}/merchantQuery/grabbleHandlerName",
					data:{
						page:page,
						rows:rows,
						name:name,
						/* lat:lat,
						lng:lng  */
					}
				},function(d){
					console.log(d);
                    $('#list').html("");
                    var datas=d.data.records;
                    //alert(d.data.records.length);
						$("#count").html('约'+datas.length+'条数据');

                    if(datas!= null    && datas.length  > 0){
                        // $("#count").html('约'+data.totals+'条数据');
                        for(var i = 0 ; i < datas.length ; i++){
                            var data = datas[i];
							console.log(datas);
							var html = template.load({
								host : '${basePath!}/',
								name : "grabble_handler",
								data : data
								
							});
							$html = $(html);
							$html.attr('data',data.id);
							$html.click(function(){
								 window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');
							});
							$('#list').append($html);
							
						}
						
					}else
                        {
                            if (datas.length 	== 0) {
                                $('#list').empty();
                                $('#list').append('<dl>' +
                                        '<dt>' +
                                        '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">' +
                                        '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂未找到相关信息</div>' +
                                        '</div>' +
                                        '</dt>' +
                                        '</dl>');

                        }
					}
				});
			}
			
		});
	
	</script>
		
</html>