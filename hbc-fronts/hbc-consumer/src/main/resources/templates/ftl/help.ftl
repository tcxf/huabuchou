<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <title>帮助</title>
    <style>
        body{
            background: #fff;
        }
        .help-top>img{
            width: 100%;
        }
        .top-title>p {
            padding-top: 2%;
            margin-left: 2%;
            color: #333;
        }
        .panel-default > .panel-heading>a{
            color: #333;
            text-decoration: none;
            font-size: 14px;
            font-weight: 400;
        }
        .panel-default > .panel-heading{
            background-color: #fff;
            border-bottom: 1px solid #ededed;
            padding: 15px;
        }
        .panel-default > .panel-heading>a>h4{
            color: #333;
            font-size: 14px;
            font-weight: 400;
        }
        .panel {
            margin-bottom: 0;
            background-color: #fff;
            border: none;
            border-radius: 0;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }
        .panel-body{
            font-size: 0.9rem;

        }
    </style>
</head>
<body>
<div class="top-title">
    <p>热门问题</p>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#cc1" data-toggle="collapse">
            <h4 class="panel-title">
                1:哪些人可以申请额度？
            </h4>
        </a>
    </div>
    <div class="panel-collapse collapse" id="cc1">
        <div class="panel-body">
            年龄21（含）-50周岁，且具有稳定的工作和收入来源，即可申请。其中，如果您符合以下条件中的一项或几项，将更容易通过审核：
            1）如果您有房贷、车贷或信用卡，且信用记录良好。
            2）如果您的芝麻信用评分达到666以上，且在申请时提交芝麻信用。
            温馨提示：以上条件仅作申请参考，部分条件可能因具体活动而有所不同，以活动页面的宣传为准。
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#cc2" data-toggle="collapse">
            <h4 class="panel-title">
                2:如何申请额度？
            </h4>
        </a>
    </div>
    <div class="panel-collapse collapse" id="cc2">
        <div class="panel-body">
            您可按照以下步骤操作即可申请额度：
            1）用手机号注册花不愁账户；
            2）准备好身份证、银行卡等证件，按提示完善资料并拍照上传证件信息；
            3）等待客服来电(4000000555)以便进一步核实个人身份；
            4）收到申请结果的短信通知。
            花不愁公司可视情况增减、调整申请额度及申请借款时所需的信息及身份验证等步骤，具体以申请时的实际要求为准。
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#cc3" data-toggle="collapse">
            <h4 class="panel-title">
                3:如何提高额度？
            </h4>
        </a>
    </div>
    <div class="panel-collapse collapse" id="cc3">
        <div class="panel-body">
            1) 珍惜个人信用，保持良好还款习惯，按时还款，勿产生逾期记录;
            2) 补充个人信息，多多益善哦，信息需真实，可增加提额概率;
            温馨提示: 提额仅限花不愁官方渠道，任何第三方机构或个人，以提额名义索要账号密码的，都是骗子，千万别给!!
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#cc4" data-toggle="collapse">
            <h4 class="panel-title">
                4:注册时提示手机号码已被注册过怎么办？
            </h4>
        </a>
    </div>
    <div class="panel-collapse collapse" id="cc4">
        <div class="panel-body">
            请您核对手机号码是否输入正确，
            若仍提示该手机号码原已被占用或注册，
            表示该手机号已注册过花不愁旗下的产品，
            您可以直接登录。
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#cc5" data-toggle="collapse">
            <h4 class="panel-title">
                5:申请信用钱包的时候，需要填写哪些资料？
            </h4>
        </a>
    </div>
    <div class="panel-collapse collapse" id="cc5">
        <div class="panel-body">
            申请的时候，您只需要填写个人身份资料和手机账单的授权。
            个人身份资料包括您的个人姓名，身份证号，职业信息、学历信息等。
        </div>
    </div>
</div>

<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<#include "./agreement/mune_btn.ftl">
</body>
</html>