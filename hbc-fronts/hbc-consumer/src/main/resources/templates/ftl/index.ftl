<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="format-detection" content="telephone=no" />
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
		<link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
		<title>授信</title>
		<style>
			body,
			html {
			}
		</style>
	</head>

	<body>
		
		<!-- header -->
		<div class="header_bj">
			
			<div class="header_content">
				<div class="header_title">我的授信(元)</div>
				<div class="money" id="money">--</div>
			</div>
			<img src="${basePath!}/images/bj.png" />
		</div>
		<div class="main_bar">
			<div class="webkit-box mat">
				<div class="wkt-flex" id="scan">
					扫码消费
				</div>
				<div class="xt"></div>
				<div class="wkt-flex" id="mobile-trans">
					手机转账
				</div>
				<div class="xt"></div>
				<div id = "tx" class="wkt-flex">
					提现
				</div>
			</div>
			<!-- 清除浮动 -->
			<div style="clear:both;"></div>
		</div>
		
		<div class="c-content" onclick="javascript:window.location='<%=path%>/b/bill.htm'">
			<div class="webkit-box">
				<div class="wkt-flex">
					七日待还
				</div>
				<div class="wkt-flex text-right">
					<span>${unpayAmount}</span><i class="fa fa-angle-right main-i" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		
		<div class="c-content main-poster">
			<img src="${basePath!}/images/gg.jpg" />
		</div>
		
		<div class="c-content" style="background: none;">
			<div class="webkit-box main-title">
				<div class="wkt-flex">
					&nbsp;&nbsp;消费记录
				</div>
				<div class="wkt-flex text-right" onclick="javascript:window.location='<%=path%>/b/historyRecord.htm'">
					查看更多 <i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;
				</div>
			</div>
			<div id="trading-list">
			
			</div>
			<div style="margin-top: 55px;"></div>
		</div>
		<jsp:include page="/WEB-INF/jsp/common/footer.jsp">
			<jsp:param name="name" value="index" />
		</jsp:include>
	</body>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src ="<%=path%>/template/template.js"></script>
	<script>
		$(document).ready(function(){
			
			$.post('<%=path%>/loadTrading.htm',null,function(d){
				if(d.data != null && d.data.length > 0){
					for(var i = 0; i < d.data.length ; i++){
						ob = d.data[i];
						var data = {
							mname:ob.mname,
							amount:parseFloat(ob.amount).toFixed(2),	
							id:ob.id,
							path:'<%=path%>',
							createDateStr:ob.createDateStr
						};
						var $html = template.load({
							host : api.base,
							name : "tradinglist",
							data : data
						});	
						console.log($html);
						$("#trading-list").append($html);
					}
				}
			},'json');
			
			$.post('<%=path%>/loadAccountBalance.htm',null,function(d){
				$("#money").html(d.data);
			},"json");
			
			$("#mobile-trans").click(function(){
				window.location = "<%=path%>/t/mtrans.htm";
			});
			$("#tx").click(function(){
				api.tools.toast("功能即将开放");
			});
			
			var option = ["scanQRCode"];
			api.tools.initWxJsSdk(option, function(){
				$("#scan").click(function(){
					wx.scanQRCode({
					    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
					    scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
					    success: function (res) {
					    	var result = res.resultStr;
					    	$.post('<%=path%>/system/decode.htm',{sign:result},function(d){
					    		if(d.resultCode == "1"){
					    			window.location = '<%=path%>/t/doTrans.htm?phone='+d.data;
					    		}else{
					    			api.tools.toast("二维码识别失败");
					    		}
					    	},"json");
						}
					});
				});
			});
		});
	</script>
</html>