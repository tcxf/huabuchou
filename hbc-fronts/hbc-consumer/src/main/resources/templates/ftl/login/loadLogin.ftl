<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <title>登录</title>
    <style>
        body{
            background:url(${basePath!}/img/imgs/new_bg_img.png)no-repeat;
            background-size: cover;
        }
        .list_content{
            margin-top: 30%;
            padding:0 60px;
        }
        .list_content li{
            text-align: center;
            margin-left:10px;
            margin-right:10px
        }
        .m_tab {
            line-height: 48px;
            height: 48px;
            background:transparent;

        }
        .active>a{
            color: #f49110 !important;

        }

        .m_tab .active {
            border-bottom: 2px solid #f49110;

        }
        .m_tab a{
            color: #999;
            font-weight: 600;
            display: block;
        }

        .a-content{
            margin-top:10%
        }

        .login-model {
            width: 85%;
            padding: 20px 0px 20px 5px;
        }
        .sms-btn1{
            font-size: 0.8rem;
            padding: 3px 8px;
            border:1px solid #f49110;
            color:#f49110;
            border-radius: 30px;
        }
        input{
            outline:medium;
        }
        .a_zhuce{
            margin-top:20px;
            width:100%;
            text-align:right;
        }
        .a_zhuce>a{
            padding-right:20px;
            color:#999;
            font-size:0.8rem
        }
        #hyzm-btn{
            background: transparent;
            border: 1px solid #f49110;
            outline: none;
            border-radius: 30px;
            font-size: 0.8rem;
            color: #f49110;
            cursor: pointer
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer
        }
    </style>
</head>
<body>
<div class="list_content">
    <div class="m_tab">
        <ul class="webkit-box">
            <li class="active wkt-flex"><a href="#all" data-toggle="tab">会员</a></li>
            <li class="wkt-flex"><a href="#dfk" data-toggle="tab">商户</a></li>
        </ul>
    </div>
</div>
<div class="m_content tab-content " >
    <div class="c_thing tab-pane active" id="all">
        <div class="a-content login-model">
            <div class="webkit-box ">
                <div style="line-height: 30px; width: 11%;">
                    <img src="${basePath!}/img/imgs/ico_zhanghao.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="loginName" name="loginName" class="form-control reg-input" placeholder="请输入您的手机号码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin: 10px 10px 20px 10px"></div>
            <div class="webkit-box ">
                <div style="line-height: 30px; width: 11%;margin-top: 0.3em;">
                    <img src="${basePath!}/img/imgs/poss_word.png" />
                </div>
                <div class="wkt-flex" style="padding-top: 5px">
                    <input type="tel" id="yCode" name="yCode" class="form-control reg-input" placeholder="输入验证码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 0.8rem;">
                </div>
                <div style="margin-top: 3%">
                    <input type="button" id="hyzm-btn"  value="获取验证码"/>
                </div>
            </div>
            <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>

            <div>
              <button class="btn-block yzm-btn tj">登录</button>
            </div>
            <div class="a_zhuce redirect">
                <a href="${basePath!}/register/goRegister?type=1&openId=${openId!}#all">立即注册</a>
            </div>
        </div>
    </div>
    <div class="c_thing tab-pane" id="dfk">
        <div class="a-content login-model">
            <div class="webkit-box ">
                <div style="line-height: 30px; width: 11%;">
                    <img src="${basePath!}/img/imgs/ico_zhanghao.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel"  name="loginName2" id="loginName2" class="form-control reg-input" placeholder="请输入您的手机号码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin: 10px 10px 20px 10px"></div>
            <div class="webkit-box ">
                <div style="line-height: 30px; width: 11%;margin-top: 0.3rem;">
                    <img src="${basePath!}/img/imgs/poss_word.png" />
                </div>
                <div class="wkt-flex" style="padding-top: 5px">
                    <input type="password"  name="password" id="password" class="form-control reg-input" placeholder="输入您的登录密码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>
            <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
            <div>
                 <button class="btn-block yzm-btn tj2" >登录</button>
            </div>
            <div class="a_zhuce redirect">
                <a href="${basePath!}/user/userInfo/goForgetPassword" style="color:#f49110">忘记密码</a>
                <a href="${basePath!}/register/goRegister?type=1&openId=${openId!}#marchant">立即注册</a>
            </div>
        </div>
    </div>
</div>

<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script src="${basePath!}/js/ajax.js"></script>
<script>
    $("#hyzm-btn").click(function(){
        getycode();
    });

    $(".tj").click(function(){
        login(1)
    });

    $(".tj2").click(function(){
        login(2)
    });

    function getycode() {
        var mobile = $("#loginName").val();
        if(mobile==null||mobile==""){
            layer.msg("请输入手机号码");
            return;
        }
        var re = /^1\d{10}$/;
        if (!re.test(mobile)) {
            layer.msg("请输入正确的手机号码");
            return;
        }
        $("#hyzm-btn").attr("disabled", true);
        var url = "${basePath!}/cuser/getYzm?mobile="+mobile+"&type=YZM";
        var data={};
        ajax.loadService(url,data, function(data){
            layer.msg(data.msg);
            startTime();
        });
    }

    var i = 60;
    function startTime(){
        $("#hyzm-btn").val("获取验证码("+i+"秒)");
        if(i > 0){
            i--;
            setTimeout(function(){
                startTime();
            },1000);
        }else{
            $("#hyzm-btn").val("获取验证码");
            $("#hyzm-btn").attr("disabled", false);
            i = 60;
        }
    }

    function login(type) {
        var data = {};
        if (type == 1) {
            data.mobile = $("#loginName").val();
            data.ycode = $("#yCode").val();
            data.openId = "${openId!}";
            data.type = type;
            if (data.mobile == null || data.mobile == "") {
                layer.msg("请输入手机号码");
                return null;
            }
            var re = /^1\d{10}$/;
            if (!re.test(data.mobile)) {
                layer.msg("请输入正确的手机号码");
                return;
            }
            if (data.ycode == null || data.ycode == "") {
                layer.msg("请输入验证码");
                return null;
            }
        }
        if (type == 2) {
            data.mobile = $("#loginName2").val();
            data.pwd = $("#password").val();
            data.openId = "${openId!}";
            data.type = type;
            if (data.mobile == null || data.mobile == "") {
                layer.msg("请输入手机号码");
                return null;
            }
            re = /^1\d{10}$/
            if (!re.test(data.mobile)) {
                layer.msg("请输入正确的手机号码");
                return;
            }
            if (data.pwd == null || data.pwd == "") {
                layer.msg("请输入密码");
                return null;
            }
            if (data.pwd.length < 6 || data.pwd.length > 16)
            {
                layer.msg("密码必须是6-16位");
                return null;
            }
        }
        var url = "${basePath!}/cuser/login";
        ajax.loadService(url, data, function (data) {
            if (data.code == "0") {
                window.location.href = "${basePath!}/user/index/Goindex";
            }
            if (data.code == "1") {
                layer.msg(data.msg);
            }
        });
    }

</script>
</body>
</html>