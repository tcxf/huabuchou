<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript" src="${basePath!}/template/template.js"></script>
    <#include "common.ftl">
    <title>会员管理</title>
</head>
<body>
<div class="top_content">
    <div class="bg_img">
        <div class="search_input">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-10">
                        <input id="search_input" type="text" name="realName" placeholder="请输入您要搜索的会员信息">
                        <img class="search_img"src="${basePath!}/img/img/icon_souyisou.png" alt="">
                        <img class="clear_img" src="${basePath!}/img/img/icon_delete.png" alt="">
                    </div>
                    <div class="col-xs-2" id="queryAll">
                        搜索
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vip_list">
    <div class="list_top">
        <ul>
            <li>会员头像</li>
            <li>会员名称</li>
            <li>会员手机</li>
        </ul>
    </div>
    <div class="vip-content">
        <ul id="list">
        </ul>
    </div>
</div>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {
        search_inputs();
        $("#queryAll").click(function () {
            search_inputs();
        })
        $(".clear_img").click(function () {
            $("#search_input").val("").focus(); // 清空并获得焦点
        })
        function search_inputs() {
            var realName =$("#search_input").val();
            // alert(realName)
            $('#list').empty();
            $.ajax({
                url: '${basePath!}/Vips/findVip',
                data:{
                    realName:realName
                },
                type: 'POST',
                dataType: 'json',
                error: function (error) {
                   //alert(error);
                },
                success: function (d) {
                    console.log(d);
                    var da = d.data;
                    if (da!=null && da.records!= null && da.records.length > 0) {
                        for (var i = 0; i < da.records.length; i++) {
                            var data = da.records[i];
                            if(data.headImg=='images/uhead.png'){
                                data.headImg='${basePath!}/'+data.headImg;
                            }
                            var html = template.load({
                                host: '${basePath!}/',
                                name: "vip_list",
                                data: data
                            });
                            $html = $(html);
                            $('#list').append($html);
                        }
                    } else {
                        $('#list').html('<div class="content" align="center">' +
                                '<img src="${basePath}/img/img/queshenye_huiyuanjil.png" alt=""/>' +
                                '<h5>暂无记录...</h5>' +
                                '</div>');
                    }
                }
            });
        }
    });
</script>
</body>
</html>