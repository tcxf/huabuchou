<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="${basePath!}/css/LCalendar.css" />
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
    <title>添加优惠券</title>
    <style>
        body{
            background: #fff;
        }
        .text_one{
            padding: 10px;
            font-size: 14px;
        }
        .a_content{
            width: 100%;
            position: relative;
        }
        .a_content>img{
            width: 94%;
            margin-left: 3%;
            height: 120px;
        }
        .coupont_content{
            width: 100%;
            position: absolute;
            top:20px;
        }
        .col-xs-3{
            text-align: center;
            padding-right: 0;
        }
        .col-xs-3 p{
            color: #f49110;
            font-size: 2rem;
        }
        .col-xs-3 h6{
            color: #f49110;

        }
        .coupont_content p{
            margin-bottom: 5px;
        }
        .coupont_content h6{
            margin: 0;
        }
        .col-xs-9{
            margin-top: 10px;
        }
        .col-xs-9 h6{
            color: #999;
        }
        .col-xs-12{
            color: #999;
        }
        .content_list{
            width: 94%;
            height: auto;
            background:#fff;
            margin-left: 3%;
            box-shadow: 0 0 5px #dedede;
            border-radius: 8px;

        }

        .content_list .row{
            border-bottom: 1px solid #ededed;
            padding: 13px;
        }
        .content_list .row:last-child{
            border: none;
        }
        .content_list .col-xs-4{
            padding-left: 0;
        }
        .content_list .col-xs-8{
            padding-left: 0;
        }

        .content_list .col-xs-8>input{
            border: none;
            outline:medium;
        }
        .content_list .col-xs-6>input{
            border: none;
            outline:medium;
        }
        .content_list .col-xs-6{
            padding-left: 0;
        }
        .content_list .col-xs-2{
            padding-right: 0;
            text-align: right;
        }
        .content_list .col-xs-2>img{
            width: 10px;
            height: 12px;
        }
        .modal{
            background: rgba(0,0,0,0.6);
        }
        .modal .content{
            width: 100%;
            height: 200px;
            background:rgba(255,255,255,0.8);
            position: fixed;
            bottom: 0;
            padding: 5px 0 10px 0;
        }
        .modal .header>ul{
            padding-left: 0;
        }
        .modal .header li{
            float: left;
            list-style: none;
            width: 50%;
        }
        .modal .header li a{
            padding: 5px 10px;
            background: #fff;
            color: #333;
            border-radius: 8px;
            text-decoration: none;
        }
        .modal .header li:last-child{
            text-align: right;
        }
        .modal .header li #play_ok{

            background: #00a0e9;
            color: #fff;
        }
        .media-body{
            padding-top: 30px;
            text-align: center;
        }
        .media-body>ul{
            padding-left: 0;
        }
        .media-body li{
            list-style: none;
            height: 40px;
            line-height: 40px;
            font-size: 2rem;
        }
        .media-body li:hover{
            background: #999;
            color: #fff;
        }
        .a-btn{
            width: 94%;
            height: 40px;
            background:#f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
            margin-top: 40px;
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }

        .b-btn{
            width: 94%;
            height: 40px;
            background:#f5f5f5;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
            margin-top: 40px;
        }
        .b-btn>a{
            color: #333;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="text_one">
    示例
</div>
<div class="a_content">
    <img src="${basePath!}/img/imgs/youhuijuan_beij.png" alt="">
    <div class="coupont_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3">
                    <p>
                        <span style="font-size: small">¥</span>
                        10
                    </p>
                    <h6>优惠券</h6>
                </div>
                <div class="col-xs-9">
                    <p>店庆活动</p>
                    <h6>2018-04-09 至 2018-04-15</h6>
                </div>
            </div>
            <div class="row" style="margin-top: 26px;padding-left: 10px">
                <div class="col-xs-12">满100减30元</div>
            </div>
        </div>
    </div>
</div>
<div class="text_one">
    编辑
</div>
<div class="content_list">
    <form id="_f">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                优惠券名称
            </div>
            <div class="col-xs-8">
                <input type="text" id="name" name="name" placeholder="店庆活动"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                优惠价格
            </div>
            <div class="col-xs-8">
                <input type="text"name="money" onkeyup="value=value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')" id="money" placeholder="10.0"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                减免条件
            </div>
            <div class="col-xs-8">
                <input style="width: 100%" type="text" name="fullMoney" onkeyup="value=value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')" id="fullMoney" placeholder="请输入您的减免条件"/>
                <input type="hidden" class="timeType" id="timeType1" name="timeType" value="1" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                结束类型
            </div>

                <div class="col-xs-6"  id="end_lei">
                    <input type="text" id="" onfocus="this.blur()" placeholder="固定截止时间"/>

                </div>

        </div>
        <div class="modal" id="mymodal">

            <div class="content">

                <div class="media-body">
                    <ul>
                        <li id="jiezhi_time">固定截止时间</li>
                        <li id="text_one">从领取日起计算</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" id="end_time">
            <div class="col-xs-4">
                结束时间
            </div>
            <div class="col-xs-6">
                <input UNSELECTABLE='true' onfocus="this.blur()"  type="text" name="endDate" id="end_date" placeholder="选择开始日期"  />
            </div>
        </div>
        <div class="row" id="keyong_time" style="display: none">
            <div class="col-xs-4">
                可用天数
            </div>
            <div class="col-xs-8">
                <input name="days" id="days" type="text" placeholder="请输入可用天数"/>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="a-btn" id="bc">
    <a href="#">保存</a>
</div>
<div class="b-btn" id="tj" style="display: none">
    <a href="#">保存</a>
</div>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/LCalendar.js" type="text/javascript"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {
        $(".a-btn").click(function (){
            var name = $("#name").val();
            var fullMoney = $("#fullMoney").val();
            var money = $("#money").val();
            var endDate =  $("#end_date").val();
            var type =  $(".timeType").val();
            var days = $("#days").val();
            if(name.length==0){
                api.tools.toast("请添加优惠券名称");
                return;
            }
            if(money.length==0){
                api.tools.toast("请填写优惠价格");
                return;
            }
            if(fullMoney.length==0){
                api.tools.toast("请填写减免条件");
                return;
            }
            if(parseFloat(fullMoney)<parseFloat(money)){
                api.tools.toast("优惠金额必须小于减免条件");
                return;
            }
            if(type==1){
                $("#timeType1").attr("value","1");
                if(endDate.length==0){
                    api.tools.toast("请填写到期日期");
                    return;
                }
            }else{
                $("#timeType1").attr("value","2");
                if(days.length==0){
                    api.tools.toast("请填写可用天数");
                    return;
                }
            }
            $("#bc").css("display","none")
            $("#tj").css("display","block")
            var data = $("#_f").form1()
            $.ajax({
                url:'${basePath!}/user/Mcoupon/couponInsert',
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:data,
                success: function(data){ //成功
                    if(data.code!=1) {
                        api.tools.toast(data.msg);
                        window.location = '${basePath!}/user/Mcoupon/couponFind';
                    }else{
                        $("#bc").css("display","block")
                        $("#tj").css("display","none")
                        api.tools.toast(data.msg);
                    }
                }
            });
        });

        $.fn.form1 = function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };


        $("#end_lei").click(function () {
            document.getElementById("mymodal").style.display = "block";

        });
        $("#closed").click(function () {
            document.getElementById("mymodal").style.display = "none";
        });
        $(function () {
            //点击关闭按钮的时候，遮罩层关闭
            $(".media-body li").on('click', function () {
                $("#mymodal").css("display", "none");
                $("#end_lei").text($(this).html());
            });
            $(".media-body li:last-child").on('click', function () {
                $("#end_time").css("display", "none");
                $("#keyong_time").css("display", "block");
                $(".timeType").attr("value", 2);//推
            });
            $(".media-body li:first-child").on('click', function () {
                $("#end_time").css("display", "block");
                $("#keyong_time").css("display", "none");
                $(".timeType").attr("value", 1);//推
            });
        });
        var calendar = new LCalendar();
        calendar.init({
            'trigger': '#end_date', //标签id
            'type': 'date', //date 调出日期选择 datetime 调出日期时间选择 time 调出时间选择 ym 调出年月选择,
            'minDate': (new Date().getFullYear() - 3) + '-' + 1 + '-' + 1, //最小日期
            'maxDate': (new Date().getFullYear() + 3) + '-' + 12 + '-' + 31 //最大日期
        });
    })
</script>
</body>
</html>