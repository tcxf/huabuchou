<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zybl.css"/>
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <title>商户优惠券</title>
    <style>
        body{
            background: #fff;
        }
        *{
            font-size: 0.8rem;
            margin: 0;
            padding: 0;
        }
        h6{
            margin: 0;
        }
        p{
            margin-bottom: 5px;
        }

        .coupont_list{
            width: 100%;
            height: auto;
            margin-top: 10px;
            border-bottom: 1px solid #ededed;
            padding-bottom: 10px;
            position: relative;
        }
      .coupont_list>img{

          width: 94%;
          margin-left: 3%;
          height: 120px;
      }
        .coupont_one{
            width: 100%;
            position: absolute;
            top:30px;
        }
        .coupont_one .col-xs-3>img{
            width: 40px;
        }
        .coupont_one .col-xs-3:first-child{
            text-align: center;
            padding-right: 0;
            color: #f49110;
        }
        .coupont_one .col-xs-3:first-child p{
            font-size: 2rem;
        }
        .coupont_one .col-xs-3:last-child{
            text-align: center;
            padding: 0;
        }
        .coupont_one .col-xs-6{
            padding-left: 5px;
            margin-top: 10px;
            padding-right: 0;
        }
        .coupont_one .col-xs-6 h6{
            color: #999;
        }
        .coupont_one .col-xs-12{
            padding-left:30px;
            margin-top: 12px;
            color: #999;
        }
        .choose_btn{
            padding-top: 10px;
        }
        .choose_btn .col-xs-8{
            text-align: right;
        }
        .choose_btn .col-xs-8 a{
            padding:3px 12px;
            background: #f49110;
            border-radius: 30px;
            color: #fff;
            text-decoration: none;
        }
        .a-btn{
            margin-top: 10px;
            width: 94%;
            height: 40px;
            border: 1px dashed #f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .a-btn>a{
            color: #f49110;
            text-decoration: none;
        }
        .b-btn{
            margin-top: 10px;
            width: 94%;
            height: 40px;
            border: 1px dashed #f49110;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
        }
        .b-btn>a{
            color: #f49110;
            text-decoration: none;
        }
        :last-child .coupont_list{
            margin-bottom: 40px;

        }
        .sh{
            color:#f49110;
            padding: 3px 5px;
            border: 1px solid #f49110;
            border-radius: 30px;
        }
    </style>
</head>
<body>
<#if type?? && type=="1">
        <div class="a-btn">
            <a href="#">添加优惠券</a>
        </div>
<#else >
   <div class="b-btn">
       <a href="#">添加优惠券</a>
   </div>
</#if>
   <div class="coupont_lists" id="coupon">

   </div>





</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {

        //
        // var startx, starty;
        // //获得角度
        // function getAngle(angx, angy) {
        //     return Math.atan2(angy, angx) * 180 / Math.PI;
        // };
        //
        // //根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
        // function getDirection(startx, starty, endx, endy) {
        //     var angx = endx - startx;
        //     var angy = endy - starty;
        //     var result = 0;
        //
        //     //如果滑动距离太短
        //     if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
        //         return result;
        //     }
        //
        //     var angle = getAngle(angx, angy);
        //     if (angle >= -135 && angle <= -45) {
        //         result = 1;
        //     } else if (angle > 45 && angle < 135) {
        //         result = 2;
        //     } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
        //         result = 3;
        //     } else if (angle >= -45 && angle <= 45) {
        //         result = 4;
        //     }
        //
        //     return result;
        // }
        // //手指接触屏幕
        // document.addEventListener("touchstart", function(e) {
        //     startx = e.touches[0].pageX;
        //     starty = e.touches[0].pageY;
        // }, false);
        // //手指离开屏幕
        // document.addEventListener("touchend", function(e) {
        //     var endx, endy;
        //     endx = e.changedTouches[0].pageX;
        //     endy = e.changedTouches[0].pageY;
        //     var direction = getDirection(startx, starty, endx, endy);
        //     switch (direction) {
        //         case 0:
        //             alert("未滑动！");
        //             break;
        //         case 1:
        //             alert("向上！")
        //             break;
        //         case 2:
        //             alert("向下！")
        //             break;
        //         case 3:
        //             alert("向左！")
        //             break;
        //         case 4:
        //             alert("向右！")
        //             break;
        //         default:
        //     }
        // }, false);




        $(".b-btn").click(function () {
            api.tools.toast("该商户审核未通过，或者已禁用，不能添加优惠券");
            return;
        });
$(".a-btn").click(function () {
            window.location.href='${basePath!}/user/Mcoupon/couponAdd';
        });
        coupon();
        function coupon() {
            $("#coupon").empty();
            $.ajax({
                url:'${basePath!}/user/Mcoupon/couponList',
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:'',
                success: function(d){ //成功
                    var da = d.data;

                    if(da!=null && da.length>0){
                        for(var i = 0 ; i < da.length ; i++){
                            var data = d.data[i];
                            data.path = '${basePath!}'
                            if(data.status==3){
                                if(data.couponType==0){
                                    data.imgs = "/img/imgs/post_no.png";
                                    data.type1 = 'style="display:none"';
                                }else {
                                    data.imgs = "/img/imgs/post_sx.png";
                                    data.type1 = 'style="display:none"';
                                }
                                data.type3='';
                                data.imgss='';
                            }else if(data.status==1){
                                data.type3='style="display:none"';
                                data.imgss='<a href="#" role="button" class="sh">审核中</a>';
                            }else {
                                data.imgs = "/img/imgs/post_ok.png";
                                data.type1 ='';
                                if(data.status==2){
                                    data.type2='style="display:none"';
                                }
                                data.type3='';
                                data.imgss='';
                            }
                            var html = template.load({
                                host: '${basePath!}/',
                                name: "red_packet_setting_list",
                                data: data
                            });
                            $html = $(html);
                            $html.find(".sx").attr("data",data.id);
                            $html.find(".bj").attr("data",data.id);
                            $html.find(".bj").click(function () {
                                window.location.href = "${basePath!}/user/Mcoupon/couponUpdate?id="+$(this).attr('data');
                            })
                            $html.find(".sx").click(function () {
                                $.ajax({
                                    url:'${basePath!}/user/Mcoupon/couponFailure',
                                    type:'post', //数据发送方式
                                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                                    data:{
                                        id:$(this).attr('data')
                                    },
                                    success: function(d){ //成功
                                        api.tools.toast(d.msg)
                                        coupon();
                                    }
                                })

                            })



                            $('#coupon').append($html);
                        }
                    }else{
                        $('#coupon').html('<div class="content" align="center">'+
                                '<img src="${basePath}/img/img/icon_zanwujilu.png" alt=""/>'+
                                '<h5>暂无记录...</h5>'+
                                '</div>');
                    }
                }

            })
        }
    })

</script>
</html>