<!DOCTYPE>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 <#include "common.ftl">
    <title>店铺资料</title>
    <style>
       body{
            background:#fff;
        }


        .map_model{
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .amap-sug-result { z-index: 1050;}
        #tipinput{
            border: 1px solid #999!important;
            padding-left: 5px;
        }
        #quxiao{
            height: 30px;
            background: #00a0e9!important;
            color: #fff!important;
        }
        .a-content{
            width:100%;
            background:#fff;

        }
        #top-yzm{
            margin-top: -200px;
            text-align: right;

        }
        .sms-btn{
            color: #000000 !important;
        }
        #myPageTop input {
            width: 170px;
            height: 30px;
        }
        input{
            font-size: 14px;
            -webkit-user-select: auto !important;
        }

        .dianpu-photo{
            margin-top:20px;
            width:100%;
            height:100px;
            clear:both
        }
        .dianpu-photo li{
            float:left;
            width:40%;
            text-align:center;
            padding:10px;
        }
        .dianpu-photo li img{
            width:120px;
            height:100px;
        }
        .sj-ul li {
            padding: 10px 0px 1px 10px;
            position: relative;
        }
        .tj{
            background:#f49110;
            color:#ffffff;
            font-size: 1rem;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            border-radius:30px;
            margin-top:50px;
            clear:both

        }
        .yzm-btn>a{
            color:#ffffff;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="a-content" style="margin-top: 0px; padding:0px;">
    <form id="_f" method="post" class="form-horizontal">
        <ul class="sj-ul">
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        店铺简称:
                    </div>
                    <div class="wkt-flex">
                        <input type="hidden"  id="ids" name="id"  value="${m.id!}">
                    <#--<input type="text" id="simpleName" <%-- <c:if test="${m.authExamine != 0 && m.authExamine != 2 && (m.simpleName != null && m.simpleName != '')}"> UNSELECTABLE='true' onfocus="this.blur()"</c:if> --%> value="${m.simpleName}" class="form-control reg-input" placeholder="请填写店铺简称"-->
                    <#--style="background-size:3%;font-size:14px;-->
                    <#--background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">-->
                        <input type="text" id="simpleName"  name="simpleName"  value="${m.simpleName!}" class="form-control reg-input" placeholder="请填写店铺名称"
                               style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <form id="_f" method="post" class="form-horizontal">
                        <div class="input-left-name">
                            店铺名称:
                        </div>
                        <div class="wkt-flex">
                            <input type="text" id="name"  name="dname" value="${m.name!}" class="form-control reg-input" placeholder="请填写店铺名称"
                                   style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                        </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        法人证件号码:
                    </div>
                    <div class="wkt-flex">
                        <input type="text" id="idCard" name="idCard"  value="${m.idCard!}" class="form-control reg-input" placeholder="请填写法人证件号"
                               style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        法人名称:
                    </div>
                    <div class="wkt-flex">
                        <input type="text" id="legalName" name="legalName"  value="${m.legalName!}" class="form-control reg-input" placeholder="请填写法人名称"
                               style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        行业类型：
                    </div>
                    <div class="wkt-flex">
                        <input type="hidden" name="micId" id="micId" value="${m.micId!}"/>
                        <input type="tel"   onfocus="this.blur()" id="mcName" value="${m.hname!}" class="form-control reg-input" placeholder="餐饮"
                               style="background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        开店时间：
                    </div>
                    <div class="wkt-flex">
                        <input type="tel"    UNSELECTABLE='true' onfocus="this.blur()" id="openDate" value="${m.openDate!}" name="openDate" class="form-control reg-input" placeholder="2013-12-23"
                               style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        店铺电话：
                    </div>
                    <div class="wkt-flex">
                        <input type="tel" id="phoneNumber" name="mobile" value="${m.mobile!}" class="form-control reg-input" placeholder="请填写店铺电话"
                               style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        位置：

                    </div>
                    <div class="wkt-flex">
                        <input type="hidden" name="area" id="areaId" value="${m.areaId!}"/>
                        <input type="tel" id="displayName" value="${m.displayName!}" UNSELECTABLE='true' onfocus="this.blur()" class="form-control reg-input" placeholder="湖南省-长沙市-芙蓉区-白沙街道"
                               style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">

                    </div>
                </div>
            </li>
            <li id="go_map">
                <div class="webkit-box">
                    <div class="input-left-name">
                        详细地址：
                    </div>
                    <div class="wkt-flex">
                                     <input type="text" id="address" readonly name="address" value="${m.address!}" class="form-control reg-input" placeholder="请输入详细地址"
                                            style=" background:transparent;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                                    <input type="hidden" id="lng"  name="lng" value="${m.lng!}" >
                                    <input type="hidden" id="lat" name="lat" value="${m.lat!}" >
                    </div>
                </div>
            </li>
            <li>
                <div class="webkit-box">
                    <div class="input-left-name">
                        店铺介绍：
                    </div>
                    <div class="wkt-flex">
                        <input type="text" id="recommendGoods"  name="recommendGoods" value="${m.recommendGoods!}" class="form-control reg-input" placeholder="请填写店铺介绍"
                               style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                    </div>
                </div>
            </li>
        </ul>

        <div class="dianpu-photo">
            <p style="font-size:0.8rem;color:#999;padding:10px">添加店铺照片</p>


            <span style="margin-left: 10px" class="img" file-id="localPhoto" file-value="${m.localPhoto!}"></span>


        </div>

        <div class="dianpu-photo" style="margin-top: 20%">
            <p style="font-size:0.8rem;color:#999;padding:10px">添加营业执照</p>


            <span style="margin-left: 10px" class="img" file-id="licensePic" file-value="${m.licensePic!}" ></span>

        </div>
    </form>
</div>
<div class="yzm-btn tj" id="submit-save">
    <a href="#">保存</a>
</div>


<div class="map_model">
    <div id="container"></div>
    <div id="myPageTop">
        <table>
            <tr>
                <td>
                    <label>请输入关键字：</label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="tipinput"/>
                    <input type="button" value="确定" id="quxiao">
                </td>
            </tr>
        </table>
    </div>
</div>

</body>
<#include "../agreement/mune_btn.ftl">
<script>
    $("#go_map").click(function () {
        $(".map_model").css("display","block")
    });
    $(document).ready(function () {

        $(".img").uploadImg();

        function closeLayer(){
            layer.close(l);
        }

        window.onbeforeunload = function() {
            window.opener.location.reload();
        };


        var n = new Date();
        var dtPicker = new mui.DtPicker({
            type: 'date',
            beginDate: new Date(1900, 1, 1),
            endDate: new Date(n.getFullYear(), n.getMonth(), n.getDate())
        });
        $("#openDate").click(function() {
            dtPicker.show(function(s) {
                $("#openDate").val(s.y.text + "-" + s.m.text + "-" + s.d.text);
            });
        });

        //定义一个Array对象
        var ids = new Array();
        var categoryList = new Array();
        var mapGet = new Array();

        //遍历list
			<#list list as lists >
			text="${lists.name!}";
			id="${lists.id!}";

			//循环把id push进Array（ids）对象里面
			ids.push(id);
			//循环把name push进Array（ids）对象里面
			categoryList.push(text);

			//定义一个obj 对象
			var aa = new Object();
			//把 id  text 赋值进 obj对象
			aa.a = id;
			aa.b = text;
			//
			mapGet.push(aa);

            </#list>

        var categoryPicker = new mui.PopPicker();
        categoryPicker.setData(categoryList);
        $("#mcName").click(function(){
            //循环categoryList赋值
            for (var i = 0; i <categoryList.length ; i++) {
                $("#mcName").val(categoryList[i].text);
                $("#micId").val(mapGet[i].a);

            }

            categoryPicker.show(function(s) {
                $("#mcName").val(s);
                for (var i = 0; i <mapGet.length ; i++) {
                    // mapGet[i].b = s;
                    if(mapGet[i].b == s)
                    {
                        $("#micId").val(mapGet[i].a);
                        break;
                    }


                }
            });
        });

        var pickerCity = new mui.PopPicker({
            layer: 3
        });
        $.ajax({
            type:'GET',
            url:'${basePath!}/js/area.json',
            success: function (data)
            {
                pickerCity.setData(data);
            },
            dataType: "json",//请求数据类型
        },function(d){
            console.log(d);
        });


        $("#displayName").click(function(){
            pickerCity.show(function(s) {
                $("#areaId").val(s[2].value);
                $("#displayName").val(s[0].text+s[1].text+s[2].text);
            });
        });

        $("#submit-save").click(function (){
            if(!$("#_f").validate()) return;
            var ids=$.trim($("#ids").val());
            var simpleName = $.trim($("#simpleName").val());
            var micId = $.trim($("#micId").val());
            var openDate = $.trim($("#openDate").val());
            // alert(openDate)
            var areaId = $.trim($("#areaId").val());
            if(areaId=="" || areaId ==null || areaId ==ids)
                areaId='${m.area!}';
            var displayName=$.trim($("#displayName").val());
            var address = $.trim($("#address").val());
            var phoneNumber = $.trim($("#phoneNumber").val());
            var name = $.trim($("#name").val());
            var idCard = $.trim($("#idCard").val());
            var legalName = $.trim($("#legalName").val());
            var licensePic=$("#img_licensePic").attr("src");
            var localPhoto=$("#img_localPhoto").attr("src");
            var recommendGoods=$.trim($("#recommendGoods").val());
            var lat=$.trim($("#lat").val());
            var lng=$.trim($("#lng").val());
            // alert(id)
            if(simpleName.length < 2){
                layer.msg("店铺简称至少需要2个字");
                // api.tools.toast('店铺简称至少需要2个字');
                return;
            }
            if(simpleName.length > 15){
                layer.msg("店铺简称最多只能10个字");
                // api.tools.toast('店铺简称最多只能10个字');
                return;
            }
            if(!isNaN(simpleName)){
                layer.msg("店铺简称不能为数字")
                // api.tools.toast('店铺简称不能为数字');
                return;
            }

            var regex = /^[\u0391-\uFFE5\w]+$/;
            if(!regex.test(simpleName)){
                layer.msg("店铺简称不能包含特殊字符");
                // api.tools.toast('店铺简称不能包含特殊字符');
                return;
            }

            if(name.length < 2){
                layer.msg("店铺名称至少需要2个字");
                // api.tools.toast('店铺名称至少需要2个字');
                return;
            }
            if(name.length > 15){
                layer.msg("店铺名称最多只能10个字");
                // api.tools.toast('店铺名称最多只能10个字');;
                return;
            }
            if(!isNaN(name)){
                layer.msg("店铺名称不能为数字");
                // api.tools.toast('店铺名称不能为数字');
                return;
            }

            var regex = /^[\u0391-\uFFE5\w]+$/;
            if(!regex.test(name)){
                layer.msg("店铺名称不能包含特殊字符");
                // api.tools.toast('店铺名称不能包含特殊字符');
// 					$("#realName").focus();
                return;
            }
            var regex = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
            if(!regex.test(idCard)){
                //api.tools.toast('请输入正确的身份照号码');
                layer.msg("请输入正确的身份照号码");
                return;
            }
            if(idCard.length < 17 ){
                layer.msg("请输入正确的身份照号码");
                // api.tools.toast('请输入正确的身份照号码');
                return;
            }
            if(legalName.length < 2){
                layer.msg("法人名称至少需要2个字");
                // api.tools.toast('法人名称至少需要2个字');
                return;
            }
            if(legalName.length > 5){
                layer.msg("法人名称不得超过4个字");
                // api.tools.toast('法人名称不得超过4个字');
                return;
            }

            if(micId == ''){
                layer.msg("请选择店铺类型");
                // api.tools.toast('请选择店铺类型');
                return;
            }

            if(openDate == ''){
                layer.msg("请选择开店时间");
                // api.tools.toast('请选择开店时间');
                return;
            }

            if(phoneNumber == ''){
                layer.msg("请填写店铺电话");
                // api.tools.toast('请填写店铺电话');
                return;
            }

            if(phoneNumber.length > 15){
                layer.msg("店铺电话最多只能输入15个数字");
                // api.tools.toast('店铺电话最多只能输入15个数字');
                return;
            }
            if(areaId == '' || areaId ==null){
                layer.msg("请选择所在地区");
                // api.tools.toast('请选择所在地区');
                return;
            }

            if(address == ''){
                layer.msg("请填写详细地址");
                // api.tools.toast('请填写详细地址');
                return;
            }
            if(address.length < 2){
                layer.msg("详细地址至少需要2个字");
                // api.tools.toast('详细地址至少需要2个字');
                return;
            }
            if(address.length > 25){
                layer.msg("详细地址最多只能25个字");
                // api.tools.toast('详细地址最多只能25个字');
                return;
            }
            if(lat==null || lat==""){
                layer.msg("请填写详细地址,搜索要开店的详细位置，点击提示的位置信息，再确定按钮！");
                return;
            }
            if(lng==null || lng==""){
                layer.msg("请填写详细地址,搜索要开店的详细位置，点击提示的位置信息，再确定按钮！");
                return;
            }
            var result={};
            result.id=ids;
            result.simpleName=simpleName;
            result.name=name;
            result.idCard=idCard;
            result.micId=micId;
            result.area=areaId;
            result.licensePic=licensePic;
            result.address=address;
            result.mobile=phoneNumber;
            result.legalName=legalName;
            result.localPhoto=localPhoto;
            result.recommendGoods=recommendGoods;
            result.openDate=openDate;
            result.lat = lat;
            result.lng = lng;
            console.log(result);

            var $this = $(this);
            $this.attr("disabled", true);
            var index = layer.load(2, {time: 10*1000});

            $.ajax({
                url: "${basePath!}/cmerchant/updateStore?type=${type!}",
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                contentType : 'application/json',
                data:JSON.stringify(result),
                success: function(data){ //成功
                    layer.close(index);
                    console.log(data);
                    if (data.code==0) {
                        //消息对话框
                        // layer.alert(data.msg);
                        window.setTimeout("window.location.href='${basePath!}/consumer/center/jumpMine'",1500);
                    } else if (data.code == 2) {
                        layer.msg(data.msg);
                        window.location.href = "${basePath!}/register/goMerchantAgreement";
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });


        var uploader = WebUploader.create({
            // 选完文件后，是否自动上传。
            auto: true,
            // swf文件路径
            swf: '${basePath!}/js/plugins/webuploader/Uploader.swf',
            // 文件接收服务端。
            server: '${basePath!}/cmerchant/upload',
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#img',
            // 只允许选择图片文件。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //地图加载
        var map = new AMap.Map("container", {
            resizeEnable: true
        });
        //输入提示

        var autoOptions = {
            input: "tipinput"
        };

        var auto = new AMap.Autocomplete(autoOptions);
        var placeSearch = new AMap.PlaceSearch({
            map: map
        });  //构造地点查询类
        AMap.event.addListener(auto, "select", select);//注册监听，当选中某条记录时会触发
        var aaa;
        var lng;
        var lat;
        var address;
        function select(e) {
            placeSearch.setCity(e.poi.adcode);
            placeSearch.search(e.poi.name);  //关键字查询查询
            var aaa=e.poi.name;
            console.log(aaa);
            var gps=[];
            gps.push(e.poi.location.lng);
            gps.push(e.poi.location.lat);
            address=e.poi.name;
            //精度
            lng=e.poi.location.lng;
            //维度
            lat=e.poi.location.lat;
            this.Gps=gps;

            console.log(this.Gps);
        }

        $("#quxiao").click(function () {
            $(".map_model").css("display","none");
            $("#address").val(address);
            $("#lng").val(lng);
            $("#lat").val(lat);
        });

    });

</script>
</html>
