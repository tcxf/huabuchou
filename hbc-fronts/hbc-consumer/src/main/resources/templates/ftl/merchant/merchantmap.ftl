<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>输入提示后查询</title>
    <style>
        .map_model{
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            overflow: hidden;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .amap-sug-result { z-index: 1050; }
    </style>
    <link rel="stylesheet" href="https://cache.amap.com/lbs/static/main1119.css"/>
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.8&key=130e68182526f47e2148ab8261a5c27e&plugin=AMap.Autocomplete,AMap.PlaceSearch"></script>
    <script type="text/javascript" src="https://cache.amap.com/lbs/static/addToolbar.js"></script>
</head>
<body>

<div class="map_model">
    <div id="container"></div>
    <div id="myPageTop">
        <table>
            <tr>
                <td>
                    <label>请输入关键字：</label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="tipinput"/>
                    <input type="button" value="确定" class="btn btn-info" id="qu">
                </td>
            </tr>
        </table>
    </div>
</div>



<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript">
        //打开地图
    $("#go_map").click(function () {
        $(".map_model").css("display","block")
    });
    //关闭地图
        $("#qu").click(function () {
            $(".map_model").css("display","none")
        });

    //地图加载
    var map = new AMap.Map("container", {
        resizeEnable: true
    });
    //输入提示

    var autoOptions = {
        input: "tipinput"
    };
    var gps;
    var auto = new AMap.Autocomplete(autoOptions);
    var placeSearch = new AMap.PlaceSearch({
        map: map
    });  //构造地点查询类
    AMap.event.addListener(auto, "select", select);//注册监听，当选中某条记录时会触发
    var lng;
    var lat;
    var address;
    function select(e) {
        placeSearch.setCity(e.poi.adcode);
        placeSearch.search(e.poi.name);  //关键字查询查询
        address=e.poi.name;
        let gps=[];
        gps.push(e.poi.location.lng);
        gps.push(e.poi.location.lat);
        //精度
         lng=e.poi.location.lng;
        //维度
         lat=e.poi.location.lat;

        this.Gps=gps;
        console.log(this.Gps);

        $("#qu").click(function () {
            window.location="${basePath!}/cmerchant/QueryMerchantQu?tipinput="+address+"&lng="+lng+"&lat="+lat;
        })
    }


</script>
</body>
</html>