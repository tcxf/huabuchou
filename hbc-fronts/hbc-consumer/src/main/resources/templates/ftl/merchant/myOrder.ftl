<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/footer.css"/>
    <title>商户订单</title>
    <style>
        .list_content{
            padding:0 30px;
            height: 43px;
            border-bottom:1px solid #dadada
        }
        .list_content li{
            text-align: center;
            margin-left:20px;
            margin-right:20px
        }
        .m_tab {
            line-height: 40px;
            height: 40px;
            background:transparent;

        }
        .m_content{
            margin-bottom: 110px;
        }
        .active>a{
            color: #f49110 !important;

        }

        .m_tab .active {
            border-bottom: 2px solid #f49110;

        }
        .m_tab a{
            color: #999;
            font-weight: 600;
            font-size: 0.8rem;
        }

        #user li{
            width:94%;
            list-style:none;
            padding:5px;
            background:#fff;
            margin-top:5px;
            margin-left:3%;
            border-radius:5px;
            box-shadow: 0 1px 10px #dadada;

        }
        #user li .col-xs-3{
            padding:0
        }
        #user li .col-xs-6 p{
            font-weight:600;
            color: #f49110;
        }
        #user li .col-xs-3:last-child{
            text-align:right;
            margin-top:47px;
            font-size:0.8rem;
        }
        #merchant li{
            width:94%;
            list-style:none;
            padding:5px;
            background:#fff;
            margin-top:5px;
            margin-left:3%;
            border-radius:5px;
            box-shadow: 0 1px 10px #dadada;

        }
        #merchant li .col-xs-3{
            padding:0
        }
        #merchant li .col-xs-6 p{
            font-weight:600;
            color: #f49110;
        }
        #merchant li .col-xs-3:last-child{
            text-align:right;
            margin-top:47px;
            font-size:0.8rem;
        }

        .jiazai{
            margin-top: 10px;
            width: 100%;
            text-align: center;
        }
        #dianji{
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="list_content">
    <div class="m_tab">
        <ul class="webkit-box">
            <li class="active wkt-flex"><a href="#user" data-toggle="tab">支付订单</a></li>
            <li class="wkt-flex"><a href="#merchant" data-toggle="tab">商户订单</a></li>
        </ul>
    </div>
</div>
<div class="m_content tab-content " >
    <div class="c_thing tab-pane active"  id="user">
        <div  id="user1">

        </div>
            <div class="jiazai" id="div1">
                <a href="javascript:void(0)" style="color: #999"  id="dianji1">点击查看更多</a>
            </div>
    </div>
    <div class="c_thing tab-pane"  id="merchant">
        <div  id="merchant1">

        </div>
        <div class="jiazai" id="div">
            <a href="javascript:void(0)" style="color: #999"  id="dianji">点击查看更多</a>
        </div>
    </div>
</div>
<div class="footer" id="footer">
    <ul>
        <li>
            <a href="${basePath!}/user/index/Goindex">
                <div class="toOne"></div>
                <p>首页</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                <div class="toTow"></div>
                <p>附近商家</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMyCredit">
                <div class="toThree"></div>
                <p>授信</p>
            </a>
        </li>
        <li class="active">
            <a href="${basePath!}/user/order/order" class ="redirect">
                <div class="toFou"></div>
                <p>订单</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMine">
                <div class="toFive"></div>
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<#--<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>-->
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script>
    $(document).ready(function() {
        $("#dianji1").click(function(){

            page1 = page1+1;
            loadOrderList();
        });
        var page1 = 1;
        var page = 1;
        var rows = 100;
        loadOrderList();
        function loadOrderList() {
            api.tools.ajax({
                url:'${basePath!}/user/order/findByMid',
                data:{
                    page:page1,
                    type:1
                }
            },function (d) {
                var d = d.data;
                if(d.records.length<10){
                    $('#div1').hide();
                }
                if(d!= null && d.records != null && d.records.length > 0){
                    for(var i = 0 ; i < d.records.length ; i++){
                        var data = d.records[i];
                        data.path = '${basePath!}';
                        var html = template.load({
                            host : '${basePath!}/',
                            name : 'order_list',
                            data : data
                        });
                        $html = $(html);

                        $html.attr("data",data.id);
                        //$html.attr("data1",data.consumeType);
                        $html.click(function(){
                            //if($(this).attr('data1')==2){
                            //window.location = '${basePath!}/rummery/orderDetail.htm?id='+$(this).attr('data');
                            //}else if($(this).attr('data1') ==1){
                            window.location = '${basePath!}/user/order/findById?id='+$(this).attr('data');
                            //}
                        });
                        $('#user1').append($html);
                    }
                }else{
                    if(page1 == 1){
                        // style="display:none"或者
                        // $("#div1").attr("style","display:none;");//隐藏div
                        $('#user1').append('<dl>'+
                                '<dt>'+
                                '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">'+
                                '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>'+
                                '</div>'+
                                '</dt>'+
                                '</dl>');
                    }
                }
            });
        }

        $("#dianji").click(function(){
            page = page+1;
            loadMorderList();
        });
        loadMorderList();
        function loadMorderList() {
            api.tools.ajax({
                url:'${basePath!}/user/order/findByMid',
                data:{
                    page:page,
                    type:2
                }
            },function (d) {
                var d = d.data;
                if(d.records.length<10){
                    $("#div").attr("style","display:none;");//隐藏div
                }
                if(d!= null && d.records != null && d.records.length > 0){
                    for(var i = 0 ; i < d.records.length ; i++){
                        var data = d.records[i];
                        data.path = '${basePath!}';
                        var html = template.load({
                            host : '${basePath!}/',
                            name : 'order_list',
                            data : data
                        });
                        $html = $(html);

                        $html.attr("data",data.id);
                        $html.click(function(){
                            window.location = '${basePath!}/user/order/mfindById?id='+$(this).attr('data');
                        });
                        $('#merchant1').append($html);
                    }
                }else{
                    if(page == 1){
                        // $('#div1').hide();
                        $('#merchant1').append('<dl>'+
                                '<dt>'+
                                '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">'+
                                '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>'+
                                '</div>'+
                                '</dt>'+
                                '</dl>');
                    }
                }
            });
        }
    });
</script>
</body>
</html>