<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet"
          href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/y.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />

    <title>我的详情</title>

    <style>
        body{
            background: #f5f5f5;
        }
        .a_text{
            padding:10px;
        }
        .text_one{
            width: 94%;
            height: auto;
            background: #fff;
            border-radius: 8px;
            margin-left: 3%;
            box-shadow: 0 1px 10px #dadada;
        }
        .text_one>ul{
            padding-left: 0;
        }
        .text_one li{
            list-style: none;
            border-bottom: 1px solid #ededed;
            padding:10px 0;
        }
        .text_one .container-fluid{
            padding-right: 0;
        }
        .text_one li .col-xs-8{
            padding: 0;
        }
        .text_one li .col-xs-8>p{
            color:#999;
            margin-bottom:0
        }
        .text_one li .one_img>img{
            width:30px;
            height: 30px;
            border-radius: 50%;
        }
        .text_one li:last-child{
            border-bottom: none;
        }
        .text_one li:nth-child(2) .col-xs-8>img{
            width: 15px;
        }
        .text_one li .col-xs-4{
            text-align: right;
            padding-right: 5px;
            color:#999;
            padding-left:0;
        }
        .text_one li:last-child .col-xs-4{
            color: #333;
        }
        .text_one li:last-child .col-xs-4>span{
            color: #f49110;
        }
        .tow_text{
            width: 94%;
            height: 50px;
            background: #fff;
            border-radius: 8px;
            margin-left: 3%;
            box-shadow: 0 1px 10px #dadada;
        }
        .tow_text .col-xs-9{
            padding-left: 5px;
            margin-top: 15px;
            padding-right:0;
        }
        .tow_text .col-xs-9>span{
            font-size:0.8rem
        }
        .tow_text .col-xs-1{
            padding-right: 0;
            margin-top: 15px;

        }
        .tow_text .col-xs-1>.tow_line{
            width: 1px;
            height: 20px;
            background: #999;
        }
        .tow_text .col-xs-2{
            padding:0;
            text-align:center;
            margin-top: 5px;
        }
        .tow_text .col-xs-2>a{
            color:#000;
            font-size:0.8rem
        }
        .tow_text .col-xs-2 div{
            color:#000;
            font-size:0.8rem
        }
        .three_text{
            width: 94%;
            height:auto;
            background: #fff;
            border-radius: 8px;
            margin-left: 3%;
            box-shadow: 0 1px 10px #dadada;
        }
        .three_text>ul{
            padding-left: 0;
            margin-bottom: 0;
        }
        .three_text li{
            list-style: none;
            border-bottom: 1px solid #ededed;
            padding:10px 0;
        }
        .three_text li:last-child{
            border-bottom: none;
        }
        .three_text li .col-xs-4{
            font-size: 0.8rem;
            color:#666
        }
        .three_text li .col-xs-8{
            padding-left: 0;
            font-size:0.8rem;
            color:#999
        }

        .top-hd {
            background: #ffffff;
        }

        .top-hd img {
            width: 65px;
            height: 65px;
        }
        .lijizhifu>a{
            padding: 5px 25px 5px 25px;
            background: #f49110;
            border-radius: 3px;
            color:#fff;
        }

        .row {
            margin-right: 0;
        }
        .scroll{
            position: relative;
            margin: 0 auto;
            overflow: hidden;

        }
        ul{
            margin: 0;
        }

        dl{
            background:rgba(0,0,0,0.7);
            position: fixed;
            right: 0;
            bottom: 50px;
            width: 300px;
            height: 70px;
            font-size: 0;
            transition: all 1s;
            border-radius: 8px;
        }
        dl.colse{
            right: -300px;
        }
        #bbb>img{
            width: 40px;
            height: 38px;
        }
        dl dt{
            position: absolute;
            top: 16px;
            left: -45px;
            width: 50px;
            height: 30px;
            line-height: 30px;
            font-size: 12px;
            text-align: center;
        }
        dl dd{
            display: inline-block;
            margin: 0;
            padding:0;
            width: 25%;
            font-size: 12px;
            text-align: center;
        }
        dl dd img{
            width: 25px;
            padding-top: 15px;
        }
        dl dd p{
            font-size: 0.8rem;
            color: #fff;
        }
        .a-btn{
            margin-top: 20px;
            background:#999;
            color:#ffffff;
            font-size: 1.0rem;
            height: 40px;
            line-height: 40px;
            width: 100%;
            text-align: center;
            letter-spacing: 2px;
        }
        .e-content{
            margin-top: 10px;
            background: #fff;
            padding: 10px;
        }
        .e-content .e-why{
            padding: 10px 0 10px 0;
            color:#666
        }
    </style>

</head>

<body>
<div class="a_text">
    结算详情
</div>
<#--<#if entity.isUseDiscout=='1'>-->
    <#--<div class="c-content" style="margin-top: 5px;">-->
        <#--<div class="webkit-box">-->
            <#--<div class="wkt-flex">优惠券</div>-->
            <#--<div id="top-yhj">已选一张</div>-->
            <#--<div class="wkt-flex text-right" style="color: #ff3327">-->
                <#---¥${order.freeAmount}</div>-->
        <#--</div>-->
    <#--</div>-->
<#--</#if>-->


<div class="text_one">
    <ul>
        <li>
            <div class="container-fluid">
                <div class="col-xs-8 one_img">
                    <img src="${entity.localPhoto!}" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" alt="">
                    <span>${entity.simpleName!}</span>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="col-xs-8">
                    <p>总计 ¥${entity.tradeAmount!} 优惠 ¥${entity.discountAmount!}</p>
                </div>
                <div class="col-xs-4">
                    实付&nbsp&nbsp <span>¥${entity.actualAmount!}</span>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="a_text">
    商家信息
</div>
<div class="tow_text">
    <div class="container-fluid" style="padding-right:2px">
        <div class=row>
            <div class=col-xs-9>
                <img src="${basePath!}/img/imgs/2.png"style="width:20px;height: 20px" alt=""/>
                <span>${entity.address!}</span>
            </div>
            <div class="col-xs-1">
                <div class="tow_line"></div>
            </div>
            <div class="col-xs-2" onclick="javascript:window.location='tel:${entity.mobile!}'">
                <a href="#">
                    <img src="${basePath!}/img/imgs/icon_hot_line_black.png" style="width: 15px;height: 15px" alt=""/>
                    <div>电话</div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="a_text">
    订单信息
</div>
<div class="three_text">
    <ul>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4">
                        订单编号
                    </div>
                    <div class="col-xs-8">
					${entity.serialNo!}
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4">
                        下单时间
                    </div>
                    <div class="col-xs-8">
					${entity.tradingDate!}
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4">
                        支付编号
                    </div>
                    <div class="col-xs-8">
					${entity.paymentSn!}
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4">
                        支付时间
                    </div>
                    <div class="col-xs-8">
					${entity.payDate!}
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4">
                        支付方式
                    </div>
                    <div class="col-xs-8">
                        <div class="col-xs-8">
						<#if  entity.paymentType =='1'>
                            授信支付
                        <#elseif   entity.paymentType=='2' >
							    钱包支付
                        <#elseif  entity.paymentType=='3' >
                            微信支付
                        <#elseif   entity.paymentType=='4' >
                            快捷支付
                        <#else >
                            未支付
                        </#if>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<#--<%-- <div class="scroll">-->
    <#--<dl id="aaa" class="colse">-->
        <#--<dt id="bbb"><img src="${basePath!} /img/imgs/img_quick_no.png" id="imgpp" alt=""/></dt>-->
        <#--<dd>-->
            <#--<a href="${basePath!} /p/index.htm">-->
                <#--<img src="${basePath!} /img/imgs/icon_quick_shouye.png" alt=""/>-->
                <#--<p>首页</p>-->
            <#--</a>-->
        <#--</dd>-->
        <#--<dd>-->
            <#--<a href="${basePath!} /b/bill.htm"> <img src="${basePath!} /img/imgs/icon_quick_shouxin.png"alt=""/>-->
                <#--<p>授信</p>-->
            <#--</a>-->
        <#--</dd>-->
        <#--<dd>-->
            <#--<a href="${basePath!} /orderSuper/orderCount.htm"> <img src="${basePath!} /img/imgs/icon_quick_dingdan.png"alt=""/>-->
                <#--<p>订单</p>-->
            <#--</a>-->
        <#--</dd>-->
        <#--<dd>-->
            <#--<a href="${basePath!} /Mysx.htm"> <img src="${basePath!} /img/imgs/icon_quick_wode.png"alt=""/>-->
                <#--<p>我的</p>-->
            <#--</a>-->
        <#--</dd>-->
    <#--</dl>-->
<#--</div> --%>-->
</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript"
        src="${basePath!}/js/jquery-qrcode-0.14.0.min.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {

        <#--$("#tj").click(function(){-->
            <#--window.location="${basePath!}/refund/refundAsk.htm?id=${order.id}";-->
        <#--});-->
    <#--<c:if test="${order.orderStatus == 0}">-->
                <#--$("#hk").click(function(){-->
                    <#--window.location = "${basePath!}/orderInfo/toPay.htm?id=${order.id}&name="+$("#gName").val();-->
                <#--});-->
        <#--$("#foot-fq").click(function(){-->
            <#--api.tools.confirm({-->
                <#--content:"是否确认取消订单？",-->
                <#--callback:function(){-->
                    <#--api.tools.ajax({-->
                        <#--url:'${basePath!}/orderInfo/cancel.htm',-->
                        <#--data:{-->
                            <#--id:'${order.id}'-->
                        <#--}-->
                    <#--},function(d){-->
                        <#--api.tools.toast(d.resultMsg);-->
                        <#--setTimeout(function(){-->
                            <#--window.location.reload();-->
                        <#--},1500);-->
                    <#--});-->
                <#--}-->
            <#--});-->
        <#--});-->
    <#--</c:if>-->

        <#--$(".goods-info").click(function(){-->
            <#--window.location = '${basePath!}/goodsInfo/detail.htm?id='+$(this).attr('data');-->
        <#--});-->
        <#--api.tools.qrCode("#code","${code}");-->
        <#--$(function() {-->
            <#--$("#bbb").on("click",function(){-->
                <#--$("#aaa").toggleClass("colse");-->
                <#--var imgObj=document.getElementById("imgpp");-->
                <#--if(imgObj.getAttribute("src",2)=="${basePath!} /img/imgs/img_quick_yes.png"){-->
                    <#--imgObj.src="${basePath!} /img/imgs/img_quick_no.png";-->
                <#--}else{-->
                    <#--imgObj.src="${basePath!} /img/imgs/img_quick_yes.png";-->
                <#--}-->
            <#--})-->
        <#--})-->
    <#--});-->
</script>

</html>