<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/balance_list.css">

    <title>结算订单</title>
    <style>

    </style>
</head>
<body>
<div class="balance">
    <h3>${entity.yesSetMoney!}</h3>
    <p>结算总额</p>
</div>
<div class="balance_li">
    <ul>
        <li>
            <h5>${entity.noSetMoney!}</h5>
            <p>已出结算单</p>
        </li>
        <li>
            <h5>${entity.noBill!}</h5>
            <p>未出结算单</p>
        </li>
    </ul>
</div>
<div id="settle">

</div>
<div  style="text-align: center;min-height: 10px">
</div>
<div id="div" style="text-align: center;min-height: 10px">
    <a href="javascript:void(0)" style="color: #999" id="dianji">点击查看更多</a>
</div>
</body>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {
        var page = 1;

        f();
        $("#dianji").click(function(){
            page = page+1;
            f();
        });
        function f() {
            api.tools.ajax({
                url: '${basePath!}/user/order/findSettlementDeta',
                data: {
                    page: page
                }
            }, function (d) {
                var d = d.data;
                if(d.records.length<10){
                    $("#div").hide();
                }
                if (d != null && d.records != null && d.records.length > 0) {
                    for (var i = 0; i < d.records.length; i++) {
                        var data = d.records[i];
                        if (data.status == 2) {
                            data.type = '已结算';
                        } else {
                            data.type = '待结算';

                        }
                        data.path = '${basePath!}';
                        var html = template.load({
                            host: '${basePath!}/',
                            name: 'settle_order',
                            data: data
                        });
                        $html = $(html);
                        $html.attr("data", data.id);
                        $html.click(function () {
                            window.location = '${basePath!}/user/order/findsettleOrderDetail?id=' + $(this).attr('data');
                        });

                        $('#settle').append($html);
                    }
                } else {
                    if (page == 1) {
                        $('#settle').append('<dl>' +
                                '<dt>' +
                                '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">' +
                                '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>' +
                                '</div>' +
                                '</dt>' +
                                '</dl>');
                    }
                }
            });
        }
    });
</script>
</html>