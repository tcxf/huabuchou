<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/balance_list.css">
    <title>结算详情</title>
</head>
<body>
<div class="inquiry">
     <span id ="span"></span>
    <input type="hidden" id="sid" value="${sid!}">


</div>
<div class="consumption" id="trading">
</div>
</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript"
        src="${basePath!}/js/jquery-qrcode-0.14.0.min.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {
        var page = 1;
        var sid=$("#sid").val();
        api.tools.ajax({
            url:'${basePath!}/user/order/findTradingDeta',
            data:{
                page:page,
                sid:sid

            }
        },function (d) {
            var d = d.data;
            if(d!= null && d.records != null && d.records.length > 0){
                for(var i = 0 ; i < d.records.length ; i++){
                    var data = d.records[i];
                    if (data.paymentType == 1) {
                        data.type = '授信支付';
                    } else if(data.paymentType == 2) {
                        data.type = '钱包支付';
                    }else if(data.paymentType == 3) {
                        data.type = '微信支付';
                    }else if(data.paymentType == 4) {
                        data.type = '快捷支付';
                    }else{
                        data.type = '未支付';
                    }
                    $("#span").html('订单号：'+data.serialNo);
                    data.path = '${basePath!}';
                    var html = template.load({
                        host : '${basePath!}/',
                        name : 'trading_list',
                        data : data
                    });
                    $html = $(html);
                    $('#trading').append($html);
                }
            }else{
                if(page == 1){
                    $('#trading').append('<dl>'+
                            '<dt>'+
                            '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">'+
                            '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>'+
                            '</div>'+
                            '</dt>'+
                            '</dl>');
                }
            }
        });
    });

</script>
</html>