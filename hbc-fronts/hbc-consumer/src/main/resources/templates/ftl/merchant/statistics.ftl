<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>统计</title>
    <style>
        .tongji_bg{
            margin-top: 10px;
            background: url(${basePath!}/img/imgs/tongji_bg.png)no-repeat;
            background-size: cover;
            width: 94%;
            height: 200px;
            margin-left: 3%;
            box-shadow: 0 0 5px #ededed;
        }
        .jiesuan-money{
            text-align: center;
            padding-top: 20px;
        }
        .jiesuan-money>h2{
            margin: 0;
            letter-spacing: 2px;
            color: #f4910f;
            font-weight: 600;
        }
        .jiesuan-money p{
            color: #999;
            letter-spacing: 1px;
        }
        .a_list{
            width: 100%;
            padding-top: 40px;
        }
        .a_list>ul{
            padding-left: 0;
        }
        .a_list li{
            list-style: none;
            float: left;
            width: 50%;
            text-align: center;
        }
        .a_list li>h4{
            margin-bottom: 5px;
            color: #f4910f;
            font-weight: 600;
            font-size: 3rem;
        }
        .a_list li p{
            font-size:1.6rem;
            color: #999;
        }
        .jiaoyi_tj{
            margin-top: 10px;
            width: 94%;
            height: 120px;
            margin-left: 3%;
            background: #fff;
            border-radius: 8px;
            box-shadow: 0 0 5px #ededed;
            padding-top: 10px;
        }

        .jiesuan_tj{
            margin-top: 10px;
            width: 94%;
            height: 120px;
            margin-left: 3%;
            background: #fff;
            border-radius: 8px;
            box-shadow: 0 0 5px #ededed;
            padding-top: 10px;
        }
        .container-fluid .col-xs-6>a{
            text-decoration: none;
            color: #999;
        }
        .container-fluid .col-xs-6 img{
            width: 10px;
            height: 12px;
        }
        .container-fluid  .col-xs-6:last-child {
            text-align: right;
        }
        .b_list{
            width: 100%;
            margin-top: 10px;
        }
        .b_list>ul{
            padding-left: 0;
        }
        .b_list li{
            list-style: none;
            float: left;
            width: 50%;
            text-align: center;
        }
        .b_list li:first-child{
            border-right: 1px solid #ededed;
        }
        .b_list li>h4{
            margin-bottom: 5px;
            color: #f4910f;
        }
        .b_list li p{
            color: #999;
        }
    </style>
</head>
<body>
<div class="tongji_bg">

    <div class="a_list">
        <ul>
            <li>
                <h4>${entity.dayTradingCount!}</h4>
                <p>日成交数 (笔) </p>
            </li>
            <li>
                <h4>${entity.dayTradingMoney!}</h4>
                <p>日成交额 (元) </p>
            </li>
        </ul>
    </div>
</div>
<div class="jiaoyi_tj">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                交易额统计
            </div>
            <div class="col-xs-6">
                <a href="${basePath!}/user/order/findTrading?sumMoney=${entity.sumTradingMoney!}">
                    详情
                    <img src="${basePath!}/img/imgs/right_enter.png" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="b_list">
        <ul>
            <li>
                <h4>${entity.sumTradingCount!}</h4>
                <p>交易总笔数 </p>
            </li>
            <li>
                <h4>${entity.sumTradingMoney!}</h4>
                <p>交易总额 </p>
            </li>
        </ul>
    </div>
</div>
<div class="jiesuan_tj">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                结算额统计
            </div>
            <div class="col-xs-6">
                <a href="${basePath!}/user/order/findSett">
                    详情
                    <img src="${basePath!}/img/imgs/right_enter.png" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="b_list">
        <ul>
            <li>
                <h4>
                        ${entity.noSetMoney!}
                </h4>
                <p>待结算总额 </p>
            </li>
            <li>
                <h4>
                   ${entity.yesSetMoney!}
                </h4>
                <p>已结算总额 </p>
            </li>
        </ul>
    </div>
</div>
<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<#include "../agreement/mune_btn.ftl">
</body>
</html>