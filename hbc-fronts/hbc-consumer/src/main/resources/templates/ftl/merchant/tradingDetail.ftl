<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/balance_list.css">

    <link rel="stylesheet" href="${basePath!}/js/picker/css/mui.picker.css" />
    <link rel="stylesheet" href="${basePath!}/js/picker/css/mui.dtpicker.css" />
    <link rel="stylesheet" href="${basePath!}/js/picker/css/mui.poppicker.css" />
    <title>交易额详情</title>
</head>
<body>
<div class="inquiry">
    <div class="inquiry_time">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="choice_time">
                        <div class="col-xs-5"  id="start">
                            2018-05-12
                        </div>
                        <div class="col-xs-2">
                            至
                        </div>
                        <div class="col-xs-5" id="end">
                            2018-05-18
                        </div>
                    </div>
                </div>
                <#--<div class="col-xs-2">-->
                    <#--查询-->
                <#--</div>-->
            </div>
        </div>
    </div>
    <div class="inquiry_text">
        <h3>${sumMoney!}</h3>
        <h6>扫码消费</h6>
    </div>
</div>
<div class="consumption" id="trading">
</div>
<div  style="text-align: center;min-height: 10px">
</div>
<div id="div" style="text-align: center;min-height: 10px">
    <a href="javascript:void(0)" style="color: #999" id="dianji">点击查看更多</a>
</div>
</body>


<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery-qrcode-0.14.0.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.dtpicker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<script src="${basePath!}/js/template/template.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    getNowFormatDate();
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        $("#start").html(currentdate);
        $("#end").html(currentdate);
    }



    $(document).ready(function() {
        var n = new Date();
        var dtStartPicker = new mui.DtPicker({
            type: 'date',
            endDate: new Date(2018,6,16)
        });

        var dtEndPicker = new mui.DtPicker({
            type: 'date',
            beginDate: new Date(2018,5,16)
        });

        $("#start").click(function(){
            dtStartPicker.show(function(s) {
                $("#start").html(s.y.text + "-" + s.m.text + "-" + s.d.text);
                start = s.y.text + "-" + s.m.text + "-" + s.d.text;
                dtEndPicker = new mui.DtPicker({
                    type: 'date',
                    beginDate: new Date(s.y.text,parseInt(s.m.text) - 1,s.d.text)
                });
                page = 1;
                $('#trading').empty();
                loadDate();
            });
        });

        $("#end").click(function(){
            dtEndPicker.show(function(s) {
                $("#end").html(s.y.text + "-" + s.m.text + "-" + s.d.text);
                end = s.y.text + "-" + s.m.text + "-" + s.d.text;
                dtStartPicker = new mui.DtPicker({
                    type: 'date',
                    endDate: new Date(s.y.text,parseInt(s.m.text) - 1,s.d.text)
                });
                page = 1;
                $('#trading').empty();
                loadDate();
            });
        });

        start = "null";
        end = "null";

        var page = 1;
        loadDate();
        function loadDate() {
            api.tools.ajax({
                url: '${basePath!}/user/order/findTradingDeta',
                data: {
                    page: page,
                    createDateStr: start,
                    modifyDateStr: end
                }
            }, function (d) {
                var d = d.data;
                if(d.records.length<10){
                    $("#div").hide();
                }
                if(d.records.length>=10){
                    $("#div").show();
                }
                if (d != null && d.records != null && d.records.length > 0) {
                    for (var i = 0; i < d.records.length; i++) {
                        var data = d.records[i];
                        if (data.paymentType == 1) {
                            data.type = '授信支付';
                        } else if(data.paymentType == 2) {
                            data.type = '钱包支付';
                        }else if(data.paymentType == 3) {
                            data.type = '微信支付';
                        }else if(data.paymentType == 4) {
                            data.type = '快捷支付';
                        }else{
                            data.type = '未支付';
                        }
                        data.path = '${basePath!}';
                        var html = template.load({
                            host: '${basePath!}/',
                            name: 'trading_list',
                            data: data
                        });
                        $html = $(html);
                        $('#trading').append($html);
                    }
                } else {
                    if (page == 1) {
                        $('#trading').append('<dl>' +
                                '<dt>' +
                                '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">' +
                                '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>' +
                                '</div>' +
                                '</dt>' +
                                '</dl>');
                    }
                }
            });
        }
        $("#dianji").click(function(){
            page = page+1;
            loadDate();
        });
    });

</script>
</html>