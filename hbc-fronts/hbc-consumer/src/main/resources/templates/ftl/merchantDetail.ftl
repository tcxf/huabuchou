<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="<%=path %>/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=path %>/css/app.css" />
		<link rel="stylesheet" href="<%=path %>/css/zy.css" />
		<link rel="stylesheet" href="<%=path %>/css/y.css" />
        <link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
		
		<title>${m.simpleName}</title>
	    <style>
	        
	    <style>
	        #top-yh h4{
	            margin-top: 28px;
	        }
	        #top-yh p{
	            padding-top: 9px;
	            font-size: 12px;
	        }
	        .active>a{
	            color: #074314 !important;
	        }
	        .m_tab a{
	            color: #333333;
	        }
	        .c_thing .thing_left img{
	            width: 70px;
	            height: 70px;
	        }
	        .c_thing .thing_left p{
	            margin-bottom: 12%;
	        }
	        .c_thing .thing_right{
	            padding-top: 47px;
	        }
			.c_toutu img{ border-radius:50px; width:10%;}
	        .mui-active {
	            color: #074314 !important;
	        }
			#sp img{
				width: 70px;
				height: 70px;
			}
	        .yhj{
	            width: 100%;
	            background:url(/img/img/coupon_green1.png) no-repeat;background-size:100%;
	        }
		        
				        
	        body{
	            background: #eee;
	        }
	        .content>img{
	            margin-top: 15%;
	            width: 20%;
	            height: auto;
				margin-bottom:10px;
	        }
	        .content h3{
	            font-size: 1em;
	            color: #999999;
	        }
	    </style>
	</head>

	<body>
		<div class="container">
		    <div class="row" id="top-yh">
		        <div class="col-xs-2 c_toutu">
		            <img src="${res}${m.localPhoto}" alt=""/>
		        </div>
		        <div class="col-xs-9 col-xs-offset-1" style="margin-top:20px;">
		            <h4 style="font-size: 16px;color: #000000">${m.simpleName}</h4>
		            <p>${m.address}</p>
		        </div>
		    </div>
		</div>
		<div class="m_tab">
		    <ul class="webkit-box">
		        <li class="active wkt-flex"><a href="#all" data-toggle="tab">商品</a></li>
		        <li class="wkt-flex"><a href="#dfk" data-toggle="tab">优惠券</a></li>
		    </ul>
		</div>
		<div class="m_content tab-content " style="margin-top: 5px">
    		<div class="c_thing tab-pane active" id="all">
    			<ul id="sp">
    				
    			</ul>
    		</div>
    		<div class="c_yhq tab-pane" id="dfk" style="margin-top: -7px">
    			<ul id="yhq">
    				
    			</ul>
    		</div>
    	</div>
	</body>
    <script type="text/javascript" src="<%=path %>/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
	<script type="text/javascript" src="<%=path %>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
	<script type="text/javascript" src="<%=path %>/template/template.js"></script>
	<script>
		$(document).ready(function() {
			api.tools.ajax({
				url:'<%=path%>/goodsInfo/loadGoodsInfo.htm',
				data:{
					miId:'${m.id}'
				}
			},function(d){
				if(d.data!= null && d.data.length > 0){
					for(var i = 0 ; i < d.data.length ; i++){
						var data = d.data[i];
						data.img = '${res}'+data.img;

						var html = template.load({
							host : '<%=path%>/',
							name : "merchant_goods_list",
							data : data
						});
						$html = $(html);
						$html.attr('data',data.id);
						$html.click(function(){
							window.location = '<%=path%>/goodsInfo/detail.htm?id='+$(this).attr('data');
						});
						$('#sp').append($html);
					}
				}else{
					$('#sp').parent().html('<div class="content" align="center" style="height:100%;position:absolute;">'+
						    '<img src="/img/img/icon_zanwujilu.png" alt=""/>'+
						    '<h3>暂无记录...</h3>'+
						'</div>');
				}
			});
			loadRedPacket();
			function loadRedPacket(){
				$('#yhq').empty();
				api.tools.ajax({
					url:'<%=path%>/redpacket/loadRedPacket.htm',
					data:{
						miId:'${m.id}'
					}
				},function(d){
					if(d.data != null && d.data.length > 0){
						for(var i = 0 ; i < d.data.length ; i++){
							var data = d.data[i];
							data.img = '${res}'+data.img;
	
							var html = template.load({
								host : '<%=path%>/',
								name : "merchant_red_packet_setting",
								data : data
							});
							$html = $(html);
							if(data.isGet){
								$html.find(".get-red").html("已领取");
							}else{
								$html.find(".get-red").click(function(){
									var rpsId = $(this).attr("data");
									api.tools.ajax({
										url:'<%=path%>/redpacket/getRedPacket.htm',
										data:{
											rpsId:rpsId
										}
									},function(d){
										api.tools.toast(d.resultMsg);
	
										api.tools.toast(d.resultMsg);
										if(d.resultCode != -1){
											loadRedPacket();
										}
									});
								});
							}
							$('#yhq').append($html);
						}
					}else{
						$('#yhq').parent().html('<div class="content" align="center" style="height:100%;position:absolute;margin-top:7px">'+
							    '<img src="/img/img/icon_zanwujilu.png" alt=""/>'+
							    '<h3>暂无记录...</h3>'+
							'</div>');
					}
				});
			}
		});
	</script>

</html>