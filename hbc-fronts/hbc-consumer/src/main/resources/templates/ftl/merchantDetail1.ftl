
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zybl.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



    <link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
    <title>商户详情</title>
    <style>
        .c-content{
            margin-top: 0;
            padding: 10px 8px 0px 10px;
        }
        @media (min-width: 768px) and (max-width: 979px){
            .c-content{
                margin-top: 0;
                padding: 28px 8px 0px 10px;
            }

        }
        @media (min-width:979px) and (max-width: 1200px){
            .c-content{
                margin-top: 0;
                padding: 28px 8px 0px 10px;
            }

        }
        .c-content .dizhi .m-add{
            font-size: 1rem;margin-left:2%;margin-top:1%
        }
        .webkit-box img {
            width: 38%;
            margin-top:0;
        }
        .dizhi{
            padding:10px;
        }
        .t-text{
            margin-left: -2%;
        }
        .t-text>h4{
            font-size: 1.1rem;
        }
        .t-text p{
            color: red;
            padding-top: 5%;
        }
        .t-text img{
            margin-top: -1rem;
        }
        .text-right p{
            margin-top: 2.9rem;
            font-size: 0.8rem;
        }

        .m_tab a:hover{
            color: #f49110 !important;
        }
        .m_tab a{
            color: #666666;
        }
        .m_tab .active {
            border-bottom: 2px solid #f49110;
            color: #f49110;
        }
        .thing_left p{
            line-height: 14px;
        }
        .c_thing ul li{
            padding: 10px 10px 2px 10px ;
        }
        .c_thing .thing_right {
            float: right;
            width: 30%;
            font-size: 0.9em;
            text-align: right;
            color: #666;
            padding-top: 12px;
        }
        .sms-btn {
            border: 1px solid #f49110;
            color: #f49110 !important;
            border-radius: 20px;
        }

        .c_thing .thing_left img {
            float: left;
            margin-right: 10px;
            width: 60px;
            height: 60px;
            clear-both:none
        }
        .c_shop .shop_left img {
            float: left;
            margin-right: 10px;
            width: 60px;
            height: 60px;
        }
        .c_shop ul li {
            padding: 10px 10px 0 10px;
            border-bottom: 1px solid #eee;
            height: auto;
            overflow: hidden;
        }
        .c_yhq ul li {
            padding: 2px 0px;
            height: auto;
            overflow: hidden;
        }
        .hk-money {
            margin-top: 0px;
            background: #ffffff;
            color: #333;
            text-align: center;
            height: 72px;
            padding-top: 0;
        }
        .hk-money div {
            margin-top:4px;
            font-weight: 400;
        }
        .webkit-box h4{
            white-space:nowrap;
            overflow:hidden;
            text-overflow:ellipsis;
        }
        .dingwei{
            padding:5px;
        }
        .dingwei .col-xs-1{
            padding:10px 5px;
        }
        .dingwei .col-xs-2{
            padding:0;
            text-align:center;
            border-left:1px solid #ededed;
        }
        .dingwei .col-xs-2>a{
            color:#000;
            font-size:0.8rem
        }
        .dingwei .col-xs-9{
            padding:10px 0;
        }
        .m_tab li{
            margin-left:40px;
            margin-right:40px
        }
        .m_tab .active a{
            color:#f49110
        }
        .youhuijuan{
            width:100%;
            height:60px;
            background:#fff;
            padding:10px 5px;
        }
        .youhuijuan p{
            margin-bottom:5px;
            font-size:1rem;
            font-weight:600;
            color:#333
        }
        .youhuijuan .col-xs-4{
            width:40%;
            padding-right:0;
            text-align:right;
            padding:10px
        }
        .youhuijuan .col-xs-4>a{
            padding:6px 8px;
            background:#f49110;
            color:#fff;
            border-radius:20px;
            font-size:0.8rem;
            letter-spacing: 2px;
            text-decoration: none;
        }
        .m_content{
            width:100%;
            margin-top:0
        }

    </style>
</head>
<body>
<div class="c-content" style="padding:10px 0 0 10px">
    <div class="hk-money">
        <div class="wkt-flex text-left">
            <img src="${entity.localPhoto!}" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" style="width: 80px;height:80px;border-radius:10px;margin-top: -2%" alt=""/>
            <h4 style="font-size: 1rem;margin-top: -5.5rem;margin-left: 7rem;font-weight: 600;">${entity.name!}</h4>
            <div style="margin-left: 7rem">
                <#if entity.ctype==0 >
                    <img data="${entity.id!}" id="collect" src="${basePath!}/img/imgs/icon_star_no.png"  style="width:15px; height:15px;margin-top:5px" alt=""/>
                    <span  id="span" style="font-size:0.8rem;color:#ffa700">收藏</span>
                <#else >
                    <img data="${entity.id!}" id="collect"  src="${basePath!}/img/imgs/icon_star_yes.png"  style="width:15px; height:15px;margin-top:5px"" alt=""/>
                    <span id="span" style="font-size:0.8rem;color:#ffa700">取消收藏</span>
               </#if>
            </div>
        </div>

    </div>
    <div class="gwl" style="margin-top: 8px; margin-bottom: 4px;"></div>
    <div class="container-fluid dingwei">
        <div class=row>
            <div id="location">
            <div class=col-xs-1 >
                <img  src="${basePath!}/img/imgs/2.png"style="width:20px;height: 20px" alt=""/>
            </div>
            <div class="col-xs-9">
            ${entity.address! }
            </div>
            </div>
            <div class="col-xs-2">
                <a href="tel:${entity.mobile! }">
                    <img src="${basePath!}/img/imgs/icon_hot_line_black.png" style="width: 15px;height: 15px" alt=""/>
                    <div>电话</div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="m_tab" style="margin-top: 2%">
    <ul class="webkit-box">
            <li class="active wkt-flex"><a href="#dfk" data-toggle="tab">优惠券</a></li>

        <li class="wkt-flex"><a href="#shop" data-toggle="tab">店铺详情</a></li>
    </ul>
</div>
<div class="m_content tab-content">


        <!-------------商品结束----------------->
        <div class="c_yhq tab-pane active" id="dfk">
            <ul id="yhq">

            </ul>
        </div>
    <div class="c_shop tab-pane" id="shop" style="margin-top:2px">
        <div class="p-content" style="color: #000;padding:5px 21px 0px 22px">
            开业时间：<span style="color:#999"> ${entity.createDate! }</span>
            <p style="margin-top: 3%;color: #000;">
                推荐：<span style="color:#999">${entity.recommendGoods!}</span>
            </p>
        </div>
    </div>
</div>


<#include "./agreement/mune_btn.ftl">
</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.cookie.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>

<script>
    $(document).ready(function() {
        var invateCode ='${invateCode!}';
        if(invateCode!=""&&invateCode!=null){
            window.sessionStorage.setItem("invateCode",invateCode);
        }
        $("#location").click(function () {
            var id='${id}';
            window.location = '${basePath!}/nearby/dt?id='+id;
        })
        $("#collect").click(function () {
            if(${entity.ctype}==0){
                var url ='${basePath!}/consumer/center/addCollect';
            }else{
                var url='${basePath!}/consumer/center/deleteByMid';
            }

            api.tools.ajax({
                url:url,
                data:{
                    mid:'${entity.id!}'

                }
            },function(d){
                window.location = '${basePath!}/nearby/findById?id='+$("#collect").attr('data');
            });

        })
        loadRedPacket();
        function loadRedPacket(){
            $('#yhq').empty();
            api.tools.ajax({
                url:'${basePath!}/nearby/findByUserId',
                data:{
                    miId:'${entity.id!}'

                }
            },function(d){
                if(d.data != null && d.data.length > 0){
                    for(var i = 0 ; i < d.data.length ; i++){
                        var data = d.data[i];
                        data.path='${basePath!}';
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "merchant_red_packet_setting",
                            data : data
                        });
                        $html = $(html);
                        if(data.utype>=1){
                            $html.find(".get-red").html("已领取");
                            $html.find(".get-red").css("background","#999");
                        }else{
                            $html.find(".get-red").click(function(){
                                var rpsId = $(this).attr("data");
                                api.tools.ajax({
                                    url:'${basePath!}/user/Ucoupon/getCoupon',
                                    data:{
                                        id:rpsId
                                    }
                                },function(d){
                                    api.tools.toast(d.msg);


                                        loadRedPacket();

                                });
                            });
                        }
                        $('#yhq').append($html);
                    }
                }else{
                    $('#yhq').parent().html('<div class="content" align="center" style="position:absolute;background:#efeff4">'+
                            '<img src="${basePath!}/img/imgs/icon_zanwujilu.png" alt=""/>'+
                            '<h5>暂无记录...</h5>'+
                            '</div>');
                };
            });
        };
    });
</script>
</html>