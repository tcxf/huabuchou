<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/myindex.css" />
    <link rel="stylesheet" href="${basePath!}/css/footer.css"/>
    <title>我的</title>
    <style>
        .a_top .col-xs-2 img{
            width: 20px;
        }
        .a_top .tow_row>.col-xs-5>a{
            color: #fff;
            text-decoration: none;
        }
        .b-content{
            width: 100%;
            height: 70px;
            border-bottom: 1px solid #ededed;
            padding: 10px;
        }
        .b-content>ul{
            padding-left: 0;
        }
        .b-content li{
            list-style: none;
            width: 50%;
            height: 40px;
            line-height: 40px;
            float: left;
            text-align: center;
        }
        .b-content li:first-child{
            border-right: 1px solid #ededed;
        }
        .b-content li>a{
            text-decoration: none;
        }
        .b-content li img{
            width:20px;
        }
        .b-content li span{
            margin-top: 10px;
            color: #666;
        }
        .b_content-list{
            width: 100%;
            height: 100px;
            padding: 20px 5px;
        }
        .b_content-list>ul{
            padding-left: 0;
        }
        .b_content-list li{
            list-style: none;
            width: 25%;
            float: left;
            text-align: center;
            margin-bottom: 20px;
        }
        .b_content-list li>a{
            text-decoration: none;
        }
        .b_content-list li img{
            width:25%;
            height: 20px;
        }
        .b_content-list li p{
            margin-top: 10px;
            color: #666;
        }
        .tanchu{
            width: 94%;
            height: auto;
            background: #fff;
            border-radius: 8px;
            margin-left: 3%;
            padding: 5px 0 20px 0;
        }
        #erweima canvas{
            border: 1px solid #ededed;
            border-radius: 8px;
        }
        #erweima2 canvas{
            width: 120px;
            height: 120px;
            border: 1px solid #ededed;
            border-radius: 8px;
            margin-top: 20px;
        }
        .tanchu>.modal-colse{
            text-align: right;
            padding-right:10px;
        }
        .tanchu>.modal-colse>img{
            width: 15px;
        }
        .tanchu>.modals-content>p{
            color: #999;
        }
        .a_top .tow_row>.col-xs-8 p {
            color: #fff;
        }
        .modals-content{
            margin-top: 20px;
        }
        .modal-foot{
            margin-top: 20px;
        }
        .modal-foot a{
            padding: 3px 8px;
            border-radius: 30px;
            background: #f49110;
            color: #fff;
            text-decoration: none;
            font-size: 1rem;
        }
        .one_row p{
            color: #fff;
        }
        .one_row a{
            color: #fff;
        }
        .abtn{
            width:40px;
            height: 20px;
            line-height: 20px;
            border: 1px solid #fff;
            background:transparent ;
            border-radius: 30px;
            color: #fff;
        }
        .bbtn{
            width:40px;
            height: 20px;
            line-height: 20px;
            border: 1px solid #f49110;
            background:transparent ;
            border-radius: 30px;
            color: #f49110;
        }
    </style>
</head>
<body>
<div class="a_top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                <a href="${basePath!}/user/userInfo/userSetting">
                    <img src="${basePath!}/img/ico_shezhi.png" alt="">
                </a>
            </div>
        </div>
        <div class="row tow_row">
            <div class="col-xs-4">
                <a href="${basePath!}/user/userInfo/selectUserInfo">
                    <img src="${basePath!}/img/shouxin.png" style=" width: 60px;height: 60px; border-radius: 50%;" alt="" id="headUrl">
                    <p id="realName"></p>
                </a>
            </div>
            <div class="col-xs-8">
                <div id="erweima">
                </div>
                <p> 邀请码: <span><input class="a_copy_content" id="text" value=""  readonly= "true " style="width: 110px;background: transparent;border:none;outline:none"> </span><span> <button class="abtn" onclick="myCopy()">复制</button></span></p>
            </div>
        </div>
        <div class="modals" id="mymodal1">
            <div class="tanchu">
                <div class="modal-colse">
                    <img src="${basePath!}/img/imgs/ico_colseImg.png" alt="">
                </div>
                <div class="modals-content" id="erweima2">
                    <p>扫我邀请朋友注册花不愁商城</p>
                </div>
                <div class="modal-foot">
                    <p> 邀请码: <span><input class="a_copy_content" id="texta" value=""  readonly= "true " style="width:110px;background: transparent;border:none;outline:none"> </span> &nbsp<span> <button class="bbtn" onclick="amyCopy()">复制</button></span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="b-content">
    <ul>
        <li>
            <a href="${basePath!}/wallet/findwallet">
                <img src="${basePath!}/img/ico_qianbaozhifu.png" alt="">
                <span>钱包</span>
            </a>
        </li>
        <li>
            <a href="${basePath!}/bank/gobank">
                <img src="${basePath!}/img/ico_card.png" alt="">
                <span>银行卡</span>
            </a>
        </li>
    </ul>
</div>
<div class="b_content-list">
    <ul>
        <li>
            <a href="${basePath!}/user/Ucoupon/coupon">
                <img src="${basePath!}/img/imgs/consumer_my_yhj.png" style="width: 30px" alt="">
                <p>我的优惠券</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/merchant/center/jumpMyCollect">
                <img src="${basePath!}/img/imgs/consumer_my_sc.png" alt="">
                <p>我的收藏</p>
            </a>
        </li>
        <li style="display: none">
            <a href="${basePath!}/consumer/center/jumpScore">
                <img src="${basePath!}/img/imgs/consumer_my_jf.png" alt="">
                <p>我的积分</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMyPayback">
                <img src="${basePath!}/img/imgs/consumer_my_hkjl.png" alt="">
                <p>还款记录</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpAboutUs">
                <img src="${basePath!}/img/imgs/consumer_my_gywm.png" alt="">
                <p>关于我们</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/userInfo/goUserSetPassword">
                <img src="${basePath!}/img/myshop.png" alt="">
                <p>立即开店</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpHelp">
                <img src="${basePath!}/img/imgs/consumer_my_bz.png" alt="">
                <p>帮助</p>
            </a>
        </li>
    </ul>
</div>
<div class="footer" id="footer">
    <ul>
        <li>
            <a href="${basePath!}/user/index/Goindex">
                <div class="toOne"></div>
                <p>首页</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                <div class="toTow"></div>
                <p>附近商家</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMyCredit">
                <div class="toThree"></div>
                <p>授信</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/order/order" class ="redirect">
                <div class="toFou"></div>
                <p>订单</p>
            </a>
        </li>
        <li class="active">
            <a href="${basePath!}/consumer/center/jumpMine">
                <div class="toFive"></div>
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
    <script src="${basePath!}/js/jquery-1.11.3.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/jquery.qrcode.js"></script>
    <script src="${basePath!}/js/qrcode.js"></script>

    <script>
        api.tools.ajax({
            url:'${basePath!}/consumer/center/findUserInfo'
        },function(d){
            if (d.data.headImg == "images/uhead.png")
            {
                $('#headUrl').attr('src',"${basePath!}/"+d.data.headImg); //头像路径
            }else {
                $('#headUrl').attr('src',d.data.headImg); //头像路径
            }
            if(d.data.realName){
                $('#realName').html(d.data.realName); //名字
            }else{
                $('#realName').html(d.data.mobile); //手机号
            }

            $('.a_copy_content').val(d.data.invateCode); //邀请码
        });

        $(document).ready(function() {
            var qrCode="${basePath!}/register/goRegister?type=1?&&invateCode=${invateCode!}";
            $('#erweima').qrcode({width:60,height:60,correctLevel:0,text:qrCode});
            $('#erweima2').qrcode({width:183,height:182,correctLevel:0,text:qrCode});
        });


        $("#erweima").click(function () {
            document.getElementById("mymodal1").style.display="block";
        });
        $(".modal-colse>img").click(function(){
            document.getElementById("mymodal1").style.display="none";
        });


        function myCopy(){
            var ele = document.getElementById("text");//ele是要复制的元素的对象
            ele.focus();
            ele.setSelectionRange(0, ele.value.length);
            if(document.execCommand('copy', false, null)){
                alert("复制成功")
            }
        }
        function amyCopy(){
            var ele = document.getElementById("texta");//ele是要复制的元素的对象
            ele.focus();
            ele.setSelectionRange(0, ele.value.length);
            if(document.execCommand('copy', false, null)){
                alert("复制成功")
            }
        }

    </script>
</body>
</html>