<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />

    <title>修改登录密码</title>
    <style>
        body
        {
            background:url(${basePath!}/img/beijing.png)no-repeat;
            background-size: cover;

        }
        .layui-m-layer {
            z-index: 0 !important;
        }
        .a-content{
            margin-top:60%
        }
        .tj{
            background: #f49110;
            color:#ffffff;
            border-radius: 30px;
            font-size: 0.8rem;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            box-shadow: 0 1px 10px #f49110;
            text-align: center;

        }
        .login-model {
            width: 85%;
            padding: 20px 0px 20px 5px;
        }
        .disabled{
            background:#ccc;
        }
        #btn{
            background: transparent;
            border: 1px solid #f49110;
            outline: none;
            border-radius: 30px;
            font-size: 0.8rem;
            color: #f49110;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer
        }
    </style>
</head>

<body>
<div class="a-content login-model">
    <div class="webkit-box ">
        <div style="line-height: 30px; width: 11%;">
            <img src="${basePath!}/img/ico_zhanghao.png" />
        </div>
        <div class="wkt-flex">
            <input type="tel" id="mobile" name="mobile" class="form-control reg-input" placeholder="请输入您的账号" style="width: 100%; height: 30px; padding-left: 10px; font-size: 14px;">
        </div>
    </div>

    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
    <div class="webkit-box ">
        <div style="line-height: 30px; width: 11%;margin-top: 0.3em;">
            <img src="${basePath!}/img/poss_word.png" />
        </div>
        <div class="wkt-flex" style="padding-top: 5px">
            <input type="tel" id="yCode" name="yCode" class="form-control reg-input" placeholder="输入验证码" style="width: 100%; height: 30px; padding-left: 10px; font-size: 14px;">
        </div>
        <div style="margin-top: 3%">
            <input type="button" id="btn" value="获取验证码" />
        </div>
    </div>
    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
</div>
<div>
    <button class="btn-block" id="ok">下一步</button>
</div>
</body>

<script src="${basePath!}/js/jquery-1.11.3.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>.
<script type="text/javascript" src="${basePath!}/js/layer/layer.js"></script>
<script src="${basePath!}/js/ajax.js"></script>
<#include "../agreement/mune_btn.ftl">

<script>
    $(document).ready(function () {

        var i = 60;
        function startTime() {
            $("#btn").val("获取验证码(" + i + "秒)");
            if (i > 0) {
                i--;
                setTimeout(function () {
                    startTime();
                }, 1000);
            } else {
                $("#btn").val("获取验证码");
                $("#btn").attr("disabled", false);
                i = 60;
            }
        }

        $("#btn").click(function () {
            var mobile = $("#mobile").val();
            if (mobile == null || mobile == "") {
                layer.msg("请输入手机号码");
                return;
            }
            var re = /^1\d{10}$/;
            if (!re.test(mobile)) {
                layer.msg("请输入正确的手机号码");
                return;
            }
            $("#btn").attr("disabled", true);
            var url = "${basePath!}/cuser/getYzm?mobile=" + mobile + "&type=YZM";
            var data = {};
            ajax.loadService(url, data, function (data) {
                layer.msg(data.msg);
                startTime();
            });
        });

        $("#ok").click(function () {
            var mobile = $("#mobile").val();
            var yCode = $("#yCode").val();
            if (mobile == null || mobile == "") {
                layer.msg("请输入手机号码");
                return null;
            }
            var re = /^1\d{10}$/;
            if (!re.test(mobile)) {
                layer.msg("请输入正确的手机号码");
                return;
            }
            if (yCode == null || yCode == "") {
                layer.msg("请输入验证码");
                return null;
            }
            var index = layer.load();
            api.tools.ajax({
                url: "${basePath!}/user/userInfo/yzm",
                data: {
                    mobile: $("#mobile").val(),
                    yCode: $("#yCode").val()
                }
            }, function (d) {
                layer.close(index);
                if (d.code == "0") {
                    layer.msg("验证成功");
                    window.location.href = "${basePath!}/user/userInfo/setUpdatePassword?mobile=" + d.data;
                }else{
                    layer.msg(d.msg);
                }
            });
        });
    });
</script>

</html>
