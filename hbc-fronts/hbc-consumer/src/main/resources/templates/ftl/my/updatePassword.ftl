<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/balance_list.css">
    <title>设置登录密码</title>
    <style>
        body{
            background: url(${basePath!}/img/beijing.png)no-repeat;
            background-size: cover;
        }
        .btn-block{
            margin-top: 20px;
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer
        }
    </style>
</head>
<body>
    <div class="set_password">
        <ul>
            <li>
                <img src="${basePath!}/img/poss_word.png" alt="">
                <input type="password" placeholder="请设置您的登录密码" id="newpwd" name="newpwd" MaxLength="16"/>
            </li>
            <li>
                <img src="${basePath!}/img/poss_word.png" alt="">
                <input type="password" placeholder="请再次确认您的登录密码" id="password" name="password" MaxLength="16" />
            </li>
        </ul>

            <button class="btn-block" id="ok">确认</button>

    </div>
</body>

<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
        $("#ok").click(function () {
            var newpwd = $.trim($('#newpwd').val());
            var password = $.trim($('#password').val());
            //是否输入的是阿拉伯数字
            var reg = new RegExp("^\\d{6}$");
            if (newpwd.length < 6 || newpwd.length > 16 ) {
                layer.msg("密码必须是6-16位");
                return;
            }
            if (newpwd.length < 6 || newpwd.length > 16  ) {
                layer.msg("密码必须是6-16位");
                return;
            }

            if (newpwd != password) {
                layer.msg("输入的两个密码不一致");
                return;
            }
            var index = layer.load();
            api.tools.ajax({
                url: "${basePath!}/user/userInfo/updatePassword",
                data: {
                    mobile:"${mobile!}",
                    password: $("#password").val()
                }
            }, function (d) {
                layer.close(index);
                if (d.code == 0) {
                    layer.msg(d.msg);
                    window.location.href = "${basePath!}/consumer/center/jumpMine";
                } else {
                    layer.msg(d.msg);
                    $this.removeClass("disabled");
                }
            });
        });
</script>
</html>