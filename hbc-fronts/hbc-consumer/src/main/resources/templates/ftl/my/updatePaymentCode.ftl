<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/balance_list.css">
    <title>设置支付密码</title>
    <style>
        body{
            background: url(${basePath!}/img/beijing.png)no-repeat;
            background-size: cover;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            cursor: pointer
        }
    </style>
</head>
<body>
    <div class="set_password">
        <ul>
            <li>
                <img src="${basePath!}/img/poss_word.png" alt="">
                <input type="password" placeholder="请设置您的支付密码" onkeypress="keypressHandler(event)" MaxLength="6" id="newpwd" name="newpwd"/>
            </li>
            <li>
                <img src="${basePath!}/img/poss_word.png" alt="">
                <input type="password" placeholder="请再次确认您的支付密码" id="paymentCode" name="paymentCode" onkeypress="keypressHandler(event)" MaxLength="6" />
            </li>
        </ul>
        <div>
            <button class="btn-block" id="ok">确认</button>
        </div>
    </div>
</body>

<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<#include "../agreement/mune_btn.ftl">
<script>
    function keypressHandler(event){
        var code = event.charCode || event.keyCode;
        var value = String.fromCharCode(code);
        var re =/[0-9]/;
        if(!re.test(value)){
            event.preventDefault();
        }
    };
    $(document).ready(function() {
        $("#ok").click(function () {
            var newpwd = $.trim($('#newpwd').val());
            var paymentCode = $.trim($('#paymentCode').val());
            //是否输入的是阿拉伯数字
            var reg = new RegExp("^\\d{6}$");
            if (newpwd.length != 6 || !reg.test(newpwd)) {
                layer.msg("支付只能为6位数字");
                return;
            }
            if (paymentCode.length != 6 || !reg.test(paymentCode)) {
                layer.msg("支付密码长度只能为6位数字");
                return;
            }

            if (newpwd != paymentCode) {
                layer.msg("输入的两个密码不一致");
                return;
            }
            var index = layer.load();
            api.tools.ajax({
                url: "${basePath!}/user/userInfo/updatePaymentCode",
                data: {
                    paymentCode: $("#paymentCode").val()
                }
            }, function (d) {
                layer.close(index);
                if (d.code == 0) {
                    layer.msg(d.msg);
                    window.location.href = "${basePath!}/consumer/center/jumpMine";
                } else {
                    layer.msg(d.msg);
                }
            });
        });
    });
</script>
</html>