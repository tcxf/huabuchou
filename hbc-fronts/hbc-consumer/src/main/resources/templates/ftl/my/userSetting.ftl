<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/new_index.css" />
    <title>设置</title>
    <style>
        body{
            background: #fff;
        }
        .linkman .col-xs-11{
            padding: 0;
        }
        .linkman .col-xs-1{
            padding-right: 0;
            text-align: right;
        }
        .linkman .col-xs-1>img{
            width: 8px;
            height: 10px;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            position: fixed;
            bottom: 60px;
            cursor: pointer;
        }
    </style>
</head>

    <body>
        <div class="linkman">
            <ul>
                <li class="redirect" id="redirect">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-11">
                                &nbsp&nbsp修改支付密码
                            </div>
                            <div class="col-xs-1">
                                <img src="${basePath!}/img/imgs/icon_enter.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row" id="redirect1">
                            <div class="col-xs-11">
                                花不愁平台支付协议
                            </div>
                            <div class="col-xs-1">
                                <img src="${basePath!}/img/img/icon_enter.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <button class="btn-block" id="logout">退出登录</button>


</body>

    <script type="text/javascript" src="${basePath!}/js/jquery-1.11.3.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script>
        $(document).ready(function() {
            $("#logout").on("touchend",function() {
                api.tools.ajax({
                    url: "${basePath!}/user/userInfo/logout"
                }, function (d) {
                    if (d.code == 0) {
                        layer.msg(d.msg);
                        window.location.href = "${basePath!}/cuser/gologin";
                    } else {
                        layer.msg(d.msg);
                        $this.removeClass("disabled");
                    }
                });
            });

            $("#redirect").click(function () {
                window.location = '${basePath!}/user/userInfo/goUpdatePaymentCode'
            });
            $("#redirect1").click(function () {
                window.location = '${basePath!}/consumer/center/jumpSxHt'
            });
        });
    </script>

</html>