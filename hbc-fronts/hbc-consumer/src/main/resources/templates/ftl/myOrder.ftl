<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="${basePath!}/css/footer.css"/>
		
		<title>我的订单</title>
	    <style>
			body{
				background: #fff;
			}
            .m_content{
                margin-bottom: 110px;
            }
            #all{
                margin-bottom:10px
            }
            #all li{
                width:94%;
                list-style:none;
                padding:5px;
                background:#fff;
                margin-top:5px;
                margin-left:3%;
                border-radius:5px;
                box-shadow: 0 1px 10px #dadada;

            }
            #all li .col-xs-3{
                  padding:0
              }
            #all li .col-xs-6 p{
                font-weight:600;
                color: #f49110;
            }
            #all li .col-xs-3:last-child{
                text-align:right;
                margin-top:47px;

            }
			.jiazai{
                margin-top: 10px;
                width: 100%;
                text-align: center;
                margin-bottom: 60px;
			}
			#dianji{
				text-decoration: none;
			}
            .c_all .order_list:last-child{
                margin-bottom: 60px;
            }
		</style>
	</head>

	<body>
		<div class="m_content tab-content " style="margin-top: 2px">
		    <div class="c_all tab-pane active" id="all">
		        
		    </div>
          <div class="jiazai" id="div">
              <a href="javascript:void(0)" style="color: #999"  id="dianji">点击查看更多</a>
		  </div>
		</div>
        <div class="footer" id="footer">
            <ul>
                <li>
                    <a href="${basePath!}/user/index/Goindex">
                        <div class="toOne"></div>
                        <p>首页</p>
                    </a>
                </li>
                <li>
                    <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                        <div class="toTow"></div>
                        <p>附近商家</p>
                    </a>
                </li>
                <li>
                    <a href="${basePath!}/consumer/bill/jumpMyCredit">
                        <div class="toThree"></div>
                        <p>授信</p>
                    </a>
                </li>
                <li class="active">
                    <a href="${basePath!}/user/order/order" class ="redirect">
                        <div class="toFou"></div>
                        <p>订单</p>
                    </a>
                </li>
                <li>
                    <a href="${basePath!}/consumer/center/jumpMine">
                        <div class="toFive"></div>
                        <p>我的</p>
                    </a>
                </li>
            </ul>
        </div>
	</body>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script>
		$(document).ready(function() {
			var page = 1;
			var rows = 100;
			var ele = "all";
			// $(".tab-ul").find("li").click(function(){
			// 	ele = $(this).find("a").attr("href").replace("#","");
			// 	status = $(this).attr("data");
			// 	 page = 1;
			// 	 rows = 100;
			// 	 loadEnd = false;
			// 	 $(".c_all").hide();
			// 	 $("#"+ele).show();
			// 	loadOrderList();
			// });
		 	<#--<c:if test="${mystatus != null}">-->
			<#--$(".tab-ul").find("li").each(function(i,e){-->
				<#--var s = $(this).attr("data");-->
				<#--if(s == ${mystatus}){-->
				<#--ele = $(this).find("a").attr("href").replace("#","");-->
				 <#--page = 1;-->
				 <#--rows = 100; -->
				 <#--loadEnd = false;-->
				 <#--$(".c_all").hide();-->
				 <#--$("#"+ele).show();-->
				<#--loadOrderList(status);-->
				<#--}-->
			<#--});-->
			<#--</c:if>-->

            $("#dianji").click(function(){
                page = page+1;
                loadOrderList();
            });
			loadOrderList();
			
			function loadOrderList(){
				// console.log(page);
				// if(page == 1){
				// 	$('#'+ele).empty();
				// }
				api.tools.ajax({
					url:'${basePath!}/user/order/findByUid',
					data:{
						page:page,
						type:1
					}
				},function(d){
				    var d = d.data;
				    if( d.records.length <10){
                        $("#div").attr("style","display:none;");//隐藏div
					}
					if(d!= null && d.records != null && d.records.length > 0){
						// if(d.data.result.length == 0){
						// 	loadEnd = true;
						// 	return;
						// }
						for(var i = 0 ; i < d.records.length ; i++){
							var data = d.records[i];
							 data.path = '${basePath!}';
							var html = template.load({
								host : '${basePath!}/',
								name : 'order_list',
								data : data
							});
							$html = $(html);
							
							$html.attr("data",data.id);
							//$html.attr("data1",data.consumeType);
							$html.click(function(){
								//if($(this).attr('data1')==2){
									//window.location = '${basePath!}/rummery/orderDetail.htm?id='+$(this).attr('data');
								//}else if($(this).attr('data1') ==1){
									window.location = '${basePath!}/user/order/findById?id='+$(this).attr('data');
								//}
							});
							$('#'+ele).append($html);
						}
					}else{
						if(page == 1){
							$('#'+ele).append('<dl>'+
												    '<dt>'+
												    '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">'+
												        '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无订单</div>'+
												    '</div>'+
												    '</dt>'+
												'</dl>');
						}
					}
					loading = false;
				});
			}


			var loadEnd = false;
			var loading = false;
			api.tools.scroll(function(option){
				if(option.top){
					if(option.top >= (option.dheight - option.wheight - 50)){
						if(loadEnd || loading) return;
						page = page + 1;
						loading = true;
						loadOrderList();
					}
				}
			});
			
		});
	</script>

</html>
