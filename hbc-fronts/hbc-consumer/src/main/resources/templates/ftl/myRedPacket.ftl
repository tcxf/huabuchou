
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />


    <title>我的优惠券</title>
    <style>
        body{
            background: #fff;
        }
        *{
            padding: 0;
            margin: 0;
        }
        .coupont_list{
            width: 100%;
            height: auto;
        }
        .coupont_list>ul{
            padding-left: 0;
        }
        .coupont_list li{
            height: 120px;
            list-style:none
        }
        .coupont_list .one_coupont{
            height: 120px;
            position: relative;
        }
        .coupont_list .one_coupont>img{
            width:94%;
            height: 120px;
            box-shadow: 0 1px 2px #fafafa;
            margin-left:3%

        }
        .coupont_content{
            width: 100%;
            position: absolute;
            top:20px;
        }
        .coupont_content .col-xs-3{
            padding: 0 5px 0 10px;
            text-align: center;
        }
        .coupont_content .col-xs-3>p{
            margin-bottom: 0;
        }
        .coupont_content .col-xs-3>h6{
            margin-top: 0;
        }
        .coupont_content .col-xs-6{
            padding: 0;

        }
        .coupont_content :last-child.col-xs-3{
            margin-top: 10px;
            text-align:left;
            padding-left:0
        }
        .coupont_content :last-child.col-xs-3>a{
            padding: 3px 8px;
            border:1px solid #f49110;
            border-radius: 30px;
            color:#f49110;
            font-size: 0.8rem;
        }
        .coupont_content :last-child.row{
            margin-top: 22px;
            padding-left: 20px;
            color:#999
        }
        .coupont_content .col-xs-12{
            font-size:0.8rem
        }
    </style>
</head>

<body>
<div class="coupont_list">
    <ul id="redList">

    </ul>
</div>

</body>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {
        api.tools.ajax({
            url:'${basePath!}/user/Ucoupon/couponList'
        },function(d){
            var d = d.data;
            if( d.length>0){
                for(var i = 0 ; i < d.length ; i++){
                    var data = d[i];
                    data.path = '${basePath!}';
                    if(data.mrStatus==3){
                        //优惠券已失效
                        data.p='<p style="font-size: 1.4rem;color:#999">';
                        data.h6='<h6 style="color:#999">';
                        data.img=' <img src="${basePath!}/img/imgs/post_no.png" style="width: 60px;height: 60px;margin-top: -15px" alt=""/>';
					}else{
                        //判断优惠券是否过期
                        if(data.couponType==1){
							//判断优惠券是否使用
                            if(data.isUse == 0){
                                //可使用
                                data.p='<p style="font-size: 1.4rem;color:#f49110">';
                                data.h6='<h6 style="color:#f49110">';
                                data.img='<a href="#" role="button">可使用</a>';
                            }else {
                                //以使用
                                data.p='<p style="font-size: 1.4rem;color:#999">';
                                data.h6='<h6 style="color:#999">';
                                data.img='<img  src="${basePath!}/img/imgs/ico_yishiyong.png"  style="width: 60px;height: 60px;margin-top: -15px" alt="" />';
                            }
                        }else{
                            //已过期
                            data.p='<p style="font-size: 1.4rem;color:#999">';
                            data.h6='<h6 style="color:#999">';
                            data.img=' <img src="${basePath!}/img/imgs/post_no.png" style="width: 60px;height: 60px;margin-top: -15px" alt=""/>';
                        }
					}

                    var html = template.load({
                        host : '${basePath!}/',
                        name : "myCoupon",
                        data : data
                    });
                    $html = $(html);
                    $html.find(".one_coupont").attr('data',data.miId);
                    $html.find(".one_coupont").click(function(){
                        window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');
                    });
                    $('#redList').append($html);
                }

            }else{
                $('#redList').html('<div style="margin-top: 0px;width:100%;text-align:center;margin:5px auto;background:#fff;padding:10px;">暂无优惠券</div>');
            }
        });
    });
</script>

</html>