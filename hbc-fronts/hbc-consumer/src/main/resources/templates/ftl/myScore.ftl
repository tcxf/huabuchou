<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
		<link rel="stylesheet" href="${basePath!}/css/y.css"/>
		<link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
		<title>我的积分</title>
	    <style>
			body, html {
				background: #eee;
			}
			.pp-top{
				background-size: 100%;
				background: linear-gradient(#ffbc14, #ff9300);
				height: 180px;
				margin: 0 ;
			}
			#yuan div{
				font-size: 22px;
				color: #ffffff;
			}
			#yuan>img{
				width: 145px;
				height: 145px;
				margin-top: 6%;
			}
			.active>a{
				color: #f49110 !important;
			}
			.m_tab .active {
				border-bottom: 2px solid #f49110;
			}
			.m_tab a{
				color: #333333;
			}
	    </style>
	</head>

	<body>
		<div  class="pp-top" align="center">
           <div id="yuan">
              <img src="${basePath!}/img/img/iocn_yuan.png" alt=""/>
              <p style="margin-top: -107px;color: #ffffff;font-size: 0.8em" >积分数</p>
              <div style="margin-top: 6px" id="score"> </div>
           </div>
       </div>
      <div class="c-content" style="margin-top: 5px;">
          <div class="m_tab">
             <ul class="webkit-box direction">
                <li class="active wkt-flex" data="1">积分收入</li>
                <li class="wkt-flex" data="0">积分支出</li>
            </ul>
         </div>
     </div>
     <div class="m_content tab-content " style="margin-top: 5px">
         <div class="c_thing tab-pane active" id="all">
             <ul id="score-list">
                
          	</ul>
       	</div>
	</div>
	</body>
	<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
	<script>
		$(document).ready(function() {
			var direction = 1;
			$(".direction").find('li').click(function(){
				$(".direction").find('li').removeClass('active');
				direction = $(this).attr('data');
				page = 1;
				loadScoreList();
				$(this).addClass('active');
			});
			var page = 1;
			var rows = 10;
			loadScoreList();
			function loadScoreList(){
				if(page == 1){
					$('#score-list').empty();
				}
				api.tools.ajax({
					url:'${basePath!}/consumer/center/findScoreInfo',
					data:{
                        limit:rows,
						page:page,
						direction:direction
					}
				},function(d){
                    var score=d.data.score;
					$("#score").html(score);
					if(d.data!= null && d.data.pageInfo.records != null && d.data.pageInfo.records.length > 0){
						for(var i = 0 ; i < d.data.pageInfo.records.length ; i++){
							var data = d.data.pageInfo.records[i];
							var html = template.load({
								host : '${basePath!}/',
								name : 'score_list',
								data : data
							});
							$html = $(html);
							if(data.direction == 0){//支出
								$html.find('.direction').prepend('-');
								$html.find('.direction').css('color','green');
							}else{//收入
								$html.find('.direction').prepend('+');
							}
							$('#score-list').append($html);
						}
					}else{
						if(page == 1){
							$('#score-list').append('<li><div style="padding:10px;width:100%;text-align:center;">暂无数据</div></li>');
						}
					}
                    if(d.data.pageInfo.records.length == 0){
                        loadEnd = true;
                        return;
                    }
					loading = false;
				});
			}
			

			var loadEnd = false;
			var loading = false;
			api.tools.scroll(function(option){
				if(option.top){
					if(option.top >= (option.dheight - option.wheight - 50)){
						if(loadEnd || loading) return;
						page = page + 1;
						loading = true;
						loadScoreList();
					}
				}
			});
		});
	</script>

</html>