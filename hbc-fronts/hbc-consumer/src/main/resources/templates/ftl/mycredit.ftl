<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/Letters.css">
    <link rel="stylesheet" href="${basePath!}/css/footer.css"/>
    <title>授信</title>
</head>
<body>
<div class="p-top">
    <img src="${basePath!}/img/ico_bgImg.png" alt="">
    <div id="yuan" align="center">
        <div id="container">
            <div class="p-img">
                <img src="${basePath!}/img/bg_erduqiu3.png" alt=""/>
            </div>
            <div id="wave-one"></div>
            <div id="wave-two"></div>
            <div id="wave-three"></div>
        </div>
        <#if authStatus == '1'>
            <div class="p-p" <#--onclick="goMycredit()"--> id="mycredit">
                <p >可用额度(元)</p>
                <h5 id="balance"></h5>
            </div>
        </#if>
        <#if authStatus == '2'>
            <div class="p-p"">
                <p >可用额度已冻结</p>
            </div>
        </#if>
        <#if userAttribute == '1' && authStatus == '1'>
            <div class="t_e">
                    <a href="${basePath!}/user/empo/goEmpoPlus">授信提额</a>
            </div>
        </#if>
        <#if userAttribute == '2' && authStatus == '1'>
            <div class="t_e1">
                <ul>
                    <li>
                        <a href="${basePath!}/user/empo/goEmpoPlus">授信提额</a>
                    </li>
                    <#if authExamine ?? && (authExamine == '1' || authExamine == '0' || authExamine == '2')>
                        <li>
                            <a href="#" <#--onclick="altmsg()"--> id="commercial">商业授信</a>
                        </li>
                        <#else>
                            <li>
                                <a href="${basePath!}/consumer/bill/jumpBusinessCreditView">商业授信</a>
                            </li>
                    </#if>
                </ul>
            </div>
        </#if>
    </div>
</div>
<div class="menu_bill">
    <ul>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMineWaitPay">
                <h4 id="totalMoney"></h4>
                <p>待还账单</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpUnOutBill">
                <h4 id="notbillMoney"></h4>
                <p>未出账单</p>
            </a>
        </li>
    </ul>
</div>
<div class="menu_bill_list">
    <ul>
        <li>
            <a href="${basePath!}/consumer/bill/jumpHistoryYears">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/ic_lishi.png" alt="">
                        </div>
                        <div class="col-xs-9">
                            历史账单
                        </div>
                        <div class="col-xs-2">
                            <img src="${basePath!}/img/right_enter.png" alt="">
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpPlanYears">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-1">
                            <img src="${basePath!}/img/ico_fenqi.png" alt="">
                        </div>
                        <div class="col-xs-9">
                            分期账单
                        </div>
                        <div class="col-xs-2">
                            <img src="${basePath!}/img/right_enter.png" alt="">
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>
<div class="footer" id="footer">
    <ul>
        <li>
            <a href="${basePath!}/user/index/Goindex">
                <div class="toOne"></div>
                <p>首页</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                <div class="toTow"></div>
                <p>附近商家</p>
            </a>
        </li>
        <li class="active">
            <a href="${basePath!}/consumer/bill/jumpMyCredit">
                <div class="toThree"></div>
                <p>花不愁</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/order/order" class ="redirect">
                <div class="toFou"></div>
                <p>订单</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMine">
                <div class="toFive"></div>
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>

<script>
    var waveOneinc = 0;
    var waveTwoinc = 15;
    var waveThreeinc = 25;
    setInterval(function(){
    $('#wave-one').css('transform', 'rotate('+waveOneinc+'deg)');
    waveOneinc+=1.5;
    waveTwoinc+=1.15;
    waveThreeinc+=1.25;
    $('#wave-two').css('transform', 'rotate('+waveTwoinc+'deg)');
    $('#wave-three').css('transform', 'rotate(-'+waveThreeinc+'deg)');
    }, 20);

    $(document).ready(function() {
        api.tools.ajax({
            url:"${basePath!}/consumer/bill/findMyCreditInfo",
            data:{
            }
        },function(d){
            $("#balance").html(d.data.balance);//总额度
            $("#totalMoney").html(d.data.totalMoneyMap.totalMoney);//待还金额
            $("#notbillMoney").html(d.data.totalMoney);//未出账单
        });
    });


    $("#mycredit").click(function(){
        window.location.href = "${basePath!}/consumer/bill/jumpCreditView";
    });

    <#--function goMycredit(){-->
        <#--window.location.href = "${basePath!}/consumer/bill/jumpCreditView";-->
    <#--}-->

    $("#commercial").click(function(){
        layer.msg("商户正在审核中！");
    });
    // function altmsg() {
    //     layer.msg("商户正在审核中！");
    // }


</script>
</body>
</html>