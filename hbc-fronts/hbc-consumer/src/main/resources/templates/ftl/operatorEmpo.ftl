<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
    <link rel="stylesheet" href="${basePath!}/css/loading.css">
    <title>运营商授权</title>
    <style>
        body{
            background: #fff;
        }
        .top_head{
            width: 100%;
            height: 40px;
            background: #ff7a00;
            text-align: center;
            line-height: 40px;
            color: #fff;

        }
        .content_list{
            margin: 30px 10px 0 10px;
        }
        .content_list>ul{
            padding-left: 0;
        }
        .content_list li{
            list-style: none;
        }
        .content_list .row{
            margin: 10px 0 0 0;
            border-bottom: 1px solid #ededed;
            padding-bottom: 10px;
        }
        .content_list .col-xs-3{
            padding: 0;

        }
        .content_list .col-xs-8{
            padding-left: 0;
        }
        .content_list .col-xs-8>input{
            border: none;
            outline:medium;
        }
        .content_list .col-xs-6{
            padding-left: 0;
            border-right: 1px solid #ededed;
        }
        .content_list .col-xs-6>input{
            border: none;
            outline:medium;
            width: 100%;
        }
        .content_list :last-child .col-xs-3{
            padding-left: 5px;
        }
        .content_list .col-xs-3>a{
            font-size: 0.8rem;
            text-decoration: none;
            color: #ff7a00;
        }
        .content_list .row .yzn_a img{
            width: 60px;
            height: 20px;
        }
        .yzn_a{
            width: 70px;
            height: 24px;
            background: #999;
            border: 1px solid #ededed;
            text-align: center;
            letter-spacing: 4px;
        }
        .yzn_a>I{
            font-size: 16px;
        }
        #yzm_img{
            display: none;
        }
        #dx_img{
            display: none;
        }
        .gouxuan{
            margin-top: 10px;
            width: 100%;
            padding: 10px;
            font-size: 1.2rem;
        }
        .gouxuan .col-xs-8 span{
            color: #ff7a00;
        }
        .gouxuan .col-xs-4{
            padding: 0;
        }
        .gouxuan .col-xs-4>a{
            color: #999;
            font-weight: 600;
            text-decoration: none;
        }
        input[type=radio],input[type=checkbox]  {
            display: inline-block;
            vertical-align: middle;
            width: 18px;
            height: 18px;
            margin-left: 5px;
            margin-top: -2px;
            -webkit-appearance: none;
            background-color: transparent;
            border: 0;
            outline: 0 !important;
            line-height: 20px;
            color: #d8d8d8;
        }
        input[type=checkbox]:after  {
            content: "";
            display:block;
            width: 18px;
            height: 18px;
            text-align: center;
            line-height: 12px;
            font-size: 14px;
            color: #fff;
            border: 2px solid #ddd;
            background-color: #fff;
            box-sizing:border-box;
        }
        input[type=checkbox]:checked:after  {
            border: 4px solid #ddd;
            background-color: #ff7a00;
        }

        input[type=checkbox]:checked:after  {
            content: "√";
            border-color: #ff7a00;
            background-color: #ff7a00;

        }
        .a-btn{
            width: 90%;
            height: 40px;
            background:#ff7a00;
            line-height: 40px;
            text-align: center;
            margin:5px auto;
            border-radius: 5px;
        }
        .a-btn>a{
            color: #fff;
            text-decoration: none;
        }
        .a-text{
            margin: 20px;
            border-top:1px solid #ededed;
            padding-top: 20px;
            color: #999;
        }
        .modal{
            background: rgba(0,0,0,0.6);


        }
        .modal .content{
            width: 90%;
            height: auto;
            margin: 45% auto;
            background:rgba(255,255,255,0.8);
            border-radius: 5px;
        }
        .header{
            text-align: right;
            padding: 5px
        }
        #closed{
            width: 20px;
        }
        .media-body{
            padding: 20px;
        }
        .media-body span{
            color: #ff7a00;
        }
    </style>
</head>
<body>
    <div class="top_head">
        运营商授权
    </div>
    <div class="content_list">
        <ul>
            <li>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-3">
                            手机号码:
                        </div>
                        <div class="col-xs-8">
                            <input type="text" placeholder="13988888888" id="mobile" disabled="disabled" value="${mobile!}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            服务密码:
                        </div>
                        <div class="col-xs-8">
                            <input type="text" placeholder="请输入服务密码" id="serviceCode"/>
                        </div>
                    </div>
                    <div class="row"  id="dx_img">
                        <div class="col-xs-3">
                            验证码:
                        </div>
                        <div class="col-xs-6">
                            <input type="text" placeholder="请输入短信验证码" id="randomPassword"/>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" id="getYzm">获取验证码</a>
                        </div>
                    </div>
                    <div class="row"  id="yzm_img">
                        <div class="col-xs-3">
                            图片验证码:
                        </div>
                        <div class="col-xs-6">
                            <input type="hidden" id="totalScore" name="totalScore" value="${totalScore!}" />
                            <input type="text" placeholder="请输入图片验证码" id="code"/>
                        </div>
                        <div class="col-xs-3">
                            <input type="hidden" value="${yzm}" id="yzm" />
                            <img src="data:image/gif;base64,${icon!}">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="gouxuan">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8">
                    <input type="checkbox" checked>
                    同意 <span>账户数据收集协议</span>
                </div>
                <div class="col-xs-4">
                    <a href="#" id="forget_password">忘记服务密码?</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal">
        <div class="modal-dialog" id="modal-dialog">
            <div class="content">
                <div class="header">
                    <img src="${basePath!}/img/imgs/icon_delete.png" id="closed" alt="">
                </div>
                <div class="media-body">
                    <p>
                        <b>移动</b>：发送“ <span>CZMM#入网证件号#新密码#新密码</span>”新密码为6位数字，到10086，请确认两次输入密码一致 。  
                    </p>
                    <p>
                        <b>联通</b>：发送“ <span>MMCZ#新密码#证件号后6位</span>”新密码为6为数字，到10010，请确认两次输入密码一致 。  
                    </p>
                    <p>
                        <b>电信</b>：用本机拨打 <span>10001按1-3-1键</span>获取服务密码。
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="a-btn">
        <a href="#" id="tj">授权认证</a>
    </div>
    <div class="a-text">
        <p>温馨提示</p>
        <h6>1.请授权本人实名认证手机号码</h6>
        <h6>2.登录成功之后会收到运营商短信,无需理会</h6>
    </div>

    <script src="${basePath!}/js/jquery-1.11.3.js" type="text/javascript"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script src="${basePath!}/js/html5shiv.min.js"></script>
    <script src="${basePath!}/js/respond.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>

    <script>
        $(document).ready(function(){
            var yzm = $("#yzm").val();
            if (yzm.length == 0){
                $("#dx_img").css("display","block");
            } else{
                $("#yzm_img").css("display","block");
            }
            $("#getYzm").click(function(){
                var mobile = $("#mobile").val();
                api.tools.ajax({
                    url:"${basePath!}/user/empo/sendVerificationCode",
                    data:{
                        mobile:mobile
                    },
                },function(d){
                    api.tools.toast(d.msg);
                });
            });
            $("#tj").click(function(){
                var mobile = $("#mobile").val();
                var serviceCode = $("#serviceCode").val();
                var randomPassword = $("#randomPassword").val();
                var code = $("#code").val();
                var totalScore = $("#totalScore").val();
                api.tools.ajax({
                    url:"${basePath!}/user/empo/threeMethod",
                    data:{
                        mobile:mobile,
                        serviceCode:serviceCode,
                        randomPassword:randomPassword,
                        code:code,
                        totalScore:totalScore
                    }
                },function(d){
                    api.tools.toast(d.msg);
                    if (d.code == 0)
                    {
                        window.location.href = "${basePath!}/user/index/goIndex";
                    }else {
                        api.tools.toast(d.msg);
                    }
                });
            });
       });
        $("#forget_password").click(function () {
            document.getElementById("mymodal").style.display="block";
        });
        $("#closed").click(function () {
            document.getElementById("mymodal").style.display="none";
        });
    </script>
</body>
</html>