<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>二维码收款</title>
    <style>
        .a_top{
            width: 100%;
            height: 160px;
            background: url(${basePath!}/img/merchant_my_background.png)no-repeat;
            background-size: cover;
            text-align: center;
            padding-top: 30px;
        }
        .a_top>img{
            width: 70px;
        }
        .a_top p{
            color:#fff;
            margin-top: 10px;
            letter-spacing: 1px;
        }
        .erweima_content{
            width: 94%;
            height: auto;
            background: #fff;
            box-shadow: 2px 2px #ededed;
            margin-left: 3%;
            border-radius: 10px;
            position: absolute;
            top:110px
        }
        .b_top{
            width: 100%;
            height: 60px;
            background: #f5f5f5;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            padding: 20px 10px;

        }
        .b_top>img{
            width: 20px;
        }
        .b_top span{
            color:#f49110
        }
        .content_img{
            width: 100%;
            text-align: center;
            padding: 30px 0 50px;
        }
        .>img{
            width: 70%;
        }
    </style>
</head>
<body>
<div class="a_top">
    <img src="${basePath!}/img/logo_img.png" alt="">
        <p>随时随地,想花就花!</p>
</div>
<div class="erweima_content">
    <div class="b_top">
        <img src="${basePath!}/img/shoukuan_img.png" alt="">
        <span>二维码收款(${mName!})</span>
    </div>
    <div class="content_img" id="erweima" >

    </div>
</div>
</body>
<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/jquery.qrcode.js"></script>
<script src="${basePath!}/js/qrcode.js"></script>
<#include "../agreement/mune_btn.ftl">
<script type="text/javascript">
    //通过画布动态将数据信息以二维码图片显示
    $(document).ready(function() {
        var qrCode="${gotoPayUrl}";
        $('#erweima').qrcode({width:183,height:182,correctLevel:0,text:qrCode});
    });
</script>
</html>