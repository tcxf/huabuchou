<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>登录注册引导页</title>
    <style>
        body{
            background: url(${basePath!}/img/denglu_loding.png)no-repeat;
            background-size: cover;
        }
        .go_index{
            padding:20px;
            text-align: right;
        }
        .go_index>a{
            text-decoration: none;
            color: #f49110;
        }
        .title_text{
            text-align: center;
            margin-top: 90%;
        }
        .title_text>h3{
            color: #f49110;
        }
        .title_text p{
            margin-bottom: 0;
            color: #666;
        }
        .a-text{
            margin-top: 40px;
            width: 100%;
            padding: 30px;
        }
        .a-text>ul{
            padding-left: 0;
        }
        .a-text li{
            list-style: none;
            float: left;
            text-align: center;
            width: 50%;

        }
        .a-text .a-login a{
            padding: 10px 40px;
            background: #f49110;
            border-radius: 50px;
            color: #fff;
            text-decoration: none;
            box-shadow: 1px 3px 10px #f49110;
        }
        .a-text .a-register a{
            padding: 10px 40px;
            border:1px solid #f49110;
            border-radius: 50px;
            color: #f49110;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="go_index">
    <a href="${basePath!}/user/index/Goindex">进入首页</a>
</div>
<div class="title_text">
    <h3>加入会员 畅想优惠折扣</h3>
    <p>授信消费服务，最长30天免息</p>
    <p>吃喝玩乐优惠不听，最高劲享4折</p>
</div>
<div class="a-text">
    <ul>
        <li class="a-login">
            <a href="${basePath!}/cuser/gologin?openId=${openId!}">登录</a>
        </li>
        <li class="a-register">
            <a href="${basePath!}/register/goRegister?type=1&openId=${openId!}">注册</a>
        </li>
    </ul>
</div>
</body>
</html>