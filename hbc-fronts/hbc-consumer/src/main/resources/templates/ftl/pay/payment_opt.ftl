<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/zhifu2.css" />
    <link rel="stylesheet" href="${basePath!}/css/possword.css" />
    <title>扫码支付</title>
</head>
<body>
<div class="top_one">
    <ul>
        <li>
            <b>付款</b>
        </li>
        <li>
            <span>支付金额:</span>
            <input type="number" id="input_text"  placeholder="请输入支付金额">
        </li>
    </ul>
</div>
<div class="top_one">
    <div class="container-fluid">
        <div class="row" onclick="selectRedPacketByUserId()">
            <div class="col-xs-6">
                <b>优惠券:</b>
                <input id="mredmarketId" type="hidden">
                <input id="mredmarketmoney" type="hidden">
            </div>
            <div class="col-xs-6">
                <div>选择优惠券</div>
            </div>
        </div>
    </div>
</div>
<div class="top_one">
    <ul>
        <li>
            <b>请选择支付方式</b>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/images/icon_shouxinzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>授信支付 <span>(可用余额：0.00)</span></p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="shouxin" name="payType" value="1" checked>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/images/ico_qianbaozhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>钱包余额 <span>(可用余额：0.00)</span></p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="qianbao" name="payType" value="2">
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/images/ico_weinzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>微信支付</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="weixin" name="payType" value="3">
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/images/ico_yinlianzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>网银快捷</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="wangyin" name="payType" value="4">
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="my_card" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                我的银行卡
            </div>
            <input id="agreeid" type="hidden">

            <div class="col-xs-8 card_name">
                选择银行卡
            </div>
        </div>
    </div>
</div>


<div class="b_btn">
    <a href="javascript:void(0);" class="ljzf_but all_w">确认支付</a>
</div>

<script src="${basePath!}/js/jquery.js"></script>
<script>
    $(".ljzf_but").click(function () {
        window.location.href="${basePath!}/pay/goToPayBootpage";
    });

  function selectRedPacketByUserId(){
      window.location.href="${basePath!}/pay/goToPayBootpage";
  }
</script>
</body>
</html>