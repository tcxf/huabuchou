<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<script src="http://zeptojs.com/zepto.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=no" />
<title>手机端六位密码输入</title>
<style>
	body {
		background: #eeeeee;
	}

    .contaiter {
        display: flex;
        justify-content: center;
        margin-top: 10%;
    }
    .wrap{
        margin: 10px auto;
        padding-top: 43px;
    }
    .pwd-box{
        width: 240px;
        height: 50px;
        margin: 0 auto;
        position: relative;
    }
    .pwd-box .fake-box{
        width: 100%;
        height: 100%;
        border:1px solid #ff7a00;
        border-radius: 7px;
        -moz-border-radius: 7px;
        -webkit-border-radius: 7px;
        overflow: hidden;
        position: absolute;
        z-index: 0;
    }
    .pwd-box .pwd-input{
        width: 100%;
        height: 100%;
        position: absolute;
        top:0;
        left: 0;
        z-index: 1;
        filter:alpha(opacity=0);
        -moz-opacity:0;
        opacity:0; }
    .fake-box input{
        padding: 0;
        width: 16.6%;
        height: 100%;
        float:left;
        background: #ffffff;
        text-align: center;
        font-size: 20px;
        border: none;
        border-right:1px solid #ff7a00;
    }
    .pwd-box input:last-child{
        border: none;
    }
</style>
</head>
<body>
<div class="contaiter">
    <div class="webkit-box">
        <div class="wkt-flex" id="title">请输入支付密码</div>
    </div>
</div>
<div class="wrap">
    <div class="pwd-box">
        <input type="tel" maxlength="6" class="pwd-input" id="pwd-input">
        <div class="fake-box">
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
            <input type="password" readonly UNSELECTABLE='true' onfocus="this.blur()" />
        </div>
    </div>
</div>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>

	<script>
		var $input = $(".fake-box input");
		var newpwd = null;
		var renewpwd = null;
		var step = 1; //1-第一次输入密码 2-重复密码
		$("#pwd-input").on("input", function() {
			var pwd = $(this).val().trim();
			for (var i = 0, len = pwd.length; i < len; i++) {
				$input.eq("" + i + "").val(pwd[i]);
			}  
			$input.each(function() {
	            var index = $(this).index();
	            if (index >= len) {
	                $(this).val("");
	            }
	        });
			if (len == 6) {
				if(step == 1){
					newpwd  =  pwd;
					console.log(newpwd);
					$("#title").html("请再输入一次密码");
					//清空输入框
					$("#pwd-input").val("");
					 $input.val("");
					  step = 2;
				}else{
					renewpwd = pwd;
					console.log(renewpwd);
					setPwd();
				}
			}
			function  setPwd(){
				if(renewpwd != newpwd){
				    api.tools.toast('两次密码输入不一致，请重新输入');
					$("#pwd-input").val("");
					$input.val("");
					return;
				}
			api.tools.ajax({
					url:'${basePath!}/user/register/addTbUserAccount?type=${type!}',
					data : {
						newpwd : newpwd
					}
				}, function(d) {
					api.tools.toast(d.msg);
					$('.tj').removeClass('disabled');
					$('.tj').html('完成');
					if (d.code != -1) {
						setTimeout(function() {
							window.location.href = "${basePath!}/user/register/goFastEmpo";
						}, 1500);
					}
				});
			}
		});
	</script>
</body>
</html>


