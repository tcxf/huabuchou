<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
    <title>注册</title>
    <style>
        body{
            background:url(${basePath!}/img/imgs/new_bg_img.png)no-repeat;
            background-size: cover;
        }
        .list_content{
            margin-top: 30%;
            padding:0 60px;
        }
        .list_content li{
            text-align: center;
            margin-left:10px;
            margin-right:10px
        }
        .m_tab {
            line-height: 48px;
            height: 48px;
            background:transparent;

        }
        .container_gress{
            margin-top: 10px;
            color:#999;
            height: 20px;
            line-height: 20px;
        }
        .container_gress .col-xs-12{
            text-align: center;
        }
        .container_gress .col-xs-12>.col-xs-1{
            text-align: center;
            padding: 0;
            margin-top: -2px;
        }
        .container_gress .col-xs-12>.col-xs-11{
            text-align: left;
            padding: 0;
        }
        .container_gress .col-xs-12>.col-xs-11>a{
            text-decoration: none;
            font-size: 12px;
        }

        .active>a{
            color: #f49110 !important;

        }

        .m_tab .active {
            border-bottom: 2px solid #f49110;

        }
        .m_tab a{
            color: #999;
            font-weight: 600;

        }
        .login-model {
            width: 85%;
            padding: 20px 0px 20px 5px;
        }
        input{
            outline:medium;
        }
        .sms-btn{
            background: transparent;
            border: 1px solid #f49110;
            outline: none;
            border-radius: 30px;
            font-size: 0.8rem;
            color: #f49110!important;
            cursor: pointer
        }
        #btn{
            background: transparent;
            border: 1px solid #f49110;
            outline: none;
            border-radius: 30px;
            font-size: 0.8rem;
            color: #f49110!important;
            padding: 3px 5px;
        }
        #btn2{
            background: transparent;
            border: 1px solid #f49110;
            outline: none;
            border-radius: 30px;
            font-size: 0.8rem;
            color: #f49110!important;
            padding: 3px 5px;
        }
        .btn-block{
            width: 94%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            background: #f49110;
            color: #fff;
            border-radius: 30px;
            outline: none;
            border: none;
            text-align: center;
            cursor: pointer
        }
    </style>
</head>
<body>
<div class="list_content">
    <div class="m_tab">
        <ul class="webkit-box">
            <li class="active wkt-flex"><a href="#all" data-toggle="tab">会员</a></li>
            <li class="wkt-flex"><a href="#marchant" data-toggle="tab">商户</a></li>
        </ul>
    </div>
</div>
<div class="m_content tab-content " >
    <div class="c_thing tab-pane active" id="all">
        <div class="a-content login-model">
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;">
                    <img src="${basePath!}/img/imgs/yaoqingma_img.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel" name="invateCode" id="invateCode" class="form-control reg-input"  placeholder="邀请码,可选填" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin: 5px"></div>
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;">
                    <img src="${basePath!}/img/imgs/ico_zhanghao.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel"  name="mobile" id="mobile" class="form-control reg-input" placeholder="请输入您的手机号码" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin:  5px"></div>
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;margin-top: 0.3em;">
                    <img src="${basePath!}/img/imgs/messge_img.png" />
                </div>
                <div class="wkt-flex" style="padding-top: 5px">
                    <input type="tel" name="yCode" id="yCode" class="form-control reg-input" placeholder="输入验证码" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
                <div style="margin-top: 5%">
                    <input type="button" class="sms-btn hyzm-btn" id="btn" value="发送验证码" />
                </div>
            </div>
            <div class="gwl" style="margin:  5px"></div>
        <div>
          <button class="btn-block tj">下一步</button>
        </div>
            <div class="container_gress">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-1">
                                <input type="checkbox" id="agree"/>
                            </div>
                            <div class="col-xs-11">
                                同意<a href="#mymodalTow" data-toggle="modal" style="color: #008fff;">《服务协议》</a>
                                <a href="#mymodal" data-toggle="modal" style="color: #008fff;">《用户协议》</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </div>
    <div class="c_thing tab-pane" id="marchant">
        <div class="a-content login-model">
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;">
                    <img src="${basePath!}/img/imgs/yaoqingma_img.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel" name="invateCode2" id="invateCode2"  class="form-control reg-input" placeholder="邀请码,可选填" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin: 5px"></div>
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;">
                    <img src="${basePath!}/img/imgs/ico_zhanghao.png" />
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="loginName" name="loginName" class="form-control reg-input" placeholder="请输入您的手机号码" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>

            <div class="gwl" style="margin:  5px"></div>
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;margin-top: 0.3em;">
                    <img src="${basePath!}/img/imgs/messge_img.png" />
                </div>
                <div class="wkt-flex" style="padding-top: 5px">
                    <input type="tel" id="ycode2" name="ycode2" class="form-control reg-input" placeholder="输入验证码" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
                <div style="margin-top: 5%">
                    <input type="button" class="sms-btn hyzm-btn" id="btn2" value="获取验证码" />
                </div>
            </div>
            <div class="gwl" style="margin:  5px"></div>
            <div class="webkit-box ">
                <div style="line-height: 35px; width: 11%;">
                    <img src="${basePath!}/img/imgs/poss_word.png" />
                </div>
                <div class="wkt-flex">
                    <input type="password"  name="password" id="password" class="form-control reg-input" placeholder="请设置您的登录密码" style="width: 100%; height: 40px; padding-left: 10px; font-size: 0.8rem;">
                </div>
            </div>
            <div class="gwl" style="margin:  5px"></div>
            <div>
            <button class="btn-block tj2">下一步</button>
            </div>
            <div class="container_gress">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-1">
                                <input type="checkbox" id="agree1"/>
                            </div>
                            <div class="col-xs-11">
                                同意<a href="#mymodalThree" data-toggle="modal" style="color: #008fff;">《服务协议》</a>
                                <a href="#mymodalOne" data-toggle="modal" style="color: #008fff;">《商户协议》</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <#include "../agreement/user_register.ftl">
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodalOne" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <#include "../agreement/merchants_register.ftl">
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodalTow" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <#include "../agreement/serve.ftl">
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodalThree" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <#include "../agreement/serve.ftl">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="${basePath!}/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer/layer.js"></script>
<script src="${basePath!}/js/ajax.js"></script>




<script>

    var ewminvateCode = "${invateCode!}";
    var oname = "${oname!}";
    $("#use_name").html(oname);
    if(ewminvateCode==""||ewminvateCode==null){
        ewminvateCode = window.sessionStorage.getItem("invateCode");
    }
    $("#invateCode").val(ewminvateCode);
    $("#invateCode2").val(ewminvateCode);
    $(".tj").click(function(){
        register("1");
    });
    $(".tj2").click(function(){
        register("2");
    });

    $("#btn").click(function(){
        getyzmcode("btn");
    });
    $("#btn2").click(function(){
        getyzmcode("btn2");
    });

    function getyzmcode(btnId) {
        var i=60;
        var telmobile;
        if("btn"==btnId){
            var mobile = $("#mobile").val().trim();
            if(mobile==null||mobile==""){
                layer.msg("手机号码不能为空");
                return;
            }
            if(mobile.length!=11){
                layer.msg("请输入正确的手机号码");
                return;
            }
            telmobile = mobile;
        }
        if("btn2"==btnId){
            var loginName = $("#loginName").val().trim()
            if(loginName==null||loginName==""){
                layer.msg("手机号码不能为空");
                return;
            }
            if(loginName.length!=11){
                layer.msg("请输入正确的手机号码");
                return;
            }
            telmobile = loginName;
        }

        $("#"+btnId+"").attr("disabled", true);
        var url = "${basePath!}/register/getCode?mobile="+telmobile+"&type=YZM";
        var data={};
        startTime(btnId,i);
        ajax.loadService(url,data, function(data){
            layer.msg(data.msg);
        });
    }

    function startTime(btnId,i){
        $("#"+btnId+"").val("获取验证码("+i+"秒)");
        if(i > 0){
            i--;
            setTimeout(function(){
                startTime(btnId,i);
            },1000);
        }else{
            $("#"+btnId+"").val("获取验证码");
            $("#"+btnId+"").attr("disabled", false);

        }
    }

    function register(type,btnId){
        var data = {};
        if(type==1){
            var mobile = $("#mobile").val().trim();
            var ycode = $("#yCode").val().trim();
            if(mobile==null||mobile==""){
                layer.msg("手机号码不能为空");
                return;
            }
            if(mobile.length!=11){
                layer.msg("请输入正确的手机号码");
                return;
            }
            if(yCode==null||yCode==""){
                layer.msg("验证码不能为空");
                return;
            }
            if(ycode==null||ycode==""){
                layer.msg("验证码不能为空");
                return;
            }
            if(!$("#agree").is(':checked')) {
                layer.msg("请先同意《用户协议》");
                return;
            }
            data.mobile = mobile;
            data.invateCode = $("#invateCode").val().trim();
            data.openId = "${openId!}";
            data.ycode = ycode;
            data.type = type;
        }

        if(type==2){
            var loginName = $("#loginName").val().trim();
            var ycode = $("#ycode2").val().trim();
            var password = $("#password").val().trim();
            if(loginName==null||loginName==""){
                layer.msg("手机号码不能为空");
                return;
            }
            if(loginName.length!=11){
                layer.msg("请输入正确的手机号码");
                return;
            }
            if(ycode==null||ycode==""){
                layer.msg("验证码不能为空");
                return;
            }
            if(password==null||password==""){
                layer.msg("密码不能为空");
                return;
            }
            if(password.length < 6 || password.length > 16){
                layer.msg("密码必须是6-16个字符");
                return;
            }
            if(!$("#agree1").is(':checked')) {
                layer.msg("请先同意《用户协议》");
                return;
            }
            data.mobile = loginName;
            data.invateCode = $("#invateCode2").val().trim();
            data.openId = "${openId!}";
            data.ycode = ycode;
            data.password = password;
            data.type = type;
        }
        var url = "${basePath!}/register/register";
        ajax.loadService(url, data, function (data) {
            if (data.code == "0") {
                window.localStorage.removeItem("invateCode");
                window.location.href = "${basePath!}/register/paymentCode?type="+type+"";
            }
            if (data.code == "1") {
                layer.msg(data.msg);
            }
        });
    }
</script>
</body>
</html>