<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
		<link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
		<title>还款详情</title>
		<style>
			body,
			html {
				background: #efeff4;
			}
			.layui-m-layer{
				z-index: 0 !important;
			}
			h3{
				margin-top: 0px !important;
				margin-bottom: 0px !important;
				text-align: left !important;
			}
			.layui-m-layercont{
				padding-right: 60px !important;
			}
		</style>
	</head>

	<body>
		<#--<div class="c-content" style="padding:0px;">
			<ul class="sj-ul">
				<li>
					<div class="webkit-box">
						<div class="input-left-name">
							交易类型
						</div>
						<div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
							<span  style="color:#00CC00">还款</span>
						</div>
					</div>	
				</li>
			</ul>	
		</div>	-->
		<div class="c-content" style="padding:0px;">
			<ul class="sj-ul">

                <li>
                    <div class="webkit-box">
                        <div class="input-left-name">
                            支付单号
                        </div>
                        <div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
                            <span  style="" id="repaySn"></span>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="webkit-box">
                        <div class="input-left-name">
							还款时间
                        </div>
                        <div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
                            <span  style="" id="payDate"></span>
                        </div>
                    </div>
                </li>


                <li>
                    <div class="webkit-box">
                        <div class="input-left-name">
                            收款方
                        </div>
                        <div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
                            <span  style="" id="simpleName"></span>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="webkit-box">
                        <div class="input-left-name">
                            还款方式
                        </div>
                        <div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
                            <span  style="" id="paymentType"></span>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="webkit-box">
                        <div class="input-left-name">
                            还款金额
                        </div>
                        <div class="wkt-flex" style="text-align:right; line-height:30px; padding-right:5%">
                            <span  style="" id="amount"></span>
                        </div>
                    </div>
                </li>
			</ul>
		</div>
	</body>
    <script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
    <script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
    <script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
    <script type="text/javascript" src="${basePath!}/js/info.js"></script>
    <script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
    <script type="text/javascript" src="${basePath!}/template/template.js"></script>
    <script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
    <script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
    <#include "./agreement/mune_btn.ftl">
    <script>
        api.tools.ajax({
            url:'${basePath!}/consumer/center/findMyRepayDetail',
            data:{
                id:"${id!}"
            }
        },function(d){
            console.log(d);
            if(d.code == 1){
                api.tools.toast(d.msg);
            }else {
                $("#amount").html(d.data.tbRepayLog.totalMoney);
                $("#simpleName").html(d.data.fName);
                var payDate= d.data.tbRepayLog.payDate == null ? "无":d.data.tbRepayLog.payDate;
                $("#payDate").html(payDate);
                //var paymentTypeStr = (d.data.tbRepayLog.paymentType=="0"?"支付宝还款":(d.data.tbRepayLog.paymentType == "1"?"微信还款":"网银还款"));
                var paymentTypeStr = "网银还款";
                $("#paymentType").html(paymentTypeStr);
                var repaySn= d.data.tbRepayLog.repaySn == null ? "无":d.data.tbRepayLog.repaySn;
                $("#repaySn").html(repaySn);
            }
        });
	</script>

</html>