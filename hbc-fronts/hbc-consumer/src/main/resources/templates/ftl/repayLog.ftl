<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="${basePath!}/css/app.css" />
		<link rel="stylesheet" href="${basePath!}/css/y.css"/>
		<link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
		<title>还款纪录</title>
		<style>
			body,
			html {
				background: #eee;
			}
		</style>
	</head>

	<body>
		<div id="list"></div>
	</body>
	<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
	<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="${basePath!}/js/info.js"></script>
	<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="${basePath!}/template/template.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
	<#include "./agreement/mune_btn.ftl">
	<script>
		$(document).ready(function() {
			var page = 1;
			var rows = 10;
			loadList();
			function loadList(){
				if(page == 1){
					$('#list').empty();
				}
				api.tools.ajax({
					url:'${basePath!}/consumer/center/findMyPaybackPageInfo',
					data:{
                        limit:rows,
						page:page
					}
				},function(d){
					if(d.data!= null && d.data.records != null && d.data.records.length > 0){
						for(var i = 0 ; i < d.data.records.length ; i++){
							var data = d.data.records[i];
							data.paymentTypeStr = "网银还款";
                            data.typeStr =data.status=='1'?(data.payType=="1"?"账单还款":"正常还款"):"还款失败";
							var html = template.load({
								host : '${basePath!}/',
								name : 'repaylog',
								data : data
							});
							$html = $(html);
							$html.attr("data",data.id);
							$html.click(function(){
								window.location = '${basePath!}/consumer/center/jumpMyRepayDetail?id='+$(this).attr('data');
							});
							$('#list').append($html);
						}
					}else{
						if(page == 1){
							$('#list').append('<dl>'+
												    '<dt>'+
												    '<div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">'+
												        '<div class="input-left-name" style="font-weight:normal; width:100%;margin-top: 2%;text-align:center;">暂无还款纪录</div>'+
												    '</div>'+
												    '</dt>'+
												'</dl>');
						}
					}
                    if(d.data!= null && d.data.records.length == 0){
                        loadEnd = true;
                        return;
                    }
					loading = false;
				});
			}
			

			var loadEnd = false;
			var loading = false;
			api.tools.scroll(function(option){
				if(option.top){
					if(option.top >= (option.dheight - option.wheight - 50)){
						if(loadEnd || loading) return;
						page = page + 1;
						loading = true;
						loadList();
					}
				}
			});
		});
	</script>

</html>