<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/bills.css">
    <title>待还账单</title>
</head>
<body>
<div class="Bills">
    <p>待还款金额</p>
    <h2 id="totalMoney"></h2>
    <h6>最后还款日7月7日</h6>
</div>

<!--账单-->
<div class="bills_list" id="bills_list_tow">
    <ul id="bills_list">
        <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-8">
                        <p>消费分期(每月还款+服务费)</p>
                        <h6>2018-06-12</h6>
                    </div>
                    <div class="col-xs-4">
                        1000.00
                    </div>
                </div>
            </div>
        </li>-->

    </ul>
</div>

<!--交易记录表-->
<div class="bills_list" id="bills_list_one">
    <ul id="trade_list">
        <#--<li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-8">
                        <p>芙蓉华天酒店消费</p>
                        <h6>2018-06-12</h6>
                    </div>
                    <div class="col-xs-4">
                        1000.00
                    </div>
                </div>
            </div>
        </li>-->
    </ul>

    <div class="nub_text">
        合计比数: <span id="totalSize"></span>
    </div>
</div>


<div class="my_btn">
    <ul>
        <li class="repayment">
            <a href="#">立即还款</a>
        </li>
        <li class="by_stages">
            <a href="#">我要分期</a>
        </li>
    </ul>
</div>

<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script type="text/javascript" src="${basePath!}/template/template.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.picker.js"></script>
<script type="text/javascript" src="${basePath!}/js/picker/js/mui.poppicker.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>

    $(document).ready(function() {

        loadTradingDetail();
        //加载交易记录
        function loadTradingDetail(){
            api.tools.ajax({
                url:"${basePath!}/consumer/bill/findMineWaitPayInfo",
                /*data:{
                    page:page,
                    limit:rows
                }*/
            },function(d){
                console.log(d)
                $("#totalMoney").html(d.data.totalMoneyMap.totalMoney)    /*渲染待还金额*/
                $("#totalSize").html(d.data.totalSize)     /*渲染总数量*/

                /*加载账单列表 逾期或者是待还*/
                if(d.data.payList!=null && d.data.payList.length > 0 ) {
                    for (var i = 0; i < d.data.payList.length; i++) {
                        var obj = d.data.payList[i];
                        if (obj.waitpay == 0 && obj.splitType == 1) { /1:分期 *1:待还 0:未还*/
                            data = {
                                name: "消费分期(每月还款+服务费+违约金)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate
                            };
                        }else if(obj.waitpay == 1 && obj.splitType == 1){
                            data = {
                                name: "消费分期(每月还款+服务费)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate
                            };
                        }else if(obj.waitpay == 0 && obj.splitType == 0){
                            data = {
                                name: "逾期总计(本金+违约金)",
                                money: obj.totalMoney,
                                time: obj.repaymentDate
                            };
                        }
                        var html = template.load({
                            host: '${basePath!}/',
                            name: "bill_list",
                            data: data
                        });
                        $html = $(html);
                        $html.find(".container-fluid").attr("data1", d.data.payList[i].planId);
                        $html.find(".container-fluid").attr("data2", d.data.payList[i].waitpay);
                        $html.find(".container-fluid").attr("data3", d.data.payList[i].splitType);
                        $html.find(".container-fluid").click(function () {
                            /*跳转页面(逾期未还/待还)*/
                            window.location = "${basePath!}/consumer/bill/jumpPlanDetailInfo?planId=" + $(this).attr("data1") + "&waitpay=" + $(this).attr("data2")+ "&splitType=" + $(this).attr("data3");
                        });
                        $('#bills_list').append($html);
                    }
                }
                /*加载本月已出账单交易记录 或者逾期已出账单交易记录*/
                if(d.data.totalTradeList != null && d.data.totalTradeList.length>0){

                    for (var i = 0; i < d.data.totalTradeList.length; i++) {
                        var obj = d.data.totalTradeList[i];

                        data = {
                            name: "交易记录",
                            money: obj.actualAmount,
                            time: obj.tradingDate
                        };

                        var html = template.load({
                            host: '${basePath!}/',
                            name: "bill_trading_detail",
                            data: data
                        });
                        $html = $(html);
                        $html.find(".container-fluid").attr("data1", d.data.totalTradeList[i].id);
                        $html.find(".container-fluid").click(function () {
                            /*跳转页面(逾期未还/待还)*/
                            window.location = "${basePath!}/user/order/findById?id=" + $(this).attr("data1");
                        });
                        $('#trade_list').append($html);
                    }
                }

                /*控制还款按钮 以及分期按钮*/
                if(d.data.payButton){
                    $(".repayment").css("display","block")
                }
                if(d.data.planButton){
                    $(".by_stages").css("display","block")
                }


            });
        }

    });


</script>
</body>
</html>