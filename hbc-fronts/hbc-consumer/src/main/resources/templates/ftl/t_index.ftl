<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="${basePath!}/css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zybl.css"/>
    <link rel="stylesheet" href="${basePath!}/css/footer.css"/>
    <link rel="stylesheet" href="${basePath!}/js/layer_mobile/need/layer.css" />
    <link rel="stylesheet" href="${basePath!}/css/swiper-4.3.3.min.css">
    <link rel="stylesheet" href="${basePath!}/css/t_index.css">
    <title>花不愁同城消费</title>
    <style>
        .index_top{
            width: 100%;
            height: auto;
            padding-bottom: 10px;
            background: #fff;
        }
        .banner_img{
            width: 96%;
            height: 140px;
            margin-left: 2%;
        }
        .search .col-xs-8>p{
            margin: 0;
            color: #2C2C2C;
        }
        .swiper-container{
            width: 100%;
            height: 140px;
        }
        .swiper-container img{
            width: 100%;
            height: 140px;
            border-radius: 10px;
        }
        .top_menu{
            width: 100%;
            height: 40px;
            line-height: 40px;
        }
        .top_menu .col-xs-3{
            padding: 0;
            text-align: center;
        }
        .top_menu .col-xs-3>img{
            width: 11px;
        }
        .top_menu .col-xs-3 span{
            color: #909090;
        }
        .top_menu .col-xs-3:last-child{
            text-align: right;
            width: 20%;
        }
        .top_menu .col-xs-3:last-child span{
            color: #909090;
        }
        .top_menu .col-xs-3:last-child>img{
            width: 16px;
        }
        .top_menu .col-xs-6{
            padding: 0;
            width: 55%;
        }
        .top_menu .col-xs-6 img{
            width: 15px;
            margin-top: -12px;
        }
        .top_menu .col-xs-6 .col-xs-8{
            margin-top: -5px;
            margin-left: -30px;
        }
        .top_menu .search{
            width: 100%;
            height: 30px;
            background: #f5f5f5;
            border-radius: 30px;
            margin-top: 5px;
        }

        #menu{
            margin:5px auto;
            width:96%;
            border-radius: 10px;
        }
        @media (min-width: 375px) and (max-width: 979px){
            .p-top>ul>li>img {
                width: 18%;
            }
        }
        @media (min-width: 768px) and (max-width: 979px){
            .p-top>ul>li>img {
                width: 10%;
            }
            .m-sou>img {
                width: 5%;
                margin-left: 7%;
            }
        }
        @media (min-width:979px) and (max-width: 1200px){
            .p-top>ul>li>img {
                width: 8%;
            }
            .m-sou>img {
                width: 3%;
                margin-left: 7%;
            }
            #scan>img {
                width: 14%;
            }
        }
        .mui-active{
            color: #4671d5 !important;
        }
        .mui-fullscreen {
            position: absolute;
            top: 214px;
            right: 0;
            bottom: 0;
            left: 0;
        }
        #banner>img{
            width: 100%;
        }
        .carousel-indicators li{
            width:6px;
            height:6px;
            margin:0;
        }
        .carousel-indicators .active{
            width:6px;
            height:6px;

        }
        body{position: relative;background: #eee;}
        #footerBtn{position: absolute;width: 100%}
        .content>img{
            margin-top: 15%;
            width: 100px;
            height: 88px;
        }

        .content h3{
            font-size: 12px;
            color: #999999;
        }
        .f-menu .phone{
            text-align:center
        }
        .f-menu img{
            margin-bottom: 20%;
        }
        .shop_content .p1{
            white-space:nowrap;
            overflow:hidden;
            text-overflow:ellipsis;
        }
        .shop_content .p2{
            white-space:nowrap;
            overflow:hidden;
            text-overflow:ellipsis;
        }
        .p-fenqi{
            width: 100%;
            height: 330px;
        }
        .p-fenqi li{
            float: left;
            background: #fff;
            padding-left: 2%;
            padding-top: 2%
        }
        .p-fenqi h5{
            color: #666;
        }
        .p-fenqi img{
            width: 90%;
        }
        .p-fenqi p {
            line-height: 14px;
            margin-top: 10%;
            color: #ff0000;
            font-size: 0.9rem;
        }
        .f-menu p {
            font-size: 12px;
            line-height: 16px;
        }
        .p-fenqi li:nth-child(2n){
            border: 1px solid #dadada;
            border-bottom: none;
        }
        .fjsj2 .active a{
            color: #333;
        }

        .c_shop .shop_left p {
            font-size: 0.8em;
            color: #333;
            margin-top: 0;
        }
        .c_shop h3{
            margin-top: 5px;
        }
        .c_shop{
            border-bottom-left-radius: 8px;
            border-bottom-right-radius: 8px;
            margin-bottom: 80px;
        }
    </style>
</head>
<body>
<div class="index_top">
    <div class="top_menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3" id="dw">
                    <img src="${basePath!}/img/home_icon_position_gray.png" alt="">
                    <span id="location"></span>
                </div>
                <div class="col-xs-6">
                    <a href="${basePath!}/merchantQuery/Queryhotsearch">
                        <div class="search">
                            <div class="col-xs-4">
                                <img src="${basePath!}/img/imgs/icon_souyisou.png" alt="">
                            </div>
                            <div class="col-xs-8">
                                <p>试试搜索</p>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="col-xs-3" id="scanQRCode">
                    <img src="${basePath!}/img/home_icon_sm_gray.png" alt="">
                    <span>扫码</span>
                </div>
            </div>
        </div>
    </div>
    <div class="banner_img">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide-center none-effect"><a id="url0" href="#"> <img id="img0" src="${basePath!}/img/imgs/banner_01.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'"></a></div>
                <div class="swiper-slide"><a id="url1" href="#"> <img id="img1" src="${basePath!}/img/imgs/banner_02.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'"></a></div>
                <div class="swiper-slide"><a id="url2" href="#"> <img id="img2" src="${basePath!}/img/imgs/banner_1.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'"></a></div>
                <div class="swiper-slide"><a id="url3" href="#"> <img id="img3" src="${basePath!}/img/imgs/banner_04.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'"></a></div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>


<div class="container" id="menu">
    <div class="p-menu">
        <div class="hotel">
            <a href="#">
                <img src="${basePath!}/img/home_icon_gd.png" alt=""/>
                <p>更多</p>
            </a>
        </div>
    </div>
</div>

<!-------------优惠券----------------->
<div class="g-shop">
    <div class="huiyoujuan_text">
        <img alt="" src="${basePath!}/img/imgs/img_youhuijuan.png">
        优惠券
        <div class="wkt-flex text-right" style="margin-top: -2.9rem;margin-right: 3%;"id="yhq_tz">
            <a href="${basePath!}/packet/coupons" style="color: #7a7a7a;font-weight:400;font-size:0.8rem">更多<b style="color:#f49110">> </b></a>
        </div>
    </div>
    <div class="shop_content">
        <div class="shop_up">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-8" id="shop_left_0" style="padding-right: 1px">
                        <img src="${basePath}/img/home_img_one.png" onerror="this.src='${basePath}/img/imgs/picture_zhanwutupian.png'">
                        <span id="s0" class="s3" style="display:none"></span>
                    </div>
                    <div class="col-xs-4" id="shop_left_1" style="padding-left: 2px;">
                        <img src="${basePath}/img/home_img_two.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" alt="">
                        <span id="s1" class="s3" style="display:none"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="shop_down">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4" id="shop_left_2" style="padding-right: 2px">
                        <img src="${basePath!}/img/home_img_g.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" alt="">

                        <span id="s2" class="s3" style="display:none"></span>
                    </div>
                    <div class="col-xs-4" id="shop_left_3" style="padding-right: 1px;padding-left: 1px;">
                        <img src="${basePath!}/img/home_img_f.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" alt="">

                        <span id="s3" class="s3" style="display:none"></span>
                    </div>
                    <div class="col-xs-4" id="shop_left_4" style="padding-left: 2px">
                        <img src="${basePath!}/img/home_img_s.png" onerror="this.src='${basePath!}/img/imgs/picture_zhanwutupian.png'" alt="">
                        <span id="s4" class="s3" style="display:none"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="a_advertisement" style="clear: both">
    <img alt="" src="${basePath!}/img/home_smallbanner.png">
</div>
<div class="m_content">
    <div class="huiyoujuan_text">
        <img alt="" src="${basePath!}/img/myshop.png">
        附近商家
    </div>
    <div class="c_shop " id="shop">
        <ul>

        </ul>
        <div id="div" style="text-align: center;padding: 30px">
            <a href="javascript:void(0)" style="color: #999" id="dianji">点击查看更多</a>
        </div>
    </div>

</div>


<div class="footer" id="footer">
    <ul>
        <li class="active">
            <a href="${basePath!}/user/index/Goindex">
                <div class="toOne"></div>
                <p>首页</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/merchants/findNaturegoods?nature=null">
                <div class="toTow"></div>
                <p>附近商家</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/bill/jumpMyCredit">
                <div class="toThree"></div>
                <p>授信</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/user/order/order" class ="redirect">
                <div class="toFou"></div>
                <p>订单</p>
            </a>
        </li>
        <li>
            <a href="${basePath!}/consumer/center/jumpMine">
                <div class="toFive"></div>
                <p>我的</p>
            </a>
        </li>
    </ul>
</div>
<script src="${basePath!}/js/jquery.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script src="${basePath!}/js/swiper-4.3.3.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${basePath!}/js/template/template.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
<script type="text/javascript">
    define = null;
    require = null;
</script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
</body>

<script>
    $(document).ready(function() {
        var invateCode ='${invateCode!}';
        if(invateCode!=""&&invateCode!=null){
            window.sessionStorage.setItem("invateCode",invateCode);
        }
        var lat='null';
        var lng='null';
        var limit= 1;
        var page= 1;
        var types = null;
        var type = 0;
        $("#img").click(function(){
        });
        location();
        function location(){
            api.tools.location(function(d){
                lat = d.lat;
                lng = d.lng;
                $("#location").html(d.address.city);
                page = 1;
                loadMerchant();
            },function(){
                $("#location").html('定位失败');
                page = 1;
                loadMerchant();
            });
        }

        $("#dw").click(function(){
            location();
        });

        console.log("${ticketMap.jsapi_ticket!}");
        console.log("${ticketMap.url!}");
       //微信JS config接口注入权限验证配置
        wx.config({
            debug: false,
            appId: "${ticketMap.appId!}",
            timestamp:"${ticketMap.timestamp!}",
            nonceStr:"${ticketMap.noncestr!}",
            signature:"${ticketMap.sign!}",
            jsApiList : [ 'checkJsApi', 'scanQRCode' ]
        });

        wx.ready(function() {
            //扫描二维码
            document.querySelector('#scanQRCode').onclick = function() {
                wx.scanQRCode({
                    needResult : 0, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
                    scanType : [ "qrCode", "barCode" ], // 可以指定扫二维码还是一维码，默认二者都有
                    success : function(res) {
                        console.log(res);
                    },
                    fail : function(res) {
                        console.log(res)
                    }
                });
            };//end_document_scanQRCode

        });//end_ready


        loadfindNatures();
        loadlibao();
        banner();
        <#--//加载banner图片-->
        var element;
        function banner(){
            api.tools.ajax({
                url:'${basePath!}/user/index/QueryBanner',
            },function(d){
                if(d.data!= null && d.data != null && d.data.length > 0){
                    for(var i = 0 ; i < d.data.length ; i++){
                        var data = d.data[i];
                        //图片赋值
                       // element = document.getElementById(('img'+i));
                        //element.src = data.img;
                        $("#img"+i).attr("src",data.img);
                        $("#url"+i).attr('href',"${basePath!}/nearby/findById?id="+data.href);
                    }
                }
            });
        }
        function loadMerchant(){
            api.tools.ajax({
                url:'${basePath!}/nearby/findList',
                data:{
                    lat:lat,
                    lng:lng,
                    page:page,
                    micId:"null"
                }
            },function(d){
                var data=d.data.records;
                if(data.length<10){
                    $("#div").hide();
                }
                if(data.length>=10){
                    $("#div").show();
                }
                if(data!= null    && data.length > 0){
                    for(var i = 0 ; i < data.length ; i++){
                        var data1 = data[i];
                        data1.path = '${basePath!}';
                        if(data1.fullMoney==0 && data1.money==0){
                            data1.coupon='';
                        }else {
                            data1.coupon = '满' + data1.fullMoney + '减' + data1.money + '元';
                        }
                        if(data1.localPhoto!=null) {
                            var le =data1.localPhoto.lastIndexOf('.',data1.localPhoto.length);
                            var img = data1.localPhoto.substr((le), data1.localPhoto.length);
                            data1.img ="_65x65"+img;
                        }else{
                            data1.img = "";
                        }
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "index_merchant_list",
                            data : data1
                        });
                        $html = $(html);
                        $('#shop').find('ul').append($html);
                        $html.find('.detail').click(function(){

                            window.location = '${basePath!}/nearby/findById?id='+$(this).attr('data');


                        });
                    }
                }else{
                    if(page==0) {
                        $('#shop').html('<div class="content" align="center" style="background:#efeff4">' +
                                '<img src="${basePath!}/img/imgs/icon_zanwujilu.png" alt=""/>' +
                                '<h5>暂无记录...</h5>' +
                                '</div>');
                    }

                    loadEnd = true;
                }
                loading = false;
            });
        }

        //商户分类更多跳转
        $(".hotel").click(function(){
            window.location = '${basePath!}/merchants/findNature';
        });
        $(".p-gg").click(function(){
            window.location = '${basePath!}/b/apply';
        });

        //查询商户分类
        var   nature= 	null;
        var   nature0= 	null;
        var   nature1= 	null;
        var   nature2= 	null;
        var   nature3= 	null;
        var   nature4= 	null;

        function loadfindNatures(){
            api.tools.ajax({
                url:'${basePath!}/merchants/Querymerchantcategory',
                data:{}
            },function(d){
                console.log(d);
                if(d.data!= null ){
                    var list = new Array();

                    for (var i = 0; i < d.data.length; i++) {
                        if(i==0)
                        {
                            var nature0 = d.data[i].id;
                        }
                        if(i==1)
                        {
                            var nature1 = d.data[i].id;
                        }
                        if(i==2)
                        {
                            var nature2 = d.data[i].id;
                        }
                        if(i==3)
                        {
                            var nature3 = d.data[i].id;
                        }
                        if(i==4)
                        {
                            var nature4 = d.data[i].id;
                        }
                        if(i==5)
                        {
                            var nature5 = d.data[i].id;
                        }
                        if(i>=4)break;
                        list[i] = d.data[i];
                        list[i].icon = d.data[i].icon;
                        list[i].img ="";
                    }

                    var html = template.load({
                        host : '${basePath!}/',
                        name : "index_natures_list",
                        data:{
                            data :  list
                        }
                    });

                    $html = $(html);
                    $('.p-menu').prepend($html);
                    $(".Leisure a").click(function(){
                        nature=	$(this).children("span").html();
                        window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature;
                    });

                    //优惠券跳转
                    if(nature0==null || nature0 ==""){
                        $("#shop_left_0").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+'null';
                        })
                    }else {
                        $("#shop_left_0").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature0;
                        })
                    }

                    if(nature1==null || nature1 ==""){
                        $("#shop_left_1").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+'null';
                        })
                    }else {
                        $("#shop_left_1").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature1;
                        })
                    }

                    if(nature2==null || nature2 ==""){
                        $("#shop_left_2").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+'null';
                        })
                    }else {
                        $("#shop_left_2").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature2;
                        })
                    }

                    if(nature3==null || nature3 ==""){
                        $("#shop_left_3").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+'null';
                        })
                    }else {
                        $("#shop_left_3").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature3;
                        })
                    }

                    if(nature4==null || nature4==""){
                        $("#shop_left_4").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+'null';
                        })
                    }else {
                        $("#shop_left_4").click(function(){
                            window.location = '${basePath!}/merchants/findNaturegoods?nature='+nature4;
                        })
                    }

                }
            });
        }
        $("#dianji").click(function(){
            page = page+1;
            loadMerchant();
        });
        function loadyhqxi(){
            api.tools.ajax({
                url:'${basePath!}/redpacket/loadRedPacket',
                data:{
                }
            },function(d){
                if(d.data!= null ){
                    for (var int = 0; int < d.data.length; int++) {
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "merchant_list",
                            data:{
                                data : d.data
                            }
                        });
                        $html = $(html);
                        for(var i = 0 ; i < d.data.length ; i++){
                            if($('#'+d.data[i].miId).html() == ""){
                                $("#"+d.data[i].miId).append("满"+d.data[i].fullMoney+"减"+d.data[i].money);
                            }
                        }
                    }
                }
            });
        }

        function loadweather(){
            api.tools.ajax({
                url:'${basePath!}/weather',
                data:{
                    lat:lat,
                    lng:lng,
                }
            },function(d){
                if(d.data!= null ){
                    d.data.data.qw = parseInt(d.data.data.qw);
                    var html = template.load({
                        host : '${basePath!}/',
                        name : "weather_list",
                        data :d.data.data
                    });
                }
            },function(d){
                if(d.data!= null ){
                    for (var int = 0; int < d.data.length; int++) {
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "merchant_list",
                            data:{
                                data : d.data
                            }
                        });
                        $html = $(html);
                        for(var i = 0 ; i < d.data.length ; i++){
                            if($('#'+d.data[i].miId).html() == ""){
                                $("#"+d.data[i].miId).append("满"+d.data[i].fullMoney+"减"+d.data[i].actualMoney);
                            }
                        }
                    }
                }
            });
        }
        function loadweather(){
            api.tools.ajax({
                url:'${basePath!}/weather',
                data:{
                    lat:lat,
                    lng:lng,
                }
            },function(d){
                if(d.data!= null ){
                    d.data.data.qw = parseInt(d.data.data.qw);
                    var html = template.load({
                        host : '${basePath!}/',
                        name : "weather_list",
                        data :d.data.data
                    });
                }
            },function(d){
                if(d.data!= null ){
                    for (var int = 0; int < d.data.length; int++) {
                        var html = template.load({
                            host : '${basePath!}/',
                            name : "merchant_list",
                            data:{
                                data : d.data
                            }
                        });
                        $html = $(html);
                        for(var i = 0 ; i < d.data.length ; i++){
                            if($('#'+d.data[i].miId).html() == ""){
                                $("#"+d.data[i].miId).append("满"+d.data[i].fullMoney+"减"+d.data[i].actualMoney);
                            }
                        }
                    }
                }
            });
        }

        //轮播图
        var mySwiper = new Swiper ('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
            },
            autoplay:{
                delay: 2500,
                disableOnInteraction: false,
            },
            speed:1000,
            centeredSlides: true,
            paginationClickable: true,
            slideShadows : true,

        });
        function loadlibao(){
            api.tools.ajax({
                <#--url:'${basePath!}/userInfoWx/xyh1.htm',-->
                data:{
                }
            },function(d){
                if (d.data.pid != null) {
                    loadxyh();
                }

            });
        }
        function loadxyh(){
            api.tools.ajax({
                url:'${basePath!}/userInfoWx/xyh',
                data:{
                }
            },function(d){
                if(d.resultCode !=1 ){
                    window.location = '${basePath!}/userInfoWx/newhd';
                }else{
                    if(d.data == ""){
                        window.location = '${basePath!}/userInfoWx/newhd';
                    }
                }
            });
        }
    });
</script>

</html>