<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="${basePath!}/css/app.css"/>
    <link rel="stylesheet" href="${basePath!}/css/zy.css"/>
    <link rel="stylesheet" href="${basePath!}/css/y.css"/>
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css"/>

    <title>钱包管理</title>
    <style>
        body,
        html {
            background: #fff;
        }

        .pp-top {
            width: 100%;
            height: 160px;
            background: url("${basePath!}/img/img/merchant_my_background.png") no-repeat;
            background-size: cover;
            text-align: center;

        }

        .tixian {
            text-align: right;
            padding: 10px;
        }

        .tixian > a {
            color: #fff;
            text-decoration: none;
            font-size: 0.8rem
        }

        .pp-top .num {

            font-size: 2rem;
            color: #fff;
            margin-top: 5%;
        }

        .pp-top .num > p {
            color: #fff;
            margin-top: 10px;
            font-size: 0.8rem
        }

        .c-content {
            background: transparent;
            padding: 0;
        }

        .c-content li {
            text-align: center;
            margin-left: 20px;
            margin-right: 20px
        }

        .m_tab {
            line-height: 48px;
            height: 48px;
            background: transparent;
            border-bottom: 1px solid #ededed;
        }

        .active > a {
            color: #f49110 !important;

        }

        .m_tab .active {
            border-bottom: 2px solid #f49110;

        }

        .m_tab a {
            color: #666;

        }

        .m_content ul li {
            padding: 10px 10px 0 10px;
            border-bottom: 1px solid #eee;
            height: auto;
            overflow: hidden;
        }

        .m_content .text-left {
            color: #ffa170;
        }

        .m_content .text-left p {
            font-size: 0.8rem;
        }

        .m_content .text-right {
            color: #f49110;
            font-size: 0.8rem;
        }

        .m_content .text-right p {
            font-size: 0.8rem;
        }

        .zanwu {
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="pp-top">
    <div class="tixian">
        <a href="${basePath!}/wallet/WalletTx">
            立即提现
        </a>
    </div>
    <div class="num">
    <#if w.totalAmount != 0>
        ${w.totalAmount!?c}
            <p> 账户余额</p>
</#if>
<#if w.totalAmount == 0>
            0.00
            <p> 账户余额</p>
</#if>
    </div>
</div>
<div class="c-content">
    <div class="m_tab">
        <ul class="webkit-box">
            <li class="active wkt-flex" id="sr"><a href="#all" data-toggle="tab">收入记录</a></li>
            <li class="wkt-flex" id="zc"><a href="#zhichu" data-toggle="tab">支出记录</a></li>
            <li class="wkt-flex" id="tx"><a href="#dfk" data-toggle="tab">提现记录</a></li>
        </ul>
    </div>
</div>
<div class="m_content tab-content " style="margin-top: 5px">
    <div class="c_thing tab-pane active" id="all">
        <ul>

        </ul>
    </div>
    <div class="c_thing tab-pane " id="zhichu">
        <ul>

        </ul>
    </div>
    <div class="c_thing tab-pane" id="dfk">
        <ul>

        </ul>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/bootstrap.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery-qrcode-0.14.0.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<script src="${basePath!}/js/template/template.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>
    $(document).ready(function () {

        api.tools.ajax({
            url: '${basePath!}/wallet/findWalletIncome',
            data: {
                wid: '${w.id}',
                utype: '99'
        }
    }, function (d) {
        if (d.data != null &&  d.data.length > 0) {
            for (var i = 0; i < d.data.length; i++) {
                data = d.data[i];
                if (data.type == 1) {
                    data.zt = "还款入金";
                }
                else  if (data.type == 2){
                    data.zt = "现金消费入金";
                } else  if (data.type == 3){
                    data.zt = "授信金结算";
                }else  if (data.type == 4){
                    data.zt = "返佣入金";
                }else  if (data.type == 5){
                    data.zt = "提现驳回入金";
                }
                if (data.settlementStatus == 0){
                    data.jszt = "未结算"
                }else {
                    data.jszt = "已结算"
                }
                html = template.load({
                    host: '${basePath!}/',
                    name: 'wallet_rz',
                    data: data
                });

                $html = $(html);
                $("#all ul").append($html);
            }
        } else {
            $("#all ul").append("<div class='zanwu'>暂无记录</div>");
        }
    });

        api.tools.ajax({
            url: '${basePath!}/wallet/findWalletIncome',
            data: {
                wid: '${w.id}',
                utype: '1'
            }
        }, function (d) {
            if (d.data != null &&  d.data.length > 0) {
                for (var i = 0; i < d.data.length; i++) {
                    data = d.data[i];
                    if (data.type == 6) {
                        data.zt = "提现支出";
                    }
                    else  if (data.type == 7){
                        data.zt = "消费支出";
                    }

                    html = template.load({
                        host: '${basePath!}/',
                        name: 'wallet_tx',
                        data: data
                    });

                    $html = $(html);
                    $("#zhichu ul").append($html);
                }
            } else {

                $("#zhichu ul").append("<div class='zanwu'>暂无记录</div>");
            }
        });

        api.tools.ajax({
            url: '${basePath!}/wallet/findWalletwithdrawals',
            data: {
                uid: '${w.uid}',
                utype: '1'
            }
        }, function (d) {
            if (d.data != null &&  d.data.length > 0) {

                for (var i = 0; i < d.data.length; i++) {
                    data = d.data[i];
                    if (data.state ==1){
                        data.zt='申请中';
                    }
                    else if (data.state ==2){
                        data.zt='提现成功';
                    }
                    else  if (data.state ==3){
                        data.zt='提现失败';
                    }
                    html = template.load({
                        host: '${basePath!}/',
                        name: 'wallet_qx',
                        data: data
                    });

                    $html = $(html);
                    $("#dfk ul").append($html);
                }
            } else {
                $("#dfk ul").append("<div class='zanwu'>暂无记录</div>");
            }
        });
    });
</script>

</html>