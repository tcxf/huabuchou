<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/app.css" />
    <link rel="stylesheet" href="${basePath!}/css/yys.css" />
    <link rel="stylesheet" href="${basePath!}/js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />

    <title>立即提现</title>
    <style>
        .input-left-name{
            width: 100%;
        }
        .input-left-name select{

            height: 30px;
            line-height: 30px;
            padding: 0 10px;
        }
        ::-webkit-input-placeholder{
            font-size: 14px;
        }
        .p-input .col-xs-5{
            padding-left: 0;
        }
        .p-input .col-xs-5>input{
            font-size: 1.6rem!important;
        }
        .p-dz{
            font-size: 12px;
        }
        .btn_block{
            margin-top: 20px;
            width: 94%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            margin-left: 3%;
            border-radius: 30px;
            color: #fff;
            background: #f49110;
            padding: 0;
            border: none;
            outline: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="a-content">
   <div class="webkit-box">
        <div class="input-left-name">
            <#if bind?exists && (bind?size > 0) >
                <input type="hidden" id="cardListEmpty" value="no"/>
                <select id="bank_select" style="background:#f5f5f5;">
                <#list bind as b>
                   <option value="${b.id}">${b.bankName!} ${b.bankCard!?substring((b.bankCard)?length-4,(b.bankCard)?length)} </option>
               </#list>
                </select>
           <#else>
              <span> <a href="${basePath!}/bank/savebank" style="color: #333;text-decoration: none">暂无绑卡</a> </span>
                <input type="hidden" id="cardListEmpty" value="yes"/>
          </#if>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-5" style="padding-top:10px">
                提现金额
            </div>
        </div>
        <div class="row p-input">
            <div class="col-xs-1">
                <p>￥</p>
            </div>
            <div class="col-xs-5">
                <input type="text" name="phone" id="phone" class="form-control reg-input" placeholder="请输入提现金额"/>
            </div>
            <div class="col-xs-6" style="margin-top: 3%">
                <div id="sjje" style="font-size: 0.8rem;text-align: left;color:#999">实际到账：</div>
                <div class="p-dz" id='sxje'></div>
            </div>
        </div>
        <div class="webkit-box" style="margin-top: 2%">
            <div class="input-left-name" style="width:160px;">
                <b style="color: #999">账户余额 </b><span style="color:#333;font-size: 0.8rem;font-weight:600;">￥
                   <#if w.totalAmount != 0>
                        ${w.totalAmount!}
                   </#if>
<#if w.totalAmount == 0>
            0.00
</#if>
                </span>
            </div>
            <div class="wkt-flex " style=" margin-top: 2.5%">

                <span style="color:#f49110;font-size:0.8rem;cursor: pointer" id="qb">全部提现</span>

            </div>
        </div>
    </div>
</div>
<button class="btn_block">下一步</button>


</body>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src ="${basePath!}/js/template/template.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {

        var sjje=null;
        var sjtxje =null;
        var txsxje =null;
        //限制输入框只能输入数字并到小数点后两位
        $("#phone").bind("keypress", function(event) {
            var event= event || window.event;
            var getValue = $(this).val();
            //控制第一个不能输入小数点"."
            if (getValue.length == 0 && event.which == 46) {
                event.preventDefault();
                return;
            }
            //控制只能输入一个小数点"."
            if (getValue.indexOf('.') != -1 && event.which == 46) {
                event.preventDefault();
                return;
            }
            //控制小数点后只能输入两位数
            if(getValue.indexOf('.')!=-1 && getValue.length-getValue.indexOf('.')>2){
                event.preventDefault();
                return;
            }
            //控制只能输入的值
            if (event.which && (event.which < 48 || event.which > 57) && event.which != 8 && event.which != 46) {
                event.preventDefault();
                return;
            }
        })
        //失去焦点是触发
        $("#shop-price").bind("blur", function(event) {
            var value = $(this).val(), reg = /\.$/;
            if (reg.test(value)) {
                value = value.replace(reg, "");
                $(this).val(value);
            }
        });

        $(function() {
            $("#bbb").on("click",function(){
                $("#aaa").toggleClass("colse");
                var imgObj=document.getElementById("imgpp");
                if(imgObj.getAttribute("src",2)=="<%=path %>/img/img/img_quick_yes.png"){
                    imgObj.src="${basePath!}/img/img/img_quick_no.png";
                }else{
                    imgObj.src="${basePath!}/img/img/img_quick_yes.png";
                }
            });
        });
        $("#qb").click(function(){
            var zje =${w.totalAmount!?c};
            $("#phone").val(zje);
            sjje=  $("#phone").val();
            if ('${a.userTypeId!}'=='0'){
                if (${c.presentType!} == 1){
                    if (${c.presentContent!}==0){
                        txsxje  = sjje ;
                        sjtxje  = sjje -txsxje;
                        $("#sjje").html("实际到账：￥" + sjtxje);
                        $("#sxje").html("提现手续费：￥"+ txsxje);
                    }else{
                        txsxje = sjje *(${c.presentContent!}/100);
                        txsxje =    Math.round(txsxje * 100) / 100;
                        sjtxje  = sjje -txsxje;
                        sjtxje =    Math.round(sjtxje * 100) / 100;
                        $("#sjje").html("实际到账：￥" + sjtxje);
                        $("#sxje").html("提现手续费：￥"+ txsxje);
                    }
                } else if (${c.presentType!} == 2){
                    sjtxje  = sjje - ${c.presentContent!};
                    sjtxje =    Math.round(sjtxje * 100) / 100;
                    txsxje = sjje -sjtxje;
                    $("#sjje").html("实际到账：￥" + sjtxje);
                    $("#sxje").html("提现手续费：￥"+ txsxje);
                }
            }else if ('${a.userTypeId!}'=='1'){
                var list = JSON.parse('${m!}');
                sjje=  $("#phone").val();
                for (var i = 0; i <list.length ; i++) {
                    if (list[i].maximumIntervalValue == ""){
                        var max= -1;
                    } else {
                        var max = parseInt(list[i].maximumIntervalValue);
                    }
                    var min=    parseInt(list[i].minimumIntervalValue);
                    if (max == -1) {
                        if (sjje >= min) {
                        if (list[i].presentType == '1') {
                            txsxje = sjje * (list[i].presentContent / 100);
                            txsxje =    Math.round(txsxje * 100) / 100;
                            sjtxje = sjje - txsxje;
                            sjtxje =    Math.round(sjtxje * 100) / 100;
                            $("#sjje").html("实际到账：￥" + sjtxje);
                            $("#sxje").html("提现手续费：￥" + txsxje);
                            return;
                        } else if (list[i].presentType == '2') {
                             sjtxje= sjje - list[i].presentContent;
                            sjtxje =    Math.round(sjtxje * 100) / 100;
                            txsxje = sjje - sjtxje;
                            $("#sjje").html("实际到账：￥" + sjtxje);
                            $("#sxje").html("提现手续费：￥" + txsxje);
                            return;
                        }
                        }
                    }else {
                        if (sjje <= max) {
                            if (sjje >= min) {
                                if (list[i].presentType == '1') {
                                    txsxje = sjje * (list[i].presentContent / 100);
                                    txsxje =    Math.round(txsxje * 100) / 100;
                                    sjtxje = sjje - txsxje;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                } else if (list[i].presentType == '2') {
                                    sjtxje   = sjje - list[i].presentContent;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    txsxje = sjje - sjtxje;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                }
                            }
                        }
                    }
                }
            }

        });

        $("#phone").bind('input propertychange', function() {
            if($(this).val().length==0){
                $("#sjje").html("实际到账：￥0");
            }else{
                sjje = $(this).val();
                sjje =  Math.round(sjje * 100) / 100;
                if ('${a.userTypeId!}'=='0'){
                    if (${c.presentType!} == 1){
                        if (${c.presentContent!}==0){
                            txsxje  = sjje ;
                            sjtxje  = sjje -txsxje;
                            $("#sjje").html("实际到账：￥" + sjtxje);
                            $("#sxje").html("提现手续费：￥"+ txsxje);
                        }else{
                            txsxje = sjje *(${c.presentContent!}/100);
                            txsxje =    Math.round(txsxje * 100) / 100;
                            sjtxje  = sjje -txsxje;
                            sjtxje =    Math.round(sjtxje * 100) / 100;
                            $("#sjje").html("实际到账：￥" + sjtxje);
                            $("#sxje").html("提现手续费：￥"+ txsxje);
                        }
                    } else if (${c.presentType!} == 2){
                        sjtxje    = sjje - ${c.presentContent!};
                        sjtxje =    Math.round(sjtxje * 100) / 100;
                        txsxje = ${c.presentContent!};
                        $("#sjje").html("实际到账：￥" + sjtxje);
                        $("#sxje").html("提现手续费：￥"+ txsxje);
                    }
                }else if ('${a.userTypeId!}'=='1'){
                    var list = JSON.parse('${m!}');
                    sjje = $(this).val();
                    for (var i = 0; i <list.length ; i++) {
                        if (list[i].maximumIntervalValue == ""){
                            var max= -1;
                        } else {
                            var max = parseInt(list[i].maximumIntervalValue);
                        }
                    var min=    parseInt(list[i].minimumIntervalValue);
                    if (max == -1) {
                        if (sjje >= min) {
                            if (list[i].presentType == '1') {
                                txsxje = sjje * (list[i].presentContent / 100);
                                txsxje = Math.round(txsxje * 100) / 100;
                                sjtxje = sjje - txsxje;
                                sjtxje =    Math.round(sjtxje * 100) / 100;
                                $("#sjje").html("实际到账：￥" + sjtxje);
                                $("#sxje").html("提现手续费：￥" + txsxje);
                                return;
                            } else if (list[i].presentType == '2') {
                                sjtxje = sjje - list[i].presentContent;
                                sjtxje =    Math.round(sjtxje * 100) / 100;
                                txsxje = list[i].presentContent;
                                $("#sjje").html("实际到账：￥" + sjtxje);
                                $("#sxje").html("提现手续费：￥" + txsxje);
                                return;
                            }
                        }
                    }else {
                        if (sjje <= max) {
                            if (sjje >= min) {
                                if (list[i].presentType == '1') {
                                    txsxje = sjje * (list[i].presentContent / 100);
                                    txsxje =    Math.round(txsxje * 100) / 100;
                                    sjtxje = sjje - txsxje;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                } else if (list[i].presentType == '2') {
                                    sjtxje    = sjje - list[i].presentContent;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    txsxje = list[i].presentContent;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                }
                            }
                        }
                    }
                    }
                }
            }
        });

        $(".btn_block").click(function(){
            var options=$("#bank_select option:selected");
            var bid = options.val();
            var pitch =options.text();
            var txje=$("#phone").val();
            var balance='${w.totalAmount?c}'; //余额
            if(($("#phone").val())==null){
                api.tools.toast('请输入提现的金额');
            }
            if(txje==null || txje=="" ){
                api.tools.toast('请输入提现的金额');
                return;
            }
            if(parseFloat(txje) <= 2){
                api.tools.toast('提现的金额不能少于3元');
                return;
            }
            if(parseFloat(txje) > parseFloat(balance)){
                api.tools.toast('余额不足');
                return;
            }
            var cardListEmpty = $("#cardListEmpty").val();
            if(cardListEmpty == "yes"){
                api.tools.toast('您还未绑卡，请先绑卡');
                window.location.href = "${basePath!}/bank/savebank?bk="+1;
            }else{

                var tc = "<div>" +
                        '实际到账'+
                        "<span style='color:#f49110;font-size: 18px'>" +
                        sjtxje+
                        "</span>" +
                        '元,您的结算款将会结算到您的'+
                        "<span>" +
                        pitch+
                        "</span>" +
                        '的银行卡中，是否确认?'+
                        "</div>";

                layer.open({
                //    content: '实际到账'+sjtxje+'元,您的结算款将会结算到您的'+pitch+'的银行卡中，是否确认?',
                    content:tc,
                    btn: ['确定', '取消'],
                    yes: function(){
                        window.location.href = '${basePath!}/wallet/findtxdx?bid=' + bid + '&txje=' + txje + '&sjtxje=' + sjtxje+ '&txsxje=' + txsxje;
                        $("#phone").val("");
                    }

                });
            }
        });
    });
</script>
</html>