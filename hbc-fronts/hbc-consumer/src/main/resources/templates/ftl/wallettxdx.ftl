<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />
    <title>提现发送短信</title>
    <style>
        body {
            background: #fff;
        }
        #top-title>p{
            padding:15px 0 10px 20px;
            color: #666;

        }
        .wirte_yzm>.container-fluid{
            height: 40px;
            line-height: normal;
        }
        .wirte_yzm .col-xs-10{
            padding-left: 10px;
            padding-right: 0;
            width: 70%;
            font-size: 12px;
        }
        .wirte_yzm input{
            border: none;
            outline: none;
            width: 140px;
            background: transparent;
            font-size: 12px;
        }
        .wirte_yzm .col-xs-2{
            width: 30%;
            padding: 0;
        }
        .wirte_yzm .col-xs-2>a{
            padding: 3px 5px;
            border: 1px solid #f49110;
            text-decoration: none;
            color: #f49110;
            border-radius: 30px;
            font-size: 12px;
        }
        .b_line{
            width: 90%;
            height: 1px;
            background: #ededed;
            margin-top: 10px;
            margin-left: 5%;
        }
        .tj{
            background: #f49110;
            color:#ffffff;
            border-radius: 30px;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left: 3%;
            margin-top: 50px;
            text-align: center;
        }
    </style>
</head>
<body>
<div id="top-title">
    <p>短信将发送到您 ${b.mobile!?substring(0,3)}****${b.mobile!?substring((b.mobile)?length-4,(b.mobile)?length)} 的手机上，请注意查收</p>
</div>
<div class="wirte_yzm">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-10">
                <span>短信验证码:</span>
                <input type="text" id="yCode" class="reg-input" placeholder="请输入您的短信验证码">
            </div>
            <div class="col-xs-2">
                <a class="sms-btn" href="#" role="button" id="hq">获取验证码</a>
            </div>
        </div>
    </div>
</div>
<div class="b_line"></div>
<div class="yzm-btn tj">
    确认提现
</div>

<script type="text/javascript" src="${basePath!}/js/jquery.js"></script>
<script type="text/javascript" src="${basePath!}/js/mui/mui.min.js"></script>
<script type="text/javascript" src="${basePath!}/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="${basePath!}/js/info.js"></script>
<script type="text/javascript" src ="${basePath!}/js/template/template.js"></script>
<#include "./agreement/mune_btn.ftl">
<script>
    $(document).ready(function() {


        $("#hq").click(function(){
            $.ajax({
                url:"${basePath!}/wallet/getsys",
                type:'POST', //GET
                data:{mobile:${b.mobile},
                    type:"YZM"},
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    if(d.code == 1){
                        startTime();

                    }
                }
            });


        });
        var i = 60;
        function startTime(){
            $("#hq").html("重新发送("+i+"秒)");
            if(i > 0){
                i--;
                setTimeout(function(){
                    startTime();
                },1000);
            }else{
                $("#hq").html("获取验证码");
                $("#hq").removeClass("disabled");
                i = 60;
            }
        }

        $(".tj").on("click",function(){
            var yCode =   $("#yCode").val();
            if (yCode == '' ) {
                api.tools.toast("验证码不能为空");
                return;
            }
            $.ajax({
                type:"post",
                url:"${basePath!}/wallet/walletdoReg",
                data:{txje:${txje },
                    sjtxje:${sjtxje},
                    txsxje:${txsxje},
                    yCode:$("#yCode").val(),
                    type:"YZM",
                    bid:'${b.id}'
                },
                success:function(msg){
                    if(msg.code == 0){
                        api.tools.toast(msg.msg);
                        window.location.href = "${basePath!}/wallet/waleettxwc";
                    }else {
                        api.tools.toast(msg.msg);
                    }


                },
                dataType : "json"
            });



        });
    });
</script>
</body>
</html>
