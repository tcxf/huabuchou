<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap/css/bootstrap.css" />

    <title>提现申请成功</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 10rem;
        }
        .s_content{
          padding: 20px;

        }
        .s_content>p{
            color: #666;
        }
        .tj{
            margin-top: 40%;
            background:#f49110;
            color:#ffffff;
            border-radius:30px;
            height: 40px;
            line-height: 40px;
            width: 94%;
            margin-left:3%;
            text-align: center;

        }
    </style>

</head>
<body>
<div class="top">
    <img src="${basePath!}/img/img/img_is_ok.png" alt=""/>
</div>
<div class="s_content">
    <p>您的提现申请已成功，系统将在24小时内为您处理
        请您耐心等待。</p>
</div>
<div class="yzm-btn tj">
    完成
</div>
<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
<script>
    $(document).ready(function() {


        $(".tj").on("click",function(){
            window.location.href = "${basePath!}/user/index/Goindex";
        });
    });


</script>
</body>
</html>