package com.tcxf.hbc.merchantpc.common.config;

import com.tcxf.hbc.common.bean.resolver.TokenArgumentResolver;
import com.tcxf.hbc.merchantpc.common.interceptor.LoginInterce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * @author liaozeyong
 * @date 2018年7月3日17:29:37
 */
@Configuration
public class ConsumerWebmvcConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private CacheManager cacheManager;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new TokenArgumentResolver(cacheManager));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new LoginInterce()).addPathPatterns("/**").excludePathPatterns("");
        super.addInterceptors(registry);
    }

}