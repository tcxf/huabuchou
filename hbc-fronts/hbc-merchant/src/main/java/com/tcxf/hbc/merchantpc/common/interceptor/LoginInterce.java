package com.tcxf.hbc.merchantpc.common.interceptor;


import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.bean.interceptor.BaseInterceptor;

import com.tcxf.hbc.common.util.ValidateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.data.redis.core.RedisTemplate;

import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


public class LoginInterce extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(BaseInterceptor.class);


    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        RedisTemplate  redisTemplate = (RedisTemplate) factory.getBean("redisTemplate");
        //判断方法是否需要进行登录验证
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        //方法级上是否需要登录验证，如果不需要登录验证,直接通过
        if (method.getAnnotation(AuthorizationNO.class) != null) {
            return true;
        }
        String cookieValue = getCookieValue(request,"m_id");
        //判断cook为空跳转登陆
        if (cookieValue == null){
            String basePath = request.getScheme() + "://"+request.getServerName() +
                    ":" + request.getServerPort() + request.getContextPath() + "/";
            response.sendRedirect(basePath+"login/gologin");
            return  false;
        }else {
            //判断redis不为空
        Object id = redisTemplate.opsForValue().get(cookieValue);
        if (ValidateUtil.isEmpty(id)){
            String basePath = request.getScheme() + "://"+request.getServerName() +
                    ":" + request.getServerPort() + request.getContextPath() + "/";
            response.sendRedirect(basePath+"login/gologin");
            return  false;
        }
        }
        return true;
    }
    private String getCookieValue(HttpServletRequest request, String name){
        Map<String,Cookie> map = ReadCookieMap(request);
        Cookie cookieValue = map.get(name);
        if(ValidateUtil.isEmpty(cookieValue)){
            return null;
        }
        return cookieValue.getValue();
    }

    private Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
        Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
        Cookie[] cookies = request.getCookies();
        if(null!=cookies){
            for(Cookie cookie : cookies){
                cookieMap.put(cookie.getName(), cookie);
            }
        }
        return cookieMap;
    }
}
