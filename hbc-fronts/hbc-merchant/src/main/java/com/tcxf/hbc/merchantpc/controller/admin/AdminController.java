package com.tcxf.hbc.merchantpc.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.AdminService;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import com.tcxf.hbc.merchantpc.vo.AdminVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedHashMap;

@RestController
@RequestMapping("/admin")
public class AdminController  extends BaseController {

    @Autowired
    public AdminService adminService;
    @Autowired
    public OrderInfoService orderInfoService;

    /**
     * 跳转管理员页面
     * @return
     */
    @RequestMapping("/findAdmin")
    public ModelAndView findAdmin(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/admin/adminManager");
        return mode;
    }

    /**
     * 查询所有管理员
     * @param vo
     * @return
     */
    @RequestMapping(value = "/findAdminList",method = RequestMethod.POST)
    public R findAdminList(AdminVo vo, HttpServletRequest request){
        //session
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid=dtoR.getData().getmMerchantInfo().getId();
        if(("").equals(vo.getAdminName())){
            vo.setAdminName("null");
        }
        R<Object> r = new R<>();
        Page page = adminService.findAdminList(mid,vo.getPage(),vo.getAdminName());
//        LinkedHashMap data = (LinkedHashMap) r.getData();
//        Page page = (Page) r.getData();
        r.setData(page);
        return r;
    }

    /**
     * 跳转到添加管理员页面
     * @return
     */
    @RequestMapping("/addAmin")
    public ModelAndView addAmin(String id){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/admin/adminAdd");
        return mode;
    }

    /**
     * 添加管理员
     * @param vo
     * @return
     */
    @RequestMapping(value = "/insertAdmin",method = RequestMethod.POST)
    public R insertAdmin(PAdmin vo,HttpServletRequest request){
        //session
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid=dtoR.getData().getmMerchantInfo().getId();
        vo.setAid(mid);
        vo.setType(0);
        vo.setCreateDate(new Date());
        vo.setModifyDate(new Date());
        return adminService.insertAdmin(vo);
    }

    /**
     * 修改管理员页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/updateAdmin",method = {RequestMethod.GET})
    public ModelAndView updateAdmin(String id){
       ModelAndView mode = new ModelAndView();
       R r = adminService.updateAdmin(id);
       mode.addObject("admin",r.getData());
       mode.setViewName("ftl/admin/adminHandler");
        return mode;
    }

    /**
     * 根据id修改管理员
     * @param vo
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(AdminVo vo){
        return adminService.updateById(vo);
    }

    /**
     * 删除管理员
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    public R deleteById(String id){
        return adminService.deleteById(id);
    }

    /**
     * 跳转到分配角色页面
     * @return
     */
    @RequestMapping(value = "/selectRole",method = RequestMethod.GET)
    public ModelAndView selectRole(String id,HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        //session
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid=dtoR.getData().getmMerchantInfo().getId();
        mode.addObject("id",id);
        mode.addObject("list",adminService.selectRole(mid).getData());
        mode.setViewName("ftl/admin/selectRole");
        return mode;
    }

    /**
     * 根据管理id查询该管理员的角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/selectAdminRoleStr",method = RequestMethod.POST)
    public R selectAdminRoleStr(String id){
        return adminService.selectAdminRoleStr(id);
    }

    /**
     * 根据管理员id分配角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/distribution",method = RequestMethod.POST)
    public R distribution(String id,String ids){
        return adminService.distribution(id,ids);
    }




}
