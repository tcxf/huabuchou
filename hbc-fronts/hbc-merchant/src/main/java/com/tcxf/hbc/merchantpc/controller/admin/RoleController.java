package com.tcxf.hbc.merchantpc.controller.admin;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.MRoleService;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import com.tcxf.hbc.merchantpc.vo.RoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    public MRoleService mRoleService;
    @Autowired
    public OrderInfoService orderInfoService;

    @RequestMapping("/faindRole")
    public ModelAndView faindRole(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/role/roleManager");
        return  mode;
    }

    /**
     * 查询所有角色
     * @param vo
     * @return
     */
    @RequestMapping(value = "/roleManagerList",method = RequestMethod.POST)
    public R roleManagerList(RoleVo vo,HttpServletRequest request){
        //session
        R r = new R();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        Page page  =  mRoleService.roleManagerList(vo.getPage(),dtoR.getData().getmMerchantInfo().getId());
//        LinkedHashMap data = (LinkedHashMap) r.getData();
        r.setData(page);
        return r;
    }

    /**
     *
     * 跳转到添加角色页面
     * @return
     */
    @RequestMapping("/addRole")
    public ModelAndView addRole(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("ftl/role/roleAdd");
        return  mode;
    }

    /**
     * 添加角色
     * @param tbRole
     * @return
     */
    @RequestMapping(value = "/insertRole",method = RequestMethod.POST)
    public R insertRole(TbRole tbRole,HttpServletRequest request){
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid=dtoR.getData().getmMerchantInfo().getId();
        tbRole.setUid(mid);
        return mRoleService.insertRole(tbRole);
    }

    /**
     * 跳转修改页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/updateRole",method = RequestMethod.GET)
    public ModelAndView updateRole(String id){
        ModelAndView mode = new ModelAndView();
        mode.addObject("role",mRoleService.updateRole(id).getData());
        mode.setViewName("ftl/role/roleHandler");
        return mode;
    }

    /**
     * 修改角色
     * @param tbRole
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(TbRole tbRole){
        return mRoleService.updateById(tbRole);
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    public R deleteById(String id){
        return mRoleService.deleteById(id);
    }

    /**
     * 进入分配资源页面
     * @return
     */
    @RequestMapping(value = "/selectResources",method = RequestMethod.GET)
    public ModelAndView selectResources(HttpServletRequest request,String id){
        ModelAndView mode = new ModelAndView();
        R r = mRoleService.selectResources();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid=dtoR.getData().getmMerchantInfo().getId();
        Map<String,Object> map = (Map<String, Object>) r.getData();
        mode.addObject("list",map.get("list"));
        mode.addObject("pList",map.get("pList"));
        mode.addObject("id",id);
        mode.setViewName("ftl/role/selectResources");
        return mode;
    }

    /**
     * 查询该管理员的资源权限
     * @param id
     * @return
     */
    @RequestMapping(value = "/loadRoleResourcesStr",method = RequestMethod.POST)
    public R loadRoleResourcesStr(String id){
        return mRoleService.loadRoleResourcesStr(id);
    }

    /**
     * 分配权限
     * @param id
     * @param ids
     * @return
     */
    @RequestMapping(value = "/distribution",method = RequestMethod.POST)
    public R distribution(String id,String ids){
        return mRoleService.distribution(id,ids);
    }

}
