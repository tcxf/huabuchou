package com.tcxf.hbc.merchantpc.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.MTbResourcesService;
import com.tcxf.hbc.merchantpc.vo.ResourcesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/re")
public class TbResourcesController {
    @Autowired
    public MTbResourcesService mTbResourcesService;

    @RequestMapping(value = "/r",method = RequestMethod.GET)
    public ModelAndView r(){
        ModelAndView mode = new ModelAndView();
        mode.addObject("tList",mTbResourcesService.r().getData());
        mode.setViewName("ftl/resources/resourcesManager");
        return  mode;
    }

    /**
     * 根据条件查询资源
     * @return
     */
    @RequestMapping(value = "/findResources",method = RequestMethod.POST)
    public R findResources(ResourcesVo vo ){
        if("".equals(vo.getResourceName())){
            vo.setResourceName("null");
        }
        if("".equals(vo.getParentId())){
            vo.setParentId("null");
        }
        R r = new R();
        Page page = mTbResourcesService.findResources(vo.getPage(),vo.getResourceName(),vo.getParentId());
        r.setData(page);
        return r;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    public R deleteById(String id){
        return  mTbResourcesService.deleteById(id);
    }

    /**
     * 修改页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/updateResources",method = RequestMethod.GET)
    public ModelAndView updateResources(String id){
        ModelAndView mode = new ModelAndView();
        R r = mTbResourcesService.updateResources(id);
        Map<String,Object> map = (Map<String, Object>) r.getData();
        mode.addObject("resources",map.get("resources"));
        mode.addObject("tList",map.get("tList"));
        mode.setViewName("ftl/resources/resourcesUpdate");
        return mode;
    }

    /**
     * 修改资源信息
     * @param tbResources
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(TbResources tbResources){
        return  mTbResourcesService.updateById(tbResources);
    }

    /**
     * 添加页面
     * @return
     */
    @RequestMapping(value = "/AddResources",method = RequestMethod.GET)
    public ModelAndView AddResources(){
        ModelAndView model = new ModelAndView();
        R r = mTbResourcesService.AddResources();
        Map<String,Object> map = (Map<String, Object>) r.getData();
        model.addObject("tList",map.get("tList"));
        model.setViewName("ftl/resources/resourcesHandler");
        return model;
    }

    /**
     * 添加
     * @param tbResources
     * @return
     */
    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public R insert(TbResources tbResources){
        return  mTbResourcesService.insert(tbResources);
    }

}
