package com.tcxf.hbc.merchantpc.controller.bank;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.feign.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class BankController extends BaseController {
    @Autowired
    public BankService bankService;


    /**
     * 跳转钱包页面
     * @return
     */
    @RequestMapping("findbank")
    public ModelAndView findbank(){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/bindInfoManager");
        return m;
    }
    /**
     * 通过账户表ID查询所有绑卡信息
     * @return
     */
    @RequestMapping("findbankinfo")
    public R findbankinfo(HttpServletRequest request){
        R<Object> r = new R<>();
        Page id = bankService.findbank(getAID(request).toString(),"1");
        r.setData(id);
        return r;
    }

    /**
     * 跳转新绑卡页面
     * @return
     */
    @RequestMapping("newbank")
    public ModelAndView newbank(){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/bankCardADD1");
        return m;
    }

    @RequestMapping("newbankadd")
    public R newbankadd(HttpServletRequest request){
        R<Object> r = new R<>();
        try {
            request.setCharacterEncoding("UTF-8");
            String s = request.getParameter("bname");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        TbBindCardInfo info = new TbBindCardInfo();
        info.setOid(getAID(request).toString());
        info.setMobile(request.getParameter("bm"));
        info.setBankName(request.getParameter("bname"));
        info.setBankCard(request.getParameter("bcard"));
        info.setIdCard( request.getParameter("idCard"));
        info.setName(request.getParameter("buname"));
        info.setContractType(TbBindCardInfo.CONTRACTTYPE_1);
        r = bankService.savebank(info);
        return r;
    }
}
