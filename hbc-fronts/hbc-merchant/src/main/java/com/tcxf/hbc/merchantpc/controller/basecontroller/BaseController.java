package com.tcxf.hbc.merchantpc.controller.basecontroller;

import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class BaseController {

	@Resource(name = "redisTemplate")
	private RedisTemplate template;

	/**
	 * 设置缓存
	 * @param name
	 * @param val
	 * @return
	 */
	protected  boolean save(String cookieName,String name,Object val){
		if (!ValidateUtil.isEmpty(val)){
			//设置redis缓存
			template.opsForValue().set(name, val,30,TimeUnit.MINUTES);
			//添加cookie
			Cookie cookie = new Cookie(cookieName,name);
			cookie.setMaxAge(30 * 60);// 设置为30min
			cookie.setPath("/");
			getResponse().addCookie(cookie);
			return true;
		}
	 return false;
	}

	/**
	 * 删除缓存
	 * @param name
	 * @return
	 */
	protected  boolean del(HttpServletRequest request,String name){
		String cookieValue = getCookieValue(request,name);
		if (!ValidateUtil.isEmpty(name)){
			//删除redis缓存
			template.delete(cookieValue);
			return true;
		}
		return false;
	}

	/**
	 * 查看id缓存
	 * @param
	 * @return
	 */
	protected  Object getAID(HttpServletRequest request){
		//读取redis缓存
		String cookieValue = getCookieValue(request,"m_id");
		Object m_id = template.opsForValue().get(cookieValue);
		if (!ValidateUtil.isEmpty(m_id)){
			return m_id;
		}
		return null;

	}

	/**
	 * 查看session_authority_admin缓存
	 * @param
	 * @return
	 */
	protected  Object getadmin(HttpServletRequest request){
		//读取redis缓存
		String cookieValue = getCookieValue(request,"session_authority_admin");
		Object session_authority_admin = template.opsForValue().get(cookieValue);
		if (!ValidateUtil.isEmpty(session_authority_admin)){
			return session_authority_admin;
		}
		return null;
	}
	/**
	 * 获得respgeonse
	 * @return
	 */
	protected  HttpServletResponse getResponse(){
		ServletRequestAttributes  requestAttributes  = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(ValidateUtil.isEmpty(requestAttributes)){
			return null;
		}
		return requestAttributes.getResponse();
	}

	/**
	 * 获得request
	 * @return
	 */
	protected  HttpServletRequest getRequest(){
		ServletRequestAttributes  requestAttributes  = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(ValidateUtil.isEmpty(requestAttributes)){
			return null;
		}
		return requestAttributes.getRequest();
	}

	private String getCookieValue(HttpServletRequest request, String name){
		Map<String,Cookie> map = ReadCookieMap(request);
		Cookie cookieValue = map.get(name);
		if(ValidateUtil.isEmpty(cookieValue)){
			return null;
		}
		return cookieValue.getValue();
	}

	private Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
		Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
		Cookie[] cookies = request.getCookies();
		if(null!=cookies){
			for(Cookie cookie : cookies){
				cookieMap.put(cookie.getName(), cookie);
			}
		}
		return cookieMap;
	}
}
