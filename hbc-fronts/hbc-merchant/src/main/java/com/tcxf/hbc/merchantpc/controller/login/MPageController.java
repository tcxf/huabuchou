package com.tcxf.hbc.merchantpc.controller.login;

import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import com.tcxf.hbc.merchantpc.feign.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/page")
public class MPageController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Resource(name = "redisTemplate")
    private RedisTemplate template;

    @RequestMapping(value = "/index")
    public ModelAndView index(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("ftl/layout/index");
        Object id = getAID(request);
        R<UserInfoDto> userInfoDtoR = orderInfoService.userAccountInfo(id.toString());
        List<TbRole> list = (List<TbRole>) getadmin(request);
        R<Map<String , Object>> mapR = userService.index(userInfoDtoR.getData().getmMerchantInfo().getId() , list);
        Map<String , Object> map = mapR.getData();
        modelAndView.addObject("loginAdmin",map.get("loginAdmin"));
        modelAndView.addObject("functions",map.get("functions"));
        modelAndView.addObject("tbResources",map.get("tbResources"));
        modelAndView.addObject("menuResources",map.get("menuResources"));
        return modelAndView;
    }
}
