package com.tcxf.hbc.merchantpc.controller.login;



import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.LoginDto;
import com.tcxf.hbc.merchantpc.feign.UserService;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@RestController
@RequestMapping("/login")
public class UserLonginController extends BaseController {
    @Autowired
    public UserService userService;
    
    /**
     * 认证页面
     * @return ModelAndView
     */
    @GetMapping("/gologin")
    @AuthorizationNO
    public ModelAndView require() {
        return new ModelAndView("ftl/login");
    }

    /**
     * 登陆验证
     * @param request
     * @return
     */
    @RequestMapping("/logins")
    @AuthorizationNO
    public R login(HttpServletRequest request){
        R<Map<String,Object>> r = new R<>();
        LoginDto dto = new LoginDto();
        dto.setMobile(request.getParameter("loginName"));
        dto.setPwd(request.getParameter("pwd"));
        dto.setType(request.getParameter("type"));
        r = userService.loginPc(dto);
        Map<String,Object> map = r.getData();
        //如果登陆成功，就把账户表的ID保存在缓存里
        if (r.getCode()==0){
            R<List<TbRole>> listR = userService.getRole(map.get("mobile").toString() , "1");
            List<TbRole> list = listR.getData();
            //把数据存才缓存数据库
            save("session_authority_admin","session_authority_admin"+map.get("id").toString() , list);
            save("m_id","mid"+map.get("id").toString(),map.get("id").toString());
        }
        return r;
    }


    /**
     * 退出登陆
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        //清空session
        //request.getSession().removeAttribute("id");
        //request.getSession().removeAttribute("session_authority_admin");
        //清空缓存
        boolean b = del(request, "m_id");
        boolean b1 = del(request, "session_authority_admin");
        return new ModelAndView("ftl/login");
    }
}