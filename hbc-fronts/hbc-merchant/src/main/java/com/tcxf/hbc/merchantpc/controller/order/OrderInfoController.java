package com.tcxf.hbc.merchantpc.controller.order;


import com.baomidou.mybatisplus.plugins.Page;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;


@RestController
@RequestMapping("/order")
public class OrderInfoController extends BaseController {
    @Autowired
    public OrderInfoService orderInfoService;



    /**
     * 跳转交易记录页面
     * @return
     */
    @RequestMapping("findorder")
    public ModelAndView findorder(){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/morderInfoManager");
        return m;
    }

    @RequestMapping("findorderlist")
    public R findorderlist(HttpServletRequest request){
        R<Object> r = new R<>();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid = dtoR.getData().getmMerchantInfo().getId();
        HashMap<String, Object> map = new HashMap<>();
        map.put("page",  request.getParameter("page"));
        map.put("mid",mid);
        map.put("uname",request.getParameter("uname"));
        Page page = orderInfoService.findMbyuid(map);
        r.setData(page);
        return r;
    }

    /**
     * 跳转交易记录详情页面
     * @return
     */
    @RequestMapping("findorderbyone")
    public ModelAndView findorderbyone(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        R id = orderInfoService.findById(request.getParameter("id"));
        m.addObject("order",id.getData());
        m.setViewName("ftl/morderDeta");
        return m;
    }

    @RequestMapping(value = "/index",method = {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView index(HttpServletRequest request){
        ModelAndView model = new ModelAndView();
        R<UserInfoDto> dto  = orderInfoService.userAccountInfo(getAID(request).toString());
        R r= orderInfoService.findStati(dto.getData().getmMerchantInfo().getId());
        model.addObject("entity",r.getData());
        model.setViewName("ftl/layout/center");
        return  model;
    }

    @RequestMapping(value = "/findByTraind",method = RequestMethod.POST)
    public R findByTraind(HttpServletRequest request){
        R<UserInfoDto> dto  = orderInfoService.userAccountInfo(getAID(request).toString());
        return  orderInfoService.findByTraind(dto.getData().getmMerchantInfo().getId());
    }

    /**
     * 跳转结算记录页面
     * @return
     */
    @RequestMapping("findsettlement")
    public ModelAndView findsettlement(){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/settlementDetailManager");
        return m;
    }

    /**
     * 查询结算记录
     * @return
     */
    @RequestMapping("findsettlementlist")
    public R findsettlementlist(HttpServletRequest request){
        R<Object> r = new R<>();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        HashMap<String, Object> map = new HashMap<>();
        String mid = dtoR.getData().getmMerchantInfo().getId();
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String mname = request.getParameter("mname");
        String status = request.getParameter("status");
        map.put("page",  request.getParameter("page"));
        map.put("mid",mid);
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        map.put("mname",mname);
        map.put("status",status);
        Page page = orderInfoService.findSettlementMDeta(map);
        r.setData(page);
        return r;
    }

    /**
     * 跳转结算明细记录页面
     * @return
     */
    @RequestMapping("findsettlementlistmx")
    public ModelAndView findsettlementlistmx(HttpServletRequest request){
        String id = request.getParameter("id");
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/settlementDetailLog");
        m.addObject("id",id);
        return m;
    }

    /**
     * 查询结算记录交易明细
     * @return
     */
    @RequestMapping("findjymx")
    public R findjymx(HttpServletRequest request){
        R<Object> r = new R<>();
        String page = request.getParameter("page");
        String sid = request.getParameter("id");
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        String mid = dtoR.getData().getmMerchantInfo().getId();
        Page tradingDeta = orderInfoService.findTradingDeta(page, mid, "null", "null", sid);
        r.setData(tradingDeta);
        return r;
    }
}
