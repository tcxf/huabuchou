package com.tcxf.hbc.merchantpc.controller.redpacketsetting;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import com.tcxf.hbc.merchantpc.feign.RedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/red")
public class RedPacketSettingController extends BaseController {
    @Autowired
    public RedService redService;

    @Autowired
    public OrderInfoService orderInfoService;


    /**
     * 跳转添加优惠卷页面
     *
     * @return
     */
    @RequestMapping("gosavered")
    public ModelAndView gosavered(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        R r = orderInfoService.QueryMerchatInfo(dtoR.getData().getmMerchantInfo().getId());
        m.addObject("zt",r.getData());
        m.setViewName("ftl/redPacketSettingHandler");
        return m;
    }

    /**
     * 跳转修改优惠卷页面
     *
     * @return
     */
    @RequestMapping("goupred")
    public ModelAndView goupred(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R r = redService.couponUpdate(request.getParameter("id"));
        m.addObject("red", r.getData());
        m.setViewName("ftl/redPacketSettingUP");
        return m;
    }

    /**
     * 跳转优惠卷页面
     *
     * @return
     */
    @RequestMapping("gored")
    public ModelAndView gored() {
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/redPacketSettingManager");
        return m;
    }

    /**
     * 查询商户下面的优惠劵
     *
     * @param request
     * @return
     */
    @RequestMapping("findred")
    public R findred(HttpServletRequest request) {
        R<Object> r = new R<>();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("miid", dtoR.getData().getmMerchantInfo().getId());
        map.put("name", request.getParameter("name"));
        map.put("page",request.getParameter("page"));
        map.put("limit",request.getParameter("limit"));
        Page page = redService.couponList(map);
        r.setData(page);
        return r;

    }

    /**
     * 新增优惠劵
     *
     * @param request
     * @return
     */
    @RequestMapping("savered")
    public R savered(HttpServletRequest request) {
        R<Object> r = new R<>();
        MRedPacketSetting setting = new MRedPacketSetting();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(getAID(request).toString());
        setting.setMiId(dtoR.getData().getmMerchantInfo().getId());
        setting.setFullMoney(new BigDecimal(request.getParameter("fullMoney")));
        setting.setMoney(new BigDecimal(request.getParameter("money")));
        setting.setStatus("1");
        setting.setName(request.getParameter("name"));
        String d = request.getParameter("endDate");
       SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       Date endDate = null;
        try {
            endDate = dateFormat.parse(d);
        } catch (Exception e) {
           e.printStackTrace();
        }
        Date date = new Date();
        if(request.getParameter("timeType").equals("1")) {
            if (endDate.getTime() - date.getTime() < 0) {
                R re = new R();
                re.setMsg("您添加的优惠券已过期，请重新选择结束时间");
                re.setCode(1);
                return re;
            }
        }
        if (request.getParameter("timeType").equals("1")) {
            setting.setTimeType(Integer.parseInt(request.getParameter("timeType")));
            setting.setModifyDate(endDate);
        } else if (request.getParameter("timeType").equals("2")) {
            setting.setTimeType(Integer.parseInt(request.getParameter("timeType")));
            setting.setDays(Integer.parseInt(request.getParameter("days")));
        }
        r = redService.couponInsert(setting);
        return r;
    }

    /**
     * 修改优惠券
     *
     * @param request
     * @return
     */
    @RequestMapping("upred")
    public R upred(HttpServletRequest request) {
        R<Object> r = new R<>();
        MRedPacketSetting setting = new MRedPacketSetting();
        setting.setId(request.getParameter("id"));
        setting.setFullMoney(new BigDecimal(request.getParameter("fullMoney")));
        setting.setMoney(new BigDecimal(request.getParameter("money")));
        setting.setStatus("1");
        setting.setSubTitle(request.getParameter("title"));
        setting.setName(request.getParameter("name"));
        String d = request.getParameter("endDate");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date endDate = null;
        try {
            endDate = dateFormat.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Date date = new Date();
        if(request.getParameter("timeType").equals("1")) {
            if (endDate.getTime() - date.getTime() < 0) {
                R re = new R();
                re.setMsg("您添加的优惠券已过期，请重新选择结束时间");
                re.setCode(1);
                return re;
            }
        }
        if (request.getParameter("timeType").equals("1")) {
            setting.setTimeType(Integer.parseInt(request.getParameter("timeType")));
            setting.setModifyDate(endDate);
        } else if (request.getParameter("timeType").equals("2")) {
            setting.setTimeType(Integer.parseInt(request.getParameter("timeType")));
            setting.setDays(Integer.parseInt(request.getParameter("days")));
        }
        r = redService.couponUpdateById(setting);
        return r;
    }

    /**
     * 设置优惠券失效
     *
     * @param request
     * @return
     */
    @RequestMapping("redcouponFailure")
    public R redcouponFailure(HttpServletRequest request) {
        R<Object> r = new R<>();
        r = redService.couponFailure(request.getParameter("id"));
        return r;
    }
}
