package com.tcxf.hbc.merchantpc.controller.vip;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import com.tcxf.hbc.merchantpc.feign.VipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;

@RestController
@RequestMapping("/vip")
public class VipController extends BaseController {
    @Autowired
    public VipService vipService;
    @Autowired
    public OrderInfoService orderInfoService;


    /**
     * 跳转会员页面
     * @param request
     * @return
     */
    @RequestMapping("findvip")
    public ModelAndView findvip(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/VipManager");
        return m;
    }

    /**
     * 查看会员信息
     * @param request
     * @return
     */
    @RequestMapping("viplist")
    public R viplist(HttpServletRequest request){
        R<Object> r = new R<>();
        String id = getAID(request).toString();
        R<UserInfoDto> dtoR = orderInfoService.userAccountInfo(id);
        String name = request.getParameter("name");
        if(name==null || name .equals("")){
            name ="null";
        }
        Page page = vipService.QueryVip(dtoR.getData().getmMerchantInfo().getId(), name);
        r.setData(page);
        return r;
    }
}
