package com.tcxf.hbc.merchantpc.controller.wallet;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.controller.basecontroller.BaseController;
import com.tcxf.hbc.merchantpc.feign.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mwallet")
public class WalletController extends BaseController {
    @Autowired
    public WalletService walletService;


    /**
     * 跳转钱包页面
     *
     * @return
     */
    @RequestMapping("findwallet")
    public ModelAndView findwallet(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R<TbWallet> findwallet = walletService.findwallet(getAID(request).toString());
        TbWallet tbWallet=  findwallet.getData();
        tbWallet.setTotalAmount(new Calculate(tbWallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        m.addObject("w",tbWallet);
        m.setViewName("ftl/walletlist");
        return m;
    }

    /**
     * 查询收入支出记录
     *
     * @param request
     * @return
     */
    @RequestMapping("findWalletIncome")
    public R findWalletIncome(HttpServletRequest request) {
        R<Object> r = new R<>();
        Page walletIncome = walletService.findWalletIncome(request.getParameter("type"), request.getParameter("wid"), request.getParameter("page"), request.getParameter("limit"));
        r.setData(walletIncome);
        return r;
    }

    /**
     * 跳转提现记录页面
     *
     * @param request
     * @return
     */
    @RequestMapping("findWalletwithdrawals")
    public ModelAndView findWalletwithdrawals(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        m.setViewName("ftl/wallettxjl");
        return m;
    }

    /**
     * 查询提现记录
     *
     * @param request
     * @return
     */
    @RequestMapping("findWalletwithdrawalslist")
    public R findWalletwithdrawalslist(HttpServletRequest request) {
        Page m_id = walletService.findWalletwithdrawalsList(getAID(request).toString());
        R<Object> objectR = new R<>();
        objectR.setData(m_id);
        return objectR;
    }

    /**
     * 跳转提申请页面
     *
     * @param request
     * @return
     */
    @RequestMapping("findWallettx")
    public ModelAndView findWallettx(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R id = walletService.findBindCardinfo(getAID(request).toString(),TbBindCardInfo.CONTRACTTYPE_1);
        R<TbWallet> findwallet = walletService.findwallet(getAID(request).toString());
        TbWallet tbWallet=  findwallet.getData();
        tbWallet.setTotalAmount(new Calculate(tbWallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        HashMap<String, Object> map = new HashMap<>();
        map.put("utype", 2);
        R bYmerchant = walletService.findBYmerchant(map);
        List<TbWalletCashrate> list = (List<TbWalletCashrate>) bYmerchant.getData();
        m.addObject("c",JsonTools.toJson(list));
        m.addObject("b", id.getData());
        m.addObject("w",tbWallet);
        m.setViewName("ftl/wallettx");
        return m;
    }

    /**
     * 跳转到发短信的页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/findtxdx")
    public ModelAndView findtxdx(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        R bind = walletService.findBind(request.getParameter("id"));
        m.addObject("sjtxje", request.getParameter("sjtxje"));
        m.addObject("txsxje", request.getParameter("txsxje"));
        m.addObject("txje", request.getParameter("txje"));
        m.addObject("b", bind.getData());
        m.setViewName("ftl/wallettxwc");
        return m;
    }

    /**
     * 发送验证码
     *
     * @param request
     * @return
     */
    @RequestMapping("/getsys")
    public R getsys(HttpServletRequest request) {
        R<Object> r = new R<>();
        r = walletService.getsys(request.getParameter("mobile"), request.getParameter("type"));
        return r;
    }

    /**
     * 钱包提现
     *
     * @param request
     * @return
     */
    @RequestMapping("/walletdoReg")
    public R walletdoReg(HttpServletRequest request) {
        R<Object> r = new R<>();
        Object id = getAID(request);
        String type = request.getParameter("type");
        String yCode = request.getParameter("yCode");
        String bid = request.getParameter("bid");
        String txje = request.getParameter("txje");
        String sjtxje = request.getParameter("sjtxje");
        String txsxje = request.getParameter("txsxje");
        System.out.println(new BigDecimal(txsxje));
        Map<String, Object> map = new HashMap<>();
        map.put("uid", id);
        map.put("type", type);
        map.put("yCode", yCode);
        map.put("bid", bid);
        map.put("txje", txje);
        map.put("sjtxje", sjtxje);
        map.put("txsxje", txsxje);
        r = walletService.walletdoReg(map);
        return r;
    }
}
