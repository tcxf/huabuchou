package com.tcxf.hbc.merchantpc.dto;

public class LoginDto {
    private String mobile;//手机号
    private String type;//判断是用户登陆还是商户登陆
    private String pwd;//密码
    private String code;//短信验证码

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
