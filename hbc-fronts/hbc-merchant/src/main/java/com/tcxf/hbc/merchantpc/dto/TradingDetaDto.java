package com.tcxf.hbc.merchantpc.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class TradingDetaDto extends TbTradingDetail {
    private String simpleName;

    private String localPhoto;
    private String cmobile;
    private String mobile;
    private String mobileStr;

    private String localPhotoStr;

    private BigDecimal discountAmountStr;

    public String getCmobile() {
        return cmobile;
    }

    public void setCmobile(String cmobile) {
        this.cmobile = cmobile;
    }

    public BigDecimal getDiscountAmountStr() {
        return discountAmountStr;
    }

    public void setDiscountAmountStr(BigDecimal discountAmountStr) {
        this.discountAmountStr = discountAmountStr;
    }

    public String getLocalPhotoStr() {
        return localPhotoStr;
    }

    public void setLocalPhotoStr(String localPhotoStr) {
        this.localPhotoStr = localPhotoStr;
    }

    public String getMobileStr() {
        return mobileStr;
    }

    public void setMobileStr(String mobileStr) {
        this.mobileStr = mobileStr;
    }

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @TableField("loop_start")
    private Date loopStart;//结算单周期开始时间

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @TableField("loop_end")
    private Date loopEnd;//结算单周期结束时间

    private String status;//结算状态

    private Long amount;//结算金额

    public Date getLoopStart() {
        return loopStart;
    }

    public void setLoopStart(Date loopStart) {
        this.loopStart = loopStart;
    }

    public Date getLoopEnd() {
        return loopEnd;
    }

    public void setLoopEnd(Date loopEnd) {
        this.loopEnd = loopEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date payDate;

    private String paymentType;//支付类型

    private String paymentSn;//支付编号

    private String realName;//用户名称

    public String getSettStatus() {
        return settStatus;
    }

    public void setSettStatus(String settStatus) {
        this.settStatus = settStatus;
    }

    private String settStatus;

    private String headImg;//用户头像

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    private BigDecimal discountFee;
}
