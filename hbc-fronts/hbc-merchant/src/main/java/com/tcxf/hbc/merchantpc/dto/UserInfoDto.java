package com.tcxf.hbc.merchantpc.dto;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;

import java.io.Serializable;

public class UserInfoDto implements Serializable {
    private TbUserAccount tbUserAccount;
    private TbUserAccountTypeCon tbUserAccountTypeCon;
    private CUserInfo cUserInfo;
    private MMerchantInfo mMerchantInfo;

    public TbUserAccount getTbUserAccount() {
        return tbUserAccount;
    }

    public void setTbUserAccount(TbUserAccount tbUserAccount) {
        this.tbUserAccount = tbUserAccount;
    }

    public TbUserAccountTypeCon getTbUserAccountTypeCon() {
        return tbUserAccountTypeCon;
    }

    public void setTbUserAccountTypeCon(TbUserAccountTypeCon tbUserAccountTypeCon) {
        this.tbUserAccountTypeCon = tbUserAccountTypeCon;
    }

    public CUserInfo getcUserInfo() {
        return cUserInfo;
    }

    public void setcUserInfo(CUserInfo cUserInfo) {
        this.cUserInfo = cUserInfo;
    }

    public MMerchantInfo getmMerchantInfo() {
        return mMerchantInfo;
    }

    public void setmMerchantInfo(MMerchantInfo mMerchantInfo) {
        this.mMerchantInfo = mMerchantInfo;
    }
}
