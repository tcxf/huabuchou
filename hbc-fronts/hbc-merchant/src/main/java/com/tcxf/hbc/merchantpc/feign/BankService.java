package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.BankServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
@FeignClient(name = "hbc-mc-service", fallback = BankServiceFallbackImpl.class)

public interface BankService {
    /**
     * 通过账户表ID查询所有绑卡信息
     * @param oid
     * @return
     */
    @RequestMapping(value = "/bank/findMbank/{oid}/{type}" , method = RequestMethod.POST)
    Page findbank(@PathVariable("oid") String oid,@PathVariable("type") String type);

    /**
     * 新增绑卡
     * @return
     */
    @RequestMapping(value = "/bank/savebank" , method = RequestMethod.POST)
    R savebank(@RequestBody TbBindCardInfo tbBindCardInfo);
}
