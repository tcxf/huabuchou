package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.MRoleServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service",fallback = MRoleServiceFallbackImpl.class)
public interface MRoleService {

    @RequestMapping(value = "/mRole/roleManagerList/{page}/{aid}",method = RequestMethod.POST)
    public Page roleManagerList(@PathVariable("page") String page, @PathVariable("aid") String aid);

    @RequestMapping(value = "/mRole/insertRole",method = RequestMethod.POST)
    public R insertRole(@RequestBody TbRole tbRole);

    @RequestMapping(value = "/mRole/updateRole/{id}",method = RequestMethod.POST)
    public R updateRole(@PathVariable("id") String id);

    @RequestMapping(value = "/mRole/updateById",method = RequestMethod.POST)
    public R updateById(@RequestBody TbRole tbRole);

    @RequestMapping(value = "/mRole/deleteById/{id}",method = RequestMethod.POST)
    public R deleteById(@PathVariable("id") String id);

    @RequestMapping(value = "/mRole/selectResources",method = RequestMethod.POST)
    public R selectResources();

    @RequestMapping(value = "/mRole/loadRoleResourcesStr/{id}",method = RequestMethod.POST)
    public R loadRoleResourcesStr(@PathVariable("id") String id);

    @RequestMapping(value = "/mRole/distribution/{id}/{ids}",method = RequestMethod.POST)
    public R distribution(@PathVariable("id") String id,@PathVariable("ids") String ids);


}
