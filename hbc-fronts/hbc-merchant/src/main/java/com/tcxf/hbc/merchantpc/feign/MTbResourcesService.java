package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.MTbResoucesServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service",fallback = MTbResoucesServiceFallbackImpl.class)
public interface MTbResourcesService {

    @RequestMapping(value = "/mRe/r",method = RequestMethod.POST)
    public R r();

    @RequestMapping(value = "/mRe/findResources/{page}/{name}/{pid}",method = RequestMethod.POST)
    public Page findResources(@PathVariable("page") String page, @PathVariable("name") String name, @PathVariable("pid") String pid);

    @RequestMapping(value = "/mRe/deleteById/{id}",method = RequestMethod.POST)
    public R deleteById(@PathVariable("id") String id);

    @RequestMapping(value = "/mRe/updateResources/{id}",method = RequestMethod.POST)
    public R updateResources(@PathVariable("id") String id);

    @RequestMapping(value = "/mRe/updateById",method = RequestMethod.POST)
    public R updateById(@RequestBody TbResources tbResources);

    @RequestMapping(value = "/mRe/AddResources",method = RequestMethod.POST)
    public R AddResources();

    @RequestMapping(value = "/mRe/insert",method = RequestMethod.POST)
    public R insert(@RequestBody TbResources tbResources);

}
