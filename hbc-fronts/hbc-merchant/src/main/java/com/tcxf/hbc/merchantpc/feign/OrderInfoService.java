package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.fallback.OrderInfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "hbc-mc-service", fallback = OrderInfoServiceFallbackImpl.class)
public interface OrderInfoService {



    /**
     * 获取TbUserAccount内容
     * @param id
     * @return
     */
    @PostMapping("/clogin/getUserAccountInfo/{id}")
    R<UserInfoDto> userAccountInfo(@PathVariable("id")String id );
    /**
     *商户订单查询
     *
     */
    @RequestMapping(value = "/uorder/findMByUid",method = RequestMethod.POST)
    Page findMbyuid(@RequestBody Map<String, Object> map);
    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/uorder/findById/{id}",method = RequestMethod.POST)
    R findById(@PathVariable("id") String id);

    @RequestMapping(value = "/uorder/findByTraind/{mid}",method = RequestMethod.POST)
     R findByTraind(@PathVariable("mid")String mid);

    @RequestMapping(value = "/uorder/findStati/{mid}",method = RequestMethod.POST)
     R findStati(@PathVariable("mid")String mid);

    /**
     * 结算订单
     * @param page
     * @param mid
     * @return
     */
    @RequestMapping(value = "/uorder/findSettlementMDeta",method = RequestMethod.POST)
    Page findSettlementMDeta (@RequestBody Map<String, Object> map);

    /**
     * 交易详情
     * @return
     */
    @RequestMapping(value = "/uorder/findTradingDeta/{page}/{mid}/{createDate}/{modifyDate}/{sid}",method = RequestMethod.POST)
     Page findTradingDeta (@PathVariable("page") String page,@PathVariable("mid") String mid,
                               @PathVariable("createDate") String createDate,@PathVariable("modifyDate")
                                       String modifyDate,@PathVariable("sid")String sid);

    /**
     * 查询商户信息
     * @return
     */
    @RequestMapping(value = "/merchat/QueryMerchatInfo/{id}",method = RequestMethod.POST)
    R QueryMerchatInfo (@PathVariable("id") String id);
}
