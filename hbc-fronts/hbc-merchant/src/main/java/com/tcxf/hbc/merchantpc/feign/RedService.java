package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.RedServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "hbc-mc-service", fallback = RedServiceFallbackImpl.class)
public interface RedService {

    /**
     *查询商户下的优惠券
     * @return
     */
    @RequestMapping(value="/coupon/findMlist",method ={RequestMethod.POST} )
    Page couponList(@RequestBody Map<String, Object> params);

    /**
     * 创建优惠券
     * @param mRedPacketSetting
     * @return
     */
    @RequestMapping(value="/coupon/couponInsert",method ={RequestMethod.POST} )
     R couponInsert(@RequestBody MRedPacketSetting mRedPacketSetting);


    /**
     * 编辑优惠券页面
     * @param id
     * @return
     */
    @RequestMapping(value="/coupon/couponUpdate/{id}",method ={RequestMethod.POST} )
     R couponUpdate(@PathVariable("id") String id);


    /**
     *
     * 修改优惠券
     * @param mRedPacketSetting
     * @return
     */

    @RequestMapping(value="/coupon/couponUpdateById",method ={RequestMethod.POST} )
    R couponUpdateById(@RequestBody MRedPacketSetting mRedPacketSetting);

    /**
     * 使优惠券失效
     * @param id
     * @return
     */
    @RequestMapping(value="/coupon/couponFailure/{id}",method ={RequestMethod.POST} )
      R couponFailure(@PathVariable("id") String id);
}
