package com.tcxf.hbc.merchantpc.feign;

import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.dto.LoginDto;
import com.tcxf.hbc.merchantpc.feign.fallback.UserServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@FeignClient(name = "hbc-mc-service", fallback = UserServiceFallbackImpl.class)
public interface UserService {
    /**
     * 登陆
     *
     * @param loginDto
     * @return
     */
    @PostMapping("/clogin/login")
    R login(@RequestBody LoginDto loginDto);

    @PostMapping("/clogin/loginPc")
    R loginPc(@RequestBody LoginDto loginDto);

    /**
     * 获取角色
     * @param mobile
     * @param type
     * @return
     */
    @PostMapping("/mPage/getRole/{mobile}/{type}")
    R<List<TbRole>> getRole(@PathVariable("mobile") String mobile , @PathVariable("type")String type);

    /**
     * 获取资源
     * @param params
     * @return
     */
    @RequestMapping(value = "/mPage/index/{mid}",method = {RequestMethod.POST})
    R<Map<String , Object>> index(@PathVariable("mid") String mid,@RequestBody List<TbRole> params);
}