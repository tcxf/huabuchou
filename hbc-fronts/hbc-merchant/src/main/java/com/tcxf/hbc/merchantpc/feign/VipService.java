package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.VipServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service", fallback = VipServiceFallbackImpl.class)
public interface VipService {

    /**
     * 查询会员用户
     * @param mid
     * @param realName
     * @return
     */
    @RequestMapping(value = "/Vip/QueryVip/{mid}/{realName}",method = RequestMethod.POST)
    Page QueryVip(@PathVariable("mid")String mid, @PathVariable("realName")String realName);
}
