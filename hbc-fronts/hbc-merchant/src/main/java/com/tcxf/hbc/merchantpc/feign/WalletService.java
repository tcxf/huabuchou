package com.tcxf.hbc.merchantpc.feign;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.fallback.WalletServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@FeignClient(name = "hbc-mc-service", fallback = WalletServiceFallbackImpl.class)
public interface WalletService {
    /**
     * 查询钱包
     * @param oid
     * @return
     */
    @RequestMapping(value = "/wallet/wallets/{oid}" , method = RequestMethod.POST)
     R<TbWallet> findwallet(@PathVariable("oid") String oid) ;

    /**
     * 查询钱包的收入支出记录
     * @param utype
     * @param wid
     * @return
     */
    @RequestMapping(value = "/wallet/findMWalletIncome/{utype}/{wid}/{page}/{limit}", method = RequestMethod.POST)
    Page findWalletIncome(@PathVariable("utype") String utype,@PathVariable("wid") String wid,@PathVariable("page") String page,@PathVariable("limit") String limit);

    /**
     * 查询钱包提现记录
     * @param uid
     * @return
     */
    @RequestMapping(value = "/wallet/findMWalletwithdrawalsList/{uid}", method = RequestMethod.POST)
    Page findWalletwithdrawalsList(@PathVariable("uid") String uid);

    /**
     * 查询用户所有绑卡信息
     * @param oid
     * @return
     */
    @RequestMapping(value="wallet/findUBindCardinfo/{oid}/{type}",method ={RequestMethod.POST})
    R findBindCardinfo(@PathVariable("oid") String oid,@PathVariable("type") String type);

    /**
     * 查询用户选择提现卡的信息
     * @param id
     * @return
     */
    @RequestMapping(value ="/wallet/findBind/{id}", method = RequestMethod.POST)
    R findBind(@PathVariable("id") String id);

    /**
     * 获取短信验证码
     *
     * @return
     */
    @RequestMapping("/wallet/getsys/{mobile}/{type}")
    R getsys(@PathVariable("mobile") String mobile,@PathVariable("type") String type);

    /**
     * 钱包提现申请
     * @param
     */
    @RequestMapping(value="/wallet/walletdoReg", method = RequestMethod.POST)
     R walletdoReg(@RequestBody Map<String, Object> map);

    /**
     * 查询商户提现费率
     * @param map
     * @return
     */
    @RequestMapping(value="/wallet/findBYmerchant", method = RequestMethod.POST)
     R findBYmerchant(@RequestBody Map<String, Object> map);
    }
