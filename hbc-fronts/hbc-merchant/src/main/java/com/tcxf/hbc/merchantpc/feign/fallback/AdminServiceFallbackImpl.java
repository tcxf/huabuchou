package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceFallbackImpl implements AdminService {
    @Override
    public Page findAdminList(String mid, String page, String name) {
        return null;
    }

    @Override
    public R insertAdmin(PAdmin admin) {
        return null;
    }

    @Override
    public R updateAdmin(String id) {
        return null;
    }

    @Override
    public R updateById(PAdmin admin) {
        return null;
    }

    @Override
    public R deleteById(String id) {
        return null;
    }

    @Override
    public R selectRole(String aid) {
        return null;
    }

    @Override
    public R selectAdminRoleStr(String id) {
        return null;
    }

    @Override
    public R distribution(String id, String ids) {
        return null;
    }
}
