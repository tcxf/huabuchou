package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.BankService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankServiceFallbackImpl implements BankService {
    @Override
    public Page findbank(String oid, String type) {
        return null;
    }

    @Override
    public R savebank(TbBindCardInfo tbBindCardInfo) {
        return null;
    }


}
