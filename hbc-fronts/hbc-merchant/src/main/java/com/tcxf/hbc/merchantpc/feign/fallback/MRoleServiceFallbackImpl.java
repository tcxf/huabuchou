package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.MRoleService;
import org.springframework.stereotype.Service;

@Service
public class MRoleServiceFallbackImpl implements MRoleService {
    @Override
    public Page roleManagerList(String page, String aid) {
        return null;
    }

    @Override
    public R insertRole(TbRole tbRole) {
        return null;
    }

    @Override
    public R updateRole(String id) {
        return null;
    }

    @Override
    public R updateById(TbRole tbRole) {
        return null;
    }

    @Override
    public R deleteById(String id) {
        return null;
    }

    @Override
    public R selectResources() {
        return null;
    }

    @Override
    public R loadRoleResourcesStr(String id) {
        return null;
    }

    @Override
    public R distribution(String id, String ids) {
        return null;
    }
}
