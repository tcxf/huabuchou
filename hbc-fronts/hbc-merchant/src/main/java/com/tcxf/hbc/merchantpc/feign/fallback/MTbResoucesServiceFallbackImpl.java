package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.MTbResourcesService;
import org.springframework.stereotype.Service;

@Service
public class MTbResoucesServiceFallbackImpl implements MTbResourcesService {
    @Override
    public R r() {
        return null;
    }

    @Override
    public Page findResources(String page, String name, String pid) {
        return null;
    }

    @Override
    public R deleteById(String id) {
        return null;
    }

    @Override
    public R updateResources(String id) {
        return null;
    }

    @Override
    public R updateById(TbResources tbResources) {
        return null;
    }

    @Override
    public R AddResources() {
        return null;
    }

    @Override
    public R insert(TbResources tbResources) {
        return null;
    }
}
