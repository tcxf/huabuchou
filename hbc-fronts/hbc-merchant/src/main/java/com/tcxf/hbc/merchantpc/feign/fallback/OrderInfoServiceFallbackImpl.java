package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.dto.UserInfoDto;
import com.tcxf.hbc.merchantpc.feign.OrderInfoService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class OrderInfoServiceFallbackImpl implements OrderInfoService {
    @Override
    public R<UserInfoDto> userAccountInfo(String id) {
        return null;
    }

    @Override
    public Page findMbyuid(Map<String, Object> map) {
        return null;
    }

    @Override
    public R findById(String id) {
        return null;
    }

    @Override
    public R findByTraind(String mid) {
        return null;
    }

    @Override
    public R findStati(String mid) {
        return null;
    }

    @Override
    public Page findSettlementMDeta(Map<String, Object> map) {
        return null;
    }


    @Override
    public Page findTradingDeta(String page, String mid, String createDate, String modifyDate, String sid) {
        return null;
    }

    @Override
    public R QueryMerchatInfo(String id) {
        return null;
    }
}
