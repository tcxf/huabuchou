package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.RedService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RedServiceFallbackImpl implements RedService {


    @Override
    public Page couponList(Map<String, Object> params) {
        return null;
    }

    @Override
    public R couponInsert(MRedPacketSetting mRedPacketSetting) {
        return null;
    }

    @Override
    public R couponUpdate(String id) {
        return null;
    }

    @Override
    public R couponUpdateById(MRedPacketSetting mRedPacketSetting) {
        return null;
    }

    @Override
    public R couponFailure(String id) {
        return null;
    }
}
