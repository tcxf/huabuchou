package com.tcxf.hbc.merchantpc.feign.fallback;

import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.dto.LoginDto;
import com.tcxf.hbc.merchantpc.feign.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author zhouyj
 * @date 2018/3/10
 * 用户服务的fallback
 */
@Service
public class UserServiceFallbackImpl implements UserService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public R login(LoginDto loginDto) {
        return null;
    }

    @Override
    public R loginPc(LoginDto loginDto) {
        return null;
    }

    @Override
    public R<List<TbRole>> getRole(String id, String type) {
        return null;
    }

    @Override
    public R<Map<String, Object>> index(String mid, List<TbRole> params) {
        return null;
    }


}
