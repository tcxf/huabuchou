package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.VipService;
import org.springframework.stereotype.Service;

@Service
public class VipServiceFallbackImpl implements VipService {
    @Override
    public Page QueryVip(String mid, String realName) {
        return null;
    }
}
