package com.tcxf.hbc.merchantpc.feign.fallback;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.merchantpc.feign.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class WalletServiceFallbackImpl implements WalletService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R<TbWallet> findwallet(String oid) {
        return null;
    }

    @Override
    public Page findWalletIncome(String utype, String wid, String page, String limit) {
        return null;
    }

    @Override
    public Page findWalletwithdrawalsList(String uid) {
        return null;
    }

    @Override
    public R findBindCardinfo(String oid, String type) {
        return null;
    }


    @Override
    public R findBind(String id) {
        return null;
    }

    @Override
    public R getsys(String mobile, String type) {
        return null;
    }

    @Override
    public R walletdoReg(Map<String, Object> map) {
        return null;
    }

    @Override
    public R findBYmerchant(Map<String, Object> map) {
        return null;
    }


}
