package com.tcxf.hbc.merchantpc.vo;

import com.tcxf.hbc.common.entity.TbResources;

public class ResourcesVo extends TbResources {
    private  String page;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
