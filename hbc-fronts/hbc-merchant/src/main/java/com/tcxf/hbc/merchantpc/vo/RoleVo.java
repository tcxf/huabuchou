package com.tcxf.hbc.merchantpc.vo;

import com.tcxf.hbc.common.entity.TbRole;

public class RoleVo extends TbRole {
    private String page;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
