var res_uri = "http://res.zoomyblion.com/";
var context="/zybl-manager";
$(function(){
	$.ajax({
		url:context+"/merchantInfo/improveQuotaInfo",
	    type:'POST', //GET
	    data:{"mid": $("#mid").val()},
	    timeout:30000,    //超时时间
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(d){
			if(d.resultCode == "1"){
				//多头借贷接口信息
				var borrowingMultiJson = d.data.borrowingMultiJson;
				//个人投资任职接口信息
				var investmentTenureJson = d.data.investmentTenureJson;
				//银行卡接口信息
				var bankCardJson = d.data.bankCardJson;
				
				//多头借贷接口解析--开始
				if(borrowingMultiJson != "" && borrowingMultiJson != null){
					borrowingMultiJson = JSON.parse(d.data.borrowingMultiJson)
					
					var list = borrowingMultiJson.data.list;
					
					if(list != null && list != ""){
                		var sb_tr = "";
                		$.each(list, function (n, value) {
							sb_tr += "<tr><td>"+value.blackRiskType+"</td>";
							sb_tr += "<td>"+value.blackFactsType+"</td>";
							sb_tr += "<td style='width:50%;'>"+value.blackFacts+"</td>";
							sb_tr += "<td>"+value.blackAmt+"</td>";
							sb_tr += "<td>"+value.blackHappenDate+"</td></tr>";
			           	});
                		$("#sb_tbd").append(sb_tr);
					}
				}
				
				//个人投资任职接口解析--开始
				if(investmentTenureJson != "" && investmentTenureJson != null){
					investmentTenureJson = JSON.parse(d.data.investmentTenureJson)
					if(investmentTenureJson.code == "0000"){
						var corpLegalPersons = investmentTenureJson.data.corpLegalPersons;
						var dishones = investmentTenureJson.data.dishones;
						var enforeements = investmentTenureJson.data.enforeements;
						var penaltyRecords = investmentTenureJson.data.penaltyRecords;
						//企业法人信息
						if(corpLegalPersons != null && corpLegalPersons != ""){
							$.each(corpLegalPersons, function (n, value) {
								$("#gs_frxm").text(value.name);
								$("#gs_qymc").text(value.entName);
								$("#gs_zch").text(value.regNo);
								$("#gs_zczb").text(value.regCap);
								$("#gs_qyzt").text(value.entStatus);
				           	});
						}
						//被执行人信息
						if(enforeements != null && enforeements != ""){
							$.each(enforeements, function (n, value) {
								$("#gs_bzxrxm").text(value.name);
								$("#gs_bzxrsfz").text(value.identityNo);
								$("#gs_bzxrajzt").text(value.caseStatus);
				           	});
						}
						//失信被执行人信息
						if(dishones != null && dishones != ""){
							$.each(dishones, function (n, value) {
								$("#gs_sxmc").text(value.name);
								$("#gs_sxsfz").text(value.identityNo);
								$("#gs_sxfr").text(value.legalPerson);
								$("#gs_sxqx").text(value.specificCase);
				           	});
						}
						//行政处罚历史信息
						if(penaltyRecords != null && penaltyRecords != ""){
							$.each(penaltyRecords, function (n, value) {
								$("#gs_xzcfdsr").text(value.name);
								$("#gs_xzcfzjh").text(value.identityNo);
								$("#gs_xzcfwfss").text(value.illegalFact);
				           	});
						}
					}
				}
				
				//银行卡接口解析--开始
				if(bankCardJson != "" && bankCardJson != null){
					bankCardJson = JSON.parse(d.data.bankCardJson)
					
					var information = bankCardJson.result.information;
					//设置基本信息
					$("#yh_yhkh").text(bankCardJson.result.bankcard);
					$("#yh_yhjc").text(information.abbreviation);
					$("#yh_logo").attr("src", information.bankimage);
					$("#yh_yhqc").text(information.bankname);
					$("#yh_yhwz").text(information.bankurl);
					$("#yh_klx").text(information.cardname);
					$("#yh_yhkzl").text(information.cardtype);
					$("#yh_ywqc").text(information.enbankname);
				}
			}
	    }
	});
});
