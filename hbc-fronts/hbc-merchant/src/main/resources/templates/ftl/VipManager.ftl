
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 项目</title>
<meta name="keywords" content="">
<meta name="description" content="">

<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>VIP会员</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn"
											class="btn btn-primary btn-sm">
											<i class="fa fa-refresh"></i> 刷新
										</button>
									</div>
									<div class="col-sm-8"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">会员名称</label>
								<input type="text" name="name" placeholder="请输入会员名称"
									id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning"
								value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script type="text/javascript">
		$(document).ready(function() {
    $("#parentId").select();
    var $pager = $("#data").pager({
        url: "${basePath!}/vip/viplist",
        formId: "searchForm",
        pagerPages: 3,
        template: {
            header: 
            	'<div class="fixed-table-container form-group">' + 
            		'<table class="table table-hover">' + 
            			'<thead>' + 
            				'<tr>' + 
            					'<th>会员姓名</th>'+
            					'<th>会员手机</th>' +
                                 '<th>注册时间</th>' +
                                '<th>授信状态</th>' +
            				'</tr>' +
            			'</thead><tbody>',
            body: 
            	'<tr>' + 
            		'<td>{realName}</td>'+
            		'<td>{mobile}</td>' +
                    '<td>{createDate}</td>' +
                    '<td id="status_{id}"></td>' +
            	 '</tr>',
            footer: '</tbody></table>',
            noData: '<tr><td colspan="6"><center>暂无数据</center></tr>'
        },
        callbak: function(result) {
            var list = result.data.records;
            for (var i = 0; i < list.length; i++) {
                if(list[i].authStatus == 0){
                    $('#status_' + list[i].id).html("未授信");
				}else if (list[i].authStatus == 1){
                    $('#status_' + list[i].id).html("已授信");
				}
            }


            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
            });
        }
    });
    $("#loading-example-btn").click(function() {
        $pager.gotoPage($pager.pageNumber);
    });
});
	</script>

</body>
</html>
