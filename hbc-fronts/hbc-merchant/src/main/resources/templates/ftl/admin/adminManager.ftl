<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>管理员管理</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建管理员</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">名称</label>
								<input type="text" placeholder="请输入管理员名称" id="adminName" name="adminName" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list" style="overflow: auto" id="data">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<style>
	/*定义滚动条高宽及背景 高宽分别对应横竖滚动条的尺寸*/  
::-webkit-scrollbar  
{  
    width: 5px;  
    height: 5px;  
    background-color: #F5F5F5;  
}  
  
/*定义滚动条轨道 内阴影+圆角*/  
::-webkit-scrollbar-track  
{  
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);  
    border-radius: 2px;  
    background-color: #F5F5F5;  
}  
  
/*定义滑块 内阴影+圆角*/  
::-webkit-scrollbar-thumb  
{  
    border-radius: 2px;  
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);  
    background-color: #555;  
}  
	</style>
	<script type="text/javascript">
	$(document).ready(function(){
	  var $pager = $("#data").pager({
			url:"${basePath!}/admin/findAdminList",
            type:"POST",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>名称</th>'+
							'<th>用户名</th>'+
							'<th>邮箱</th>'+
							'<th>状态</th>'+
                			'<th>管理员类型</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{adminName}</td>'+
							'<td>{userName}</td>'+
							'<td>{regEmail}</td>'+
							'<td id="state_{id}"></td>'+
                			'<td id="type_{id}"></td>'+
							'<td >'+
							'<a href="javascript:void(0);" id="bj_{id}" data="{id},{userName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
							'<a href="javascript:void(0);" id="sc_{id}" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> 删除 </a> '+
							'<a href="javascript:void(0);" id="fp_{id}" data-id="{id}" class="btn btn-success btn-sm distribution"><i class="fa fa-plus"></i> 分配角色 </a>'+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="7"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].status==1)
						$("#state_"+list[i].id).html("启用");
					else
						$("#state_"+list[i].id).html("停用");
					if(list[i].type==1){
						$("#type_"+list[i].id).html("默认管理员");
                        $("#bj_"+list[i].id).hide();
                        $("#sc_"+list[i].id).hide();
                        $("#fp_"+list[i].id).hide();
					}else
						$("#type_"+list[i].id).html("普通管理员");
				}
				
				$(".distribution").unbind("click");
				$(".distribution").bind("click", function(){
					var id = $(this).attr("data-id");
					parent.openLayerWithBtn('分配角色','${basePath!}/admin/selectRole?id='+id,'500px','300px', null, ['选好了','不选了'], function(index, layero){
						var body = parent.layer.getChildFrame('body', index);
						var $checked = body.find(".role:checked");
						var ids = "";
						if($checked != null && $checked.length > 0){
							$checked.each(function(i,e){
								ids += ","+$(e).val();
							});
							ids = ids.substring(1);
						}
						$.post("${basePath!}/admin/distribution",{id:id,ids:ids},function(r){
							parent.messageModel(r.msg);
							parent.layer.close(index);
						},"json");
					});
				});
				

				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('${basePath!}/admin/deleteById?',param,$pager);
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改管理员-'+title,'${basePath!}/admin/updateAdmin?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${basePath!}/opera/mAdmin/delete.htm',param,$pager);
			}
		});
		
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加管理员','${basePath!}/admin/addAmin','1000px','700px',$pager);
		});
	});
	
</script>

	</body>
</html>
