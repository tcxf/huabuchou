
<html>
<head>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">


<style type="">
body {
	background-image: none;
}
</style>
</head>


<body>
	<div class="mar-top col-lg-12 main-tabls display:none;" id="roleDiv" style="margin-left: 0px;">
		<div id="distribution">
			<div class="form-group" style="padding:10px;">
             	<div for="siteurl"><b>选择角色</b></div><br/>
             	<div class="field">
						<#list list as l>
                            <label><input type="checkbox" class="role automatic-save" name="ids" id="${l.id!}" value="${l.id!}"/> ${l.roleName!}</label>
						</#list>


				</div>
			</div>
	  	</div>
	</div>
</body>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$.post("${basePath!}/admin/selectAdminRoleStr",{id:"${id!}"},function(r){
			var admins = r.data;
			if(admins != null){
				for(var i = 0 ; i < admins.length ; i++){
					console.log("==="+admins[i]);
					$("#"+admins[i].roleId).iCheck('check');
				}
			}

			$("#roleDiv").show();
		},"json");
		
		
		
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
		    increaseArea: '20%' // optional
		});
	});
	
</script>
</html>