<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>会员管理</h5>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">用户名称</label>
								<input type="text" name="realName" placeholder="请输用户名称或者手机号码" id="realName" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	
	
	<script type="text/javascript">
	$(document).ready(function(){
		
   	
		var $pager =  $("#data").pager({
			url:"${hsj}/page/assList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>用户名称</th>'+
							'<th>手机号</th>'+
							'<th>用户注册时间</th>'+
							'<th>授信状态</th>'+
							'<th>是否为老客户</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{realName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{createDateStr}</td>'+
							'<td id="authStatus_{id}"></td>'+
							'<td id="mtype_{id}"></td>'+  
							'<td id="status_{id}">'+ 
						'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){  
					var authStatus = list[i].authStatus;
					var mtype = list[i].mtype;
					if(authStatus){
						$("#authStatus_"+list[i].id).html("已授信");
					}else{
						$("#authStatus_"+list[i].id).html("未授信");
					}
					
					var $btn;
					if(mtype==0){
						$("#mtype_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>非老客户</span>");
						$btn = $('<a href="javascript:void(0);" data-id="'+list[i].id+'" class="btn btn-primary btn-sm lk"><i class="fa fa-unlock"></i> <span>设为老客户</span> </a>');
					}else{
						$("#mtype_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>老客户</span>");
						$btn =  $('<a href="javascript:void(0);" data-id="'+list[i].id+'" class="btn btn-primary btn-sm lk"><i class="fa fa-lock"></i> <span>取消老客户</span> </a>');
					}
					
					
					$btn.bind("click", function(){
						var isFreeze = "";
						if($(this).find("i").hasClass("fa-lock")){
							$(this).find("i").removeClass("fa-lock");
							$(this).find("i").addClass("fa-unlock");
							$(this).find("i").parent().find("span").html("设为老客户");
							isFreeze = "true";
						}else{
							$(this).find("i").removeClass("fa-unlock");
							$(this).find("i").addClass("fa-lock");
							$(this).find("i").parent().find("span").html("取消老客户");
							isFreeze = "false";
						}
						var id =$(this).attr("data-id");
						$.post('<%=path%>/page/assUpdate.htm',{isFreeze:isFreeze,uid:id},function(r){
							parent.messageModel(r.resultMsg);
							if(r.resultCode=="1"){
								$pager.gotoPage($pager.pageNumber);
							}
						},"json");
					});
					
					$("#status_"+list[i].id).append($btn);
					
					
				}
				
				
				//编辑页面
				$(".edit").click(function (){
					var id = $(this).attr("data");
					$.ajax({
						url:"${hsj}/wallet/wallettxdk.htm",
					    type:'POST', //GET
					    data:{id:id
					    	},
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    success:function(d){
					    	if(d.resultCode==1){
					    		parent.messageModel(d.resultMsg);
					    		window.location.href="<%=path %>/wallet/walletxjl.htm";
					    	}
					    }
					});
				});
				//编辑页面
				$(".detail").click(function (){
					var id = $(this).attr("data");
					$.ajax({
						url:"${hsj}/wallet/wallettxbh.htm",
					    type:'POST', //GET
					    data:{
					    	id:id
					    },
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    success:function(d){
					    	if(d.resultCode==1){
					    		parent.messageModel(d.resultMsg);
					    		window.location.href="<%=path %>/wallet/walletxjl.htm";
					    	}
					    }
					});	
				
				});	

				$("#loading-example-btn").click(function(){
					$pager.gotoPage($pager.pageNumber);
				});		
		
				}
			
			
			
		});

	
	});
	
</script>

	</body>
</html>
