<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />

    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="css/y.css" />
    <link rel="stylesheet" href="js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <script type="text/javascript" src="js/mui/mui.min.js" ></script>
    <title>银行卡预留手机号变更</title>

    <style>
        body,html{
            background: #eee;
        }
        .bank_card{
            margin-top: 5%;
            text-align: center;
        }
        .bank_card>img{
            width: 90%;
        }
        .container{
            margin-top: 5%;
            background: #3093f9;
            border-radius: 5px;
            background-size:95% ;
            color: #ffffff;
            width: 95%;
            line-height: 38px ;
        }
    </style>
</head>
<body>
<div class="container">
	<c:forEach items="${blist }" var="list">
		<div class="row">
        <div class="col-xs-7" style="margin-top: 2%;font-size: 16px ">
            <img src="<%=path %>/img/img/ionic-yhk.png" alt=""/>
            ${list.bankName }
        </div>
        <div class="col-xs-5" style="font-size: 14px;margin-top: 3%">
            手机尾号 ${list.mobile }
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="font-size: 22px">
        	${list.bankCard }
            **** &nbsp****   &nbsp**** &nbsp****　4254
        </div>
    </div>
	</c:forEach>
    
</div>
<div class="bank_card">
    <img id="addBankCard " onclick="javascript:window.location.href='<%=path %>/bankCard/bankCardAdd.htm'" src="<%=path %>/img/img/ionic-tjk.png" alt=""/>
    <div  style="color: #b3b3b3;">添加银行卡</div>
</div>
</body>
</html>

