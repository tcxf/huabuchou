<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 基本表单</title>
<meta name="keywords" content="">
<meta name="description" content="">

<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>银行卡信息</h5>
						<div class="form-group"
							style="position: absolute;right:20px;top:5px;z-index: 99999;">
							<div>
								<input class="btn btn-primary submit-save" id="submit-save"
									type="button" value="保存内容" /> <input
									class="btn btn-white cancel" id="cancel" type="button"
									value="取消" />
							</div>
						</div>
					</div>
					<div class="ibox-content">
						<form id="_f" method="post" class="form-horizontal">
							<input type="hidden" name="id" value="" />
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户行名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="20"
											placeholder="请输入开户行名称" id="bname">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">请输入卡号</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="19"
											placeholder="请输入卡号" id="bcard">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户人姓名</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="5"
											placeholder="请输入开户人姓名" id="buname">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">请输入手机号</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="11"
											placeholder="请输入手机号" id="bm">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户人身份证</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="18"
											placeholder="请输入开户人身份证" id="idCard">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">默认卡</label>
									<div class="col-sm-8">
										<select class=input id="isDefault">
											<option selected value=1>是</option>
											<option value=2>否</option>
										</select>
									</div>
								</div>

							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
	$(document).ready(function() {
	    $("#isDefault").select();
		$("#submit-save").click(function() {
			if($("#bname").val()==""){
				parent.messageModel("请填写开户行！");
				return;
			}
			if($("#bcard").val()==""){
				parent.messageModel("请填写卡号！！");
				return;
			}
			if($("#buname").val()==""){
				parent.messageModel("请填写开户人姓名！");
				return;
			}
			if($("#bm").val()==""){
				parent.messageModel("请填写手机号！");
				return;
			}
			if($("#idCard").val()==""){
				parent.messageModel("请填写身份证号码！");
				return;
			}
            if($("#bankcn").val()==""){
                parent.messageModel("请填写银行卡编码！");
                return;
            }
			$.ajax({
				type: "post",
				url: "${basePath!}/bank/newbankadd?bname="+$("#bname").val()+ "&buname=" + $("#buname").val(),
				data: "&bcard=" + $("#bcard").val()  + "&bm=" + $("#bm").val() + "&idCard=" + $("#idCard").val() + "&isDefault=" + $("#isDefault").val(),
				success: function(msg) {
					if (msg.code == 0) {
						parent.messageModel(msg.msg);
						parent.c.gotoPage(null);
						parent.closeLayer();
				
					} else {
						parent.messageModel(msg.msg);
					}
				},
				dataType: "json"
			});
		});

	});
	</script>