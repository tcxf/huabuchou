<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<%=path %>/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="css/zy.css"/>
    <link rel="stylesheet" href="css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="js/layer_mobile/need/layer.css" />
    <title></title>
    <style>
        .body{
            background: #f5f5f5;
        }
        #top-title{
            margin-top: 2%;
            margin-left: 2%;
        }
          .m-tcxy h4{
            text-align: center;
            color: #333;
        }
        .m-tcxy p{
            font-size: 0.8rem;
            color: #666;
        }
    </style>
</head>
<body>
<div id="top-title">
    <p>请绑定持卡人本人的银行卡</p>
</div>
<div class="c-content" style="margin-top: 0px; padding:0px;">
    <ul class="sj-ul">
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    持卡人:
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="夏侯琳"
                           style=" background-size:3%;font-size: 14px ;
                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                </div>
            </div>
        </li>
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    身份证号：
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="430**** **** **** *452"
                           style="  background-size:3%;font-size: 14px ;
							width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                </div>
            </div>
        </li>
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    卡号：
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="请输入"
                           style="background-size:3%;font-size: 14px ;
							width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                </div>
            </div>
        </li>
    </ul>
</div>
   <div class="modal" id="mymodal1" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <div class="m-tcxy">
       <h4>银行卡支付协议</h4>
        <p>
          &nbsp&nbsp&nbsp&nbsp银行卡支付服务协议（以下简称“本协议”）是城市运营商与“佰联同城”会员(以下简称“您”)就“佰联同城”消费服务平台消费、授信及消费分期相关支付服务
                             （以下简称“本服务”）的使用等事项所订立的有效合约。会员通过手机网络页面点击确认或与本区域城市运营商以其他线下方式选择接受本协议，
                               即表示会员与本区域城市运营商已达成协议并同意接受本协议的全部约定内容。
        </p>
        <p>
                                在接受本协议之前，请您仔细阅读本协议的全部内容。如果您不同意本协议的任意内容，或者无法准确理解对条款的解释，请不要进行后续操作。
        </p>
        <p>您在“佰联同城”消费服务平台中绑定银行卡时，应确保您在使用本服务时的银行卡为您本人的银行卡，确保您使用银行卡的行为合法、有效，
                                未侵犯任何第三方合法权益；否则因此造成城市运营商、商户或持卡人损失的，您应负责赔偿并承担全部法律责任，包括但不限于冻结您的相关银行账户及资金、
                                从您的银行账户扣除相应的款项等。
        </p>
        <p>
                             您应妥善保管银行卡、密码以及“佰联同城”消费服务平台账号、密码等有关的一切信息。如您遗失银行卡、“佰联同城”消费服务平台账户密码或相关信息的，
                             您应及时通知发卡行及城市运营商，以减少可能发生的损失。因泄露密码、丢失银行卡或手机等所致损失需由您自行承担。
        </p> 
        <p>您认可和同意：输入密码或手机验证码即视为您确认交易和交易金额并不可撤销地向“佰联同城”消费服务平台发出指令，
        “佰联同城”消费服务平台有权根据您的该指令委托银行或第三方从您的银行卡中划扣资金给相关商户或收款人。届时您不应以未在交易单据中签名、签名不符、
                           非本人意愿交易等原因要退款或逃避其责任。
        </p>
        <p>
                            如您向城市运营商申请授信、消费分期等服务，须提供申请和相关资料，当城市运营商通过审核允许您消费分期或同意授信，则需签订相关合同、协议，您在相关合同、协议上签字认可，
                            表示您同意在之后的分期还款约定之日会主动发起扣款指令，否则城市运营商可委托第三方从您银行卡账户无条件扣划相应款项，绝无异议。
                            您在对使用本服务过程中签字认可或发出指令的真实性及有效性承担全部责任，您承诺，“佰联同城”消费服务平台依照您的指令进行操作的一切风险由您承担。
        </p>
        <p>您认可“佰联同城”消费服务平台账户的使用记录数据、交易金额数据等均以“佰联同城”消费服务平台记录的数据为准。
        </p>
        <p>
                               同时您授权留存您在“佰联同城”消费服务平台网站或客户端填写的相应信息，以供后续向您持续性地提供相应服务（包括但不限于将本信息用于向您推广、提供其他优质产品或服务）。
        </p>
        <p>您同意并授权“佰联同城”消费服务平台依据其自身判断对涉嫌欺诈或被他人控制并用于欺诈目的的账户采取相应的措施，上述措施包括但不限于冻结账户及资金、
                              处置涉嫌欺诈的资金。出现下列情况之一的，“佰联同城”消费服务平台有权立即终止您使用“佰联同城”消费服务平台相关服务而无需承担任何责任：（1）违反本协议的约定；
                             （2）违反“佰联同城”消费服务平台网站的条款、协议、规则、通告等相关规定，而被网站终止提供服务的；（3）城市运营商认为向您提供本服务存在风险的；（4）您的银行卡有效期届满。
        </p>
        <p>
                              您同意，本协议适用我国法律。因城市运营商与您就本协议的签订、履行或解释发生争议，双方应努力友好协商解决。如协商不成，同意由城市运营商住所地法院管辖审理双方的纠纷或争议。
        </p>
        <p>
                             本协议内容包括协议正文及所有“佰联同城”消费服务平台已经发布的或将来可能发布的使用规则，所有规则为本协议不可分割的一部分，与协议正文具有相同法律效力。
                             若您在本协议内容发生修订后，继续使用本服务的，则视为您同意最新修订的协议内容，否则您须立即停止使用本服务。
        </p>
    </div>
            </div>
        
        </div>
    </div>
</div>
<div id="xy">
    <input type="checkbox"/>
    <span>我已阅读并同意 <b style="color: #999999"><a href="#mymodal1" data-toggle="modal" style="color: #17ae61"> 《实名协议》</a> </b></span>
</div>

<div onclick="javascript:window.location.href='<%=path %>/bankCard/bankCardAddTwo.htm'" class="yzm-btn tj" style="background: #17ae61; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 40%; margin: 0 auto; margin-top: 150px;">
    下一步
</div>
	 <script src="<%=path %>/js/jquery-1.11.3.js"></script>
<script src="<%=path %>/js/bootstrap.js"></script>
 <script src="<%=path %>/js/html5shiv.min.js"></script>
    <script src="<%=path %>/js/respond.min.js"></script>
</body>
</html>