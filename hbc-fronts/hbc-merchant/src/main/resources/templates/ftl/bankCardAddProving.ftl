<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="js/mui/css/mui.min.css" />
    <script type="text/javascript" src="js/mui/mui.min.js"></script>

    <link rel="stylesheet" href="js/layer_mobile/need/layer.css" />
    <script type="text/javascript" src="js/layer_mobile/layer.js"></script>

    <script type="text/javascript" src="js/info.js"></script>

    <title></title>
    <style>
        body,
        html {
            background: #eee;
            height: 100%;
        }

        .layui-m-layer {
            z-index: 0 !important;
        }
        #top-title>p{
            padding-top: 2%;
            margin-left: 2%;
        }

        .login-model {
            margin-top: 5% !important;
            width: 100%;
            margin: 0 auto;
            border-radius: 7px;
            padding: 18px;
        }
    </style>
</head>

<body>
<div id="top-title">
    <p>验证码将发送到手机号188****3459</p>
</div>
<div class="c-content login-model_01" style="margin-top:0;">
    <div class="webkit-box ">
        <div style="line-height: 30px; width: 11%;">
            <div class="input-left-name">
                验证码：
            </div>
        </div>
        <div class="wkt-flex">
            <input type="text" id="name" name="name" class="form-control reg-input" placeholder="请输入" style="width: 100%; height: 30px; padding-left: 27px; font-size: 14px;">
        </div>
    </div>
</div>
<div onclick="javascript:window.location.href='<%=path %>/bankCard/bankCardAddSuccess.htm'" class="yzm-btn tj" style="background: #17ae61; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 40%; margin: 0 auto; margin-top: 50px;">
    完成
</div>



</body>

<script>
    $(document).ready(function() {

    });
</script>

</html>
