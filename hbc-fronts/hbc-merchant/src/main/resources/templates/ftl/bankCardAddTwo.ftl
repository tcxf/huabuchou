<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/zy.css"/>
    <link rel="stylesheet" href="css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="js/layer_mobile/need/layer.css" />
    <title></title>
    <style>
        .body{
            background: #f5f5f5;
        }
        #top-title{
            margin-top: 2%;
            margin-left: 2%;
        }
    </style>
</head>
<body>
<div id="top-title">
    <p>交通银行信用卡</p>
</div>
<div class="c-content" style="margin-top: 0px; padding:0px;">
    <ul class="sj-ul">
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    有效期:
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="月份/年份"
                           style=" background-size:3%;font-size: 14px ;
                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
                </div>
            </div>
        </li>
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    CVN码：
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="背面最后3位数字"
                           style="  background-size:3%;font-size: 14px ;
							width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                </div>
            </div>
        </li>
        <li style="padding: 10px 0px 2px 10px;">
            <div class="webkit-box">
                <div class="input-left-name">
                    手机号：
                </div>
                <div class="wkt-flex">
                    <input type="tel" id="phone" name="phone" class="form-control reg-input" placeholder="请输入开户银行手机号码"
                           style="background-size:3%;font-size: 14px ;
							width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
                </div>
            </div>
        </li>
    </ul>
</div>
<div id="xy">
    <input type="checkbox"/>
    <span>我已阅读并同意 <b style="color: #999999"> 《快捷支付服务协议》 </b></span>
</div>

<div onclick="javascript:window.location.href='<%=path %>/bankCard/bankCardAddProving.htm'" class="yzm-btn tj" style="background: #17ae61; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 40%; margin: 0 auto; margin-top: 150px;">
    下一步
</div>
</body>
</html>
