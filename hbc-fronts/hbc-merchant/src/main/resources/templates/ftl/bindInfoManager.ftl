
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>银行卡管理</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新增银行卡</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
	$(document).ready(function(){
   		
		var $pager = $("#data").pager({
			url:"${basePath!}/bank/findbankinfo",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>开户银行</th>'+
							'<th>银行卡号</th>'+
							'<th>持卡人手机号</th>'+
							'<th>持卡人姓名</th>'+
							/* '<th>操作</th>'+ */
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{bankName}</td>'+
							'<td>{bankCard}</td>'+
							'<td>{mobile}</td>'+
							'<td >{name}</td>'+
						/* 	'<td style="width:200px;">'+
								'<a href="javascript:void(0);" data="{id},{name}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> 删除 </a> '+
							'</td>'+ */
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="6"><center>暂无数据</center></tr>'
			},
			callbak: function(blist){
				var list = blist.data.records;
				for(var i = 0 ; i < list.length ; i++){
					$("#img_"+list[i].id).html('<img src="${basePath!}'+list[i].img+'" style="width:75px;height:75px;"/>');
				}
				
				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('/.htm',param,$pager);
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改商品-'+title,'/.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('/.htm',param,$pager);
			}
		});
		
		//打开新增页面
		 $("#new").click(function (){
			parent.openLayer('添加银行卡','${basePath!}/bank/newbank','1000px','700px',$pager);
		});
	
			
	});
	
</script>

	</body>
</html>
