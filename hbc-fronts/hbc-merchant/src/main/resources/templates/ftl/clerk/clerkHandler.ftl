<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=path %>/css/animate.css" rel="stylesheet">
    <link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>${id==null?"添加":"修改"}店员</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
								<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value="${id}"/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">店员登录名</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,maxLength[20],loginName" validate-msg="required:店员登录名不能为空,loginName:店员登录名不能有特殊字符" id="logName" name="logName" value="${entity.logName}" maxlength="30" placeholder="请填写店员登陆名">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">店员姓名</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,maxLength[20],loginName" validate-msg="required:店员姓名不能为空,loginName:店员姓名不能有特殊字符" id="sname" name="sname" value="${entity.sname}" maxlength="30" placeholder="请填写店员姓名">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">登陆密码</label>
									<div class="col-sm-8">
										<input type="text" id="logPwd" name="logPwd" class="form-control" validate-rule="<c:if test="${id == null }">required</c:if>,maxLength[10],ffstring" class="form-control" placeholder="店员登陆密码<c:if test="${id != null }">，为空则不更新密码</c:if>" />
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label"></label>
									<div class="col-sm-8">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
        	
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $("#cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
    			if(!$("#_f").validate()) return;
    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
    			var index = layer.load(2, {time: 10*1000});
    			$.ajax({
    				url:'${hsj}/salesclerk/<c:choose><c:when test="${id != null}">salesclerkEditAjaxSubmit</c:when><c:otherwise>salesclerkAddAjaxSubmit</c:otherwise></c:choose>.htm',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:$("#_f").serialize(),
    				success: function(data){ //成功
    					layer.close(index);
    					var obj = data;
    					if (obj.resultCode == "-1") {
    						parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
    						$this.html("保存内容");
    						$this.attr("disabled", false);
    					}
    					
    					if (obj.resultCode == "1") {
    						//消息对话框
    						parent.messageModel("保存成功!");
    						parent.c.gotoPage(null);
    						parent.closeLayer();
    					}
    				}
    			});
    		});
            
        });
    </script>
</body>

</html>
