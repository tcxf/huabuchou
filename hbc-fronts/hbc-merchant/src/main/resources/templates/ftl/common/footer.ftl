<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String base = request.getContextPath();
String name = (String)request.getParameter("name");
%>
<footer class="mui-bar mui-bar-tab" id="footerBtn" style="box-shadow: none; border-top: 1px solid #cccccc;">
	<div class="webkit-box footer-m">
		<div class="wkt-flex" onclick="javascript:window.location='<%=base%>/index.htm';">
			<img src="<%=base %>/images/sx<%if(name != null && name.equals("index")){%>-select<%} %>.png"/>
			<div <%if(name != null && name.equals("index")){%>class="active"<%} %>>授信</div>
		</div>
		<div class="wkt-flex" onclick="javascript:window.location='<%=base%>/b/bill.htm';">
			<img src="<%=base %>/images/zd<%if(name != null && name.equals("bill")){%>-select<%} %>.png"/>
			<div <%if(name != null && name.equals("bill")){%>class="active"<%} %>>账单</div>
		</div>
		<div class="wkt-flex">
			<img src="<%=base %>/images/fx<%if(name != null && name.equals("find")){%>-select<%} %>.png"/>
			<div <%if(name != null && name.equals("find")){%>class="active"<%} %>>发现</div>
		</div>
		<c:if test="${sid == null }">
		<div class="wkt-flex" onclick="javascript:window.location='<%=base%>/Mysx.htm';">
			<img src="<%=base %>/images/my<%if(name != null && name.equals("my")){%>-select<%} %>.png"/>
			<div <%if(name != null && name.equals("my")){%>class="active"<%} %>>我的</div>
		</div>
		</c:if>
	</div>
</footer>