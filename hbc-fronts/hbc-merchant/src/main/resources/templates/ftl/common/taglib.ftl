<%@page import="com.hsj.grant.common.util.SystemSettingTools"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set value="<%=request.getContextPath() %>" var="hsj"></c:set>
<c:set value="<%= SystemSettingTools.getSystemSetting().getResSiteUri() %>" var="res"></c:set>
