<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>- 基本表单</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="shortcut icon" href="favicon.ico">
<link href="<%=path%>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="<%=path%>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="<%=path%>/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<%=path%>/css/animate.css" rel="stylesheet">
<link href="<%=path%>/css/style.css?v=4.1.0" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/plugins/webuploader/webuploader.css">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
					<form id="_f" method="post" class="form-horizontal">
						<input type="hidden" id="id" value="${entity.id}" />
						<div class="ibox-title">
							<h5>商品分类信息</h5>
						</div>
						<div class="ibox-content active tab-pane">
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<label class="col-xs-4 control-label" style="line-height: 30px;">分类名称</label>
								<div class="col-xs-8">
									<input type="text" id="name" class="form-control"
										value="${entity.name}"
										validate-rule="required,maxLength[10],loginName"
										validate-msg="required:请填写分类名称,maxLength:分类名称最多只能输入10个字,loginName:分类名称不能有特殊符号"
										style="margin-bottom: 20px;" placeholder="请填写分类名称">
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<label class="col-xs-4 control-label" style="line-height: 30px;">颜色代码</label>
								<div class="col-xs-8">
							 <select name="colorCode"
										id="colorCode" class="form-control"'  style="margin-bottom: 20px; font-size: 10px;" >
										<option >无</option>
										<option value='#FFCC00' <c:if test="${entity != null && entity.colorCode=='#FFCC00'}">selected</c:if>>中黄色</option>
										<option value='#FF6600'<c:if test="${entity != null && entity.colorCode=='#FF6600'}">selected</c:if>>橙色</option>
										<option value='#FF0033'<c:if test="${entity != null && entity.colorCode=='#FF0033'}">selected</c:if>>红色</option>
										<option value='#99CC33'<c:if test="${entity != null && entity.colorCode=='#99CC33'}">selected</c:if>>若绿色</option>
										<option value='#9999FF'<c:if test="${entity != null && entity.colorCode=='#9999FF'}">selected</c:if>>青紫色</option>
										<option value='#66CCFF'<c:if test="${entity != null && entity.colorCode=='#66CCFF'}">selected</c:if>>湖蓝色</option>
										<option value='#CCFF00'<c:if test="${entity != null && entity.colorCode=='#CCFF00'}">selected</c:if>>若草色</option>
										<option value='#FF9999'<c:if test="${entity != null && entity.colorCode=='#FF9999'}">selected</c:if>>珊瑚色</option>
										<option value='#FF66FF'<c:if test="${entity != null && entity.colorCode=='#FF66FF'}">selected</c:if>>赤紫色</option>
										<option value='#0066FF'<c:if test="${entity != null && entity.colorCode=='#0066FF'}">selected</c:if>>蓝色</option>
										<option value='#339933'<c:if test="${entity != null && entity.colorCode=='#339933'}">selected</c:if>>绿色</option>
										<option value='#6600CC'<c:if test="${entity != null && entity.colorCode=='#6600CC'}">selected</c:if>>紫色</option>
										<option value='#996633'<c:if test="${entity != null && entity.colorCode=='#996633'}">selected</c:if>>褐色</option>
										<option value='#FFFF00'<c:if test="${entity != null && entity.colorCode=='#FFFF00'}">selected</c:if>>柠檬黄</option>
									</select>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<label class="col-xs-4 control-label" style="line-height: 30px;">排序编号</label>
								<div class="col-xs-8">
									<input type="text" id="orderList" class="form-control"
										value="${entity.orderList}" validate-rule="required,number"
										validate-msg="required:请填写排序编号,number:分类编号只能输入数字"
										style="margin-bottom: 20px;" placeholder="请填写排序编号">
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-2" style="text-align: center">
									<input class="btn btn-primary submit-save tj" type="button"
										value=" 提 交 " />
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- 全局js -->
		<script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
		<script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>
		<!-- 自定义js -->
		<script src="<%=path%>/js/content.js?v=1.0.0"></script>
		<script type="text/javascript" src="<%=path%>/js/common.js"></script>
		<script src="<%=path%>/js/plugins/layer/layer.min.js"></script>
		<!-- iCheck -->
		<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
		<script src="<%=path%>/js/plugins/webuploader/webuploader.js"></script>
		<script>
		$(document).ready(function() {
		   $("#colorCode").select();
     $(".tj").click(function() {
        if (!$("#_f").validate()) {
            return;
        }
        var $this = $(this);
        $this.html(" 处 理 中 ");
        $this.attr("disabled", true);
        index = layer.load(2, {
            time: 10 * 1000
        });
        $.post('<%=path%>/goodsCategory/<c:if test="${entity == null}">goodsCategoryAddAjaxSubmit</c:if><c:if test="${entity != null}">goodsCategoryEditAjaxSubmit</c:if>.htm', {
            id: $("#id").val(),
            name: $("#name").val(),
            orderList: $("#orderList").val(),
            colorCode: $("#colorCode").val()
        },
        function(obj) {
            layer.close(index);
            window.parent.layer.alert(obj.resultMsg == null ? "系统异常": obj.resultMsg);
            if (obj.resultCode == "-1") {
                $this.html(" 提 交 ");
                $this.attr("disabled", false);
            } else {
                window.parent.closeLayer();
            }
        },
        "json");
    });
});
		</script>
</body>

</html>