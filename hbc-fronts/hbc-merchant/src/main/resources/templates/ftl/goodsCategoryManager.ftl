<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商品分类</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 添加分类</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data">
							<div class="fixed-table-container form-group">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>分类名称</th>
											<th>商品数量</th>
											<th>分类排序</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody id="list">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <script src="<%=path %>/js/template/template.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		loadList();
		
		$('#new').click(function(){
			showEdit(1);
		});
	});

	function showEdit(type,id){
		var height = $(window).height();
		var width = $(window).width();
		if(type == 1){
			title = "添加分类";
			uri = "<%=path%>/goodsCategory/forwardGoodsCategoryAdd.htm";
		}else{
			title = "修改分类";
			uri = "<%=path%>/goodsCategory/forwardGoodsCategoryEdit.htm?id="+id;
		}
		l = layer.open({
		  type: 2,
		  title:title,
		  skin: 'layui-layer-demo', //样式类名
		  closeBtn: 1, //不显示关闭按钮
		  offset:['0px',(width-400)+'px'],
		  area:['400px',height+'px'],
		  anim: 2,
		  shadeClose: false, //开启遮罩关闭
		  content: uri
		});
	}
	
	var l;
	function closeLayer(){
		layer.close(l);
		loadList();
	}
	function loadList(){
		$("#list").empty();
		$.post('<%=path%>/goodsCategory/goodsCategoryManagerList.htm',null,function(d){
			if(d.data.length == 0){
				$("#list").append('<tr style="text-align:center;"><td colspan="3">暂无商品分类</td></tr>');
			}else{
				for(var i = 0 ; i < d.data.length ; i++){
					var html = template.load({
						host : '<%=path%>/',
						name : "goods_category",
						data : d.data[i]
					});
					$html = $(html);
					$html.find(".edit").click(function(){
						showEdit(2,$(this).attr("data"));
					});
					$html.find(".remove").click(function(){
						if($(this).attr('data-size') > 0){
							parent.messageModel('该分类下还有商品信息，无法删除');
							return;
						}
						var id = $(this).attr('data');
						var $this = $(this);
						parent.confirm('是否删除当前分类？',function(){
							$.post('<%=path%>/goodsCategory/forwardGoodsCategoryDel.htm',{id:id},function(d){
								parent.messageModel(d.resultMsg);
								if(d.resultCode != -1){
									$this.parent().parent().remove();
								}
							},'json');
						});
					});
					$("#list").append($html);
				}
			}
		},'json');
	}
	</script>

	</body>
</html>
