
<meta charset="UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta charset="UTF-8">

<link rel="shortcut icon" href="favicon.ico">
<link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="${basePath!}/css/animate.css" rel="stylesheet">
<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox-content row">
                <div class="col-sm-12">
                    <div class="row row-sm text-center">
                        <h2 class="text-left">交易记录</h2>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-error">
                                <div class="h1 text-fff h1 _font" id="s_1">¥ ${entity.dayTradingCount!}</div>
                                <span class="text-fff _font">今日交易笔数</span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-success">
                                <div class="h1 text-fff font-thin h1 _font" id="s_2">¥  ${entity.dayTradingMoney!}</div>
                                <span class="text-fff _font">今日交易金额</span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-primary">
                                <div class="h1 text-fff font-thin h1 _font" id="s_3">¥  ${entity.monthTradingCount!}</div>
                                <span class="text-fff _font">本月交易笔数</span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-info">
                                <div class="text-fff font-thin h1 _font" id="s_4">¥  ${entity.monthTradingMoney!}</div>
                                <span class="text-fff _font">本月交易金额</span>
                            </div>
                        </div>
                        <h2 class="text-left">结算统计</h2>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-primary">
                                <div class="h1 text-fff font-thin h1 _font" id="s_5">¥  ${entity.noSetMoney!}</div>
                                <span class="text-fff _font">待结算金额</span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="panel padder-v item bg-success">
                                <div class="h1 text-fff font-thin h1 _font" id="s_7">¥  ${entity.yesSetMoney!}</div>
                                <span class="text-fff _font">已结算金额</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="row ibox-content">
                <h2 class="text-left">近七日交易</h2>
                <div class="col-sm-12" id="flot-chart" style="height:400px;">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript" src="${basePath!}/js/echarts.simple.min.js"></script>
<!-- Flot -->
<!--flotdemo-->
<script type="text/javascript">
    $(document).ready(function(){
        var t = [1,2,14,15,5,7];
        <#--for(var i = 0 ; i < t.length ; i++){-->
            <#--$.post("${basePath!}/sta/statistics.htm",{type:t[i]},function(d){-->
                <#--if(d.data.type==14)-->
                    <#--$("#s_3").html("¥ "+d.data.v);-->
                <#--if(d.data.type==15)-->
                    <#--$("#s_4").html("¥ "+d.data.v);-->
                <#--if(d.data.type != 1 && d.data.type != 3 )-->
                    <#--$("#s_"+d.data.type).html("¥ "+d.data.v);-->
                <#--else-->
                    <#--$("#s_"+d.data.type).html(d.data.v);-->
            <#--},"json");-->
        <#--}-->



        $.post("${basePath!}/order/findByTraind",{},function(d){
            var data = d.data;
            var chart = echarts.init(document.getElementById('flot-chart'));
            option = {
                title: {
                    text: '近七日交易额'
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                legend: {
                    data:['交易额']
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        dataView: {readOnly: false},
                        magicType: {type: ['line', 'bar']},
                        restore: {},
                        saveAsImage: {}
                    }
                },
                xAxis:  {
                    type: 'category',
                    boundaryGap: false,
                    data: data.dtimeArr
                },
                yAxis: {
                    axisLabel: {
                        formatter: function (val) {
                            return '¥ '+val;
                        }
                    },
                    type: 'value'
                },
                series: [
                    {
                        name:'交易额',
                        type:'line',
                        data:data.tMoneyArr
                    }
                ]
            };
            chart.setOption(option);
        },"json");

    });
</script>
</body>

</html>