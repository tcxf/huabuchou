<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">

    <title> 授信管理系统- 商户端</title>

    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .nav .open > a,
        .nav .open > a:hover,
        .nav .open > a:focus {
            background: #9c5058i;
        }

        .navbar-static-side {
            background: url(${basePath!}/img/img/img_right_background.png) no-repeat;
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3)
        }

        .nav-header {
            background: transparent;
            font-size: 20px;
            padding: 8px 25px;
        }

        .hidden-folded {
            color: #dadada !important;
            font-size: 12px;

        }

        body.mini-navbar .nav-header {

            background-color: #372d2d;
        }

        .nav > li a {

            color: #999;
        }

        .nav > li > a:hover {

            color: #fff;
        }

        .nav > li > a {
            padding: 20px 20px 8px 25px;
        }

        .border-bottom {
            border-bottom: none !important;
        }

        .navbar-default .nav > li > a:hover, .navbar-default .nav > li > a:focus {
            background-color: #372d2d;
            color: white;
        }

        .nav > li.active {
            border-left: 4px solid #9c5058;
            background: #372d2d;
        }

        .navbar-fixed-top, .navbar-static-top {
            background: url(${basePath!}/img/img/img_top_background.png) no-repeat;
            padding-bottom: 10px;
            box-shadow: none;
        }

        .btn-info {
            color: #000 !important;
            background-color: #fff;
            border-color: #fff;
        }

        .btn-info:focus {
            color: #000 !important;
            background-color: #fff;
            border-color: #fff;
        }

    </style>
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close"><i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                                    <span class="block m-t-xs" style="font-size:20px;">
                                        <strong class="font-bold dropdown">
                                        	<a class="dropdown-toggle" data-toggle="dropdown" href="#"
                                               style="background: transparent;">${loginAdmin.name!}</a>
                                        	<ul class="dropdown-menu dropdown-alerts">
				                                <li>
				                                    <a href="mailbox.html">
				                                        <div>
				                                            <i class="fa fa-cog fa-lg"></i> 修改密码
				                                        </div>
				                                    </a>
				                                </li>
				                                <li class="divider"></li>
				                                <li id="logout">
				                                    <a href="javascript:void(0);">
				                                        <div>
				                                            <i class="fa fa-power-off fa-lg"></i> 退出登录
				                                        </div>
				                                    </a>
				                                </li>
				                                <li class="divider"></li>
				                            </ul>
                                        </strong>
                                    </span>
                                </span>
                        </a>
                    </div>
                    <div class="logo-element">
                    ${loginAdmin.name!}
                    </div>
                </li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <span class="ng-scope">分类</span>
                </li>
                <li>
                    <a class="J_menuItem" href="${basePath!}/order/index">
                        <i class="fa fa-home"></i>
                        <span class="nav-label">主页</span>
                    </a>
                </li>
                <#list menuResources as r>
                    <li>
                        <a href="#"><i class="fa${r.resourceUrl!}"></i> <span class="nav-label">${r.resourceName!}</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <#list functions?keys as f>
                                <#list tbResources as fu>
                                    <#if  fu.parentId?exists && r.id == f && r.id = fu.parentId?string>
                                        <li name = "${r.id}${f}"><a class="J_menuItem" href="${basePath!}${fu.resourceUrl!}">${fu.resourceName!}</a></li>
                                    </#if>
                                </#list>
                            </#list>
                        </ul>
                    </li>
                </#list>
            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-info " href="#"><i
                        class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right" style="display:none;">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li class="m-t-xs">
                                <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="${basePath!}/img/a7.jpg">
                                    </a>
                                    <div class="media-body">
                                        <small class="pull-right">46小时前</small>
                                        <strong>小四</strong> 是不是只有我死了,你们才不爵迹
                                        <br>
                                        <small class="text-muted">3天前 2014.11.8</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="${basePath!}/img/a4.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy">25小时前</small>
                                        <strong>二愣子</strong> 呵呵
                                        <br>
                                        <small class="text-muted">昨天</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a class="J_menuItem" href="mailbox.html">
                                        <i class="fa fa-envelope"></i> <strong> 查看所有消息</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="mailbox.html">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> 您有16条未读消息
                                        <span class="pull-right text-muted small">4分钟前</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="profile.html">
                                    <div>
                                        <i class="fa fa-qq fa-fw"></i> 3条新回复
                                        <span class="pull-right text-muted small">12分钟钱</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a class="J_menuItem" href="notifications.html">
                                        <strong>查看所有 </strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe id="J_iframe" width="100%" height="100%" src="${basePath!}/order/index"
                    frameborder="0" data-id="index/index.htm" seamless></iframe>
        </div>
    </div>

    <!--右侧部分结束-->
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${basePath!}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="${basePath!}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/hAdmin.js?v=4.1.0"></script>
<script type="text/javascript" src="${basePath!}/js/index.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>

<!-- 第三方插件 -->
<script src="${basePath!}/js/plugins/pace/pace.min.js"></script>
<script>
    $(document).ready(function () {

        $("#logout").click(function () {
            layer.confirm('您确认退出系统吗?', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                window.location.href = "${basePath!}/login/logout";
            }, function () {
            });
        });
    });
    var l;
    var c;

    function closeLayer() {
        layer.close(l);
    }

    function closeLayerId(id) {
        layer.close(id);
    }

    function loadingLayer() {
        l = layer.open({
            type: 3
        });
    }

    //弹窗插件
    function openLayer(title, url, width, height, children) {
        //iframe层-父子操作
        c = children;
        l = layer.open({
            type: 2,
            title: title,
            area: [width, height],
            fix: false, //不固定
            maxmin: true,
            skin: 'layui-layer-nobg',
            content: url,
            scrollbar: false
        });
        layer.full(l);
    }


    function openLayerWithBtn(title, url, width, height, children, btns, okfun) {
        //iframe层-父子操作
        c = children;
        l = layer.open({
            type: 2,
            title: title,
            area: [width, height],
            fix: false, //不固定
            maxmin: true,
            content: url,
            btn: btns,
            yes: okfun
        });
    }

    //消费框插件
    function messageModel(title) {
        layer.alert(title);
    }

    function confirm(msg, callback) {
        l2 = layer.confirm(msg, {
            btn: ['确定', '取消'] //按钮
        }, function () {
            callback();
            layer.close(l2);
        }, function () {
        });
    }

    function deleteDataCallBak(url, param, callback) {
        layer.confirm("您确定要删除吗？", {
            btn: ['确定', '取消'] //按钮
        }, function () {
            $.ajax({
                url: url,
                type: 'get', //数据发送方式
                dataType: 'html', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data: param + '&time=' + new Date(), //要传递的数据
                success: function (data) { //成功
                    var obj = JSON.parse(data);
                    if (obj.resultMsg != null && obj.resultMsg != "")
                        messageModel(obj.resultMsg);
                    if (obj.resultCode != "-1") {
                        if (callback != null) {
                            callback(data);
                        }
                    }
                }
            });
        }, function () {
        });
    }

    //删除
    function deleteData(url,param,pager){
        var bo = false;
        layer.confirm("您确定要删除吗？", {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url:url,
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:param, //要传递的数据
                success: function(data){ //成功
                    if(data.msg != null && data.msg != ""){
                        messageModel(data.msg);
                        pager.gotoPage(null);
                    }

                }
            });
        }, function(){});
    }

    //弹出对话框
    function modelYesOrNo(url, param, pager, content) {
        var bo = false;
        layer.confirm(content, {
            btn: ['确定', '取消'] //按钮
        }, function () {
            $.ajax({
                url: url,
                type: 'get', //数据发送方式
                dataType: 'html', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data: param + '&time=' + new Date(), //要传递的数据
                success: function (data) { //成功
                    var obj = JSON.parse(data);
                    if (obj.resultMsg != null && obj.resultMsg != "")
                        messageModel(obj.resultMsg);
                    if (obj.resultCode != "-1") {
                        if (pager != null) {
                            pager.gotoPage(null);
                        }
                    }
                }
            });
        }, function () {
        });
    }


    //请求URL弹出对话框
    function modelReqUrl(url, param, pager) {
        $.ajax({
            url: url,
            type: 'get', //数据发送方式
            dataType: 'html', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
            data: param + '&time=' + new Date(), //要传递的数据
            error: function () { //失败

            },
            beforeSend: function () {
            },
            success: function (data) { //成功
                var obj = JSON.parse(data);
                if (obj.resultMsg != null && obj.resultMsg != "")
                    messageModel(obj.resultMsg);
                if (obj.resultCode != "-1") {
                    if (pager != null) {
                        pager.gotoPage(null);
                    }
                }
            }
        });
    }

    function modelContent(obj) {
        layer.open({
            type: 1,
            title: false,
            area: ['300px', '250px'],
            closeBtn: 0,
            shadeClose: true,
            skin: 'yourclass',
            content: obj
        });
    }

    //关闭选项卡
    function guanbi(obj) {
        //判断该标签后面是否还有子选项卡
        var tabx = $(obj).parent().parent().next().find("a").html();
        if (tabx != undefined) {
            $(obj).parent().parent().next().find("a").tab('show');
        } else {
            $(obj).parent().parent().prev().find("a").tab('show');
        }

        var id = $(obj).parent().attr("href");
        $(obj).parent().parent().remove();
        $(id).remove();
    }
</script>
</body>

</html>
