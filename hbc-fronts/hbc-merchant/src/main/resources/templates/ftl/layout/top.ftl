<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<meta charset="UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="<%=path%>/js/validator-0.7.0/jquery.validator.css" />
<script type="text/javascript" src="<%=path%>/js/validator-0.7.0/jquery.validator.js"></script>
<script type="text/javascript" src="<%=path%>/js/validator-0.7.0/local/zh_CN.js"></script>
<script>
    $(function(){
        //构建用户修改密码对话框
        createUserPwdUpdateDlg();
    });

    //退出系统
    function logout() {
        $.messager.confirm('退出系统','您确认退出系统吗?',function(r){
            if (r){
                window.location.href = "<%=path%>/login/logout.htm";
            }
        });
    }

    function isPwdExist(){
        //手动触发表单验证
        $('#userPwdForm').trigger("validate");
        //检测表单是否所有字段都验证通过
        $('#userPwdForm').isValid(function(v){
            if(v){
                userPwdUpdateAjaxSubmit();
            }
        });
    }

    function userPwdUpdateAjaxSubmit(){
        $.messager.progress({
            title:'请等待',
            msg:'正在努力处理中...'
        });
        $.ajax({
            cache: false,
            type: "POST",
            url:"<%= path %>/admin/userPwdUpdateAjaxSubmit.htm",
            data:$('#userPwdForm').serialize(),
            error: function(request) {
                $.messager.progress('close');
                $.messager.show({
                    title:'提示',
                    msg:'操作失败',
                    timeout:3000,
                    showType:'slide'
                });
            },
            success: function(results) {
                $.messager.progress('close');
                var resultJson = $.parseJSON(results);
                if(resultJson.resultCode == '1'){
                    $.messager.alert('','操作成功','info',function(){
                        $('#userPwdUpdateDlg').dialog('close');
                        $("#oldPwd").val("");
                        $("#pwd").val("");
                        $("#confirmPwd").val("");
                    });
                }
                else{
                    $.messager.show({
                        title:'提示',
                        msg:resultJson.resultMsg,
                        timeout:3000,
                        showType:'slide'
                    });
                }
            }
        });
    }

    //构建修改密码对话框
    function createUserPwdUpdateDlg(){
        $('#userPwdUpdateDlg').dialog({
            title: '管理员修改密码',
            width: 550,
            height: 250,
            closed: true,
            cache: false,
            resizable:true,
            modal: true,
            buttons: [{
                text:'保存',
                iconCls:'icon-ok',
                handler:function(){
                    isPwdExist();
                }
            },{
                text:'取消',
                iconCls:'icon-cancel',
                handler:function(){
                    $('#userPwdUpdateDlg').dialog('close');
                }
            }]
        });
    }

    //修改用户密码
    function updateUserInfo(){
        $('#userPwdUpdateDlg').dialog('open');
    }

</script>
<div class="topleft">
    <img src="images/logo.png" title="系统首页" />
</div>
<div class="topright">
    <div class="user">
        <!-- tab 消息资源链接最好从数据库里面读 -->
        <span>${loginUser.userName}</span>
        <a href="javascript:void(0);" onclick="updateUserInfo()"><i>修改密码</i></a>
        <a href="javascript:void(0);" onclick="logout()"><i>退出系统</i></a>
    </div>
</div>
<div id="userPwdUpdateDlg">
    <form id="userPwdForm" method="post">
        <div class="formbody">
            <ul class="forminfo">
                <li><label>原密码:</label>
                    <input id="oldPwd" name="oldPwd" type="password"  class="dfinput" data-rule="密码:required;oldPwd;oldPwd;"/>
                </li>
                <li id='pwdLi'><label>密码:</label><input id="pwd" name="pwd" type="password" class="dfinput" data-rule="密码:required;password;password;"/></li>
                <li id='confirmPwdLi'><label>确认密码:</label><input id="confirmPwd" name="confirmPwd" type="password" class="dfinput" data-rule="确认密码:required;confirmPwd;match(pwd);"/></li>
            </ul>
        </div>
    </form>
</div>