<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="<%=path%>/css/bootstrap/css/bootstrap.css" />
<link rel="stylesheet"
	href="<%=path%>/css/Font-Awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="<%=path%>/css/app.css" />
<link rel="stylesheet" href="<%=path%>/js/mui/css/mui.min.css" />
<link rel="stylesheet" href="<%=path%>/js/layer_mobile/need/layer.css" />
<title>注册</title>
<style>
body,html {
	background: #eee;
	height: 100%;
}

.layui-m-layer {
	z-index: 0 !important;
}
</style>
</head>

<body>
	<div class="c-content login-model" style="margin-top: 0px;">
		<div class="webkit-box ">
			<div style="line-height: 30px; width: 11%;">
				<img src="<%=path%>/img/images/icon_sj.png" />
			</div>
			<div class="wkt-flex">
				<input type="tel" id="mobile" name="mobile"
					class="form-control reg-input" placeholder="请填写注册手机号"
					style="width: 100%; height: 30px; padding-left: 0px; font-size: 14px;">
			</div>
			<div style="line-height: 30px;" class="clear-input">
				<img src="<%=path%>/img/images/icon_delete.png" />
			</div>
		</div>

		<div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
		<div class="webkit-box ">
			<div style="line-height: 30px; width: 11%;">
				<img src="<%=path%>/img/images/icon_messges.png" />
			</div>
			<div class="wkt-flex">
				<input type="tel" id="yCode" name="yCode"
					class="form-control reg-input" placeholder="请填写短信验证码"
					style="width: 100%; height: 30px; padding-left: 0px; font-size: 14px;">
			</div>
			<div>
				<a class="btn btn-default sms-btn"
					style=" font-size: 0.8em; padding: 5px; " href="#" role="button">获取验证码</a>
			</div>
		</div>

		<div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
		<div class="webkit-box ">
			<div style="line-height: 30px; width: 11%;">
				<img src="<%=path%>/img/images/icon_password.png" />
			</div>
			<div class="wkt-flex">
				<input type="password" id="pwds" name="pwds"
					class="form-control reg-input" placeholder="请输入登陆密码"
					style="width: 100%; height: 30px; padding-left: 0px; font-size: 14px;">
			</div>
			<div style="line-height: 30px;" class="clear-input">
				<img src="<%=path%>/img/images/icon_delete.png" />
			</div>
		</div>

	</div>
	<div class="yzm-btn tj"
		style="background: #17ae61; color:#ffffff; border-radius: 5px; font-size: 17px; height: 50px; line-height: 50px; width: 85%; margin: 0 auto; margin-top: 50px;">
		立即注册</div>
</body>
<script type="text/javascript" src="<%=path%>/js/mui/mui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.js"></script>
<script type="text/javascript" src="<%=path%>/js/layer_mobile/layer.js"></script>
<script type="text/javascript" src="<%=path%>/js/info.js"></script>
<script>
		$(document).ready(function() {
			$(".clear-input").click(function(){
				$(this).parent().find("input").val("");
				$(this).parent().find("input").trigger("keyup");
				$(this).parent().find("input").focus();
			});
			$(".sms-btn").click(function(){
				if($(this).hasClass("disabled")) return;
				if($("#mobile").val().trim().length==0){
					api.tools.toast("手机号码不能为空");
					 return;
				}
				if(!(/^1[34578]\d{9}$/.test($("#mobile").val()))){ 
			        api.tools.toast("手机号码有误，请重填");  
			        return; 
			    } 
				$(".sms-btn").addClass("disabled");
// 				发送短信验证码
				api.tools.ajax({
					url:"<%=path%>/system/getValiCode.htm",
					data:{
						mobile:$("#mobile").val(),
						type:3
					}
				},function(d){
					api.tools.toast(d.resultMsg);
					if(d.resultCode != 1){
						$(".sms-btn").removeClass("disabled");
					}else{
						startTime();
					}
				});
			});
			var i = 60;
			function startTime(){
				$(".sms-btn").html("获取验证码("+i+"秒)");
				if(i > 0){
					i--;
					setTimeout(function(){
						startTime();
					},1000);
				}else{
					$(".sms-btn").html("获取验证码");
					$(".sms-btn").removeClass("disabled");
					i = 60;
				}
			}
			
			
			
			$(".tj").on("click",function(){
				if($(this).hasClass('disabled')) return;
				
				if($("#yCode").val().trim().length==0){
					api.tools.toast("验证码不能为空");
					 return;
				}
				if($("#pwds").val().trim().length==0){
					api.tools.toast("密码不能为空");
					 return;
				}
				if(!(/^.{6,20}$/.test($("#pwds").val()))){
				  api.tools.toast("请输入6~18位数的密码长度");
					 return;
				}
				if(!(/^\w+$/.test($("#pwds").val()))){
				      api.tools.toast("输入密码中有非法字符");
					return;
				}
					
				   
				
			
				$(this).addClass('disabled');
				$(this).html('注册中...');
				$this = $(this);
				$.ajax({
					type:"post",
					url:"<%=path%>/userInfoWx/register.htm",
					data:"mobile="+$("#mobile").val()+"&pwds="+$("#pwds").val()+"&yCode="+$("#yCode").val(),
					success:function(msg){
						console.log(msg);
						if(msg.resultCode == "1"){
							api.tools.toast("注册成功");            				
	           				 window.location.href = "<%=path%>/login/loanLogin.htm";
																return;
															} else {
																$this
																		.removeClass('disabled');
																$this
																		.html('立即注册');
																api.tools
																		.toast(msg.resultMsg);
															}

														},
														dataType : "json"
													});
										});

					});
</script>

</html>