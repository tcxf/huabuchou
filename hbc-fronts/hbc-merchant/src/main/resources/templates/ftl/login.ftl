<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>- 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

    <style>
        body {
            background: url(${basePath!}/img/img/shop_background.png) no-repeat;
            background-size: cover;
            background-position: center center;
            background-attachment: fixed;
        }

        .top-box {
            margin-top: 7%;
            text-align: center
        }

        .top-img {

            margin: 0 auto;

        }

        .top-img > img {
            width: 40rem;
        }

        .m-t {
            margin-top: 30px;
            position: relative;
        }

        .t-form {
            width: 32rem;
            height: 20rem;
            margin: -34px auto;
            padding: 20px;
            background: #fff;

        }

        .t-form .img1 {
            position: absolute;
            top: 5px;
            left: 4px
        }

        .t-form .img2 {
            position: absolute;
            top: 52px;
            left: 5px
        }

        .t-btn {
            width: 100%;
            height: 40px;
            line-height: 40px;
            background-color: #5695f1;
            color: #fff;
            text-align: center;
            font-size: 16px
        }
    </style>
</head>

<body>

<div class="top-box">
    <div class="top-img">
        <img src="${basePath!}/img/img/shop_header_background.png" alt=""/>
    </div>
</div>

<div class="t-form">
    <form class="m-t" role="form">
        <div class="form-group">
            <div style="line-height: 30px; width: 11%;">
                <img class="img1" src="${basePath!}/img/img/name.png" style="width: 20px;height: 20px">
            </div>
            <div class="wkt-flex">
                <input type="text" id="loginName" class="form-control reg-input" style="padding-left:25px"
                       placeholder="用户名">
            </div>
        </div>
        <div class="form-group">
            <div style="line-height: 30px; width: 11%;">
                <img class="img2" src="${basePath!}/img/img/password.png" style="width: 20px;height: 20px">
            </div>
            <div class="wkt-flex">
                <input type="password" id="pwd" class="form-control reg-input" style="padding-left:25px"
                       placeholder="密码">
            </div>
        </div>
        <div class="form-group">
            <input type="checkbox">
            记住用户名和密码
        </div>
        <div class="t-btn" id="loginButton">登 录</div>
    </form>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>

</body>
<script type="text/javascript">

    $(function () {
        //iframe页面session超时跳到顶层登陆页面
        sessionTimeOutExitIframe()
        $("#loginButton").click(function () {

            if (validLogin() == true) {
                loginAjaxSubmit();
            }
            console.log(2);
        });
    });

    function sessionTimeOutExitIframe() {
        if (window.parent.length > 0) {
            window.parent.location = "${basePath!}/login/gologin";
        }
    }

    function validLogin() {
        if ($("#loginName").val() == "") {
            layer.msg('请填写商户id或者手机号码');
            return false;
        }
        if ($("#pwd").val() == "") {
            layer.msg('请填写密码');
            return false;
        }
        return true;
    }

    function keyLogin() {
        if (event.keyCode == 13) {  //回车键的键值为13
            if (validLogin() == true) {
                loginAjaxSubmit();
            }
            console.log(2);
        }
    }

    function loginAjaxSubmit() {
        $.ajax({
            cache: false,
            type: "POST",
            url: "${basePath!}/login/logins",
            data: {loginName: $("#loginName").val(), pwd: $("#pwd").val(), type: 2},
            error: function (request) {
                layer.msg('登陆发生错误');
            },
            success: function (results) {
                if (results.code == 0) {
                    window.parent.location = "${basePath!}/page/index";
                } else {
                    layer.msg(results.msg);
                }
            }
        });
    }

    function validRestPwd() {
        if ($("#userNameForRest").val() == "") {
            jNotify("请输入用户名!", {
                ShowOverlay: false,
                MinWidth: 100,
                VerticalPosition: "bottom",
                HorizontalPosition: "right"
            });
            return false;
        }
        if ($("#regEmail").val() == "") {
            jNotify("请输入注册邮箱!", {
                ShowOverlay: false,
                MinWidth: 100,
                VerticalPosition: "bottom",
                HorizontalPosition: "right"
            });
            return false;
        }
        return true;
    }

    function loginRestPwdToggle(toogleFlag) {
        if (toogleFlag == 1) {
            $("#loginDiv").show();
            $("#resetPwdDiv").hide();
        } else {
            $("#resetPwdDiv").show();
            $("#loginDiv").hide();
        }
    }
</script>

</html>
