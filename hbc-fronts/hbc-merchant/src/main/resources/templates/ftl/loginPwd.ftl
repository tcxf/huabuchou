<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<%=path %>/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=path %>/css/Font-Awesome/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="<%=path %>/css/app.css" />
		<link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path %>/js/layer_mobile/need/layer.css" />
		</script>
		<title>修改登陆密码</title>
	    <style>
	        body,
	        html {
	            background: #eee;
	            height: 100%;
	        }
			.c-content{
				width:90%;
				border-radius: 10px;
				margin-left:5%;
			}
	        .tj{
	        	background: linear-gradient(to bottom,#21b8ff, #359afe); color:#ffffff; border-radius: 5px; font-size: 1rem; height: 50px; line-height: 50px; width: 95%; margin: 0 auto; margin-top: 50px;
	        }
	        .disabled{
	        	background:#ccc;
	        }
	    </style>
	</head>

	<body>
		<div class="c-content login-model">
		    <div class="webkit-box ">
		        <div style="line-height: 30px; width: 11%;border-right: 1px solid #cccccc">
		            <img src="<%=path %>/img/images/icon_password.png" />
		        </div>
		        <div class="wkt-flex">
		            <input type="password" id="oldpwd" class="form-control reg-input" placeholder="请输入原密码" style="width: 100%; height: 30px; padding-left: 14px; font-size: 14px;">
		        </div>
		        <div style="line-height: 30px;" class="clear-input">
		            <img src="<%=path %>/img/images/icon_delete.png" />
		        </div>
		    </div>
		
		    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
		    <div class="webkit-box ">
		        <div style="line-height: 30px; width: 11%;border-right: 1px solid #cccccc">
		            <img src="<%=path %>/img/images/icon_password.png" />
		        </div>
		        <div class="wkt-flex">
		            <input type="password" id="newpwd" class="form-control reg-input" placeholder="请输入新密码" style="width: 100%; height: 30px; padding-left: 14px; font-size: 14px;">
		        </div>
		        <div style="line-height: 30px;" class="clear-input">
		            <img src="<%=path %>/img/images/icon_delete.png" />
		        </div>
		    </div>
		
		    <div class="gwl" style="margin-top: 10px; margin-bottom: 20px;"></div>
		    <div class="webkit-box ">
		        <div style="line-height: 30px; width: 11%;border-right: 1px solid #cccccc">
		            <img src="<%=path %>/img/images/icon_password.png" />
		        </div>
		        <div class="wkt-flex">
		            <input type="password" id="renewpwd" class="form-control reg-input" placeholder="请再输入一遍新密码" style="width: 100%; height: 30px; padding-left: 14px; font-size: 14px;">
		        </div>
		        <div style="line-height: 30px;" class="clear-input">
		            <img src="<%=path %>/img/images/icon_delete.png" />
		        </div>
		    </div>
		</div>
		<div class="yzm-btn tj" style="">
		    完成
		</div>
	</body>
	<script type="text/javascript" src="<%=path %>/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
	<script type="text/javascript" src="<%=path %>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
	<script type="text/javascript" src="<%=path %>/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="<%=path %>/template/template.js"></script>
	<script type="text/javascript" src="<%=path %>/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="<%=path %>/js/picker/js/mui.poppicker.js"></script>
	<script>
		$(document).ready(function() {
			$(".clear-input").click(function(){
				$(this).parent().find("input").val("");
				$(this).parent().find("input").trigger("keyup");
				$(this).parent().find("input").focus();
			});
			
			$(".tj").click(function(){
				var oldpwd = $.trim($('#oldpwd').val());
				var newpwd = $.trim($('#newpwd').val());
				var renewpwd = $.trim($('#renewpwd').val());
				
				if(oldpwd.length < 6){
					api.tools.toast('原密码长度最少为6位字符');
// 					$("#oldpwd").focus();
					return;
				}
				
				var regex = /^[\u0391-\uFFE5\w]+$/;
				if(!regex.test(oldpwd)){
					api.tools.toast('原密码不能包含特殊字符');
// 					$("#oldpwd").focus();
					return;
				}

				if(newpwd.length < 6){
					api.tools.toast('新密码长度最少为6位字符');
// 					$("#newpwd").focus();
					return;
				}

				if(newpwd.length > 18){
					api.tools.toast('新密码长度最多为18位字符');
// 					$("#newpwd").focus();
					return;
				}
				
				if(!regex.test(newpwd)){
					api.tools.toast('新密码不能包含特殊字符');
// 					$("#oldpwd").focus();
					return;
				}

				if(renewpwd.length < 6){
					api.tools.toast('重复新密码长度最少为6位字符');
// 					$("#newpwd").focus();
					return;
				}

				if(renewpwd.length > 18){
					api.tools.toast('重复新密码长度最多为18位字符');
// 					$("#newpwd").focus();
					return;
				}
				
				if(!regex.test(renewpwd)){
					api.tools.toast('重复新密码不能包含特殊字符');
// 					$("#oldpwd").focus();
					return;
				}
				
				if(newpwd != renewpwd){
					api.tools.toast('输入的两个新密码不一致');
// 					$("#renewpwd").focus();
					return;
				}
				
				$('.tj').addClass('disabled');
				$('.tj').html('处理中...');
				api.tools.ajax({
					url:'<%=path%>/m/merchant/updateLoginPwd.htm',
					data:{
						oldpwd:oldpwd,
						newpwd:newpwd
					}
				},function(d){
					api.tools.toast(d.resultMsg);
					$('.tj').removeClass('disabled');
					$('.tj').html('完成');
					if(d.resultCode != -1){
						setTimeout(function(){
							api.tools.goback();
						},1500);
					}
				});
			});
		});
	</script>

</html>