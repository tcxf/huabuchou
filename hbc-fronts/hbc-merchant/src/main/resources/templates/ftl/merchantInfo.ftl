<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="<%=path %>/css/zy.css"/>
    <link rel="stylesheet" href="<%=path %>/css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=path %>/css/app.css" />
    <link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
    <link rel="stylesheet" href="<%=path %>/js/layer_mobile/need/layer.css" />
	<link rel="stylesheet" href="<%=path %>/js/picker/css/mui.picker.css" />
	<link rel="stylesheet" href="<%=path %>/js/picker/css/mui.dtpicker.css" />
	<link rel="stylesheet" href="<%=path %>/js/picker/css/mui.poppicker.css" />
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.4&ak=C65XoKSp1EVpgNTlrnk4A8fnIQGuS0z7"></script>
    <title>店铺资料</title>
    <style>
     html{
       	height:900px;
       }
       .a-content{
       	width:94%;
       	background:#fff;
       	border-radius: 10px;
       	margin-left:3%;
       }
        #top-yzm{
            margin-top: -200px;
            text-align: right;

        }
        .sms-btn{
            color: #000000 !important;
        }
        input{
            font-size: 14px
        }
         .dianpu-photo{
        	margin-top:20px;
        	width:100%;
        	height:100px;
        }
         .dianpu-photo li{
         	float:left;
         	width:50%;
         	text-align:center;
         	padding:10px;
         }
         .sj-ul li {
		    padding: 10px 0px 1px 10px;
		    position: relative;
		}
		.tj{
			background:linear-gradient(to bottom,#21b8ff, #359afe); 
			color:#ffffff; 
			font-size: 1rem; 
			height: 50px; 
			line-height: 50px; 
			width: 100%; 
			position: fixed;
			bottom:0
			
		}
		   #container{height:150px;width:100%;}
          #r-result{width:100%;}
    </style>
</head>
<body>
	<div class="a-content" style="margin-top: 0px; padding:0px;">
	    <ul class="sj-ul">
	    	
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    店铺简称:
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="simpleName" <%-- <c:if test="${m.authExamine != 0 && m.authExamine != 2 && (m.simpleName != null && m.simpleName != '')}"> UNSELECTABLE='true' onfocus="this.blur()"</c:if> --%> value="${m.simpleName}" class="form-control reg-input" placeholder="请填写店铺简称"
	                           style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
	                </div>
	            </div>
	        </li>
	         <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    店铺名称:
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="name"  value="${m.name}" class="form-control reg-input" placeholder="请填写店铺名称"
	                           style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
	                </div>
	            </div>
	        </li>
	         <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    法人证件号:
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="idCard"  value="${m.idCard}" class="form-control reg-input" placeholder="请填写法人证件号"
	                           style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
	                </div>
	            </div>
	        </li>
	         <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    法人名称:
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="legalName"  value="${m.legalName}" class="form-control reg-input" placeholder="请填写法人名称"
	                           style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
	                </div>
	            </div>
	        </li>
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    类型：
	                </div>
	                <div class="wkt-flex">
	                	<input type="hidden" id="micId" value="${m.micId}"/>
	                    <input type="tel"   onfocus="this.blur()" id="mcName" value="${m.mcName}" class="form-control reg-input" placeholder="餐饮"
	                           style="background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
	                </div>
	            </div>
	        </li>
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    开店时间：
	                </div>
	                <div class="wkt-flex">
	                    <input type="tel"  UNSELECTABLE='true' onfocus="this.blur()" id="openDate" value="${m.openDate}" name="phone" class="form-control reg-input" placeholder="2013-12-23"
	                           style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
	                </div>
	            </div>
	        </li>
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                   店铺电话：
	                </div>
	                <div class="wkt-flex">
	                    <input type="tel" id="phoneNumber"  value="${m.phoneNumber}" class="form-control reg-input" placeholder="请填写店铺电话"
	                           style="background-size:3%;font-size:14px;
	                         background-color:#fff;width: 95%; height: 30px; padding-left: 0px; text-align:right;">
	                </div>
	            </div>
	        </li>
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    位置：
	                </div>
	                <div class="wkt-flex">
	                	 <input type="hidden" id="areaId" value="${m.areaId}"/>
	                    <input type="tel" id="displayName" value="${m.displayName}" UNSELECTABLE='true' onfocus="this.blur()" class="form-control reg-input" placeholder="湖南省-长沙市-芙蓉区-白沙街道"
	                           style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
	               
	                </div>
	            </div>
	        </li>
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
	                    详细地址：
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="address" value="${m.address}" class="form-control reg-input" placeholder="请输入详细地址"
	                           style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
	                </div>
	            </div>
	        </li>
	       <li style=" padding: 10px 10px 10px 10px;display:none">
	           <div id="container"></div>
		        <div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
	        </li> 
	        <li>
	            <div class="webkit-box">
	                <div class="input-left-name">
						店铺介绍：
	                </div>
	                <div class="wkt-flex">
	                    <input type="text" id="recommendGoods"  value="${m.recommendGoods}" class="form-control reg-input" placeholder="请填写店铺介绍"
	                           style=" background-size:3%;font-size:14px;
								width: 95%; height: 30px; padding-left: 0px; text-align:right; margin-right:5%;">
	                </div>
	            </div>
	        </li>
	    </ul>
	</div>
	<div class="dianpu-photo" style="position: relative;">
		<ul >
			<li>
				<c:if test="${m.localPhoto != null}">
				    <img id="localPhoto" src="${res}${m.localPhoto}" style="width: 100%;height:130px;margin-top: 0; z-index=100; border-radius: 5px;" alt=""/>
						<img id="localPhoto" src="${hsj}/img/img/icon_addphotoshop.png" style="width: 25px;height:25px; top:50px;left:21%;z-index=1000; position: absolute; border-radius: 5px;" alt=""/>
						<p style="color:#fff;font-size:0.8rem;top:77px;left:18%;z-index=1000; position: absolute;">重新上传</p>
					</c:if>
					<c:if test="${m.localPhoto == null}">
				    <img id="localPhoto" src="${hsj}/img/img/icon_add_shop_photo.png" style="width: 30%;margin-top: 0;" alt=""/>
					<p style="font-size:0.8rem;color:#999;margin-top:2%">添加门头照片</p>
				</c:if>
			</li>
			<li>
				<c:if test="${m.licensePic != null}">
				    <img id="licensePic" src="${res}${m.licensePic}" style="width: 100%;height:130px;margin-top: 0; z-index=100; border-radius: 5px;" alt=""/>
					<img id="localPhoto" src="${hsj}/img/img/icon_addphotoshop.png" style="width: 25px;height:25px; top:50px;left:72%;z-index=1000; position: absolute; border-radius: 5px;" alt=""/>
						<p style="color:#fff;font-size:0.8rem;top:77px;left:69%;z-index=1000; position: absolute;">重新上传</p>
				</c:if>
				<c:if test="${m.licensePic == null}">
				    <img id="licensePic" src="${hsj}/img/img/icon_add_business_license.png" style="width:30%;margin-top: 0;" alt=""/>
					<p style="font-size:0.8rem;color:#999;margin-top:2%">添加营业执照</p>
				</c:if>	
			</li>
		</ul>
	</div>
	<c:if test="${m.authExamine == 0 || m.authExamine == 2}">
	<div class="yzm-btn tj" style="background:linear-gradient(to bottom,#21b8ff, #359afe); color:#ffffff; border-radius: 5px; font-size: 1rem; height: 50px; line-height: 50px; width: 95%; margin: 0 0.6rem;">
	   提交审核
	</div>
	</c:if>
	<c:if test="${m.authExamine == 3}">
	<div class="yzm-btn tj" style="background:linear-gradient(to bottom,#21b8ff, #359afe); color:#ffffff; border-radius: 5px; font-size: 1rem; height: 50px; line-height: 50px; width: 95%; margin: 0 0.6rem;">
	    保存
	</div>
	</c:if>
	<c:if test="${m.authExamine == 1}">
	<div class="yzm-btn " style="background:linear-gradient(to bottom,#21b8ff, #359afe); color:#ffffff; border-radius: 5px; font-size: 1rem; height: 50px; line-height: 50px;width: 95%; margin: 0 0.6rem;">
	    资料审核中
	</div>
	</c:if>
</body>
	<script type="text/javascript" src="<%=path%>/js/jquery.js"></script>
	<script type="text/javascript" src="<%=path%>/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/info.js"></script>
	<script type="text/javascript" src="<%=path %>/js/picker/js/mui.picker.js"></script>
	<script type="text/javascript" src="<%=path %>/js/picker/js/mui.dtpicker.js"></script>
	<script type="text/javascript" src="<%=path %>/js/picker/js/mui.poppicker.js"></script>
	<script type="text/javascript" src="<%=path %>/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script>
		$(document).ready(function() {
			/* var map;
			var lat = "${(m.lat == null || m.lat == '')?'39.90923':m.lat}";
			var lng = "${(m.lng == null || m.lng == '')?'116.397428':m.lng}";
			var sellat = "${(m.lat == null || m.lat == '')?'':m.lat}";
			var sellng = "${(m.lng == null || m.lng == '')?'':m.lng}";
			if(sellat != "" && sellat != ""){
				init();
			}else{
				function location(){
					mapObj = new AMap.Map('iCenter');
					mapObj.plugin(['AMap.Geolocation','AMap.Geocoder'], function () {
					    geolocation = new AMap.Geolocation({
					        enableHighAccuracy: true,//是否使用高精度定位，默认:true
					        timeout: 10000,          //超过10秒后停止定位，默认：无穷大
					        maximumAge: 0,           //定位结果缓存0毫秒，默认：0
					        convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
					        showButton: true,        //显示定位按钮，默认：true
					        buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
					        buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
					        showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
					        showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
					        panToLocation: true,     //定位成功后将定位到的位置作为地图中心点，默认：true
					        zoomToAccuracy:true      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
					    });
					    mapObj.addControl(geolocation);
					    AMap.event.addListener(geolocation, 'complete', function(d){
					    	 lat = d.position.lat;
					    	 lng = d.position.lng;
					    	 init();
					    });//返回定位信息
					    AMap.event.addListener(geolocation, 'error', function(d){
					    	 init();
					    });      //返回定位出错信息
					    geolocation.getCurrentPosition();
					});
					api.tools.location(function(d){
						lat = d.lat;
						lng = d.lng;
						init();
					},function(){
						init();
					});
				}
				location();
			}
			function init(){
		        map = new AMap.Map('container', {
		            center: [lng, lat],
		            zoom: 13
		        });
		        map.plugin(["AMap.ToolBar"], function() {
		            map.addControl(new AMap.ToolBar());
		        });
		        
		        if(sellat != "" && sellat != ""){
					new AMap.Marker({
		                position : new AMap.LngLat(sellng,sellat),
		                map : map
		            });
		        }
		        var _onClick = function(e){
		        	map.clearMap();
		        	
		        	sellat = e.lnglat.lat;
		        	sellng = e.lnglat.lng;
		            new AMap.Marker({
		                position : e.lnglat,
		                map : map
		            });
		        };
		        map.on('click', _onClick);//绑定
		    }
			
			init(); */
			
			 function G(id) {
		        return document.getElementById(id);
		    }

		    var map = new BMap.Map("container");
		    var point = new BMap.Point(116.331398,39.897445);
		    map.centerAndZoom(point,12);

		    function myFun(result){
		        var cityName = result.name;
		        map.setCenter(cityName);
		        /* alert("当前定位城市:"+cityName); */
		    } 
		    var myCity = new BMap.LocalCity();
		    myCity.get(myFun); // 初始化地图,设置城市和地图级别。
		    map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
		    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
		    var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
		            {"input" : "address"
		                ,"location" : map
		            });
		    ac.setInputValue("${m.address}")

		    ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
		        var str = "";
		        var _value = e.fromitem.value;
		        var value = "";
		        if (e.fromitem.index > -1) {
		            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		        }
		        str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

		        value = "";
		        if (e.toitem.index > -1) {
		            _value = e.toitem.value;
		            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		        }
		        str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
		        G("searchResultPanel").innerHTML = str;
		    });

		    var myValue;
		    ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
		        var _value = e.item.value;
		        myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		        G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

		        setPlace();
		    });
             var lng="";
             var lat="";
		    function setPlace(){
		        map.clearOverlays();    //清除地图上所有覆盖物
		        function myFun(){
		            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
		            map.centerAndZoom(pp, 18);
		            map.addOverlay(new BMap.Marker(pp));    //添加标注
		            console.log(pp);
		            lng=   pp.lng;
		            lat=   pp.lat;
		            console.log("lng:"+lng); 
		            console.log( "lat:"+lat); 
		          
		        }
		        var local = new BMap.LocalSearch(map, { //智能搜索
		            onSearchComplete: myFun
		        });
		        local.search(myValue);
		        
		    }
			
			
			var n = new Date();
			var dtPicker = new mui.DtPicker({
				type: 'date',
				beginDate: new Date(1900, 1, 1),
				endDate: new Date(n.getFullYear(), n.getMonth(), n.getDate())
			});
			$("#openDate").click(function() {
				dtPicker.show(function(s) {
					$("#openDate").val(s.y.text + "-" + s.m.text + "-" + s.d.text);
				});
			});

			
			var categoryList = new Array();
			<c:forEach items="${mcList}" var="e" varStatus = "i">
			categoryList[${i.index}] = {
				text:"${e.name}",
				value:"${e.id}"
			}
			</c:forEach>
			var categoryPicker = new mui.PopPicker();
			categoryPicker.setData(categoryList);
			$("#mcName").click(function(){
				categoryPicker.show(function(s) {
					$("#micId").val(s[0].value);
					$("#mcName").val(s[0].text);
				});
			});

			
			
			var pickerCity = new mui.PopPicker({
			    layer: 3
			});
			api.tools.ajax({
				url:'<%=path%>/js/area.json'
			},function(d){
				pickerCity.setData(d);
			});
			$("#displayName").click(function(){
				pickerCity.show(function(s) {
					$("#areaId").val(s[2].value);
					$("#displayName").val(s[0].text+s[1].text+s[2].text);
				});
			});
			var localPhoto = '';
			var licensePic = '';
			
			var option = ["scanQRCode", "uploadImage", "chooseImage"];
			api.tools.initWxJsSdk(option,
			function() {
				$("#localPhoto").click(function() {
					var $this = $(this);
					wx.chooseImage({
						count: 1,
						// 默认9
						sizeType: ['original', 'compressed'],
						// 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'],
						// 可以指定来源是相册还是相机，默认二者都有
						success: function(res) {
							var img = res.localIds[0]; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
							if (api.tools.isIos()) {
								wx.getLocalImgData({
									localId: img,
									success: function(r) {
										$this.attr("src", r.localData);
									}
								});
							} else {
								$this.attr("src", img);
							}
							wx.uploadImage({
								localId: img,
								// 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1,
								// 默认为1，显示进度提示
								success: function(res) {
									localPhoto = res.serverId; // 返回图片的服务器端ID
								}
							});
						}
					});
				});
				
				$("#licensePic").click(function() {
					var $this = $(this);
					wx.chooseImage({
						count: 1,
						// 默认9
						sizeType: ['original', 'compressed'],
						// 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'],
						// 可以指定来源是相册还是相机，默认二者都有
						success: function(res) {
							var img = res.localIds[0]; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
							if (api.tools.isIos()) {
								wx.getLocalImgData({
									localId: img,
									success: function(r) {
										$this.attr("src", r.localData);
									}
								});
							} else {
								$this.attr("src", img);
							}
							wx.uploadImage({
								localId: img,
								// 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1,
								// 默认为1，显示进度提示
								success: function(res) {
									licensePic = res.serverId; // 返回图片的服务器端ID
								}
							});
						}
					});
				});
			});
			
			$(".tj").click(function() {
				if ($(this).hasClass('disabled')) return;
				var simpleName = $.trim($("#simpleName").val());
				var micId = $.trim($("#micId").val());
				var openDate = $.trim($("#openDate").val());
				var areaId = $.trim($("#areaId").val());
				if(areaId == null || areaId== ""){
					areaId='${m.areaId}';  
				} 
				var displayName=$.trim($("#displayName").val());
				var address = $.trim($("#address").val());
				var phoneNumber = $.trim($("#phoneNumber").val());
				var name = $.trim($("#name").val());
				var idCard = $.trim($("#idCard").val());
				var legalName = $.trim($("#legalName").val());
			
				
				
				if(simpleName.length < 2){
					api.tools.toast('店铺简称至少需要2个字');
// 					$("#simpleName").focus();
					return;
				}
				if(simpleName.length > 15){
					api.tools.toast('店铺简称最多只能10个字');
// 					$("#simpleName").focus();
					return;
				}
				if(!isNaN(simpleName)){
					api.tools.toast('店铺简称不能为数字');
// 					$("#realName").focus();
					return;
				}
				
				var regex = /^[\u0391-\uFFE5\w]+$/;
				if(!regex.test(simpleName)){
					api.tools.toast('店铺简称不能包含特殊字符');
// 					$("#realName").focus();
					return;
				}
				
				if(name.length < 2){
					api.tools.toast('店铺名称至少需要2个字');
// 					$("#simpleName").focus();
					return;
				}
				if(name.length > 15){
					api.tools.toast('店铺名称最多只能10个字');
// 					$("#simpleName").focus();
					return;
				}
				if(!isNaN(name)){
					api.tools.toast('店铺名称不能为数字');
// 					$("#realName").focus();
					return;
				}
				
				var regex = /^[\u0391-\uFFE5\w]+$/;
				if(!regex.test(name)){
					api.tools.toast('店铺名称不能包含特殊字符');
// 					$("#realName").focus();
					return;
				}
				var regex = /^[\u0391-\uFFE5\w]+$/;
				if(!regex.test(idCard)){
					api.tools.toast('请输入正确的身份照号码');
// 					$("#realName").focus();
					return;
				}
				if(idCard.length < 17 ){
					api.tools.toast('请输入正确的身份照号码');
// 					$("#phoneNumber").focus();
					return;
				}
				if(legalName.length < 2){
					api.tools.toast('法人名称至少需要2个字');
				}
				if(legalName.length > 5){
					api.tools.toast('法人名称不得超过4个字');
				}
				
				
				
				
				if(micId == ''){
					api.tools.toast('请选择店铺类型');
// 					$("#micId").focus();
					return;
				}
				
				if(openDate == ''){
					api.tools.toast('请选择开店时间');
// 					$("#openDate").focus();
					return;
				}
				
				if(phoneNumber == ''){
					api.tools.toast('请填写店铺电话');
// 					$("#phoneNumber").focus();
					return;
				}
				
				if(phoneNumber.length > 15){
					api.tools.toast('店铺电话最多只能输入15个数字');
// 					$("#phoneNumber").focus();
					return;
				}
				
				if(areaId == ''){
					api.tools.toast('请选择所在地区');
// 					$("#areaId").focus();
					return;
				}
				
				if(address == ''){
					api.tools.toast('请填写详细地址');
// 					$("#address").focus();
					return;
				}
				if(address.length < 2){
					api.tools.toast('详细地址至少需要2个字');
// 					$("#address").focus();
					return;
				}
				if(address.length > 25){
					api.tools.toast('详细地址最多只能25个字');
// 					$("#address").focus();
					return;
				}
				
				/* if(sellat == '' && sellng == ''){
					api.tools.toast('请在地图上选择您的位置');
					return;
				}
				 */
				$(this).addClass('disabled');
				$(this).html('提交中...');
				$this = $(this);
				api.tools.ajax({
					url: "<%=path%>/m/merchant/updateBaseInfo.htm",
					data: {
						simpleName: simpleName,
						name :name,
						idCard:idCard,
						micId: micId,
						legalName:legalName,
						openDate: openDate,
						areaId: areaId,
					/* 	lng:lng,
						lat:lat, */
						address: address,
						localPhoto:localPhoto,
						licensePic:licensePic,
						recommendGoods:$("#recommendGoods").val(),
						phoneNumber:phoneNumber
					}
				},
				function(d) {
					api.tools.toast(d.resultMsg);
					$this.html('提交审核');
					$this.removeClass('disabled');
					if(d.resultCode != -1){
						setTimeout(function(){
							window.location = '<%=path%>/m/merchant/my.htm';
						},1500);
					}
				});
			});
		});
		
	</script>
</html>
