
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 基本表单</title>
<meta name="keywords" content="">
<meta name="description" content="">

<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="form-group"
		style="position: absolute;right:20px;top:20px;z-index: 99999;">
		<div>
			<input class="btn btn-white cancel" id="cancel" type="button"
				value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#base"
							aria-expanded="true"> 订单信息</a></li>
							
						<li><a data-toggle="tab" href="#payment" aria-expanded="true">支付记录</a></li>
					</ul>

					<div class="tab-content">
						<div id="base" class="ibox-content active tab-pane">
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单编号</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.serialNo!} </label>
								</div>
								
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单总额</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.tradeAmount!} </label>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠金额</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
                                    ${order.discountAmount!}</label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">下单用户</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
								${order.uid!}</label>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">用户手机</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.mobileStr!} </label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单状态</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										<#if order.paymentStatus == '1'>
										    已支付
											<#else>
											未支付
										</#if>
									</label>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label"> </label> <label
										class="col-sm-8 control-label" style="text-align: left;">

									</label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
						</div>
						<div id="payment" class="ibox-content tab-pane">
							<div class="ibox-content">
								<div class="project-list pager-list" id="hkjl-list">
									<div class="fixed-table-container form-group pager-content">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>支付编号</th>
													<th>支付方式</th>
													<th>支付金额</th>
													<th>支付时间</th>
													<th>支付状态</th>
												</tr>
											</thead>
											<tbody>
													<tr>
														<td>${order.serialNo!}</td>
														<#if order.tradingType == '0'><td>授信消费</td>
														<#else>
														<td>非授信消费</td>
														</#if>
														<td>¥${order.actualAmount!}</td>
														<td>${order.tradingDate!}</td>
														<#if order.paymentStatus == '1'><td>已支付</td>
														<#else>
														<td>未支付</td>
														</#if>
													</tr>
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<script>
			$(document).ready(function() {
				$(".cancel").click(function(){parent.closeLayer();});
			});
		</script>
</body>

</html>
