<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

<#include "common/common.ftl">
    <style>
        .dingdan-list {
            padding: 20px;
        }

        .dingdan-list .row {
            padding-bottom: 20px;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>商户订单管理</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="searchForm" class="form-inline">
                        <div class="form-group">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i
                                            class="fa fa-refresh"></i> 刷新
                                    </button>
                                </div>
                                <div class="col-sm-8">

                                </div>
                            </div>
                        </div>
                        <div class="dingdan-list">
                            <div class="container">
                                <div class="row">
                                    <div class="form-group col-md-4 col-lg-4 form-inline">
                                        <label class="control-label">下单用户:</label>
                                        <input type="text" name="uname" placeholder="请输入下单用户" class="form-control">
                                    </div>

                                    <!-- <div class="form-group col-md-4 col-lg-4">
                                        <label class="control-label">订单状态:</label>
                                            <select id="status" name="status">
                                                <option value="">全部</option>
                                                <option value="3">已完成</option>
                                                <option value="0">待支付</option>
                                                <option value="1">已支付</option>
                                                <option value="2">已取消 </option>
                                            </select>
                                    </div>
                                     -->
                                    <!--
                                   <div class="form-group col-md-3 col-lg-3">
                                       <label class="control-label">订单类型:</label>
                                           <select id="orderType" name="orderType">
                                               <option value="">全部</option>
                                               <option value="1">商品订单</option>
                                               <option value="2">扫码消费</option>
                                           </select>
                                   </div> -->
                                    <div class="form-group col-lg-1" style="text-align:center">
                                        <input type="button" id="search" class="btn btn-warning" value="搜索"/>
                                    </div>
                                </div>
                          <#--      <div class="row">
                                    <div class="form-group col-md-4 col-lg-4">
                                        <label class="control-label">优惠券:</label>
                                        <select id="coupon" name="coupon">
                                            <option value="">全部</option>
                                            <option value="1">使用</option>
                                            <option value="0">未使用</option>
                                        </select>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </form>
                    <div class="project-list" id="sj"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#parentId").select();
        $("#status").select();
        $("#orderType").select();

        var $pager = $("#sj").pager({
            url: "${basePath!}/order/findorderlist",
            formId: "searchForm",
            pagerPages: 3,
            template: {
                header: '<div class="fixed-table-container form-group">' +
                '<table class="table table-hover">' +
                '<thead>' +
                '<tr>' +
                '<th>商户名称</th>' +
                '<th>商户手机</th>' +
                '<th>下单用户</th>' +
                '<th>订单编号</th>' +
                '<th>订单总额</th>' +
                '<th>下单时间</th>' +
                '<th>订单状态</th>' +
                '<th>操作</th>' +
                '</tr>' +
                '</thead><tbody>',
                body: '<tr>' +
                '<td>{simpleName}</td>' +

                '<td>{mobile}</td>' +

                '<td>{realName}</td>' +

                '<td>{serialNo}</td>' +

                '<td>{tradeAmount}</td>' +

                '<td>{tradingDate}</td>' +

                '<td id="orderStatus_{id}">已完成</td>' +

                '<td>' +
                '<a data1="{tradingType}" data="{id}" class="btn btn-info btn-sm order-detail"><i class="fa fa-edit"></i> 查看 </a> ' +
                '</td>' +

                '</tr>',
                footer: '</tbody></table>',
                noData: '<tr><td colspan="7"><center>暂无数据</center></tr>'
            },
            callbak: function (result) {
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                    if (list[i].paymentStatus == 1){
                        $("#orderStatus_"+list[i].id).html('已支付');
                    } else {
                        $("#orderStatus_"+list[i].id).html('未支付');
                    }

                }

                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });

                $(".order-detail").click(function () {
                    var id = $(this).attr("data");
                    parent.openLayer('订单详情', '${basePath!}/order/findorderbyone?id=' + id , '1000px', '700px', $pager);
                });
            }
        });
    });

</script>

</body>
</html>
