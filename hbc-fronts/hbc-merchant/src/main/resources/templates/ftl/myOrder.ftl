<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />

    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="css/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="css/zy.css"/>
    <link rel="stylesheet" href="css/y.css"/>
    <script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="js/mui/css/mui.min.css" />
    <script type="text/javascript" src="js/mui/mui.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title></title>
    <style>
        .m_tab a{
            color: #666666;
        }
        .m_tab a:active{
            color:#2c683a;
        }
    </style>
</head>
<body>
<div class="m_tab">
    <ul class="webkit-box">
        <li class="active wkt-flex"><a href="#all" data-toggle="tab">全部</a></li>
        <li class="wkt-flex"><a href="#dfk" data-toggle="tab">待付款</a></li>
        <li class="wkt-flex"><a href="#yfk" data-toggle="tab">已付款</a></li>
        <li class="wkt-flex"><a href="#ywc" data-toggle="tab">已完成</a></li>
        <li class="wkt-flex"><a href="#yqx" data-toggle="tab">已取消</a></li>
    </ul>
</div>
<div class="m_content tab-content " style="margin-top: 2px">
    <div class="c_all tab-pane active" id="all">
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb;margin-top:-13px">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">马可波罗西餐厅</div>
                <div class="wkt-flex" style="text-align:right; line-height:30px;">
                    <span  style="color:#2c6839;margin-right: 8%">待付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/shopping5.png" style="width: 60px;height:60px;margin-left: 10px;margin-top: 8px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    澳大利亚进口牛排
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left:41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
    </div>
    <div class="c_all tab-pane" id="dfk">
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb;margin-top:-13px">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">马可波罗西餐厅</div>
                <div class="wkt-flex" style="text-align:right; line-height:30px;">
                    <span  style="color:#2c6839;margin-right: 8%">待付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/shopping5.png" style="width: 60px;height:60px;margin-left: 10px;margin-top: 8px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    澳大利亚进口牛排
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left:41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
    </div>

    <div class="c_all tab-pane" id="yfk">
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
    </div>
    <div class="c_all tab-pane" id="ywc">
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb;margin-top:-13px">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">马可波罗西餐厅</div>
                <div class="wkt-flex" style="text-align:right; line-height:30px;">
                    <span  style="color:#2c6839;margin-right: 8%">待付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/shopping5.png" style="width: 60px;height:60px;margin-left: 10px;margin-top: 8px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    澳大利亚进口牛排
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left:41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
    </div>
    <div class="c_all tab-pane" id="yqx">
        <dl>
            <dt>
            <div class="webkit-box" style="background: #ffffff;border-bottom: 1px solid #ebebeb">
                <div class="input-left-name" style="font-weight:normal; width:50%;margin-top: 2%;margin-left: 3%">三国时代公司</div>
                <div class="wkt-flex" style="text-align:right; line-height:44px;">
                    <span  style="color:#2c6839;margin-right: 8%">已付款</span>
                </div>
            </div>
            </dt>
            <dd class="row" style="background: #ffffff">
                <div class="col-xs-3">
                    <img src="img/img/icon_kuaisusaoma.png" style="width: 80px;height: 80px">
                </div>
                <div class="col-xs-9" style="margin-top:5px;line-height: 30px;font-size: 16px">
                    快速扫码商品消费
                    <p style="color: #ff5f57;font-size: 16px">￥1200.00
                        <span style="margin-left: 41px;color: #a0a0a0;font-size: 14px">2017/01/14 17:00</span>
                    </p>
                </div>
            </dd>
        </dl>
    </div>
</div>


</body>
</html>