
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
<#include "common/common.ftl">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>优惠券配置信息</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
								<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value=""/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠券名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,maxLength[20],loginName" validate-msg="required:优惠券名称不能为空,loginName:优惠券名称不能有特殊符号" id="name" name="name" value="" maxlength="30" placeholder="请填写优惠券名称">
									</div>
								</div>

							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">满足金额</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,money,maxLength[7]" validate-msg="required:请输入满足金额,money:数字小数点后不能超过两位,maxLength:金额长度不能超过8位数字" id="fullMoney" name="fullMoney" value="" maxlength="8" placeholder="请输入满足金额">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠金额</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,money,maxLength[6],largeThan[#fullMoney]" validate-msg="required:请输入优惠金额,money:数字小数点后不能超过两位,maxLength:金额长度不能超过7位数字,largeThan:金额不能大于原价" id="money" name="money" value="" maxlength="7" placeholder="请输入优惠金额">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结束类型</label>
									<div class="col-sm-8">
										<select class="dfinput" name="timeType" id="timeType" style="width:200px;">
											<option value="1" >固定结束时间</option>
											<option value="2">从领取日起计算</option>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结束时间</label>
									<div class="col-sm-8">
										<input type="text" style="display:none;" class="form-control" validate-rule="required,number" 
										validate-msg="required:请输入持续天数,number:天数必须为数字" id="days" name="days" value="" placeholder="请输入持续天数">
										<input type="text" style="background-color:#ffffff" readonly class="form-control" validate-rule="required" 
										validate-msg="required:请选择结束时间," id="endDate" name="endDate" value="" placeholder="请选择结束时间">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
			$("select").select();
    		laydate({
    			elem: '#endDate', 
    			event: 'click', //触发事件
    		  	format: 'YYYY-MM-DD' //日期格式
    		});
            //$("#timeType").select();
            $("#timeType").change(function(){
            	if($(this).val() == 1){
            		$("#days").hide();
            		$("#endDate").show();
            	}else{
            		$("#days").show();
            		$("#endDate").hide();
            	}
            });
            $("#timeType").trigger('change');
            $("#cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
                var name = $("#name").val();
                var fullMoney = $("#fullMoney").val();
                var money = $("#money").val();
                var name = $("#name").val();
                var type =  $("#timeType").val();
                var days =  $("#days").val();
                var endDate =  $("#endDate").val();
                if(name.length==0){
                    parent.messageModel("请添加优惠券名称");
                    return;
                }
                if(fullMoney.length==0){
                    parent.messageModel("请填写优惠券满足金额");
                    return;
                }
                if(money.length==0){
                    parent.messageModel("请填写优惠券优惠金额");
                    return;
                }
                if(type==1){
                    $("#timeType1").attr("value","1");
                    if(endDate.length==0){
                        parent.messageModel("请填写到期日期");
                        return;
                    }
                }else{
                    $("#timeType1").attr("value","2");
                    if(days.length==0){
                        parent.messageModel("请填写可用天数");
                        return;
                    }
                }
                var st = ${zt.status!};
                if (st != 1){
                    parent.messageModel("商户审核不通过，不能创建优惠券");
                    return;
				}
                var money=  parseFloat($("#money").val());
                var fullMoney=parseFloat($("#fullMoney").val());
                if(money >= fullMoney){
                    parent.messageModel("优惠金额不能大于满足金额");
                    return;
                }

    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", false);
    			var index = layer.load(2, {time: 10*1000});
    			$.ajax({
    				url:'${basePath!}/red/savered',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:$("#_f").serialize(),
    				success: function(data){ //成功
    					layer.close(index);
    					var obj = data;
    					if (obj.code == 0) {
    						//消息对话框
    						parent.messageModel("保存成功!");
    						parent.c.gotoPage(null);
    						parent.closeLayer();
    					}else  {
                            parent.messageModel(obj.msg);
                        }
    				}
    			});
    		});
        });
    </script>
</body>

</html>
