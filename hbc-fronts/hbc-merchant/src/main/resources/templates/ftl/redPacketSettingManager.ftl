
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 项目</title>
<meta name="keywords" content="">
<meta name="description" content="">

<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>优惠券配置管理</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new"
								class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>
								新建优惠券配置</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn"
											class="btn btn-primary btn-sm">
											<i class="fa fa-refresh"></i> 刷新
										</button>
									</div>
									<div class="col-sm-8"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">优惠券配置名称</label>
								<input type="text" name="name" placeholder="请输入优惠名称"
									id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning"
								value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script type="text/javascript">
		$(document).ready(function() {
    $("#parentId").select();
    var $pager = $("#data").pager({
        url: "${basePath!}/red/findred",
        formId: "searchForm",
        pagerPages: 3,
        template: {
            header: 
            	'<div class="fixed-table-container form-group">' + 
            		'<table class="table table-hover">' + 
            			'<thead>' + 
            				'<tr>' +
                                 '<th>名称</th>' +
            					'<th>满多少</th>' +
                                 '<th>减多少</th>' +
                                '<th>创建时间</th>' +
            					'<th>结束时间</th>' +
                                '<th>状态</th>' +
				                '<th>操作</th>' +
            				'</tr>' + 
            			'</thead><tbody>',
            body: 
            	'<tr>' +
                    '<td>{name}</td>' +
            		'<td>{fullMoney}</td>' +
                    '<td>{money}</td>' +
                      '<td>{createDate}</td>' +
            		'<td id="endDate_{id}"></td>' +
                    '<td id="status_{id}"></td>' +
            		'<td style="width:200px;">' + 
            			'<a href="javascript:void(0);" id="bj_{id}" data="{id},{descInfo}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> ' +
            			'<a href="javascript:void(0);" id="sx_{id}" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> 失效 </a> '
            		+ '</td>' + 
            	 '</tr>',
            footer: '</tbody></table>',
            noData: '<tr><td colspan="6"><center>暂无数据</center></tr>'
        },
        callbak: function(result) {
            var list = result.data.records;
            for (var i = 0; i < list.length; i++) {
                if (list[i].timeType == 1) {
                    $('#endDate_' + list[i].id).html(list[i].endDate);
                } else {
                    $('#endDate_' + list[i].id).html('请领取日起可用：' + list[i].days + '天');
                }
                if (list[i].status == 1){
                    $('#status_' + list[i].id).html("审核中");
                    $("#sx_" + list[i].id).hide();
				}else if(list[i].status == 2){
                    $('#status_' + list[i].id).html("审核已通过");
                    $("#bj_" + list[i].id).hide();
                    $("#sx_" + list[i].id).show();
				}
                else if(list[i].status == 3){
                    $('#status_' + list[i].id).html("已失效");
                    $("#bj_" + list[i].id).hide();
                    $("#sx_" + list[i].id).hide();
                }
            }

            //删除事件
            $(".remove").click(function() {
                //删除数据请求
                var param = 'id=' + $(this).attr("data-id");
                var bo = parent.deleteData('${basePath!}/red/redcouponFailure?id=' + id, param, $pager);
            })

            //编辑页面
            $(".edit").click(function() {
                var title = $(this).attr("data").split(",")[1];
                var id = $(this).attr("data").split(",")[0];
                parent.openLayer('修改优惠券配置-' + title, '${basePath!}/red/goupred?id=' + id, '1000px', '700px', $pager);
            });

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
            });
            // check 全部选中
            $('.checks').on('ifChecked',
            function(event) {
                $("input[id^='check-id']").iCheck('check');
            });

            // check 全部移除
            $('.checks').on('ifUnchecked',
            function(event) {
                $("input[id^='check-id']").iCheck('uncheck');
            });
        }
    });
   

    $("#loading-example-btn").click(function() {
        $pager.gotoPage(1);
    });

    //打开新增页面
    $("#new").click(function() {
        parent.openLayer('添加优惠券配置', '${basePath!}/red/gosavered', '1000px', '700px', $pager);
    });
});
	</script>

</body>
</html>
