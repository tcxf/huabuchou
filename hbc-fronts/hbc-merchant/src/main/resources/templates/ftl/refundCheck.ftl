<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">
	<title>- 项目</title> 
	<link href="<%=path%>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	 
    <style>
        #p-content{ 
            width: 600px; 
            height: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow:10px 10px 10px 10px #dadada;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 13px;
            font-weight: bold;
        }
        .p-dz{
            margin-top: 10px;
        }
        #p-content .p-footer{
            margin-top: 30%;  
        }
    </style>
</head>
<body>
<form>  
    <div id="p-content">
            <div class="form-group">
                <label>是否通过</label> 
                <div>
                    <select class="form-control" id="checkStauts" onchange="change();" name="checkStauts">
						<option  value="true">通过</option>
						<option value="false">不通过</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none" id="content">
                <label>驳回信息:</label>
                <div> 
                	<textarea placeholder="请输入驳回信息"   id=failureMsg name="failureMsg" rows="15" cols="70"></textarea>
                </div>   
                <div class="p-dz" id='sjje'></div> 
            </div>  
            <div class="">
                <button type="button" onclick="check()" class="btn btn-info btn-block" id="tj">完成</button>
            </div>    
    </div> 
    </form>  
    <script type="text/javascript">
    
    var flag=true;  
    function change(){
  	  	if(flag){ 
     		$("#content").css("display","block");  
 	    	flag=false;
     	}else{ 
 	    	$("#content").css("display","none");//隐藏div
     		flag=true;
     	}
    	  
    }    
    function check(){
    	var cStauts=$("#checkStauts option:selected").val();
    	var id='${tid }';
	 	$.ajax({ 
			type:"post",
			url:"<%=path%>/refund/check.htm",
			data:{
				tid : id, 
				checkStauts : cStauts,
				failureMsg : $("#failureMsg").val() 
			},
			   success:function(msg){
				   parent.messageModel(msg.resultMsg); 
				   /* var index = layer.load(2, {time: 10*1000});
				   layer.close(index);  */ 
				   // parent.c.gotoPage(null); 
				   parent.closeLayer();     
					if(msg.resultCode == "1"){  
						window.location.href = "<%=path%>/page/index.htm";     
							return; 
					}
			},
			dataType : "json"
		});   
    } 
    </script>
  
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
	
</body>
</html>
