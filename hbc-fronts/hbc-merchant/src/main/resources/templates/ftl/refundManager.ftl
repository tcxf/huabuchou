<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 项目</title>
<meta name="keywords" content="">
<meta name="description" content="">

<link rel="shortcut icon" href="favicon.ico">
<link href="<%=path%>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="<%=path%>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="<%=path%>/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<%=path%>/css/animate.css" rel="stylesheet">
<link href="<%=path%>/css/style.css?v=4.1.0" rel="stylesheet">
<link href="<%=path%>/css/auth.css?v=4.1.0" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>退款列表</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3>
										交易总额：¥ <span id="totalAmount">--</span> 元
									</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">退款时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate"
												id="startDate" style="width:100%" maxlength="30"
												placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate"
												id="endDate" style="width:100%" maxlength="30"
												placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户名称</label>
									<div class="col-sm-6">
										<input type="text" id="uname" class="form-control"
											  name="uname" maxlength="30" placeholder="请填写用户名称">    
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									 <label class="col-sm-3 control-label">类型</label> 
										<div class="col-sm-9"> 
											<select id="tradeType" name="refundStatus"> 
											<option value="">全部</option> 
												<option value="0">退款中</option> 
												<option value="1">已退款</option>
												<option value="2">审核中 </option>
												<option value="4">退款驳回</option>
											</select> 
									</div> 
								</div>
<!-- 								<div class="form-group col-sm-6 col-lg-4">  -->
<!-- 								   <label class="col-sm-3 control-label">商品分类</label>  -->
<!-- 										<div class="col-sm-9">  -->
<!-- 											<select id="mmname" name="mmname" >  -->
<!-- 											<option value="">全部</option> -->
<!-- 											<c:forEach items="${List}" var="i"> -->
<!-- 											<option id="iname" value="${i.id}">${i.name}</option> -->
<!-- 											</c:forEach> -->
<!-- 											</select>  -->
<!-- 										</div>  -->
<!-- 								</div> 						 -->
							</div>
							<div>
								<div class="form-group col-sm-12"
									style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button"
										value=" 查 询 " />  <input id="ccs" class="btn btn-info"
										type="button" value=" 下 载 报 表 " /> 
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- 全局js -->
	<script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path%>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path%>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#ccs").click(function(){
			var tradeType = $("#tradeType option:selected").val();
			window.location.href = "<%=path%>/refund/daochu.htm?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&uname="+$("#uname").val()+"&refundStatus="+tradeType;
		}); 
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#tradeType").select();
		var $pager = $("#data").pager({
	
			url:"<%=path%>/refund/refundManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+ 
							'<th>用户名称</th>'+
							'<th>用户手机</th>'+
							'<th>交易金额</th>'+
							'<th>交易时间</th>'+
							'<th>退款金额</th>'+
							'<th>退款状态</th>'+
							'<th>退款时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{uname}</td>'+
							'<td>{mobile}</td>'+
							'<td>{amount}</td>'+
							'<td>{tradingDateStr}</td>'+
							'<td>{amount}</td>'+
							'<td id="status_{id}">{refundStatus}</td>'+
							'<td>{applyRefundDate}</td>'+
							'<td id="opera_{id}"><a href="javascript:void(0);" data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-list-ul"></i> 查看明细 </a> </td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].refundStatus == 0){
						$("#status_"+list[i].id).html("退款中");
					}else if(list[i].refundStatus == 1){
						$("#status_"+list[i].id).html("已退款");
					}else if(list[i].refundStatus == 2){
						$("#status_"+list[i].id).html("审核中");
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm sure"><i class="fa fa-check"></i> 退款审核</a>');
					}else if(list[i].refundStatus == 4){
						$("#status_"+list[i].id).html("退款驳回");
					}else{
						$("#status_"+list[i].id).html("初始状态");
					}
				}
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				$(".tlog").click(function(){ 
					var id = $(this).attr("data");
					parent.openLayer('订单详情','<%=path%>/refund/orderDetail.htm?tid='+id,'1000px','700px',$pager);
				});
				 
				$(".sure").click(function(){
					var tid=$(this).attr("data");  
					<%-- window.location='<%=path%>/refund/goCheck.htm?tid='+tid;  --%> 
					 parent.openLayer('审核','<%=path%>/refund/goCheck.htm?tid='+tid,'300px','50px');
				});   
				loadAmount();
			}
		});
		
		function loadAmount(){
			$.ajax({
				url:"<%=path%>/refund/totalAmount.htm",
									type : 'POST', //GET
									data : $("#searchForm").serialize(),
									timeout : 30000, //超时时间
									dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
									success : function(d) {
										$("#totalAmount").html(d.data);
									}
								});
							}
 
							$("#loading-example-btn").click(function() {
								$pager.gotoPage($pager.pageNumber);
							});

							//删除多个内容
							$(".deleteCheck")
									.click(
											function() {
												var ids = "";
												$("input[name='checkbox-name']")
														.each(
																function() {
																	if (this.checked == true
																			&& $(
																					this)
																					.val() != "-1") {
																		ids += $(
																				this)
																				.val()
																				+ ",";
																	}
																});
												if (ids.length <= 0) {
													parent
															.messageModel("请选择您要删的内容");
												}

												if (ids.length > 0) {
													//删除数据请求
													var param = 'id=' + ids;
													var bo = parent
															.deleteData(
																	'${hsj}/tradingDetail/delete.htm',
																	param,
																	$pager);
												}
											});
						});
	</script>
 
</body>
</html>
