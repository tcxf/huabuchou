<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<%=path %>/css/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<%=path %>/css/Font-Awesome/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="<%=path %>/css/app.css" />
		<link rel="stylesheet" href="<%=path %>/css/zy.css"/>
		<link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path %>/js/layer_mobile/need/layer.css" />
		</script>
		<title>设置</title>
	    <style>
	        .body{
	            background: #f5f5f5;
	        }
	        .textright{
	            text-align: right;margin-right: 4%
	        }
	        .textright>img{
	           width: 7px;
	           height:10px;
	        }
	        .tj{
	        	background:linear-gradient(to bottom,#21b8ff, #359afe);  color:#ffffff; border-radius: 5px; font-size: 1rem; height: 50px; line-height: 50px; width: 95%; margin-left:2.5%; position: fixed;bottom: 20px
	        }
	    </style>
	</head>

	<body>
		<div class="c-content" style="margin-top: 0px; padding:0px;">
		    <ul class="sj-ul">
		        <li style="padding: 10px 0px 1px 10px;" class="redirect" url="<%=path%>/m/merchant/loginPwd.htm">
		            <div class="webkit-box">
		                <div class="input-left-name">
		                    修改登陆密码
		                </div>
		                <div class="wkt-flex textright" style=" margin-right:3%;">
		                    <img src="<%=path %>/img/img/icon_enter_small.png" alt=""/>
		                </div>
		            </div>
		        </li>
		        <li style="padding: 10px 0px 1px 10px;" class="redirect" url="<%=path%>/m/merchant/payMentPwd.htm">
		            <div class="webkit-box">
		                <div class="input-left-name">
		                    修改支付密码
		                </div>
		                <div class="wkt-flex textright" style=" margin-right:3%;">
		                    <img src="<%=path %>/img/img/icon_enter_small.png" alt=""/>
		                </div>
		            </div>
		        </li>
		    </ul>
		</div>
		<div class="yzm-btn tj logout">退出</div>
	</body>
	<script type="text/javascript" src="<%=path %>/js/mui/mui.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
	<script type="text/javascript" src="<%=path %>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
	<script type="text/javascript" src="<%=path %>/js/echarts.simple.min.js"></script>
	<script type="text/javascript" src="<%=path %>/template/template.js"></script>
	<script>
		$(document).ready(function() {
			$(".logout").click(function(){
				window.location = "<%=path%>/login/logout.htm";
			});
		});
	</script>

</html>