<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
	<#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>交易明细<a href="javascript:history.back(-1)">返回上一级</a></h5>
                </div>
                <div class="ibox-content">
                    <form id="searchForm" class="form-horizontal">
                        <div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                    <div class="project-list pager-list" id="data"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var context = "${basePath!}";
        var context2 = "${basePath!}";

        var $pager = $("#data").pager({
            url: "${basePath!}/order/findjymx?id=${id}",
            formId: "searchForm",
            pagerPages: 3,
            template: {
                header: '<div class="fixed-table-container form-group pager-content">' +
                '<table class="table table-hover">' +
                '<thead>' +
                '<tr>' +
                '<th>结算单号</th>' +
                '<th>实际支付金额</th>' +
                '<th>下单用户</th>' +
                '<th>支付时间</th>' +
                '<th>交易类型</th>' +
                '</tr>' +
                '</thead><tbody>',
                body: '<tr>' +
                '<td>{serialNo}</td>' +
                '<td>{actualAmount}</td>' +
                '<td>{realName}</td>' +
                '<td>{createDate}</td>' +
                '<td id="tradingType_{id}"></td>' +
                '</tr>',
                footer: '</tbody></table>',
                noData: '<tr><td colspan="10"><center>暂无数据</center></tr>'
            },
            callbak: function (result) {
                var list = result.data.records;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].tradingType == '0') {
                         $('#tradingType_' + list[i].id).html("授信消费");
                    } else if (list[i].tradingType == '1') {
                         $('#tradingType_' + list[i].id).html("非授信消费");

                    }
                }

            }
        });

        $("#loading-example-btn").click(function () {
            $pager.gotoPage($pager.pageNumber);
        });
    });

</script>

</body>
</html>
