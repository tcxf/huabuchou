<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <#include "common/common.ftl">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <!-- <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
                    <li><a data-toggle="tab" href="#uncreate" aria-expanded="true"> 未生成结算单</a></li> -->
                </ul>
                <div class="tab-content">
                    <div id="base" class="ibox-content active tab-pane">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>授信结算管理</h5>
                            </div>
                            <div class="ibox-content">
                                <form id="searchForm" class="form-horizontal">

                                    <div class="row m-b-sm m-t-sm">
                                        <div class="col-md-1">
                                            <a>   <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
                                        </div>
                                        <div class="col-sm-8">

                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">结算时间</label>
                                            <div class="col-sm-9">
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">交易单号</label>
                                            <div class="col-sm-9">
                                                <input id="mname" type="text" class="form-control" name="mname" maxlength="30" placeholder="请填写交易单号">
                                            </div>
                                        </div>


                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">结算状态</label>
                                            <div class="col-sm-9">
                                                <select id="status" name="status">
                                                    <option value="">全部</option>
                                                    <option value="0">待确认</option>
                                                    <option value="1">待结算</option>
                                                    <option value="2">已结算</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div>
                                        <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                            <input class="btn btn-success" id="search" type="button" value=" 查 询 " />
                                          <a href="${basePath!}/fund/settlementDetail/SettlementPoi">  <input id="" class="btn btn-info" type="button" value=" 下 载 报 表 " /></a>
                                        </div>
                                    </div>
                                </form>
                                <div style="clear:both;"></div>
                                <div class="project-list pager-list" id="data"></div>
                            </div>
                        </div>
                    </div>
                    <div id="uncreate" class="ibox-content  tab-pane">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>结算明细</h5>
                            </div>
                            <div class="ibox-content">
                                <form id="searchForm" class="form-horizontal">
                                    <div>
                                        <div class="form-group col-sm-6">
                                            <h3>待结算总额：¥ <span id="ua">--</span> 元</h3>
                                        </div>
                                    </div>
                                </form>
                                <div style="clear:both;"></div>
                                <div class="project-list pager-list" id="data-uncreate"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#status").select();
            laydate({
                elem: '#startDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });
            var $pager = $("#data").pager({
                url:"${basePath!}/order/findsettlementlist",
                formId:"searchForm",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>交易单号</th>'+
                    '<th>所属运营商</th>'+
                    '<th>出账时间</th>'+
                    '<th>结算时间</th>'+
                    '<th>结算状态</th>'+
                    '<th>商户结算</th>'+

                    '<th>操作</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{simpleName}</td>'+
                    '<td>{loopStart}</td>'+
                    '<td>{loopEnd}</td>'+
                    '<td id="status_{id}"></td>' +
                    '<td>{oamount}</td>'+
                    '<td><a href="javascript:void(0);"  data="{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 交易明细 </a> </td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    var list = result.data.records;
                    for (var i = 0; i < list.length; i++) {
                        if(list[i].status == 0){
                            $('#status_' + list[i].id).html("待确认");
                        }else if (list[i].status == 1){
                            $('#status_' + list[i].id).html("待结算");
                        }else {
                            $('#status_' + list[i].id).html("已结算");
                        }

                    }
                    $("#status").select();
                    $(".detail").click(function () {
                        var id=$(this).attr("data");
                        window.location.href="${basePath!}/order/findsettlementlistmx?id="+id;
                    })
                }
            });

            $("#loading-example-btn").click(function(){
                $pager.gotoPage($pager.pageNumber);
            });

        });

    </script>

</body>
</html>
