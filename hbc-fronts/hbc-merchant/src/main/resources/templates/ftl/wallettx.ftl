<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">

    <title></title>
    <style>

        #p-content {
            width: 600px;
            height: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow: 10px 10px 10px 10px #dadada;
        }

        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 13px;
            font-weight: bold;
        }

        .p-dz {
            margin-top: 10px;
        }

        #p-content .p-footer {
            margin-top: 50%;
        }
    </style>
  <#include "common/common.ftl">
</head>
<body>
<div id="p-content">
    <form id="_f">
        <div class="form-group">
            <label>提现金额</label>
            <div>
                <input type="text" placeholder="请输入金额" width="50%" id='txje' name="txje" class="form-control"
                       validate-rule="txmoney,txmoneyComperae[${w.totalAmount!}]" value=""/>
                <span>可提现金额：${w.totalAmount!}</span>
            </div>
            <div class="p-dz" id='sjje'></div>
            <div class="p-dz" id='sxje'></div>
        </div>
        <div class="form-group">
            <label>请选择银行卡</label>
            <div>
                             <#if b?exists && (b?size > 0) >
                                 <input type="hidden" id="cardListEmpty" value="no"/>
                                 <select class="form-control" id="micId" name="micId">
               <#list b as b>
                   <option value="${b.id}">${b.bankName!} ${b.bankCard!?substring((b.bankCard)?length-4,(b.bankCard)?length)} </option>
               </#list>
                                 </select>
                             <#else>
              <span>暂无绑卡</span>
                <input type="hidden" id="cardListEmpty" value="yes"/>
                             </#if>
            </div>
        </div>
        <div class="p-footer">
            <button type="button" class="btn btn-info btn-block" id='wc'>下一步</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var list =JSON.parse('${c!}');
        var sjje = null;
        var sjtxje =null;
        var txsxje =null;
        $('#txje').bind('input propertychange', function () {
            if ($(this).val().length == 0) {
                $("#sjje").html("实际到账：￥0");
            } else {
                sjje = $(this).val();
                for (var i = 0; i <list.length ; i++) {
                    if (list[i].maximumIntervalValue == ""){
                        var max= -1;
                    } else {
                        var max = parseInt(list[i].maximumIntervalValue);
                    }
                    var min=    parseInt(list[i].minimumIntervalValue);
                    if (max == -1) {
                        if (sjje >= min) {
                            if (list[i].presentType == '1') {
                                txsxje = sjje * (list[i].presentContent / 100);
                                txsxje =    Math.round(txsxje * 100) / 100;
                                sjtxje = sjje - txsxje;
                                sjtxje =    Math.round(sjtxje * 100) / 100;
                                $("#sjje").html("实际到账：￥" + sjtxje);
                                $("#sxje").html("提现手续费：￥" + txsxje);
                                return;
                            } else if (list[i].presentType == '2') {
                                sjtxje= sjje - list[i].presentContent;
                                sjtxje =    Math.round(sjtxje * 100) / 100;
                                txsxje = list[i].presentContent;
                                $("#sjje").html("实际到账：￥" + sjtxje);
                                $("#sxje").html("提现手续费：￥" + txsxje);
                                return;
                            }
                        }
                    }else {
                        if (sjje <= max) {
                            if (sjje >= min) {
                                if (list[i].presentType == '1') {
                                    txsxje = sjje * (list[i].presentContent / 100);
                                    txsxje =    Math.round(txsxje * 100) / 100;
                                    sjtxje = sjje - txsxje;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                } else if (list[i].presentType == '2') {
                                    sjtxje   = sjje - list[i].presentContent;
                                    sjtxje =    Math.round(sjtxje * 100) / 100;
                                    txsxje = list[i].presentContent;
                                    $("#sjje").html("实际到账：￥" + sjtxje);
                                    $("#sxje").html("提现手续费：￥" + txsxje);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        });


        $("#wc").click(function () {
            var options=$("#micId option:selected");
            var id = options.val();
            var txje = $("#txje").val();
            var balance='${w.totalAmount?c}'; //余额
            if(txje==null || txje=="" ){
                parent.messageModel('请输入提现的金额');
                return;
            }
            if(parseFloat(txje) <= 2){
                parent.messageModel('提现的金额不能少于3');
                return;
            }
            if(parseFloat(txje) > parseFloat(balance)){
                parent.messageModel('余额不足');
                return;
            }
            if ( id !="" && typeof(id) !="undefined") {
                window.location.href = '${basePath!}/mwallet/findtxdx?id=' + id + '&txje=' + txje + '&sjtxje=' + sjtxje+ '&txsxje=' + txsxje;
            } else {
                parent.messageModel("暂未绑卡，请先去绑卡");
            }
        });

    });

</script>
</body>
</html>
