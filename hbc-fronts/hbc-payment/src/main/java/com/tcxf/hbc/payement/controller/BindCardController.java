package com.tcxf.hbc.payement.controller;

import com.tcxf.hbc.common.bean.config.PayUrlPropertiesConfig;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.BankUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.secure.Base64;
import com.tcxf.hbc.payement.feign.BindCarSercice;
import com.tcxf.hbc.payement.feign.ITbUserAccountService;
import com.tcxf.hbc.payement.feign.PayService;
import com.tcxf.hbc.payement.model.dto.QuickPayAgreeapplyDto;
import com.tcxf.hbc.payement.model.dto.QuickPayAgreeconfirmResData;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/bindCard")
public class BindCardController {
    private static final Logger logger = LogManager.getLogger(BindCardController.class);

    @Autowired
    PayUrlPropertiesConfig payUrlPropertiesConfig;
    @Autowired
    private BindCarSercice bindCarSercice;

    @Autowired
    private PayService payService;
    @Autowired
    private ITbUserAccountService bbUserAccountService;

    /**
     * 查找已经绑定银行卡
     * @param tbBindCardInfo
     * @return
     */
    @RequestMapping("/findbank")
    @ResponseBody
    public R findbank(@RequestBody TbBindCardInfo tbBindCardInfo){
        TbUserAccount tbUserAccount = bbUserAccountService.selectUserAccount(tbBindCardInfo.getOid()).getData();
        return  bindCarSercice.findbank(tbUserAccount.getId(),TbBindCardInfo.CONTRACTTYPE_2);
    }

    /**
     * 跳转到绑卡界面
     * @param request
     * @return
     */
    @RequestMapping("/goToBindCard")
    public ModelAndView goToBindCard(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        String urlType = request.getParameter("urlType");
        String payList = request.getParameter("payList");
        String rdid = request.getParameter("rdid");
        if(payList!=null){
            payList = new String( Base64.decode(payList));
        }
        ModelAndView modelAndView = new ModelAndView("ftl/addQuickBankCard");
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("urlType", urlType);
        modelAndView.addObject("payList", payList);
        modelAndView.addObject("rdid", rdid);
        return modelAndView;
    }

    /**
     * 进行绑卡
     * @param quickPayAgreeapplyDto
     * @return
     */
    @RequestMapping("/onQuickPayAgreeapply")
    @ResponseBody
    public R onQuickPayAgreeapply(@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto){
        R r = payService.onQuickPayAgreeapply(quickPayAgreeapplyDto);
        return r;
    }

    @RequestMapping("/onAgreeconfirm")
    @ResponseBody
    public R onAgreeconfirm (@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto){
        R r = payService.onAgreeconfirm(quickPayAgreeapplyDto);
        if(r.getCode()==R.SUCCESS){
            //添加银行卡在本地数据库
            QuickPayAgreeconfirmResData quickPayAgreeconfirmResData = (QuickPayAgreeconfirmResData) r.getData();
            TbUserAccount tbUserAccount = bbUserAccountService.selectUserAccount(quickPayAgreeapplyDto.getMeruserid()).getData();
            TbBindCardInfo tbBindCardInfo = new TbBindCardInfo();
            tbBindCardInfo.setBankSn(BankUtil.getBanksn(quickPayAgreeconfirmResData.getBankname()));
            tbBindCardInfo.setBankCard(quickPayAgreeapplyDto.getAcctno());
            tbBindCardInfo.setBankName(quickPayAgreeconfirmResData.getBankname());
            tbBindCardInfo.setOid(tbUserAccount.getId());
            tbBindCardInfo.setContractType(TbBindCardInfo.CONTRACTTYPE_2);
            tbBindCardInfo.setIdCard(quickPayAgreeapplyDto.getIdno());
            tbBindCardInfo.setMobile(quickPayAgreeapplyDto.getMobile());
            tbBindCardInfo.setContractId(quickPayAgreeconfirmResData.getAgreeid());
            tbBindCardInfo.setName(quickPayAgreeapplyDto.getAcctname());
            R bankR  = bindCarSercice.savebank(tbBindCardInfo);
            if(bankR.getCode()==R.SUCCESS){
                return r;
            }else{
                return bankR;
            }
        }
        return r;
    }

    /**
     *
     *跳转到还款界面
     * @return
     */
    @RequestMapping("/goToRepaymentPage")
    public ModelAndView goToRepaymentPage(HttpServletRequest request) {
        String type = request.getParameter("type");
        String payList = request.getParameter("payList");
        if(payList!=null){
            payList = Base64.encode(payList.getBytes());
        }
        String rdid = request.getParameter("rdid");
        if("2".equals(type)){
            return new ModelAndView("redirect:" + payUrlPropertiesConfig.getGotoRepayment() + "/consumer/consumer/bill/jumpAdvancePaybackMoney");
        }else if("3".equals(type)){
           return new ModelAndView("redirect:" + payUrlPropertiesConfig.getGotoRepayment() + "/consumer/consumer/bill/jumpJmPaybackMoney?payList="+payList+"&rdid="+rdid);
        }
        return null;
    }

    /**
     * 跳转到绑卡短信验证码确认页面
     * @param request
     * @return
     */
    @RequestMapping("/goToAgreeconfirm")
    public ModelAndView goToAgreeconfirm(HttpServletRequest request) {
        String meruserid = request.getParameter("meruserid");
        String acctno = request.getParameter("acctno");
        String idno = request.getParameter("idno");
        String acctname = request.getParameter("acctname");
        String mobile = request.getParameter("mobile");
        String thpinfo = request.getParameter("thpinfo");
        String urlType = request.getParameter("urlType");
        String payList = request.getParameter("payList");
        String rdid = request.getParameter("rdid");
        ModelAndView modelAndView = new ModelAndView("ftl/agreeconfirm");
        modelAndView.addObject("meruserid", meruserid);
        modelAndView.addObject("acctno", acctno);
        modelAndView.addObject("idno", idno);
        modelAndView.addObject("acctname", acctname);
        modelAndView.addObject("mobile", mobile);
        modelAndView.addObject("thpinfo", thpinfo);
        modelAndView.addObject("urlType", urlType);
        modelAndView.addObject("payList", payList);
        modelAndView.addObject("rdid", rdid);
        return modelAndView;
    }
}
