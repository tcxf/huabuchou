package com.tcxf.hbc.payement.controller;

import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.PacketService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/packet")
public class PacketController {
    private static final Logger logger = LogManager.getLogger(PacketController.class);

    @Autowired
    private PacketService packetService;

    @RequestMapping("/loadReadPcke")
    @ResponseBody
    public R loadReadPcke(){
        R r =  packetService.wdlpack();
        return r;
    }
}
