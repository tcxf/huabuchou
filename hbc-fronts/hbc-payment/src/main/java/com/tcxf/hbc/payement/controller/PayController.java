package com.tcxf.hbc.payement.controller;

import com.tcxf.hbc.common.bean.config.WxPayPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.util.secure.DES;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.payement.feign.*;
import com.tcxf.hbc.payement.model.dto.PayDto;
import com.tcxf.hbc.payement.model.dto.QuickPayPayagreeconfirmResData;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@RestController
@RequestMapping("/pay")
public class PayController {
    private static final Logger logger = LogManager.getLogger(PayController.class);

    @Autowired
    private PayService payService;

    @Autowired
    private MredPacketService mredPacketService;

    @Autowired
    private ITbWalletService walletService;
    @Autowired
    private TbAccountInfoService tbAccountInfoService;

    @Autowired
    private MMerchantInfoService mMerchantInfoService;

    @Autowired
    private OoperainfoService ooperainfoService;

    @Autowired
    private UserService userService;

    @Autowired
    private MMerchantOtherInfoService mMerchantOtherInfoService;

    @Autowired
    private WxPayPropertiesConfig wxPayPropertiesConfig;

    private final static String SPLIT_CHARACTER = "_";

    private final static String USE_WX_PAY_NO = "0";

    /**
     * 跳转支付界面(微信回调 同时获取微信code 以便拿取微信openId)
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/goToPaymentOptPage")
    public ModelAndView goToPaymentOptPage(HttpServletRequest request, HttpServletResponse response) {
        //获取微信授权code 以便得到openId
        String code = request.getParameter("code");
        String state = request.getParameter("state");
        String openId = request.getParameter("openId");
        Pair<String, String> pair = getMidAndUserId(state);
       //支付公众号openId
        if(ValidateUtil.isEmpty(openId)&&!wxPayPropertiesConfig.getWxPay().equals(USE_WX_PAY_NO)){
            openId = HttpUtil.getOpenId(code, HttpUtil.APP_ID, HttpUtil.SECRET);
            //如果未获取未微信openId 则重新去获取code进行获取openId
            if(ValidateUtil.isEmpty(openId)){
                return  new ModelAndView("redirect:/pay/goTowxGetCode?mId="+pair.getLeft()+"&userId="+pair.getRight());
            }
        }
        Map<String, Object> map = new HashMap<String, Object>();
        MMerchantInfo selectmMerchantInfo = mMerchantInfoService.selectmMerchantInfo(pair.getLeft()).getData();
        TbWallet selectTbWallet = walletService.selectUserWallet(pair.getRight()).getData();
        //处理金额格式化小数点后两位
        selectTbWallet.setTotalAmount(new Calculate(selectTbWallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        TbAccountInfo selectTbAccountInfo = tbAccountInfoService.selectTbAccountInfo(pair.getRight(), selectmMerchantInfo.getOid()).getData();
        //处理授信金额 解密
        if (!ValidateUtil.isEmpty(selectTbAccountInfo)) {
            String balance = DESdecryptdf(selectTbAccountInfo.getBalance());
            //金额格式化小数点后两位
            BigDecimal formatbalance = new Calculate(balance).format(2,RoundingMode.DOWN);
            selectTbAccountInfo.setBalance(formatbalance.toString());
        }
        OOperaInfo selectOOperaInfo = ooperainfoService.selectooperainfoBymId(selectmMerchantInfo.getId()).getData();
        map.put("mId", pair.getLeft());
        map.put("merchantInfo", selectmMerchantInfo);
        map.put("openId", openId);
        map.put("userId", pair.getRight());
        map.put("wallet", selectTbWallet);
        map.put("accountInfo", selectTbAccountInfo);
        map.put("sxUrl","http://" +selectOOperaInfo.getOsn() +"/consumer/consumer/bill/jumpMyCredit?uId="+pair.getRight()+"oId="+
                selectOOperaInfo.getId()+"&type=onPay&state="+state+"&openId="+openId);
        //数据验证
        validateData(map);
        ModelAndView modelAndView = new ModelAndView("ftl/payment_opt");
        modelAndView.addObject("mapData", map);
        return modelAndView;
    }

    /**
     * @param state
     * @return Pair<String , String> left 为 mid  right 为 userId
     */
    private Pair<String, String> getMidAndUserId(String state) {
        if (ValidateUtil.isEmpty(state)) {
            return null;
        }
        List<String> list = StringUtil.splitIds(state, SPLIT_CHARACTER);
        return new Pair<String, String>(list.get(0), list.get(1));
    }

    /**
     * 金额的解密
     */
    private String DESdecryptdf(final String banlance) {
        String balanceMoney = null;
        try {
            balanceMoney = DES.decryptdf(banlance);
            return balanceMoney;
        } catch (Exception e) {
            throw new CheckedException("查找授信额度失败");
        }
    }

    /**
     * 数据有效性验证
     *
     * @param map
     * @return
     */
    private void validateData(Map<String, Object> map) {
        if (ValidateUtil.isEmpty(map.get("openId"))&&!wxPayPropertiesConfig.getWxPay().equals(USE_WX_PAY_NO)) {
            throw new CheckedException("获取微信信息失败，请稍后再试");
        }
        if (ValidateUtil.isEmpty(map.get("merchantInfo"))) {
            throw new CheckedException("获取商户信息失败，请稍后再试");
        }
        MMerchantInfo selectmMerchantInfo = (MMerchantInfo) map.get("merchantInfo");
        if (!MMerchantInfo.status_Y.equals(selectmMerchantInfo.getStatus())) {
            throw new CheckedException("该商户暂未启用，不能进行收款");
        }
        if (ValidateUtil.isEmpty(map.get("wallet"))) {
            throw new CheckedException("获取用户钱包信息失败，请稍后再试");
        }
        if (ValidateUtil.isEmpty(map.get("userId"))) {
            throw new CheckedException("获取用户信息失败，请稍后再试");
        }
        CUserInfo cUserInfo = userService.selectUserInfoById(map.get("userId").toString()).getData();
        if (ValidateUtil.isEmpty(cUserInfo)) {
            throw new CheckedException("获取用户信息失败，请稍后再试");
        }
        OOperaInfo selectOOperaInfo = ooperainfoService.selectooperainfoBymId(selectmMerchantInfo.getId()).getData();
        if (ValidateUtil.isEmpty(selectOOperaInfo)) {
            throw new CheckedException("未找到该商户的运营商信息，暂不能支付");
        }
        if (ValidateUtil.isEmpty(selectOOperaInfo.getAppid()) || ValidateUtil.isEmpty(selectOOperaInfo.getAppsecret())) {
            throw new CheckedException("请先配置运营商微信公众号信息");
        }
    }

    /**
     * 查找用户优惠券信息
     *
     * @return
     */
    @RequestMapping("/selectRedPacketByUserId")
    @ResponseBody
    public R selectRedPacketByUserId(@RequestBody PayDto payDto) {
        return mredPacketService.selectRedPacketAvailable(payDto.getUserId(),
                payDto.getmId(), payDto.getAmount());
    }

    @RequestMapping("/goToPayOk")
    public ModelAndView goToPayOk(HttpServletRequest request) {
        String mId = request.getParameter("mId");
        String userId = request.getParameter("userId");
        MMerchantInfo selectmMerchantInfo = mMerchantInfoService.selectmMerchantInfo(mId).getData();
        ModelAndView modelAndView = new ModelAndView("ftl/pay_Ok");
        modelAndView.addObject("mId", mId);
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("name", selectmMerchantInfo.getName());
        return modelAndView;
    }

    /**
     * 支付成功之后界面跳转（直接支付回调）
     *
     * @return
     */
    @RequestMapping("/webcallbackurl")
    public ModelAndView webcallbackurl(HttpServletRequest request) {
        String mId = request.getParameter("mId");
        String userId = request.getParameter("userId");
        String url = request.getRequestURL().toString();
        String queryurl = request.getQueryString();
        if (!ValidateUtil.isEmpty(queryurl)) {
            url += "?" + queryurl;
        }
        OOperaInfo selectOOperaInfo = ooperainfoService.selectooperainfoBymId(mId).getData();
        CUserInfo cUserInfo = userService.selectUserInfoById(userId).getData();
        MMerchantOtherInfo selectMMerchantOtherInfo = mMerchantOtherInfoService.selectMMerchantOtherInfo(mId).getData();
        MMerchantInfo selectmMerchantInfo = mMerchantInfoService.selectmMerchantInfo(mId).getData();
        //支持微信支付的环境 则去获取ticke配置注入，否则 以免测试环境与生产环境一起运行导致微信token冲突
        Map<String, Object> map = new HashMap<String,Object>();
        if(!wxPayPropertiesConfig.getWxPay().equals(USE_WX_PAY_NO)){
            map = HttpUtil.ticketConfig(url, HttpUtil.APP_ID, HttpUtil.SECRET);
        }
        String basePath = request.getScheme() + "://" + request.getServerName() +
                ":" + request.getServerPort() + request.getContextPath() + "/";
        ModelAndView modelAndView = new ModelAndView("ftl/webcallbackurl");
        modelAndView.addObject("ticketMap", map);
        modelAndView.addObject("invateCode", cUserInfo.getInvateCode());
        modelAndView.addObject("userId", cUserInfo.getId());
        modelAndView.addObject("localPhoto", selectMMerchantOtherInfo.getLocalPhoto());
        modelAndView.addObject("mId", selectMMerchantOtherInfo.getMid());
        modelAndView.addObject("name", selectmMerchantInfo.getName());
        modelAndView.addObject("goindex", "http://" + selectOOperaInfo.getOsn() + "/consumer/user/index/Goindex?invateCode="+cUserInfo.getInvateCode()+"");
        //modelAndView.addObject("nearby", "http://" + selectOOperaInfo.getOsn() + "/consumer/nearby/findById?id="+mId+"&invateCode="+cUserInfo.getInvateCode()+"");
        modelAndView.addObject("nearby", basePath.replace(":80", "")+"pay/webcallbackurl?mId="+mId+"&userId="+userId);
        return modelAndView;
    }

    @RequestMapping("/goToNearby")
    public ModelAndView goToNearby(HttpServletRequest request) {
        String mId = request.getParameter("mId");
        String invateCode = request.getParameter("invateCode");
        OOperaInfo selectOOperaInfo = ooperainfoService.selectooperainfoBymId(mId).getData();
        return new ModelAndView("redirect: http://"+selectOOperaInfo.getOsn()+"/consumer/nearby/findById?id="+mId+"&invateCode="+invateCode+"");
    }

    /**
     * 获取微信授权code (支付公众下的code)
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping("/goTowxGetCode")
    public void goTowxGetCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String basePath = request.getScheme() + "://" + request.getServerName() +
                ":" + request.getServerPort() + request.getContextPath() + "/";
        String mid = request.getParameter("mId");
        String userId = request.getParameter("userId");
        //根据配置参数 是否需要支持维修支付 如果不需要则不需要从微信端获取openId
        if(wxPayPropertiesConfig.getWxPay().equals(USE_WX_PAY_NO)){
            response.sendRedirect(basePath+"pay/goToPaymentOptPage?state="+mid + SPLIT_CHARACTER + userId);
            return;
        }
        String redirectUrl = HttpUtil.buildSendRedUrl(basePath, HttpUtil.APP_ID);
        redirectUrl = redirectUrl.replace("statecode", mid + SPLIT_CHARACTER + userId);
        redirectUrl = redirectUrl.replace("redirectUrlValue", "pay/goToPaymentOptPage");
        response.sendRedirect(redirectUrl);
    }

    /**
     * 支付
     *
     * @param payDto
     * @return
     */
    @RequestMapping("/onPay")
    @ResponseBody
    public R onPay(@RequestBody PayDto payDto) {
        R<PayDto> r = payService.onPay(payDto);
        return r;
    }


    /**
     * 支付成功回调
     *
     * @param request
     * @param response
     */
    @RequestMapping("/onpayNotify")
    @ResponseBody
    public void onpayNotify(HttpServletRequest request, HttpServletResponse response) {
        logger.error("进入支付通知回调");
        TreeMap<String, String> map = getParams(request);
        logger.error("支付通知回调 获取商户订单："+map.get("cusorderid"));
        R r = payService.onpayNotify(map);
       if(r.getCode()==R.SUCCESS) {
            logger.error("支付通知回调完成");
            try {
                request.setCharacterEncoding("gbk");//通知传输的编码为GBK
                response.setCharacterEncoding("gbk");
                response.getOutputStream().write("success".getBytes());
                response.flushBuffer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
     *
     * @param request
     * @return
     */
    private TreeMap<String, String> getParams(HttpServletRequest request) {
        TreeMap<String, String> map = new TreeMap<String, String>();
        Map reqMap = request.getParameterMap();
        for (Object key : reqMap.keySet()) {
            String value = ((String[]) reqMap.get(key))[0];
            System.out.println(key + ";" + value);
            map.put(key.toString(), value);
        }
        return map;
    }

    @RequestMapping("/goToPayagreeconfirm")
    public ModelAndView goToPayagreeconfirm(HttpServletRequest request) {
        String orderid = request.getParameter("orderid");
        String agreeid = request.getParameter("agreeid");
        String mId = request.getParameter("mId");
        String userId = request.getParameter("userId");
        String thpinfo = request.getParameter("thpinfo");
        ModelAndView modelAndView = new ModelAndView("ftl/payagreeconfirm");
        modelAndView.addObject("orderid", orderid);
        modelAndView.addObject("agreeid", agreeid);
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("mId", mId);
        modelAndView.addObject("thpinfo", thpinfo);
        return modelAndView;
    }

    /**
     * 快捷支付确认
     *
     * @param payagreeconfirmDto
     * @return
     */
    @RequestMapping("/onPayagreeconfirm")
    @ResponseBody
    public R onPayagreeconfirm(@RequestBody PayagreeconfirmDto payagreeconfirmDto) {
        R<QuickPayPayagreeconfirmResData> r = payService.onPayagreeconfirm(payagreeconfirmDto);
        return r;
    }



    /**
     * 钱包数据验证
     *
     * @param
     * @return
     */
    @RequestMapping("/onCheckTbWallet")
    @ResponseBody
    public R onCheckTbWallet(){
        R r = payService.onCheckTbWallet();
        return r;
    }


    /**
     * 钱包数据验证
     *
     * @param
     * @return
     */
    @RequestMapping("/goToCheckTbWalletPage")
    public ModelAndView goToCheckTbWalletPage(){
        ModelAndView modelAndView = new ModelAndView("ftl/onCheckTbWallet");
        return modelAndView;
    }

}

