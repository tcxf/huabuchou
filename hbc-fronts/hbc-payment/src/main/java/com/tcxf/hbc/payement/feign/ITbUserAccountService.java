package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.TbUserAccountServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@FeignClient(name = "hbc-trans-service", fallback = TbUserAccountServiceFallbackImpl.class)
public interface ITbUserAccountService {
    /**
     * 获取用户accountID
     * @param userId
     * @return
     */
    @RequestMapping("/userAccount/selectUserAccount")
    public R<TbUserAccount> selectUserAccount(@RequestParam("userId") String userId);
}
