package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.TbWalletServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "hbc-trans-service", fallback = TbWalletServiceFallbackImpl.class)
public interface ITbWalletService{
    /**
     * 获取用户钱包
     * @param userId
     * @return
     */
    @RequestMapping("/wallet/selectUserWallet")
    public R<TbWallet> selectUserWallet(@RequestParam("userId") String userId);
}
