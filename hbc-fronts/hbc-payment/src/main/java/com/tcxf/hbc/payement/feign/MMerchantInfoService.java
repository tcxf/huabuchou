package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.MMerchantInfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:44
 * @Description: 商户信息
 */
@FeignClient(name = "hbc-trans-service", fallback = MMerchantInfoServiceFallbackImpl.class)
public interface MMerchantInfoService {

    @RequestMapping("/merchantInfo/selectmMerchantInfo")
    public R<MMerchantInfo> selectmMerchantInfo(@RequestParam("mId") String mId);
}
