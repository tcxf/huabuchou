package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.MMerchantOtherInfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:44
 * @Description: 商户其他信息
 */
@FeignClient(name = "hbc-trans-service", fallback = MMerchantOtherInfoServiceFallbackImpl.class)
public interface MMerchantOtherInfoService {

    @RequestMapping("/merchantOtherInfo/selectMMerchantOtherInfo")
    public R<MMerchantOtherInfo> selectMMerchantOtherInfo(@RequestParam("mId") String mId);
}
