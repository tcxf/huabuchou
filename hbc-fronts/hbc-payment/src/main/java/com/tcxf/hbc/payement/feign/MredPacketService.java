package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.MredPacketServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:44
 * @Description: 优惠卷
 */
@FeignClient(name = "hbc-trans-service", fallback = MredPacketServiceFallbackImpl.class)
public interface MredPacketService {

    @RequestMapping("/mredPacket/selectRedPacketAvailable")
    public R selectRedPacketAvailable(@RequestParam("userId") String userId,
                                      @RequestParam("mId")String mId,
                                      @RequestParam("amount")BigDecimal amount);
}
