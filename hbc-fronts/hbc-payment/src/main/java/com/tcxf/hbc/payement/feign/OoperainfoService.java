package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.OoperainfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:44
 * @Description: 运营商信息
 */
@FeignClient(name = "hbc-trans-service", fallback = OoperainfoServiceFallbackImpl.class)
public interface OoperainfoService {

    @RequestMapping("/operaInfo/selectooperainfoBymId")
    public R<OOperaInfo> selectooperainfoBymId(@RequestParam("mId") String mId);
}
