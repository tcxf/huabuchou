package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.PacketServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-mc-service", fallback =PacketServiceImpl.class)
public interface PacketService {

    @RequestMapping(value = "/packets/NOpacket",method ={RequestMethod.POST} )
    public R wdlpack();

}
