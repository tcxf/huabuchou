package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.payement.feign.fallback.PayServiceFallbackImpl;
import com.tcxf.hbc.payement.model.dto.PayDto;
import com.tcxf.hbc.payement.model.dto.QuickPayAgreeapplyDto;
import com.tcxf.hbc.payement.model.dto.QuickPayAgreeconfirmResData;
import com.tcxf.hbc.payement.model.dto.QuickPayPayagreeconfirmResData;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.TreeMap;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 11:54
 * @Description:
 */
@FeignClient(name = "hbc-trans-service", fallback = PayServiceFallbackImpl.class)
public interface PayService {

    /**
     * 支付
     * @param payDto
     * @return
     */
    @RequestMapping("/pay/onPay")
    public R onPay(@RequestBody PayDto payDto);

    /**
     *  签约
     * @param quickPayAgreeapplyDto
     * @return
     */
    @RequestMapping("/pay/onQuickPayAgreeapply")
    public R onQuickPayAgreeapply(@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto);

    /**
     *  签约确认
     * @param quickPayAgreeapplyDto
     * @return
     */
    @RequestMapping("/pay/onAgreeconfirm")
    public R<QuickPayAgreeconfirmResData> onAgreeconfirm(@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto);


    /**
     * 支付成功通知回调
     * @param map
     * @return
     */
    @RequestMapping("/pay/onpayNotify")
    public R onpayNotify(@RequestBody TreeMap<String, String> map);

    @RequestMapping("/pay/onPayagreeconfirm")
    R<QuickPayPayagreeconfirmResData> onPayagreeconfirm(PayagreeconfirmDto payagreeconfirmDto);

    /**
     * 钱包数据验证
     * @param
     * @return
     */
    @RequestMapping("/pay/onCheckTbWallet")
    R onCheckTbWallet();
}
