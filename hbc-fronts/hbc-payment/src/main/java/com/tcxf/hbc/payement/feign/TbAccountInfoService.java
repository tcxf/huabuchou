package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.TbAccountInfoServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 11:54
 * @Description:
 */
@FeignClient(name = "hbc-trans-service", fallback = TbAccountInfoServiceFallbackImpl.class)
public interface TbAccountInfoService {
    /**
     * 获取用户授信额度
     * @param userId
     * @return
     */
    @RequestMapping("/accountInfo/selectTbAccountInfo")
    public R<TbAccountInfo> selectTbAccountInfo(@RequestParam("userId") String userId, @RequestParam("oId") String oId);

}
