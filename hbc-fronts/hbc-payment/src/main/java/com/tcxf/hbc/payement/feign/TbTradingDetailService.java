package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.TbTradingDetailServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "hbc-trans-service", fallback = TbTradingDetailServiceFallbackImpl.class)
public interface TbTradingDetailService {

    @RequestMapping("/tbTradingDetail/selecbTradingDetailBySerialNo")
    public R<TbTradingDetail> selecbTradingDetailBySerialNo(@RequestParam("serialNo") String serialNo);
}
