package com.tcxf.hbc.payement.feign;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.fallback.UserServiceFallbackImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhouyj
 * @date 2018/3/10
 */
@FeignClient(name = "hbc-trans-service", fallback = UserServiceFallbackImpl.class)
public interface UserService {

  /**
   * 获取用户信息 根据 openId
   * @param openId
   * @return
   */
  @RequestMapping("/cUserInfo/selectUserInfoByOpenId")
  public R<CUserInfo> selectUserInfoByOpenId(@RequestParam("openId") String openId);

  @RequestMapping("/cUserInfo/selectUserInfoById")
  public R<CUserInfo>  selectUserInfoById(@RequestParam("userId")String userId);
}
