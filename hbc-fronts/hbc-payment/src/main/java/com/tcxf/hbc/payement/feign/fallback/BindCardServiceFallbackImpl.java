package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.BindCarSercice;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BindCardServiceFallbackImpl implements BindCarSercice {


    @Override
    public R findbank(String oid, String type) {
        return null;
    }

    @Override
    public Boolean delbank(String id) {
        return null;
    }

    @Override
    public R savebank(TbBindCardInfo tbBindCardInfo) {
        return null;
    }


}
