package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.payement.feign.MMerchantInfoService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:46
 * @Description:
 */
@Service
public class MMerchantInfoServiceFallbackImpl implements MMerchantInfoService {

    @Override
    public R<MMerchantInfo> selectmMerchantInfo(String mId) {
        throw new CheckedException("网络异常，请稍后再试");
    }
}
