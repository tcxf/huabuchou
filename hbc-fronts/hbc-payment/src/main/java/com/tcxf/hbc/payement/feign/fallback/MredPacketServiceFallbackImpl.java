package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.MredPacketService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 16:46
 * @Description:
 */
@Service
public class MredPacketServiceFallbackImpl implements MredPacketService {
    @Override
    public R selectRedPacketAvailable(String userId, String mId, BigDecimal amount) {
        System.out.println("服务调用异常");
        return null;
    }
}
