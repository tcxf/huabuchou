package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.payement.feign.PayService;
import com.tcxf.hbc.payement.model.dto.PayDto;
import com.tcxf.hbc.payement.model.dto.QuickPayAgreeapplyDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.TreeMap;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 11:55
 * @Description:
 */
@Service
public class PayServiceFallbackImpl implements PayService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R onPay(@RequestBody PayDto payDto) {
        logger.info("支付失败");
        throw new CheckedException("支付失败，请稍后再试");
    }

    @Override
    public R onQuickPayAgreeapply(QuickPayAgreeapplyDto quickPayAgreeapplyDto) {
        return null;
    }

    @Override
    public R onAgreeconfirm(QuickPayAgreeapplyDto quickPayAgreeapplyDto) {
        return null;
    }

    @Override
    public R onpayNotify(TreeMap<String, String> data) {
        System.out.println("支付回调通知服务调用异常");
        return null;
    }

    @Override
    public R onPayagreeconfirm(PayagreeconfirmDto payagreeconfirmDto) {
        return null;
    }

    @Override
    public R onCheckTbWallet() {
        return null;
    }
}
