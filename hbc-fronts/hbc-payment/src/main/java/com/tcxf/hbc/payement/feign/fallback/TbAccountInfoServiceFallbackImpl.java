package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.TbAccountInfoService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 11:55
 * @Description:
 */
@Service
public class TbAccountInfoServiceFallbackImpl implements TbAccountInfoService {

    @Override
    public R<TbAccountInfo> selectTbAccountInfo(String userId, String oId) {
        return null;
    }
}
