package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.TbTradingDetailService;
import org.springframework.stereotype.Service;

@Service
public class TbTradingDetailServiceFallbackImpl implements TbTradingDetailService {

    @Override
    public R<TbTradingDetail> selecbTradingDetailBySerialNo(String serialNo) {
        return null;
    }
}
