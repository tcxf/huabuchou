package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.BindCarSercice;
import com.tcxf.hbc.payement.feign.ITbUserAccountService;
import org.springframework.stereotype.Service;

@Service
public class TbUserAccountServiceFallbackImpl implements ITbUserAccountService {


    @Override
    public R<TbUserAccount> selectUserAccount(String userId) {
        return null;
    }
}
