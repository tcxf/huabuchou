package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.ITbWalletService;
import org.springframework.stereotype.Service;



@Service
public class TbWalletServiceFallbackImpl implements ITbWalletService {

    @Override
    public R<TbWallet> selectUserWallet(String userId) {
        return null;
    }
}
