package com.tcxf.hbc.payement.feign.fallback;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.payement.feign.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author zhouyj
 * @date 2018/3/10
 * 用户服务的fallback
 */
@Service
public class UserServiceFallbackImpl implements UserService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public R<CUserInfo> selectUserInfoByOpenId(String openId) {
        return null;
    }

    @Override
    public R<CUserInfo> selectUserInfoById(String userId) {
        return null;
    }
}
