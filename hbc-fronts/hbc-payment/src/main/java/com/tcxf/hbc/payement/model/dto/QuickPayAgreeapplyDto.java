package com.tcxf.hbc.payement.model.dto;

/**
 * @Auther: liuxu
 * @Date: 2018/8/8 16:18
 * @Description: 签约申请DTO
 */
public class QuickPayAgreeapplyDto {
    /**
     * 商户用户号 这里取userId
     */
    private String meruserid;
    /**
     * 银行卡号
     */
    private String acctno ;
    /**
     * 身份证号
     */
    private String idno ;
    /**
     * 姓名
     */
    private String acctname;
    /**
     * 手机
     */
    private String mobile;

    /**
     * 验证码
     */
    private String smscode;

    /**
     * 签约申请信息
     */
    private String thpinfo;

    /**
     * 银行名称
     */
    private String bankname;

    public String getMeruserid() {
        return meruserid;
    }

    public void setMeruserid(String meruserid) {
        this.meruserid = meruserid;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSmscode() {
        return smscode;
    }

    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}
