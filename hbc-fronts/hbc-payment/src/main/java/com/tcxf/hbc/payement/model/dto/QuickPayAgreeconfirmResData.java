package com.tcxf.hbc.payement.model.dto;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 签约申请确认响应数据结构
 */
public class QuickPayAgreeconfirmResData {

    /**
     0000: 签约成功,流程完成
     3051: 协议已存在,请勿重复签约
     3999: 签约失败
     3058：短信验证码错误
     3057：请重新获取验证码
     3004: 卡号错误(无法识别卡bin)
     */
    public  final static String TRXSTATUS_0000="0000";
    /**
     * 返回码	SUCCESS/FAIL
     */
    private String retcode;
    /**
     * 返回码说明
     */
    private String retmsg;
    /**
     * 随机字符串	随机生成的字符串  SUCCESS返回
     */
    private String randomstr;
    /**
     * 签名 SUCCESS返回
     */
    private String sign;

    /**
     * 状态
     */
    private String trxstatus;
    /**
     * 错误信息
     */
    private String errmsg;
    /**
     * 协议编号		否	32	状态为0000时返回
     */
    private String agreeid;
    /**
     * 银行代码		否	16	状态为0000时返回
     */
    private String bankcode;
    /**
     * 银行名称		否	100	状态为0000时返回
     */
    private String bankname;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTrxstatus() {
        return trxstatus;
    }

    public void setTrxstatus(String trxstatus) {
        this.trxstatus = trxstatus;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }
}
