package com.tcxf.hbc.payement.model.dto;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 支付确认响应数据结构
 */
public class QuickPayPayagreeconfirmResData {

    /**
     * 交易状态	交易的状态,
     * 0000:交易成功,交易流程完成
     * 其余详看说明
     */
    public  final static String TRXSTATUS_0000="0000";

    public  final static String TRXSTATUS_2000="2000";

    public  final static String TRXSTATUS_2008="2008";


    /**
     * 返回码	SUCCESS/FAIL
     * 	否	8	此字段是通信标识，非交易结果，交易是否成功需要查看trxstatus来判断
     */
    private String retcode;
    /**
     * 返回码说明
     */
    private String retmsg;
    /**
     * 随机字符串	随机生成的字符串	是	32	SUCCESS返回
     */
    private String randomstr;
    /**
     * 签名 SUCCESS返回
     */
    private String sign;

    //业务参数  当retcode为SUCCESS时有返回
    /**
     * 交易状态	交易的状态,
     * 0000:交易成功,交易流程完成
     * 其余详看说明
     */
    private String trxstatus;
    /**
     * 错误原因	失败的原因说明
     */
    private String errmsg;
    /**
     * 商户订单号
     */
    private String orderid;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTrxstatus() {
        return trxstatus;
    }

    public void setTrxstatus(String trxstatus) {
        this.trxstatus = trxstatus;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }
}