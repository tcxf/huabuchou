<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${basePath!}/css/card_list.css" />
        <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
		<title>添加银行卡</title>
        <style>
            .container_gress{
                margin-top: 10px;
                color:#999;
                height: 20px;
                line-height: 20px;
            }
            .container_gress .col-xs-12{
                text-align: center;
            }
            .container_gress .col-xs-12>.col-xs-3{
                text-align: right;
                padding: 0;
				margin-top: 5px;
            }
            .container_gress .col-xs-12>.col-xs-9{
                text-align: left;
                padding: 0;
            }
            .container_gress .col-xs-12>.col-xs-9>a{
                text-decoration: none;
                font-size: 12px;
            }
		</style>
	</head>
	<body>
    <div class="card_content">
        <p>请绑定持卡人本人的银行卡</p>
        <div class="card_list">
            <ul>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                真实姓名
                            </div>
                            <div class="col-xs-8">
                                <input type="text" type="tel" id="acctname"  placeholder="请输入您的姓名">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                身份证号码
                            </div>
                            <div class="col-xs-8">
                                <input type="text" id="idno"  placeholder="请输入您的身份证号码">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                银行卡号
                            </div>
                            <div class="col-xs-8">
                                <input type="tel" id="acctno"  placeholder="请输入您的银行卡号">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                手机号码
                            </div>
                            <div class="col-xs-8">
                                <input  type="tel" id="mobile"  placeholder="请输入您的手机号">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="agree_btn tj" id="quickPayAgreeapply">
        <a href="#" >下一步</a>
    </div>
    <!--
    <div class="container_gress">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-3">
                        <input type="checkbox" id="agree1"/>
                    </div>
                    <div class="col-xs-9">
                        同意
                        <a href="#mymodalSix" data-toggle="modal" style="color: #008fff;">《银行卡支付协议》</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
	</body>
    <script src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script src="${basePath!}/js/layer/layer.js"></script>
    <script src="${basePath!}/js/ajax.js"></script>
	<script>
        $("#bankCard").on("input",function(){
            var val=$(this).val().replace(/\s/g,"").replace(/(\d{4})/g,"$1 ");
            $(this).val(val);
        });
        $("#bankCard").on("keyup",function(){
            //获取当前光标的位置
            var caret = this.selectionStart;
            //获取当前的value
            var value = this.value;
            //从左边沿到坐标之间的空格数
            var sp =  (value.slice(0, caret).match(/\s/g) || []).length;
            //去掉所有空格
            var nospace = value.replace(/\s/g, '');
            //重新插入空格
            var curVal = this.value = nospace.replace(/\D+/g,"").replace(/(\d{4})/g, "$1 ").trim();
            //从左边沿到原坐标之间的空格数
            var curSp = (curVal.slice(0, caret).match(/\s/g) || []).length;
            //修正光标位置
            this.selectionEnd = this.selectionStart = caret + curSp - sp;
        });
        $("#quickPayAgreeapply").click(function () {
            var url="${basePath!}/bindCard/onQuickPayAgreeapply";
            var par ={};
            par.meruserid = "${userId!}";
            par.acctno = $("#acctno").val();
            par.idno = $("#idno").val();
            par.acctname = $("#acctname").val();
            par.mobile = $("#mobile").val();
            if(par.acctname==null||par.acctname==""){
                layer.msg("姓名不能为空")
                return;
            }
            if(par.acctno==null||par.acctno==""){
                layer.msg("卡号不能为空")
                return;
            }
            if(par.idno==null||par.idno==""){
                layer.msg("身份证号码不能为空")
                return;
            }
            if(par.mobile==null||par.mobile==""){
                layer.msg("手机号码不能为空")
                return;
            }
            ajax.loadService(url,par, function(data){
                if(data.code=="0") {
                    var listStr;
                    <#if (payList)??>
                         listStr=JSON.stringify(${payList!});
                    <#else >
                         listStr='0';
                    </#if>
                    window.location.href="${basePath!}/bindCard/goToAgreeconfirm?meruserid="+par.meruserid+"&acctno="+par.acctno+"&" +
                            "idno="+par.idno+"&acctname="+par.acctname+"&mobile="+par.mobile+"&urlType=${urlType!}&thpinfo="+data.data+"&payList="+listStr+"&rdid="+"${rdid!}";
                }
                if(data.code=="1"){
                    layer.msg(data.msg);
                }
            });
        });

	</script>

</html>