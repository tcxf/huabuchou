<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>钱包数据验证</title>
</head>
<body>

<div class="s_content">
    <table class="table">
        <th>钱包ID</th>
        <th>钱包类型</th>
        <th>所属用户ID</th>
        <th>钱包总金额</th>
        <th>钱包明细总入金额</th>
        <th>钱包明细总出金额</th>
        <tbody id="tbody">
        </tbody>
    </table>
</div>


<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script src="${basePath!}/js/ajax.js"></script>
<script>
    var url="${basePath!}/pay/onCheckTbWallet";
    var par={};
    ajax.loadService(url,par, function(data){
        if(data.code==0){
            var html="";
            if(data.data.length==0){
                html+="<tr>";
                html+="<td>数据正常</td>";
                html+="</tr>";
                $("#tbody").html(html);
                return;
            }
            for(var i in data.data) {
                html+="<tr>";
                html+="<td>"+data.data[i].walletId+"</td>";
                var wallettype="";
                if(data.data[i].walletType=="1"){
                    wallettype="消费者用户"
                }
                if(data.data[i].walletType=="2"){
                    wallettype="商户"
                }
                if(data.data[i].walletType=="3"){
                    wallettype="运营商"
                }
                if(data.data[i].walletType=="4"){
                    wallettype="资金端"
                }
                html+="<td>"+wallettype+"</td>";
                html+="<td>"+data.data[i].uId+"</td>";
                html+="<td>"+data.data[i].walletMoney+"</td>";
                html+="<td>"+data.data[i].addamount+"</td>";
                html+="<td>"+data.data[i].subtractmount+"</td>";
                html+="</tr>";
            }
            $("#tbody").html(html);
        }
    });
</script>
</body>
</html>