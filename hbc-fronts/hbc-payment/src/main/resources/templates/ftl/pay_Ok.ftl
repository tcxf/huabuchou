<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>支付成功</title>
    <style>
        body{
            background: #fff;
        }
        .top{
            text-align: center;
            margin-top: 10%;
        }
        .top>img{
            width: 9rem;
        }
        .s_content{
            text-align: center;

        }
        .s_content>p{
            color: #333;
            margin-top: 10px;
            font-weight: 600;
        }
        .s_content h6{
            color: #999;
        }
        .tj{
            width: 94%;
            margin-top: 10%;
            height: 40px;
            line-height: 40px;
            margin-left: 3%;
            text-align: center;
            background:#f49110;
            border-radius:30px;
            cursor: pointer;
        }
        .tj>a{
            color: #fff;
            text-decoration: none;
        }
        .pay_ok{
            margin-top: 40px;
            width: 100%;
            height: 40px;
            line-height: 40px;
            border-bottom: 1px solid #ededed;
        }
        .pay_ok .col-xs-6:last-child{
            text-align: right;
        }
    </style>
</head>
<body>
<div class="top">
    <img src="${basePath!}/imgs/pay_ok.png" alt=""/>
</div>
<div class="s_content">
    <p>支付成功</p>
</div>
<div class="pay_ok">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                收款方:
            </div>
            <div class="col-xs-6">
                ${name!}
            </div>
        </div>
    </div>
</div>
<div class="tj">
    <a href="${basePath!}/pay/webcallbackurl?mId=${mId!}&userId=${userId!}" type="buttom" class="btn btn-block">完成</a>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<script>
    //清掉缓存数据
    window.sessionStorage.removeItem("pay_payType");
    window.sessionStorage.removeItem("pay_amount");
    window.sessionStorage.removeItem("pay_mredmarketId");
    window.sessionStorage.removeItem("pay_yh");
    window.sessionStorage.removeItem("pay_state");
    window.sessionStorage.removeItem("pay_openId");
    window.sessionStorage.removeItem("pay_acctno");
    window.sessionStorage.removeItem("pay_bankname");
    window.sessionStorage.removeItem("pay_agreeid");

</script>
</body>
</html>