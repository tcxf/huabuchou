<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${basePath!}/css/card_list.css" />
        <link rel="stylesheet" href="${basePath!}/css/agreement.css" />
		<title>确认支付</title>
        <style>
            .container_gress{
                margin-top: 10px;
                color:#999;
                height: 20px;
                line-height: 20px;
            }
            .container_gress .col-xs-12{
                text-align: center;
            }
            .container_gress .col-xs-12>.col-xs-3{
                text-align: right;
                padding: 0;
				margin-top: -2px;
            }
            .container_gress .col-xs-12>.col-xs-9{
                text-align: left;
                padding: 0;
            }
            .container_gress .col-xs-12>.col-xs-9>a{
                text-decoration: none;
                font-size: 12px;
            }
		</style>
	</head>
	<body>
    <div class="card_content">
        <div class="card_list">
            <ul>
                <li>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-4">
                                验证码
                            </div>
                            <div class="col-xs-8">
                                <input  type="tel" id="smscode" placeholder="请输入验证码">
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <div class="agree_btn tj" id="agreeconfirm">
        <a href="#" >确认支付</a>
    </div>
    <!--
    <div class="container_gress">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-3">
                        <input type="checkbox" id="agree1"/>
                    </div>
                    <div class="col-xs-9">
                        同意
                        <a href="#mymodalSix" data-toggle="modal" style="color: #008fff;">《银行卡支付协议》</a>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
	</body>
    <script src="${basePath!}/js/jquery.js"></script>
    <script src="${basePath!}/js/bootstrap.js"></script>
    <script src="${basePath!}/js/layer/layer.js"></script>
    <script src="${basePath!}/js/ajax.js"></script>
	<script>
        $("#agreeconfirm").click(function () {
            var url="${basePath!}/pay/onPayagreeconfirm";
            var data ={};
            data.orderid = "${orderid!}";
            data.agreeid = "${agreeid!}";
            data.smscode = $("#smscode").val();
            data.thpinfo = "${thpinfo!?json_string}";
            if(data.smscode==null||data.smscode==""){
                layer.msg("验证码不能为空")
                return;
            }
            ajax.loadService(url,data, function(data){
                if(data.code=="0") {
                    window.location.href="${basePath!}/pay/goToPayOk?mId="+"${mId!}"+"&userId="+"${userId!}"+"";
                }
                if(data.code=="1"){
                    layer.msg(data.msg);
                }
            });
        });
	</script>

</html>