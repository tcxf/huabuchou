<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/zhifu.css" />
    <link rel="stylesheet" href="${basePath!}/css/possword.css" />
    <title>扫码支付</title>
    <style>
        input::-webkit-input-placeholder{
            font-size: 14px;
        }
    </style>

</head>
<body>
<div class="top_one">
    <ul>
        <li>
            <b>向商户${(mapData.merchantInfo.name)!""}付款</b>
        </li>
        <li>
            <span><b>支付金额:</b></span>
            <input type="number" id="input_text"  placeholder="请输入支付金额" style="padding-left: 20px">
        </li>
    </ul>
</div>
<div class="top_one">
    <div class="container-fluid">
        <div class="row" onclick="selectRedPacketByUserId()">
            <div class="col-xs-6">
                <b>优惠券:</b>
                <input id="mredmarketId" type="hidden">
                <input id="mredmarketmoney" type="hidden" value="0">
            </div>
            <div class="col-xs-6">
                <div>选择优惠券</div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="mymodal" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" style="float: left">&times;</button>
        </div>
        <div class="modal-body">
            <div class="nonuse_coupons">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-6">
                            不使用优惠券
                        </div>
                        <div class="col-xs-6">
                            <input type="radio" id="nonuse" name="nonuse" value="1" checked>
                        </div>
                    </div>
                </div>
            </div>


            <div class="coupont_list" id="mredmarket">
                <ul>

                </ul>
            </div>
        </div>
    </div>
</div>
<div class="top_one">
    <ul>
        <li>
            <b>请选择支付方式</b>
        </li>
        <li class="one_pay">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/imgs/icon_sx_no.png" alt="">
                    </div>
                    <div class="col-xs-6">
                        <p>授信支付</p>
                    </div>
                    <div class="col-xs-4" onclick="goTosx()">
                        <a href="javascript:void(0)">立即授信</a>
                    </div>
                </div>
            </div>
        </li>
        <li class="sx_pay">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/imgs/icon_shouxinzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>授信支付 <span>(授信余额：${(((mapData.accountInfo.balance)?number)?string('0.00'))!"0.00"})</span></p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="shouxin" name="payType" value="1" checked>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/imgs/ico_qianbaozhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>钱包余额 <span>(可用余额：${((mapData.wallet.totalAmount)?string('0.00'))!"0.00"})</span></p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="qianbao" name="payType" value="2">
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/imgs/ico_weinzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>微信支付</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="weixin" name="payType" value="3">
                    </div>
                </div>
            </div>
        </li>
        <li class="more_pay">
            <a href="#">查看更多支付方式▼</a>
        </li>
        <li class="yinlian">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="${basePath!}/imgs/ico_yinlianzhifu.png" alt="">
                    </div>
                    <div class="col-xs-8">
                        <p>网银快捷</p>
                    </div>
                    <div class="col-xs-2">
                        <input type="radio" id="wangyin" name="payType" value="4">
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="my_card" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-4">
                我的银行卡
            </div>
            <input id="agreeid" type="hidden">

            <div class="col-xs-8 card_name">
                选择银行卡
            </div>
        </div>
    </div>
</div>

<div class="ftc_wzsf">
    <div class="srzfmm_box">
        <div class="qsrzfmm_bt clear_wl">
            <img src="${basePath!}/imgs/ico_cancel.png" class="tx close fl">
            <span class="fl">请输入支付密码</span></div>
        <div class="zfmmxx_shop">
            <div class="zhifu_price"></div>
        </div>
        <ul class="mm_box">
            <li></li><li></li><li></li><li></li><li></li><li></li>
        </ul>
    </div>
    <div class="numb_box">
        <ul class="nub_ggg">
            <li><a href="javascript:void(0);" class="zf_num">1</a></li>
            <li><a href="javascript:void(0);" class="zf_num">2</a></li>
            <li><a href="javascript:void(0);" class="zf_num">3</a></li>
            <li><a href="javascript:void(0);" class="zf_num">4</a></li>
            <li><a href="javascript:void(0);" class="zf_num">5</a></li>
            <li><a href="javascript:void(0);" class="zf_num">6</a></li>
            <li><a href="javascript:void(0);" class="zf_num">7</a></li>
            <li><a href="javascript:void(0);" class="zf_num">8</a></li>
            <li><a href="javascript:void(0);" class="zf_num">9</a></li>
            <li><a href="javascript:void(0);" class="zf_empty">清空</a></li>
            <li><a href="javascript:void(0);" class="zf_num">0</a></li>
            <li><a href="javascript:void(0);" class="zf_del">删除</a></li>
        </ul>
    </div>
    <div class="hbbj"></div>
</div>

<div class="b_btn">
    <a href="javascript:void(0);" class="ljzf_but all_w" style="cursor:pointer">确认支付</a>
</div>


<div id="add_card">
    <div class="add_card-content">
        <div class="add_card-header">
            <img src="${basePath!}/imgs/ico_cancel.png" class="closed" alt="">
        </div>
        <div class="add_card-body" id="yes_card" style="display: none" >
            <ul class="yes_card">

            </ul>

            <div class="wo_btn" onclick="addCard()">
                <a href="Javascript:void(0)" >添加银行卡</a>
            </div>
        </div>
        <div class="no_card" style="display: none">
            <div class="p_text">
                暂未绑卡，请先去添加您的银行卡
            </div>
            <div class="wo_btn" onclick="addCard()">
                <a href="Javascript:void(0)">添加银行卡</a>
            </div>
        </div>
    </div>
</div>
<div class="tishi">

</div>
<div class="bgLodding">
    <div class="lodding">
        <span>数</span>
        <span>据</span>
        <span>提</span>
        <span>交</span>
        <span>中</span>
        <span>.</span>
        <span>.</span>
        <span>.</span>
    </div>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<script>
    var ajax = {
        timeout : 300000,
        type : "POST",
        dataType : "json",
        loadindex:"",
        initProperty : function() {
            ajax.timeout = 300000;
            ajax.type = "POST";
            ajax.dataType = "json";
        },
        loadService : function(url,data,success,error) {
            ajax.loadAjax(url,data,"json",true, success,error);
        },

        loadServiceAsync : function(url,data,success,error) {
            ajax.loadAjax(url,data,"json",false, success,error);
        },
        loadAjax : function(url,data,dataType,async,success,error){

            $.ajax( {
                beforeSend: function(){

                },
                complete: function(){

                },
                url : url,
                timeout : ajax.timeout,
                type : ajax.type,
                dataType : dataType,
                contentType:"application/json",
                async : async,
                data : JSON.stringify(data),
                success :success,
                error : error
            });
        }
    };



    //初始化界面参数
    var pay_payType = window.sessionStorage.getItem("pay_payType");
    var pay_amount = window.sessionStorage.getItem("pay_amount");
    var pay_mredmarketId = window.sessionStorage.getItem("pay_mredmarketId");
    var pay_yh = window.sessionStorage.getItem("pay_yh");
    $("#input_text").val(pay_amount);
    $("#mredmarketId").val(pay_mredmarketId);
    if(pay_yh!=""&&pay_yh!=null){
        $(".top_one .col-xs-6:last-child div").html(pay_yh);
    }
    if(pay_payType=="4"){
        $(".yinlian").css("display","block");
        $(".more_pay").css("display","none");
        $(".my_card").css("display","block");
    }
    var pay_acctno = window.sessionStorage.getItem("pay_acctno");
    var pay_bankname = window.sessionStorage.getItem("pay_bankname");
    var pay_agreeid = window.sessionStorage.getItem("pay_agreeid");
    if(pay_acctno!=""&&pay_acctno!=null){
        pay_acctno=pay_acctno.substring(pay_acctno.length-4,pay_acctno.length);
        $(".card_name").html(pay_bankname+"尾号"+pay_acctno);
        $("#agreeid").val(pay_agreeid);
    }
    var accountInfo = "${mapData.accountInfo!}";
    if(accountInfo==""){
        $(".one_pay").css("display","block");
        $(".sx_pay").css("display","none");
        $("#weixin").attr('checked','true');
    }
    else{
        $(".one_pay").css("display","none");
        $(".sx_pay").css("display","block");
        var balance = "${(((mapData.accountInfo.balance)?number)?string('0.00'))!"0.00"}";
        if(balance=="0.00"&&pay_payType!="4"){
            $("#weixin").attr('checked','true');
        }
    }

    $("input[name='payType'][value='"+pay_payType+"']").attr('checked','true');
    $(document).ready(function(){
        $(document).ajaxStop(function(e) {
            //util.hideLoading();
            //util.hideMainLoading();
        });
        $(document).ajaxError(function(event,jqXHR, textStatus, errorThrown) {
            //util.hideLoading();
            //util.hideMainLoading();
            //util.showErrorMessage("系统异常，请重新登录")
        });

        onTap=function(){
            document.getElementById("mymodal").style.display="block";
            document.getElementById("mymodal").style.background="rgba(0,0,0,0.5)";
            $(".close").click(function(){
                document.getElementById("mymodal").style.display="none";
            })
        };
        $("#input_text").click(function () {
            $(this).parent().find("input").focus();
        });
        $(".ljzf_but").click(function () {
            var amount =$("#input_text").val().trim();
            var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
            if(!reg.test(amount)){
                showDiv("请输入正确的金额");
                return;
            }
            if(amount.length==0||amount==""||amount==null||amount<=0) {
                showDiv("请输入金额");
                return;
            }
            var payType = $("input[name='payType']:checked").val();
            var wallet = "${((mapData.wallet.totalAmount)?string('0.00'))!"0.00"}";
            var balance = "${(((mapData.accountInfo.balance)?number)?string('0.00'))!"0.00"}";
            var openId = "${mapData.openId!}";
            var mredmarketmoney=$("#mredmarketmoney").val();
            var subamount =  accSub(amount*1,mredmarketmoney*1);
            if(payType==2&&((subamount*1)>(wallet*1))){
                showDiv("钱包余额不足");
                return;
            }
            if(payType==1&& ((subamount*1)>(balance*1))) {
                showDiv("授信余额不足");
                return;
            }
            if(payType==3&&(openId==""||openId==null)){
                showDiv("该环境下暂不支持微信支付");
                return;
            }
            var agreeid=$("#agreeid").val();
            if(payType==4&&(agreeid==""||agreeid==null)){
                showDiv("请选择银行卡");
                return;
            }
            if(payType==1||payType==2||payType==4){
                //解绑事件
                $(".nub_ggg li .zf_num").unbind("click");
                var i=0;
                //出现浮动层
                $(".zhifu_price").html("￥"+(subamount))
                $(".ftc_wzsf").show();
                $(".nub_ggg li .zf_del").click(function(){
                    if(i>0){
                        i--
                        $(".mm_box li").eq(i).removeClass("mmdd");
                        $(".mm_box li").eq(i).attr("data","");
                    }
                });
                $(".nub_ggg li .zf_empty").click(function(){
                    $(".mm_box li").removeClass("mmdd");
                    $(".mm_box li").attr("data","");
                    i=0;
                });

                //关闭浮动
                $(".close").click(function(){
                    $(".ftc_wzsf").hide();

                    $(".mm_box li").removeClass("mmdd");
                    $(".mm_box li").attr("data","");
                    i=0;
                });
                $(".nub_ggg li .zf_num").click(function(){
                    if(i<6){
                        $(".mm_box li").eq(i).addClass("mmdd");
                        $(".mm_box li").eq(i).attr("data",$(this).text());
                        i++
                        if (i==6) {
                            var data = "";
                            $(".mm_box li").each(function(){
                                data += $(this).attr("data");
                            });
                            pay(data,payType,amount);
                        };
                    }
                });
            }
            if(payType==3){
                pay("",payType,amount);
                return;
            }
        });
        $(".my_card").click(function () {
            $("#add_card").css("display","block");
            $("#add_card").css("background","rgba(0,0,0,0.5)");
            var url="${basePath!}/bindCard/findbank";
            var data ={};
            data.oid = "${mapData.userId!}";
            ajax.loadService(url,data, function(data){
                if(data.code=="0") {
                    var html = "";
                    if(data.data.length==0){
                        $("#yes_card").css("display","none");
                        $(".no_card").css("display","block");
                    }else{
                        $("#yes_card").css("display","block");
                        $(".no_card").css("display","none");
                    }
                    for (var i in data.data) {
                        var mp = data.data[i];
                        var codeno=mp.bankCard.substring(mp.bankCard.length-4,mp.bankCard.length);
                        var bankName;
                        if(mp.bankName!=null&&mp.bankName!=""&&mp.bankName.length>6){
                            bankName = mp.bankName.substring(mp.bankName.length-6,mp.bankName.length);
                        }else{
                            bankName=mp.bankName;
                        }
                        var bankSn = mp.bankSn;
                        if(bankSn==null||bankSn==""){
                            bankSn ="ico_yinlianzhifu";
                        }
                        html += '<li>';
                        html += '<input type="hidden" value='+mp.contractId+' name="agreeid">';
                        html += '<div class="container-fluid">';
                        html += '<div class="row">';
                        html += '<div class="col-xs-2">';
                        html += '<img src="${basePath!}/imgs/'+bankSn+'.png" alt="">';
                        html += '</div>';
                        html += '<div class="col-xs-5 name_card">';
                        html += ''+bankName+'';
                        html += '</div>';
                        html += '<div class="col-xs-4 nub_card">';
                        html += '尾号'+codeno+'';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</li>';
                    }
                    $("#yes_card>ul").html(html);

                    $(".add_card-body li").click(function () {
                        $("#agreeid").val($(this).find("input[name=agreeid]").val());
                        var name=$(this).text();
                        $(".card_name").html(name);
                        $("#add_card").css("display","none");
                    })
                }
                if(data.code=="1"){
                    showDiv(data.msg);
                }
            });

        });
        $(".closed").click(function () {
            $("#add_card").css("display","none");
        });
        $("#wangyin").click(function () {
            $(".my_card").css("display","block");
        });
        $("#shouxin").click(function () {
            $(".my_card").css("display","none");
            $(".card_name").html("选择银行卡");
            $(".card_nub").html("");
        });
        $("#qianbao").click(function () {
            $(".my_card").css("display","none");
            $(".card_name").html("选择银行卡");
            $(".card_nub").html("");
        });
        $("#weixin").click(function () {
            $(".my_card").css("display","none");
            $(".card_name").html("选择银行卡");
            $(".card_nub").html("");
        });
        $(".nonuse_coupons").click(function () {
            $("#mymodal").css("display","none");
            $(".top_one .col-xs-6 b").html("优惠券");
            $(".top_one .col-xs-6:last-child div").html("选择优惠券");
            $("#mredmarketId").val("");
            $("#mredmarketmoney").val("");

        });
    });


    function accSub(arg1,arg2){
        var r1,r2,m,n;
        try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
        try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
        m=Math.pow(10,Math.max(r1,r2));
        //last modify by deeka
        //动态控制精度长度
        n=(r1>=r2)?r1:r2;
        return ((arg1*m-arg2*m)/m).toFixed(n);
    }


    function selectRedPacketByUserId(){
        var url = "${basePath!}/pay/selectRedPacketByUserId"
        var data = {};
        data.userId="${mapData.userId!}";
        data.mId="${mapData.mId!}";
        var amount =$("#input_text").val().trim();
        var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
        if(!reg.test(amount)){
            showDiv("请输入正确的金额");
            return;
        }
        if(amount.length==0||amount==""||amount==null||amount<=0) {
            showDiv("请先输入金额");
            return;
        }
        data.amount=amount
        ajax.loadService(url,data, function(data){
            if(data.code=="0") {
                var html = "";
                if(data.data.length==0){
                    html+='<div class="zw">'
                    html+='<img src="${basePath!}/imgs/queshenye_youhuijuan.png" style ="width:45%;margin-top:10px" alt="">'
                    html+=' <p>暂无优惠券</p>'
                    html+='</div>'
                }
                for (var i in data.data) {
                    var mp=data.data[i];
                    html += '<li>';
                    html += '<input type="hidden" value='+mp.id+' name="mredmarketId">';
                    html += '<input type="hidden" value='+mp.money+' name="mredmarketmoney">';
                    html += '<div class="one_coupont">';
                    html += '<img src="${basePath!}/imgs/youhuijuan_beij.png" alt="">';
                    html += '</div>';
                    html += '<div class="coupont_content">';
                    html += '<div class="container-fluid">';
                    html += '<div class="row">';
                    html += '<div class="col-xs-3">';
                    html += '<p style="font-size: 3rem;color:#f49110"> <span style="font-size: small">¥</span>'+mp.money+'</p>';
                    html += '<h6 style="color:#f49110">优惠券</h6>';
                    html += '</div>';
                    html += '<div class="col-xs-6">';
                    html += '<p>'+mp.miName+'</p>';
                    html += '<h6>'+mp.createDate+'至'+mp.endDate+'</h6>';
                    html += '</div>';
                    html += '<div class="col-xs-3">';
                    html += '<a href="#">可使用</a>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="row">';
                    html += '<div class="col-xs-12">';
                    html += '满'+mp.fullMoney+'减'+mp.money+'元';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</li>';
                }
                $("#mredmarket").html(html);
                $(".modal li").click(function () {
                    $(".modal li").removeClass("active");
                    $(this).addClass("active");
                    var tent= $(this).find(".coupont_content .col-xs-6>p").html();
                    var yh = $(this).find(".coupont_content .col-xs-12").html();
                    $(".top_one .col-xs-6 b").html("优惠券&nbsp&nbsp&nbsp&nbsp"+tent);
                    $(".top_one .col-xs-6:last-child div").html(yh);
                    $(".modal").css("display","none")
                    $("#mredmarketId").val($(this).find("input[name=mredmarketId]").val());
                    $("#mredmarketmoney").val($(this).find("input[name=mredmarketmoney]").val());
                });
            }
            if(data.code=="1"){
                showDiv(data.msg);
            }
        });

        document.getElementById("mymodal").style.display="block";
        document.getElementById("mymodal").style.background="rgba(0,0,0,0.5)";
        $(".close").click(function(){
            document.getElementById("mymodal").style.display="none";
        })
    };




    function pay(passWord,payType ,amount){
        var url="${basePath!}/pay/onPay";
        var par={};
        par.openId = "${mapData.openId!}";
        par.mId = "${mapData.mId!}";
        par.userId = "${mapData.userId!}";
        par.payType = payType;
        par.amount = amount;
        par.passWord = passWord;
        par.agreeid=$("#agreeid").val();
        par.mredmarketId = $("#mredmarketId").val().trim();
        showWd();
        ajax.loadService(url,par, function(data){
            hidewd();
            if(data.code=="0"&&(payType==3)){
                var jsonObject = JSON.parse(data.data);
                WeixinJSBridge.invoke(
                        'getBrandWCPayRequest', {
                            "appId":""+jsonObject.appId+"",     //公众号名称，由商户传入
                            "timeStamp":""+jsonObject.timeStamp+"",//时间戳，自1970年以来的秒数
                            "nonceStr":""+jsonObject.nonceStr+"", //随机串
                            "package":""+jsonObject.package+"",
                            "signType":""+jsonObject.signType+"",         //微信签名方式：
                            "paySign":""+jsonObject.paySign+"" //微信签名
                        },
                        function(res){
                            // 使用以上方式判断前端返回,郑重提示：res.err_msg将在用户支付成功后返回
                            //  ok，但并不保证它绝对可靠。所以一切支付结果以异步通知为准
                            if(res.err_msg == "get_brand_wcpay_request:ok" ) {
                                window.location.href="${basePath!}/pay/goToPayOk?mId="+par.mId+"&userId="+par.userId+"";
                            }
                        }
                );
            }

            if(data.code=="0"&&(payType==1||payType==2)){
                window.location.href="${basePath!}"+data.data;
            }
            if(data.code=="0"&&payType==4){
                var jsonObject = JSON.parse(data.data);
                if(jsonObject.trxstatus=="0000"){
                    window.location.href="${basePath!}/pay/goToPayOk?mId="+par.mId+"&userId="+par.userId+"";
                    return;
                }
                if(jsonObject.trxstatus=="1999"){
                    window.location.href="${basePath!}/pay/goToPayagreeconfirm?orderid="+jsonObject.orderid+"&thpinfo="+jsonObject.thpinfo+"&agreeid="+par.agreeid+"&mId="+par.mId+"&userId="+par.userId+"";
                }
            }
            if(data.code=="1"){
                showDiv(data.msg);
            }
        });
    }

    function quickPay(payType ,amount){
        var url="${basePath!}/pay/quickPay";
        var par={};
        par.openId = "${mapData.openId!}";
        par.mId = "${mapData.mId!}";
        par.userId = "${mapData.userId!}";
        par.payType = payType;
        par.amount = amount;
        par.mredmarketId = $("#mredmarketId").val().trim();
        ajax.loadService(url,par, function(data){

        });
    }



    function addCard(){
        //将当前页面数据 放入缓存里面
        var payType = $("input[name='payType']:checked").val();
        var amount =$("#input_text").val().trim();
        var pay_mredmarketId = $("#mredmarketId").val().trim();
        var pay_yh = $(".top_one .col-xs-6:last-child div").html();
        var pay_openId ="${mapData.openId!}"; ;
        var pay_state ="${mapData.mId!}"+"_"+"${mapData.userId!}";
        window.sessionStorage.setItem("pay_payType",payType);
        window.sessionStorage.setItem("pay_amount",amount);
        window.sessionStorage.setItem("pay_mredmarketId",pay_mredmarketId);
        window.sessionStorage.setItem("pay_yh",pay_yh);
        window.sessionStorage.setItem("pay_state",pay_state);
        window.sessionStorage.setItem("pay_openId",pay_openId);
        window.location ="${basePath!}/bindCard/goToBindCard?userId=${mapData.userId!}&urlType=1";
    }

    function goTosx(){
        //将当前页面数据 放入缓存里面
        var payType = $("input[name='payType']:checked").val();
        var amount =$("#input_text").val().trim();
        var pay_mredmarketId = $("#mredmarketId").val().trim();
        var pay_yh = $(".top_one .col-xs-6:last-child div").html();
        var pay_openId ="${mapData.openId!}"; ;
        var pay_state ="${mapData.mId!}"+"_"+"${mapData.userId!}";
        window.sessionStorage.setItem("pay_payType",payType);
        window.sessionStorage.setItem("pay_amount",amount);
        window.sessionStorage.setItem("pay_mredmarketId",pay_mredmarketId);
        window.sessionStorage.setItem("pay_yh",pay_yh);
        window.sessionStorage.setItem("pay_state",pay_state);
        window.sessionStorage.setItem("pay_openId",pay_openId);
        window.location ="${mapData.sxUrl!}";
    }

    function showDiv(value) {
        $(".tishi").html(value);
        $(".tishi").css("display","block");
        setTimeout(hideDiv,3000);
    }
    function hideDiv(){
        $(".tishi").css("display","none");
    }
    function showWd() {
        $(".bgLodding").css("display","block");
    }
    function hidewd(){
        $(".bgLodding").css("display","none");
    }
    $(".more_pay").click(function () {
        $(".yinlian").css("display","block");
        $(".more_pay").css("display","none");
    });
</script>
</body>
</html>