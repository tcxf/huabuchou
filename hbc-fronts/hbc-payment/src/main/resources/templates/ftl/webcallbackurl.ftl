<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/share.css">
    <title>分享</title>
</head>
<body>
<div class="share">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                <img src="${localPhoto!}" alt="">
            </div>
            <div class="col-xs-6">
                <h4>${name!}</h4>
            </div>
        </div>
    </div>
    <div class="hail-fellow">
        <a href="#" id="hail-fellow">邀请好友</a>
    </div>
    <div class="go_index">
        <a href="${goindex!}">进入首页</a>
    </div>
    <div class="notice">
        返现须知:
    </div>
    <div class="notice_enter">
        <img src="${basePath!}/imgs/ico_img_all.png" alt="">
    </div>
</div>
<div class="product">
    <ul id="ul">

    </ul>
</div>
<div class="shareModelOne">
    <div class="shareContentOne">
        <div class="shareContent-bodyOne">
            <div class="share_enter">
                <img src="${basePath!}/imgs/jiantou.png" alt="">
            </div>
            <div class="share_img">
                <img src="${basePath!}/imgs/tanchukuang.png" alt="">
            </div>
        </div>
    </div>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/bootstrap.js"></script>
<script src="${basePath!}/js/ajax.js"></script>
<script src="${basePath!}/js/layer/layer.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>



    wx.ready(function(){
        // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
        wx.onMenuShareTimeline({
            title: '花不愁邀请好友领现金红包', //分享标题
            link: '${nearby!}',  // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: '${localPhoto!}', // 分享图标
            success: function () {
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
        wx.onMenuShareAppMessage({
            title: '花不愁邀请好友领现金红包', // 分享标题
            desc: '最高10元大红包，花不愁@你', // 分享描述
            link: '${nearby!}',  // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: '${localPhoto!}', // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () {
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });

    });

    $(document).ready(function() {
        wx.config({
            debug: false,
            appId: "${ticketMap.appId!}",
            timestamp:"${ticketMap.timestamp!}",
            nonceStr:"${ticketMap.noncestr!}",
            signature:"${ticketMap.sign!}",
            jsApiList : [ 'onMenuShareTimeline', 'onMenuShareAppMessage' ]
        });//end_config


        var url = "${basePath!}/packet/loadReadPcke";
        var data = {};
        ajax.loadService(url, data, function (data) {
            if (data.code == "0"){
                var html="";
            for(var i in data.data.records){
                html+='<li>';
                html+='<div class="productList">';
                html+='<img src="'+data.data.records[i].localPhoto+'" alt="">';
                html+='</div>';
                html+='<div class="container-fluid">';
                html+='<div class="row">';
                html+='<div class="col-xs-6">';
                html+='<p>'+data.data.records[i].name+'</p>';
                html+='<h6>满'+data.data.records[i].fullMoney+'减'+data.data.records[i].actualMoney+'元</h6>';
                html+='</div>';
                html+='<div class="col-xs-6">';
                html+='<a href="${basePath!}/pay/goToNearby?mId='+data.data.records[i].miId+'&invateCode=${invateCode!}">去领劵</a>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</li>';
            }
            $("#ul").html(html)
            }
         });

        });

    $("#hail-fellow").click(function () {
        $(".shareModelOne").css("display","block");
    });
    $(".shareModelOne").click(function () {
        $(".shareModelOne").css("display","none");
    });
    var t=setTimeout(" $(\".shareModelOne\").css(\"display\",\"block\");",1000)
</script>
</body>
</html>