package com.tcxf.hbc.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhouyj
 * @date 2018/3/23
 */
@EnableAsync
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.tcxf.hbc.admin", "com.tcxf.hbc.common"})
public class    HbcAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(com.tcxf.hbc.admin.HbcAdminApplication.class, args);
    }
}