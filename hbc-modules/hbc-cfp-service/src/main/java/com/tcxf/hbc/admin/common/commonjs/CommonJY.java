package com.tcxf.hbc.admin.common.commonjs;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IMMerchantCategoryService;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.admin.service.ITbAreaService;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import com.xiaoleilu.hutool.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 地区 银行卡 文件上传....  通用Controller 用于引用common.js
 * jiangyong
 * 2018年7月19日08:37:43
 */
@RestController
@RequestMapping("/common")
public class CommonJY extends BaseController {
    @Autowired
    private ITbAreaService iTbAreaService;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    @Autowired
    private IMMerchantCategoryService iMMerchantCategoryService;

    @Autowired
    protected IOOperaInfoService ioOperaInfoService;

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:43 2018/6/19
     * 加载省市区三级联动
     */
    @RequestMapping("/loadPcd")
    public R findPcdInfo(String parentId)
    {
        List<TbArea> tbAreas = iTbAreaService.selectList(new EntityWrapper<TbArea>().eq("parent_id",parentId));
        R r = new R<>();
        r.setData(tbAreas);
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:45 2018/7/3
     * 根据id查询path路径
     */
    @RequestMapping("/loadArea")
    public R findPcdInfoById(String id)
    {
        TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id",id));
        R r = new R<>();
        r.setData(tbArea);
        return  r;
    }

    /**
     * 文件上传
     *
     * @param file
     * @return
     */

    @PostMapping("/upload")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:39 2018/6/19
     * 加载行业类型下拉选项
     */
    @RequestMapping("/loadIndustryType")
    public R findIndustryType()
    {
        List<MMerchantCategory> mMerchantInfos = iMMerchantCategoryService.selectList(new EntityWrapper<MMerchantCategory>());
        R r = new R<>();
        r.setData(mMerchantInfos);
        return  r;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:00 2018/6/11
     * 查询资金端下面的运营商
     */
    @RequestMapping("/allopera")
    public R findAllOpera(HttpServletRequest request)
    {
        Map<String, Object> paramMap = new HashMap<>();
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        List<OOperaInfo> oOperaInfos = ioOperaInfoService.selectOperaByfid(paramMap);
        R r = new R<>();
        r.setData(oOperaInfos);
        return  r;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:00 2018/6/11
     * 查询所有的运营商
     */
    @RequestMapping("/alloperas")
    public R findAllOpera()
    {
        List<OOperaInfo> oOperaInfos = ioOperaInfoService.selectList(new EntityWrapper<OOperaInfo>().ne("status",2));
        R r = new R<>();
        r.setData(oOperaInfos);
        return  r;
    }

}
