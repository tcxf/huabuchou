package com.tcxf.hbc.admin.common.config;

import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@WebFilter(urlPatterns = "/*")
public class LoginSessionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //HttpSession httpSession = request.getSession();
        BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        RedisTemplate redisTemplate = (RedisTemplate) factory.getBean("redisTemplate");
        String uri = request.getRequestURI();
        if(
                uri.contains("/css/") ||
                uri.contains("/hetong/") ||
                uri.contains("/fonts/") ||
                uri.contains("/images/") ||
                uri.contains("/img/") ||
                uri.contains("/js/") ||
                uri.contains("/kindeditor") ||
                uri.contains("/themes/") ||
                uri.contains("/uploadfile/") ||
                uri.contains("/fund/flogin/login") ||
                uri.contains("/fund/flogin/gologin") ||
                uri.contains("/fund/flogin/test") ||
                uri.contains("/opera/opera/login") ||
                uri.contains("/opera/opera/gologin") ||
                uri.contains("/platfrom/platfrom/login") ||
                uri.contains("/platfrom/platfrom/gologin")||
                uri.contains("/common")||
                uri.contains("/platfrom/user")||
                uri.contains("/platfrom/p_payback")||
                uri.contains("/platfrom/p_merchant")||
                uri.contains("/Settlement/settlement")||
                uri.contains("/merchantInfo/forwardMerchantInfoht")||
                uri.contains("/merchantInfo/forwardMerchantInfoht1")
                        
                )
        {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if(uri.startsWith("/cfp/platfrom"))
        {
            String cookieValue = getCookieValue(request,"session_admin_id");
            //Object obj = httpSession.getAttribute("session_admin_id");
            if(cookieValue !=null)
            {
                Object id = redisTemplate.opsForValue().get(cookieValue);
                //String session_fund_id = (String) obj;
                if (id == null)
                {
                    response.sendRedirect(request.getContextPath()+"/platfrom/platfrom/gologin");
                    return ;
                }else {
                    //httpSession.setAttribute("session_admin_id", obj);
                    filterChain.doFilter(servletRequest, servletResponse);

                    String session_admin_name = getCookieValue(request,"session_admin_name");
                    String session_authority_admin = getCookieValue(request,"session_authority_admin");
                    Object session_admin_name_value = redisTemplate.opsForValue().get(session_admin_name);
                    Object session_authority_admin_value = redisTemplate.opsForValue().get(session_authority_admin);
                    redisTemplate.opsForValue().set(session_admin_name,session_admin_name_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(session_authority_admin,session_authority_admin_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(cookieValue,id,30,TimeUnit.MINUTES);

                    return ;
                }
            }
            else
            {
                response.sendRedirect(request.getContextPath()+"/platfrom/platfrom/gologin");
                return ;
            }
        }

        if(uri.startsWith("/cfp/fund"))
        {
            String cookieValue = getCookieValue(request,"session_fund_id");
            //Object obj = httpSession.getAttribute("session_fund_id");
            if(cookieValue !=null)
            {
                Object id = redisTemplate.opsForValue().get(cookieValue);
                //String session_fund_id = (String) obj;
                if (id == null)
                {
                    response.sendRedirect(request.getContextPath()+"/fund/flogin/gologin");
                    return ;
                }else {
                    //httpSession.setAttribute("session_fund_id", obj);
                    filterChain.doFilter(servletRequest, servletResponse);

                    String session_fund_name = getCookieValue(request,"session_fund_name");
                    String session_authority_fund = getCookieValue(request,"session_authority_fund");
                    Object session_fund_name_value = redisTemplate.opsForValue().get(session_fund_name);
                    Object session_authority_fund_value = redisTemplate.opsForValue().get(session_authority_fund);
                    redisTemplate.opsForValue().set(session_fund_name,session_fund_name_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(session_authority_fund,session_authority_fund_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(cookieValue,id,30,TimeUnit.MINUTES);


                    return ;
                }
            }
            else
            {
                response.sendRedirect(request.getContextPath()+"/fund/flogin/gologin");
                return ;
            }
        }

        if(uri.startsWith("/cfp/opera"))
        {
           // Object obj = httpSession.getAttribute("session_opera_id");
            String cookieValue = getCookieValue(request,"session_opera_id");
            if(cookieValue !=null)
            {
                Object id = redisTemplate.opsForValue().get(cookieValue);
                //String session_fund_id = (String) obj;
                if (id == null)
                {
                    response.sendRedirect(request.getContextPath()+"/opera/opera/gologin");
                    return ;
                }else {
                    //httpSession.setAttribute("session_opera_id", obj);
                    filterChain.doFilter(servletRequest, servletResponse);

                    String session_opera_name = getCookieValue(request,"session_opera_name");
                    String session_authority_opera = getCookieValue(request,"session_authority_opera");
                    Object session_opera_name_value = redisTemplate.opsForValue().get(session_opera_name);
                    Object session_authority_opera_value = redisTemplate.opsForValue().get(session_authority_opera);
                    redisTemplate.opsForValue().set(session_opera_name,session_opera_name_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(session_authority_opera,session_authority_opera_value,30,TimeUnit.MINUTES);
                    redisTemplate.opsForValue().set(cookieValue,id,30,TimeUnit.MINUTES);
                    return ;
                }
            }
            else
            {
                response.sendRedirect(request.getContextPath()+"/opera/opera/gologin");
                return ;
            }
        }

    }

    @Override
    public void destroy() {

    }
    private String getCookieValue(HttpServletRequest request, String name){
        Map<String,Cookie> map = ReadCookieMap(request);
        Cookie cookieValue = map.get(name);
        if(ValidateUtil.isEmpty(cookieValue)){
            return null;
        }
        return cookieValue.getValue();
    }

    private Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
        Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
        Cookie[] cookies = request.getCookies();
        if(null!=cookies){
            for(Cookie cookie : cookies){
                cookieMap.put(cookie.getName(), cookie);
            }
        }
        return cookieMap;
    }
}
