//package com.tcxf.hbc.admin.common.pdf;
//
//import java.io.*;
//import java.util.Map;
//import java.util.Random;
//
//import com.itextpdf.text.pdf.BaseFont;;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.Resource;
//import org.springframework.util.ResourceUtils;
//import org.xhtmlrenderer.pdf.ITextRenderer;
//import freemarker.template.Configuration;
//import freemarker.template.Template;
//
//import javax.servlet.http.HttpServletResponse;
//
//
//public class PdfHandlers {
//
//    /**
//     * 随机生成数字
//     */
//    public static int randoms() {
//        StringBuilder str = new StringBuilder();//定义变长字符串
//        Random random = new Random();
//        //随机生成数字，并添加到字符串
//        for (int i = 0; i < 8; i++) {
//            str.append(random.nextInt(10));
//        }
//        //将字符串转换为数字并输出
//        int num = Integer.parseInt(str.toString());
//        return num;
//    }
//
//    private static final String CONTRACT = "";//合同文件存储路径
//    private static final String TEMPLATE = "";//模板存储路径
//
//    private static final String PDFNAME = randoms() + "hetong";//pdf文件名
//    private static final String HTMLNAME = "hetong";//html文件名
//
//
//    public static String contractHandler(String templateName,
//                                         Map <String, Object> paramMap, HttpServletResponse response) throws Exception {
//        // 获取本地模板存储路径、合同文件存储路径
//        String templatePath = TEMPLATE;
//        String contractPath = CONTRACT;
//        // 组装html和pdf合同的全路径URL
//        String localHtmlUrl = contractPath + HTMLNAME + ".html";
//        String localPdfPath = contractPath + "/";
//        // 判断本地路径是否存在如果不存在则创建
//        File localFile = ResourceUtils.getFile("classpath:static/"+HTMLNAME+"");
////        Resource fileRource = new ClassPathResource("hetong");
////        File localFile = (File) fileRource;
//        if (!localFile.exists()) {
//            localFile.mkdirs();
//        }
//
//
//        String localPdfUrl = localFile + "/" + PDFNAME + ".pdf";
//        templateName = templateName + ".ftl";
//        htmHandler(templatePath, templateName, localHtmlUrl, paramMap);// 生成html合同
//        pdfHandler(localHtmlUrl, localPdfUrl, response);// 根据html合同生成pdf合同
//        deleteFile(localHtmlUrl);// 删除html格式合同
//        System.out.println("PDF生成成功" + localPdfUrl);
//        return HTMLNAME+"/"+PDFNAME + ".pdf";
//    }
//
//    /**
//     * 生成html格式合同
//     */
//    private static void htmHandler(String templatePath, String templateName,
//                                   String htmUrl, Map <String, Object> paramMap) throws Exception {
//
//        File file = ResourceUtils.getFile("classpath:templates");
//        if (file.exists()) {
//            File[] files = file.listFiles();
//            if (files != null) {
//                for (File childFile : files) {
//                    System.out.println(childFile.getName());
//                }
//            }
//        }
//        templatePath = file.getPath();
//        Configuration cfg = new Configuration();
//        cfg.setDefaultEncoding("UTF-8");
//        cfg.setDirectoryForTemplateLoading(new File(templatePath));
//
//        Template template = cfg.getTemplate(templateName);
//
//        File outHtmFile = new File(htmUrl);
//
//        Writer out = new BufferedWriter(new OutputStreamWriter(
//                new FileOutputStream(outHtmFile)));
//        template.process(paramMap, out);
//
//        out.close();
//    }
//
//    /**
//     * 生成pdf格式合同
//     */
//    private static void pdfHandler(String htmUrl, String pdfUrl, HttpServletResponse response) {
//        File htmFile = null;
//        File pdfFile = null;
//        OutputStream os = null;
//        try {
//            htmFile = new File(htmUrl);
//            pdfFile = new File(pdfUrl);
//            String url = htmFile.toURI().toURL().toString();
//
//            os = new FileOutputStream(pdfFile);
//
//            org.xhtmlrenderer.pdf.ITextRenderer renderer = new ITextRenderer();
//            renderer.setDocument(url);
//
//            org.xhtmlrenderer.pdf.ITextFontResolver fontResolver = renderer
//                    .getFontResolver();
//            // 解决中文支持问题
//            fontResolver.addFont(ResourceUtils.getFile("classpath:templates").getPath() + "/simsun.ttc",
//                    BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//
//            renderer.layout();
//            renderer.createPDF(os);
//
//        } catch (Exception e) {
//            e.getMessage();
//        } finally {
//            try {
//                os.close();
//            } catch (Exception e) {
//                e.getMessage();
//            }
//
//        }
//    }
//
//    /**
//     * 删除文件
//     */
//    private static void deleteFile(String fileUrl) {
//        File file = new File(fileUrl);
//        file.delete();
//    }
//
//}
