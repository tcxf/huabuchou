package com.tcxf.hbc.admin.common.pdf;
import com.itextpdf.text.pdf.BaseFont;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import org.springframework.util.ResourceUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;
import java.util.Random;

public  class PdfHandlerss {

    /**
     * 随机生成数字
     */
    public static int randoms() {
        StringBuilder str = new StringBuilder();//定义变长字符串
        Random random = new Random();
        //随机生成数字，并添加到字符串
        for (int i = 0; i < 8; i++) {
            str.append(random.nextInt(10));
        }
        //将字符串转换为数字并输出
        int num = Integer.parseInt(str.toString());
        return num;
    }


    public static String getReturnData(String urlString) throws UnsupportedEncodingException {
        String res = "";
        try {
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line;
            }
            in.close();

        } catch (Exception e) {
           e.printStackTrace();
        }

        return res;
    }

    public static String contractHandler(HttpServletRequest request, FastFileStorageClient fastFileStorageClient, FdfsPropertiesConfig fdfsPropertiesConfig) throws Exception {
        //获取域名
        StringBuffer geturl = request.getRequestURL();
        String tempContextUrl = geturl.delete(geturl.length() - request.getRequestURI().length(), geturl.length()).append(request.getServletContext().getContextPath()).append("/").toString();
        String id = request.getParameter("id");
        String url = tempContextUrl+"/opera/merchantInfo/forwardMerchantInfoht1?id="+id;
        String res = getReturnData(url);
        String pdfPath = pdfHandler(res,fastFileStorageClient);// 根据html合同生成pdf合同
        System.out.println("PDF生成成功" + pdfPath);
        return fdfsPropertiesConfig.getFileHost() + pdfPath;
    }


    /**
     * 生成pdf格式合同
     */
    private static String pdfHandler(String htmUrl,FastFileStorageClient fastFileStorageClient) {
        File pdfFile = null;
        try {
            org.xhtmlrenderer.pdf.ITextRenderer renderer = new ITextRenderer();
            System.out.println(htmUrl);
            renderer.setDocumentFromString(htmUrl);
            ///renderer.setDocument(htmUrl);

            org.xhtmlrenderer.pdf.ITextFontResolver fontResolver = renderer
                    .getFontResolver();
            // 解决中文支持问题
            String _path = System.getProperty("user.dir");
            fontResolver.addFont(_path + "/templates/simsun.ttc",
                    BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            renderer.layout();
            ByteArrayOutputStream out1 = new ByteArrayOutputStream(1024);
            renderer.createPDF(out1);

            byte[] content = out1.toByteArray();
            StorePath storePath = fastFileStorageClient.uploadFile(content, "pdf");

            return  storePath.getFullPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 删除文件
     */
    private static void deleteFile(String fileUrl) {
        File file = new File(fileUrl);
        file.delete();
    }

}
