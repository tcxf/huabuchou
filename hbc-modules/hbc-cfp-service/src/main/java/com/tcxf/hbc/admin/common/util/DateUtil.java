package com.tcxf.hbc.admin.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;


/**
 * 日期处理工具类
 * </br>
 *
 * @author:wenz
 * @verion:1.0
 * @date
 * @version:1.0
 * @date:2017年5月26日下午2:49:38
 * @since JDK1.8 Spring4.0.4.RELEASE Copyright (C) 2016 Allrights reserved http://www.wisdom-info.cn/
 */
public class DateUtil extends DateUtils{
  
	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);
    private static String defaultDatePattern = null;
    public static String timePattern = "HH:mm";
    /** 日期格式yyyy-MM字符串常量 */
    public static final String MONTH_FORMAT = "yyyy-MM";
    /** 日期格式yyyy-MM-dd字符串常量 */
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_SHORT = "yyyyMMdd";
    /** 日期格式HH:mm:ss字符串常量 */
    public static final String HOUR_FORMAT = "HH:mm:ss";
    /** 日期格式yyyy-MM-dd HH:mm:ss字符串常量 */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /** 某天开始时分秒字符串常量  00:00:00 */
    public static final String DAY_BEGIN_STRING_HHMMSS = " 00:00:00";
    
    /** 日期格式HH:mm:ss字符串常量 */  
    private static final String HOUR = "HH";  
    /**
     * 某天结束时分秒字符串常量  23:59:59
     */
    public static final String DAY_END_STRING_HHMMSS = " 23:59:59";
    public static SimpleDateFormat sdf_date_format = new SimpleDateFormat(DATE_FORMAT);
    public static SimpleDateFormat sdf_hour_format = new SimpleDateFormat(HOUR_FORMAT);
    public static SimpleDateFormat hour_format = new SimpleDateFormat(HOUR);
    public static SimpleDateFormat sdf_datetime_format = new SimpleDateFormat(DATETIME_FORMAT);
    public static SimpleDateFormat sdf_date_format_short = new SimpleDateFormat(DATE_FORMAT_SHORT);


    /**
     * Instantiates a new Date util.
     */
    public DateUtil() {
    }


    /**
     * Gets curr calendar.
     *
     * @return the curr calendar
     */
    public static Calendar getCurrCalendar()
    {
        return  Calendar.getInstance();
    }


    /***
     * 时间格式化
     * @param date  日期
     * @param format  格式化
     * @return
     */
    public  static  String  getDateFormat(Date date,String format)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return  simpleDateFormat.format(date);
    }


    /***
     * 格式化的时间到毫秒
     * @return
     */
    public synchronized   static  String  getCurrDateTimeMillFormat()
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return  simpleDateFormat.format(new Date());
    }




    /**
     * Gets date.
     *
     * @param dateStr the date str
     * @return the date
     */
    public static Date getDate(String dateStr)
    {
    	if(null!=dateStr)
    	{
    		try {
    			if(dateStr.length()==19)
    			{
    				if(dateStr.indexOf("/")>0)
    				{
    					return sdf_datetime_format.parse(dateStr.replaceAll("/", "-"));
    				}
    				return sdf_datetime_format.parse(dateStr);
    			}else if(dateStr.length()==10)
    			{
    				return sdf_date_format.parse(dateStr);
    			}else if(dateStr.length()==8){
    				return sdf_hour_format.parse(dateStr);
    			}
			} catch (Exception e) {
				logger.error("格式化时间错误", e);
			}
    	}
    	return null;
    }
    
    /**
     * Gets date.
     *
     * @param dateStr the date str
     * @return the date
     */
    public static Date getDate2(String dateStr)
    {
    	if(null!=dateStr)
    	{
    		try {
    			if(dateStr.length()==19||dateStr.length()==21)
    			{
    				if(dateStr.indexOf("/")>0)
    				{
    					return sdf_datetime_format.parse(dateStr.replaceAll("/", "-"));
    				}
    				return sdf_datetime_format.parse(dateStr);
    			}else if(dateStr.length()==10)
    			{
    				return sdf_date_format.parse(dateStr);
    			}else if(dateStr.length()==8){
    				return sdf_hour_format.parse(dateStr);
    			}
			} catch (Exception e) {
				logger.error("格式化时间错误", e);
			}
    	}
    	return null;
    }



    /**
     * 获得服务器当前日期及时间，以格式为：yyyy-MM-dd HH:mm:ss的日期字符串形式返回
     *
     * @return date time
     * @date Mar 11, 2012
     */
    public static String getDateTime() {
        try {  
            return sdf_datetime_format.format(getCurrCalendar().getTime());
        } catch (Exception e) {  
            logger.debug("DateUtil.getDateTime():" + e.getMessage());  
            return "";  
        }  
    }
    
    
    
    /**
     * 以格式为yyyy-MM-dd的日期字符串形式返回
     *
     * @return date time
     * @date Mar 11, 2012
     */
    public static String getDateTimeforManDay(Date date) {
        try {  
            return sdf_date_format.format(date);
        } catch (Exception e) {  
            logger.debug("DateUtil.getDateTime():" + e.getMessage());  
            return "";  
        }  
    }
    
    
    /**
     * 以格式为HH的日期字符串形式返回
     *
     * @return date time
     * @date Mar 11, 2012
     */
    public static String getHourTime(Date date) {
        try {  
            return hour_format.format(date);
        } catch (Exception e) {  
            logger.debug("DateUtil.getDateTime():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
     *
     * @return date long
     */
    public static Date getDateLong() {
	     ParsePosition pos = new ParsePosition(0);
	     Date strtodate = sdf_datetime_format.parse(getDateTime(), pos);
	     return strtodate;
	  }


    /**
     * 获得服务器当前日期，以格式为：yyyy-MM-dd的日期字符串形式返回
     *
     * @return date
     * @date Mar 11, 2012
     */
    public static String getDate() {
        try {  
            return sdf_date_format.format(getCurrCalendar().getTime());
        } catch (Exception e) {  
            logger.debug("DateUtil.getDate():" + e.getMessage());  
            return "";  
        }  
    }
    /**
     * 获得服务器当前日期，以格式为：yyyyMMdd的日期字符串形式返回
     *
     * @return date
     * @date Mar 11, 2012
     */
    public static String getDateShort() {
        try {
            return sdf_date_format_short.format(getCurrCalendar().getTime());
        } catch (Exception e) {
            logger.debug("DateUtil.getDate():" + e.getMessage());
            return "";
        }
    }

    /**
     * 获得服务器当前时间，以格式为：HH:mm:ss的日期字符串形式返回
     *
     * @return time
     * @date Mar 11, 2012
     */
    public static String getTime() {
        String temp = " ";  
        try {  
            temp += sdf_hour_format.format(getCurrCalendar().getTime());
            return temp;  
        } catch (Exception e) {  
            logger.debug("DateUtil.getTime():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 统计时开始日期的默认值
     *
     * @return start date
     * @date Mar 11, 2012
     */
    public static String getStartDate() {
        try {  
            return getYear() + "-01-01";  
        } catch (Exception e) {  
            logger.debug("DateUtil.getStartDate():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 统计时结束日期的默认值
     *
     * @return end date
     * @date Mar 11, 2012
     */
    public static String getEndDate() {
        try {  
            return getDate();  
        } catch (Exception e) {  
            logger.debug("DateUtil.getEndDate():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 获得服务器当前日期的年份
     *
     * @return year
     * @date Mar 11, 2012
     */
    public static String getYear() {
        try {  
            return String.valueOf(getCurrCalendar().get(Calendar.YEAR));
        } catch (Exception e) {  
            logger.debug("DateUtil.getYear():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 获得服务器当前日期的月份
     *
     * @return month
     * @date Mar 11, 2012
     */
    public static String getMonth() {
        try {  
            java.text.DecimalFormat df = new java.text.DecimalFormat();  
            df.applyPattern("00;00");  
            return df.format((getCurrCalendar().get(Calendar.MONTH) + 1));
        } catch (Exception e) {  
            logger.debug("DateUtil.getMonth():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 获得服务器在当前月中天数
     *
     * @return day
     * @date Mar 11, 2012
     */
    public static String getDay() {
        try {  
            return String.valueOf(getCurrCalendar().get(Calendar.DAY_OF_MONTH));
        } catch (Exception e) {  
            logger.debug("DateUtil.getDay():" + e.getMessage());  
            return "";  
        }  
    }

    /**
     * 比较两个日期相差的天数
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return margin
     * @date Mar 11, 2012
     */
    public static int getMargin(String date1, String date2) {
        int margin;  
        try {  
            ParsePosition pos = new ParsePosition(0);  
            ParsePosition pos1 = new ParsePosition(0);  
            Date dt1 = sdf_date_format.parse(date1, pos);  
            Date dt2 = sdf_date_format.parse(date2, pos1);  
            long l = dt1.getTime() - dt2.getTime();  
            margin = (int) (l / (24 * 60 * 60 * 1000));  
            return margin;  
        } catch (Exception e) {  
            logger.debug("DateUtil.getMargin():" + e.toString());  
            return 0;  
        }  
    }

    /**
     * 比较两个日期相差的天数
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return double margin
     * @date Mar 11, 2012
     */
    public static double getDoubleMargin(String date1, String date2) {
        double margin;  
        try {  
            ParsePosition pos = new ParsePosition(0);  
            ParsePosition pos1 = new ParsePosition(0);  
            Date dt1 = sdf_datetime_format.parse(date1, pos);  
            Date dt2 = sdf_datetime_format.parse(date2, pos1);  
            long l = dt1.getTime() - dt2.getTime();  
            margin = (l / (24 * 60 * 60 * 1000.00));  
            return margin;  
        } catch (Exception e) {  
            logger.debug("DateUtil.getMargin():" + e.toString());  
            return 0;  
        }  
    }

    /**
     * 比较两个日期相差的月数
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return month margin
     * @date Mar 11, 2012
     */
    public static int getMonthMargin(String date1, String date2) {
        int margin;  
        try {  
            margin = (Integer.parseInt(date2.substring(0, 4)) - Integer.parseInt(date1.substring(0, 4))) * 12;  
            margin += (Integer.parseInt(date2.substring(4, 7).replaceAll("-0",  
                    "-")) - Integer.parseInt(date1.substring(4, 7).replaceAll("-0", "-")));  
            return margin;  
        } catch (Exception e) {  
            logger.debug("DateUtil.getMargin():" + e.toString());  
            return 0;  
        }  
    }

    /**
     * 返回日期加X天后的日期
     *
     * @param date the date
     * @param i    the
     * @return string
     * @date Mar 11, 2012
     */
    public static String addDay(String date, int i) {
        try {  
            GregorianCalendar gCal = new GregorianCalendar(  
                    Integer.parseInt(date.substring(0, 4)),   
                    Integer.parseInt(date.substring(5, 7)) - 1,   
                    Integer.parseInt(date.substring(8, 10)));  
            gCal.add(GregorianCalendar.DATE, i);  
            return sdf_date_format.format(gCal.getTime());  
        } catch (Exception e) {  
            logger.debug("DateUtil.addDay():" + e.toString());  
            return getDate();  
        }  
    }

    /**
     * 返回日期加X月后的日期
     *
     * @param date the date
     * @param i    the
     * @return string
     * @date Mar 11, 2012
     */
    public static String addMonth(String date, int i) {
        try {  
            GregorianCalendar gCal = new GregorianCalendar(  
                    Integer.parseInt(date.substring(0, 4)),   
                    Integer.parseInt(date.substring(5, 7)) - 1,   
                    Integer.parseInt(date.substring(8, 10)));  
            gCal.add(GregorianCalendar.MONTH, i);  
            return sdf_date_format.format(gCal.getTime());  
        } catch (Exception e) {  
            logger.debug("DateUtil.addMonth():" + e.toString());  
            return getDate();  
        }  
    }

    /**
     * 返回日期加X年后的日期
     *
     * @param date the date
     * @param i    the
     * @return string
     * @date Mar 11, 2012
     */
    public static String addYear(String date, int i) {
        try {  
            GregorianCalendar gCal = new GregorianCalendar(  
                    Integer.parseInt(date.substring(0, 4)),   
                    Integer.parseInt(date.substring(5, 7)) - 1,   
                    Integer.parseInt(date.substring(8, 10)));  
            gCal.add(GregorianCalendar.YEAR, i);  
            return sdf_date_format.format(gCal.getTime());  
        } catch (Exception e) {  
            logger.debug("DateUtil.addYear():" + e.toString());  
            return "";  
        }  
    }

    /**
     * 返回某年某月中的最大天
     *
     * @param iyear  the iyear
     * @param imonth the imonth
     * @return max day
     * @date Mar 11, 2012
     */
    public static int getMaxDay(int iyear, int imonth) {
        int day = 0;  
        try {  
            if (imonth == 1 || imonth == 3 || imonth == 5 || imonth == 7  
                    || imonth == 8 || imonth == 10 || imonth == 12) {  
                day = 31;  
            } else if (imonth == 4 || imonth == 6 || imonth == 9 || imonth == 11) {  
                day = 30;  
            } else if ((0 == (iyear % 4)) && (0 != (iyear % 100)) || (0 == (iyear % 400))) {  
                day = 29;  
            } else {  
                day = 28;  
            }  
            return day;  
        } catch (Exception e) {  
            logger.debug("DateUtil.getMonthDay():" + e.toString());  
            return 1;  
        }  
    }

    /**
     * 格式化日期
     *
     * @param orgDate the org date
     * @param Type    the type
     * @param Span    the span
     * @return string
     * @date Mar 11, 2012
     */
    @SuppressWarnings("static-access")
    public String rollDate(String orgDate, int Type, int Span) {  
        try {  
            String temp = "";  
            int iyear, imonth, iday;  
            int iPos = 0;  
            char seperater = '-';  
            if (orgDate == null || orgDate.length() < 6) {  
                return "";  
            }  
  
            iPos = orgDate.indexOf(seperater);  
            if (iPos > 0) {  
                iyear = Integer.parseInt(orgDate.substring(0, iPos));  
                temp = orgDate.substring(iPos + 1);  
            } else {  
                iyear = Integer.parseInt(orgDate.substring(0, 4));  
                temp = orgDate.substring(4);  
            }  
  
            iPos = temp.indexOf(seperater);  
            if (iPos > 0) {  
                imonth = Integer.parseInt(temp.substring(0, iPos));  
                temp = temp.substring(iPos + 1);  
            } else {  
                imonth = Integer.parseInt(temp.substring(0, 2));  
                temp = temp.substring(2);  
            }  
  
            imonth--;  
            if (imonth < 0 || imonth > 11) {  
                imonth = 0;  
            }  
  
            iday = Integer.parseInt(temp);  
            if (iday < 1 || iday > 31)  
                iday = 1;  
  
            Calendar orgcale = Calendar.getInstance();  
            orgcale.set(iyear, imonth, iday);  
            temp = this.rollDate(orgcale, Type, Span);  
            return temp;  
        } catch (Exception e) {  
            return "";  
        }  
    }

    /**
     * Roll date string.
     *
     * @param cal  the cal
     * @param Type the type
     * @param Span the span
     * @return the string
     */
    public static String rollDate(Calendar cal, int Type, int Span) {
        try {  
            String temp = "";  
            Calendar rolcale;  
            rolcale = cal;  
            rolcale.add(Type, Span);  
            temp = sdf_date_format.format(rolcale.getTime());  
            return temp;  
        } catch (Exception e) {  
            return "";  
        }  
    }

    /**
     * 返回默认的日期格式
     *
     * @return date pattern
     * @date Mar 11, 2012
     */
    public static synchronized String getDatePattern() {
        defaultDatePattern = "yyyy-MM-dd";  
        return defaultDatePattern;  
    }
    
    public static synchronized String getDatePattern1() {
        defaultDatePattern = "yyyy-MM-dd HH:mm:ss";  
        return defaultDatePattern;  
    }

    /**
     * 将指定日期按默认格式进行格式代化成字符串后输出如：yyyy-MM-dd
     *
     * @param aDate the a date
     * @return date
     * @date Mar 11, 2012
     */
    public static final String getDate(Date aDate) {
        SimpleDateFormat df = null;  
        String returnValue = "";  
        if (aDate != null) {  
            df = new SimpleDateFormat(getDatePattern());  
            returnValue = df.format(aDate);  
        }  
        return (returnValue);  
    }

    
    public static final String getDateHavehms(Date aDate) {
        SimpleDateFormat df = null;  
        String returnValue = "";  
        if (aDate != null) {  
            df = new SimpleDateFormat(getDatePattern1());  
            returnValue = df.format(aDate);  
        }  
        return (returnValue);  
    }
    /**
     * 取得给定日期的时间字符串，格式为当前默认时间格式
     *
     * @param theTime the the time
     * @return time now
     * @date Mar 11, 2012
     */
    public static String getTimeNow(Date theTime) {
        return getDateTime(timePattern, theTime);  
    }

    /**
     * 取得当前时间的Calendar日历对象
     *
     * @return today
     * @throws ParseException the parse exception
     * @date Mar 11, 2012
     */
    public Calendar getToday() throws ParseException {
        Date today = new Date();  
        SimpleDateFormat df = new SimpleDateFormat(getDatePattern());  
        String todayAsString = df.format(today);  
        Calendar cal = new GregorianCalendar();  
        cal.setTime(convertStringToDate(todayAsString));  
        return cal;  
    }

    /**
     * 将日期类转换成指定格式的字符串形式
     *
     * @param aMask the a mask
     * @param aDate the a date
     * @return date time
     * @date Mar 11, 2012
     */
    public static final String getDateTime(String aMask, Date aDate) {
        SimpleDateFormat df = null;  
        String returnValue = "";  
  
        if (aDate == null) {  
            logger.error("aDate is null!");  
        } else {  
            df = new SimpleDateFormat(aMask);  
            returnValue = df.format(aDate);  
        }  
        return (returnValue);  
    }

    /**
     * 将指定的日期转换成默认格式的字符串形式
     *
     * @param aDate the a date
     * @return string
     * @date Mar 11, 2012
     */
    public static final String convertDateToString(Date aDate) {
        return getDateTime(getDatePattern(), aDate);  
    }

    /**
     * 将日期字符串按指定格式转换成日期类型
     *
     * @param aMask   指定的日期格式，如:yyyy-MM-dd
     * @param strDate 待转换的日期字符串
     * @return date
     * @throws ParseException the parse exception
     * @date Mar 11, 2012
     */
    public static final Date convertStringToDate(String aMask, String strDate)
            throws ParseException {  
        SimpleDateFormat df = null;  
        Date date = null;  
        df = new SimpleDateFormat(aMask);  
  
        if (logger.isDebugEnabled()) {  
            logger.debug("converting '" + strDate + "' to date with mask '" + aMask + "'");  
        }  
        try {  
            date = df.parse(strDate);  
        } catch (ParseException pe) {  
            logger.error("ParseException: " + pe);  
            throw pe;  
        }  
        return (date);  
    }

    /**
     * 将日期字符串按默认格式转换成日期类型
     *
     * @param strDate the str date
     * @return date
     * @throws ParseException the parse exception
     * @date Mar 11, 2012
     */
    public static Date convertStringToDate(String strDate)
            throws ParseException {  
        Date aDate = null;  
  
        try {  
            if (logger.isDebugEnabled()) {  
                logger.debug("converting date with pattern: " + getDatePattern());  
            }  
            aDate = convertStringToDate(getDatePattern(), strDate);  
        } catch (ParseException pe) {  
            logger.error("Could not convert '" + strDate + "' to a date, throwing exception");  
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());  
        }  
        return aDate;  
    }

    /**
     * 返回一个JAVA简单类型的日期字符串
     *
     * @return simple date format
     * @date Mar 11, 2012
     */
    public static String getSimpleDateFormat() {
        SimpleDateFormat formatter = new SimpleDateFormat();  
        String NDateTime = formatter.format(new Date());  
        return NDateTime;  
    }

    /**
     * 将指定字符串格式的日期与当前时间比较
     *
     * @param strDate 需要比较时间
     * @return <p>       int code       <ul>       <li>-1 当前时间 < 比较时间 </li>       <li> 0 当前时间 = 比较时间 </li>       <li>>=1当前时间 > 比较时间 </li>       </ul>       </p>
     * @date Feb 17, 2012
     */
    public static int compareToCurTime (String strDate) {
        if (StringUtils.isBlank(strDate)) {  
            return -1;  
        }  
        Date curTime = getCurrCalendar().getTime();
        String strCurTime = null;  
        try {  
            strCurTime = sdf_datetime_format.format(curTime);  
        } catch (Exception e) {  
            if (logger.isDebugEnabled()) {  
                logger.debug("[Could not format '" + strDate + "' to a date, throwing exception:" + e.getLocalizedMessage() + "]");  
            }  
        }  
        if (StringUtils.isNotBlank(strCurTime)) {  
            return strCurTime.compareTo(strDate);  
        }  
        return -1;  
    }

    /**
     * 为查询日期添加最小时间
     *
     * @param param the param
     * @return date
     */
    @SuppressWarnings("deprecation")
    public static Date addStartTime(Date param) {  
        Date date = param;  
        try {  
            date.setHours(0);  
            date.setMinutes(0);  
            date.setSeconds(0);  
            return date;  
        } catch (Exception ex) {  
            return date;  
        }  
    }

    /**
     * 为查询日期添加最大时间
     *
     * @param param the param
     * @return date
     */
    @SuppressWarnings("deprecation")
    public static Date addEndTime(Date param) {  
        Date date = param;  
        try {  
            date.setHours(23);  
            date.setMinutes(59);  
            date.setSeconds(59);
            return date;  
        } catch (Exception ex) {  
            return date;  
        }  
    }

    /**
     * 返回系统现在年份中指定月份的天数
     *
     * @param month the month
     * @return 指定月的总天数 month last day
     */
    @SuppressWarnings("deprecation")
    public static String getMonthLastDay(int month) {  
        Date date = new Date();  
        int[][] day = { { 0, 30, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },  
                { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };  
        int year = date.getYear() + 1900;  
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {  
            return day[1][month] + "";  
        } else {  
            return day[0][month] + "";  
        }  
    }

    /**
     * 返回指定年份中指定月份的天数
     *
     * @param year  the year
     * @param month the month
     * @return 指定月的总天数 month last day
     */
    public static String getMonthLastDay(int year, int month) {
        int[][] day = { { 0, 30, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },  
                { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };  
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {  
            return day[1][month] + "";  
        } else {  
            return day[0][month] + "";  
        }  
    }

    /**
     * 判断是平年还是闰年
     *
     * @param year the year
     * @return boolean
     * @date Mar 11, 2012
     */
    public static boolean isLeapyear(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0) {  
            return true;  
        } else {  
            return false;  
        }  
    }

    /**
     * 取得当前时间的日戳
     *
     * @return timestamp
     * @date Mar 11, 2012
     */
    @SuppressWarnings("deprecation")
    public static String getTimestamp() {  
        Date date = getCurrCalendar().getTime();
        String timestamp = "" + (date.getYear() + 1900) + date.getMonth()  
                + date.getDate() + date.getMinutes() + date.getSeconds()  
                + date.getTime();  
        return timestamp;  
    }

    /**
     * 取得指定时间的日戳
     *
     * @param date the date
     * @return timestamp
     */
    @SuppressWarnings("deprecation")
    public static String getTimestamp(Date date) {  
        String timestamp = "" + (date.getYear() + 1900) + date.getMonth()  
                + date.getDate() + date.getMinutes() + date.getSeconds()  
                + date.getTime();  
        return timestamp;  
    }
    //判断闰年
    public static boolean isLeap(int year)
    {
        if (((year % 100 == 0) && year % 400 == 0) || ((year % 100 != 0) && year % 4 == 0)){
            return true;
        }
        else{
            return false;
        }
    }

    //返回当月天数
    public static int getDays(int year, int month)
    {
        int days;
        int febDay = 28;
        if (isLeap(year)){
            febDay = 29;
        }
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                days = 30;
                break;
            case 2:
                days = febDay;
                break;
            default:
                days = 0;
                break;
        }
        return days;
    }



    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
    	System.out.println(getBeginDayOfYesterday());
    	System.out.println(getEndDayOfYesterDay());
    }  
    
    //获得当天开始时间 
    public static String getTimesmorning() {  
        Calendar cal = Calendar.getInstance();  
        cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.MILLISECOND, 0);  
        String format = sdf_datetime_format.format(cal.getTime());
        return format;
    }  
  
    // 获得当天结束时间  
    public static String getTimesnight() {  
        Calendar cal = Calendar.getInstance();  
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.MILLISECOND, 999);
        String format = sdf_datetime_format.format(cal.getTime());
        return format;
    }

    
    /**
     *@describe 
     *@parameter 
     *@return  
     *@author  Jiangyong
     *@Creationtime 2018/10/10
     *@Modifier
     */
    //获取当天的开始时间
    public static Date getDayBegin() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    //获取当天的结束时间
    public static Date getDayEnd() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    //获取昨天的开始时间
    public static String getBeginDayOfYesterday() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayBegin());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String format = sdf_datetime_format.format(cal.getTime());
        return format;
    }

    //获取昨天的结束时间
    public static String getEndDayOfYesterDay() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getDayEnd());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String format = sdf_datetime_format.format(cal.getTime());
        return format;
    }
    /**
     *@describe 
     *@parameter
     *@return  
     *@author  Jiangyong
     *@Creationtime 2018/10/10
     *@Modifier
     */



    public static HashMap<String, Object> publicMethodForTime(int timeTime){
        HashMap<String, Object> hashMap = new HashMap<>();
        Date startDate = null;
        Date endDate = null;
        if (timeTime == 1) {//今日
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            startDate = c.getTime();
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.MILLISECOND, -1);
            endDate = c.getTime();
        } else if (timeTime == 2) {//昨天
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -1);
            startDate = c.getTime();
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.MILLISECOND, -1);
            endDate = c.getTime();
        } else if (timeTime == 3) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.DAY_OF_WEEK, 2);
            startDate = c.getTime();
            c.add(Calendar.DAY_OF_MONTH, 7);
            c.add(Calendar.MILLISECOND, -1);
            endDate = c.getTime();
        } else if (timeTime == 4) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.DAY_OF_MONTH, 1);
            startDate = c.getTime();
            c.add(Calendar.MONTH, 1);
            c.add(Calendar.MILLISECOND, -1);
            endDate = c.getTime();
        }
        hashMap.put("startDate",startDate);
        hashMap.put("endDate",endDate);
        return  hashMap;
    }


        /**
        * @Author:YWT_tai
        * @Description
        * @Date: 15:29 2018/6/25
         * 查询6天前的日期
        */
        public static Date getBeforeSixDay() {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -6);
            Date startDate = c.getTime();
            return startDate;
        }

}
