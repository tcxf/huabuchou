package com.tcxf.hbc.admin.controller.SettlementDetail;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.feign.ISettlementInfoService;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.admin.service.ITbSettlementDetailService;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RequestMapping("/fund/settlementDetail")
@RestController
public class FSettlementController extends BaseController {
    @Autowired
    public ITbSettlementDetailService iTbSettlementDetailService;

    @Autowired
    public IOOperaInfoService ioOperaInfoService;


    /**
     * 资金端结算跳转
     * @return
     */
    @RequestMapping("/qu")
    public ModelAndView qu(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        List<OOperaInfo> list = ioOperaInfoService.selectList(new EntityWrapper<OOperaInfo>());
        String fid = (String) get(request,"session_fund_id");
        modelAndView.addObject("fid",fid);
        modelAndView.addObject("list",list);
        modelAndView.setViewName("fund/settlementDetailManager");
        return modelAndView;
    }

    /**
     * 资金端结算查看分页
     * @return
     */
    @RequestMapping("/settlementDetailManagerList")
    public R settlementDetailManagerList(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        R r = new R <>();
        Page pageInfo =iTbSettlementDetailService.QueryALLSettlement(new Query <TbSettlementDetailDto>(paramMap));
        r.setData(pageInfo);
        return  r;
    }

    /**
     *待结算总额
     * @param request
     * @return
     */
    @RequestMapping("/DAmount")
    public R DAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object>  dAmount= iTbSettlementDetailService.DAmount(paramMap);
        System.out.println(dAmount);
        r.setData(dAmount);
        return r;
    }

    /**
     *已结算总额
     * @param request
     * @return
     */
    @RequestMapping("/YAmount")
    public R YAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object>  yAmount= iTbSettlementDetailService.YAmount(paramMap);
        System.out.println(yAmount);
        r.setData(yAmount);
        return r;
    }

    /**
     *运营商返佣 商户返佣 会员返佣 （总额）
     * @param request
     * @return
     */
    @RequestMapping("/Maids")
    public R Maids(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object>  maids= iTbSettlementDetailService.Maids(paramMap);
        System.out.println(maids);
        r.setData(maids);
        return r;
    }

    /**
     * 资金端结算跳转
     * @return
     */
    @RequestMapping("/qux")
    public ModelAndView qux(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String id = request.getParameter("id");
        modelAndView.addObject("id",id);
        modelAndView.setViewName("fund/settlementDetailLog");
        return modelAndView;
    }

    /**
     * 交易明细
     * @param request
     * @return
     */
    @RequestMapping("/tradingDetailManagerList")
    public R tradingDetailManagerList(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String id = request.getParameter("id");
        paramMap.put("id", id);
        System.out.println(id);
        R r = new R <>();
        Page pageInfo =iTbSettlementDetailService.TransactionDetails(new Query <TbSettlementDetailDto>(paramMap));
        r.setData(pageInfo);
        return  r;
    }

    /**
     * 线下结算
     * @param request
     * @return
     */
    @RequestMapping("/underlineSettlement")
    public R underlineSettlement(HttpServletRequest request){
      iTbSettlementDetailService.doSettlement(request);
      return R.newOK("结算成功,等待平台收款确认","");
    }

    /**
     * 导出资金端结算记录poi报表
     */
    @RequestMapping("/SettlementPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        //获取登陆的id
        String fid = get(request,"session_fund_id").toString();
        paramMap.put("fid",fid);
        if("".equals(paramMap.get("startDate"))) {
            paramMap.put("startDate", com.tcxf.hbc.common.util.DateUtil.getMonthFirstDay());
        }
        if("".equals(paramMap.get("endDate"))) {
            paramMap.put("endDate", com.tcxf.hbc.common.util.DateUtil.getCurrentTime());
        }
        List <TbSettlementDetailDto> listDto = iTbSettlementDetailService.QueryALLSettlementx(paramMap);

        String[] title = {" 编号","结算单号","商户名称", "所属运营商","出账时间", "结算时间", "结算状态", "结算状态(运营商)","结算状态（平台）","商户结算","运营商结算" ,"平台结算","结算金额","待结算总额","已结算总额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (TbSettlementDetailDto tbTradingDetailDto : listDto) {
            String[] strings = new String[13];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getMname();
            strings[3] = tbTradingDetailDto.getOname();
            strings[4] = DateUtil.getDateHavehms(tbTradingDetailDto.getOutSettlementDate());
            strings[5] = DateUtil.getDateHavehms(tbTradingDetailDto.getSettlementTime());
            strings[6] = tbTradingDetailDto.getStatus();
            strings[7] = tbTradingDetailDto.getOperStatus();
            strings[8] = tbTradingDetailDto.getPlatStatus();
            strings[9] = String.valueOf(tbTradingDetailDto.getOamount());
            strings[10] = String.valueOf(tbTradingDetailDto.getOperatorShareFee());
            strings[11] = String.valueOf(tbTradingDetailDto.getPlatfromShareFee());
            strings[12] = String.valueOf(tbTradingDetailDto.getSumamount());
            list.add(strings);
            i++;
        }
        String[] totalString =new String[15];
        Map<String,Object>  dsumamount = iTbSettlementDetailService.DAmount(paramMap);
        Map<String,Object>  ysumamount = iTbSettlementDetailService.YAmount(paramMap);
        totalString[13] = String.valueOf(dsumamount.get("dsumamount"));
        totalString[14] = String.valueOf(ysumamount.get("ysumamount"));
        list.add(0,totalString);
        ExportUtil.createExcel(list, title, response, "结算记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }
}
