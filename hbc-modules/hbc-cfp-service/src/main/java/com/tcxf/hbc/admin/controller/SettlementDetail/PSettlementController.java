package com.tcxf.hbc.admin.controller.SettlementDetail;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.feign.ISettlementInfoService;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.UnoutSettmentDto;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.admin.service.ITbSettlementDetailService;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.common.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/platfrom/pSettlement")
public class PSettlementController {

    @Autowired
    private ITbSettlementDetailService iTbSettlementDetailService;
    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;
    @Autowired
    private ISettlementInfoService iSettlementInfoService;
    @Autowired
    private IOOperaInfoService ioOperaInfoService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:27 2018/6/28
     * 跳转结算管理 查询明细
    */
    @RequestMapping("/jumpSettlementDetail")
    public ModelAndView modelAndView(String id ){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id",id);
        modelAndView.setViewName("platfrom/settlementDetailLog");
        return modelAndView;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:27 2018/6/28
     * 跳转结算管理
     */
    @RequestMapping("/jumpSettlementManager")
    public ModelAndView modelAndView(){
        ModelAndView modelAndView = new ModelAndView();
        List<OOperaInfo> list = ioOperaInfoService.selectList(new EntityWrapper<OOperaInfo>());
        modelAndView.addObject("list",list);
        modelAndView.setViewName("platfrom/settlementDetailManager");
        return modelAndView;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:28 2018/6/28
     * 结算管理分页查询
     */
    @RequestMapping("/pSettlementPageInfo")
    public R findSettlementPageInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page = iTbSettlementDetailService.QueryALLSettlement(new Query <TbSettlementDetailDto>(paramMap));
        R r = new R<>();
        r.setData(page);
        return  r;
    }

    /**
     * 平台确认打款
     * liaozeyong
     * @param id
     * @return
     */
    @RequestMapping("/ok")
    public R ok(String id){
        Boolean b = iSettlementInfoService.Settlements(id).getData();
        if (b)
        {
            TbSettlementDetail tbSettlementDetail = new TbSettlementDetailDto();
            tbSettlementDetail.setId(id);
            tbSettlementDetail.setIsPay("1");
            tbSettlementDetail.setOperStatus("2");
            tbSettlementDetail.setPlatStatus("2");
            tbSettlementDetail.setStatus("2");
            iTbSettlementDetailService.updateById(tbSettlementDetail);
            return R.newOK();
        }
        return R.newError();
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:28 2018/6/28
     * 结算管理 明细分页查询
    */
    @RequestMapping("/settlementDetail")
    public R findSettlementDetailInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page = iTbTradingDetailService.findTradingDetail(paramMap);
        R r = new R<>();
        r.setData(page);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:20 2018/6/28
     * 查询 结算管理(平台结算金额 运营商返佣 直接上级返佣 间接上级返佣)  根据结算表主键id查询
    */
    @RequestMapping("/fanYongInfo")
    public R findFanYongInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String, Object> mmp=  iTbSettlementDetailService.fanYongInfo(paramMap);
        BigDecimal operaSettlementNum=(BigDecimal)mmp.get("operaSettlementNum");
        mmp.put("operaSettlementNum",operaSettlementNum.divide(new BigDecimal("100")));
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:22 2018/6/29
     * 查询结算管理(头部 运营商返佣 直接上级返佣 间接上级返佣)根据条件查询
    */
    @RequestMapping("/SettlementHeadInfo")
    public R findSettlementHeadInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String, Object> mmp=  iTbSettlementDetailService.findSettlementHeadInfo(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
     *待结算总额
     * @param request
     * @return
     */
    @RequestMapping("/DAmount")
    public R DAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object>  dAmount= iTbSettlementDetailService.DAmount(paramMap);
        System.out.println(dAmount);
        r.setData(dAmount);
        return r;
    }

    /**
     *已结算总额
     * @param request
     * @return
     */
    @RequestMapping("/YAmount")
    public R YAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object>  yAmount= iTbSettlementDetailService.YAmount(paramMap);
        System.out.println(yAmount);
        r.setData(yAmount);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:54 2018/6/29
     * 查询结算管理 (平台返佣待结算金额)
    */
    @RequestMapping("/PFanYongWaitSet")
    public R findPFanYongWaitSet(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String, Object> mmp=  iTbSettlementDetailService.findPFanYongWaitSet(paramMap);
        BigDecimal PFanYongWaitSet=(BigDecimal)mmp.get("PFanYongWaitSet");
        mmp.put("PFanYongWaitSet",PFanYongWaitSet.divide(new BigDecimal("100")));
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:13 2018/6/30
     * 查询结算管理 (平台返佣已结算金额)
    */
    @RequestMapping("/PFanYongAlreadySet")
    public R findPFanYongAlreadySet(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String, Object> mmp=  iTbSettlementDetailService.findPFanYongAlreadySet(paramMap);
        BigDecimal PFanYongAlreadySet=(BigDecimal)mmp.get("PFanYongAlreadySet");
        mmp.put("PFanYongAlreadySet",PFanYongAlreadySet.divide(new BigDecimal("100")));
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:28 2018/6/28
     * 结算管理 未出结算单分页
     */
    @RequestMapping("/pNoSettlementPageInfo")
    public R findNoSettlementPageInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page=  iTbTradingDetailService.findNoSettlementPageInfo(new Query<UnoutSettmentDto>(paramMap));
        if(page!=null && page.getRecords().size()==1)
        {
            UnoutSettmentDto o = (UnoutSettmentDto)page.getRecords().get(0);
            if(StringUtil.isEmpty(o.getMid()) && StringUtil.isEmpty(o.getOid()))
            {
                page.getRecords().remove(0);
                page.setSize(0);
                page.setTotal(0);
            }
        }
        R r = new R<>();
        r.setData(page);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:57 2018/6/30
     * 跳转每个商户交易明细 jumpMerchantTradeDetail
    */
    @RequestMapping("/jumpMerchantTradeDetail")
    public ModelAndView jumpMerchantTradeDetail(String mid ){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("mid",mid);
        modelAndView.setViewName("platfrom/settlementMerchantDetailLog");
        return modelAndView;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:14 2018/6/30
     * 查询未出结算单 （每个商户）交易明细
    */
    @RequestMapping("/MerchantSettlementDetail")
    public R findMerchantSettlementDetail(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page=  iTbTradingDetailService.findMerchantSettlementDetail(new Query<TbTradingDetailDto>(paramMap));
        R r = new R<>();
        r.setData(page);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:48 2018/6/29
     * 下载报表
    */
    @RequestMapping("/AlreadyOrderPoi")
    public void exportAlreadyOrderPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page=  iTbSettlementDetailService.findSettlementPageInfo(new Query<TbSettlementDetailDto>(paramMap));

        String[] title = { "编号", "结算单号","商户名称","所属运营商", "出账时间", "结算时间", "结算状态","平台结算金额","运营商返佣","间接上级返佣" ,"直接上级返佣"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<TbSettlementDetailDto> listDto=page.getRecords();

        for (TbSettlementDetailDto tbSettlementDetailDto : listDto) {
            String[] strings = new String[11];
            strings[0] = i + "";
            strings[1] = tbSettlementDetailDto.getSerialNo();
            strings[2] = tbSettlementDetailDto.getLegalName();
            strings[3] = tbSettlementDetailDto.getOname();
            strings[4] = DateUtil.getDateHavehms(tbSettlementDetailDto.getOutSettlementDate());
            strings[5] = DateUtil.getDateHavehms(tbSettlementDetailDto.getSettlementTime());
            strings[6] = tbSettlementDetailDto.getPlatStatus();
            paramMap.put("id",tbSettlementDetailDto.getId());
            Map<String, Object> mmp=  iTbSettlementDetailService.fanYongInfo(paramMap);
            strings[7] =  String.valueOf(mmp.get("operaSettlementNum"));
            strings[8] =  String.valueOf(mmp.get("oshareFee"));
            strings[9] =  String.valueOf(mmp.get("rshareAmount"));
            strings[10] =  String.valueOf(mmp.get("ishareAmount")) ;
            list.add(strings);
            i++;
        }

        ExportUtil.createExcel(list, title, response, "提前还款记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:27 2018/7/11
     * 确认付款
    */
    @RequestMapping("/updateplatStatus")
    public R updateplatStatus(TbSettlementDetail tbSettlementDetail)
    {
        R r = new R<>();
        tbSettlementDetail.setPlatStatus("1");
        boolean b = iTbSettlementDetailService.updateById(tbSettlementDetail);
        if(b){
            r.setCode(R.SUCCESS);
            r.setMsg("确认结算，资金端打款!");
        }
        return  r;
    }

}
