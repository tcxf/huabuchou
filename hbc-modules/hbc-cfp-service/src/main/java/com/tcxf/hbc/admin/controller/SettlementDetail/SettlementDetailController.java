package com.tcxf.hbc.admin.controller.SettlementDetail;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.feign.ISettlementInfoService;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.UnoutSettmentDto;
import com.tcxf.hbc.admin.service.ITbBindCardInfoService;
import com.tcxf.hbc.admin.service.ITbSettlementDetailService;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 运营商已结算
 * liaozeyong
 * 2018年6月30日10:37:37
 */
@RestController
@RequestMapping("/opera/settlementDetail")
public class SettlementDetailController extends BaseController {

    @Autowired
    private ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    private ITbSettlementDetailService iTbSettlementDetailService;
    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    /**
     * 跳转运营商商户结算界面
     * @return
     */
    @RequestMapping("/settlementDetailManager")
    public ModelAndView settlementDetailManager(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("manager/settlementDetailManager");
        modelAndView.addObject("ooid" ,get(request ,"session_opera_id"));
        return modelAndView;
    }

    /**
     * 运营商查询商户已结算
     * 分页展示
     * @return
     */
    @RequestMapping("/settlementDetailManagerList")
    public R findOperaSettlement(HttpServletRequest request) {
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        //设置代表已结算
        Page page = iTbSettlementDetailService.QueryALLSettlement(new Query <TbSettlementDetailDto>(paramMap));
        r.setData(page);
        return  r;
    }

    /**
     *待结算总额
     * @param request
     * @return
     */
    @RequestMapping("/DAmount")
    public R DAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("ooid" ,get(request , "session_opera_id"));
        Map<String,Object>  dAmount= iTbSettlementDetailService.DAmount(paramMap);
        r.setData(dAmount);
        return r;
    }

    /**
     *已结算总额
     * @param request
     * @return
     */
    @RequestMapping("/YAmount")
    public R YAmount(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("ooid" ,get(request , "session_opera_id"));
        Map<String,Object>  yAmount= iTbSettlementDetailService.YAmount(paramMap);
        r.setData(yAmount);
        return r;
    }

    /**
     * 跳转明细详情界面
     * @param id
     * @return
     */
    @RequestMapping("/settlementDetailLog")
    public ModelAndView modelAndView(String id ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id",id);
        modelAndView.setViewName("manager/settlementDetailLog");
        return modelAndView;
    }

    /**
     * 已结算单明细展示
     * @param id
     * @return
     */
    @RequestMapping("/tradingDetailManagerList")
    public R tradingDetailManagerList(HttpServletRequest request ,String id)
    {
        R r = new R();
        String p = request.getParameter("page");
        Map<String , Object> map = new HashMap();
        map.put("id",id);
        map.put("page",p);
        Page page = iTbTradingDetailService.findTradingDetail(map);
        r.setData(page);
        return r;
    }

    /**
     * 未结算单分页展示
     * @return
     */
    @RequestMapping("/uncreateSettlementDetail")
    public R uncreateSettlementDetail(HttpServletRequest request)
    {
        R r = new R();
        String p = request.getParameter("page");
        String oid = (String) get(request , "session_opera_id");
        Map<String , Object> map = new HashMap();
        map.put("page",p);
        map.put("oid",oid);
        Query query = new Query(map);
        Page page = iTbTradingDetailService.findNoSettlementPageInfo(query);
        if(page!=null && page.getRecords().size()==1)
        {
            UnoutSettmentDto o = (UnoutSettmentDto)page.getRecords().get(0);
            if(StringUtil.isEmpty(o.getMid()) && StringUtil.isEmpty(o.getOid()))
            {
                page.getRecords().remove(0);
                page.setSize(0);
                page.setTotal(0);
            }
        }
        r.setData(page);
        return r;
    }

    /**
     * 跳转到未生成结算单明细
     * liaozeyong
     * @param mid
     * @return
     */
    @RequestMapping("/jumpMerchantTradeDetail")
    public ModelAndView jumpMerchantTradeDetail(String mid){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("mid",mid);
        modelAndView.setViewName("manager/settlementMerchantDetailLog");
        return modelAndView;
    }

    /**
     * 查询未生成结算单明细
     * liaozeyong
     * @param request
     * @return
     */
    @RequestMapping("/MerchantSettlementDetail")
    public R findMerchantSettlementDetail(HttpServletRequest request)
    {
        R r = new R<>();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page=  iTbTradingDetailService.findMerchantSettlementDetail(new Query<TbTradingDetailDto>(paramMap));
        r.setData(page);
        return  r;
    }

    /**
     * 确认结算
     * @param id
     */
    @RequestMapping("sureSettlement")
    public R sureSettlement(String id) {
        R r = new R();
        TbSettlementDetail tbSettlementDetail = new TbSettlementDetail();
        tbSettlementDetail.setId(id);
        tbSettlementDetail.setOperStatus("1");
        iTbSettlementDetailService.updateById(tbSettlementDetail);
        r.setMsg("确认成功");
        return r;
    }

    /**
     * 查询已结算总额和未结算总额
     * @return
     */
    @RequestMapping("/totalAmount")
    public R totalAmount(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        R r = new R();
        String oid = (String) get(request , "session_opera_id");
        //获取已结算总额
        paramMap.put("oid",oid);
        paramMap.put("status",2);
        int s = iTbSettlementDetailService.totalAmount(paramMap);
        //获取未结算总额
        Map<String, Object> paramMap2 = MapUtil.getMapForRequest(request);
        paramMap2.put("oid",oid);
        paramMap2.put("status",1);
        int uns =  iTbSettlementDetailService.totalAmount(paramMap2);
        Map<String , Object> map2 = new HashMap<>();
        map2.put("uns",uns);
        map2.put("s",s);
        r.setData(map2);
        return r;
    }

    /**
     * 下载报表
     * @param request
     * @param response
     */
    @RequestMapping("/jsmx")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",get(request , "session_opera_id"));
        //设置代表已结算
        paramMap.put("status",0);
        List<TbSettlementDetailDto> listDto =  iTbSettlementDetailService.QueryALLSettlementx(paramMap);
        String[] title = {"","交易单号", "商户名称","所属运营商", "出账时间", "结算时间", "结算状态","结算状态(平台)","结算状态(运营商)","商户结算","运营商结算","平台结算","结算金额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (TbSettlementDetailDto tbSettlementDetailDto : listDto) {
            String[] strings = new String[13];
            strings[0] = i + "";
            strings[1] = tbSettlementDetailDto.getSerialNo();
            strings[2] = tbSettlementDetailDto.getMname();
            strings[3] = tbSettlementDetailDto.getOname();
            strings[4] = DateUtil.getDateHavehms(tbSettlementDetailDto.getOutSettlementDate());
            strings[5] = DateUtil.getDateHavehms(tbSettlementDetailDto.getSettlementTime());
            strings[6] = tbSettlementDetailDto.getStatus();
            strings[7] = tbSettlementDetailDto.getPlatStatus();
            strings[8] = tbSettlementDetailDto.getOperStatus();
            strings[9] = tbSettlementDetailDto.getOamount().toString();
            strings[10] = tbSettlementDetailDto.getOperatorShareFee().toString();
            strings[11] = tbSettlementDetailDto.getPlatfromShareFee().toString();
            strings[12] = tbSettlementDetailDto.getSumamount().toString();
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "已生成结算单记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }

    /**
     * 查询未出账单金额总额
     * @param request
     * @return
     */
    @RequestMapping("/uncreateSettlementAmount")
    public R uncreateSettlementAmount(HttpServletRequest request){
        R r = new R();
        Map<String , Object> map = new HashMap<>();
        map.put("oid",get(request , "session_opera_id"));
        BigDecimal ua = iTbTradingDetailService.uncreateAmount(map);
        Map<String , Object> map2 = new HashMap<>();
        map2.put("ua",ua);
        r.setData(map2);
        return r;
    }
}
