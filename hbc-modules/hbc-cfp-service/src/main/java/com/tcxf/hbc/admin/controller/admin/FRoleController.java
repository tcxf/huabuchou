package com.tcxf.hbc.admin.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.tcxf.hbc.admin.service.ITbRoleResourcesService;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.management.relation.Role;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 运营商端 角色模块
 *
 */
@RestController
@RequestMapping("/fund/fRole")
public class FRoleController extends BaseController {
    @Autowired
    private ITbRoleService iTbRoleService;

    @Autowired
    private ITbResourcesService iTbResourcesService;

    @Autowired
    private ITbRoleResourcesService iTbRoleResourcesService;

    @Autowired
    private IPAdminService adminService;


    @RequestMapping("/faindRole")
    public ModelAndView faindRole(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("fund/role/roleManager");
        return  mode;
    }
    @RequestMapping("/roleManagerList")
    public R roleManagerList(HttpServletRequest request){
        R r = new R();
        //获取登陆的id
        String aid = get(request,"session_fund_id").toString();
        //查询该管理员 下的所有角色
        Page page = iTbRoleService.selectPage(aid);
        r.setData(page);
        return  r;
    }
    /**
     *
     * 跳转到添加角色页面
     * @return
     */
    @RequestMapping("/addRole")

    public ModelAndView addRole(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("fund/role/roleAdd");
        return  mode;
    }
    @RequestMapping("/insertRole")
    public R insertRole(HttpServletRequest request,TbRole role){
        R r = new R();
        //获取登陆的id
        String aid = get(request,"session_fund_id").toString();
        role.setUid(aid);
        role.setUtype("4");
        Boolean bo = iTbRoleService.insert(role);
        if(bo){
            r.setMsg("添加成功");
        }else{
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     *
     * 跳转到修改角色管理页面
     * @return
     */
    @RequestMapping("/updateRole")
    public ModelAndView updateRole(String id){
        ModelAndView mode = new ModelAndView();
        TbRole role = iTbRoleService.selectById(id);
        mode.addObject("role",role);
        mode.setViewName("fund/role/roleHandler");
        return  mode;
    }

    /**
     * 根据id修改角色信息
     * @param role
     * @return
     */
    @RequestMapping("/updateById")
    public R updateById(TbRole role){
        R r = new R();
        TbRole ro = iTbRoleService.selectById(role.getId());
        ro.setRemark(role.getRemark());
        ro.setRoleName(role.getRoleName());
        Boolean bo =iTbRoleService.updateById(ro);
        if(bo){
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return  r;

    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @RequestMapping("/deleteById")
    public R deleteById( String id){
        R r = new R();
        Boolean bo = iTbRoleService.deleteById(id);
        if(bo){
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 进入分配资源页面
     * @return
     */
    @RequestMapping("/selectResources")
    public  ModelAndView selectResources(String id){
        ModelAndView mode = new ModelAndView();
        Map<String,Object> map1 = new HashMap<>();
        map1.put("resource_type","2");
        map1.put("type","3");
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","1");
        map.put("type","3");
        //查询默认管理员下的所有资源路径和模块
        List<TbResources> pList  = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map1));
        List<TbResources> list  = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        mode.addObject("list",list);
        mode.addObject("pList",pList);
        mode.addObject("id",id);
        mode.setViewName("fund/role/selectResources");
        return mode;
    }

    /**
     * 查询该管理员的资源权限
     * @param id
     * @return
     */
    @RequestMapping("/loadRoleResourcesStr")
    public  R loadRoleResourcesStr(String id){
        R r = new R();
        List<TbRoleResources> tList = iTbRoleResourcesService.selectList(new EntityWrapper<TbRoleResources>().eq("role_id",id));
        r.setData(tList);
        return  r;

    }
    @RequestMapping("/distribution")
    public R distribution(String id,String ids){
        R r = new R();
        if(id == null){
            r.setMsg("不存在角色id");
        }else {
            if (ids == null ||"".equals(ids)) {
                r.setMsg("请选择权限");
                r.setCode(1);
            }else{
                iTbRoleResourcesService.role(id,ids);
                r.setMsg("分配权限成功");
            }
        }
        return  r;
    }
}
