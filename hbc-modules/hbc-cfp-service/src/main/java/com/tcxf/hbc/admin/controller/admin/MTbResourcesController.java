package com.tcxf.hbc.admin.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.admin.model.vo.ResourcesVo;
import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.tcxf.hbc.admin.service.ITbRoleResourcesService;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.common.util.R;
import com.xiaoleilu.hutool.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 运营商 资源模块
 *
 */
@RestController
@RequestMapping("/opera/mRe")
public class MTbResourcesController {
    @Autowired
    private ITbResourcesService iTbResourcesService;
    @Autowired
    private ITbRoleResourcesService iTbRoleResourcesService;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    /**
     * 跳转到资源管理页面
     * @return
     */
    @RequestMapping("/r")
    public ModelAndView r(){
        ModelAndView mode= new ModelAndView();
        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",2);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        mode.addObject("tList",tList);
        mode.setViewName("manager/resources/resourcesManager");
        return mode;
    }

    /**
     * 根据条件查询资源分页
     * @return
     */
    @RequestMapping("/findResources")
    public R findResources(ResourcesVo tbResources) {
        R r = new R<>();
        ModelAndView mode = new ModelAndView();
        //Page page = iTbResourcesService.getResourcesList();
        Map<String,Object> map = new HashMap<>();
        map.put("page",tbResources.getPage());
        if(tbResources.getParentId()!=null) {
            if ("0".equals(tbResources.getParentId())) {
                map.put("resourceType", "2");
            } else {
                map.put("parentId", tbResources.getParentId());

            }
        }
        map.put("resourceName",tbResources.getResourceName());
        map.put("type",2);
        Page page = iTbResourcesService.getPage(map);
        r.setData(page);
        return r;
    }

    /**
     * 根据id删除资源信息
     * @param
     * @return
     */
    @RequestMapping("/deleteById")
    public R deleteById( String id){
        R r = new R<>();
        List<TbRoleResources> ro =  iTbRoleResourcesService.selectList(new EntityWrapper<TbRoleResources>().eq("resource_id",id));
        if(ro.size()==0){
            Boolean se = iTbResourcesService.deleteById(id);
            r.setMsg("删除成功");
        }else{
            r.setMsg("有角色关联该资源路径，请先取消关联在来删除");
        }
        return r;
    }

    /**
     * 跳转到修改资源页面
     * @param id
     * @return
     */
    @RequestMapping("/updateResources")
    public ModelAndView updateResources( String id){
        ModelAndView mode = new ModelAndView();
        //查询要修改的资源信息
        TbResources resources = iTbResourcesService.selectById(id);
        //查询所有模块信息

        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",2);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        mode.addObject("resources",resources);
        mode.addObject("tList",tList);
        mode.setViewName("manager/resources/resourcesUpdate");
        return  mode;
    }
    /**
     * 根据id修改资源信息
     * @param vo
     * @return
     */
    @RequestMapping("/updateById")
    public R updateById(TbResources vo){
        R r = new R();
        ModelAndView mode = new ModelAndView();
        boolean se = iTbResourcesService.updateById(vo);

        return r;
    }

    /**
     * 跳转到资源添加页面
     * @return
     */
    @RequestMapping("/AddResources")
    public ModelAndView AddResources(){
        ModelAndView mode= new ModelAndView();
        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",2);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        mode.addObject("tList",tList);
        mode.setViewName("manager/resources/resourcesHandler");
        return mode;
    }

    /**
     * 添加资源
     * @param vo
     * @return
     */
    @RequestMapping("/insert")
    public R insert(TbResources vo){

        vo.setCreateDate(new Date());
        vo.setModifyDate(new Date());
        R r = new R();
        ModelAndView mode = new ModelAndView();
        vo.setType(2);
        boolean se = iTbResourcesService.insert(vo);
        return r;
    }

    /**
     * 文件上传
     *
     * @param file
     * @return
     */

    @PostMapping("/otherInfo/upload")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
//            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }
}