package com.tcxf.hbc.admin.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.ITbThreeSalesRateService;
import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


/**
 * 运营商端 分润
 */
@RestController
@RequestMapping("/opera/saleRate")
public class OSalesRateController extends BaseController {
    @Autowired
    private ITbThreeSalesRateService iTbThreeSalesRateService;


    /**
     * 跳转到分润页面
     * @param request
     * @return
     */
    @RequestMapping("/findSaleRate")
    public ModelAndView findSaleRateByOid(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        String oid = get(request,"session_opera_id").toString();
        TbThreeSalesRate salesRate = iTbThreeSalesRateService.selectOne(new EntityWrapper<TbThreeSalesRate>().eq("oid",oid));
        mode.addObject("salesRate",salesRate);
        String type ="0";
        if(salesRate!=null){
            type="1";
        }
        mode.addObject("type",type);
        mode.setViewName("manager/activity/salesRateAdd");
        return  mode;
    }

    /**
     * 设置分润比率
     * @param request
     * @param tbThreeSalesRate
     * @return
     */
    @RequestMapping("/insert")
    public R insert( HttpServletRequest request,TbThreeSalesRate tbThreeSalesRate) {
        R r = new R();
        if (tbThreeSalesRate.getId() == null || "".equals(tbThreeSalesRate.getId())) {
            String oid = get(request,"session_opera_id").toString();
            tbThreeSalesRate.setOid(oid);
            if (iTbThreeSalesRateService.insert(tbThreeSalesRate)) {
                r.setMsg("添加成功");
            } else {
                r.setMsg("添加失败");
            }
        }else {
            if (iTbThreeSalesRateService.updateById(tbThreeSalesRate)) {
                r.setMsg("修改成功");
            } else {
                r.setMsg("修改失败");
            }
        }
            return r;

        }


}
