package com.tcxf.hbc.admin.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.vo.AdminVo;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbPeopleRoleService;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.secure.MD5Util;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 平台端 管理员模块
 *
 */
@RestController
@RequestMapping("/platfrom/admin")
public class PAdminController extends BaseController {

    @Autowired
    private IPAdminService adminService;
    @Autowired
    private ITbRoleService iTbRoleService;

    @Autowired
    private ITbPeopleRoleService iTbPeopleRoleService;

    @RequestMapping("/findAdmin")
    public ModelAndView findAdmin(){
        ModelAndView mode = new ModelAndView();

        mode.setViewName("platfrom/admin/adminManager");
        return mode;
    }

    /**
     * 根据条件分页查询
     * @param adminVo
     * @return
     */
   // @RequestMapping(value="/findAdminList",method ={RequestMethod.POST,RequestMethod.GET} )
    @RequestMapping("/findAdminList")
    public R findAdminList(HttpServletRequest request, AdminVo adminVo){
        R r = new R();
        Map<String, Object> params = new HashMap<>();
        params.put("page",adminVo.getPage());
        params.put("name",adminVo.getAdminName());
        //获取登陆的id
        String aid = get(request,"session_admin_id").toString();
         PAdmin admin = adminService.selectById(aid);
         params.put("aid",admin.getAid());
        Page page = adminService.findAdminCondition(params);
        r.setData(page);
        return  r;
    }

    /**
     * 添加管理员
     * @param admin
     * @return
     */
    @RequestMapping("/insertAdmin")
    public R insertAdmin(HttpServletRequest request,PAdmin admin){
        R r = new R();
        ModelAndView mode = new ModelAndView();
        PAdmin pAdmin= adminService.selectOne(new EntityWrapper<PAdmin>().eq("user_name",admin.getUserName()));
        if(ValidateUtil.isEmpty(pAdmin)) {
        //获取登陆的id
        String aid = get(request,"session_admin_id").toString();
        PAdmin admin1 = adminService.selectById(aid);
        PasswordEncoder ENCODER = new BCryptPasswordEncoder();
        String pwd =   ENCODER.encode(admin.getPwd());
        admin.setPwd(pwd);
        admin.setUiType(1);
        admin.setAid(admin1.getAid());
        try {
            Boolean bo = adminService.insert(admin);
            if(bo){
                r.setMsg("添加成功");
            }else{
                r.setMsg("添加失败");
            }

        }catch (Exception e){
            r.setMsg("添加失败");
        }
        }else {
            r.setMsg("登陆用户名已存在，请重新输入账号");
            r.setCode(1);
        }
        return r;
    }

    /**
     * 跳转到添加管理员页面
     * @return
     */
    @RequestMapping("/addAmin")
    public ModelAndView addAmin(String id){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("platfrom/admin/adminAdd");
        return mode;
    }

    /**
     *跳转到修改管理员页面
     * @return
     */
    @GetMapping("/updateAdmin")
    public ModelAndView updateAdmin(String id){
        ModelAndView mode = new ModelAndView();
        PAdmin admin = adminService.selectById(id);
        mode.addObject("admin",admin);
        mode.setViewName("platfrom/admin/adminHandler");
        return  mode;
    }

    /**
     * 根据id修改管理信息
     * @param admin
     * @return
     */
    @RequestMapping("/updateById")
    public R updateById(PAdmin admin){
        R r = new R();
        PAdmin admin1 =  adminService.selectById(admin.getId());
        if(!ValidateUtil.isEmpty(admin.getPwd())){
            PasswordEncoder ENCODER = new BCryptPasswordEncoder();
            String pwd =   ENCODER.encode(admin.getPwd());
            admin1.setPwd(pwd);
        }
        admin1.setUserName(admin.getUserName());
        admin1.setAdminName(admin.getAdminName());
        admin1.setRegEmail(admin.getRegEmail());
        Boolean bo = adminService.updateById(admin1);
        if(bo){
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return  r;
    }

    /**
     *
     * 根据id删除管理
     * @param
     * @return
     */
    @RequestMapping("/deleteById")
    public R deleteById(String id ){
        R r = new R();
        Boolean bo = adminService.deleteById(id);
        if(bo){
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 跳转到分配角色页面
     * @return
     */
    @RequestMapping("/selectRole")
    public ModelAndView selectRole(HttpServletRequest request,String id){
        ModelAndView mode = new ModelAndView();
        //获取登陆的id
        String aid = get(request,"session_admin_id").toString();
        PAdmin admin1 = adminService.selectById(aid);
        //查询该默认管理员下的角色
        List<TbRole> list = iTbRoleService.selectList(new EntityWrapper<TbRole>().eq("uid",admin1.getAid()));
        mode.addObject("id",id);
        mode.addObject("list",list);
        mode.setViewName("platfrom/admin/selectRole");
        return mode;
    }

    /**
     * 根据管理id查询该管理员的角色
     * @param id
     * @return
     */
    @RequestMapping("selectAdminRoleStr")
    public R selectAdminRoleStr(String id){
        R r = new R();
        List<TbPeopleRole> tList = iTbPeopleRoleService.selectList(new EntityWrapper<TbPeopleRole>().eq("people_user_id",id));
        r.setData(tList);
        return r;


    }

    /**
     * 根据管理员id分配角色
     * @param id
     * @return
     */
    @RequestMapping("/distribution")
    public R distribution(String id,String ids) {
        R r = new R();
        try {
            if(id==null){
                r.setMsg("管理员不存在");
                r.setCode(1);
            }else {
                if (ids == null || "".equals(ids)) {
                    r.setMsg("请选择角色");
                    r.setCode(1);
                } else {
                    TbPeopleRole tbPeopleRole = iTbPeopleRoleService.selectOne(new EntityWrapper<TbPeopleRole>().eq("people_user_id", id));
                    iTbPeopleRoleService.delete(new EntityWrapper<TbPeopleRole>().eq("people_user_id", id));
                    String[] ids_ = ids.split(",");
                    for (String str : ids_) {
                        TbPeopleRole tbPeopleRole1 = new TbPeopleRole();
                        tbPeopleRole1.setPeopleUserId(id);
                        tbPeopleRole1.setPeopleUserType("3");
                        tbPeopleRole1.setRoleId(str);
                        iTbPeopleRoleService.insert(tbPeopleRole1);
                    }
                    r.setMsg("分配角色成功");

                }
            }
        }catch (Exception e){
            r.setMsg("分配角色失败");
        }
       return  r;

    }
}
