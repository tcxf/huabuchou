package com.tcxf.hbc.admin.controller.basecontroller;

import com.google.common.base.Strings;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liuxu
 * @Date: 2018/7/31 20:38
 * @Description:
 */
@RestController
public class MyErrorController  extends BasicErrorController {

    public MyErrorController(){
        super(new DefaultErrorAttributes(), new ErrorProperties());
    }

    private static final String PATH = "/error";

    @Override
    @AuthorizationNO
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        String requestType = request.getHeader("X-Requested-With");
        System.out.println(requestType);
        Map<String, Object> body = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
        Map<String, Object> map = new HashMap<String, Object>();
        String msg = body.get("message").toString();
        System.out.println();
        if(ValidateUtil.isEmpty(body.get("exception"))){
            return null;
        }
        if(body.get("exception").toString().equals(CheckedException.class.getName())){
            if(!ValidateUtil.isEmpty(msg)){
                msg = msg.substring(msg.lastIndexOf(":")+1,msg.length());
            }
        }else{
            msg ="系统异常，请稍后再试";
        }
        map.put("code", R.FAIL);
        map.put("msg", msg);
        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }

    /**
     * 覆盖默认的HTML响应
     */
    @Override
    @AuthorizationNO
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
        //请求的状态
        HttpStatus status = getStatus(request);
        response.setStatus(HttpStatus.OK.value());
        Map<String, Object> model = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.TEXT_HTML));
        //指定自定义的视图
        String msg = model.get("message").toString();
        if(!ValidateUtil.isEmpty(model.get("exception"))&&model.get("exception").toString().equals(CheckedException.class.getName())){
            if(!ValidateUtil.isEmpty(msg)){
                msg = msg.substring(msg.lastIndexOf(":")+1,msg.length());
            }
        }else{
            msg ="系统异常，请稍后再试";
        }
        ModelAndView modelAndView = new ModelAndView("cfperror");
        modelAndView.addObject("errmsg", ""+status.toString()+":"+msg+"");
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}
