package com.tcxf.hbc.admin.controller.deal;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.tcxf.hbc.common.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 运营商端  交易管理
 * liaozeyong
 * 2018年6月30日10:27:28
 */
@RestController
@RequestMapping("/opera/msf")
public class MerchantShareFeeController extends BaseController {

    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    @RequestMapping("/msfManager")
    public ModelAndView merchantDeal(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("manager/msfManager");
        return modelAndView;
    }

    /**
     * 交易明细列表
     *
     * @return
     * @author liaozeyong
     * @date 2018年6月25日20:03:16
     * @throws
     */
    @RequestMapping("/msfManagerList")
    public R msfManagerList(HttpServletRequest request) {
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",get(request , "session_opera_id"));
        Query query = new Query(paramMap);
        Page page = iTbTradingDetailService.msfManagerList(query);
        r.setData(page);
        return r;
    }

    /**
     * 商户让利总金额
     * @param request
     * @return
     */
    @RequestMapping("/totalAmount")
    public R totalAmount(HttpServletRequest request)
    {
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid" , get(request , "session_opera_id"));
        BigDecimal totalmount = iTbTradingDetailService.metchatTotalAmount(paramMap);
        r.setData(totalmount);
        return r;
    }


    /**
     * 商户让利详情跳转
     * @param request
     * @return
     */
    @RequestMapping("/QueryOneMsf")
    public ModelAndView QueryOneMsf(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String tid = request.getParameter("tid");
        modelAndView.addObject("tid",tid);
        modelAndView.setViewName("manager/msfHandler");
        return modelAndView;
    }


    /**
     *商户让利查看详情
     * @param request
     * @return
     */
    @RequestMapping("/detailInfo")
    public  R detailInfo(HttpServletRequest request){
        R r=new R();
        String tid = request.getParameter("tid");
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("tid",tid);
        Page data = iTbTradingDetailService.transaction(new Query <TransactionDto>(paramMap));
        r.setData(data);
        return r;
    }
    /**
     * 下载报表
     */
    @RequestMapping("/sjrldaochu")
    public void sjrldaochu(HttpServletRequest request,HttpServletResponse response){
        try{
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            paramMap.put("oid" , get(request , "session_opera_id"));
            if("".equals(paramMap.get("startDate"))) {
                paramMap.put("startDate", com.tcxf.hbc.common.util.DateUtil.getMonthFirstDay());
            }
            if("".equals(paramMap.get("endDate"))) {
                paramMap.put("endDate", com.tcxf.hbc.common.util.DateUtil.getCurrentTime());
            }
            String[] title = { "交易单号", "用户名称","用户手机", "商户名称", "商户手机", "交易状态","交易时间", "交易金额","优惠金额", "实付金额", "让利金额"};
            int i = 1;
            List<String[]> list = new LinkedList<String[]>();
            List<TbTradingDetailDto> listDto=iTbTradingDetailService.msf(paramMap);
            for (TbTradingDetailDto tbTradingDetailDto : listDto) {
                String[] strings = new String[12];
                strings[0] = i + "";
                strings[1] = tbTradingDetailDto.getSerialNo();
                strings[2] = tbTradingDetailDto.getRealName();
                strings[3] = tbTradingDetailDto.getNmobile();
                strings[4] = tbTradingDetailDto.getMname();
                strings[5] = tbTradingDetailDto.getMmobile();
                strings[6] = tbTradingDetailDto.getPaymentStatus();
                strings[7] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
                strings[8] = tbTradingDetailDto.getTradeAmount().toString();
                strings[9] = tbTradingDetailDto.getDiscountAmount().toString();
                strings[10] = tbTradingDetailDto.getActualAmount().toString();
                strings[11] = tbTradingDetailDto.getOperatorShareFee();
                list.add(strings);
                i++;
            }
            ExportUtil.createExcel(list, title, response, "商户让利记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
        }catch (Exception e){

        }
    }
}
