package com.tcxf.hbc.admin.controller.deal;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 9:46 2018/6/23
 */

@RestController
@RequestMapping("/opera/o_share")
public class OShareProfitController extends BaseController {

    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:55 2018/6/23
     * 跳转运营商让利
    */
    @GetMapping("/jumpOpera")
    public ModelAndView jumpMerchantCate()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/psfManager");
        return mode;
    }
    
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:25 2018/6/26
    */
    @GetMapping("/jumpMerProfitDetail")
    public ModelAndView jumpMerProfitDetail(String id)
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/psfDetail");
        mode.addObject("id",id);
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:55 2018/6/23
     * 查询运营商让利分页
    */
    @RequestMapping("/OperaPageInfo")
    public R findOperaPageInfo(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String oid = (String) get(request, "session_opera_id");
        paramMap.put("oid",oid);
        R r=new R<>();
        Page pageInfo=  iTbTradingDetailService.findOperaPageInfo(new Query<TbTradingDetailDto>(paramMap));
        r.setData(pageInfo);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:39 2018/6/23
     * 根据条件查询总分润
    */
    @RequestMapping("/TotalProfit")
    public R findTotalProfit(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String oid = (String) get(request, "session_opera_id");
        paramMap.put("oid",oid);
        R r=new R<>();
        Map<String,Object> map=  iTbTradingDetailService.findTotalProfit(paramMap);
        r.setData(map);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:34 2018/6/25
     * 运营商报表的导出
    */

    @RequestMapping("/consumeManagerPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String oid = (String) get(request, "session_opera_id");
        paramMap.put("oid",oid);
        Page page=  iTbTradingDetailService.findOperaPageInfo(new Query<TbTradingDetailDto>(paramMap));
        String[] title = { "编号", "流水单号","商户名称", "行业类型", "下单用户","交易状态", "交易时间","交易总额","优惠金额","实付总额","让利金额","让利总金额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<TbTradingDetailDto> listDto=page.getRecords();

        for (TbTradingDetailDto tbTradingDetailDto : listDto) {
            String[] strings = new String[11];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getLegalName();
            strings[3] = tbTradingDetailDto.getNature();
            strings[4] = tbTradingDetailDto.getRealName();
            strings[5] = tbTradingDetailDto.getPaymentStatus();
            strings[6] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
            strings[7] = String.valueOf(tbTradingDetailDto.getTradeAmount());
            strings[8] = String.valueOf(tbTradingDetailDto.getDiscountAmount());
            strings[9] = String.valueOf(tbTradingDetailDto.getActualAmount());
            strings[10] = String.valueOf(tbTradingDetailDto.getPshareProfit());
            list.add(strings);
            i++;
        }
        String[] totalString =new String[12];
        Map<String,Object> map=  iTbTradingDetailService.findTotalProfit(paramMap);
        totalString[11] = String.valueOf(map.get("totalProfit"));
        list.add(0,totalString);

        ExportUtil.createExcel(list, title, response, "运营商让利导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }

}
