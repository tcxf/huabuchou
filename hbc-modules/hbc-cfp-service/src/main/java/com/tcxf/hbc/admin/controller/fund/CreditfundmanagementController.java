package com.tcxf.hbc.admin.controller.fund;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IFMoneyInfoService;
import com.tcxf.hbc.admin.service.IFMoneyOtherInfoService;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * jiangyoong
 * 2018年9月5日19:19:49
 * 设置资金管理
 */
@RestController
@RequestMapping("/fund/creditfundmanagement")
public class CreditfundmanagementController extends BaseController {

    @Autowired
    private IFMoneyInfoService fMoneyInfoService;

    @Autowired
    private IFMoneyOtherInfoService ifMoneyOtherInfoService;

    /**
     *授信资金管理跳转
     * @return
     */
    @RequestMapping("/fundmanagement")
    public ModelAndView Creditfundmanagement(HttpServletRequest request){
        ModelAndView modelAndView =new ModelAndView();
        String fid = (String) get(request,"session_fund_id");
        FMoneyInfo fmoenyinfo = fMoneyInfoService.selectOne(new EntityWrapper <FMoneyInfo>().eq("fid", fid));

        //今日授信余额
        String start = DateUtil.getTimesmorning();
        String end = DateUtil.getTimesnight();
        BigDecimal b = ifMoneyOtherInfoService.DayYUmoney(fid);
        BigDecimal b1 = ifMoneyOtherInfoService.DaySummoney(start, end,fid);
        BigDecimal b2 = b.subtract(b1);

        //授信资金池余额
        BigDecimal summoney= ifMoneyOtherInfoService.DayYUmoneyss(fid);
        BigDecimal  Lj = ifMoneyOtherInfoService.LJSummoney(fid);
        BigDecimal LJS= summoney.subtract(Lj);
        modelAndView.addObject("LJS",LJS);

        modelAndView.addObject("fmoenyinfo",fmoenyinfo);
        modelAndView.addObject("b2",b2);
        modelAndView.setViewName("fund/creditline");
        return  modelAndView;
    }

    /**
     * 授信资金池跳转
     * @return
     */
    @RequestMapping("/settingupcreditfunds")
    public ModelAndView Settingupcreditfunds(){
        return  new ModelAndView("fund/settingupcreditfunds");
    }

    /***
     * 今日授信资金
     * @return
     */
    @RequestMapping("/settingupcreditfundsday")
    public ModelAndView Settingupcreditfundsday(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String fid = (String) get(request,"session_fund_id");
        FMoneyInfo fmoenyinfo = fMoneyInfoService.selectOne(new EntityWrapper <FMoneyInfo>().eq("fid", fid));
        BigDecimal multiply = fmoenyinfo.getMoney().multiply(BigDecimal.valueOf(0.0001));
        modelAndView.addObject("daymoney",multiply);
        modelAndView.setViewName("fund/settingupcreditfundsday");
        System.out.println(fmoenyinfo.getMoney()+"-----------------------------------");
        return  modelAndView;
    }

    /**
     * 开启授信资金管理状态
     * @return
     */
    @RequestMapping("/insertStatus")
    public R insertMoenymanagement(HttpServletRequest request){
        R r=new R();
        String fid = (String) get(request,"session_fund_id");
        String status = request.getParameter("status");
        FMoneyInfo fmoenyinfo = fMoneyInfoService.selectOne(new EntityWrapper <FMoneyInfo>().eq("fid", fid));
        fmoenyinfo.setStatus(status);
        fMoneyInfoService.updateById(fmoenyinfo);
        return r;
    }


    /**
     * 添加资金池额度
     * @return
     */
    @RequestMapping("/insertMoeny")
    public R insertMoeny(HttpServletRequest request){
        try {
            String fid = (String) get(request,"session_fund_id");
            String moenys = request.getParameter("moeny");
            BigDecimal moeny = Calculate.multiply_10000(new BigDecimal(moenys)).getAmount();
            FMoneyInfo fmoenyinfo = fMoneyInfoService.selectOne(new EntityWrapper <FMoneyInfo>().eq("fid", fid));
            FMoneyOtherInfo fmoenyotherinfo = new FMoneyOtherInfo();
            fmoenyotherinfo.setFmid(fmoenyinfo.getId());
            fmoenyotherinfo.setFid(fid);
            fmoenyotherinfo.setMoney(moeny);
            fmoenyotherinfo.setType("1");
            fmoenyotherinfo.setCreateDate(new Date());
            BigDecimal m = fmoenyinfo.getMoney();
            BigDecimal bm = moeny.add(m);
            fmoenyinfo.setMoney(bm);
            fMoneyInfoService.updateById(fmoenyinfo);
            ifMoneyOtherInfoService.insert(fmoenyotherinfo);
        }catch (Exception e){
            e.printStackTrace();
            throw new CheckedException("保存失败");
        }
        return  R.newOK();
    }

    /**
     * 当前资金池总额
     * @return
     */
    @RequestMapping("/SumMoeny")
    public R SumMoeny(HttpServletRequest request){
        R r= new R();
        String fid = (String) get(request,"session_fund_id");
        BigDecimal summoney= ifMoneyOtherInfoService.Summoney(fid);
        r.setData(summoney);
        return  r;
    }

    /**
     * 添加当前资金金额 （不能大于资金池额度）
     * @return
     */
    @RequestMapping("/dayinsertMoeny")
    public R dayinsertMoeny(HttpServletRequest request){
        R r=new R();
        String fid = (String) get(request,"session_fund_id");
        String daymoenys = request.getParameter("daymoeny");
        BigDecimal daymoeny = Calculate.multiply_10000(new BigDecimal(daymoenys)).getAmount();
        FMoneyInfo fmoenyinfo = fMoneyInfoService.selectOne(new EntityWrapper <FMoneyInfo>().eq("fid", fid));
//        BigDecimal dayMoneys= fmoenyinfo.getDayMoney();
//        BigDecimal bdaymoeny = dayMoneys.add(daymoeny);
        fmoenyinfo.setDayMoney(daymoeny);
        boolean b = fMoneyInfoService.updateById(fmoenyinfo);
        if(b){
            r.setMsg("添加成功");
        }else {
            r.setMsg("添加失败");
        }
        return  r;
    }


    /**
     * 今日授信额度
     * @return
     */
    @RequestMapping("/DaySummoney")
    public R  DaySummoney(HttpServletRequest request){
        R r=new R();
        String fid = (String) get(request,"session_fund_id");
        String start = DateUtil.getTimesmorning();
        String end = DateUtil.getTimesnight();
        BigDecimal b = ifMoneyOtherInfoService.DaySummoney(start, end,fid);
        r.setData(b);
        return  r;
    }

    /**
     * 累计授信总额
     * @return
     */
    @RequestMapping("/LJSummoney")
    public R  LJSummoney(HttpServletRequest request){
        R r=new R();
        String fid = (String) get(request,"session_fund_id");
        BigDecimal b = ifMoneyOtherInfoService.LJSummoney(fid);
        r.setData(b);
        return  r;
    }

    /**
     * 每日授信限额总额
     * @return
     */
    @RequestMapping("/DaySummoneys")
    public R  DaySummoneys(HttpServletRequest request){
        R r=new R();
        String fid = (String) get(request,"session_fund_id");
        BigDecimal b = ifMoneyOtherInfoService.DayYUmoneys(fid);
        r.setData(b);
        return  r;
    }
}
