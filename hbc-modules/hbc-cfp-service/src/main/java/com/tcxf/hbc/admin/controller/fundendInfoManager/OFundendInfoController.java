package com.tcxf.hbc.admin.controller.fundendInfoManager;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.FFundendDto;
import com.tcxf.hbc.admin.service.IFFundendBindService;
import com.tcxf.hbc.admin.service.IFFundendService;
import com.tcxf.hbc.admin.service.ITbRepaymentRatioDictService;
import com.tcxf.hbc.admin.service.ITbSettlementDetailService;
import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运营商端 资金端管理
 * liaozeyong
 * 2018年6月30日10:28:55
 */
@RestController
@RequestMapping("/opera/ofundend")
public class OFundendInfoController extends BaseController {

    @Autowired
    private IFFundendService ifFundendService;
    @Autowired
    private ITbRepaymentRatioDictService iTbRepaymentRatioDictService;
    @Autowired
    private ITbSettlementDetailService iTbSettlementDetailService;
    @Autowired
    private IFFundendBindService ifFundendBindService;

    /**
     * 跳转资金端列表界面
     * @return
     */
    @RequestMapping("/loadfindByOid")
    public ModelAndView loadfindByOid(){
        return new ModelAndView("manager/FundendInfoManager");
    }

    /**
     * 根据运营商id分页查询资金端
     * liaozeyong
     * @param
     * @return
     */
    @RequestMapping("/selectFundendByOid")
    public R selectFunendByOid(HttpServletRequest request)
    {
        R r = new R();
        String p = (String) request.getParameter("page");
        //获取session中的运营商id
        String id = (String) get(request , "session_opera_id");
        //把id放入map集合中
        HashMap<String, Object> map = new HashMap<>();
        map.put("page",p);
        map.put("id",id);
        Page page = ifFundendService.selectFunendByOid(map);
        r.setData(page);
        return r;
    }

    /**
     * 跳转查看资金端详情页面
     * @return
     */
    @RequestMapping("/loadfundupfind")
    public ModelAndView loadfundupfind(HttpServletRequest request , String id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id",id);
        FFundend fundend = ifFundendService.selectOne(new EntityWrapper<FFundend>().eq("id",id));
        TbRepaymentRatioDict tbRepaymentRatioDict = iTbRepaymentRatioDictService.selectOne(new EntityWrapper<TbRepaymentRatioDict>().eq("fid",fundend.getId()).eq("oid",get(request , "session_opera_id")));
        modelAndView.addObject("entity",fundend);
        modelAndView.addObject("tbRepaymentRatioDict",tbRepaymentRatioDict);
        modelAndView.setViewName("manager/fundendfundfind");
        return  modelAndView;
    }

    /**
     * 运营商查询商户结算
     * @return
     */
    @RequestMapping("/settlementDetailManagerList")
    public R findOperaSettlement(HttpServletRequest request){
        R r = new R();
        String p = (String) request.getParameter("page");
        String oid = (String) get(request , "session_opera_id");
        String status = request.getParameter("status");
        Map<String , Object> map = new HashMap<>();
        map.put("oid",oid);
        //设置代表已结算
        map.put("status",status);
        Page page = iTbSettlementDetailService.findOperaSettlement(map);
        r.setData(page);
        return  r;
    }

    /**
     * 设置默认资金端
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("/updateForFundo")
    public ModelAndView updateForFundo(HttpServletRequest request , String id){
        String oid = (String) get(request , "session_opera_id");
        List<FFundendBind> list = ifFundendBindService.selectList(new EntityWrapper<FFundendBind>().eq("oid",oid));
        for (FFundendBind fundendBind:list) {
            //把取消原有默认资金端
            if (fundendBind.getStatus().equals("1"))
            {
                fundendBind.setStatus("0");
            }else if (fundendBind.getFid().equals(id)&&fundendBind.getOid().equals(oid))//设置修改后的资金端
            {
                fundendBind.setStatus("1");
            }
            ifFundendBindService.updateById(fundendBind);
        }
        return new ModelAndView("manager/FundendInfoManager");
    }
}
