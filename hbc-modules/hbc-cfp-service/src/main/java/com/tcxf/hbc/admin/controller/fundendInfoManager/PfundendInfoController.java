package com.tcxf.hbc.admin.controller.fundendInfoManager;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.vo.FFundendVo;
import com.tcxf.hbc.admin.model.vo.TbRepaymentRatioDictVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.secure.MD5Util;
import com.tcxf.hbc.common.vo.CreditVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import sun.rmi.runtime.NewThreadAction;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * liaozeyong
 * 2018年6月13日13:55:57
 * 平台端--资金端绑定运营商模块
 */
@RestController
@RequestMapping("/platfrom/fundend")
public class PfundendInfoController extends BaseController {

    @Autowired
    private IFFundendService ifFundendService;
    @Autowired
    private IFFundendBindService ifFundendBindService;
    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    @Autowired
    private ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    private ITbRepaymentRatioDictService iTbRepaymentRatioDictService;
    @Autowired
    private ITbWalletService iTbWalletService;
    @Autowired
    private ITbPeopleRoleService iTbPeopleRoleService;
    @Autowired
    private ThreelementController threelementController;

    /**
     * 跳转绑定资金端界面
     * liaozeyong
     * 2018年6月8日09:15:14
     * @return
     */
    @RequestMapping("/loadfind")
    public ModelAndView loadfind()
    {
        return new ModelAndView("platfrom/FundendInfoManager");
    }

    /**
     * 分页查询资金端
     * liaozeyong
     * @param
     * @return
     */
    @RequestMapping("/loadfundendManagerList")
    public R loadfundendManagerList(HttpServletRequest request)
    {
        R r = new R();
        String name = (String) request.getParameter("name");
        String p = (String) request.getParameter("page");
        HashMap<String, Object> map = new HashMap<>();
        map.put("page",p);
        Query<FFundend> query = new Query<>(map);
        if ("".equals(name))
        {
            Page page = ifFundendService.selectPage(query,new EntityWrapper<FFundend>().orderDesc(Collections.singleton("create_date")));
            r.setData(page);
        }else {
            Page page = ifFundendService.selectPage(query,new EntityWrapper<FFundend>().like("fname",name).orderDesc(Collections.singleton("create_date")));
            r.setData(page);
        }
        return r;
    }

    /**
     * 跳转添加资金端界面
     * liaozeyong
     * @return
     */
    @RequestMapping("/loadfundAdd")
    public ModelAndView loadfundAdd() {
        return new ModelAndView("platfrom/fundendHandler");
    }

    /**
     * 跳转查看运营商管理详情界面
     * @param id
     * @return
     */
    @GetMapping("/loadfundDetails")
    public ModelAndView loadfundAddyys(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        modelAndView.setViewName("platfrom/fundendYys");
        return modelAndView;
    }

    /**
     * 跳转编辑资金端界面
     * 传值：id和fFundendDto用于页面默认值
     * @param id
     * @return
     */
    @GetMapping("/loadfundupfind")
    public ModelAndView loadfundupfind(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        FFundend fFundendDto = ifFundendService.selectOne(new EntityWrapper<FFundend>().eq("id",id));
        modelAndView.addObject("id",id);
        modelAndView.addObject("fFundendDtos",fFundendDto);
        modelAndView.setViewName("platfrom/fundendfundfind");
        return  modelAndView;
    }

    /**
     * 查看资金端绑定运营商详情展示
     * @param id
     * @return
     */
    @RequestMapping("/loadfundendoidfind")
    public R loadfundendoidfind(String id)
    {
        R r = new R();
        Map<String, Object> params = new HashMap<>();
        Page page = ioOperaInfoService.getOperaInfoByFid(params,id);
        r.setData(page);
        return r;
    }

    /**
     * 资金端编辑，查看绑卡信息详情
     * liaozeyong
     * @param id
     * @return
     */
    @RequestMapping("/loadBindCard")
    public R loadBindCard(String id)
    {
        R r = new R();
        Map<String, Object> params = new HashMap<>();
        params.put("id",id);
        Page page = iTbBindCardInfoService.selectBindCardList(params);
        r.setData(page);
        return r;
    }

    /**
     * 根据资金端id删除绑定运营商
     * @param id
     * @return
     */
    @RequestMapping("/delBindCard")
    public R delBindCard(String id)
    {
        R r = new R();
        boolean b = iTbBindCardInfoService.deleteById(id);
        if(b)
        {
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 修改资金端绑卡信息
     * @param tbBindCardInfo
     * @return
     */
    @RequestMapping("/updateBindCard")
    public R updateBindCard(TbBindCardInfo tbBindCardInfo)
    {
        R r = new R();
        //获取到原来的值
        CreditVo creditVo = new CreditVo();
        creditVo.setMobile(tbBindCardInfo.getMobile());
        creditVo.setAccountNO(tbBindCardInfo.getBankCard());
        creditVo.setIdCard(tbBindCardInfo.getIdCard());
        creditVo.setUserName(tbBindCardInfo.getName());
        R r1 = threelementController.threelementCheck(creditVo);
        if (r1.getCode() == 1){
            r1.setMsg("银行卡验证失败");
            return r1;
        }
        TbBindCardInfo tbBindCardInfo1 = iTbBindCardInfoService.selectOne(new EntityWrapper<TbBindCardInfo>().eq("id",tbBindCardInfo.getId()));
        //把修改的值赋值上去
        tbBindCardInfo1.setBankCard(tbBindCardInfo.getBankCard());
        tbBindCardInfo1.setBankName(tbBindCardInfo.getBankName());
        tbBindCardInfo1.setName(tbBindCardInfo.getName());
        tbBindCardInfo1.setMobile(tbBindCardInfo.getMobile());
        tbBindCardInfo1.setBankSn(tbBindCardInfo.getBankSn());
        tbBindCardInfo1.setIdCard(tbBindCardInfo.getIdCard());
        tbBindCardInfo1.setBankCard(tbBindCardInfo.getBankCard());
        tbBindCardInfo1.setIsDefault(tbBindCardInfo.getIsDefault());
        //调用修改的方法
        boolean b = iTbBindCardInfoService.updateById(tbBindCardInfo1);
        if (b)
        {
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return r;
    }

    /**
     * 添加资金端绑卡信息
     * @param tbBindCardInfo
     * @return
     */
    @RequestMapping("/newBindCard")
    public R newBindCard(TbBindCardInfo tbBindCardInfo)
    {
        R r = new R();
        //设置此为平台端的，平台端为4
        tbBindCardInfo.setUtype("4");
        boolean b = iTbBindCardInfoService.insert(tbBindCardInfo);
        if (b)
        {
            r.setMsg("添加成功");
        }else {
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     * 编辑资金端修改功能
     * @param fFundendVo
     * @return
     */
    @RequestMapping("/fundInfoEditAjaxSubmit")
    public R fundInfoEditAjaxSubmit(FFundendVo fFundendVo)
    {
        R r = new R();
        FFundend fundend =ifFundendService.selectOne(new EntityWrapper<FFundend>().eq("id",fFundendVo.getId()));
        TbRepaymentRatioDict tbRepaymentRatioDict = new TbRepaymentRatioDict();
        //把vo里面的值拿出来放到entity里面
        fundend.setMobile(fFundendVo.getMobile());
        fundend.setFname(fFundendVo.getFname());
        fundend.setName(fFundendVo.getName());
        fundend.setBankcard(fFundendVo.getBankcard());
        fundend.setAddress(fFundendVo.getAreaId());
        fundend.setAddressInfo(fFundendVo.getAddressInfo());
        tbRepaymentRatioDict.setYqRate(fFundendVo.getYqRate());
        tbRepaymentRatioDict.setServiceFee(fFundendVo.getServiceFee());
        //调用修改的方法
        boolean b1 = ifFundendService.updateById(fundend);
        TbWallet tbWallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid",fFundendVo.getId()));
        tbWallet.setWname(fFundendVo.getFname());
        tbWallet.setUname(fFundendVo.getName());
        iTbWalletService.updateById(tbWallet);
        if (b1)
        {
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return  r;
    }

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 添加资金端
     * @param fFundendVo
     * @return
     */
    @RequestMapping("/fundInfoAddAjaxSubmit")
    public R fundInfoAddAjaxSubmit(FFundendVo fFundendVo)
    {
        R r = ifFundendService.addFundend(fFundendVo);
        return r;
    }

    /**
     * 根据资金端查询已绑定和未绑定运营商
     * @return
     */
    @RequestMapping("/loadfundendoidList")
    public R loadfundendoidList(HttpServletRequest request,String id , String name){
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("id",id);
        paramMap.put("name",name);
        Page page = ioOperaInfoService.selectOperaByFundend(paramMap);
        r.setData(page);
        return r;
    }

    /**
     * 根据id查询还款比率
     * @param id
     * @param fid
     * @return
     */
    @RequestMapping("/redactOpera")
    public ModelAndView redactOpera(String id,String fid){
        ModelAndView modelAndView = new ModelAndView();
        TbRepaymentRatioDict tbRepaymentRatioDict = iTbRepaymentRatioDictService.selectOne(
                new EntityWrapper<TbRepaymentRatioDict>().eq("oid",id).eq("fid",fid));
        modelAndView.addObject("tbRepaymentRatioDict",tbRepaymentRatioDict);
        modelAndView.setViewName("platfrom/redactOpera");
        return modelAndView;
    }

    /**
     * 修改绑定资金端的还款比率
     * @param tbRepaymentRatioDictVo
     * @return
     */
    @RequestMapping("/redactOperaEditAjaxSubmit")
    public R redactOperaEditAjaxSubmit(TbRepaymentRatioDictVo tbRepaymentRatioDictVo){
        R r = new R();
        boolean insert = iTbRepaymentRatioDictService.update(tbRepaymentRatioDictVo,
                new EntityWrapper<TbRepaymentRatioDict>().eq("oid",tbRepaymentRatioDictVo.getOid()).eq("fid",tbRepaymentRatioDictVo.getFid()));
        r.setMsg("修改成功");
        return  r;
    }

    /**
     * 跳转立即绑定界面
     * @param id
     * @param fid
     * @return
     */
    @RequestMapping("/loadfundBinding")
    public  ModelAndView loadfundBinding(String id,String fid){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id",id);
        modelAndView.addObject("fid",fid);
        modelAndView.setViewName("platfrom/redactOperaAdd");
        return modelAndView;
    }

    /**
     * 资金端绑定运营商
     * @param tbRepaymentRatioDict
     * @return
     */
    @RequestMapping("/redactOperaAddAjaxSubmit")
    public R redactOperaAddAjaxSubmit(@RequestBody TbRepaymentRatioDict tbRepaymentRatioDict){
        R r = new R();
        String oid = tbRepaymentRatioDict.getOid();
        String fid = tbRepaymentRatioDict.getFid();
        FFundendBind fFundendBind = new FFundendBind();
        fFundendBind.setStatus("0");
        fFundendBind.setCreateDate(new Date());
        fFundendBind.setOid(oid);
        fFundendBind.setFid(fid);
        ifFundendBindService.insert(fFundendBind);
        tbRepaymentRatioDict.setCreateDate(new Date());
        boolean insert = iTbRepaymentRatioDictService.insert(tbRepaymentRatioDict);
        r.setMsg("绑定成功");
        return r;
    }
}
