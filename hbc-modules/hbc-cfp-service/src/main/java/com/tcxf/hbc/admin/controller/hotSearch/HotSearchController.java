package com.tcxf.hbc.admin.controller.hotSearch;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.MHotSearchDto;
import com.tcxf.hbc.admin.model.vo.HotSearchVo;
import com.tcxf.hbc.admin.service.IMHotSearchService;
import com.tcxf.hbc.admin.service.IMMerchantInfoService;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 运营商端  --热门搜索
 * liaozeyong
 * 2018年6月30日10:29:51
 */
@RestController
@RequestMapping("/opera/hotSearch")
public class HotSearchController extends BaseController {

    @Autowired
    private IMHotSearchService imHotSearchService;
    @Autowired
    private IMMerchantInfoService imMerchantInfoService;
    /**
     * 跳转热门搜索界面
     * @return
     */
    @RequestMapping("/hotSearchManager")
    public ModelAndView hotSearchManager() {
        return new ModelAndView("manager/hotSearchManager");
    }

    /**
     * 分页展示页面搜索
     * @return
     */
    @RequestMapping("/hotSearchManagerList")
    public R hotSearchManagerList(HttpServletRequest request){
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String id = (String) get(request , "session_opera_id");
        paramMap.put("oid",id);
        Page page = imHotSearchService.selectHotSearchByRid(paramMap);
        r.setData(page);
        return r;
    }

    /**
     * 跳转至添加热门搜索界面
     *
     * @return
     * @author liaozeyong
     * @date 2018年6月27日08:51:01
     * @throws
     */
    @RequestMapping("/forwardHotSearchAdd")
    public ModelAndView forwardHotSearchAdd() {
        return new ModelAndView("manager/hotSearchHandler");
    }

    /**
     * 分配资源打开商户列表
     * @return
     */
    @RequestMapping("/merchantInfoManager")
    public ModelAndView merchantInfoManager() {
        return new ModelAndView("manager/merchantInfoManagerSel");
    }

    /**
     * 商户列表信息
     *
     * @param request
     * @return
     * @author liaozeyong
     * @date 2018年6月27日10:27:46
     * @throws
     */
    @RequestMapping("/merchantInfoManagerList")
    public R  merchantInfoManagerList(HttpServletRequest request) {
        R r = new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String oid = (String) get(request , "session_opera_id");
        paramMap.put("oid",oid);
        Page page = imMerchantInfoService.findByOid(paramMap);
        r.setData(page);
        return r;
    }

    /**
     * 添加热门搜索方法
     * liaozeyong
     * @param
     */
    @RequestMapping("/hotSearchAddAjaxSubmit")
    public R hotSearchAddAjaxSubmit(HttpServletRequest request, MHotSearch mHotSearch) {
        R r = new R();
        String oid = (String) get(request , "session_opera_id");
        mHotSearch.setOid(oid);
        boolean b = imHotSearchService.insert(mHotSearch);
        if (b)
        {
            r.setMsg("添加成功");
        }else {
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     * 热门搜索删除
     * @param id
     * @return
     */
    @RequestMapping("/forwardHotSearchDel")
    public R forwardHotSearchDel(String id){
        R r = new R();
        Boolean b = imHotSearchService.delete(new EntityWrapper<MHotSearch>().eq("id",id));
        if (b)
        {
            r.setMsg("删除成功");
        }else {
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 跳转修改热门搜索界面
     * @param id
     * @return
     */
    @RequestMapping("/forwardHotSearchEdit")
    public ModelAndView forwardHotSearchEdit(HttpServletRequest request , String id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        Map<String , Object> map = new HashMap<>();
        map.put("oid",get(request , "session_opera_id"));
        map.put("hid",id);
        MHotSearchDto mHotSearchDto = imHotSearchService.selectHotSearch(map);
        modelAndView.addObject("entity",mHotSearchDto);
        modelAndView.setViewName("manager/hotSearchHandlerEdit");
        return modelAndView;
    }

    /**
     * 热门搜索修改方法
     * @return
     */
    @RequestMapping("/hotSearchEditAjaxSubmit")
    public R hotSearchEditAjaxSubmit(MHotSearch mHotSearch) {
        R r = new R();
        boolean b = imHotSearchService.updateById(mHotSearch);
        if (b)
        {
            r.setMsg("修改成功");
        }else {
            r.setMsg("修改失败");
        }
        return r;
    }
}
