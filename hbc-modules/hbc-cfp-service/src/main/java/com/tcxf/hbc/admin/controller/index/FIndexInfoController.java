package com.tcxf.hbc.admin.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/fund/f_index")
public class FIndexInfoController extends BaseController {

    @Autowired
    protected ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    protected ICUserInfoService iCUserInfoService;

    @Autowired
    protected IMMerchantInfoService iMMerchantInfoService;

    @Autowired
    protected IOOperaInfoService iOOperaInfoService;

    @Autowired
    protected ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    protected ITbSettlementDetailService iTbSettlementDetailService;

    @Autowired
    protected IFFundendBindService iFFundendBindService;

    @Autowired
    protected ITbRepaymentPlanService iTbRepaymentPlanService;
    @Autowired
    private IFFundendService ifFundendService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:50 2018/6/21
     * 跳转资金端首页
    */
    @GetMapping("/test")
    public ModelAndView test()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/layout/center");
        return mode;
    }

    /**
     * 跳转修改密码页面
     */
    @GetMapping("/goToUpdatePassWordPage")
    public ModelAndView goToUpdatePassWordPage(HttpServletRequest request)
    {
        String type = request.getParameter("type");
        String pwd = request.getParameter("pwd");
        ModelAndView mode= new ModelAndView();
        mode.addObject("type",type);
        mode.addObject("pwd",pwd);
        mode.setViewName("fund/layout/updatePassword");
        return mode;
    }

    /**
     * 修改密码
     */
    @RequestMapping("/updatePassWord")
    public R updatePassWord(HttpServletRequest request,@RequestBody String json)
    {
        Map<String,String> map = JsonTools.parseJSON2Map(json);
        String ypwd = map.get("ypwd");
        String password = map.get("password");
        String id = (String) get(request,"session_fund_id");
        PAdmin pAdmin = new PAdmin();
        pAdmin.setId(id);
        FFundend selectFFundend = ifFundendService.selectById(id);
        //判断密码
        if (!ENCODER.matches(ypwd,selectFFundend.getPwd()))
        {
            return R.newErrorMsg("原密码错误");
        }
        selectFFundend.setPwd(ENCODER.encode(password));
        ifFundendService.updateById(selectFFundend);
        return R.newOK("修改成功",map.get("type"));
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 13:55 2018/6/5
     */
    @GetMapping("/info")
    @ResponseBody
    public R getIndexInfo(HttpServletRequest request)
    {
        String fid = (String) get(request,"session_fund_id");

        //查询资金端所有的运营商
        List<FFundendBind> flist = iFFundendBindService.selectList(new EntityWrapper<FFundendBind>().eq("fid", fid));

        // 七天的交易记录(1 今日 2 昨日 3 本周 4 本月)
       String time = request.getParameter("dateTime")==null ? "1" :  request.getParameter("dateTime");
        Integer dateTime = Integer.parseInt(time);

        //处理开始时间结束时间
        HashMap<String, Object> resultMap = DateUtil.publicMethodForTime(dateTime);
        String startDate =  DateUtil.getDateHavehms((Date) resultMap.get("startDate"));
        String endDate =  DateUtil.getDateHavehms((Date) resultMap.get("endDate"));

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("startDate",startDate);
        paramMap.put("endDate",endDate);
        paramMap.put("fid",fid);
        paramMap.put("isFund","1");//资金端不需要非授信消费其它端需要统计


        //已授信(用户数量+商户数量)
        int authUserNum = iCUserInfoService.findAlreadyAuthNum(paramMap);//用户数量
        int authMerNum = iMMerchantInfoService.findAlreadyAuthNum(paramMap);//商户数量
        int totalNum=authUserNum+authMerNum;

        //逾期用户数量
        int overDueNum =  iTbRepaymentPlanService.findoverDueNum(paramMap);

        //运营商
        int operaNum = iFFundendBindService.selectCount(new EntityWrapper<FFundendBind>().eq("fid",fid).gt("create_date",startDate).lt("create_date",endDate));

        //已还款 （用户分期的  +　用户未分期）
        BigDecimal payBackMoney=iTbRepaymentDetailService.findAlreadyPayBackPlan(paramMap);

        //已结算 （用户分期的  +　用户未分期）
        BigDecimal SettledMoney=iTbSettlementDetailService.findPAlreadySettled(paramMap);

        //交易金额
        BigDecimal tradeMoney=new BigDecimal("0");
        if(flist!=null && flist.size()>0){
            for (FFundendBind fFundendBind:flist) {
                paramMap.put("isFund","1");//资金端不需要非授信消费其它端需要统计
                paramMap.put("oid",fFundendBind.getOid());
                tradeMoney= tradeMoney.add(iTbTradingDetailService.selectTotalMoney(paramMap));
            }
        }

        //数据总和 :交易金额
        BigDecimal pTradeMoney=new BigDecimal("0");
        Map<String, Object> tradeMap = new HashMap<>();
        if(flist!=null && flist.size()>0){
            for (FFundendBind fFundendBind:flist) {
                tradeMap.put("oid",fFundendBind.getOid());
                tradeMap.put("isFund","1");//资金端不需要非授信消费其它端需要统计
                pTradeMoney= pTradeMoney.add(iTbTradingDetailService.selectTotalMoney(tradeMap));//交易金额
            }
        }

        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("fid",fid);
        //数据总和 :(已还款 已结算 待结算 逾期 待还款)
        Map<String,Object>  pMap=iTbTradingDetailService.findPtotalData(totalMap);
        BigDecimal pPayback =(BigDecimal) pMap.get("pPayback");//已还款
        BigDecimal pAlreadySettled =(BigDecimal) pMap.get("pAlreadySettled");//已结算
        BigDecimal pWaitSettled =(BigDecimal) pMap.get("pWaitSettled");//待结算
        BigDecimal overDue =(BigDecimal) pMap.get("overDue");//逾期
        BigDecimal waitPay =(BigDecimal) pMap.get("waitPay");//待还款

        //近七天交易金额
        List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
        Map<String, Object> fmap = new HashMap<>();

        if(flist!=null && flist.size()>0){
            for (FFundendBind fFundendBind:flist) {
                fmap.put("oid",fFundendBind.getOid());
                fmap.put("isFund","1");//资金端不需要非授信消费其它端需要统计
                List<Map<String,Object>> listMap =iTbTradingDetailService.findsevendaytrade(fmap);
                allList.addAll(listMap);
            }
        }
        BigDecimal sixMoney=new BigDecimal("0");
        BigDecimal fiveMoney=new BigDecimal("0");
        BigDecimal fourMoney=new BigDecimal("0");
        BigDecimal threeMoney=new BigDecimal("0");
        BigDecimal secMoney=new BigDecimal("0");
        BigDecimal firMoney=new BigDecimal("0");
        BigDecimal currentMoney=new BigDecimal("0");

        //处理成数组
        Map<String, Object> map = dealSevenDayData(allList, sixMoney, fiveMoney, fourMoney, threeMoney, secMoney, firMoney, currentMoney);
        String[] dtimeArr =(String[]) map.get("dTime");
        BigDecimal[] tMoneyArr =(BigDecimal[]) map.get("sMoney");

        //返回值
        R r=  new R<>();
        HashMap<Object, Object> mmp = new HashMap<>();
        mmp.put("dTimeArr",dtimeArr);//时间
        mmp.put("tMoneyArr",tMoneyArr);//资金
        mmp.put("totalNum",totalNum);//已授信用户
        mmp.put("overDueNum",overDueNum);//逾期用户数量
        mmp.put("operaNum",operaNum);//运营商
        mmp.put("tradeMoney",tradeMoney);//交易金额
        mmp.put("payBackMoney",payBackMoney);//已还款
        mmp.put("SettledMoney",SettledMoney);//已结算


        //数据总和
        mmp.put("pTradeMoney",pTradeMoney);//交易金额
        mmp.put("pPayback",pPayback);//已还款
        mmp.put("pAlreadySettled",pAlreadySettled);//已结算
        mmp.put("overDue",overDue);//逾期
        mmp.put("waitPay",waitPay);//待还
        mmp.put("pWaitSettled",pWaitSettled);//待结算
        r.setData(mmp);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:38 2018/6/25
     * 处理近七天的数据
    */
     private Map<String,Object> dealSevenDayData(List<Map<String, Object>> listMap, BigDecimal sixMoney, BigDecimal fiveMoney, BigDecimal fourMoney, BigDecimal threeMoney, BigDecimal secMoney, BigDecimal firMoney, BigDecimal currentMoney) {
        BigDecimal[] sMoney = new BigDecimal[7];
        String[] dTime = new String[7];
        Map<String, Object> resultMap = new HashMap<>();

         for (Map<String,Object> mmp:listMap) {
             if (DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()).equals(mmp.get("dateTime"))){
                 sixMoney= sixMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),1).equals(mmp.get("dateTime"))){
                 fiveMoney= fiveMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),2).equals(mmp.get("dateTime"))){
                 fourMoney= fourMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),3).equals(mmp.get("dateTime"))){
                 threeMoney= threeMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),4).equals(mmp.get("dateTime"))){
                 secMoney= secMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),5).equals(mmp.get("dateTime"))){
                 firMoney= firMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
             if (DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),6).equals(mmp.get("dateTime"))){
                 currentMoney= currentMoney.add((BigDecimal)mmp.get("totalMoney"));
             }
         }
         //近七天交易金额
         sMoney[0]=sixMoney;
         sMoney[1]=fiveMoney;
         sMoney[2]=fourMoney;
         sMoney[3]=threeMoney;
         sMoney[4]=secMoney;
         sMoney[5]=firMoney;
         sMoney[6]=currentMoney;

         //所属天数
         dTime[0]=DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()).substring(5);
         dTime[1]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),1).substring(5);
         dTime[2]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),2).substring(5);
         dTime[3]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),3).substring(5);
         dTime[4]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),4).substring(5);
         dTime[5]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),5).substring(5);
         dTime[6]=DateUtil.addDay(DateUtil.getDateTimeforManDay(DateUtil.getBeforeSixDay()),6).substring(5);
         resultMap.put("sMoney",sMoney);
         resultMap.put("dTime",dTime);
         return resultMap;
    }


}
