package com.tcxf.hbc.admin.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IIndexBannerService;
import com.tcxf.hbc.admin.service.IMMerchantInfoService;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sanxi
 * 运营商首页图片
 */
@RestController
@RequestMapping("/opera/banner")
public class OBannerController extends BaseController {
    @Autowired
    private IIndexBannerService iIndexBannerService;
    @Autowired
    private IMMerchantInfoService imMerchantInfoService;

    /**
     * 跳转到banner查看页面
     * @return
     */
    @RequestMapping("/banner")
    public ModelAndView banner(){
        ModelAndView mode = new ModelAndView();
        mode.setViewName("manager/activity/indexBanner");
        return mode;
    }

    /**
     * 根据登陆的运营商id查询首页图片
     * @return
     */
    @RequestMapping("/bannerList")
    public R bannerList(HttpServletRequest request){
        R r = new R();
        //获取登陆的id
        String aid = get(request,"session_opera_id").toString();
       List<IndexBanner>  list = iIndexBannerService.selectList(new EntityWrapper<IndexBanner>().eq("oid",aid));
       r.setData(list);
       return  r;
    }

    /**
     * 添加首页图片
     * @param banner
     * @return
     */
    @RequestMapping("/insert")
    public R insert(HttpServletRequest request, IndexBanner banner){
        R r = new R();
        //获取登陆的id
        String oid = get(request,"session_opera_id").toString();
        banner.setOid(oid);
        try {
            if (iIndexBannerService.insert(banner)) {
                r.setMsg("添加成功");
            } else {
                r.setMsg("添加失败");
            }
        }catch (Exception e){
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     * 修改首页图片
     * @param banner
     * @return
     */
    @RequestMapping("/update")
    public R update(IndexBanner banner){
        R r = new R();
        try {
            if(iIndexBannerService.updateById(banner)){
                r.setMsg("修改成功");
            }else{
                r.setMsg("修改失败");
            }
        }catch (Exception e){
            r.setMsg("修改失败");
        }
        return  r;
    }

    /**
     *
     *根据id删除
     * @param id
     * @return
     */
    @RequestMapping("/deleteById")
    public  R deleteById(String id){
        R r = new R();
        try {
            if(iIndexBannerService.deleteById(id)){
                r.setMsg("删除成功");
            }else {
                r.setMsg("删除失败");
            }
        }catch (Exception e){
            r.setMsg("删除失败");
        }
        return  r;
    }

    /**
     *
     * 跳转到选择商户的页面
     * @return
     */
    @RequestMapping("/findMerchant")
    public ModelAndView findMerchant(String id){
        ModelAndView mode = new ModelAndView();
        mode.addObject("mid",id);
        mode.setViewName("manager/activity/indexBannerHref");
        return mode;
    }

    /**
     * 查询运营商下所有的商户
     * @param request
     * @return
     */
    @RequestMapping("/findMerchantList")
    public R findMerchantList(HttpServletRequest request,String name,String page){
        R r = new R();
        //获取登陆的id
        String oid = get(request,"session_opera_id").toString();
        //查询该管理员下的所有角色
        Map<String,Object> map = new HashMap<>();
        map.put("oid",oid);
        map.put("name",name);
        map.put("page",page);
       Page page1 = imMerchantInfoService.selectPage(map);
        r.setData(page1);
        return r;
    }

    /**
     * 选择商户之后设置好链接的地址和名称
     * @param name
     * @return
     */
    @RequestMapping("/bannerHref")
    public R bannerHref(String name){
        R r = new R();
        Map<String,Object> map = new HashMap<>();
        String [] i = name.split(",");
        map.put("name","商户名称:"+i[0]);
        map.put("mid",i[1]);
        map.put("id",i[2]);
        r.setData(map);
        return r;
    }
}
