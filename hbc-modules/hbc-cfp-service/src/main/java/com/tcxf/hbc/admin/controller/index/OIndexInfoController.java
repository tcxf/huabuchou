package com.tcxf.hbc.admin.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
/**
 * @author YWT_tai
 * @Date :Created in 15:04 2018/6/21
 */

@RestController
@RequestMapping("/opera/o_index")
public class OIndexInfoController extends BaseController {

    @Autowired
    protected ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    protected ICUserInfoService iCUserInfoService;

    @Autowired
    protected IMMerchantInfoService iMMerchantInfoService;

    @Autowired
    protected IOOperaInfoService iOOperaInfoService;

    @Autowired
    protected ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    protected ITbSettlementDetailService iTbSettlementDetailService;

    @Autowired
    protected ICUserRefConService ICUserRefConService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:43 2018/6/21
     * 跳转运营商管理主页面
    */
    @GetMapping("/jumpOCenter")
    public ModelAndView jumpOCenter()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/layout/center");
        return mode;
    }
    /**
     * 跳转修改密码页面
     */
    @GetMapping("/goToUpdatePassWordPage")
    public ModelAndView goToUpdatePassWordPage(HttpServletRequest request)
    {
        String type = request.getParameter("type");
        String pwd = request.getParameter("pwd");
        ModelAndView mode= new ModelAndView();
        mode.addObject("type",type);
        mode.addObject("pwd",pwd);
        mode.setViewName("manager/layout/updatePassword");
        return mode;
    }

    /**
     * 修改密码
     */
    @RequestMapping("/updatePassWord")
    public R updatePassWord(HttpServletRequest request,@RequestBody String json)
    {
        Map<String,String> map = JsonTools.parseJSON2Map(json);
        String ypwd = map.get("ypwd");
        String password = map.get("password");
        String id = (String) get(request,"session_opera_id");
        PAdmin pAdmin = new PAdmin();
        pAdmin.setId(id);
        OOperaInfo selectOOperaInfo = iOOperaInfoService.selectById(id);
        //判断密码
        if (!ENCODER.matches(ypwd,selectOOperaInfo.getPwd()))
        {
            return R.newErrorMsg("原密码错误");
        }
        selectOOperaInfo.setPwd(ENCODER.encode(password));
        iOOperaInfoService.updateById(selectOOperaInfo);
        return R.newOK("修改成功",map.get("type"));
    }

     /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:44 2018/6/21
      * 运营商 主页面数据显示
     */
    @GetMapping("/info")
    @ResponseBody
    public R getIndexInfo(HttpServletRequest request)
    {
        //处理开始时间结束时间 dateTime:(1 今日 2 昨日 3 本周 4 本月)
        HashMap<String, Object> resultMap = DateUtil.publicMethodForTime(Integer.parseInt(request.getParameter("dateTime")));
        String startDate =  DateUtil.getDateHavehms((Date) resultMap.get("startDate"));
        String endDate =  DateUtil.getDateHavehms((Date) resultMap.get("endDate"));

         //入参
         Map<String, Object> paramMap = new HashMap<>();
        String oid ="";
        Object session_opera_id = get(request, "session_opera_id");
        if(session_opera_id instanceof String){
            oid =(String)session_opera_id;
        }
         paramMap.put("oid",oid);
         paramMap.put("startDate",startDate);
         paramMap.put("endDate",endDate);
         paramMap.put("isFund",null);//资金端不需要非授信消费其它端需要统计

        //消费者数量
         int consumerNum=iCUserInfoService.selectConsumercount(paramMap);

        //商户数量
        int merchantNum =  iMMerchantInfoService.selectCount(new EntityWrapper<MMerchantInfo>().gt("create_date",startDate).lt("create_date",endDate).eq("oid",oid));

        //交易金额
        BigDecimal tradeMoney=iTbTradingDetailService.selectTotalMoney(paramMap);

        //已还款 （用户分期的  +　用户未分期）
        BigDecimal payBackMoney= iTbRepaymentDetailService.findAlreadyPayBackPlan(paramMap);

        //待还款
        BigDecimal waitPayplan =  iTbRepaymentDetailService.findWaitPay(paramMap);

        //已结算
        BigDecimal SettledMoney= iTbSettlementDetailService.findPAlreadySettled(paramMap);

        //待结算
        BigDecimal pWaitSettledByDateTime = iTbSettlementDetailService.findWaitSettled(paramMap);

        //数据总和
        //交易金额 已还款 已结算 预期 待还款 待结算
        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("oid",oid);
        totalMap.put("isFund",null);//资金端不需要非授信消费其它端需要统计
        Map<String,Object> pMap=iTbTradingDetailService.findPtotalData(totalMap);
        BigDecimal pTradeMoney=iTbTradingDetailService.selectTotalMoney(totalMap);//交易金额
        BigDecimal pPayback= (BigDecimal) pMap.get("pPayback");//已还款
        BigDecimal pAlreadySettled= (BigDecimal) pMap.get("pAlreadySettled");//已结算
        BigDecimal pWaitSettled= (BigDecimal) pMap.get("pWaitSettled");//已结算
        BigDecimal overDue= (BigDecimal) pMap.get("overDue");//逾期
        BigDecimal waitPay= (BigDecimal) pMap.get("waitPay");//待还款

        // 七天的交易记录
        Map<String, Object> omap = new HashMap<>();
        omap.put("oid",oid);
        omap.put("isFund",null);//资金端不需要非授信消费其它端需要统计
        List<Map<String,Object>> listMap =iTbTradingDetailService.findsevendaytrade(omap);

        //处理成数组
        ArrayList<String> timeList = new ArrayList<>();
        ArrayList<BigDecimal> tMoneyList = new ArrayList<>();
        dealArr(listMap, timeList, tMoneyList);

        String[] tm = new String[timeList.size()];
        BigDecimal[] tMon = new BigDecimal[tMoneyList.size()];
        String[] dtimeArr = timeList.toArray(tm);
        BigDecimal[] tMoneyArr = tMoneyList.toArray(tMon);

        //返回值
        R r=  new R<>();
        HashMap<Object, Object> mmp = new HashMap<>();
        mmp.put("dTimeArr",dtimeArr);//时间
        mmp.put("tMoneyArr",tMoneyArr);//资金
        mmp.put("consumerNum",consumerNum);//消费者
        mmp.put("merchantNum",merchantNum);//商户
        mmp.put("tradeMoney",tradeMoney);//交易金额
        mmp.put("payBackMoney",payBackMoney);//已还款
        mmp.put("waitPayplan",waitPayplan);//待还款
        mmp.put("SettledMoney",SettledMoney);//已结算
        mmp.put("pWaitSettledByDateTime",pWaitSettledByDateTime);//待结算

        //数据总和
        mmp.put("pTradeMoney",pTradeMoney);//交易金额
        mmp.put("pPayback",pPayback);//已还款
        mmp.put("pAlreadySettled",pAlreadySettled);//已结算
        mmp.put("pWaitSettled",pWaitSettled);//待结算
        mmp.put("overDue",overDue);//逾期
        mmp.put("waitPay",waitPay);//待还


        r.setData(mmp);
        return r;
    }

    //处理数组公共方法
    static void dealArr(List<Map<String, Object>> listMap, ArrayList<String> timeList, ArrayList<BigDecimal> tMoneyList) {
        for (Map<String,Object> map:listMap) {
            String dtime = (String) map.get("dateTime");
            String substring = dtime.substring(5);
            BigDecimal tMoney = (BigDecimal) map.get("totalMoney");
            timeList.add(substring);
            tMoneyList.add(tMoney);
        }
    }


}
