package com.tcxf.hbc.admin.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/platfrom/p_index")
public class PIndexInfoController extends BaseController {

    @Autowired
    protected ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    protected ICUserInfoService iCUserInfoService;

    @Autowired
    protected IMMerchantInfoService iMMerchantInfoService;

    @Autowired
    protected IOOperaInfoService iOOperaInfoService;

    @Autowired
    protected ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    protected ITbSettlementDetailService iTbSettlementDetailService;
    @Autowired
    private IPAdminService ipAdminService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:50 2018/6/21
     * 跳转平台首页
    */
    @GetMapping("/test")
    public ModelAndView test()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("platfrom/layout/center");
        return mode;
    }

    /**
     * 跳转修改密码页面
     */
    @GetMapping(value="/goToupdatepwd")
    public ModelAndView goToupdatepwd(HttpServletRequest request)
    {
        String type = request.getParameter("type");
        String pwd = request.getParameter("pwd");
        ModelAndView mode= new ModelAndView();
        mode.addObject("type",type);
        mode.addObject("pwd",pwd);
        mode.setViewName("platfrom/layout/updatePassword");
        return mode;
    }

    /**
     * 修改密码
     */
    @RequestMapping("/updatePassWord")
    public R updatePassWord(HttpServletRequest request,@RequestBody String json)
    {
        Map<String,String> map = JsonTools.parseJSON2Map(json);
        String ypwd = map.get("ypwd");
        String password = map.get("password");
        String id = (String) get(request,"session_admin_id");
        PAdmin pAdmin = new PAdmin();
        pAdmin.setId(id);
        PAdmin selectpAdmin = ipAdminService.selectById(id);
        //判断密码
        if (!ENCODER.matches(ypwd,selectpAdmin.getPwd()))
        {
            return R.newErrorMsg("原密码错误");
        }
        selectpAdmin.setPwd(ENCODER.encode(password));
        ipAdminService.updateById(selectpAdmin);
        return R.newOK("修改成功",map.get("type"));
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 13:55 2018/6/5
     */
    @GetMapping("/info")
    @ResponseBody
    public R getIndexInfo(HttpServletRequest request)
    {

        //1 今日 2 昨日 3 本周 4 本月
       String time = request.getParameter("dateTime")==null ? "1" :  request.getParameter("dateTime");
        //String time ="2";
        Integer dateTime = Integer.parseInt(time);

        //处理开始时间结束时间
        HashMap<String, Object> resultMap = DateUtil.publicMethodForTime(dateTime);
        String startDate =  DateUtil.getDateHavehms((Date) resultMap.get("startDate"));
        String endDate =  DateUtil.getDateHavehms((Date) resultMap.get("endDate"));

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("startDate",startDate);
        paramMap.put("endDate",endDate);
        paramMap.put("isFund",null);//资金端不需要非授信消费其它端需要统计

        //消费者
        int consumerNum = iCUserInfoService.selectCount(new EntityWrapper<CUserInfo>().gt("create_date",startDate).lt("create_date",endDate));

        //商户
        int merchantNum = iMMerchantInfoService.selectCount(new EntityWrapper<MMerchantInfo>().gt("create_date",startDate).lt("create_date",endDate));

        //运营商
        int operaNum = iOOperaInfoService.selectCount(new EntityWrapper<OOperaInfo>().gt("create_date",startDate).lt("create_date",endDate));

        //交易金额
        BigDecimal tradeMoney=iTbTradingDetailService.selectTotalMoney(paramMap);

        //已还款 （用户分期的  +　用户未分期） + 提前还款
        BigDecimal payBackMoney= iTbRepaymentDetailService.findAlreadyPayBackPlan(paramMap);//用户分期 用户未分期



        //已结算
        BigDecimal SettledMoney= iTbSettlementDetailService.findPAlreadySettled(paramMap);

        //数据总和
        /*交易金额 已还款 已结算 预期 待还款 待结算 */
        Map<String, Object> totalMap = new HashMap<>();
        Map<String,Object> pMap=iTbTradingDetailService.findPtotalData(totalMap);
        pMap.put("isFund",null);
        BigDecimal pTradeMoney=iTbTradingDetailService.selectTotalMoney(totalMap);//交易金额
        BigDecimal pPayback= (BigDecimal) pMap.get("pPayback");//已还款
        BigDecimal pAlreadySettled= (BigDecimal) pMap.get("pAlreadySettled");//已结算
        BigDecimal pWaitSettled= (BigDecimal) pMap.get("pWaitSettled");//待结算
        BigDecimal overDue= (BigDecimal) pMap.get("overDue");//逾期
        BigDecimal waitPay= (BigDecimal) pMap.get("waitPay");//待还款


        // 七天的交易记录
        Integer oid =null;
        Map<String, Object> omap = new HashMap<>();
        if(request.getParameter("oid")!=null){
            oid =Integer.parseInt(request.getParameter("oid")) ;
        }
        omap.put("oid",oid);
        omap.put("isFund",null);
        List<Map<String,Object>> listMap =iTbTradingDetailService.findsevendaytrade(omap);

        //处理成数组
        ArrayList<String> timeList = new ArrayList<>();
        ArrayList<BigDecimal> tMoneyList = new ArrayList<>();
        OIndexInfoController.dealArr(listMap, timeList, tMoneyList);

        String[] tm = new String[timeList.size()];
        BigDecimal[] tMon = new BigDecimal[tMoneyList.size()];
        String[] dtimeArr = timeList.toArray(tm);
        BigDecimal[] tMoneyArr = tMoneyList.toArray(tMon);

        //返回值
        R r=  new R<>();
        HashMap<Object, Object> mmp = new HashMap<>();
        mmp.put("dTimeArr",dtimeArr);//时间
        mmp.put("tMoneyArr",tMoneyArr);//资金
        mmp.put("consumerNum",consumerNum);//消费者
        mmp.put("merchantNum",merchantNum);//商户
        mmp.put("operaNum",operaNum);//运营商
        mmp.put("tradeMoney",tradeMoney);//交易金额
        mmp.put("payBackMoney",payBackMoney);//已还款
        mmp.put("SettledMoney",SettledMoney);//已结算

        //数据总和
        mmp.put("pTradeMoney",pTradeMoney);//交易金额
        mmp.put("pPayback",pPayback);//已还款
        mmp.put("pAlreadySettled",pAlreadySettled);//已结算
        mmp.put("overDue",overDue);//逾期
        mmp.put("waitPay",waitPay);//待还
        mmp.put("pWaitSettled",pWaitSettled);//待结算

        r.setData(mmp);
        return r;
    }



}
