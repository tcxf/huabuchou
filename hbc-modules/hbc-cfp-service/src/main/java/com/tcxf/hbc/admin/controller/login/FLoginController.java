package com.tcxf.hbc.admin.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IFFundendService;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.secure.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * liaozeyong
 * 2018年6月28日14:09:17
 * 资金端--登陆
 */
@RestController
@RequestMapping("/fund/flogin")
public class FLoginController extends BaseController {

    @Autowired
    private IPAdminService ipAdminService;
    @Autowired
    private ITbRoleService iTbRoleService;
    @Autowired
    private IFFundendService ifFundendService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    /**
     * 资金端登陆
     * liaozeyong
     * 2018年6月5日15:14:44
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public R require(HttpServletRequest request, FFundend fFundend)
    {
        //定义一个传值的类
        R r = new R();
        FFundend fundend = new FFundend();
        fundend.setAccount(fFundend.getAccount());
        //验证平台端账号存在不存在
        FFundend f = ifFundendService.selectOne(new EntityWrapper<FFundend>().eq("account",fFundend.getAccount()));
        if (f != null)
        {
            //加密
            boolean b = ENCODER.matches(fFundend.getPwd(),f.getPwd());
            //判断密码
            if (b == false)
            {
                r.setMsg("用户名密码错误");
                return r;
            }
            //判断status为启用 ,1为启用，0为不启用
            else if (!"1".equals(f.getStatus()))
            {
                r.setMsg("管理用户已被禁用");
            }
            else {
                Map<String , Object> map = new HashMap<>();
                map.put("id",f.getId());
                map.put("type",4);
                r.setMsg("登陆成功");
                //request.getSession().setAttribute("session_fund_id", f.getId());
                //request.getSession().setAttribute("session_fund_name", f);
                List<TbRole> resourcesSet = iTbRoleService.selectRole(map);
                //request.getSession().setAttribute("session_authority_fund", resourcesSet);
                save("session_fund_id","session_fund_id"+f.getId(),f.getId());
                save("session_fund_name","session_fund_name"+f.getId(),f);
                save("session_authority_fund","session_authority_fund"+f.getId(),resourcesSet);
                if(fFundend.getPwd().equals(fFundend.getAccount().substring(fFundend.getAccount().length()-6,fFundend.getAccount().length()))){
                    r.setData("true");
                }
                r.setCode(1);
            }
        }else {
                Map<String,Object> umap = new HashMap<>();
                umap.put("user_name",fFundend.getAccount());
                umap.put("ui_type",3);
                PAdmin admin = ipAdminService.selectOne(new EntityWrapper<PAdmin>().allEq(umap));
                if(ValidateUtil.isEmpty(admin) ||!ENCODER.matches(fFundend.getPwd(),admin.getPwd())){
                    r.setMsg("账号或密码错误");
                }else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", admin.getId());
                    map.put("type", 4);
                    r.setMsg("登陆成功");
                    FFundend ff = ifFundendService.selectById(admin.getAid());
                    //获取角色
                    List<TbRole> resourcesSet = iTbRoleService.selectRole(map);
                    //request.getSession().setAttribute("session_authority_opera", resourcesSet);
                    save("session_fund_id", "session_fund_id" + ff.getId(), ff.getId());
                    save("session_fund_name", "session_fund_name" + ff.getId(), ff);
                    save("session_authority_fund", "session_authority_fund" + ff.getId(), resourcesSet);
                    r.setCode(1);
                }
        }
        return r;
    }

    @RequestMapping("/gologin")
    public ModelAndView login()
    {
        return new ModelAndView("fund/login");
    }

    /**
     * 退出登陆
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        //清空session
        //request.getSession().removeAttribute("session_fund_id");
        //request.getSession().removeAttribute("session_fund_name");
        //request.getSession().removeAttribute("session_authority_fund");
        del(request,"session_fund_id");
        del(request,"session_fund_name");
        del(request,"session_authority_fund");
        return new ModelAndView("fund/login");
    }

}
