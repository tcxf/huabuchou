package com.tcxf.hbc.admin.controller.login;

import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 资金端  --权限获取
 * liaozeyong
 * 2018年6月30日10:33:17
 */
@RestController
@RequestMapping("/fund/fpage")
public class FPageController extends BaseController {

    @Autowired
    private ITbResourcesService iTbResourcesService;

    /**
     * 登陆后获取权限
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request){
        //从session获取值
        FFundend fundend = (FFundend) get(request,"session_fund_name");
        //request.getSession().getAttribute("session_fund_name");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginAdmin",fundend);
        //放入菜单
        List<TbResources> menuResources = new ArrayList<TbResources>();
        //放入资源
        Map<String, List<TbResources>> function = new HashMap<String, List<TbResources>>();
        //获取登陆用户的角色
        List<TbRole> tbRoles = (List<TbRole>) get(request,"session_authority_fund");
        //获取角色资源
        List<TbResources> tbResources = new ArrayList<TbResources>();
        for (TbRole tbRole : tbRoles) {
            //连表查询所有菜单栏,根据type值区分默认的resources
            tbResources = iTbResourcesService.selectResources(tbRole.getId(),3);
            for (TbResources resources : tbResources)
            {
                //判断为2说明为菜单，1为资源
                if ("2".equals(resources.getResourceType()))
                {
                    menuResources.add(resources);
                } else {
                    List<TbResources> l;
                    //判断为不为模块，没有parentId说明是模块
                    if (resources.getParentId() != null)
                    {
                        l =  function.get(resources.getParentId());
                        if(null == l)
                        {
                            l = new ArrayList<TbResources>();
                        }
                        l.add(resources);
                        function.put(resources.getParentId().toString(),l);
                    }
                }
            }
        }
        modelAndView.addObject("functions", function);
        modelAndView.addObject("tbResources",tbResources);
        modelAndView.addObject("menuResources", menuResources);
        modelAndView.setViewName("fund/layout/index");
        return modelAndView;
    }

    /**
     * 返回主页
     * @return
     */
    @RequestMapping("/createCenterHtml")
    public ModelAndView createCenterHtml() {
        return new ModelAndView("fund/layout/center");
    }
}
