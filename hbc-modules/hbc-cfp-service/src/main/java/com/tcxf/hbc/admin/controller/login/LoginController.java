package com.tcxf.hbc.admin.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.roleDto;
import com.tcxf.hbc.admin.model.vo.PAdminvo;
import com.tcxf.hbc.admin.service.ICUserInfoService;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.secure.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author zhouyj
 * @date 2018/3/10
 * 平台端登陆
 */
@RestController
@RequestMapping("/platfrom/platfrom")
public class LoginController extends BaseController {

    /**
     * 认证页面
     * @return ModelAndView
     */

    @Autowired
    private IPAdminService ipAdminService;
    @Autowired
    private ITbRoleService iTbRoleService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    /**
     * 平台端登陆
     * liaozeyong
     * 2018年6月5日15:14:44
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public R require(HttpServletRequest request, PAdminvo pAdminvo)
    {
        //定义一个传值的类
        R r = new R();
        PAdmin admin = new PAdmin();
        admin.setUserName(pAdminvo.getUserName());
        //md5加密
        MD5Util md5Util = new MD5Util();

        //验证平台端账号存在不存在
        Map<String,Object> umap = new HashMap<>();
        umap.put("user_name",pAdminvo.getUserName());
        umap.put("ui_type",1);
        PAdmin pAdmin = ipAdminService.selectOne(new EntityWrapper<PAdmin>().allEq(umap));
        if (pAdmin != null)
        {
            //加密
            boolean b = ENCODER.matches(pAdminvo.getPwd(),pAdmin.getPwd());
            //判断 密码
            if (b == false)
            {
                r.setMsg("用户名密码错误");
                return r;
            }
            //判断status为启用,1为启用，0为不启用
            else if (!"0".equals(pAdmin.getStatus()))
            {
                r.setMsg("管理用户已被禁用");
            }
            else {
                Map<String , Object> map = new HashMap<>();
                map.put("id",pAdmin.getId());
                map.put("type",3);
                r.setMsg("登陆成功");
               // request.getSession().setAttribute("session_admin_id", pAdmin.getId());
                //request.getSession().setAttribute("session_admin_name", pAdmin);
                List<TbRole> resourcesSet = iTbRoleService.selectRole(map);
                //request.getSession().setAttribute("session_authority_admin", resourcesSet);
                save("session_admin_id","session_admin_id"+ pAdmin.getId(), pAdmin.getId());
                save("session_admin_name","session_admin_name"+pAdmin.getId(),pAdmin);
                save("session_authority_admin","session_authority_admin"+pAdmin.getId(),resourcesSet);
                if(pAdminvo.getPwd().equals("123456")){
                    r.setData("true");
                }
                r.setCode(1);
            }
        }else {
            r.setMsg("账号不存在");
        }
        return r;
    }

    @Autowired
    protected ICUserInfoService icUserInfoService;

    @GetMapping("/gologin")
    public ModelAndView login()
    {
        return new ModelAndView("platfrom/login");
    }

    /**
     * 退出登陆
     * liaozeyong
     * @param request
     */
    @RequestMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        //清空session
        //request.getSession().removeAttribute("session_admin_id");
        //request.getSession().removeAttribute("session_admin_name");
        //request.getSession().removeAttribute("session_authority_admin");
        del(request,"session_admin_id");
        del(request,"session_admin_name");
        del(request,"session_authority_admin");
        return new ModelAndView("platfrom/login");
    }
    /**
     * 认证页面
     * @return ModelAndView
     */
    @GetMapping("/about")
    public ModelAndView aboutus()
    {
        this.getUserInfo();
        ModelAndView modelAndView=new ModelAndView("ftl/AboutUs");
        return modelAndView;
    }

    public void getUserInfo()
    {
        CUserInfo userInfo1 = icUserInfoService.selectById("1000");
        System.out.println("userInfo1============" + userInfo1);
    }
}