package com.tcxf.hbc.admin.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.secure.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * liaozeyong
 * 2018年6月5日13:43:42
 * 运营商端--登陆
 */
@RestController
@RequestMapping("/opera/opera")
public class OLoginController extends BaseController {

    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    @Autowired
    private ITbRoleService iTbRoleService;

    @Autowired
    private IPAdminService ipAdminService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 退出登陆
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        //清空session
        del(request,"session_opera_id");
        del(request,"session_opera_name");
        del(request,"session_authority_opera");

        return new ModelAndView("manager/login");
    }
    @RequestMapping("/gologin")
    public ModelAndView gologin(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("manager/login");
        return modelAndView;
    }

    /**
     * 运营商登陆
     * liaozeyong
     * 2018年6月5日15:14:44
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public R require(HttpServletRequest request,OOperaInfo oOperaInfo)
    {
        //定义一个传值的类
        R r = new R();
        //验证账号存在不存在
        OOperaInfo operaInfo = ioOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("mobile",oOperaInfo.getMobile()));
        //md5加密
        if (operaInfo != null)
        {
            boolean b = ENCODER.matches(oOperaInfo.getPwd(),operaInfo.getPwd());
            //判断密码
            if (b == false)
            {
                r.setMsg("用户名密码错误");
                return r;
            }
            //判断status为启用,1为启用，0为未开通，2为禁用
            else if (!"1".equals(operaInfo.getStatus()))
            {
                r.setMsg("管理用户已被禁用");
            }
            else {
                Map<String , Object> map = new HashMap<>();
                map.put("id",operaInfo.getId());
                map.put("type",2);
                r.setMsg("登陆成功");
                //request.getSession().setAttribute("session_opera_id", operaInfo.getId());
                //request.getSession().setAttribute("session_opera_name", operaInfo);
                //获取角色
                List<TbRole> resourcesSet = iTbRoleService.selectRole(map);
                //request.getSession().setAttribute("session_authority_opera", resourcesSet);
                save("session_opera_id","session_opera_id"+ operaInfo.getId(), operaInfo.getId());
                save("session_opera_name","session_opera_name"+operaInfo.getId(),operaInfo);
                save("session_authority_opera","session_authority_opera"+operaInfo.getId(),resourcesSet);
                if(oOperaInfo.getPwd().equals(oOperaInfo.getMobile().substring(oOperaInfo.getMobile().length()-6,oOperaInfo.getMobile().length()))){
                    r.setData("true");
                }
                r.setCode(1);
            }
        }
        else{
            Map<String,Object> umap = new HashMap<>();
            umap.put("user_name",oOperaInfo.getMobile());
            umap.put("ui_type",2);
            PAdmin admin = ipAdminService.selectOne(new EntityWrapper<PAdmin>().allEq(umap));
            if(ValidateUtil.isEmpty(admin) ||!ENCODER.matches(oOperaInfo.getPwd(),admin.getPwd())){
                r.setMsg("账号或密码错误");
            }else {
                Map<String, Object> map = new HashMap<>();
                map.put("id", admin.getId());
                map.put("type", 2);
                r.setMsg("登陆成功");
                OOperaInfo operaInfo1 = ioOperaInfoService.selectById(admin.getAid());
                //获取角色
                List<TbRole> resourcesSet = iTbRoleService.selectRole(map);
                //request.getSession().setAttribute("session_authority_opera", resourcesSet);
                save("session_opera_id", "session_opera_id" + operaInfo1.getId(), operaInfo1.getId());
                save("session_opera_name", "session_opera_name" + operaInfo1.getId(), operaInfo1);
                save("session_authority_opera", "session_authority_opera" + operaInfo1.getId(), resourcesSet);
                r.setCode(1);
            }

        }
        return r;
    }

}
