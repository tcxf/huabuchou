package com.tcxf.hbc.admin.controller.login;

import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运营商端    权限
 * liaozeyong
 * 2018年6月30日10:34:10
 */
@RestController
@RequestMapping("/opera/oPage")
public class OPageController extends BaseController {

    @Autowired
    private ITbResourcesService iTbResourcesService;

    @RequestMapping("/test")
    public ModelAndView test(){
        return new ModelAndView("manager/login");
    }
    /**
     * 登陆后获取权限
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request){
        //从session获取值
        OOperaInfo operaInfo = (OOperaInfo) get(request,"session_opera_name");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginAdmin",operaInfo);
        //放入菜单
        List<TbResources> menuResources = new ArrayList<TbResources>();
        //放入资源
        Map<String, List<TbResources>> function = new HashMap<String, List<TbResources>>();
        //获取登陆用户的角色
        List<TbRole> tbRoles = (List<TbRole>)get(request,"session_authority_opera");
        //获取角色资源
        List<TbResources> tbResources = new ArrayList<TbResources>();
        for (TbRole tbRole : tbRoles) {
            //连表查询所有菜单栏
            tbResources = iTbResourcesService.selectResources(tbRole.getId(),2);
            for (TbResources resources : tbResources)
            {
                //判断为2说明为菜单，1为资源
                if ("2".equals(resources.getResourceType()))
                {
                    menuResources.add(resources);
                } else {
                    List<TbResources> l;
                    //判断为不为模块，没有parentId说明是模块
                    if (resources.getParentId() != null)
                    {
                        l =  function.get(resources.getParentId());
                        if(null == l)
                        {
                            l = new ArrayList<TbResources>();
                        }
                        l.add(resources);
                        function.put(resources.getParentId().toString(),l);
                    }
                }
            }
        }
        modelAndView.addObject("functions", function);
        modelAndView.addObject("tbResources",tbResources);
        modelAndView.addObject("menuResources", menuResources);
        modelAndView.setViewName("manager/layout/index");
        return modelAndView;
    }

    /**
     * 返回主 页
     * @return
     */
    @RequestMapping("/createCenterHtml")
    public ModelAndView createCenterHtml() {

        return new ModelAndView("manager/layout/center");
    }
}
