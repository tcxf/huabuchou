package com.tcxf.hbc.admin.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.tcxf.hbc.admin.service.ITbAreaService;
import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * liaozeyong
 * 2018年6月13日13:55:26
 * 平台--登陆后跳转主界面的所有操作
 */
@RestController
@RequestMapping("/platfrom/page")
public class PageController extends BaseController {

    @Autowired
    private ITbResourcesService iTbResourcesService;
    @Autowired
    private IPAdminService ipAdminService;
    @Autowired
    private ITbAreaService iTbAreaService;

    /**
     * 登陆后获取权限
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request){
        PAdmin loginAdmin = (PAdmin) get(request,"session_admin_name");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginAdmin", loginAdmin);

        //放入菜单
        List<TbResources> menuResources = new ArrayList<TbResources>();
        //放入资源
        Map<String, List<TbResources>> function = new HashMap<String, List<TbResources>>();
        //获取登陆用户的角色
        List<TbRole> tbRoles = (List<TbRole>) get(request,"session_authority_admin");
        //获取角色资源
        List<TbResources> tbResources = new ArrayList<TbResources>();
        for (TbRole tbRole : tbRoles) {
            //long强转为String
            String id = String.valueOf(tbRole.getId());
            tbResources = iTbResourcesService.selectResources(id,1);//连表查询所有菜单栏
            for (TbResources resources : tbResources)
            {
                //判断为2说明为菜单，1为资源
                if ("2".equals(resources.getResourceType()))
                {
                    menuResources.add(resources);
                } else {
                    List<TbResources> l;
                    //判断为不为模块，没有parentId说明是模块
                    if (resources.getParentId() != null)
                    {
                        l =  function.get(resources.getParentId());
                        if(null == l)
                        {
                            l = new ArrayList<TbResources>();
                        }
                        l.add(resources);
                        function.put(resources.getParentId().toString(),l);
                    }
                }
            }
        }
        modelAndView.addObject("functions", function);
        modelAndView.addObject("tbResources",tbResources);
        modelAndView.addObject("menuResources", menuResources);
//        modelAndView.addObject("list",list);
        modelAndView.setViewName("platfrom/layout/index");
        return modelAndView;
    }

    @RequestMapping("/createCenterHtml")
    public ModelAndView createCenterHtml() {
        return new ModelAndView("platfrom/layout/center");
    }

    /**
     * 根据id获取地区
     * @param id
     * @return
     */
    @RequestMapping("/loadArea")
    public R loadArea(String id){
        R r = new R();
        TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id",id));
        r.setData(tbArea);
        return r;
    }

}
