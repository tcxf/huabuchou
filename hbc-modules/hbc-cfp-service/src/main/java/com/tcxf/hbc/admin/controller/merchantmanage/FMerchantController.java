package com.tcxf.hbc.admin.controller.merchantmanage;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.model.dto.MMerchantInfoDto;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.vo.MerchantlnfoVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
@RestController
@RequestMapping("/fund/f_merchant")
public class FMerchantController {

        private IMMerchantCategoryService iMMerchantCategoryService;

        @Autowired
        private IMMerchantInfoService iMMerchantInfoService;

        @Autowired
        private ITbAreaService iTbAreaService;

        @Autowired
        private IMMerchantOtherInfoService imMerchantOtherInfoService;

        @Autowired
        private IOOperaInfoService ioOperaInfoService;

        @Autowired
        private ITbBindCardInfoService iTbBindCardInfoService;

        @Autowired
        private ITbTradingDetailService iTbTradingDetailService;

        @Autowired
        private ITbSettlementDetailService iTbSettlementDetailService;

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 14:06 2018/6/14
         * 跳转商户行业分类页面
         */
        @GetMapping("/jumpMerchantCate")
        public ModelAndView jumpMerchantCate()
        {
            ModelAndView mode= new ModelAndView();
            mode.setViewName("fund/merchantCategoryManager");
            return mode;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 16:48 2018/6/14
         * 跳转新增商家分类页面
         */
        @GetMapping("/jumpMerchantCategoryAdd")
        public ModelAndView jumpMerchantCategoryAdd()
        {
            ModelAndView mode= new ModelAndView();
            mode.setViewName("fund/merchantCategoryHandler");
            return mode;
        }


        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 14:09 2018/6/14
         * 查询商户行业 分页信息
         */
        @RequestMapping("/pageInfo")
        public R findMerchantCatePageInfo(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request); //(new Query<MMerchantCategory>(paramMap))
            Integer page =Integer.parseInt((String) paramMap.get("page")) ;
            Integer rows =Integer.parseInt((String) paramMap.get("limit")) ;
            Page pageInfo= iMMerchantCategoryService.selectPage(new Page<MMerchantCategory>(page,rows), new EntityWrapper<MMerchantCategory>().like("name",paramMap.get("name").toString()).orderBy("order_list"));
            R r = new R<>();
            r.setData(pageInfo);
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 10:54 2018/6/19
         * 跳转商户管理新增页面
         */
        @GetMapping("/jumpMerchantInfoAdd")
        public ModelAndView jumpMerchantInfoAdd()
        {
            ModelAndView mode= new ModelAndView();
            mode.setViewName("fund/merchantInfoAdd");
            return mode;
        }


        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 20:03 2018/6/14
         * 添加或者修改商家分类
         * */
        @RequestMapping("/insertOrUpdate")
        public R insertOrUpdateMerchantCate(MMerchantCategory mMerchantCategory)
        {
            R r = new R<>();
            List<MMerchantCategory> list = iMMerchantCategoryService.selectList(new EntityWrapper<MMerchantCategory>().ne("id", mMerchantCategory.getId()));
            //校验名称
            for (MMerchantCategory mchant:list) {
                if(mMerchantCategory.getName().equals(mchant.getName())){
                    r.setCode(R.FAIL);
                    r.setMsg("商户类型名已存在！");
                    return r;
                }
            }
            //修改
            if(!"".equals(mMerchantCategory.getId()) && mMerchantCategory.getId()!=null){
                mMerchantCategory.setModifyDate(new Date());
                boolean update = iMMerchantCategoryService.update(mMerchantCategory, new EntityWrapper<MMerchantCategory>().eq("id",mMerchantCategory.getId()));
                r.setMsg("修改成功！");
                r.setCode(R.SUCCESS);
            }else{//添加
                mMerchantCategory.setCreateDate(new Date());
                boolean insert = iMMerchantCategoryService.insert(mMerchantCategory);
                r.setMsg("添加成功！");
                r.setCode(R.SUCCESS);
            }
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 20:21 2018/6/14
         * 跳转编辑商家分类
         */
        @RequestMapping("/jumpMerchantCategoryEdit")
        public ModelAndView insertMerchantCate(String id)
        {
            ModelAndView modelAndView = new ModelAndView();
            MMerchantCategory mMerchantCategory = iMMerchantCategoryService.selectOne(new EntityWrapper<MMerchantCategory>().eq("id", id));
            modelAndView.addObject("mMerchantCategory",mMerchantCategory);
            modelAndView.setViewName("fund/merchantCategoryEdit");
            return  modelAndView;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 11:12 2018/6/15
         * 删除商家分类
         */
        @RequestMapping("/deleteMerchantCategory")
        public R deleteMerchantCategory(MMerchantCategory mMerchantCategory)
        {
            boolean delete = iMMerchantCategoryService.delete(new EntityWrapper<MMerchantCategory>().eq("id", mMerchantCategory.getId()));
            R r = new R<>();
            if (delete){
                r.setMsg("删除成功!");
            }
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 15:09 2018/6/15
         * 跳转商户管理分页页面
         */
        @GetMapping("/jumpMerchantManager")
        public ModelAndView jumpMerchantManager()
        {
            ModelAndView mode= new ModelAndView();
            mode.setViewName("fund/merchantInfoManager");
            return mode;
        }


        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 15:12 2018/6/15
         * 查询商户管理分页信息
         */
        @RequestMapping("/MerchantManagerPageInfo")
        public R findMerchantManagerPageInfo(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            Page pageInfo=  iMMerchantInfoService.findMerchantManagerPageInfo(new Query<MMerchantInfoDto>(paramMap));
            R r = new R<>();
            r.setData(pageInfo);
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 9:39 2018/6/19
         * 加载行业类型下拉选项
         */
        @RequestMapping("/loadIndustryType")
        public R findIndustryType()
        {
            List<MMerchantCategory> mMerchantInfos = iMMerchantCategoryService.selectList(new EntityWrapper<MMerchantCategory>());
            R r = new R<>();
            r.setData(mMerchantInfos);
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 10:43 2018/6/19
         * 加载省市区三级联动
         */
        @RequestMapping("/loadPcd")
        public R findPcdInfo(String parentId)
        {
            List<TbArea> tbAreas = iTbAreaService.selectList(new EntityWrapper<TbArea>().eq("parent_id",parentId));
            R r = new R<>();
            r.setData(tbAreas);
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 17:45 2018/7/3
         * 根据id查询path路径
         */
        @RequestMapping("/loadArea")
        public R findPcdInfoById(String id)
        {
            TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id",id));
            R r = new R<>();
            r.setData(tbArea);
            return  r;
        }



        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 14:17 2018/6/19
         * 新增商户
         */
        @RequestMapping("/addMerchant")
        public R insertMerchant(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            iMMerchantInfoService.addMerchantInfo(paramMap);
            R r = new R<>();
            r.setData("");
            return  r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 15:55 2018/7/3
         * 修改商户信息
         */
        @RequestMapping("/updateMerchantInfo")
        public R updateMerchantInfo(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            iMMerchantInfoService.updateMerchantInfo(paramMap);
            return  R.newOK();
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 19:54 2018/6/19
         * 加载商户其它信息
         */
        @RequestMapping("/merchantDetail")
        public ModelAndView findMerchantInfo(String id)
        {
            ModelAndView modelAndView = new ModelAndView();
            MMerchantInfo mMerchantInfo = iMMerchantInfoService.selectById(id);
            MMerchantOtherInfo mMerchantOtherInfo = imMerchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",id));
            modelAndView.addObject("mMerchantInfo",mMerchantInfo);
            modelAndView.addObject("mMerchantOtherInfo",mMerchantOtherInfo);
            modelAndView.addObject("id",id);
            modelAndView.setViewName("fund/merchantInfoDetail");
            return modelAndView;
        }

        /**
         * @Description
         * @Date: 19:53 2018/6/19
         * 加载卡信息
         */
        @RequestMapping("/loadBindCardInfo")
        public R findBindCardInfo(String id)
        {
            R r = new R<>();
            List<TbBindCardInfo> olist = iTbBindCardInfoService.selectList(new EntityWrapper<TbBindCardInfo>().eq("oid",id));
            r.setData(olist);
            return r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 20:31 2018/6/19
         * 加载交易记录分页
         */
        @RequestMapping("/tradingDetailManagerList")
        public R findTradingPageInfo(HttpServletRequest request)
        {
            Boolean flag=true;
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            R r = new R<>();
            Page pageInfo=  iTbTradingDetailService.findTradingPageInfo(new Query<TbTradingDetailDto>(paramMap));
            r.setData(pageInfo);
            return r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 21:09 2018/6/19
         * 加载交易总金额
         */
        @RequestMapping("/totalJymxAmount")
        public R findTradingtotalAmount(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            R r = new R<>();
            BigDecimal totalMoney=  iTbTradingDetailService.findTradingtotalAmount(paramMap);
            r.setData(totalMoney);
            return r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 21:28 2018/6/19
         * 导出交易记录poi报表
         */
        @RequestMapping("/tradingPoi")
        public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            Page pageInfo=  iTbTradingDetailService.findTradingPageInfo(new Query<TbTradingDetailDto>(paramMap));

            String[] title = {"编号","交易单号", "商户id","商户名称", "行业类型", "用户id", "用户名称","交易类型","交易时间","交易金额" ,"结算金额","交易总金额"};
            int i = 1;
            List<String[]> list = new LinkedList<String[]>();
            List<TbTradingDetailDto> listDto=pageInfo.getRecords();

            for (TbTradingDetailDto tbTradingDetailDto : listDto) {
                String[] strings = new String[11];
                strings[0] = i + "";
                strings[1] = tbTradingDetailDto.getSerialNo();
                strings[2] = tbTradingDetailDto.getMid();
                strings[3] = tbTradingDetailDto.getLegalName();
                strings[4] = tbTradingDetailDto.getNature();
                strings[5] = tbTradingDetailDto.getUid();
                strings[6] = tbTradingDetailDto.getRealName();
                strings[7] = tbTradingDetailDto.getTradingType();
                strings[8] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
                strings[9] = String.valueOf(tbTradingDetailDto.getTradeAmount());
                strings[10] = String.valueOf(tbTradingDetailDto.getActualAmount());
                list.add(strings);
                i++;
            }
            String[] totalString =new String[12];
            BigDecimal totalMoney=  iTbTradingDetailService.findTradingtotalAmount(paramMap);
            totalString[11] = String.valueOf(totalMoney);
            list.add(0,totalString);

            ExportUtil.createExcel(list, title, response, "交易记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 11:20 2018/6/20
         * 查询 结算明细
         */
        @RequestMapping("/clearDetail")
        public R findClearDetailPageInfo(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            R r = new R<>();
            Page pageInfo=  iTbSettlementDetailService.findClearDetailPageInfo(new Query<TbSettlementDetailDto>(paramMap));
            r.setData(pageInfo);
            return r;
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 11:37 2018/6/20
         * 待结算 金额 已结算金额
         */
        @RequestMapping("/clearMoney")
        public R findclearMoneyInfo(HttpServletRequest request)
        {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            R r = new R<>();
            Map mmp=  iTbSettlementDetailService.findclearMoneyInfo(paramMap);
            r.setData(mmp);
            return r;
        }


        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 17:40 2018/6/15
         * 查询商户今日新增 总数量
         */
        @RequestMapping("/todayAddMerchant")
        public R findMerchantNumber()
        {
            R r = new R<>();
            HashMap<String, Object> mmp = new HashMap<>();
            //商户数量
            int merchantNum = iMMerchantInfoService.selectCount(new EntityWrapper<MMerchantInfo>().gt("create_date",DateUtil.getTimesmorning()).lt("create_date",DateUtil.getTimesnight()));
            int totalNum = iMMerchantInfoService.selectCount(new EntityWrapper<MMerchantInfo>());
            //返回值
            mmp.put("merchantNum",merchantNum);//每日新增商户数量
            mmp.put("totalNum",totalNum);//商户总数量
            r.setData(mmp);
            return  r;
        }


        /**
         * 跳转修改商户页面
         * @param id
         * @return
         */
        @RequestMapping("/update")
        public ModelAndView update(String id)
        {
            ModelAndView mode = new ModelAndView();
            MMerchantInfo mMerchantInfo = iMMerchantInfoService.selectById(id);
            MMerchantOtherInfo mMerchantOtherInfo = imMerchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",id));
            List<MMerchantCategory> mlist = iMMerchantCategoryService.selectList(new EntityWrapper<MMerchantCategory>());
            List<OOperaInfo> olist = ioOperaInfoService.selectList(new EntityWrapper<OOperaInfo>());
            mode.addObject("mMerchantInfo",mMerchantInfo);
            mode.addObject("mMerchantOtherInfo",mMerchantOtherInfo);
            mode.addObject("mlist",mlist);
            mode.addObject("olist",olist);
            mode.setViewName("fund/merchantInfoHandler");
            return mode;

        }

        /**
         * 根据id修改商户
         * @param m
         * @return
         */
        @RequestMapping("updateById")
        public  R updateById(MerchantlnfoVo m)
        {
            R r = new R();
            //修改商户基本信息
            Boolean bo =  iMMerchantInfoService.updateById(m);
            MMerchantOtherInfo mm = imMerchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",m.getId()));
            mm.setNature(m.getNature());
            mm.setOperArea(m.getOperArea());
            mm.setOperYear(m.getOperYear());
            mm.setRegMoney(m.getRegMoney());
            mm.setPhoneNumber(m.getPhoneNumber());
            mm.setRecommendGoods(m.getRecommendGoods());
            mm.setLicensePic(m.getLicensePic());
            mm.setLegalPhoto(m.getLegalPhoto());
            mm.setNormalCard(m.getNormalCard());
            mm.setCreditCard(m.getCreditCard());
            mm.setLagalFacePhoto(m.getLagalFacePhoto());
            mm.setSettlementLoop(m.getSettlementLoop());
            mm.setShareFee(m.getShareFee());
            Boolean b = imMerchantOtherInfoService.updateById(mm);
            r.setMsg("修改成功");
            return  r;
        }

        /**
         *
         * 商户绑卡
         * @param
         * @return
         */
        @RequestMapping("/addCard")
        public R addCard(String id,String bankCard,String bankName,String bankSn,String idCard,String isDefault,String mobile)
        {
            R r = new R();
            TbBindCardInfo tb = new TbBindCardInfo();
            tb.setBankCard(bankCard);
            tb.setName(bankName);
            tb.setBankSn(bankSn);
            tb.setIdCard(idCard);
            tb.setIsDefault(isDefault);
            tb.setMobile(mobile);
            tb.setOid(id);
            if(iTbBindCardInfoService.insert(tb))
            {
                r.setMsg("添加成功");
            }else{
                r.setMsg("添加失败");
            }
            return  r;

        }

        /**
         * 根据商户id查询绑定的银行卡
         * @param id
         * @return
         */
        @RequestMapping("/findCard")
        public R findCard(String id)
        {
            R r = new R();
            List<TbBindCardInfo> tlist = iTbBindCardInfoService.selectList(new EntityWrapper<TbBindCardInfo>().eq("oid",id));
            r.setData(tlist);
            return r;
        }

        /**
         * 根据id删除
         * @param id
         * @return
         */
        @RequestMapping("/deleteCardById")
        public R deleteById(String id)
        {
            R r = new R();
            if(iTbBindCardInfoService.deleteById(id)){
                r.setMsg("删除成功");
            }else{
                r.setMsg("删除失败");
            }
            return  r;
        }

        /**
         *
         * 修改银行卡信息
         * @param id
         * @param bankCard
         * @param bankName
         * @param bankSn
         * @param idCard
         * @param isDefault
         * @param mobile
         * @return
         */
        @RequestMapping("/updateCardByid")
        public R updateCardByid(String id, String bankCard, String bankName, String bankSn, String idCard, String isDefault, String mobile)
        {
            R r = new R();
            TbBindCardInfo tb = new TbBindCardInfo();
            tb.setId(id);
            tb.setBankCard(bankCard);
            tb.setName(bankName);
            tb.setBankSn(bankSn);
            tb.setIdCard(idCard);
            tb.setIsDefault(isDefault);
            tb.setMobile(mobile);
            if(iTbBindCardInfoService.update(tb,new EntityWrapper<TbBindCardInfo>().eq("id",id)))
            {
                r.setMsg("修改成功");
            }else{
                r.setMsg("修改失败");
            }
            return  r;

        }

}
