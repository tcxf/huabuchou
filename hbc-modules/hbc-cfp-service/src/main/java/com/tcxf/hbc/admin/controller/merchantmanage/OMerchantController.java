package com.tcxf.hbc.admin.controller.merchantmanage;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.admin.common.pdf.PdfHandlerss;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.MMerchantDto;
import com.tcxf.hbc.admin.model.dto.TbBankDto;
import com.tcxf.hbc.admin.model.vo.MerchantlnfoVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.vo.CreditVo;
import org.bouncycastle.crypto.paddings.TBCPadding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/opera/merchantInfo")
public class OMerchantController extends BaseController {
    @Autowired
    public IMMerchantInfoService imMerchantInfoService;

    @Autowired
    public IMMerchantCategoryService imMerchantCategoryService;

    @Autowired
    public ITbBindCardInfoService iTbBindCardInfoService;

    @Autowired
    public ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    public IMMerchantOtherInfoService imMerchantOtherInfoService;

    @Autowired
    private ITbBusinessCreditChecklistService tbBusinessCreditChecklistService;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    @Autowired
    private  IOOperaOtherInfoService ioOperaOtherInfoService;

    @Autowired
    private IOOperaInfoService ioOperaInfoService;

    @Autowired
    public IFFundendService ifFundendService;

    @Autowired
    public ITbUserAccountTypeConService iTbUserAccountTypeConService;

    @Autowired
    private ThreelementController threelementController;

    @Autowired
    private ITbBusinessCreditService iTbBusinessCreditService;

    PdfHandlerss pdfHandlers=new PdfHandlerss();

    @Resource(name = "redisTemplate")
    private RedisTemplate redisTemplate;

    //密码加密
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 分页查询跳转界面
     *
     * @return
     */
    @RequestMapping("/qu")
    public ModelAndView queryAll(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String oid =  get(request,"session_opera_id").toString();
        List <MMerchantCategory> list = imMerchantCategoryService.selectList(new EntityWrapper <MMerchantCategory>());
        mode.addObject("list", list);
        mode.addObject("oid",oid);
        mode.setViewName("manager/merchantInfoManager");
        return mode;
    }



    /**
     * 分页查询所有商户
     *
     * @param request
     * @return
     */
    @RequestMapping("/merchantInfoManagerList")
    public R merchantInfoManagerList(HttpServletRequest request) {
        R r = new R <>();
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        String oid =  get(request,"session_opera_id").toString();
        paramMap.put("oid", oid);
        Page pageInfo = imMerchantInfoService.QueryAll(new Query <MMerchantDto>(paramMap));
        r.setData(pageInfo);
        return r;
    }

    /**
     * 查看商户详情
     *
     * @param request
     * @return
     */
    @RequestMapping("/forwardMerchantInfoDetail")
    public ModelAndView forwardMerchantInfoDetail(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MMerchantDto entity = imMerchantInfoService.QueryOne(id);
        System.out.println(entity);
        mode.addObject("entity", entity);
        mode.addObject("id", id);
        mode.setViewName("manager/merchantInfoDetail");
        return mode;
    }
    /**
     * @Description
     * @Date: 19:53 2018/6/19
     * 加载卡信息
     */
    @RequestMapping("/loadBindCardInfo")
    public R findBindCardInfo(MerchantlnfoVo m, HttpServletRequest reques)
    {
        R r = new R<>();
        String id = reques.getParameter("id");
        MMerchantInfo minfo = imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id", id));
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper <TbUserAccountTypeCon>().eq("con_id",minfo.getId()));
        List<TbBindCardInfo> olist = iTbBindCardInfoService.selectList(new EntityWrapper<TbBindCardInfo>().eq("oid",tbUserAccountTypeCon.getAcctId()));
        r.setData(olist);
        return r;
    }


    /**
     * 查看协议
     *
     * @return
     */
    @RequestMapping("/forwardMerchantInfoht")
    public ModelAndView forwardMerchantInfoht(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MMerchantInfo merchantInfo = imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id", id));
        MMerchantOtherInfo  merchantOtherInfo= imMerchantOtherInfoService.selectOne(new EntityWrapper <MMerchantOtherInfo>().eq("mid", id));
        OOperaInfo oOperaInfo = ioOperaInfoService.selectOne(new EntityWrapper <OOperaInfo>().eq("id", merchantInfo.getOid()));
        OOperaOtherInfo oOperaOtherInfo = ioOperaOtherInfoService.selectOne(new EntityWrapper <OOperaOtherInfo>().eq("oid", oOperaInfo.getId()));
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper <TbUserAccountTypeCon>().eq("con_id",merchantInfo.getId()));
        TbBindCardInfo tbBindCardInfom = iTbBindCardInfoService.bankm(tbUserAccountTypeCon.getAcctId());
        TbBindCardInfo tbBindCardInfoo = iTbBindCardInfoService.banko(oOperaInfo.getId());
        mode.addObject("merchantInfo",merchantInfo);
        mode.addObject("oOperaInfo",oOperaInfo);
        mode.addObject("oOperaOtherInfo",oOperaOtherInfo);
        mode.addObject("tbBindCardInfom",tbBindCardInfom);
        mode.addObject("tbBindCardInfoo",tbBindCardInfoo);
        mode.addObject("merchantOtherInfo",merchantOtherInfo);
        mode.setViewName("manager/hetong");
        return mode;
    }

    /**
     * 查看协议
     *
     * @return
     */
    @RequestMapping("/forwardMerchantInfoht1")
    public ModelAndView forwardMerchantInfoht1(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        MMerchantInfo merchantInfo = imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id", id));
        MMerchantOtherInfo  merchantOtherInfo= imMerchantOtherInfoService.selectOne(new EntityWrapper <MMerchantOtherInfo>().eq("mid", id));
        OOperaInfo oOperaInfo = ioOperaInfoService.selectOne(new EntityWrapper <OOperaInfo>().eq("id", merchantInfo.getOid()));
        OOperaOtherInfo oOperaOtherInfo = ioOperaOtherInfoService.selectOne(new EntityWrapper <OOperaOtherInfo>().eq("oid", oOperaInfo.getId()));
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper <TbUserAccountTypeCon>().eq("con_id",merchantInfo.getId()));
        TbBindCardInfo tbBindCardInfom = iTbBindCardInfoService.bankm(tbUserAccountTypeCon.getAcctId());
        TbBindCardInfo tbBindCardInfoo = iTbBindCardInfoService.banko(oOperaInfo.getId());
        mode.addObject("merchantInfo",merchantInfo);
        mode.addObject("oOperaInfo",oOperaInfo);
        mode.addObject("oOperaOtherInfo",oOperaOtherInfo);
        mode.addObject("tbBindCardInfom",tbBindCardInfom);
        mode.addObject("tbBindCardInfoo",tbBindCardInfoo);
        mode.addObject("merchantOtherInfo",merchantOtherInfo);
        mode.setViewName("hetong");
        return mode;
    }


    /**m
     * 商户资料审核
     *
     * @return
     */
    @RequestMapping("/showApplyMerchant")
    public ModelAndView showApply(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MMerchantDto entity = imMerchantInfoService.QueryOne(id);
        mode.addObject("id", id);
        mode.addObject("entity", entity);
        mode.setViewName("manager/showApplyMerchant");
        return mode;
    }

    /**
     * 商户资料审核
     *
     * @param request
     * @return
     */
    @RequestMapping("/doExamine")
    public R doExamine(HttpServletRequest request, MMerchantOtherInfo mMerchantOtherInfo) {
        ModelAndView mode = new ModelAndView();
        R r = new R();
        String id = request.getParameter("mid");
        String authExamine=request.getParameter("ret");
        String reason=request.getParameter("reason");
        String settlementLoop = request.getParameter("settlementLoop");
        String shareFee=request.getParameter("shareFee");
        String latestday=request.getParameter("latestday");
        String minmoney=request.getParameter("minmoney");
        BigDecimal b=new BigDecimal(shareFee);
        MMerchantOtherInfo mm = imMerchantOtherInfoService.selectOne(new EntityWrapper <MMerchantOtherInfo>().eq("mid", id));
         MMerchantInfo merchantInfo= imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id",id));
        if (mm.getMid() != null && authExamine.equals("2")) {
            mm.setAuthExamine("2");
            merchantInfo.setStatus("0");
            mm.setSettlementLoop(settlementLoop);
            mm.setExamineFailReason(reason);
            mm.setShareFee(b);
            merchantInfo.setExamineDate(new Date());
            mm.setOpenDate(DateUtil.getDateHavehms(new Date()));
            imMerchantOtherInfoService.updateById(mm);
            imMerchantInfoService.updateById(merchantInfo);
            r.setMsg("审核失败");
        } else {
            mm.setAuthExamine("3");
            merchantInfo.setStatus("1");
            mm.setShareFee(b);
            if(settlementLoop.equals("0")){
                mm.setSettlementLoop("8");
                mm.setMinmoney(new BigDecimal(minmoney));
                mm.setLatestday(latestday);
            }else {
                mm.setSettlementLoop(settlementLoop);
            }
            merchantInfo.setExamineDate(new Date());
            mm.setOpenDate(DateUtil.getDateHavehms(new Date()));
            imMerchantOtherInfoService.updateById(mm);
            imMerchantInfoService.updateById(merchantInfo);
            TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper <TbUserAccountTypeCon>().eq("con_id",merchantInfo.getId()));
            redisTemplate.delete(tbUserAccountTypeCon.getAcctId() + "_account_info");
            r.setMsg("审核成功");
        }
        return r;
    }

    /**
     * 修改商户跳转
     *
     * @param request
     * @return
     */
    @RequestMapping("/forwardMerchantInfoEdit")
    public ModelAndView forwardMerchantInfoEdit(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MMerchantDto entity = imMerchantInfoService.QueryOne(id);
        List <MMerchantCategory> list = imMerchantCategoryService.selectList(new EntityWrapper <MMerchantCategory>());
        mode.addObject("list", list);
      //  System.out.println(entity);
        mode.addObject("entity", entity);
        mode.addObject("id", id);
        mode.setViewName("manager/merchantInfoHandler");
        return mode;
    }

    /**
     * 根据id修改商户 (防表单重复提交)
     *
     * @param m
     * @return
     */
    @RequestMapping("updateById")
    public R updateById(MerchantlnfoVo m,HttpServletRequest request) {
        imMerchantInfoService.updateById(m,request);
        return  R.newOK();
    }


    /**
     * 查询运营商的出账日和还款日
     * @param request
     * @return
     */
    @RequestMapping("/ooaDate")
    public ModelAndView ooaDate(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        String oid = get(request,"session_opera_id").toString();
        OOperaOtherInfo oo = ioOperaOtherInfoService.selectOne(new EntityWrapper<OOperaOtherInfo>().eq("oid",oid));
        List<MMerchantInfo> mlist = imMerchantInfoService.selectList(new EntityWrapper<MMerchantInfo>().eq("oid",oid));
        String type = "0";
        if(mlist.size()!=0 ){
                type="1";
        }else if(oo.getType()==1){
            type="2";
        }
        mode.addObject("type",type);
        mode.addObject("oo",oo);
        mode.setViewName("manager/activity/ooaDate");
        return  mode;

    }

    /**
     * 修改运营商的出账日和还款日
     * @param request
     * @param oOperaOtherInfo
     * @return
     */
    @RequestMapping("updateOoaDate")
    public  R updateOoDate(HttpServletRequest request,OOperaOtherInfo oOperaOtherInfo){
        R r = new R();
        String oid = get(request,"session_opera_id").toString();
        OOperaOtherInfo oo = ioOperaOtherInfoService.selectOne(new EntityWrapper<OOperaOtherInfo>().eq("oid",oid));
        oo.setOoaDate(oOperaOtherInfo.getOoaDate());
        oo.setRepayDate(oOperaOtherInfo.getRepayDate());
        oo.setType(1);
        if(ioOperaOtherInfoService.updateById(oo)) {
            r.setMsg("修改成功");
        }else {
            r.setMsg("修改失败");
        }
        return  r;
    }

    /**
     * 授信资料审核
     *
     * @param request
     * @return
     */
    @RequestMapping("/showApplysh")
    public ModelAndView showApplysh(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MMerchantDto entity = imMerchantInfoService.QueryOne(id);
        mode.addObject("id", id);
        mode.addObject("entity", entity);
        mode.setViewName("manager/showmsy");
        return mode;
    }

    /**
     * 删除绑定运行商银行卡信息
     *
     * @param id
     * @return
     */
    @RequestMapping("/delBindCard")
    public R delBindCard(String id)
    {
        R r = new R();
        boolean b = iTbBindCardInfoService.deleteById(id);
        if(b)
        {
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 修改运行商端绑卡信息
     *
     * @return
     */
    @RequestMapping("/updateBindCard")
    public R updateBindCard(TbBankDto tbBankDto, HttpServletRequest request) {
        R r = new R();
        String id = request.getParameter("id");
        /**
         * 银行卡四要素验证
         */
        CreditVo creditVo = new CreditVo();
        creditVo.setMobile(tbBankDto.getMobile());
        creditVo.setAccountNO(tbBankDto.getBankCard());
        creditVo.setIdCard(tbBankDto.getIdCard());
        creditVo.setUserName(tbBankDto.getBankName());
        R r1 = threelementController.threelementCheck(creditVo);
        if (r1.getCode() == 1){
            r1.setMsg("请输入正确的银行卡信息");
            return r1;
        }
        TbBindCardInfo tbBindCardInfo = iTbBindCardInfoService.selectOne(new EntityWrapper <TbBindCardInfo>().eq("id", id));
        tbBindCardInfo.setBankSn(tbBankDto.getBankSn());
        tbBindCardInfo.setMobile(tbBankDto.getMobile());
        tbBindCardInfo.setName(tbBankDto.getBankName());
        tbBindCardInfo.setIdCard(tbBankDto.getIdCard());
        tbBindCardInfo.setBankCard(tbBankDto.getBankCard());
        tbBindCardInfo.setIsDefault(tbBankDto.getIsDefault());
        tbBindCardInfo.setBankName(tbBankDto.getBankName());
        tbBindCardInfo.setModifyDate(new Date());
        boolean flag = iTbBindCardInfoService.updateById(tbBindCardInfo);
        if(flag==true){
            r.setMsg("修改银行卡成功");
        }else {
            r.setMsg("修改失败，请确定银行卡信息是否正确");
        }
        return  r;
    }

    /**
     * 添加商户端绑卡信息
     *
     */
    @RequestMapping("/newBindCard")
    public R newBindCard(TbBindCardInfo m)
    {
        R r = new R();
        /**
         * 银行卡四要素验证
         */

        CreditVo creditVo = new CreditVo();
        creditVo.setMobile(m.getMobile());
        creditVo.setAccountNO(m.getBankCard());
        creditVo.setIdCard(m.getIdCard());
        creditVo.setUserName(m.getBankName());
        R r1 = threelementController.threelementCheck(creditVo);
        if (r1.getCode() == 1){
            r1.setMsg("请输入正确的银行卡信息");
            return r1;
        }
        TbBindCardInfo banks=new TbBindCardInfo();
        banks.setOid(m.getOid());
        banks.setUtype("2");
        banks.setCardType("0");
        banks.setBankSn(m.getBankSn());
        banks.setMobile(m.getMobile());
        banks.setName(m.getName());
        banks.setIdCard(m.getIdCard());
        banks.setBankCard(m.getBankCard());
        banks.setIsDefault(m.getIsDefault());
        banks.setBankName(m.getBankName());
        banks.setCreateDate(new Date());
        boolean b = iTbBindCardInfoService.insert(banks);
        if (b) {
            r.setMsg("添加成功");
        }else {
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     *pdf　导出
     * @return
     */
    @RequestMapping("/pdf")
    public R pdf(HttpServletRequest request){
        Map<String, Object> paramMap = new HashMap<String, Object>();
        R r=new R();
        String id = request.getParameter("id");
        List<TbBindCardInfo> entity = iTbBindCardInfoService.selectList(new EntityWrapper<TbBindCardInfo>().eq("oid", id));
        MMerchantDto mentity = imMerchantInfoService.QueryOne(id);
        paramMap.put("entity",entity);
        paramMap.put("mentity",mentity);;
        r.setData(paramMap);
        try {
            String fileName = pdfHandlers.contractHandler(request,fastFileStorageClient,fdfsPropertiesConfig);
            return R.newOK(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            r.setMsg("合同导出失败");
        }
        return  r;
    }

    /**
     * 商业授信审核跳转
     */
    @RequestMapping("/showApplyshs")
    public ModelAndView showApplyshs(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String oid = get(request,"session_opera_id").toString();
        OOperaInfo oo = ioOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("id",oid));
        String id = request.getParameter("id");
        MMerchantDto entity = imMerchantInfoService.QueryOne(id);
        modelAndView.addObject("id", id);
        modelAndView.addObject("entity", entity);
        modelAndView.addObject("approverName",oo.getName());
        modelAndView.addObject("bbmobile",oo.getMobile());
        modelAndView.setViewName("manager/showApply");
        return modelAndView;
    }

    /**
     *  商业授信审核
     */
    @RequestMapping("/creditaudit")
    public R creditaudit(HttpServletRequest request){
        ModelAndView mode = new ModelAndView();
        R r = new R();
        String id = request.getParameter("bid");
        String bstatus=request.getParameter("bbstatus");
        String approverName=request.getParameter("approverName");
        String bbmoblie =request.getParameter("bbmobile");
        String endMoeny =request.getParameter("endMoeny");
        TbBusinessCreditChecklist bb =tbBusinessCreditChecklistService .selectOne(new EntityWrapper <TbBusinessCreditChecklist>().eq("bid", id));
        TbBusinessCredit businessCredit = iTbBusinessCreditService.selectOne(new EntityWrapper <TbBusinessCredit>().eq("id", id));
        TbBusinessCreditChecklist b =new TbBusinessCreditChecklist();
        if (bb==null && bstatus.equals("1")) {
            b.setBid(id);
            b.setStatus("1");
            businessCredit.setStatus("1");
            b.setApproverName(approverName);
            b.setMobile(bbmoblie);
            b.setEndMoeny(endMoeny);
            b.setCreateDate(new Date());
            r.setMsg("审核成功");
            tbBusinessCreditChecklistService.insert(b);
            iTbBusinessCreditService.updateById(businessCredit);
        } else {
            b.setBid(id);
            b.setStatus("0");
            businessCredit.setStatus("0");
            b.setApproverName(approverName);
            b.setMobile(bbmoblie);
            b.setCreateDate(new Date());
            r.setMsg("审核失败");
            tbBusinessCreditChecklistService.insert(b);
            iTbBusinessCreditService.updateById(businessCredit);
        }
        return r;
    }
}
