package com.tcxf.hbc.admin.controller.operaInfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.service.ITbBindCardInfoService;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/opera/operaInfo")
public class POpraInfoController {

    @Autowired
    private ITbBindCardInfoService iTbBindCardInfoService;

    /**
     * 根据资金端id查询绑定的运营商
     * @param id
     * @return
     */
//    @RequestMapping("/loadBindCard")
//    public R loadBindCard(String id) {
//        R r = new R();
//        Map<String, Object> params = new HashMap<>();
//        params.put("fid",id);
//        Page page = iTbBindCardInfoService.getOperaInfoByFid(params);
//        r.setData(page);
//        return r;
//    }
}
