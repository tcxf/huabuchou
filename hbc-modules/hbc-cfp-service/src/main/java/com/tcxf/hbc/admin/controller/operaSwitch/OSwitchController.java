package com.tcxf.hbc.admin.controller.operaSwitch;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.ICOperaSwitchService;
import com.tcxf.hbc.common.entity.COperaSwitch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author YWT_tai
 * @Date :Created in 9:11 2018/9/11
 */

@RestController
@RequestMapping("/opera/switch")
public class OSwitchController extends BaseController {

    @Autowired
    protected ICOperaSwitchService iCOperaSwitchService;

   /**
   * @Author:YWT_tai
   * @Description
   * @Date: 9:13 2018/9/11
    * 跳转开关控制页面
   */
    @GetMapping("/jumpOCenter")
    public ModelAndView jumpOSwitch(HttpServletRequest request) {
        ModelAndView modelAndView= new ModelAndView();
        String oid = (String) get(request,"session_opera_id");
        COperaSwitch cOperaSwitch = iCOperaSwitchService.selectOne(new EntityWrapper<COperaSwitch>().eq("oid", oid));
        modelAndView.setViewName("manager/switch/operaswitch");
        modelAndView.addObject("cOperaSwitch",cOperaSwitch);
        return modelAndView;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:32 2018/9/11
     * 改变开关状态
     *
    */
    @RequestMapping("/updateSwitchState")
    public void updateSwitchState(HttpServletRequest request) {

        String phoneState = request.getParameter("phoneState");
        String ipState = request.getParameter("ipState");
        String idcardState = request.getParameter("idcardState");

        COperaSwitch cOperaSwitch = new COperaSwitch();
        if(phoneState!=null && !"".equals(phoneState)){
            cOperaSwitch.setPhoneState(phoneState);
        }
        if(ipState!=null && !"".equals(ipState)){
            cOperaSwitch.setIpState(ipState);
        }
        if(idcardState!=null && !"".equals(idcardState)){
            cOperaSwitch.setIdcardState(idcardState);
        }
        String oid = (String) get(request,"session_opera_id");
        iCOperaSwitchService.update(cOperaSwitch,new EntityWrapper<COperaSwitch>().eq("oid", oid));
    }


}
