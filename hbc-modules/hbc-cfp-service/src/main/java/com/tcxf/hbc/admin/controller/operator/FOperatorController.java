package com.tcxf.hbc.admin.controller.operator;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.FFundendDto;
import com.tcxf.hbc.admin.service.IFFundendService;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/fund/operaInfo")

public class FOperatorController extends BaseController {

    @Autowired
    private IOOperaInfoService operaInfo;

    @Autowired
    private IFFundendService ifFundendService;
    /**
     * 跳转到资金端下的所有运行商
     */
    @RequestMapping("/qu")
    public ModelAndView qu(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String fid = (String) get(request,"session_fund_id");
        long l = System.currentTimeMillis();
        /*Date date = new Date();
        date.setTime(l);*/
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(l);
        Date date = cal.getTime();
        String start = DateUtil.getTimesmorning();
        String end = DateUtil.getTimesnight();
        int number = operaInfo.CreateQueryAllDate(start, end);
        System.out.println(number);
        modelAndView.addObject("fid", fid);
        modelAndView.addObject("number", number);
        modelAndView.setViewName("fund/operaInfoManager1");
        return modelAndView;
    }

    /**
     * 查询资金端下的运行商信息分页
     * @param request
     * @return
     */
    @RequestMapping("/operaInfoManagerList")
    public R operaInfoManagerList(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
//        String id = (String) request.getSession().getAttribute("session_fund_id");
//        paramMap.put("fid", id);
//        System.out.println(id);
        R r = new R <>();
        Page pageInfo = ifFundendService.QueryFO(new Query<FFundendDto>(paramMap));
        r.setData(pageInfo);
        return  r;
    }

    /**
     * 查看资金端下的运营商详情
     * @param request
     * @return
     */
    @RequestMapping("/forwardOperaInfoDetail")
    public ModelAndView forwardOperaInfoDetail(HttpServletRequest request){
        ModelAndView modelAndView =new ModelAndView();
        String id = request.getParameter("ffid");
        modelAndView.addObject("ffid",id);
        FFundendDto entity = ifFundendService.QueeryFOone(id);
        modelAndView.addObject("entity",entity);
        modelAndView.setViewName("fund/operaInfoDetail");
        return modelAndView;
    }


}
