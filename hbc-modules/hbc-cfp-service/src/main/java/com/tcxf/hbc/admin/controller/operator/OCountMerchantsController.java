package com.tcxf.hbc.admin.controller.operator;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.databind.JavaType;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.admin.model.dto.TbAreaDto;
import com.tcxf.hbc.admin.model.dto.TbBankDto;
import com.tcxf.hbc.admin.model.vo.OOperainfo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonUtils;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.secure.MD5Util;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.web.BaseController;
import com.xiaoleilu.hutool.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * 平台运营商
 * jiangyong
 */
@RestController
@RequestMapping("/platfrom/otherInfo")
public class OCountMerchantsController extends com.tcxf.hbc.admin.controller.basecontroller.BaseController {
    @Autowired
    private IOOperaInfoService operaInfo;

    @Autowired
    private ITbBindCardInfoService bBindCardInfo;


    @Autowired
    private ITbAreaService areaservice;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;


    @Autowired
    private ThreelementController threelementController;


    /**
     * 今日新增商户
     *
     * @return
     */
    @RequestMapping("/CreateQueryAllDate")
    public ModelAndView QueryAlldate() {
        ModelAndView mode = new ModelAndView();
        long l = System.currentTimeMillis();
        /*Date date = new Date();
        date.setTime(l);*/
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(l);
        Date date = cal.getTime();
        String start = DateUtil.getTimesmorning();
        String end = DateUtil.getTimesnight();
        int number = operaInfo.CreateQueryAllDate(start, end);
        int count = operaInfo.CountQueryOperator();
        System.out.println(number);
        mode.addObject("number", number);
        mode.addObject("count", count);
        mode.setViewName("platfrom/operaInfoManager");
        return mode;
    }

    /***
     * 统计可用运营商
     */
    @RequestMapping("/CountQueryOperator")
    public ModelAndView MerchantCount() {
        ModelAndView mode = new ModelAndView();
        int count = operaInfo.CountQueryOperator();
        System.out.println(count);
        mode.addObject("count", count);
        mode.setViewName("list");
        return mode;
    }

    /**
     * 测试分页
     *
     * @return
     */
    @RequestMapping("/find")
    public ModelAndView Find() {
//        Map map=new HashMap();
        //Query<OOperaInfo> pageq = new Query<OOperaInfo>(map);
//        uservo.getMobile();
        ModelAndView mode = new ModelAndView();
        Page<OOperaInfo> page = new Page<OOperaInfo>(1, 5);
        Page<OOperaInfo> pageResult = operaInfo.selectPage(page);
        System.out.println("总页数:" + pageResult.getPages());
        System.out.println("当前页:" + pageResult.getCurrent());
        System.out.println("总条数:" + pageResult.getTotal());
//		this.request.put("cpage", pageResult.getCurrent());
//		this.request.put("tpage", pageResult.getPages());
//		this.request.put("list", pageResult.getRecords());
        mode.addObject("page", pageResult);
        mode.setViewName("ftl/list");
        return mode;
    }

    /**
     * 分页 查询所有商户 HttpServletRequest request
     */
    @RequestMapping("/queryall")
    public R QueryAll(HttpServletRequest request) {
        String name = request.getParameter("name");
        String p = request.getParameter("page");
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        params.put("page", p);
        R r = new R<>();
        ModelAndView mode = new ModelAndView();
        Page page = operaInfo.QueryAll(new Query<MerchantSerlect>(params));
        r.setData(page);
        return r;
    }

    /**
     * 添加所有商户信息
     *
     * @param oop
     * @return
     */
    @RequestMapping("/insertOperainfo")
    public R InsertOperainfoOne(OOperainfo oop,HttpServletRequest request) {
        operaInfo.insertOperainfo(oop,request);
        return  R.newOK();
    }

    /**
     * 删除绑定运行商银行卡信息
     *
     * @param id
     * @return
     */
    @RequestMapping("/delBindCard")
    public R delBindCard(String id) {
        R r = new R();
        boolean b = bBindCardInfo.deleteById(id);
        if (b) {
            r.setMsg("删除成功");
        } else {
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 修改运行商端绑卡信息
     *
     * @return
     */
    @RequestMapping("/updateBindCard")
    public R updateBindCard(TbBankDto tbBankDto,HttpServletRequest request) {
        R r = new R();
        String id = request.getParameter("id");
        /**
         * 银行卡四要素验证
         */
        TbBindCardInfo tbBindCardInfo = bBindCardInfo.selectOne(new EntityWrapper <TbBindCardInfo>().eq("id", id));
        CreditVo creditVo = new CreditVo();
        creditVo.setMobile(tbBankDto.getMobile());
        creditVo.setAccountNO(tbBankDto.getBankCard());
        creditVo.setIdCard(tbBankDto.getIdCard());
        creditVo.setUserName(tbBankDto.getBankName());
        R r1 = threelementController.threelementCheck(creditVo);
        if (r1.getCode() == 1){
            r1.setMsg("请输入正确的银行卡信息");
            return r1;
        }
        if(tbBindCardInfo.getBankSn().equals("GDB")){
            tbBindCardInfo.setBankName("广发银行");
        }
        if(tbBindCardInfo.getBankSn().equals("HXB")){
            tbBindCardInfo.setBankName("华夏银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CNCB")){
            tbBindCardInfo.setBankName("中信银行");
        }
        if(tbBindCardInfo.getBankSn().equals("ICBC")){
            tbBindCardInfo.setBankName("中国工商银行");
        }
        if(tbBindCardInfo.getBankSn().equals("BOC")){
            tbBindCardInfo.setBankName("中国银行");
        }
        if(tbBindCardInfo.getBankSn().equals("BOC")){
            tbBindCardInfo.setBankName("上海银行");
        }
        if(tbBindCardInfo.getBankSn().equals("BCCB")){
            tbBindCardInfo.setBankName("北京银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CCB")){
            tbBindCardInfo.setBankName("中国建设银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CIB")){
            tbBindCardInfo.setBankName("兴业银行");
        }
        if(tbBindCardInfo.getBankSn().equals("GZCB")){
            tbBindCardInfo.setBankName("广州银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CMBC")){
            tbBindCardInfo.setBankName("中国民生银行");
        }
        if(tbBindCardInfo.getBankSn().equals("BTCB")){
            tbBindCardInfo.setBankName("包商银行");
        }
        if(tbBindCardInfo.getBankSn().equals("PSBC")){
            tbBindCardInfo.setBankName("中国邮政储蓄银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CCCB")){
            tbBindCardInfo.setBankName("长沙银行");
        }
        if(tbBindCardInfo.getBankSn().equals("BOCOM")){
            tbBindCardInfo.setBankName("交通银行");
        }
        if(tbBindCardInfo.getBankSn().equals("PAB")){
            tbBindCardInfo.setBankName("长沙银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CMB")){
            tbBindCardInfo.setBankName("招商银行");
        }
        if(tbBindCardInfo.getBankSn().equals("ABC")){
            tbBindCardInfo.setBankName("中国农业银行");
        }
        if(tbBindCardInfo.getBankSn().equals("CEB")){
            tbBindCardInfo.setBankName("中国光大银行");
        }
        if(tbBindCardInfo.getBankSn().equals("JXCCB")){
            tbBindCardInfo.setBankName("嘉兴银行");
        }
        if(tbBindCardInfo.getBankSn().equals("SPDB")){
            tbBindCardInfo.setBankName("浦发银行");
        }
        tbBindCardInfo.setBankSn(tbBankDto.getBankSn());
        tbBindCardInfo.setMobile(tbBankDto.getMobile());
        tbBindCardInfo.setName(tbBankDto.getName());
        tbBindCardInfo.setIdCard(tbBankDto.getIdCard());
        tbBindCardInfo.setBankCard(tbBankDto.getBankCard());
        tbBindCardInfo.setIsDefault(tbBankDto.getIsDefault());
        tbBindCardInfo.setBankName(tbBankDto.getBankName());
        tbBindCardInfo.setModifyDate(new Date());
        boolean flag = bBindCardInfo.updateById(tbBindCardInfo);
        if(flag==true){
            r.setMsg("修改银行卡成功");
        }else {
            r.setMsg("修改失败，请确定银行卡信息是否正确");
        }
        return  r;
    }

    /**
     * 添加运营商端绑卡信息
     *
     */
    @RequestMapping("/newBindCard")
    public R newBindCard(TbBindCardInfo tbBindCardInfo)
    {
        R r = new R();
        /**
         * 银行卡四要素验证
         */

        CreditVo creditVo = new CreditVo();
        creditVo.setMobile(tbBindCardInfo.getMobile());
        creditVo.setAccountNO(tbBindCardInfo.getBankCard());
        creditVo.setIdCard(tbBindCardInfo.getIdCard());
        creditVo.setUserName(tbBindCardInfo.getBankName());
        R r1 = threelementController.threelementCheck(creditVo);
        if (r1.getCode() == 1){
            r1.setMsg("请输入正确的银行卡信息");
            return r1;
        }
        tbBindCardInfo.setUtype("3");
        boolean b = bBindCardInfo.insert(tbBindCardInfo);
        if (b)
        {
            r.setMsg("添加成功");
        }else {
            r.setMsg("添加失败");
        }
        return r;
    }
    /**
     * 查询省
     */
    @RequestMapping("/areas")
    public R QueryS() {
        R r = new R<>();
        List<TbAreaDto> data = areaservice.QueryS();
        r.setData(data);
        return r;
    }

    /**
     * 添加,修改 跳转
     *
     * @return
     */
    @RequestMapping("/insert")
    public ModelAndView insert(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        mode.addObject("id", 0);
        mode.setViewName("platfrom/operaInfoHandler");
        return mode;
    }

    /**
     * 查询单个运营商户的信息详情
     *
     * @return
     */
    @RequestMapping("/operaInfoDetail")
    public ModelAndView QueeryAllOperaInfoDetail(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        String id = request.getParameter("id");
        MerchantSerlect entity = operaInfo.QueeryAllOperaInfoDetail(id);
        modelAndView.addObject("entity", entity);
        modelAndView.setViewName("platfrom/operaInfoDetail");
        return modelAndView;
    }

    /**
     * 编辑运行商
     *
     * @param request
     * @return
     */
    @RequestMapping("/forwardOperaInfoEdit")
    public ModelAndView forwardOperaInfoEdit(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String id = request.getParameter("id");
        MerchantSerlect entity = operaInfo.QueeryAllOperaInfoDetail(id);
        mode.addObject("entity", entity);
        mode.addObject("id", id);
        mode.setViewName("platfrom/operaInfoEdit");
        return mode;
    }

    /**
     * 运营商端编辑，查看绑卡信息详情
     *
     * @param
     * @return
     */
    @RequestMapping("/loadBindCard")
    public R loadBindCard(HttpServletRequest request) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        String id = request.getParameter("id");
        params.put("id", id);
        R r = new R<>();
        ModelAndView mode = new ModelAndView();
        Page page = operaInfo.QueryBanks(new Query<MerchantSerlect>(params));

        r.setData(page);
        return r;
    }

    /**
     * @Description
     * @Date: 19:53 2018/6/19
     * 加载卡信息
     */
    @RequestMapping("/loadBindCardInfo")
    public R findBindCardInfo(String id)
    {
        R r = new R<>();
        List<TbBindCardInfo> olist = bBindCardInfo.selectList(new EntityWrapper<TbBindCardInfo>().eq("oid",id));
        r.setData(olist);
        return r;
    }


    /**
     * 编辑商户信息
     *
     * @param request
     * @return
     */
    @RequestMapping("/operaInfoupdate")
    public ModelAndView UpdateOperaInfoDetail(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        return mode;
    }

    /**
     * 文件上传
     * 保存文件
     *
     * @param file
     * @return
     */
    private boolean saveFile(MultipartFile file, String path) {
        // 判断文件是否为空
        if (!file.isEmpty()) {
            try {
                File filepath = new File(path);
                if (!filepath.exists())
                    filepath.mkdirs();
                // 文件保存路径
                String savePath = path + file.getOriginalFilename();
                // 转存文件
                file.transferTo(new File(savePath));
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 文件上传
     *
     * @param file
     * @return
     */

    @PostMapping("/upload")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }

    /***
     *快捷开通商户
     * @param request
     * @return
     */
    @RequestMapping("/initData")
    public R initData(HttpServletRequest request) {
        R<Object> r = new R<>();
        String id = request.getParameter("id");
        OOperaInfo mm = operaInfo.selectOne(new EntityWrapper <OOperaInfo>().eq("id", id));
        if ("0".equals(mm.getStatus())) {
            mm.setStatus("1");
            Boolean flag = operaInfo.updateById(mm);
            if(flag==true){
                r.setMsg("开通成功");
            }else {
                r.setMsg("开通失败");
            }
        }
        return r;

    }


    /**
     * 修改所有商户信息
     *
     * @param oop
     * @return
     */
    @RequestMapping("/updateOperainfo")
    public R updateOperainfoOne(OOperainfo oop,HttpServletRequest request) {
        operaInfo.updateOperainfo(oop,request);
        return  R.newOK();
    }


}
