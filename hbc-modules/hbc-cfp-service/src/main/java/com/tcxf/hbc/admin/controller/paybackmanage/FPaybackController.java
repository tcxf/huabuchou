package com.tcxf.hbc.admin.controller.paybackmanage;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.RepaymentDetail;
import com.tcxf.hbc.admin.model.dto.RepaymentDetailDto;
import com.tcxf.hbc.admin.model.dto.TbRepayLogDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 16:00 2018/6/8、
 * 平台 ----还款管理模块
 */

@RestController
@RequestMapping("/fund/f_payback")
public class FPaybackController extends BaseController {

    @Autowired
    protected ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    protected ITbRepaymentPlanService iTbRepaymentPlanService;

    @Autowired
    protected IOOperaInfoService IOOperaInfoService;

    @Autowired
    protected ITbRepayLogService iTbRepayLogService;

    @Autowired
    protected ITbTradingDetailService iTbTradingDetailService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:11 2018/6/14
     * 跳转待还款页面
    */
    @GetMapping("/jumpWait")
    public ModelAndView jumpWait()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/repaymentPlanManager");
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:11 2018/6/14
     * 跳转逾期页面
    */
    @GetMapping("/jumpOverdue")
    public ModelAndView jumpOverdue()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/yqManager");
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:11 2018/6/14
     * 跳转账单 已还页面
    */
    @GetMapping("/jumpBillAlreadyPay")
    public ModelAndView jumpBillAlreadyPay()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/yhManager");
        return mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:14 2018/6/13
     * 跳转 待还款页面
    */
    @RequestMapping("/jumpWaitDetail")
    public ModelAndView jumpWaitDetail(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        //fund
        modelAndView.setViewName("fund/huankuanList");
        modelAndView.addObject("id",id);
        return  modelAndView;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:14 2018/6/13
     * 跳转 逾期还款详情页面
     */
    @RequestMapping("/jumpOverdueDetail")
    public ModelAndView jumpOverdueDetail(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("fund/YiqiList");
        modelAndView.addObject("id",id);
        return  modelAndView;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:14 2018/6/13
     * 跳转 逾期还款详情页面
     */
    @RequestMapping("/jumpBillAlreadyPayDetail")
    public ModelAndView jumpBillAlreadyPayDetail(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("fund/yihuan");
        modelAndView.addObject("id",id);
        return  modelAndView;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:15 2018/6/13
     * 跳转提前还款页面
    */
    @RequestMapping("/jumpAdvancePay")
    public ModelAndView jumpAdvancePay()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("fund/advanceManager");
        return  modelAndView;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:33 2018/6/14
     *  跳转提前还款 详情页面
    */
    @GetMapping("/jumpAdvancePayDetail")
    public ModelAndView jumpAdvancePayDetail(String id)
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/advancehuan");
        mode.addObject("ids",id);
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:51 2018/6/14
     * 跳转未出账单分页页面
    */
    @GetMapping("/jumpNoBill")
    public ModelAndView jumpNoBill()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/uncreateManager");
        return mode;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:53 2018/6/9
     * 查询还款管理 -(逾期/待还款/已还) 分页查询
     */
    @RequestMapping("/pageInfo")
    public R findPaybackPageInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Page paybackPage= iTbRepaymentDetailService.findPaybackPageInfo(new Query<RepaymentDetailDto>(paramMap));
        R r = new R<>();
        r.setData(paybackPage);
        return  r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:09 2018/6/11
     * 查询 待还款剩余的money
    */
    @RequestMapping("/restPay")
    public R findRestPayback(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map<String,Object> mmp=iTbRepaymentPlanService.findwaitPayByCondiction(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:09 2018/6/11
     * 查询  逾期剩余的money
     */
    @RequestMapping("/overDue")
    public R findoverDuePayback(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Map<String,Object> mmp=iTbRepaymentPlanService.findoverDuePayByCondiction(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 20:57 2018/6/12
     *根据条件查询 账单已还款
    */
    @RequestMapping("/BillAlreadyPay")
    public R findBillAlreadyPayByCondition(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Map<String,Object> mmp = iTbRepaymentPlanService.findBillAlreadyPayByCondition(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:00 2018/6/11
     * 查询资金端下面的运营商
    */
    @RequestMapping("/allopera")
    public R findAllOpera(HttpServletRequest request)
    {
        Map<String, Object> paramMap = new HashMap<>();
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        List<OOperaInfo> oOperaInfos = IOOperaInfoService.selectOperaByfid(paramMap);
        R r = new R<>();
        r.setData(oOperaInfos);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:20 2018/6/11
     * 查询待还款的明细
    */
    @RequestMapping("/detailInfo")
    public R findpaybackDetailInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page paybackPage= iTbRepaymentDetailService.findpaybackDetailInfo(new Query<RepaymentDetail>(paramMap));
        R r = new R<>();
        r.setData(paybackPage);
        return  r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:42 2018/6/13
     * 查询 提前还款分页信息
    */
    @RequestMapping("/advancePay")
    public R findAdvancePayPageInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Page paybackPage =  iTbRepayLogService.findAdvancePayPageInfo(new Query<TbRepayLogDto>(paramMap));
        R r = new R<>();
        r.setData(paybackPage);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:02 2018/6/13
     * 根据条件查询 提前还款总金额
    */
    @RequestMapping("/advancePayTotalMoney")
    public R findAdvancePayTotalMoney(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Map<String,Object> mmp=  iTbRepayLogService.findAdvancePayTotalMoney(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:51 2018/6/13
     * 查看 提前还款详情记录 分页待定
    */
    @RequestMapping("/advancePayDetail")
    public R findAdvancePayDetailInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page=  iTbTradingDetailService.findAdvancePayDetailInfo(new Query<TbTradingDetailDto>(paramMap));
        R r = new R<>();
        r.setData(page);
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:31 2018/6/14
     * 查询 未出账单分页查询
    */
    @RequestMapping("/noBill")
    public R findNoBillPageInfo(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid", fid);
        Page pageinfo=  iTbTradingDetailService.findNoBillPageInfo(new Query<TbTradingDetailDto>(paramMap));
        R r = new R<>();
        r.setData(pageinfo);
        return  r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:04 2018/6/27
     * 跳转未出账单详情页面
    */
    @RequestMapping("/jumpNoBillDetail")
    public ModelAndView jumpNoBillDetail(String id)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("fund/noBillDetail");
        modelAndView.addObject("id",id);
        return  modelAndView;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:56 2018/6/27
     * 未出账单详情
    */
    @RequestMapping("/noBillDetail")
    public R findNoBillDetail(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page pageinfo=  iTbTradingDetailService.findNoBillDetail(new Query<TbTradingDetailDto>(paramMap));
        R r = new R<>();
        r.setData(pageinfo);
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:58 2018/7/11
     * 查询未出账单总金额
     */
    @RequestMapping("/noBillTotalMoney")
    public R findNoBillTotalMoney(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Map<String,Object> mmp=  iTbTradingDetailService.findNoBillTotalMoney(paramMap);
        R r = new R<>();
        r.setData(mmp);
        return  r;
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:21 2018/6/12
     * 导出:还款管理的execle报表 包括:(逾期/待还款/已还)
    */
    @RequestMapping("/PaybackPoi")
    public void exportPaybackPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        paramMap.put("fid",fid);
        Page paybackPage= iTbRepaymentDetailService.findPaybackPageInfo(new Query<RepaymentDetailDto>(paramMap));

        String  isPay =(String)paramMap.get("isPay");//1:已还款 2:未还款
        String  status =(String)paramMap.get("status");//1:逾期还款 2:待还款

        //待还款头execle head
        String[]  title = { "编号", "所属运营商", "流水单号", "用户id", "用户手机", "用户姓名", "应还本金", "滞纳金", "分期利息", "本期应还", "期数", "还款日",
                "待还款金额", "本金", "利息" };

        //逾期还款execle head
        String[]   title1 ={ "编号", "所属运营商", "流水单号", "用户id", "用户手机", "用户姓名", "应还本金", "滞纳金", "分期利息", "本期应还", "期数", "还款日","逾期天数",
                "待还款金额", "本金", "利息","滞纳金" };

        //已还款execle head
        String[]  title2 = { "编号", "所属运营商", "流水单号", "用户id", "用户手机", "用户姓名", "已还本金", "滞纳金", "分期利息", "已还", "期数", "还款日","还款时间","还款方式",
                "待还款金额", "本金", "利息","滞纳金" };

        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<RepaymentDetailDto> listdato=paybackPage.getRecords();
        for (RepaymentDetailDto repaymentDetailDto : listdato) {
            String[] strings=null;
            if("2".equals(status) && "0".equals(isPay) ){
               strings = new String[12];//   待还款
            }else if("1".equals(status) && "0".equals(isPay)){
                strings = new String[13];//  逾期未还
            }else if("1".equals(isPay)){
                strings = new String[14];//已还款
            }else{
                throw new RuntimeException("非法请求!");
            }
            strings[0] = i + "";
            strings[1] = repaymentDetailDto.getOname();
            strings[2] = repaymentDetailDto.getSerialNo();
            strings[3] = String.valueOf(repaymentDetailDto.getUserId());
            strings[4] = repaymentDetailDto.getTelPhone();
            strings[5] = repaymentDetailDto.getRealName();
            strings[6] = String.valueOf(repaymentDetailDto.getCurrentCorpus());
            strings[7] = String.valueOf(repaymentDetailDto.getLateFee());
            strings[8] = String.valueOf(repaymentDetailDto.getCurrentFee());
            BigDecimal d = repaymentDetailDto.getCurrentCorpus().add(repaymentDetailDto.getCurrentFee());
            strings[9] = d.toString();
            String timeStr = repaymentDetailDto.getCurrentTime()+ "/" + repaymentDetailDto.getTotalTime();
            strings[10] = timeStr;
            strings[11] = repaymentDetailDto.getRepaymentDate();
            if("1".equals(status) && "0".equals(isPay)){
                strings[12] =  repaymentDetailDto.getMargin();//逾期天数
            }
            if("1".equals(isPay)){
                strings[12] =  repaymentDetailDto.getRepaymentDate();//还款时间
                strings[13] =  repaymentDetailDto.getRepaymentType();//还款方式
            }
            list.add(strings);
            i++;
        }
        Map<String, Object> mmp = new HashMap<>();
        String[] strings =null;
        if("2".equals(status) && "0".equals(isPay)){
            strings = new String[15];//17
             mmp=iTbRepaymentPlanService.findwaitPayByCondiction(paramMap);
            strings[12] = String.valueOf(mmp.get("totalMoney")) ; // 代还款总额
            strings[13] = String.valueOf(mmp.get("currentCorpus")); // 当前还款本金
            strings[14] =  String.valueOf(mmp.get("currentFee"));// 当前还款利息
        }
        if("1".equals(status) && "0".equals(isPay)){
            strings = new String[17];//17
            mmp=iTbRepaymentPlanService.findoverDuePayByCondiction(paramMap);
            strings[13] = String.valueOf(mmp.get("totalMoney")) ; // 代还款总额
            strings[14] = String.valueOf(mmp.get("currentCorpus")); // 当前还款本金
            strings[15] =  String.valueOf(mmp.get("currentFee"));// 当前还款利息
            strings[16] =  String.valueOf(mmp.get("lateFee"));// 滞纳金
        }
        if("1".equals(isPay)){
            strings = new String[18];//已还
            mmp=iTbRepaymentPlanService.findBillAlreadyPayByCondition(paramMap);
            strings[14] = String.valueOf(mmp.get("totalMoney")) ; // 已还款总额
            strings[15] = String.valueOf(mmp.get("currentCorpus")); // 已还款本金
            strings[16] =  String.valueOf(mmp.get("currentFee"));// 当前还款利息
            strings[17] =  String.valueOf(mmp.get("lateFee"));// 滞纳金
        }
        list.add(0, strings);
        ExportUtil.createExcel(list, "1".equals(isPay) ? title2:"2".equals(status) ? title:title1, response, "" + ("1".equals(isPay) ? "已还":"2".equals(status) ? "待还":"逾期" ) +"记录导出(" + DateUtil.getCurrDateTimeMillFormat() +").xlsx");
    }

        /**
        * @Author:YWT_tai
        * @Description
        * @Date: 16:41 2018/6/13
         *导出: 提前还款报表
        */
        @RequestMapping("/advancePayPoi")
        public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            Page page =  iTbRepayLogService.findAdvancePayPageInfo(new Query<TbRepayLogDto>(paramMap));

            String[] title = { "编号", "流水单号","用户类型", "还款用户", "还款金额", "还款方式","所属运营商","所属资金端","还款时间" ,"总金额"};
            int i = 1;
            List<String[]> list = new LinkedList<String[]>();
            List<TbRepayLogDto> listDto=page.getRecords();

            for (TbRepayLogDto tbRepayLogDto : listDto) {
                String[] strings = new String[9];
                strings[0] = i + "";
                strings[1] = tbRepayLogDto.getRepaySn();
                strings[2] = tbRepayLogDto.getUtype();
                strings[3] = tbRepayLogDto.getRealName();
                strings[4] = tbRepayLogDto.getAmount().toString();
                strings[5] = tbRepayLogDto.getPaymentType();
                strings[6] = tbRepayLogDto.getOname();
                strings[7] = tbRepayLogDto.getFname();
                strings[8] = DateUtil.getDateHavehms(tbRepayLogDto.getPayDate());
                list.add(strings);
                i++;
            }
            String[] totalString =new String[10];
            Map<String,Object> mmp=  iTbRepayLogService.findAdvancePayTotalMoney(paramMap);
            totalString[9] = String.valueOf(mmp.get("totalAmount"));
            list.add(0,totalString);

            ExportUtil.createExcel(list, title, response, "提前还款记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
        }


        /**
        * @Author:YWT_tai
        * @Description
        * @Date: 10:38 2018/6/27
         * 未出账单 报表下载
        */
        @RequestMapping("/noBillPoi")
        public void exportnoBillPoi(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
            String fid = (String) get(request,"session_fund_id");
            paramMap.put("fid",fid);
            if("".equals(paramMap.get("startDate"))) {
                paramMap.put("startDate", com.tcxf.hbc.common.util.DateUtil.getMonthFirstDay());
            }
            if("".equals(paramMap.get("endDate"))) {
                paramMap.put("endDate", com.tcxf.hbc.common.util.DateUtil.getCurrentTime());
            }
            List<TbTradingDetailDto> listDto =  iTbTradingDetailService.findNoBillPageInfoB(paramMap);

            String[] title = { "编号", "流水单号","用户名称", "用户类型", "账单总额", "所属运营商","所属资金端","交易时间" ,"未出总金额"};
            int i = 1;
            List<String[]> list = new LinkedList<String[]>();

            for (TbTradingDetailDto tbTradingDetailDto : listDto) {
                String[] strings = new String[8];
                strings[0] = i + "";
                strings[1] = tbTradingDetailDto.getSerialNo();
                strings[2] = tbTradingDetailDto.getRealName();
                strings[3] = tbTradingDetailDto.getNature();
                strings[4] = tbTradingDetailDto.getActualAmount().toString();
                strings[5] = tbTradingDetailDto.getOname();
                strings[6] = tbTradingDetailDto.getFname();
                strings[7] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
                list.add(strings);
                i++;
            }

            String[] totalString =new String[9];
            Map<String,Object> mmp=  iTbTradingDetailService.findNoBillTotalMoney(paramMap);
            totalString[8] = String.valueOf(mmp.get("totalMoney"));
            list.add(0,totalString);


            ExportUtil.createExcel(list, title, response, "未出账单记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
        }

}
