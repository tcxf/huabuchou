package com.tcxf.hbc.admin.controller.redpacket;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IMRedPacketSettingService;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 优惠券管理-运营商端
 */

@RestController
@RequestMapping("/opera/ored")
public class OredpacketContronller extends BaseController {

    @Autowired
    private IMRedPacketSettingService iMRedPacketSettingService;

    /**
     * 跳转优惠卷审核页面-运营商
     * @return
     */
    @RequestMapping("/finddaired")
    public ModelAndView finddaired(){
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/redPacketSettingManager");
        return mode;
    }
    /**
     * 跳转审核页面-运营商
     * @return
     */
    @RequestMapping("/updatared")
    public ModelAndView updatared(HttpServletRequest request){
        String id = request.getParameter("id");
        MRedPacketSetting one = iMRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", id));
        ModelAndView mode= new ModelAndView();
        mode.addObject("r",one);
        mode.setViewName("manager/redupdata");
        return mode;
    }

    /**
     * 跳转编辑页面-运营商
     * @return
     */
    @RequestMapping("/updataredbj")
    public ModelAndView updataredbj(HttpServletRequest request){
        String id = request.getParameter("id");
        MRedPacketSetting one = iMRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", id));
        ModelAndView mode= new ModelAndView();
        mode.addObject("r",one);
        mode.setViewName("manager/redupdatabj");
        return mode;
    }
    /**
     * 跳转查看页面-运营商
     * @return
     */
    @RequestMapping("/findupdatared")
    public ModelAndView findupdatared(HttpServletRequest request){
        String id = request.getParameter("id");
        MRedPacketSetting one = iMRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", id));
        ModelAndView mode= new ModelAndView();
        mode.addObject("r",one);
        mode.setViewName("manager/redfinddata");
        return mode;
    }
    /**
     * 跳转优惠卷页面-运营商
     * @return
     */
    @RequestMapping("/findred")
    public ModelAndView findred(){
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/redPacketSettingdaish");
        return mode;
    }
    /**
     * 查询优惠卷待审核-运营商
     * @return
     */
    @RequestMapping("/findredlist")
    public R findredlist(HttpServletRequest request,String page,String name){
        String id = (String) get(request,"session_opera_id");
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("oid",id);
        params.put("status",2);
        params.put("page",page);
        params.put("name",name);
        Page page1 = iMRedPacketSettingService.findredlist(params);
        r.setData(page1);
        return r;
    }
    /**
     * 查询优惠卷-运营商
     * @return
     */
    @RequestMapping("/finddairedlist")
    public R finddairedlist(HttpServletRequest request,String page,String name){
        String id = (String) get(request,"session_opera_id");
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("oid",id);
        params.put("status",1);
        params.put("page",page);
        params.put("name",name);
        Page page1 = iMRedPacketSettingService.findredlist(params);
        r.setData(page1);
        return r;
    }

    /**
     * 修改优惠券金额-运营商
     * @return
     */
    @RequestMapping("/oupdatared")
    public R oupdatared(HttpServletRequest request){
        R r = new R<>();
        String id = request.getParameter("rid");
        String yyscq = request.getParameter("yyscq");
        String sjyh = request.getParameter("sjyh");
        MRedPacketSetting one = iMRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", id));
        one.setActualMoney(new BigDecimal(yyscq));
        one.setOpMoney(new BigDecimal(sjyh));
        one.setModifyDate(new Date());
        one.setStatus("2");
        boolean b = iMRedPacketSettingService.update(one, new EntityWrapper<MRedPacketSetting>().eq("id", id));
        if (b == true){
            r.setCode(1);
            r.setMsg("修改成功");
        }else {
            r.setCode(0);
            r.setMsg("修改失败");
        }
        return r;
    }
}
