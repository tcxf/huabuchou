package com.tcxf.hbc.admin.controller.rmcontroller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @program: Workspace3
 * @description: 评分自定义Controller
 * @author: JinPeng
 * @create: 2018-09-20 11:54
 **/
@RestController
@RequestMapping("/fund/creditCustom")
public class CreditCustomController extends BaseController {

    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;

    @Autowired
    private RefusingCreditService refusingCreditService;

    @Autowired
    private IRmBaseCreditUpmoneyService iRmBaseCreditUpmoneyService;

    @Autowired
    private IRmBaseCreditTypeService iRmBaseCreditTypeService;

    @Autowired
    private CreditScoreService creditScoreService;

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;


    /**
     * 创建拒绝授信条件
     * @param list
     * @return
     */

    @RequestMapping("/createRefusingCredit")
    @ResponseBody
    public R createrefusingCredit(@RequestBody List<RefusingCredit> list){
        String fid = (String) get(getRequest(),"session_fund_id");
        refusingCreditService.createRefusingCredit(list,fid);
        return R.newOK();
    }

    /**
     * 进入拒绝授信条件界面
     * @return
     */
    @RequestMapping("/goToRefusingCreditPage")
    public ModelAndView goToRefusingCreditPage(HttpServletRequest request){
        ModelAndView model = new ModelAndView();
        String type = ValidateUtil.isEmpty(request.getParameter("type")) ? "" : request.getParameter("type");
        String id = request.getParameter("id");

        model.addObject("id", id);
        model.addObject("type", type);
        model.setViewName("rm/adminManager");
        return  model;
    }

    /**
     * 查询拒绝授信条件
     * @return
     */
    @RequestMapping("/selectRefusingCreditByfId")
    @ResponseBody
    public R selectRefusingCreditByfId(HttpServletRequest request){
        String fid = (String) get(request,"session_fund_id");
        String relationshipId = request.getParameter("relationshipId");
        Map<String, Object> map = new HashMap<>();
        map.put("fid", fid);
        map.put("id", relationshipId);
        List<RefusingCredit> list = refusingCreditService.selectRefusingCreditByfId(map);
        Page page = new Page();
        page.setRecords(list);
        return R.newOK(page);
    }


    /**
     * 创建授信提额综合分值
     * @param list
     * @return
     */
    @RequestMapping("/createCreditScore")
    @ResponseBody
    public R createCreditScore(@RequestBody List<CreditScore> list){
        String fid = (String) get(getRequest(),"session_fund_id");
        creditScoreService.createCreditScore(list,fid);
        return R.newOK();
    }


    /**
     * 跳转授信综合分值界面
     */
    @RequestMapping("/goCreditCustomView")
    public ModelAndView goCreditCustomView(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        String fid = (String) get(request,"session_fund_id");
        String id = request.getParameter("id");

        Map<String, Object> map = new HashMap<>();
        map.put("fid", fid);
        map.put("id", id);

        List<CreditScore> list = creditScoreService.selectCreditScoreByfId(map);
        String type = ValidateUtil.isEmpty(request.getParameter("type")) ? "" : request.getParameter("type");

        m.addObject("id", id);
        m.addObject("type", type);
        m.addObject("sx", list);
        m.setViewName("fund/sxpf");
        return m;
    }


    /**
     * 跳转评分自定义页面
     */
    @RequestMapping("/goCreditCustomViews")
    public ModelAndView goCreditCustomViews(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        String type = ValidateUtil.isEmpty(request.getParameter("type")) ? "" : request.getParameter("type");
        String id = request.getParameter("id");

        m.addObject("id", id);
        m.addObject("type", type);
        m.setViewName("rm/letterofcredit");
        return m;
    }
    /**
     * @Description:  初始化授信条件列表
     * @Param: [request] 
     * @return: com.tcxf.hbc.common.util.R 
     * @Author: JinPeng
     * @Date: 2018/9/20 
    */ 
    @RequestMapping("/getCreditInitialList")
    public R getCreditInitialList(HttpServletRequest request, String fid, String id){

        //根据id查询初始授信版本id
        RmSxtemplateRelationship rmSxtemplateRelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", id));

//        //查询当前版本
//        RefusingVersion refusingVersion = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("type",RefusingVersion.TYPE_2).eq("status",RefusingVersion.STATUS_YES).eq("fId",fid));
        //查询初始化授信记录列表
        List<RmBaseCreditInitial> rbcilist = iRmBaseCreditInitialService.selectList(new EntityWrapper<RmBaseCreditInitial>().eq("versionId", rmSxtemplateRelationship.getSxInitId()));
        //查询授信类别列表
        List<RmBaseCreditType> rbctlist = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().eq("status","0"));
        //封装数据
        Map<String,RmBaseCreditType> map = new HashMap<>();
        for (RmBaseCreditType rbct: rbctlist) {
            map.put(rbct.getId(),rbct);
        }

        for(RmBaseCreditInitial rbcit: rbcilist) {
           map.get(rbcit.getTypeId()).getRbcilist().add(rbcit);
        }
        return R.newOK(map);
    }


    /**
     * @Description: 初始化授信条件查询
     * @Param: [request] 
     * @return: com.tcxf.hbc.common.util.R 
     * @Author: JinPeng
     * @Date: 2018/9/20 
    */ 
    @RequestMapping("/getCreditInitialListById")
    public R getCreditInitialListById(HttpServletRequest request, String fid){

        String id = request.getParameter("id");

        //根据id查询初始授信版本id
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", id));

        RmBaseCreditInitial rbci = new RmBaseCreditInitial();
        if (!ValidateUtil.isEmpty(request.getParameter("typeId"))){
            rbci.setTypeId(request.getParameter("typeId"));
        }
        List<RmBaseCreditInitial> rbcilist = iRmBaseCreditInitialService.selectList(new EntityWrapper<RmBaseCreditInitial>().eq("typeId", request.getParameter("typeId")).eq("versionId", dqlist.getSxInitId()));
        return R.newOK(rbcilist);
    }


    /** 
     * @Description: 保存修改模板 
     * @Param: [request] 
     * @return: com.tcxf.hbc.common.util.R 
     * @Author: JinPeng
     * @Date: 2018/9/20 
    */ 
    @RequestMapping("/saveCreditInitial")
    public R saveCreditInitial(HttpServletRequest request){
        List<Object> list=new ArrayList <>();
        iRmBaseCreditInitialService.createRmBaseCreditInitial(null, null);
        return R.newOK();
    }

    /**
     * @Description:  授信提额条件列表
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/getRmBaseCreditUpmoneyList")
    public R getRmBaseCreditUpmoneyList(HttpServletRequest request){
        String fid = (String) get(getRequest(),"session_fund_id");
        String id = request.getParameter("id");

        //根据id查询初始授信版本id
        RmSxtemplateRelationship rmSxtemplateRelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", id));

        List<RmBaseCreditUpmoney> rbcilist = iRmBaseCreditUpmoneyService.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("versionId", rmSxtemplateRelationship.getSxUpmoneyId()));
        //查询授信类别列表
        List<RmBaseCreditType> rbctlist = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().eq("status",RmBaseCreditType.TYPE_1));
        //封装数据

        List<Object> list = new ArrayList<>();
        list.addAll(rbcilist);
        list.addAll(rbctlist);
        return R.newOK(list);
    }


    /**
     * @Description: 授信提额修改查询
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/getRmBaseCreditUpmoneyListById")
    public R getRmBaseCreditUpmoneyListById(HttpServletRequest request){
        String fid = (String) get(getRequest(),"session_fund_id");

        String id = request.getParameter("id");

        //根据id查询初始授信版本id
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", id));

        RmBaseCreditUpmoney rbci = new RmBaseCreditUpmoney();
        if (!ValidateUtil.isEmpty(request.getParameter("typeId"))){
            rbci.setTypeId(request.getParameter("typeId"));
        }
        List<RmBaseCreditUpmoney> rbcilist = iRmBaseCreditUpmoneyService.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("typeId", request.getParameter("typeId")).eq("versionId", dqlist.getSxUpmoneyId()));
        return R.newOK(rbcilist);
    }

    /**
     * @Description: 保存授信提额修改模板
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/saveRmBaseCreditUpmoney")
    public R saveRmBaseCreditUpmoney(@RequestBody List<RmBaseCreditUpmoney> list){
        String fid = (String) get(getRequest(),"session_fund_id");
        iRmBaseCreditUpmoneyService.createRmBaseCreditUpmoney(list,fid);
        return R.newOK();
    }



    /**
     * @Description: 跳转评分自定义主页
     * @Param: [request]
     * @return: org.springframework.web.servlet.ModelAndView
     * @Author: JinPeng
     * @Date: 2018/10/19
    */
    @RequestMapping("/goCreditCustomIndexView")
    public ModelAndView goCreditCustomIndexView(HttpServletRequest request){
        ModelAndView m = new ModelAndView();

        String fid = (String) get(getRequest(),"session_fund_id");

        //克隆版本
        RmSxtemplateRelationship kllist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_NO).eq("fid", fid));
        //当前版本
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_YES).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).eq("fid", fid));
        //历史版本
        List<RmSxtemplateRelationship> lslist = iRmSxtemplateRelationshipService.selectList(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_NO).eq("fid", fid).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).orderBy("publish_date",false));
        m.addObject("kllist", kllist);
        m.addObject("dqlist", dqlist);
        m.addObject("lslist", lslist);
//        m.setViewName("rm/zidingyi");
        m.setViewName("rm/score_customization_index");
        return m;
    }

    /**
     * 克隆大版本
     * @return
     */
    @RequestMapping("/cloneRmSxtemplateRelationship")
    public R cloneRmSxtemplateRelationship(@RequestParam("id") String  id) {
        String fid = (String) get(getRequest(),"session_fund_id");
        String relationshipid = iRmSxtemplateRelationshipService.cloneRmSxtemplateRelationship(id,fid);
        return R.newOK(relationshipid);
    }

    /**
     * 删除未发布的大版本
     * @param id
     * @return
     */
    @RequestMapping("/deleteRmSxtemplateRelationship")
    public R deleteRmSxtemplateRelationship(@RequestParam("id") String id) {

        iRmSxtemplateRelationshipService.deleteRmSxtemplateRelationshipById(id);
        return R.newOK();
    }

    /**
     * 发布大版本
     * @param id
     * @return
     */
    @RequestMapping("/updateStatus")
    public R updateStatus(@RequestParam("id") String id) {
        String fid = (String) get(getRequest(),"session_fund_id");
        RmSxtemplateRelationship rmSxtemplateRelationship = new RmSxtemplateRelationship();
        rmSxtemplateRelationship.setId(id);
        rmSxtemplateRelationship.setFid(fid);
        iRmSxtemplateRelationshipService.updateStatus(rmSxtemplateRelationship);
        return R.newOK();
    }


}
