package com.tcxf.hbc.admin.controller.rmcontroller;

import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IRmBaseCreditInitialService;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liaozeyong
 * @Date: 2018/9/26 11:13
 * @Description:
 */
@RestController
@RequestMapping("/fund/InitialCredit")
public class FInitialCreditController extends BaseController {

    @Autowired
    private CreditCustomController creditCustomControllerl;
    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;
    /**
     * 跳转到评分自定义初始授信界面
     * @return
     */
    @RequestMapping("/goChushiSx")
    public ModelAndView goChushiSx(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("fund/chushiSx");
        String type = ValidateUtil.isEmpty(request.getParameter("type")) ? "" : request.getParameter("type");
        String id = request.getParameter("id");
        R r =creditCustomControllerl.getCreditInitialList(request , get(request , "session_fund_id").toString(), id);
        Map map = (Map) r.getData();


        modelAndView.addObject("id", id);
        modelAndView.addObject("type", type);
        modelAndView.addObject("map",map);
        return modelAndView;
    }

    /**
     * 动态加载修改评分自定义初始授信数据
     * @return
     */
    @RequestMapping("/goUpdateInitialCredit")
    public R goUpdateInitialCredit(HttpServletRequest request)
    {
        R r = creditCustomControllerl.getCreditInitialListById(request , get(request , "session_fund_id").toString());
        return r;
    }

    /**
     * 提交初始授信
     * @return
     */
    @RequestMapping("/commitInitialCredit")
    public R commitInitialCredit(@RequestBody List<RmBaseCreditInitial> list)
    {
        String fid = (String) get(getRequest(),"session_fund_id");
        iRmBaseCreditInitialService.createRmBaseCreditInitial(list , fid);
        return R.newOK();
    }
}
