package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.RepaymentDto;
import com.tcxf.hbc.admin.service.IPNoticeService;
import com.tcxf.hbc.admin.service.ITbRepaymentPlanService;
import com.tcxf.hbc.common.entity.PNotice;
import com.tcxf.hbc.common.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @Auther: liaozeyong
 * @Date: 2018/9/18 17:08
 * @Description:
 */
@RestController
@RequestMapping("/fund/RMRepaymentPlanDetail")
public class FRepaymentPlanDetailController extends BaseController {

    @Autowired
    private ITbRepaymentPlanService iTbRepaymentPlanService;
    @Autowired
    private IPNoticeService ipNoticeService;

    @RequestMapping("/goBeAlsoDetailed")
    public ModelAndView goBeAlsoDetailed()
    {
        ModelAndView modelAndView = new ModelAndView("fund/beAlsoDetailed");
        return modelAndView;
    }

    /**
     * 风控本月应还明细
     * @return
     */
    @RequestMapping("/RepaymentPlanDetail")
    public R RepaymentPlanDetail(HttpServletRequest request)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String str = (String) paramMap.get("nameOrMobile");
        if (!ValidateUtil.isEmpty(str))
        {
            if (str.matches("[0-9]{1,}"))
            {
                paramMap.put("mobile",str);
            }else {
                paramMap.put("realName",str);
            }
        }
        Page page = iTbRepaymentPlanService.repaymentPlanDetail(new Query<RepaymentDto>(paramMap));
        return R.newOK(page);
    }

    /**
     * 记录还款通知时间记录
     * @param threeDay
     * @param oneDay
     * @return
     */
    @RequestMapping("/RecordNotice")
    public R RecordNotice(String threeDay , String oneDay , String id)
    {
        try {
            if (ValidateUtil.isEmpty(threeDay)&&ValidateUtil.isEmpty(oneDay)){
                return R.newOK();
            }
            PNotice pNotice = new PNotice();
            if (ValidateUtil.isEmpty(threeDay)){
                pNotice.setOneDay(oneDay);
                pNotice.setPlanId(id);
            }else {
                pNotice.setThreeDay(threeDay);
                pNotice.setPlanId(id);
            }
            pNotice.setCreateTime(new Date());
            boolean b = ipNoticeService.insert(pNotice);
            return R.newOK();
        }catch (Exception e) {
            logger.error("cfp 记录还款通知时间记录异常"+e);
            return R.newErrorMsg("操作异常");
        }
    }

    /**
     * 查询联系状态
     * liaozeyong
     * @param
     * @return
     */
    @RequestMapping("/selectNoticeByThreeDay")
    public R selectNoticeByThreeDay(HttpServletRequest request)
    {
        try{
            String id = request.getParameter("id");
            List<PNotice> pNoticeList = ipNoticeService.selectList(new EntityWrapper<PNotice>().eq("plan_id",id).isNotNull("three_day"));
            return R.newOK(pNoticeList);
        }catch (Exception e){
            logger.error("cfp 查询联系状态异常"+e);
            return R.newErrorMsg("操作异常");
        }
    }

    @RequestMapping("/selectNoticeByOneDay")
    public R selectNoticeByOneDay(HttpServletRequest request)
    {
        try{
            String id = request.getParameter("id");
            List<PNotice> pNoticeList = ipNoticeService.selectList(new EntityWrapper<PNotice>().eq("plan_id",id).isNotNull("one_day"));
            return R.newOK(pNoticeList);
        }catch (Exception e){
            logger.error("cfp 查询联系状态异常"+e);
            return R.newErrorMsg("操作异常");
        }
    }

    /**
     * 导出风控本月应还明细
     */
    @RequestMapping("/excelRepaymentPlan")
    public void excelRepaymentPlan(HttpServletRequest request, HttpServletResponse response)
    {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        List<RepaymentDto> repaymentDtos= iTbRepaymentPlanService.excelRepaymentPlanDetail(paramMap);
        String[] title = {"","流水号", "姓名","手机号", "使用金额(元)", "最低还款金额 (元)", "还款状态","前3日电话提醒","前1日电话提醒"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (RepaymentDto repaymentDto : repaymentDtos) {
            String[] strings = new String[9];
            strings[0] = i + "";
            strings[1] = repaymentDto.getSerialNo();
            strings[2] = repaymentDto.getRealName();
            strings[3] = repaymentDto.getMobile();
            strings[4] = repaymentDto.getTotalCorpus().toString();
            strings[5] = repaymentDto.getRepaymentSum();
            if ("1".equals(repaymentDto.getStatus()))
            {
                strings[6] = "已还";
            }else {
                strings[6] = "未还";
            }
            if ("0".equals(repaymentDto.getThreeDay())) {
                strings[7] = "未接通";
            }else if ("1".equals(repaymentDto.getThreeDay())) {
                strings[7] = "已通知";
            }else if ("2".equals(repaymentDto.getThreeDay())) {
                strings[7] = "占线";
            }else if ("3".equals(repaymentDto.getThreeDay())) {
                strings[7] = "空号";
            }else if ("4".equals(repaymentDto.getThreeDay())) {
                strings[7] = "停机";
            }else if (ValidateUtil.isEmpty(repaymentDto.getThreeDay())) {
                strings[7] = "未通知";
            }
            if ("0".equals(repaymentDto.getOneDay())) {
                strings[8] = "未接通";
            } else if ("1".equals(repaymentDto.getOneDay())) {
                strings[8] = "已通知";
            } else if ("2".equals(repaymentDto.getOneDay())) {
                strings[8] = "占线";
            } else if ("3".equals(repaymentDto.getOneDay())) {
                strings[8] = "空号";
            } else if ("4".equals(repaymentDto.getOneDay())) {
                strings[8] = "停机";
            } else if (ValidateUtil.isEmpty(repaymentDto.getOneDay())) {
                strings[8] = "未通知";
            }
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "本月应还明细记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }
}
