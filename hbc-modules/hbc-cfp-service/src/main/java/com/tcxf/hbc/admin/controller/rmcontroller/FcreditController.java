package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.CreditPageInfoDto;
import com.tcxf.hbc.admin.model.dto.TbrmRepayDto;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 15:01 2018/9/17
 * 用户授信模块代码
 */

@RestController
@RequestMapping("/fund/FriskControl")
public class FcreditController extends BaseController{

    @Autowired
    private  ICUserInfoService iCUserInfoService;

    @Autowired
    private ITbCreditGradesService iTbCreditGradeService;

    @Autowired
    private ITbUpMoneyService iTbUpMoneyService;

    @Autowired
    private ICUserRefConService iCUserRefConService;

    @Autowired
    private ITbRepaymentPlanService iTbRepaymentPlanService;

     private final  String SOCIALINSURANCE = "socialInsurance";//社保认证

     private final  String  ACCUMULATIONFUND = "accumulationFund";//公积金认证

     private final  String EDUCATIONAPPROVE = "educationApprove";//学历认证

     private final  String TAOBAOAPPROVE = "taobaoApprove";//淘宝认证

     private final  String JDCOMAPPROVE = "jDcomApprove";//京东认证

     private final  String DIDIDACHEAPPROVE = "didiDacheApprove";//滴滴认证

     private final  String MUTIPLESCORE = "mutipleScore";//多头负债

     private final  String DEBTCALLSCORE = "debtCallScore";//催收电话
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:15 2018/9/17
     * 跳转用户授信明细页面
    */
    @GetMapping("/jumpCuserCredit")
    public ModelAndView jumpCuserCredit()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/f_user_detail");
        return mode;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:01 2018/9/17
     * 查询用户授信明细(已授信,已拒绝)
    */
    @RequestMapping("/PageInfo")
    public R findCuserCreditPageInfo(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String fid = (String) get(request,"session_fund_id");
        //String fid = "1029292115160596481";
        paramMap.put("fid",fid);
        Page paybackPage= iCUserInfoService.findCuserCreditPageInfo(new Query<CreditPageInfoDto>(paramMap));
        R r = new R<>();
        r.setData(paybackPage);
        return r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:10 2018/9/17
     * 详情-初始授信评分
    */
    @RequestMapping("/CreditInfo")
    public R findCreditInfo(HttpServletRequest request){
        String uid = request.getParameter("uid");
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        HashMap<String, Object> resultMap = new HashMap<>();
        R r = new R<>();

        //初始授信数据
        List<TbCreditGrade> creditGradeList = iTbCreditGradeService.selectList(new EntityWrapper<TbCreditGrade>().eq("uid", uid));

        BigDecimal totalGrade = new BigDecimal("0");        //总分数
        BigDecimal initGrade = new BigDecimal("0");         //初始授信分数
        BigDecimal upmoneyGrade = new BigDecimal("0");      //授信提额分数

        //计算初始授信分数
        if(creditGradeList!=null && creditGradeList.size()>0){
            for (TbCreditGrade tbCreditGrade:creditGradeList) {
                if(tbCreditGrade.getCode().equals(DEBTCALLSCORE)){
                    initGrade=initGrade.subtract(new BigDecimal(tbCreditGrade.getValue()));
                }else{
                    initGrade=initGrade.add(new BigDecimal(tbCreditGrade.getValue()));
                }
            }
        }

        //提额中分数 code的参数
        String[] arr={SOCIALINSURANCE,ACCUMULATIONFUND,EDUCATIONAPPROVE,TAOBAOAPPROVE,JDCOMAPPROVE,DIDIDACHEAPPROVE};

        //授信提额数据
        List<TbUpMoney> tbUpMoneyList = iTbUpMoneyService.selectList(new EntityWrapper<TbUpMoney>().eq("uid", uid).in("code",arr));

        //计算授信提额分数
        if(tbUpMoneyList!=null && tbUpMoneyList.size()>0){
            for (TbUpMoney tbUpMoney:tbUpMoneyList) {
                String value = tbUpMoney.getValue();
                tbUpMoney.setValue(value==null ?"0":value);
                upmoneyGrade=upmoneyGrade.add(new BigDecimal(tbUpMoney.getValue()));
            }
        }

        //计算授信总分数
        totalGrade=initGrade.add(upmoneyGrade);

        //被拒绝数据
        CUserRefCon userRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid", uid));

        //还款详情数据
        List<TbrmRepayDto> tbRepaymentPlanList = iTbRepaymentPlanService.findUserPaymentList(paramMap);

        resultMap.put("creditGradeList",creditGradeList);
        resultMap.put("tbUpMoneyList",tbUpMoneyList);
        resultMap.put("userRefCon",userRefCon);
        resultMap.put("tbRepaymentPlanList",tbRepaymentPlanList);
        resultMap.put("totalGrade",totalGrade);
        resultMap.put("initGrade",initGrade);
        resultMap.put("upmoneyGrade",upmoneyGrade);
        r.setData(resultMap);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:10 2018/9/17
     * 详情-授信提额评分
    */
    @RequestMapping("/upCreditGradeInfo")
    public R finUpCreditGradeInfo(HttpServletRequest request){
        String uid = request.getParameter("uid");
        List<TbUpMoney> tbUpMoneyList = iTbUpMoneyService.selectList(new EntityWrapper<TbUpMoney>().eq("uid", uid));
        R r = new R<>();
        r.setData(tbUpMoneyList);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:10 2018/9/17
     * 详情-被拒绝情况
    */
    @RequestMapping("/refuseCreditInfo")
    public R findRefuseCreditInfo(HttpServletRequest request){
        String uid = request.getParameter("uid");
        CUserRefCon userRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid", uid));
        R r = new R<>();
        r.setData(userRefCon);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:10 2018/9/17
     * 详情-还款状态详情
    */
    @RequestMapping("/payBackMoneyInfo")
    public R findPayBackMoneyInfo(HttpServletRequest request){
        String uid = request.getParameter("uid");
        List<TbRepaymentPlan> tbRepaymentPlanList = iTbRepaymentPlanService.selectList(new EntityWrapper<TbRepaymentPlan>().eq("uid", uid));
        R r = new R<>();
        r.setData(tbRepaymentPlanList);
        return r;
    }



}
