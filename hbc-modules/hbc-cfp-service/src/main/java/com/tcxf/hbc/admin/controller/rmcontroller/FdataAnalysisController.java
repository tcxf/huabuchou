package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.AnalysisDto;
import com.tcxf.hbc.admin.model.dto.RmBaseCreditInitialDto;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 13:50 2018/9/26
 */

@RestController
@RequestMapping("/fund/fAnalysis")
public class FdataAnalysisController extends BaseController{

    @Autowired
    private IRmBaseCreditTypeService iRmBaseCreditTypeService;

    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IFFundendBindService iFFundendBindService;

    @Autowired
    private ITbCreditGradesService iTbCreditGradesService;

    @Autowired
    private CreditScoreService creditScoreService;

    @Autowired
    private ICUserRefConService iCUserRefConService;

    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;

    @Autowired
    private RefusingCreditService refusingCreditService;
    /*加载标识:性别、年龄、地区、多头负债、评分段、被拒绝类型 */
    private final String AGE_SCORE="ageScore";      //模板-年龄基础分
    private final String MODULE_SCORE="moduleScore";  //模板-多头负债模型评分

    private final String ADDRESS_SCORE="addressScore"; //模板-户籍基础分
    private final String SEX_SCORE="sexScore";       //模板-性别基础分

    private final String SCORING_SEGMENT="ScoringSegment";       //模板-性别基础分
    private final String REFUSING_CREDIT="refusingCredit";       //模板-拒绝授信标识

    private final String TITLE_SCORE="titleScore";   //详单非标签类号码数量
    private final String MOBILE_SCORE="mobileScore"; //催收类别号码数量

    /*tb_credit_grade code对应 type表中code*/
    private final String MUTIPLE_SCORE="mutipleScore";  //多头负债模型评分--授信评分标识

    /*拒绝授信*/
    private final  String OVERDUECHECK = "overduecheck";//逾期平台条件
    private final  String OVERDUENUMCHECK = "overduenumcheck";//逾期次数条件
    private final  String LIABILITIENUMCHECK = "liabilitienumcheck";//多头负债平台注册个数条件

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 13:50 2018/9/26
     * 跳转数据分析页面
     */
    @GetMapping("/jumpDataAnalysis")
    public ModelAndView jumpDataAnalysis()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("fund/f_data_analysis");
        return mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:54 2018/9/26
     * 查询数据分析的分类 查询typeid
    */
    @RequestMapping("/categorys")
    public R findAnalysisCategory(){
        //提额中分数 code的参数
        String[] arr={SEX_SCORE,AGE_SCORE,MODULE_SCORE,ADDRESS_SCORE,SCORING_SEGMENT,REFUSING_CREDIT};
        List<RmBaseCreditType> typeList = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().in("code",arr).orderBy("create_date",true));
        R r = new R<>();
        r.setData(typeList);
        return r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:06 2018/9/27
     * 默认值:为typeList中的第一个
    */
    @RequestMapping("/firstType")
    public R findfirstType(){
        //提额中分数 code的参数
        String[] arr={SEX_SCORE,AGE_SCORE,MODULE_SCORE,ADDRESS_SCORE,SCORING_SEGMENT,REFUSING_CREDIT};
        List<RmBaseCreditType> typeList = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().in("code",arr).orderBy("create_date",true));
        String typeId ="";
        if(typeList !=null && typeList.size()>0){
           RmBaseCreditType rmBaseCreditType = typeList.get(0);
            typeId = rmBaseCreditType.getId();
       }
        R r = new R<>();
        r.setData(typeId);
        return r;
    }



    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 13:54 2018/9/26
     * 根据typeid以及版本号id查询具体类别数据  渲染人数图形
     */
    @RequestMapping("/categorysinfo")
    public R findAnalysisDataInfoBycidvid(HttpServletRequest request){
        String typeId = request.getParameter("typeId");
        String fid = (String) get(request,"session_fund_id");
        //String fid = "1029292115160596481";
        //根据资金端id查询版本号
        String versionId ="";
        //RefusingVersion refusingCredit = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("fId", fid).eq("type", "2").eq("status", "1"));

        RmSxtemplateRelationship rmSxtemplateRelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("status", "1").eq("is_publish", "1").eq("fid",fid));
        String sxInitId = rmSxtemplateRelationship.getSxInitId();//初始授信版本id

        if(rmSxtemplateRelationship!=null){
             versionId =rmSxtemplateRelationship.getId();
        }
        //根据版本号以及typeid   查询每个具体分类阶段人数  :例如 年龄c_user_ref_con20-25  最大值25 最小值20 人数10人
        // List<RmBaseCreditInitial> list = iRmBaseCreditInitialService.selectList(new EntityWrapper<RmBaseCreditInitial>().eq("typeId", typeId).eq("versionId", versionId));
        HashMap<String, Object> paramMap = new HashMap<>();
        HashMap<Object, Object> resultMap = new HashMap<>();

        ArrayList<AnalysisDto> dtoArrayList = new ArrayList<>();
        ArrayList<AnalysisDto> overdueList = new ArrayList<>();
        ArrayList<AnalysisDto> overdueMoneyList = new ArrayList<>();
        ArrayList<String> nameList = new ArrayList<>();

        paramMap.put("typeId",typeId);
        paramMap.put("versionId",sxInitId);

        //当typeId是评分段 -----------已添加版本号筛选         ----------------------版本id需满足主版本id
        if(typeId.equals(findGradingSegmentId())){

            Map<String, Object> map = new HashMap<>();
            map.put("fid", fid);
            map.put("id", versionId);

            //查询每个授信用户的总分数
            List<CreditScore> list = creditScoreService.selectCreditScoreByfId(map);
            HashMap<String, Object> peoplePieMap = getGradingSegmentpeoplePieData(list, dtoArrayList,overdueList,overdueMoneyList, nameList,versionId,fid);
            resultMap.put("valueList", peoplePieMap.get("valueList"));
            resultMap.put("overdueList", peoplePieMap.get("overdueList"));
            resultMap.put("overdueMoneyList", peoplePieMap.get("overdueMoneyList"));
            resultMap.put("nameList",peoplePieMap.get("nameList"));
        }
        //当typeId是被拒绝类型                                 ----------------------list需要动态查询 并且 版本id需满足主版本id
        else if(typeId.equals(findInnerRefusingTypeId())){

            //查询CUserRefCon拒绝授信类型 对应的人数
            HashMap<String, Object> refusingpeoplePieMap = getrefusingCreditPeopleNumPieData(dtoArrayList, nameList,versionId,fid);
            resultMap.put("valueList", refusingpeoplePieMap.get("valueList"));
            resultMap.put("nameList",refusingpeoplePieMap.get("nameList"));
        }
        //当typeId是性别 年龄 多头负债 地区 ------------------已添加版本号筛选     ----------------------版本id需满足主版本id 并且需要传递 初始化授信版本号
        else{
            List<RmBaseCreditInitialDto> list = iRmBaseCreditInitialService.findBaseCreditInitialInfo(paramMap);
            HashMap<String, Object> peoplePieMap = getpeoplePieData(list, dtoArrayList, nameList,versionId);
            HashMap<String, Object> overDuePieMap = getoverDuePieData(list, overdueList,versionId,fid);
            HashMap<String, Object> overdueMoneyPieMap = getoverDueMoneyPieData(list, overdueMoneyList,versionId,fid);
            resultMap.put("valueList", peoplePieMap.get("valueList"));
            resultMap.put("nameList",peoplePieMap.get("nameList"));
            resultMap.put("overdueList",overDuePieMap.get("overdueList"));
            resultMap.put("overdueMoneyList",overdueMoneyPieMap.get("overdueMoneyList"));
        }
        R r = new R<>();
        r.setData(resultMap);
        return r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:02 2018/9/29
     * 内部接口:查询评分段的typeId
    */
    private  String  findGradingSegmentId() {
        RmBaseCreditType rmBaseCreditType = iRmBaseCreditTypeService.selectOne(new EntityWrapper<RmBaseCreditType>().eq("code", SCORING_SEGMENT));
        return  rmBaseCreditType.getId();
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:02 2018/9/29
     * 内部接口:查询拒绝授信类型的typeId
     */
    public  String  findInnerRefusingTypeId() {
        RmBaseCreditType rmBaseCreditType = iRmBaseCreditTypeService.selectOne(new EntityWrapper<RmBaseCreditType>().eq("code", REFUSING_CREDIT));
        return rmBaseCreditType.getId();
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:12 2018/9/29
     * 查询被拒绝类型的typeId
    */
    @RequestMapping("/findRefusingTypeId")
    public  R  findRefusingTypeId() {
        R r = new R<>();
        RmBaseCreditType rmBaseCreditType = iRmBaseCreditTypeService.selectOne(new EntityWrapper<RmBaseCreditType>().eq("code", REFUSING_CREDIT));
        r.setData(rmBaseCreditType.getId());
        return r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:05 2018/9/27
     * 人数占比统计   ------------------------已添加版本号筛选
    */
    private  HashMap<String, Object>   getpeoplePieData( List<RmBaseCreditInitialDto> list,ArrayList<AnalysisDto> dtoArrayList,ArrayList<String> nameList,String versionId){
        HashMap<String, Object> resultMap = new HashMap<>();

        if(list.size()>0 && list !=null){
            for (int i = 0; i < list.size(); i++) {
                //当最小值 和最大值是空 求对应的人数
                AnalysisDto analysisDto = new AnalysisDto();
                Map<String, Object> paramMap = new HashMap<>();
                if (ValidateUtil.isEmpty(list.get(i).getMinScore())){
                    //int details = iTbCreditGradesService.selectCount(new EntityWrapper<TbCreditGrade>().eq("details", list.get(i).getName()).eq("version_id",versionId).eq("sx_state",1));
                    paramMap.put("details",list.get(i).getName());
                    paramMap.put("versionId",versionId);
                    int details = iTbCreditGradesService.findcountByDetailNameAndVersion(paramMap);

                    analysisDto.setName(list.get(i).getName());
                    analysisDto.setValue(new BigDecimal(details));
                    analysisDto.setSort(i);
                    dtoArrayList.add(analysisDto);
                    nameList.add(list.get(i).getName());
                }else{
                    //多头负债
                    if(list.get(i).getTypeCode().equals(MODULE_SCORE)){
                       // int number = iTbCreditGradesService.selectCount(new EntityWrapper<TbCreditGrade>().between("details", list.get(i).getMinScore(), list.get(i).getMaxScore()).eq("version_id",versionId).eq("code",MUTIPLE_SCORE).eq("sx_state",1));
                        paramMap.put("MinScore",list.get(i).getMinScore());
                        paramMap.put("MaxScore",list.get(i).getMaxScore());
                        paramMap.put("versionId",versionId);
                        paramMap.put("code",MUTIPLE_SCORE);
                        int number = iTbCreditGradesService.findcountByDetailNumAndVersion(paramMap);

                        analysisDto.setName(list.get(i).getName());
                        analysisDto.setValue(new BigDecimal(number));
                        analysisDto.setSort(i);
                        dtoArrayList.add(analysisDto);
                        nameList.add(list.get(i).getName());
                    }
                    //年龄
                    if(list.get(i).getTypeCode().equals(AGE_SCORE)){
                        if(!ValidateUtil.isEmpty(list.get(i).getMinScore()) && !ValidateUtil.isEmpty(list.get(i).getMaxScore()) ){
                            //int number = iTbCreditGradesService.selectCount(new EntityWrapper<TbCreditGrade>().between("details", list.get(i).getMinScore(), list.get(i).getMaxScore()).eq("version_id",versionId).eq("code",AGE_SCORE).eq("sx_state",1));
                            paramMap.put("MinScore",list.get(i).getMinScore());
                            paramMap.put("MaxScore",list.get(i).getMaxScore());
                            paramMap.put("versionId",versionId);
                            paramMap.put("code",AGE_SCORE);
                            int number = iTbCreditGradesService.findcountByDetailNumAndVersion(paramMap);

                            analysisDto.setValue(new BigDecimal(number));
                        }else{
                            //int number = iTbCreditGradesService.selectCount(new EntityWrapper<TbCreditGrade>().ge("details", list.get(i).getMinScore()).eq("version_id",versionId).eq("code",AGE_SCORE).eq("sx_state",1));

                            paramMap.put("MinScore",list.get(i).getMinScore());
                            paramMap.put("versionId",versionId);
                            paramMap.put("code",AGE_SCORE);
                            int number =  iTbCreditGradesService.findcountByDetailMinNumAndVersion(paramMap);
                            analysisDto.setValue(new BigDecimal(number));
                        }
                        analysisDto.setName(list.get(i).getName());
                        analysisDto.setSort(i);
                        dtoArrayList.add(analysisDto);
                        nameList.add(list.get(i).getName());
                    }
                }
            }
        }
        resultMap.put("valueList",dtoArrayList);
        resultMap.put("nameList",nameList);
        return resultMap;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:58 2018/9/27
     * 加载违约人数饼状图数据 ------------------------已添加版本号筛选
    */
    private  HashMap<String, Object>  getoverDuePieData( List<RmBaseCreditInitialDto> list,ArrayList<AnalysisDto> overdueList,String versionId,String fid){
        HashMap<String, Object> resultMap = new HashMap<>();
        HashMap<String, Object> paramMap = new HashMap<>();
        if(list.size()>0 && list !=null){
            for (int i = 0; i < list.size(); i++) {
                AnalysisDto analysisDto = new AnalysisDto();
                if (ValidateUtil.isEmpty(list.get(i).getMinScore())){
                    paramMap.put("details",list.get(i).getName());
                    paramMap.put("versionId",versionId);
                    paramMap.put("fid",fid);
                    int details =  iRmBaseCreditInitialService.findNumberOverDueByDetail(paramMap);
                    analysisDto.setName(list.get(i).getName());
                    analysisDto.setValue(new BigDecimal(details));
                    analysisDto.setSort(i);
                    overdueList.add(analysisDto);
                }else{
                    //多头负债
                    paramMap.put("minScore",list.get(i).getMinScore());
                    paramMap.put("maxScore",list.get(i).getMaxScore());
                    paramMap.put("versionId",versionId);
                    paramMap.put("fid",fid);
                    int details=0;
                    if(list.get(i).getTypeCode().equals(MODULE_SCORE)){
                        paramMap.put("code",MUTIPLE_SCORE);
                         details =  iRmBaseCreditInitialService.findNumberOverDueByInterval(paramMap);
                    }
                    //年龄
                    if(list.get(i).getTypeCode().equals(AGE_SCORE)){
                        paramMap.put("code",AGE_SCORE);
                         details =  iRmBaseCreditInitialService.findNumberOverDueByInterval(paramMap);
                    }
                    analysisDto.setName(list.get(i).getName());
                    analysisDto.setValue(new BigDecimal(details));
                    analysisDto.setSort(i);
                    overdueList.add(analysisDto);
                }
            }
        }

        resultMap.put("overdueList",overdueList);
        return resultMap;

    }
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:58 2018/9/27
     * 加载违约金额饼状图数据 ------------------------已添加版本号筛选
    */
    private  HashMap<String, Object>  getoverDueMoneyPieData( List<RmBaseCreditInitialDto> list,ArrayList<AnalysisDto> overdueMoneyList,String versionId,String fid){
        HashMap<String, Object> resultMap = new HashMap<>();
        HashMap<String, Object> paramMap = new HashMap<>();
        if(list.size()>0 && list !=null){
            for (int i = 0; i < list.size(); i++) {
                AnalysisDto analysisDto = new AnalysisDto();
                if (ValidateUtil.isEmpty(list.get(i).getMinScore())){
                    paramMap.put("details",list.get(i).getName());
                    paramMap.put("versionId",versionId);
                    paramMap.put("fid",fid);
                    BigDecimal thisTotalMoney =  iRmBaseCreditInitialService.findNumberOverDueMoneyByDetail(paramMap);
                    analysisDto.setName(list.get(i).getName());
                    analysisDto.setValue(thisTotalMoney);
                    analysisDto.setSort(i);
                    overdueMoneyList.add(analysisDto);
                }else{
                    //多头负债
                    paramMap.put("minScore",list.get(i).getMinScore());
                    paramMap.put("maxScore",list.get(i).getMaxScore());
                    paramMap.put("versionId",versionId);
                    paramMap.put("fid",fid);
                    BigDecimal thisTotalMoney = new BigDecimal(0);
                    if(list.get(i).getTypeCode().equals(MODULE_SCORE)){
                        paramMap.put("code",MUTIPLE_SCORE);
                        thisTotalMoney =  iRmBaseCreditInitialService.findNumberOverDueMoneyByInterval(paramMap);
                    }
                    //年龄
                    if(list.get(i).getTypeCode().equals(AGE_SCORE)){
                        paramMap.put("code",AGE_SCORE);
                        thisTotalMoney =  iRmBaseCreditInitialService.findNumberOverDueMoneyByInterval(paramMap);
                    }
                    analysisDto.setName(list.get(i).getName());
                    analysisDto.setValue(thisTotalMoney);
                    analysisDto.setSort(i);
                    overdueMoneyList.add(analysisDto);
                }
            }
        }
        resultMap.put("overdueMoneyList",overdueMoneyList);
        return resultMap;
    }




    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 16:05 2018/9/27
     * 评分段 人数占比 违约人数 违约金额 统计 ------------------------已添加版本号筛选
     */
    private  HashMap<String, Object>   getGradingSegmentpeoplePieData( List<CreditScore> list,ArrayList<AnalysisDto> GradingSegmentValueList,ArrayList<AnalysisDto> overDuePeopleNumList,ArrayList<AnalysisDto> overDueMoneyList,ArrayList<String> nameList,String versionId,String fid){
        HashMap<String, Object> resultMap = new HashMap<>();
        HashMap<String, Object> paramMap = new HashMap<>();

        if(list.size()>0 && list !=null){
            for (int i = 0; i < list.size(); i++) {
                //当最小值 和最大值是空 求对应的人数
                AnalysisDto analysisDto = new AnalysisDto();
                AnalysisDto overDuePeople = new AnalysisDto();
                AnalysisDto overDueMoney = new AnalysisDto();
                paramMap.put("minScore",list.get(i).getMinScore());
                paramMap.put("maxScore",list.get(i).getMaxScore());
                paramMap.put("versionId",versionId);
                paramMap.put("fid",fid);

                //授信人数饼图数据
                int thisPeopleNum=iRmBaseCreditInitialService.findpeopleNumByGradingSegment(paramMap);
                nameList.add(list.get(i).getMinScore()+"-"+list.get(i).getMaxScore());
                analysisDto.setValue(new BigDecimal(thisPeopleNum));
                analysisDto.setName(list.get(i).getMinScore()+"-"+list.get(i).getMaxScore());
                analysisDto.setSort(i);
                GradingSegmentValueList.add(analysisDto);

                //违约人数饼图数据
                int thisoverDuePeopleNum=iRmBaseCreditInitialService.findoverDueNumByGradingSegment(paramMap);
                overDuePeople.setValue(new BigDecimal(thisoverDuePeopleNum));
                overDuePeople.setName(list.get(i).getMinScore()+"-"+list.get(i).getMaxScore());
                overDuePeople.setSort(i);
                overDuePeopleNumList.add(overDuePeople);

                //违约金额饼图数据
                BigDecimal thisoverDueMoney=iRmBaseCreditInitialService.findoverDueMoneyByGradingSegment(paramMap);
                overDueMoney.setValue(thisoverDueMoney == null ? new BigDecimal(0):thisoverDueMoney);
                overDueMoney.setName(list.get(i).getMinScore()+"-"+list.get(i).getMaxScore());
                overDueMoney.setSort(i);
                overDueMoneyList.add(overDueMoney);
            }
        }

        resultMap.put("valueList",GradingSegmentValueList);
        resultMap.put("overdueList",overDuePeopleNumList);
        resultMap.put("overdueMoneyList",overDueMoneyList);
        resultMap.put("nameList",nameList);
        return resultMap;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:19 2018/9/30
     * 拒绝授信类型对应的人数  ------------------------已添加版本号筛选
    */
    private  HashMap<String, Object>   getrefusingCreditPeopleNumPieData(ArrayList<AnalysisDto> fusingCreditPeopleList,ArrayList<String> nameList,String versionId,String fid){
        HashMap<String, Object> resultMap = new HashMap<>();

        String[] arr={OVERDUECHECK,OVERDUENUMCHECK,LIABILITIENUMCHECK};
        List<RefusingCredit> credits = refusingCreditService.selectList(new EntityWrapper<RefusingCredit>().in("code", arr).groupBy("code"));

        for (RefusingCredit refusingCredit:credits) {
            HashMap<String, Object> paramMap = new HashMap<>();
            paramMap.put("versionId",versionId);
            paramMap.put("refusingCode",refusingCredit.getCode());
            paramMap.put("fid",fid);
            int number = iCUserRefConService.findthisVersionNumByRefusingCode(paramMap);
            AnalysisDto analysisDto = new AnalysisDto();
            analysisDto.setValue(new BigDecimal(number));
            if(refusingCredit.getCode().equals(LIABILITIENUMCHECK)){
                analysisDto.setName(refusingCredit.getName()+"高于"+refusingCredit.getNumber()+"家");
                nameList.add(refusingCredit.getName()+"高于"+refusingCredit.getNumber()+"家");
            }else if(refusingCredit.getCode().equals(OVERDUENUMCHECK)){
                analysisDto.setName("单平台逾期"+refusingCredit.getNumber()+"次及以上");
                nameList.add("单平台逾期"+refusingCredit.getNumber()+"次及以上");
            }else{
                analysisDto.setName(refusingCredit.getName()+refusingCredit.getContent());
                nameList.add(refusingCredit.getName()+refusingCredit.getContent());
            }
            analysisDto.setSort(0);
            fusingCreditPeopleList.add(analysisDto);
        }
        resultMap.put("valueList",fusingCreditPeopleList);
        resultMap.put("nameList",nameList);
        return resultMap;
    }


}
