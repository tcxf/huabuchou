package com.tcxf.hbc.admin.controller.rmcontroller;

import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IRmBaseCreditInitialService;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liaozeyong
 * @Date: 2018/9/26 11:13
 * @Description:
 */
@RestController
@RequestMapping("/platfrom/InitialCredit")
public class InitialCreditController extends BaseController {

    @Autowired
    private CreditCustomController creditCustomControllerl;
    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;
    /**
     * 跳转到评分自定义初始授信界面
     * @return
     */
    @RequestMapping("/goChushiSx")
    public ModelAndView goChushiSx(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("platfrom/chushiSx");
//        R r =creditCustomControllerl.getCreditInitialList(request);
//        Map map = (Map) r.getData();
//        modelAndView.addObject("map",map);
        return modelAndView;
    }

    /**
     * 动态加载修改评分自定义初始授信数据
     * @return
     */
    @RequestMapping("/goUpdateInitialCredit")
    public R goUpdateInitialCredit(HttpServletRequest request)
    {
//        R r = creditCustomControllerl.getCreditInitialListById(request);
//        return r;
        return R.newOK();
    }

    /**
     * 提交初始授信
     * @return
     */
    @RequestMapping("/commitInitialCredit")
    public R commitInitialCredit(@RequestBody List<RmBaseCreditInitial> list)
    {
        R r = new R();
        iRmBaseCreditInitialService.createRmBaseCreditInitial(list , "1029292115160596481");
        return r;
    }
}
