package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.IFFundendBindService;
import com.tcxf.hbc.admin.service.IRmBaseCreditInitialService;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liaozeyong
 * @Date: 2018/9/26 11:13
 * @Description:
 */
@RestController
@RequestMapping("/opera/InitialCredit")
public class MInitialCreditController extends BaseController {

    @Autowired
    private CreditCustomController creditCustomControllerl;
    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;
    @Autowired
    private IFFundendBindService ifFundendBindService;
    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;
    /**
     * 跳转到评分自定义初始授信界面
     * @return
     */
    @RequestMapping("/goChushiSx")
    public ModelAndView goChushiSx(HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("manager/chushiSx");
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));
        String id = request.getParameter("id");
        R r =creditCustomControllerl.getCreditInitialList(request , fFundendBind.getFid(), id);
        Map map = (Map) r.getData();
        modelAndView.addObject("map",map);
        return modelAndView;
    }

    /**
     * 动态加载修改评分自定义初始授信数据
     * @return
     */
    @RequestMapping("/goUpdateInitialCredit")
    public R goUpdateInitialCredit(HttpServletRequest request)
    {
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));
        R r = creditCustomControllerl.getCreditInitialListById(request , fFundendBind.getFid());
        return r;
    }

    /**
     * 提交初始授信
     * @return
     */
    @RequestMapping("/commitInitialCredit")
    public R commitInitialCredit(@RequestBody List<RmBaseCreditInitial> list)
    {
        R r = new R();
        iRmBaseCreditInitialService.createRmBaseCreditInitial(list , "1029292115160596481");
        return r;
    }

    /**
     * @Description: 跳转评分自定义主页
     * @Param: [request]
     * @return: org.springframework.web.servlet.ModelAndView
     * @Author: JinPeng
     * @Date: 2018/10/19
     */
    @RequestMapping("/goCreditCustomIndexView")
    public ModelAndView goCreditCustomIndexView(HttpServletRequest request){
        ModelAndView m = new ModelAndView();

        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));

        //当前版本
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_YES).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).eq("fid", fFundendBind.getFid()));
        //历史版本
        List<RmSxtemplateRelationship> lslist = iRmSxtemplateRelationshipService.selectList(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_NO).eq("fid", fFundendBind.getFid()).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).orderBy("publish_date",false));
        m.addObject("dqlist", dqlist);
        m.addObject("lslist", lslist);
//        m.setViewName("rm/zidingyi");
        m.setViewName("manager/score_customization_index");
        return m;
    }


}
