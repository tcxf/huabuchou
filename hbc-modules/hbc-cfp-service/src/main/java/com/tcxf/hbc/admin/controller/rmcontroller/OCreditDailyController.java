package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.CreditScoreService;
import com.tcxf.hbc.admin.service.ICreditDailyService;
import com.tcxf.hbc.admin.service.IFFundendBindService;
import com.tcxf.hbc.common.entity.CreditDaily;
import com.tcxf.hbc.common.entity.CreditScore;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/opera/CreditDaily")
public class OCreditDailyController extends BaseController {

    @Autowired
    private ICreditDailyService iCreditDailyService;

    @Autowired
    private CreditScoreService creditScoreService;
    @Autowired
    private IFFundendBindService ifFundendBindService;

    /**
     * 跳转授信日报页面
     */
    @RequestMapping("/crde")
    public ModelAndView findcrde(HttpServletRequest request) {
        ModelAndView m = new ModelAndView();
        m.setViewName("manager/daily_credit_report");
        return m;
    }

    /**
     * 查询授信日报
     *
     * @param request
     * @return
     */
    @RequestMapping("/findcrdelist")
    public R findcrdelist(HttpServletRequest request) {
        R r = new R<>();
        String oid = (String) get(request, "session_opera_id");
        Map<String, Object> params = new HashMap<>();
        params.put("oid", oid);
        params.put("startDate", request.getParameter("startDate"));
        params.put("endDate", request.getParameter("endDate"));
        params.put("page",request.getParameter("page"));
        params.put("limit",request.getParameter("limit"));
        Page plist = iCreditDailyService.findPlist(params);
        r.setData(plist);
        return r;
    }
    /**
     * 下载报表
     * @param request
     * @param response
     */

    @RequestMapping("/exportExcle")
    public void exportPaybackPoi(HttpServletRequest request, HttpServletResponse response) {
        String oid = (String) get(request,"session_opera_id");
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",oid);
        Page plist = iCreditDailyService.findPlist(paramMap);

        String[] title = {"编号", "授信时间", "新增授信人数","新增授信额度 (元)", "授信通过率", "申请提额人数", "人均提额金额 (元)","人均授信额度 (元)"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<CreditDaily> listDto=plist.getRecords();

        for (CreditDaily CreditDaily : listDto) {
            String[] strings = new String[9];
            strings[0] = i + "";
            strings[1] =DateUtil.getDateHavehms(CreditDaily.getCreateTime());
            strings[2] = CreditDaily.getSxNumberPeople();
            strings[3] = CreditDaily.getCreditLine().toString();
            strings[4] = CreditDaily.getPassingRate();
            strings[5] = CreditDaily.getUpmoneyNumberPeople();
            strings[6] = CreditDaily.getPercapitaUpMoney().toString();
            strings[7] = CreditDaily.getPercapitaSxMoney().toString();
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "授信日报统计(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }

    /**
     * 跳转授信综合分值界面
     */
    @RequestMapping("/goCreditCustomView")
    public ModelAndView goCreditCustomView(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        String oid = (String) get(request, "session_opera_id");
        String id = request.getParameter("id");

        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid", oid).eq("status", 1));
        Map<String, Object> map = new HashMap<>();
        map.put("fid", fFundendBind.getFid());
        map.put("id", id);
        List<CreditScore> list = creditScoreService.selectCreditScoreByfId(map);
        m.addObject("sx", list);
        m.setViewName("manager/sxpf");
        return m;
    }
}
