package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmBaseCreditType;
import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jiangyong
 * @describe 授信提额查询（运营商）
 * @time 2018/10/16
 */
@RestController
@RequestMapping("/opera/creditCustom")
public class OperaCreditCustomController extends BaseController {

    @Autowired
    private IRmBaseCreditUpmoneyService iRmBaseCreditUpmoneyService;

    @Autowired
    private IRmBaseCreditTypeService iRmBaseCreditTypeService;

    @Autowired
    private RefusingVersionService refusingVersionService;


    @Autowired
    private IFFundendBindService ifFundendBindService;

    /**
     * 跳转授信提额
     */
    @RequestMapping("/goCreditCustomViews")
    public ModelAndView goCreditCustomViews(HttpServletRequest request){
        ModelAndView m = new ModelAndView();
        m.setViewName("rm/operaletterofcredit");
        return m;
    }


    /**
     * @Description:  授信提额条件列表
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/getRmBaseCreditUpmoneyList")
    public R getRmBaseCreditUpmoneyList(HttpServletRequest request){
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));

        //查询当前版本
        RefusingVersion refusingVersion = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("type",RefusingVersion.TYPE_3).eq("status",RefusingVersion.STATUS_YES).eq("fId",fFundendBind.getFid()));


        List<RmBaseCreditUpmoney> rbcilist = iRmBaseCreditUpmoneyService.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("versionId", refusingVersion.getId()));

        //查询授信类别列表
        List<RmBaseCreditType> rbctlist = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().eq("status",RmBaseCreditType.TYPE_1));


        //封装数据

        List<Object> list = new ArrayList<>();
        list.addAll(rbcilist);
        list.addAll(rbctlist);

        return R.newOK(list);
    }


    /**
     * @Description: 授信提额修改查询
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/getRmBaseCreditUpmoneyListById")
    public R getRmBaseCreditUpmoneyListById(HttpServletRequest request){
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));
        try {

            RefusingVersion refusingVersion = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("type",RefusingVersion.TYPE_3).eq("status",RefusingVersion.STATUS_YES).eq("fId",fFundendBind.getFid()));

            RmBaseCreditUpmoney rbci = new RmBaseCreditUpmoney();

            if (!ValidateUtil.isEmpty(request.getParameter("typeId"))){
                rbci.setTypeId(request.getParameter("typeId"));
            }

            List<RmBaseCreditUpmoney> rbcilist = iRmBaseCreditUpmoneyService.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("typeId", request.getParameter("typeId")).eq("versionId", refusingVersion.getId()));
            return R.newOK(rbcilist);

        }catch (Exception e){

            return R.newErrorMsg(e.getMessage());
        }

    }

    /**
     * @Description: 保存授信提额修改模板
     * @Param: [request]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/9/20
     */
    @RequestMapping("/saveRmBaseCreditUpmoney")
    public R saveRmBaseCreditUpmoney(@RequestBody List<RmBaseCreditUpmoney> list,HttpServletRequest request){
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",get(request , "session_opera_id")).eq("status" , "1"));
        iRmBaseCreditUpmoneyService.createRmBaseCreditUpmoney(list,fFundendBind.getFid());
        return R.newOK();
    }

}
