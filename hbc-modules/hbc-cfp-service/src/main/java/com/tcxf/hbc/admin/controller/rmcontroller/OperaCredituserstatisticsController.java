package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.BindFODto;
import com.tcxf.hbc.admin.service.IRmCreditmonthStatisticalService;
import com.tcxf.hbc.admin.service.IRmRepaymentSituationService;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.tcxf.hbc.common.entity.RmRepaymentSituation;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.secure.DES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author Jiangyong
 * @describe  授信用户统计 授信月份统计
 * @time 2018/10/10
 */
@RestController
@RequestMapping("/opera/Credituserstatistics")
public class OperaCredituserstatisticsController extends BaseController {

    @Autowired
    private IRmCreditmonthStatisticalService iRmCreditmonthStatisticalService;

    @Autowired
    private IRmRepaymentSituationService iRmRepaymentSituationService;

    @RequestMapping("/qu")
    public ModelAndView qu(HttpServletRequest request) throws Exception {
        String oid = (String) get(getRequest(),"session_opera_id");
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",oid);
        ModelAndView modelAndView = new ModelAndView();
        List <BindFODto> bindFODtos = iRmCreditmonthStatisticalService.QueryfOs(paramMap);
        List <BindFODto> sumzsxed = iRmCreditmonthStatisticalService.sumzsxed();
        List <BindFODto> sunsxed = iRmCreditmonthStatisticalService.sumsxed();
        //昨日授信人数
        BindFODto countpople = iRmCreditmonthStatisticalService.sumzsx();
        //授信总人数
        BindFODto countpoples = iRmCreditmonthStatisticalService.sumsx();
        //昨日授信额度
        BigDecimal sunmoeny = new BigDecimal("0");

        BigDecimal sunmoenys = new BigDecimal("0");


        for (BindFODto bindFODto : sumzsxed) {
            if (ValidateUtil.isEmpty(sumzsxed) || ValidateUtil.isEmpty(bindFODto.getSunmoeny())) {
                break;
            }
            BigDecimal moeny = new BigDecimal(DES.decryptdf(bindFODto.getSunmoeny().toString()));
            sunmoeny = sunmoeny.add(moeny);
        }

        for (BindFODto bindFODto : sunsxed) {
            if (ValidateUtil.isEmpty(sunsxed) || ValidateUtil.isEmpty(bindFODto.getSunmoenys())) {
                break;
            }
            BigDecimal moenys = new BigDecimal(DES.decryptdf(bindFODto.getSunmoenys().toString()));
            sunmoenys = sunmoenys.add(moenys);
        }
        modelAndView.addObject("sunmoeny", sunmoeny);
        modelAndView.addObject("sunmoenys", sunmoenys);
        modelAndView.addObject("countpople", countpople.getCountpople());
        modelAndView.addObject("countpoples", countpoples.getCountpoples());
        modelAndView.addObject("list", bindFODtos);
        modelAndView.setViewName("rm/operaindex_v1");
        return modelAndView;
    }

    //授信月份统计
    @RequestMapping("/QueryCredituserstatistics")
    public R QueryCredituserstatistics(HttpServletRequest request) {
        R r = new R();
        String oid = (String) get(getRequest(),"session_opera_id");
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",oid);
        Page page = iRmCreditmonthStatisticalService.QueryCredituserstatistics(new Query <RmCreditmonthStatistical>(paramMap));
        r.setData(page);
        return r;
    }

    @RequestMapping("/QueryCredituserstatisticss")
    public R QueryCredituserstatisticss(HttpServletRequest request) {
        R r = new R();
        String oid = (String) get(getRequest(),"session_opera_id");
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid",oid);
        Page page = iRmRepaymentSituationService.QueryRepaymentsituation(new Query <RmRepaymentSituation>(paramMap));
        r.setData(page);
        return r;
    }
}
