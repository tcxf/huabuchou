package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.CCreditUserInfoDto;
import com.tcxf.hbc.admin.service.ICCreditUserInfoService;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Jiangyong
 * @describe 风控系统接口使用记录（平台）
 * @time 2018/9/18
 */
@RestController
@RequestMapping("/opera/interface")
public class OperaFKInterfaceRecordController extends BaseController {

    @Autowired private ICCreditUserInfoService icCreditUserInfoService;

    @RequestMapping("/qu")
    public ModelAndView Jump(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("rm/operaInterface_usage_record");
        return  modelAndView;
    }
    /**
     *@describe 查询接口使用记录
     *@parameter
     *@return
     *@author  Jiangyong
     *@Creationtime 2018/9/19
     *@Modifier
     */
    @RequestMapping("/Queryinterface")
    public R Queryinterface (HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Page page = icCreditUserInfoService.Bank(new Query <CCreditUserInfoDto>(paramMap));
        r.setData(page);
        return  r;
    }

    /**
     * poi报表
     */
    @RequestMapping("/tradingPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        List <CCreditUserInfoDto> listDto = icCreditUserInfoService.Bank(paramMap);
        String[] title = {"编号","公司名称","接口名称","运营商名称", "授信次数","使用总额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (CCreditUserInfoDto tbTradingDetailDto : listDto) {
            String[] strings = new String[6];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getCorporatename();
            strings[2] = tbTradingDetailDto.getTypeName();
            strings[3] = tbTradingDetailDto.getOperatorType();
            strings[4] = tbTradingDetailDto.getCounts()+"次";
            strings[5] = tbTradingDetailDto.getMoney()+"元";
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "接口使用记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }
}
