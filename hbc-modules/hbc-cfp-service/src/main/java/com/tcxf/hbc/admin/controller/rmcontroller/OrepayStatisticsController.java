package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.service.ITbRepayStatisticalService;
import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 9:31 2018/9/20
 */


@RestController
@RequestMapping("/opera/ORiskControl")
public class OrepayStatisticsController extends BaseController {

    @Autowired
    private ITbRepayStatisticalService iTbRepayStatisticalService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:35 2018/9/20
     * 跳转还款统计页面
    */
    @GetMapping("/jumpORepayStatistics")
    public ModelAndView jumpRepayStatistics()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/o_repayment_statistics");
        return mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:00 2018/9/21
     * 还款统计页面
    */
    @RequestMapping("/RepayStatistics")
    public R findRepayStatistics(HttpServletRequest request){
        R r = new R<>();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Integer page =Integer.parseInt((String) paramMap.get("page")) ;
        Integer rows =Integer.parseInt((String) paramMap.get("limit")) ;
        String oid = (String) get(request,"session_opera_id");

        //查询平台每日还款记录统计
        Wrapper<TbRepayStatistical> tbRepayStatisticalWrapper = new EntityWrapper<TbRepayStatistical>();
        tbRepayStatisticalWrapper.eq("oid",oid);

        //开始时间 结束时间
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");

        if(startTime!=null && !"".equals(startTime) && endTime!=null && !"".equals(endTime)){
            tbRepayStatisticalWrapper.gt("create_time",startTime).lt("create_time",endTime);
        }

        Page pageInfo= iTbRepayStatisticalService.selectPage(new Page<TbRepayStatistical>(page,rows), tbRepayStatisticalWrapper);
        r.setData(pageInfo);
        return r;
    }



    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:33 2018/9/21
     * 报表导出
     */
    @RequestMapping("/ExportExcle")
    public void exportPaybackPoi(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Integer page =Integer.parseInt((String) paramMap.get("page")) ;
        Integer rows =Integer.parseInt((String) paramMap.get("limit")) ;
        String oid = (String) get(request,"session_opera_id");

        //查询平台每日还款记录统计
        Wrapper<TbRepayStatistical> tbRepayStatisticalWrapper = new EntityWrapper<TbRepayStatistical>();
        tbRepayStatisticalWrapper.eq("oid",oid);

        //开始时间 结束时间
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");

        if(startTime!=null && !"".equals(startTime) && endTime!=null && !"".equals(endTime)){
            tbRepayStatisticalWrapper.gt("create_time",startTime).lt("create_time",endTime);
        }

        Page pageinfo= iTbRepayStatisticalService.selectPage(new Page<TbRepayStatistical>(page,rows), tbRepayStatisticalWrapper);

        String[] title = { "编号", "还款日期","应还人数", "实还人数", "按时还款率", "应还金额(元)","实际回款金额(元)","金额回款率","中期异常人数"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<TbRepayStatistical> listDto=pageinfo.getRecords();

        for (TbRepayStatistical RepayStatistical : listDto) {
            String[] strings = new String[9];
            strings[0] = i + "";
            strings[1] =DateUtil.getDateHavehms(RepayStatistical.getCreateTime());
            strings[2] = RepayStatistical.getShouldPayPeople();
            strings[3] = RepayStatistical.getAlreadyPayPeople();
            strings[4] = RepayStatistical.getPayRate();
            strings[5] = RepayStatistical.getShouldPayMoney().toString();
            strings[6] = RepayStatistical.getActualRepayMoney().toString();
            strings[7] = RepayStatistical.getRepayRate();
            strings[8] = RepayStatistical.getUnusualNumber();
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "还款统计(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }


}
