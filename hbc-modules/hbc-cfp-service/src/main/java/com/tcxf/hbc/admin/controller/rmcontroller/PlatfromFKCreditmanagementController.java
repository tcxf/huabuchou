package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.QueryCreditRecordDto;
import com.tcxf.hbc.admin.service.ICUserInfoService;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Jiangyong
 * @describe 风控系统授信管理(平台)
 * @time 2018/9/17
 */
@RestController
@RequestMapping("/platfrom/FKCreditmanagementController")
public class PlatfromFKCreditmanagementController extends BaseController {
    @Autowired
    private ICUserInfoService icUserInfoService;

    /**
     *@describe 授信管理跳转页面
     *@parameter 
     *@return
     *@author  Jiangyong
     *@Creationtime 2018/9/17
     *@Modifier
     */
    @RequestMapping("/qu")
    public ModelAndView modelAndView(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("rm/platfromCredit_usage_record");
        return  modelAndView;
    }
    /**
     *@describe 查询授信管理
     *@parameter
     *@return  
     *@author  Jiangyong
     *@Creationtime 2018/9/17
     *@Modifier
     */
      @RequestMapping("/queryCreditRecord")
      public R QueryCreditRecord(HttpServletRequest request) throws Exception {
          R r=new R();
          Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
          Page page = icUserInfoService.QueryCreditRecord(new Query <QueryCreditRecordDto>(paramMap));
          r.setData(page);
          return  r;
        }

    /**
     * poi报表
     */
    @RequestMapping("/tradingPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        if("".equals(paramMap.get("startDate"))) {
            paramMap.put("startDate", com.tcxf.hbc.common.util.DateUtil.getMonthFirstDay());
        }
        if("".equals(paramMap.get("endDate"))) {
            paramMap.put("endDate", com.tcxf.hbc.common.util.DateUtil.getCurrentTime());
        }
        List<QueryCreditRecordDto> listDto= icUserInfoService.xz(paramMap);
        String[] title = {"编号","用户名称","手机号码","用户类型", "授信时间","授信总额", "已使用额度", "剩余授信额度"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (QueryCreditRecordDto tbTradingDetailDto : listDto) {
            String[] strings = new String[8];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getRealName();
            strings[2] = tbTradingDetailDto.getMobile();
            strings[3] = tbTradingDetailDto.getUserAttribute();
            strings[4] = DateUtil.getDateHavehms(tbTradingDetailDto.getRefCreateDate());
            strings[5] = tbTradingDetailDto.getMaxMoney()+"元";
            strings[6] = tbTradingDetailDto.getBalance()+"元";
            strings[7] = tbTradingDetailDto.getSumMoney()+"元";
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "授信使用记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }
}
