package com.tcxf.hbc.admin.controller.rmcontroller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.CreditPageInfoDto;
import com.tcxf.hbc.admin.model.dto.RepaymentDetailDto;
import com.tcxf.hbc.admin.model.dto.TbRepayLogDto;
import com.tcxf.hbc.admin.service.ITbRepayStatisticalService;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 9:31 2018/9/20
 * 还款统计模块
 */


@RestController
@RequestMapping("/platfrom/riskControl")
public class PrepayStatisticsController extends BaseController {

    @Autowired
    private ITbRepayStatisticalService iTbRepayStatisticalService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:35 2018/9/20
     * 跳转还款统计页面
    */
    @GetMapping("/jumpPRepayStatistics")
    public ModelAndView jumpRepayStatistics()
    {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("rm/repayment_statistics");
        return mode;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:00 2018/9/21
     * 还款统计页面
    */
    @RequestMapping("/prepayStatistics")
    public R findRepayStatistics(HttpServletRequest request){
        R r = new R<>();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Integer page =Integer.parseInt((String) paramMap.get("page")) ;
        Integer rows =Integer.parseInt((String) paramMap.get("limit")) ;

        //查询平台每日还款记录统计
        Wrapper<TbRepayStatistical> tbRepayStatisticalWrapper = new EntityWrapper<TbRepayStatistical>();
        tbRepayStatisticalWrapper.isNull("oid").isNull("fid");

        //开始时间 结束时间
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");

        if(startTime!=null && !"".equals(startTime) && endTime!=null && !"".equals(endTime)){
            tbRepayStatisticalWrapper.gt("create_time",startTime).lt("create_time",endTime);
        }

        Page pageInfo= iTbRepayStatisticalService.selectPage(new Page<TbRepayStatistical>(page,rows), tbRepayStatisticalWrapper);
        r.setData(pageInfo);
        return r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:33 2018/9/21
     * 报表导出
    */
    @RequestMapping("/exportExcle")
    public void exportPaybackPoi(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        Integer page =Integer.parseInt((String) paramMap.get("page")) ;
        Integer rows =Integer.parseInt((String) paramMap.get("limit")) ;

        //查询平台每日还款记录统计
        Wrapper<TbRepayStatistical> tbRepayStatisticalWrapper = new EntityWrapper<TbRepayStatistical>();
        tbRepayStatisticalWrapper.isNull("oid").isNull("fid");

        //开始时间 结束时间
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");

        if(startTime!=null && !"".equals(startTime) && endTime!=null && !"".equals(endTime)){
            tbRepayStatisticalWrapper.gt("create_time",startTime).lt("create_time",endTime);
        }

        Page pageinfo= iTbRepayStatisticalService.selectPage(new Page<TbRepayStatistical>(page,rows), tbRepayStatisticalWrapper);

        String[] title = { "编号", "还款日期","应还人数", "实还人数", "按时还款率", "应还金额(元)","实际回款金额(元)","金额回款率","中期异常人数"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<TbRepayStatistical> listDto=pageinfo.getRecords();

        for (TbRepayStatistical tbRepayStatistical : listDto) {
            String[] strings = new String[9];
            strings[0] = i + "";
            strings[1] =DateUtil.getDateHavehms(tbRepayStatistical.getCreateTime());
            strings[2] = tbRepayStatistical.getShouldPayPeople();
            strings[3] = tbRepayStatistical.getAlreadyPayPeople();
            strings[4] = tbRepayStatistical.getPayRate();
            strings[5] = tbRepayStatistical.getShouldPayMoney().toString();
            strings[6] = tbRepayStatistical.getActualRepayMoney().toString();
            strings[7] = tbRepayStatistical.getRepayRate();
            strings[8] = tbRepayStatistical.getUnusualNumber();
            list.add(strings);
            i++;
        }
        ExportUtil.createExcel(list, title, response, "还款统计(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }

}
