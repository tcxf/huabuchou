package com.tcxf.hbc.admin.controller.transactionmanagement;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.service.IOOperaInfoService;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.ExportUtil;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 运营商交易记录
 * jangyong
 */
@RestController
@RequestMapping("/opera/tradingDetail")
public class OTransactionmanagement extends BaseController {

    @Autowired
    private IOOperaInfoService ioOperaInfoService;


    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    /**
     * 查询交易记录跳转界面
     * @return
     */
    @RequestMapping("/qu")
    public ModelAndView qu(HttpServletRequest request){
        ModelAndView modelAndView =new ModelAndView();
        List<OOperaInfo> list = ioOperaInfoService.selectList(new EntityWrapper<OOperaInfo>());
        String opid = get(request,"session_opera_id").toString();
        modelAndView.addObject("opid",opid);
        modelAndView.addObject("list",list);
        modelAndView.setViewName("manager/tradingDetailManager");
        return  modelAndView;
    }

    /**
     * 分页查询平台交易记录
     * @param request
     * @return
     */
    @RequestMapping("/tradingDetailManagerList")
    public R tradingDetailManagerList(HttpServletRequest request){
        R r=new R<>();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String opid = get(request,"session_opera_id").toString();
        paramMap.put("opid", opid);
        Page data = iTbTradingDetailService.transaction(new Query<TransactionDto>(paramMap));
        r.setData(data);
        return  r;
    }

    /**
     * 交易记录详情 , 运营商让利查看详情 ，商户让利查看详情
     * @param request
     * @return
     */
    @RequestMapping("/detailInfo")
    public  R detailInfo(HttpServletRequest request){
        R r=new R();
        String tid = request.getParameter("tid");
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("tid",tid);
        Page data = iTbTradingDetailService.transaction(new Query <TransactionDto>(paramMap));
        r.setData(data);
        return r;
    }

    /**
     * 跳转交易记录
     * @param request
     * @return
     */
    @RequestMapping("/QueryOne")
    public ModelAndView QueryOne(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView();
        String tid = request.getParameter("tid");
        modelAndView.addObject("tid",tid);
        modelAndView.setViewName("manager/tradingHandler");
        return modelAndView;
    }

    /**
     * 查询交易总额 运营商抽成总额 	直接返佣总额	间接级返佣总额
     * @return
     */
    @RequestMapping("/SUM")
    public R SUM(HttpServletRequest request){
        R r=new R();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String opid = get(request,"session_opera_id").toString();
        paramMap.put("opid", opid);
        Map<String,Object>  sum = iTbTradingDetailService.SUM(paramMap);
        System.out.println(sum);
        r.setData(sum);
        return r;
    }

    /**
     * 交易记录导出交易记录poi报表
     */
    @RequestMapping("/tradingPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        String opid = get(request,"session_opera_id").toString();
        paramMap.put("opid", opid);
        if("".equals(paramMap.get("startDate"))) {
            paramMap.put("startDate", com.tcxf.hbc.common.util.DateUtil.getMonthFirstDay());
        }
        if("".equals(paramMap.get("endDate"))) {
            paramMap.put("endDate", com.tcxf.hbc.common.util.DateUtil.getCurrentTime());
        }
        List<TransactionDto> listDto=  iTbTradingDetailService.transactionB(paramMap);
        String[] title = {"编号","交易单号","下单用户", "用户手机","商户名称", "商户手机", "交易状态", "交易时间","交易金额","优惠金额","实付金额" ,"运营商获利折扣","直接级返佣","间接级返佣" ,"结算金额","交易总额","运营商获利折扣总额","直接返佣总额","间接返佣总额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        for (TransactionDto tbTradingDetailDto : listDto) {
            String[] strings = new String[15];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getRealName();
            strings[3] = tbTradingDetailDto.getMobile();
            strings[4] = tbTradingDetailDto.getName();
            strings[5] = tbTradingDetailDto.getMmobile();
            strings[6] = tbTradingDetailDto.getPaymentStatus();
            strings[7] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
            strings[8] = String.valueOf(tbTradingDetailDto.getTradeAmount());
            strings[9] = String.valueOf(tbTradingDetailDto.getDiscountAmount());
            strings[10] = String.valueOf(tbTradingDetailDto.getActualAmount());
            strings[11] = String.valueOf(tbTradingDetailDto.getOperatorShareFee());
            strings[12] = String.valueOf(tbTradingDetailDto.getRedirectShareAmount());
            strings[13] = String.valueOf(tbTradingDetailDto.getInderectShareAmount());
            strings[14] = String.valueOf(tbTradingDetailDto.getSettlementAmounts());
            list.add(strings);
            i++;
        }
        String[] totalString =new String[19];
        Map<String,Object>  sum = iTbTradingDetailService.SUM(paramMap);
        totalString[15] = String.valueOf(sum.get("sumamount"));
        totalString[16] = String.valueOf(sum.get("sumoperatorsharefee"));
        totalString[17] = String.valueOf(sum.get("sumredirectshareamount"));
        totalString[18] = String.valueOf(sum.get("suminderectshareamount"));

        list.add(0,totalString);
        ExportUtil.createExcel(list, title, response, "交易记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }
}
