package com.tcxf.hbc.admin.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.feign.ITbCreditGradeService;
import com.tcxf.hbc.admin.mapper.TbRepaymentDetailMapper;
import com.tcxf.hbc.admin.model.dto.*;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.util.secure.DES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/fund/user")
public class FuserInfoController extends BaseController {

    @Autowired
    public ICUserInfoService iCUserInfoService;

    @Autowired
    public  ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    private IOOperaInfoService ioOperaInfoService;

    @Autowired
    private ICUserRefConService icUserRefConService;
    @Autowired
    private ITbAccountInfoService iTbAccountInfoService;

    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    private ITbRepaymentPlanService iTbRepaymentPlanService;

    @Autowired
    private ITbCreditGradeService iTbCreditGradeService;
    private String uid;

    private String rapidMoney;
    private String upMoney;
    private String totalMoney;

    /**
     * 平台查询所有消费者
     */
    @RequestMapping("/userslist")
    public R userslist(HttpServletRequest request) {
        R r=new R<>();
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        //获取登陆的id
        String fid = get(request,"session_fund_id").toString();
        paramMap.put("fid",fid);
        Page <CuserinfoDto> data = iCUserInfoService.userlist(new Query <CuserinfoDto>(paramMap));
        r.setData(data);
        return  r;
    }

    /**
     * 消费者详情跳转
     * @param request
     * @return
     */
    @RequestMapping("/forwardUserInfoDetail")
    public ModelAndView forwardUserInfoDetail(HttpServletRequest request)throws Exception{
        ModelAndView mode = new ModelAndView();
        uid = request.getParameter("uid");
        CuserinfoDto entity = iCUserInfoService.QueryAll(uid);
        TbAccountInfo tb = iTbAccountInfoService.selectOne(new EntityWrapper <TbAccountInfo>().eq("oid", this.uid));
        BigDecimal rapidMoney;
        BigDecimal upMoney;
        if (ValidateUtil.isEmpty(tb) || ValidateUtil.isEmpty(tb.getRapidMoney())) {
            rapidMoney=new BigDecimal("0");
        }else {
            rapidMoney=new BigDecimal(DES.decryptdf(tb.getRapidMoney().toString()));
        }

        if (ValidateUtil.isEmpty(tb) || ValidateUtil.isEmpty(tb.getMaxMoney())) {
            upMoney=new BigDecimal("0");
        }else {
            upMoney  = new BigDecimal(DES.decryptdf(tb.getMaxMoney().toString()));
            BigDecimal b= new BigDecimal("1500");
            upMoney = upMoney.subtract(b);
        }
        mode.addObject("rapidMoney", rapidMoney);
        mode.addObject("upMoney", upMoney);
        mode.addObject("totalMoney", rapidMoney.add(upMoney));
        mode.addObject("entity",entity);
        mode.addObject("uid", uid);
        mode.setViewName("fund/userInfoDetail");
        return mode;
    }



    /**
     * 用户还款记录
     */
    @RequestMapping("/repaymentPlanManagerList")
    public  R  repaymentPlanManagerList(HttpServletRequest request){
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        R r = new R<>();
        Page pageInfo= iTbRepaymentDetailService.repaymentPlanManagerList(new Query<RepaymentDetailDto>(paramMap));
        r.setData(pageInfo);
        return r;

    }



    /**
     * 冻结或者启动用户授信状态
     */
    @RequestMapping("/userandfindfund")
    public  R  userandfindfund(HttpServletRequest request){
        R r = new R<>();
        String uid = request.getParameter("id");
        Object fundId = get(request, "session_fund_id");
        String type = request.getParameter("type");
        Map<String, Object> m = new HashMap<>();
        m.put("uid",uid);
        m.put("fid",fundId);
        CUserRefCon cUserRefCon = icUserRefConService.finduserCon(m);
        TbAccountInfo tbAccountInfo = iTbAccountInfoService.selectOne(new EntityWrapper<TbAccountInfo>().eq("oid", uid));
        cUserRefCon.setAuthStatus(type);
        boolean b = icUserRefConService.updateById(cUserRefCon);
        if (type.equals("1")){
            tbAccountInfo.setIsFreeze("0");
            iTbAccountInfoService.updateById(tbAccountInfo);
        }else if (type.equals("2")){
            tbAccountInfo.setIsFreeze("1");
            iTbAccountInfoService.updateById(tbAccountInfo);
        }
        if (b){
            r.setCode(1);
            r.setMsg("修改成功");
        }else {
            r.setCode(0);
            r.setMsg("修改失败");
        }
        return r;

    }

    /**
     * 预期列表
     */
    @RequestMapping("/pageInfo")
    public R findPaybackPageInfo(HttpServletRequest request)
    {
        String id = (String) get(request,"session_opera_id");
        System.out.println(id);
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("oid", id);
        Page paybackPage= iTbRepaymentDetailService.findPaybackPageInfo(new Query<RepaymentDetailDto>(paramMap));
        R r = new R<>();
        r.setData(paybackPage);
        return  r;
    }

    /**
     * 今日新增商户 跳转分页查询界面
     *
     * @return
     */
    @RequestMapping("/users")
    public ModelAndView QueryAlldate(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        long l = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(l);
        Date date = cal.getTime();
        String start = DateUtil.getTimesmorning();
        String end = DateUtil.getTimesnight();
        Object fid = get(request, "session_fund_id");
        int number = iCUserInfoService.FCreateQueryAllDate(start,end,fid.toString());
        HashMap<String, Object> map = new HashMap<>();
        map.put("fid",fid);
        List<OOperaInfo> list = ioOperaInfoService.selectOperaByfid(map);
        System.out.println(number);
        mode.addObject("number", number);
        mode.addObject("list",list);
        mode.setViewName("fund/userInfoManager");
        return mode;
    }
    /**
     * 加载交易记录分页
     */
    @RequestMapping("/tradingDetailManagerList")
    public R findTradingPageInfo(HttpServletRequest request)
    {
        Boolean flag=true;
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("uid", uid);
        R r = new R<>();
        Page pageInfo=  iTbTradingDetailService.findTradingPageInfo(new Query<TbTradingDetailDto>(paramMap));
        r.setData(pageInfo);
        return r;
    }

    /**
     * 交易记录导出交易记录poi报表
     */
    @RequestMapping("/tradingPoi")
    public void exportAdvancePayPoi(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("uid", uid);
        Page pageInfo=  iTbTradingDetailService.findTradingPageInfo(new Query<TbTradingDetailDto>(paramMap));
        String[] title = {"编号","交易单号","商户id", "商户名称","行业类型", "用户id", "用户名称", "交易类型","交易时间","交易金额","结算金额" ,"交易总额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<TbTradingDetailDto> listDto=pageInfo.getRecords();
        for (TbTradingDetailDto tbTradingDetailDto : listDto) {
            String[] strings = new String[11];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getMid();
            strings[3] = tbTradingDetailDto.getLegalName();
            strings[4] = tbTradingDetailDto.getNature();
            strings[5] = tbTradingDetailDto.getUid();
            strings[6] = tbTradingDetailDto.getRealName();
            strings[7] = tbTradingDetailDto.getTradingType();
            strings[8] = DateUtil.getDateHavehms(tbTradingDetailDto.getTradingDate());
            strings[9] = String.valueOf(tbTradingDetailDto.getTradeAmount());
            strings[10]= String.valueOf(tbTradingDetailDto.getActualAmount());
            list.add(strings);
            i++;
        }
        String[] totalString =new String[12];
        BigDecimal sum = iTbTradingDetailService.selectTotalMoney(paramMap);
        totalString[11] = String.valueOf(sum);
        list.add(0,totalString);
        ExportUtil.createExcel(list, title, response, "交易记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }


    /**
     * 查询逾期总额 待还总额
     * @return
     */
    @RequestMapping("/SUM")
    public R SUM(HttpServletRequest request) {
        R r = new R();
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map <String, Object> sum = iTbRepaymentDetailService.DHYQSUM(paramMap);
        System.out.println(sum);
        r.setData(sum);
        return r;
    }


    /**
     * 查询逾期总额 待还总额
     * @return
     */
    @RequestMapping("/SUMS")
    public R SUMS(HttpServletRequest request) {
        R r = new R();
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        Map <String, Object> sum = iTbRepaymentDetailService.YHSUM(paramMap);
        System.out.println(sum);
        r.setData(sum);
        return r;
    }

    /**
     * 还款记录poi报表
     */
    @RequestMapping("/tradingPois")
    public void exportAdvancePayPois(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("uid",uid);
        Page pageInfo=  iTbRepaymentDetailService.repaymentPlanManagerList(new Query<RepaymentDetailDto>(paramMap));
        String[] title = {"编号","流水单号","用户ID","还款状态", "用户姓名","应还本金", "滞纳金", "分期利息", "本期应还","期数","还款日","已还金额" ,"待还总金额","逾期总金额"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<RepaymentDetailDto> listDto=pageInfo.getRecords();
        for (RepaymentDetailDto tbTradingDetailDto : listDto) {
            String[] strings = new String[11];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getUserId();
            strings[3] = tbTradingDetailDto.getRepaymentType();
            strings[4] = tbTradingDetailDto.getRealName();
            strings[5] = String.valueOf(tbTradingDetailDto.getCurrentCorpus());
            strings[6] = String.valueOf(tbTradingDetailDto.getLateFee());
            strings[7] = String.valueOf(tbTradingDetailDto.getCurrentFee());
            BigDecimal b=tbTradingDetailDto.getCurrentCorpus().add(tbTradingDetailDto.getCurrentFee());
            strings[8] = b.toString();
            String timeStr = tbTradingDetailDto.getCurrentTime()+ "/" + tbTradingDetailDto.getTotalTime();
            strings[9] = timeStr;
            strings[10] = tbTradingDetailDto.getRepaymentDate();
            list.add(strings);
            i++;
        }
        String[] totalString =new String[14];
        Map <String, Object> yhsum = iTbRepaymentDetailService.YHSUM(paramMap);
        Map <String, Object> dhyqsum = iTbRepaymentDetailService.DHYQSUM(paramMap);
        totalString[11] = String.valueOf(yhsum.get("YhSum"));
        totalString[12] = String.valueOf(dhyqsum.get("DhSum"));
        totalString[13] = String.valueOf(dhyqsum.get("YqSum"));
        list.add(0,totalString);
        ExportUtil.createExcel(list, title, response, "还款记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }



    /**
     * 逾期记录poi报表
     */
    @RequestMapping("/tradingPoiss")
    public void exportAdvancePayPoiss(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        paramMap.put("uid", uid);
        Page pageInfo= iTbRepaymentDetailService.findPaybackPageInfo(new Query<RepaymentDetailDto>(paramMap));
        String[] title = {"编号","流水单号","用户ID","期数", "逾期本金","滞纳金", "利息费", "逾期应还", "还款日","逾期天数","逾期总金额=" ,"本金+","利息+","滞纳金"};
        int i = 1;
        List<String[]> list = new LinkedList<String[]>();
        List<RepaymentDetailDto> listDto=pageInfo.getRecords();
        for (RepaymentDetailDto tbTradingDetailDto : listDto) {
            String[] strings = new String[10];
            strings[0] = i + "";
            strings[1] = tbTradingDetailDto.getSerialNo();
            strings[2] = tbTradingDetailDto.getUserId();
            String timeStr = tbTradingDetailDto.getCurrentTime()+ "/" + tbTradingDetailDto.getTotalTime();
            strings[3] = timeStr;
            strings[4] = String.valueOf(tbTradingDetailDto.getCurrentCorpus());
            strings[5] = String.valueOf(tbTradingDetailDto.getLateFee());
            strings[6] = String.valueOf(tbTradingDetailDto.getCurrentFee());
            BigDecimal d = tbTradingDetailDto.getCurrentCorpus().add(tbTradingDetailDto.getCurrentFee());
            strings[7] = String.valueOf(d);
            strings[8] = tbTradingDetailDto.getRepaymentDate();
            strings[9] = tbTradingDetailDto.getMargin();
            list.add(strings);
            i++;
        }
        String[] totalString =new String[14];
        Map <String, Object> mmp = iTbRepaymentPlanService.findoverDuePayByCondiction(paramMap);
        totalString[10] = String.valueOf(mmp.get("totalMoney"));
        totalString[11] = String.valueOf(mmp.get("currentCorpus"));
        totalString[12] = String.valueOf(mmp.get("currentFee"));
        totalString[13] = String.valueOf(mmp.get("lateFee"));
        list.add(0,totalString);
        ExportUtil.createExcel(list, title, response, "逾期记录导出(" + DateUtil.getCurrDateTimeMillFormat() + ").xlsx");
    }


    /**
     *授信分值
     */
    @RequestMapping("/tbUpMoney")
    public  R CreditScore(){
        R r=new R();
        r.setData(iTbCreditGradeService.CreditScoreone(uid));
        return r;
    }

    /**
     *授信分值
     */
    @RequestMapping("/tbCreditGrade")
    public  R CreditScors(){
        R r=new R();
        r.setData(iTbCreditGradeService.CreditScoretow(uid));
        return r;
    }
}
