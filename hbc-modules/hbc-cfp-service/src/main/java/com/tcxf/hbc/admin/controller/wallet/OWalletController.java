package com.tcxf.hbc.admin.controller.wallet;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.vo.WalletIncomeVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/opera/owallet")
public class OWalletController extends BaseController {

    @Autowired
    protected ITbWalletService iTbWalletService;
    @Autowired
    protected ITbWalletIncomeService iTbWalletIncomeService;
    @Autowired
    protected ITbWalletWithdrawalsService iTbWalletWithdrawalsService;
    @Autowired
    protected ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    protected IOOperaInfoService iOOperaInfoService;
    @Autowired
    protected ITbWalletCashrateService iTbWalletCashrateService;
    @Autowired
    protected  MessageUtil messageUtil;

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> template;

    /**
     * 查看指定钱包-运营商
     */
    @RequestMapping("/Ofindwallets")
    public ModelAndView Ofindwallets(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        String oid = (String) get(request,"session_opera_id");
        TbWallet wallet = iTbWalletService.getfindOWallet(oid);
        wallet.setTotalAmount(new Calculate(wallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        TbWalletIncome dto = iTbWalletIncomeService.getamount(TbWalletIncome.TYPE_1, wallet.getId());
        TbWalletIncome dto1 = iTbWalletIncomeService.getamount(TbWalletIncome.TYPE_2, wallet.getId());
        TbWalletIncome dto2 = iTbWalletIncomeService.getamount(TbWalletIncome.TYPE_3, wallet.getId());
        TbWalletIncome dto3 = iTbWalletIncomeService.getamount(TbWalletIncome.TYPE_4, wallet.getId());
        mode.addObject("rje", dto.getAmount());
        mode.addObject("txcje", dto1.getAmount());
        mode.addObject("xfcje", dto2.getAmount());
        mode.addObject("sxfje", dto3.getAmount());
        mode.addObject("w", wallet);
        mode.setViewName("manager/walletlist");
        return mode;
    }

    /**
     * 查看指定钱包支出收入记录-运营商
     */
    @RequestMapping(value = "/ofindWalletIncome", method = RequestMethod.POST)
    @ResponseBody
    public R findWalletIncome(WalletIncomeVo walletIncomeVo,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("type", walletIncomeVo.getType());
        params.put("wid", walletIncomeVo.getWid());
        params.put("page",request.getParameter("page"));
        params.put("limit",request.getParameter("limit"));
        params.put("pname",request.getParameter("pname"));
        params.put("pmobile",request.getParameter("pmobile"));
        Page page = iTbWalletIncomeService.getWalletIncomeList(params);
        r.setData(page);
        return r;
    }

    /**
     * 跳转钱包提现-运营商
     */
    @RequestMapping("/OwallettxManager")
    public ModelAndView OwallettxManager(HttpServletRequest request) {

        String oid = (String) get(request,"session_opera_id");
        ModelAndView mode = new ModelAndView();
        Map<String, Object> params = new HashMap<>();
        params.put("oid", oid);
        TbWallet wallet = iTbWalletService.getfindOWallet(oid);
        wallet.setTotalAmount(new Calculate(wallet.getTotalAmount()).format(2,RoundingMode.DOWN));
        List<TbBindCardInfo> infos = iTbBindCardInfoService.selectBindCard(params);
        TbWalletCashrate utype = iTbWalletCashrateService.selectOne(new EntityWrapper<TbWalletCashrate>().eq("utype", 3));
        mode.addObject("Cashrate",utype);
        mode.addObject("w", wallet);
        mode.addObject("b", infos);
        mode.setViewName("manager/wallettx");
        return mode;
    }


    /**
     * 跳转到提现完成界面
     * @param request
     * @return
     */
    @RequestMapping("/wallettxwcManager")
    public ModelAndView wallettxwcManager(HttpServletRequest request) {
        ModelAndView mode = new ModelAndView();
        TbBindCardInfo info = iTbBindCardInfoService.selectById(request.getParameter("bid"));
        mode.addObject("info",info);
        mode.addObject("sjtxje", request.getParameter("sjtxje"));
        mode.addObject("txsxje", request.getParameter("txsxje"));
        mode.addObject("txje", request.getParameter("txje"));
        mode.setViewName("manager/wallettxwc");
        return mode;
    }


    /**
     * 获取短信验证码
     *
     * @return
     */
    @RequestMapping("/getsys")
    public R getsys(HttpServletRequest request) {
        R<Object> objectR = new R<>();
        String oid = (String) get(request,"session_opera_id");
        String mobile = request.getParameter("mobile");
        //String mobile ="17621621521";
        String type ="YZM";
        String code = messageUtil.Getmsmcode();
        String [] strArray = new String [20];
        strArray[0]=code;
        strArray[1]="5";
        messageUtil.sendSms(mobile,oid,type,strArray);
        objectR.setCode(1);
        return objectR;
    }


    /**
     * 钱包提现申请
     * @param request
     */
    @RequestMapping("/walletdoReg")
    public R walletdoReg(HttpServletRequest request) {
        R<Object> objectR = new R<>();
        String id = (String) get(request,"session_opera_id");
        String type ="YZM";
        Map<String, Object> map = new HashMap<>();
        map.put("uid", id);
        map.put("type",type);
        map.put("yCode", request.getParameter("yCode"));
        map.put("bid", request.getParameter("bid"));
        map.put("txje", request.getParameter("txje"));
        map.put("sjtxje", request.getParameter("sjtxje"));
        map.put("txsxje", request.getParameter("txsxje"));
        map.put("txsxje", request.getParameter("txsxje"));

        //设置缓存
        Long sendTime = null;
        if ( template.opsForValue().get("CODE_SEND_TIME"+id)!= null){
            sendTime = Long.parseLong(template.opsForValue().get("CODE_SEND_TIME"+id).toString());
        }
        System.out.println(sendTime);
        Long now = new Date().getTime();
        if (sendTime != null && (now - sendTime) < 1000 * 60) {
            throw new CheckedException("请等待" + (60l - ((now - sendTime) / 1000)) + "秒后再次发送");
        }
        // 1分钟过期
        template.opsForValue().set(("CODE_SEND_TIME"+id), (new Date().getTime() + ""),60,TimeUnit.SECONDS);
        try {
            objectR = iTbWalletService.updateWalletInfo(map);
        } catch (Exception e) {
            if (e instanceof  CheckedException){
                objectR.setMsg("提现申请失败,请稍后再试");
                objectR.setCode(R.FAIL);
                return objectR;
            }
        }
        if(objectR.getCode()==R.SUCCESS){
            template.delete("CODE_SEND_TIME"+id);
        }
        return objectR;
      /*  String smsCode = request.getParameter("yCode");
        String bid = request.getParameter("bid");
        String type ="YZM";
        //设置缓存
        Long sendTime = null;
        if ( template.opsForValue().get("CODE_SEND_TIME"+id)!= null){
            sendTime = Long.parseLong(template.opsForValue().get("CODE_SEND_TIME"+id).toString());
        }
        System.out.println(sendTime);
        Long now = new Date().getTime();
        if (sendTime != null && (now - sendTime) < 1000 * 60) {
            throw new CheckedException("请等待" + (60l - ((now - sendTime) / 1000)) + "秒后再次发送");
        }
        // 1分钟过期
        template.opsForValue().set(("CODE_SEND_TIME"+id), (new Date().getTime() + ""),60,TimeUnit.SECONDS);

          TbBindCardInfo info = iTbBindCardInfoService.selectOne(new EntityWrapper<TbBindCardInfo>().eq("id",bid));
          TbWallet wallet = iTbWalletService.getfindOWallet(id);
          BigDecimal y =  Calculate.subtract(wallet.getTotalAmount(), new BigDecimal(request.getParameter("txje"))).getAmount();
          //调用短信验证方法验证
			if (!messageUtil.valiCode(info.getMobile(), smsCode, type)) {
                objectR.setMsg("短信验证码错误");
                objectR.setCode(0);
                return objectR;
			}
        //验证钱包余额
        if (!Calculate.greaterOrEquals(wallet.getTotalAmount() , new BigDecimal(request.getParameter("txje")))){
            objectR.setMsg("钱包余额不足");
            objectR.setCode(0);
            return objectR;
        }
			//验证通过插入一条提现申请记录
        TbWalletWithdrawals w = new TbWalletWithdrawals();
        w.setUid(id);
        w.setState("1");
        w.setApplyTime(new Date());
        w.setUname(info.getName());
        w.setUmobile(info.getMobile());
        w.setBankCardNo(info.getBankCard());
        w.setKhh(info.getBankName());
        w.setTxAmount( new BigDecimal(request.getParameter("txje")));
        w.setDzAmount( new BigDecimal(request.getParameter("sjtxje")));
        w.setWname(wallet.getWname());//钱包名称
        w.setServiceFee(new BigDecimal(request.getParameter("txsxje")));
        w.setCreateDate(new Date());
        iTbWalletWithdrawalsService.insert(w);
        //改变钱包的余额
        wallet.setTotalAmount(y);
        boolean id1 = iTbWalletService.update(wallet, new EntityWrapper<TbWallet>().eq("id", wallet.getId()));
        //插入一条提现记录
        TbWalletIncome income = new TbWalletIncome();
        income.setAmount(new BigDecimal(request.getParameter("txje")));
        income.setWid(wallet.getId());
        income.setPlayid(id);
        income.setPname(wallet.getUname());
        income.setType("6");
        income.setTradeTotalAmount(new BigDecimal(request.getParameter("txje")));
        income.setPtepy("4");
        income.setPmobile(wallet.getUmobil());
        income.setCreateDate(new Date());
        income.setSettlementStatus("1");
        income.setTid(wallet.getId());
        boolean b = iTbWalletIncomeService.insert(income);
        if (id1 == true && b == true){
            objectR.setMsg("提现申请成功");
            objectR.setCode(1);
            template.delete("CODE_SEND_TIME"+id);
        }else {
            objectR.setMsg("提现申请失败");
            objectR.setCode(0);
        }
        return objectR;*/
    }

    /**
     * 查看所有钱包提现记录-平台
     */
    @RequestMapping("/findWalletwithdrawalsList")
    @ResponseBody
    public R findWalletwithdrawalsList(HttpServletRequest request) {
        String id = (String) get(request,"session_opera_id");
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("uid",id);
        Page page = iTbWalletWithdrawalsService.findWalletwithdrawalsList(params);
        r.setData(page);
        return r;

    }
    /**
     * 跳转钱包提现记录-平台
     */
    @RequestMapping("/findWalletTx")
    public ModelAndView findWalletTx() {
        ModelAndView mode= new ModelAndView();
        mode.setViewName("manager/wallettxjl");
        return mode;

    }
}
