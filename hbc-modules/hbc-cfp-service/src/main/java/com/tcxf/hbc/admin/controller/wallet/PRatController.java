package com.tcxf.hbc.admin.controller.wallet;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.service.ITbWalletCashrateService;
import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.tcxf.hbc.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/platfrom/rat")
public class PRatController {
    @Autowired
     ITbWalletCashrateService iTbWalletCashrateService;

    @RequestMapping("/urat")
    public ModelAndView urat(){
        ModelAndView model = new ModelAndView();
        TbWalletCashrate tb1 = iTbWalletCashrateService.selectOne(new EntityWrapper<TbWalletCashrate>().eq("utype",1));
        TbWalletCashrate tb3 = iTbWalletCashrateService.selectOne(new EntityWrapper<TbWalletCashrate>().eq("utype",3));
        TbWalletCashrate tb4 = iTbWalletCashrateService.selectOne(new EntityWrapper<TbWalletCashrate>().eq("utype",4));
        model.addObject("tb1",tb1);
        model.addObject("tb3",tb3);
        model.addObject("tb4",tb4);
        model.setViewName("platfrom/WalleRat");
        return model;
    }

    @RequestMapping("/rat")
    public R rat(String id,String content){
        R r = new R();
        TbWalletCashrate tb = iTbWalletCashrateService.selectById(id);
        tb.setPresentContent(content);
        if(iTbWalletCashrateService.updateById(tb)){
            r.setMsg("保存成功");
        }else {
            r.setMsg("保存失败");
        }
        return r;
    }


    @RequestMapping("/mmRate")
    public ModelAndView mmRate(){
        ModelAndView model = new ModelAndView();
        model.setViewName("platfrom/WalleMRat");
        return model;
    }


    @RequestMapping("/mRatList")
    public R mRatList(){
        List<TbWalletCashrate> list = iTbWalletCashrateService.selectList(new EntityWrapper<TbWalletCashrate>().eq("utype",2).orderBy("ladder_class"));
        R r = new R();

        r.setData(list);
        return r;
    }

    @RequestMapping("/delete")
    public R delete(String id){
        R r = new R();
        if(iTbWalletCashrateService.deleteById(id)){
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return  r;
    }

    @RequestMapping("/mrat")
    public  R mrat(String id,String ladderClass,String min,String max,String content,String type){
        R r = new R();
        TbWalletCashrate tb = new TbWalletCashrate();
        tb.setLadderClass(ladderClass);
        tb.setMaximumIntervalValue(max);
        tb.setMinimumIntervalValue(min);
        tb.setPresentType(type);
        tb.setPresentContent(content);
        if(id!=null && !"".equals(id)) {
            tb.setId(id);
            if (iTbWalletCashrateService.updateById(tb)) {
                r.setMsg("修改成功");
            } else {
                r.setMsg("修改失败");
            }
        }else {
            tb.setUtype("2");
            if(iTbWalletCashrateService.insert(tb)){
                r.setMsg("添加成功");
            }else{
                r.setMsg("添加失败");
            }
        }
        return r;
    }
}
