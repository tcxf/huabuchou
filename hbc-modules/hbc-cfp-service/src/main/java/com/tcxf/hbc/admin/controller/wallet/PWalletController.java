package com.tcxf.hbc.admin.controller.wallet;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.basecontroller.BaseController;
import com.tcxf.hbc.admin.model.vo.WallerVo;
import com.tcxf.hbc.admin.model.vo.WalletIncomeVo;
import com.tcxf.hbc.admin.model.vo.WalletWithdrawalsVo;
import com.tcxf.hbc.admin.service.ITbWalletIncomeService;
import com.tcxf.hbc.admin.service.ITbWalletService;
import com.tcxf.hbc.admin.service.ITbWalletWithdrawalsService;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/platfrom/wallet")
public class PWalletController extends BaseController {
    @Autowired
    protected ITbWalletService iTbWalletService;
    @Autowired
    protected ITbWalletIncomeService ITbWalletIncomeService;
    @Autowired
    protected ITbWalletWithdrawalsService iTbWalletWithdrawalsService;
    @Autowired
    protected  ITbWalletIncomeService iTbWalletIncomeService;


    /**
     * 跳转钱包首页
     */
    @RequestMapping(value = "/wallets", method = RequestMethod.GET)
    public ModelAndView wallets() {
        ModelAndView mode = new ModelAndView();
        mode.setViewName("platfrom/walletManager");
        return mode;
    }

    /**
     * 查看所有钱包-平台
     */
    @RequestMapping("/findwallet")
    public R findWallet(HttpServletRequest request) {
        R r = new R<>();
        Map<String, Object> params = new HashMap<>();
        ModelAndView mode = new ModelAndView();
        params.put("page", request.getParameter("page"));
        params.put("limit", request.getParameter("limit"));
        params.put("wname", request.getParameter("name"));
        params.put("utype", request.getParameter("utype"));
        Page page = iTbWalletService.getWalletList(params);
        r.setData(page);
        return r;
    }

    /**
     * 查看指定钱包-平台
     */
    @RequestMapping("/findwallets")
    public ModelAndView findWallets(WallerVo wallerVo) {
        ModelAndView mode = new ModelAndView();
        TbWallet wallet = iTbWalletService.getfindWallet(wallerVo.getId());
        TbWalletIncome dto = ITbWalletIncomeService.getamount(TbWalletIncome.TYPE_1, wallerVo.getId());
        TbWalletIncome dto1 = ITbWalletIncomeService.getamount(TbWalletIncome.TYPE_2, wallerVo.getId());
        TbWalletIncome dto2 = ITbWalletIncomeService.getamount(TbWalletIncome.TYPE_3, wallerVo.getId());
        mode.addObject("rje", dto.getAmount());
        mode.addObject("txcje", dto1.getAmount());
        mode.addObject("xfcje", dto2.getAmount());
        mode.addObject("w", wallet);
        mode.setViewName("platfrom/walletlist");
        return mode;
    }

    /**
     * 查看所有钱包支出收入记录-平台
     */
    @RequestMapping(value = "/findWalletIncome", method = RequestMethod.POST)
    @ResponseBody
    public R findWalletIncome(WalletIncomeVo walletIncomeVo,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("type", walletIncomeVo.getType());
        params.put("wid", walletIncomeVo.getWid());
        params.put("page", request.getParameter("page"));
        params.put("limit", request.getParameter("limit"));
        Page page = ITbWalletIncomeService.getWalletIncomeList(params);
        r.setData(page);

        return r;


    }

    /**
     * 查看指定钱包提现记录-平台
     */
    @RequestMapping(value = "/findWalletwithdrawalsBYid", method = RequestMethod.POST)
    @ResponseBody
    public R findWalletwithdrawalsBYid(WalletIncomeVo walletIncomeVo,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        Page page =  new Page<>();
        TbWallet wallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("id", walletIncomeVo.getWid()));
        if (!ValidateUtil.isEmpty(wallet.getUid())){
        params.put("uid", wallet.getUid());
        params.put("page", request.getParameter("page"));
        params.put("limit", request.getParameter("limit"));
         page = iTbWalletWithdrawalsService.findWalletwithdrawalsList(params);
        r.setData(page);
        }else {
            r.setData(page);
        }
        return r;


    }

    /**
     * 查看所有钱包提现记录-平台
     */
    @RequestMapping("/findWalletwithdrawalsList")
    @ResponseBody
    public R findWalletwithdrawalsList(WalletWithdrawalsVo walletWithdrawalsVo, HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("uid", walletWithdrawalsVo.getUid());
        params.put("wname", walletWithdrawalsVo.getWname());
        params.put("page", request.getParameter("page"));
        params.put("limit", request.getParameter("limit"));
        Page page = iTbWalletWithdrawalsService.findWalletwithdrawalsList(params);
        r.setData(page);
        return r;

    }

    /**
     * 跳转钱包提现记录-平台
     */
    @RequestMapping("/findWalletTx")
    public ModelAndView findWalletTx() {
        ModelAndView mode = new ModelAndView();
        mode.setViewName("platfrom/wallettxjl");
        return mode;

    }

    /**
     * 钱包申请提现确认-平台
     */
    @RequestMapping("/findWalletTxsh")
    @ResponseBody
    public R findWalletTxsh(HttpServletRequest request) {
        TbWalletWithdrawals t = new TbWalletWithdrawals();
        t.setState(request.getParameter("type"));
        t.setArriveTime(new Date());
        boolean b = iTbWalletWithdrawalsService.update(t, new EntityWrapper<TbWalletWithdrawals>().eq("id", Long.parseLong(request.getParameter("id"))));
        R<Object> r = new R<>();
        if (b) {
            if (request.getParameter("type").equals("3")){
                TbWalletWithdrawals one = iTbWalletWithdrawalsService.selectOne(new EntityWrapper<TbWalletWithdrawals>().eq("id", request.getParameter("id")));
                TbWallet wallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid", one.getUid()));
                BigDecimal zje = wallet.getTotalAmount().add(one.getTxAmount());
                wallet.setTotalAmount(zje);
                boolean uid = iTbWalletService.update(wallet, new EntityWrapper<TbWallet>().eq("uid", one.getUid()));
                //插入一条钱包记录
                TbWalletIncome income = new TbWalletIncome();
                income.setAmount(one.getTxAmount());
                income.setWid(wallet.getId());
                income.setPlayid(one.getUid());
                income.setPname(wallet.getUname());
                income.setType("5");
                income.setTradeTotalAmount(one.getTxAmount());
                income.setPtepy(wallet.getUtype());
                income.setPmobile(wallet.getUmobil());
                income.setCreateDate(new Date());
                income.setSettlementStatus("1");
                boolean b1 = iTbWalletIncomeService.insert(income);
                if (uid == true && b1 == true){
                    r.setCode(1);
                    r.setMsg("操作成功");
                }else {
                    r.setCode(0);
                    r.setMsg("操作失败");
                }
            }
            r.setCode(1);
            r.setMsg("操作成功");
        }
        return r;
    }

    /**
     * 改变钱包状态（冻结和启用）-平台
     */
    @RequestMapping("/upWalletST")
    @ResponseBody
    public R upWalletST(HttpServletRequest request) {
        TbWallet w = iTbWalletService.selectById(Long.parseLong(request.getParameter("id")));
        if (w.getStuta() == 1) {
            w.setStuta(2);
            iTbWalletService.update(w, new EntityWrapper<TbWallet>().eq("id", Long.parseLong(request.getParameter("id"))));
        } else if (w.getStuta() == 2) {
            w.setStuta(1);
            iTbWalletService.update(w, new EntityWrapper<TbWallet>().eq("id", Long.parseLong(request.getParameter("id"))));
        }
        R<Object> r = new R<>();
        return r;

    }
}
