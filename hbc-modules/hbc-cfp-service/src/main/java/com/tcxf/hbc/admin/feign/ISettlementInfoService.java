package com.tcxf.hbc.admin.feign;


import com.tcxf.hbc.admin.feign.fallback.SettlementInfoServiceImpl;
import com.tcxf.hbc.common.util.R;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-trans-service", fallback = SettlementInfoServiceImpl.class)
public interface ISettlementInfoService
{
    @RequestMapping(value = "/settlement/doSettlement/{sid}",method = {RequestMethod.POST})
    R<Boolean> Settlements(@PathVariable("sid") String sid);
}
