package com.tcxf.hbc.admin.feign;

import com.tcxf.hbc.admin.feign.fallback.SettlementInfoServiceImpl;
import com.tcxf.hbc.admin.feign.fallback.TbCreditGradeServiceImpl;
import com.tcxf.hbc.common.util.R;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "hbc-ctu-service", fallback = TbCreditGradeServiceImpl.class)
public interface ITbCreditGradeService {
    @RequestMapping(value = "/CreditScoreController/tbUpMoney/{uid}",method = {RequestMethod.POST})
    R CreditScoreone(@PathVariable("uid") String uid);

    @RequestMapping(value = "/CreditScoreController/tbCreditGrade/{uid}",method = {RequestMethod.POST})
    R CreditScoretow(@PathVariable("uid") String uid);
}
