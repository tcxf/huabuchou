package com.tcxf.hbc.admin.feign.fallback;

import com.tcxf.hbc.admin.feign.ISettlementInfoService;
import com.tcxf.hbc.common.util.R;
import org.springframework.stereotype.Service;

@Service
public class SettlementInfoServiceImpl implements ISettlementInfoService {
    protected org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    @Override
    public R Settlements(String sid) {
        logger.error("dosettlemnt error sid ="+ sid );
        return null;
    }
}
