package com.tcxf.hbc.admin.feign.fallback;

import com.tcxf.hbc.admin.feign.ITbCreditGradeService;
import com.tcxf.hbc.common.util.R;
import org.springframework.stereotype.Service;

@Service
public class TbCreditGradeServiceImpl implements ITbCreditGradeService {
    protected org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
    @Override
    public R CreditScoreone(String uid) {
        logger.error("CreditScoreone erroe"+uid);
        return null;
    }

    @Override
    public R CreditScoretow(String uid) {
        logger.error("CreditScoretow erroe"+uid);
        return null;
    }
}
