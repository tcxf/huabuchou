package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.BaseInterfaceInfo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface BaseInterfaceInfoMapper extends BaseMapper<BaseInterfaceInfo> {

    public BaseInterfaceInfo getBaseInterfaceInfo(String name);

    public List<BaseInterfaceInfo> getBaseInterfaceInfoList();

}
