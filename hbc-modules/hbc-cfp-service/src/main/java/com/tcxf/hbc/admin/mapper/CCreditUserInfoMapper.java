package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.CCreditUserInfoDto;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
public interface CCreditUserInfoMapper extends BaseMapper<CCreditUserInfo> {

    public List<CCreditUserInfoDto> Bank (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatoryd (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlt (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatordx (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Multipleliabilities (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Phone (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Getloginmode (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlogin (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Socialsecurity (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Accumulationfund (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> Learningletter (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> TaoBao (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> JD (Query<CCreditUserInfoDto> query , Map<String , Object> map);

    public List<CCreditUserInfoDto> DDZMF (Query<CCreditUserInfoDto> query , Map<String , Object> map);



//以下是poi导出

    public List<CCreditUserInfoDto> Bank (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatoryd (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlt (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatordx (Map<String , Object> map);

    public List<CCreditUserInfoDto> Multipleliabilities (Map<String , Object> map);

    public List<CCreditUserInfoDto> Phone (Map<String , Object> map);

    public List<CCreditUserInfoDto> Getloginmode (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlogin (Map<String , Object> map);

    public List<CCreditUserInfoDto> Socialsecurity (Map<String , Object> map);

    public List<CCreditUserInfoDto> Accumulationfund (Map<String , Object> map);

    public List<CCreditUserInfoDto> Learningletter (Map<String , Object> map);

    public List<CCreditUserInfoDto> TaoBao (Map<String , Object> map);

    public List<CCreditUserInfoDto> JD (Map<String , Object> map);

    public List<CCreditUserInfoDto> DDZMF (Map<String , Object> map);




}
