package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提额申请审核记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface CUserCreditApplyMapper extends BaseMapper<CUserCreditApply> {

}
