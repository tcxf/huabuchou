package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.CreditPageInfoDto;
import com.tcxf.hbc.admin.model.dto.CuserinfoDto;
import com.tcxf.hbc.admin.model.dto.QueryCreditRecordDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface CUserInfoMapper extends BaseMapper<CUserInfo> {

    /**
     * 查询消费者
     */
     List<CuserinfoDto> userlist(Query<CuserinfoDto> query, Map<String, Object> condition);

    int selectConsumercount(Map<String,Object> mmp);

    /**
     * 消费者基本信息详情
     */
    public CuserinfoDto QueryAll(@Param("uid") String uid);

    int findAlreadyAuthNum(Map<String,Object> paramMap);

    List<CuserinfoDto> OQueryAll(Query<CuserinfoDto> query,Map<String,Object> condition);
    /**
     * 今日新增消费者人数
     * @param start
     * @param end
     * @return
     */
    public int CreateQueryAllDate(@Param("start") String start ,@Param("end") String end);

    /**
     * 资金端今日新增消费者人数
     * @param start
     * @param end
     * @return
     */
    public int FCreateQueryAllDate(@Param("start") String start ,@Param("end") String end,@Param("fid") String fid);

    /**
     * 风控系统授信管理查询
     */
    List<QueryCreditRecordDto> QueryCreditRecord(Query<QueryCreditRecordDto> query, Map<String, Object> condition);

    List<QueryCreditRecordDto> QueryCreditRecord(Map<String,Object> map);


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:21 2018/9/17
     * 查询用户授信明细(已授信、已拒绝)
     */
    List<CreditPageInfoDto> findCuserCreditPageInfo(Query<CreditPageInfoDto> query, Map<String,Object> condition);
}
