package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 消费者用户其他信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface CUserOtherInfoMapper extends BaseMapper<CUserOtherInfo> {

}
