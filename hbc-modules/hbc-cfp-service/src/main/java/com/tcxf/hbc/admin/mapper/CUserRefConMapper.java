package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.CUserRefCon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface CUserRefConMapper extends BaseMapper<CUserRefCon> {
    /**
     * 查询资金端捆绑的用户
     * @param uid
     * @param fid
     * @return
     */
    CUserRefCon  finduserCon(Map<String,Object> map);

    int findthisVersionNumByRefusingCode(HashMap<String, Object> paramMap);
}
