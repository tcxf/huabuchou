package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.CreditDaily;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信日报表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
public interface CreditDailyMapper extends BaseMapper<CreditDaily> {
    /**
     * 查询授信记录
     */
    List<CreditDaily> getList(Query<CreditDaily> query, Map<String, Object> condition);
}
