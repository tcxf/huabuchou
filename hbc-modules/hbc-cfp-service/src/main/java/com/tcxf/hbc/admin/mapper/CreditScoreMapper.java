package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.CreditScore;
import com.tcxf.hbc.common.entity.RefusingVersion;

import java.util.List;
import java.util.Map;

/**
 * 授信提额综合分值
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface CreditScoreMapper extends BaseMapper<CreditScore> {

    List<CreditScore> selectCreditScoreByfId(Map<String, Object> map);
}
