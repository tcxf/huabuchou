package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 资金端、运营商绑定关系表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface FFundendBindMapper extends BaseMapper<FFundendBind> {

    /**
     * 按条件查询资金端资源表集合
     * @param fid 资金端id
     * @return
     */
    public List<FFundendBind> getOperaIdList(@Param("fid")String fid);
}
