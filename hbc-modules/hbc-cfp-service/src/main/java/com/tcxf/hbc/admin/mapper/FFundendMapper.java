package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.FFundendDto;
import com.tcxf.hbc.common.entity.FFundend;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金端信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface FFundendMapper extends BaseMapper<FFundend> {

    /**
     * 根据运营商查询资金端
     * @param query
     * @param map
     * @return
     */
    public List<FFundendDto> fundendByOidList(Query<FFundendDto> query , Map<String , Object> map);

    /**
     * 查询资金端详情信息
     * @param map
     * @return
     */
    public List<FFundendDto> selectFundendDetail(Map<String , Object> map);


    /**
     * 查询资金端下的运营商
     * @param query
     * @param condition
     * @return
     */
    List <FFundendDto>QueryFO(Query<FFundendDto> query,Map<String,Object> condition);
    /**
     * 更具Id查询资金端下的运营商详情
     * @return
     */

    public FFundendDto QueeryFOone(@Param("ffid") String id);
}
