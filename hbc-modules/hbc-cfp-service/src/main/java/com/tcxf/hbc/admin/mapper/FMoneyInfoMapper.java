package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.FMoneyInfo;

/**
 * <p>
 * 资金端授信上限表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
public interface FMoneyInfoMapper extends BaseMapper<FMoneyInfo> {

}
