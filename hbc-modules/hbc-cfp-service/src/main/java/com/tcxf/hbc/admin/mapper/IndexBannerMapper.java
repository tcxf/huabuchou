package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.IndexBanner;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 首页轮播图表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IndexBannerMapper extends BaseMapper<IndexBanner> {

}
