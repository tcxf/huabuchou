package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.MHotSearchDto;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 热搜店铺表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MHotSearchMapper extends BaseMapper<MHotSearch> {

    /**
     * 查询热门搜索分页
     * @param query
     * @param map
     * @return
     */
    List<MHotSearchDto> selectHotSearchByRid(Query<MHotSearchDto> query, Map<String , Object> map);

    /**
     * 热门搜索修改
     * @param map
     * @return
     */
    MHotSearchDto selectHotSearchByRid(Map<String , Object> map);
}
