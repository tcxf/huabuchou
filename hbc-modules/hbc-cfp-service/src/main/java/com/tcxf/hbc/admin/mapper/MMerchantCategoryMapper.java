package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商户行业分类表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MMerchantCategoryMapper extends BaseMapper<MMerchantCategory> {

}
