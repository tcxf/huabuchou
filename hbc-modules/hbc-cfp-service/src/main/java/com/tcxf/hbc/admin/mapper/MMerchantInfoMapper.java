package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.MMerchantDto;
import com.tcxf.hbc.admin.model.dto.MMerchantInfoDto;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商户信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MMerchantInfoMapper extends BaseMapper<MMerchantInfo> {

    List<MMerchantInfoDto> findMerchantManagerPageInfo(Query<MMerchantInfoDto> query,Map<String, Object> condition);
    /**
     * 分页查询所有商户信息
     * @param query
     * @param condition
     * @return
     */
    List<MMerchantDto> QueryAll(Query<MMerchantDto> query,Map<String,Object> condition);

    public MMerchantDto QueryOne(@Param("id") String id);

    int selectMerchantCount(Map<String,Object> paramMap);

    /**
     * 热门搜索查询商户列表
     * liaozeyong
     * @param query
     * @param map
     * @return
     */
    List<MMerchantDto> findByOid(Query<MMerchantInfoDto> query ,Map<String , Object> map);

    int findAlreadyAuthNum(Map<String,Object> paramMap);

    List<MMerchantInfo> findByLike(Query query,Map<String,Object> map);
}
