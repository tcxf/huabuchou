package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商户其他信息 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MMerchantOtherInfoMapper extends BaseMapper<MMerchantOtherInfo> {

}
