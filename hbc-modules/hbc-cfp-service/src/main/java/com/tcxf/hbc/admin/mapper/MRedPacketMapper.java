package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户领取优惠券记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MRedPacketMapper extends BaseMapper<MRedPacket> {

}
