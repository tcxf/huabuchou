package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.OredpacketDto;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优惠券表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MRedPacketSettingMapper extends BaseMapper<MRedPacketSetting> {

    /**
     * 按条件查询优惠
     * @param
     * @return
     */
    public List<OredpacketDto> findredlistQuery(Query<OredpacketDto> query, Map<String, Object> condition);

}
