package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.admin.model.dto.OperaInfoByFundend;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 运营商信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface OOperaInfoMapper extends BaseMapper<OOperaInfo> {
    /**
     * 根据时间查询今日新增商户
     * @param start 开始时间 （截取系统当前时间 +00:00:00）
     * @param end 结束时间（第二天系统当前时间 +00:00:00）
     * @return 返回int类型结果集
     */
    public int CreateQueryAllDate(@Param("start")String start, @Param("end") String end);

    /**
     *查询全部可用的商户
     * @return 返回int类型结果集
     */
    public int CountQueryOperator();

    /**
     * 分页查询所有商户
     * @return
     */
    public List<MerchantSerlect> QueryAll(Query<MerchantSerlect> query, Map<String, Object> condition );

    /**
     * 根据id查询资金端下的运营商
     * @return
     */
    public List<OOperaInfo> selectOperaInfoByFid(Query<OOperaInfo> query , Map<String , Object> condition , @Param("id")String id );

    /**
     * 查询运营商下的所有资料
     * @return
     */
    public MerchantSerlect QueeryAllOperaInfoDetail(@Param("id") String id);

    /***
     * 查寻运行商下的银行卡信息
     *
     */
    public List<MerchantSerlect> QueryBanks(Query<MerchantSerlect> query, Map<String, Object> condition );

    /**
     * 根据资金端查询已绑定和未绑定运营商
     * @param query
     * @return
     */
    List<OperaInfoByFundend> selectOperaByFundend(Query<OperaInfoByFundend> query ,Map<String ,Object> mapss);


    /**
     * 根据资金端查询已绑定和未绑定运营商
     * @param query
     * @return
     */
    public List<OOperaInfo> selectOperaByFid(@Param("id")String id );

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 18:38 2018/6/20
     * 根据条件查询运营商数量
    */
    int selectOperaCount(Map<String,Object> paramMap);

    List<OOperaInfo> selectOperaByfid(Map<String,Object> paramMap);

}
