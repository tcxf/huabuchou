package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.OOperaOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 运营商其他信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface OOperaOtherInfoMapper extends BaseMapper<OOperaOtherInfo> {

}
