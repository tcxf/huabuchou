package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.admin.model.dto.roleDto;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台用户信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface PAdminMapper extends BaseMapper<PAdmin> {


//    /***
//     * 查询所有的管理员信息
//     * @param query
//     * @param map
//     * @return
//     */
//    public List<PAdmin> getAdminList(Query<PAdmin> query, Map<Object,Object> map);
    //public List<roleDto> queryPermission(@Param(user_name));

    List<PAdmin> findByLike(Query query,Map<String,Object> map );

}
