package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.PNotice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
public interface PNoticeMapper extends BaseMapper<PNotice> {

}
