package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RefusingCredit;

import java.util.List;
import java.util.Map;

/**
 * 拒绝授信
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface RefusingCreditMapper extends BaseMapper<RefusingCredit> {

    List<RefusingCredit> selectRefusingCreditByfId(Map<String, Object> map);
}
