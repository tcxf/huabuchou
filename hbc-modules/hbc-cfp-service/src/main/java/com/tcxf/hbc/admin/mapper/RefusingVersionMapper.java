package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RefusingCredit;
import com.tcxf.hbc.common.entity.RefusingVersion;

import java.util.List;

/**
 * 授信版本
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface RefusingVersionMapper extends BaseMapper<RefusingVersion> {


}
