package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.RmBaseCreditInitialDto;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 初始授信条件模板表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
public interface RmBaseCreditInitialMapper extends BaseMapper<RmBaseCreditInitial> {

    //根据类型，版本号获得初始模板
    public List<RmBaseCreditInitial> getListByType(RmBaseCreditInitial rmBaseCreditInitial);

    List<RmBaseCreditInitialDto> findBaseCreditInitialInfo(HashMap<String,Object> paramMap);

    int findNumberOverDueByDetail(HashMap<String, Object> paramMap);

    int findNumberOverDueByInterval(HashMap<String, Object> paramMap);

    BigDecimal findNumberOverDueMoneyByDetail(HashMap<String,Object> paramMap);

    BigDecimal findNumberOverDueMoneyByInterval(HashMap<String,Object> paramMap);

    int findpeopleNumByGradingSegment(HashMap<String, Object> paramMap);

    int findoverDueNumByGradingSegment(HashMap<String, Object> paramMap);

    BigDecimal findoverDueMoneyByGradingSegment(HashMap<String, Object> paramMap);

}
