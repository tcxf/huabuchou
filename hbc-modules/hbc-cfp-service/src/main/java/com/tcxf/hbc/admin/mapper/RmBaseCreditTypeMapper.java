package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.RmBaseCreditType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 授信条件类别表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface RmBaseCreditTypeMapper extends BaseMapper<RmBaseCreditType> {

}
