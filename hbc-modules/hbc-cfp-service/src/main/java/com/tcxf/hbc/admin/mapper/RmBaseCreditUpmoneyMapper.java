package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提额授信条件模板表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface RmBaseCreditUpmoneyMapper extends BaseMapper<RmBaseCreditUpmoney> {

}
