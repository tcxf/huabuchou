package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.RefusingVersion;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 授信条件版本表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface RmBaseCreditVersionMapper extends BaseMapper<RefusingVersion> {

    public String getRefusingVersionByFid(Map<String, Object> map);

}
