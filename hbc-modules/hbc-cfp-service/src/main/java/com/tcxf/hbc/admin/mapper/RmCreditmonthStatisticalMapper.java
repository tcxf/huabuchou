package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.BindFODto;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信月份统计表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-25
 */
public interface RmCreditmonthStatisticalMapper extends BaseMapper<RmCreditmonthStatistical> {
  List<RmCreditmonthStatistical> QueryCredituserstatistics (Query<RmCreditmonthStatistical> query, Map<String, Object> condition );

  List<BindFODto> QueryfOs(Map<String, Object> params);

  BindFODto sumzsx();

  BindFODto sumsx();

  List<BindFODto> sumzsxed();

  List<BindFODto> sumsxed();
}
