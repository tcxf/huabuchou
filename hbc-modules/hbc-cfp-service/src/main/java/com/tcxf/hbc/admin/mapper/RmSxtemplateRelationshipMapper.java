package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 授信模板关系表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
public interface RmSxtemplateRelationshipMapper extends BaseMapper<RmSxtemplateRelationship> {

}
