package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户授信资金额度表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbAccountInfoMapper extends BaseMapper<TbAccountInfo> {

}
