package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 授信资金流水 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbAccountTradingLogMapper extends BaseMapper<TbAccountTradingLog> {

}
