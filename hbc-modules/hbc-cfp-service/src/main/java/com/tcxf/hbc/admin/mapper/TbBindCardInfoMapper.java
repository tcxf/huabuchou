package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 绑卡信息 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbBindCardInfoMapper extends BaseMapper<TbBindCardInfo> {

    /**
     * 查询所有银行卡信息
     */
    public List<TbBindCardInfo> QueryAllBank();

    /**
     * 资金端编辑，查看绑卡信息详情
     * liaozeyong
     * @return
     */
    public List<TbBindCardInfo> selectBindCardList(Query<TbBindCardInfo> query , Map<String , Object> condition);

    /**
     * 根据用户ID查看默认绑卡信息详情
     * liaozeyong
     * @return
     */
    public List<TbBindCardInfo> selectBindCard(Map<String , Object> condition);


    /**
     * 运营商绑卡
     * @return
     */
    TbBindCardInfo banko(@PathVariable("oid")String oid);

    /**
     * 商户帮卡
     * @return
     */
    TbBindCardInfo bankm(@PathVariable("oid")String oid);

}
