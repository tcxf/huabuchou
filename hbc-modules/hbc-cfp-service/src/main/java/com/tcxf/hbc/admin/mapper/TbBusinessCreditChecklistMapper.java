package com.tcxf.hbc.admin.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbBusinessCreditChecklist;

/**
 * <p>
 * 商业授信审核表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
public interface TbBusinessCreditChecklistMapper extends BaseMapper<TbBusinessCreditChecklist> {

}
