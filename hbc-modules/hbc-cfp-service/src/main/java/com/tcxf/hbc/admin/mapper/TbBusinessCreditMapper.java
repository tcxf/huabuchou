package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbBusinessCredit;

import java.util.HashMap;

/**
 * <p>
 * 商业授信表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
public interface TbBusinessCreditMapper extends BaseMapper<TbBusinessCredit> {

    public TbBusinessCredit getTbBusinessCreditByUid(HashMap <String, Object> map);


    public HashMap<String, Object> getTbBusinessCreditDetailsByUid(HashMap <String, Object> map);

}
