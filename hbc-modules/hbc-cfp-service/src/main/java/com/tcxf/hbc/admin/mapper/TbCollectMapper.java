package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbCollect;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 我的收藏表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface TbCollectMapper extends BaseMapper<TbCollect> {

}
