package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 快速授信评分项 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
public interface TbCreditGradesMapper extends BaseMapper<TbCreditGrade> {

    int findcountByDetailNameAndVersion(Map<String,Object> paramMap);

    int findcountByDetailNumAndVersion(Map<String, Object> paramMap);

    int findcountByDetailMinNumAndVersion(Map<String, Object> paramMap);
}
