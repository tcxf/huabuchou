package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 短信记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbMessageLogMapper extends BaseMapper<TbMessageLog> {

}
