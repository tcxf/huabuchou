package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbMessageTemplate;

public interface TbMessageTemplateMapper extends BaseMapper<TbMessageTemplate> {
}
