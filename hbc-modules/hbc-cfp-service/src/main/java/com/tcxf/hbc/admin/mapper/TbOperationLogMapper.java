package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbOperationLogMapper extends BaseMapper<TbOperationLog> {

}
