package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbPayment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 支付记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbPaymentMapper extends BaseMapper<TbPayment> {

}
