package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 后台用户角色关系表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbPeopleRoleMapper extends BaseMapper<TbPeopleRole> {

}
