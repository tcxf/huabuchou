package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.TbRepayLogDto;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRepayLogMapper extends BaseMapper<TbRepayLog> {

    List<TbRepayLogDto> findAdvancePayPageInfo(Query<TbRepayLogDto> query,Map<String, Object> condition);

    Map<String,Object> findAdvancePayTotalMoney(Map<String,Object> paramMap);
}
