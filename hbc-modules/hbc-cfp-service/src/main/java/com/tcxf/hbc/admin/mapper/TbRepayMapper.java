package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbRepay;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
public interface TbRepayMapper extends BaseMapper<TbRepay> {

}
