package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 还款统计表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
public interface TbRepayStatisticalMapper extends BaseMapper<TbRepayStatistical> {

}
