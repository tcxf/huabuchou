package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.RepaymentDetail;
import com.tcxf.hbc.admin.model.dto.RepaymentDetailDto;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRepaymentDetailMapper extends BaseMapper<TbRepaymentDetail> {

    int findAlreadyPayBacknotPlan(HashMap<String,Object> map);

    Integer findnotPayBacknotPlan(HashMap<String,Object> map);

    Integer findwaitPaynotPlan(HashMap<String,Object> map);

    List<RepaymentDetailDto> findPaybackPageInfo(Query<RepaymentDetailDto> query, Map<String,Object> condition);

    //消费者还款记录
    List<RepaymentDetailDto> findPaybackPageInfos(Query<RepaymentDetailDto> query, Map<String,Object> condition);

    List<RepaymentDetail> findpaybackDetailInfo(Query<RepaymentDetail> query,Map<String,Object> condition);

    /**
     * 平台消费者代换 逾期总额
     * @param map
     * @return
     */
    public  Map<String,Object> DHYQSUM (Map<String,Object> map);

    /**
     * 平台消费已还
     * @param map
     * @return
     */
    public  Map<String,Object> YHSUM (Map<String,Object> map);
}
