package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.RepaymentDto;
import com.tcxf.hbc.admin.model.dto.TbrmRepayDto;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款计划表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRepaymentPlanMapper extends BaseMapper<TbRepaymentPlan> {

    BigDecimal findoverDuePlan(Map<String,Object> map);

    BigDecimal findWaitPay(Map<String,Object> map);

    BigDecimal findAlreadyPayBackPlan(Map<String,Object> map);

    Map<String,Object> findwaitPayByCondiction(Map<String,Object> paramMap);

    Map<String,Object> findoverDuePayByCondiction(Map<String,Object> paramMap);

    Map<String,Object> findBillAlreadyPayByCondition(Map<String,Object> paramMap);

    int findoverDueNum(Map<String,Object> totalMap);

    /**
     * 本月应还分页
     * @param query
     * @param condition
     * @return
     */
    List<RepaymentDto> findRepaymentDetail(Query<RepaymentDto> query, Map<String,Object> condition);

    /**
     * 本月应还表单导出
     * liaozeyong
     * @param condition
     * @return
     */
    List<RepaymentDto> findRepaymentDetail(Map<String,Object> condition);

    List<TbrmRepayDto> findUserPaymentList(Map<String,Object> paramMap);
}
