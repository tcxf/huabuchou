package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 还款比率字典表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRepaymentRatioDictMapper extends BaseMapper<TbRepaymentRatioDict> {

}
