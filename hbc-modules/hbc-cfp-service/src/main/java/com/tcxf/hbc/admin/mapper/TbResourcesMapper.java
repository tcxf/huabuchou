package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbResources;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.Query;
import org.springframework.data.repository.query.Param;
import com.tcxf.hbc.admin.model.dto.tbResourcesDto;
import java.util.List;
import java.util.Map;


import java.util.List;


/**
 * <p>
 * 资源信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbResourcesMapper extends BaseMapper<TbResources> {

    /**
     * 根据权限id和权限type查询资源
     * @param map
     * @return
     */
    public List<TbResources> selectResources(Map<String,Object> map);

    public List<tbResourcesDto> getList(Query<tbResourcesDto> query, Map<String,Object> map);
}
