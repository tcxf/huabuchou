package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRoleMapper extends BaseMapper<TbRole> {

    /**
     * 查询角色
     * @param Map
     * @return
     */
    public List<TbRole> selectRole(Map<String , Object> Map);
}
