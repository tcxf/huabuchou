package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbRoleResources;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色资源关系表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbRoleResourcesMapper extends BaseMapper<TbRoleResources> {

    public List<TbRoleResources> findByResourceId(Map<String, Object> condition);
}
