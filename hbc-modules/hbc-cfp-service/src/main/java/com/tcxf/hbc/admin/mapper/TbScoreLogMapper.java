package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbScoreLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户积分明细表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbScoreLogMapper extends BaseMapper<TbScoreLog> {

}
