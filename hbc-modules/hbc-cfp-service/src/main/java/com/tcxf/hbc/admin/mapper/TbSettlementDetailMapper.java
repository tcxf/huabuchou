package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.common.util.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  交易结算表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbSettlementDetailMapper extends BaseMapper<TbSettlementDetail> {

    BigDecimal findAlreadySettled(Map<String,Object> map);

    BigDecimal findWaitSettled(Map<String,Object> map);

    List<TbSettlementDetailDto> findClearDetailPageInfo(Query<TbSettlementDetailDto> query, Map<String,Object> condition);

    /**
     * 运营商查询商户结算
     * @param condition
     * @return
     */
    List<TbSettlementDetailDto> findOperaSettlement(Query<TbSettlementDetailDto> query, Map<String,Object> condition);

    /**
     * 查询已结算总额和未结算总额
     * @param map
     * @return
     */
    int totalAmount(Map<String , Object> map);

    List<TbSettlementDetailDto> findSettlementPageInfo(Query<TbSettlementDetailDto> query, Map<String,Object> condition);

    Map<String,Object> fanYongInfo(Map<String,Object> paramMap);

    Map<String,Object> findSettlementHeadInfo(Map<String,Object> paramMap);

    Map<String,Object> findPFanYongWaitSet(Map<String,Object> paramMap);

    Map<String,Object> findPFanYongAlreadySet(Map<String,Object> paramMap);


    /**
     * 资金端结算记录
     * @param query
     * @param condition
     * @return
     */
    List<TbSettlementDetailDto> QueryALLSettlement(Query<TbSettlementDetailDto> query, Map<String,Object> condition);


    List<TbSettlementDetailDto> QueryALLSettlement(Map<String,Object> map);
    /**
     * 资金端结算管理待结算金额
     * @param paramMap
     * @return
     */
    Map<String,Object> DAmount (Map<String,Object> paramMap);

    /**
     * 资金端结算管理已结算金额
     * @param paramMap
     * @return
     */
    Map<String,Object> YAmount (Map<String,Object> paramMap);

    /**
     * 资金端结算管理运营商返佣 商户返佣 会员返佣
     * @param paramMap
     * @return
     */
    Map<String,Object> Maids (Map<String,Object> paramMap);

    /**
     * 资金端交易明细
     * @param query
     * @param condition
     * @return
     */
    List<TbSettlementDetailDto> TransactionDetails(Query<TbSettlementDetailDto> query, Map<String,Object> condition);

}
