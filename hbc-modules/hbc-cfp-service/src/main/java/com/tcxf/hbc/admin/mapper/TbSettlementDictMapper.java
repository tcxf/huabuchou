package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbSettlementDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 结算方式比例字典表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbSettlementDictMapper extends BaseMapper<TbSettlementDict> {

}
