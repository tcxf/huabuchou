package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbSettlementLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  结算支付记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbSettlementLogMapper extends BaseMapper<TbSettlementLog> {

}
