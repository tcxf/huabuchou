package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 三级分销利润分配表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbThreeSaleProfitShareMapper extends BaseMapper<TbThreeSaleProfitShare> {

}
