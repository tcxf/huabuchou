package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 三级分销利润分配比例表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbThreeSalesRateMapper extends BaseMapper<TbThreeSalesRate> {

}
