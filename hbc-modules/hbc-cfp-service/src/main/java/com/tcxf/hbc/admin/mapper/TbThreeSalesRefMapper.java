package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbThreeSalesRef;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 三级分销用户关系绑定表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbThreeSalesRefMapper extends BaseMapper<TbThreeSalesRef> {

}
