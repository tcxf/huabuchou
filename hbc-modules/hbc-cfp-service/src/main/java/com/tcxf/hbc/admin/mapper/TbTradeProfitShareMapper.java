package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbTradeProfitShare;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 交易利润分配表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbTradeProfitShareMapper extends BaseMapper<TbTradeProfitShare> {

}
