package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.model.dto.UnoutSettmentDto;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户交易记录表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbTradingDetailMapper extends BaseMapper<TbTradingDetail> {
    public Map<String,Object> findsevendaytrade(Map<String,Object> paramMap);

    public BigDecimal selectTotalMoney(Map<String,Object> map);
        BigDecimal selectTotalMoneyx(Map<String,Object>map);
    List<TbTradingDetailDto> findAdvancePayDetailInfo(Query<TbTradingDetailDto> query,Map mmp);

    List<TbTradingDetailDto> findNoBillPageInfo(Query<TbTradingDetailDto> query,Map<String,Object> condition);
    List<TbTradingDetailDto> findNoBillPageInfo(Map<String,Object> condition);

    List<TbTradingDetailDto> findTradingPageInfo(Query<TbTradingDetailDto> query,Map<String, Object> condition);

    List<TbTradingDetailDto> uncreateSettlementDetail(Query<TbTradingDetailDto> query,Map<String, Object> condition);

    /**
     * 查询已还账单明细
     * @param condition
     * @return
     */
    List<TbTradingDetailDto> findTradingDetail(Query<TbTradingDetailDto> query,Map<String , Object> condition);

    /**
     * 查询未出账单金额总额
     * @param map
     * @return
     */
    BigDecimal uncreateAmount(Map<String,Object> map);

    List<TbTradingDetailDto> findOperaPageInfo(Query<TbTradingDetailDto> query, Map<String,Object> condition);

    Map<String,Object> findTotalProfit(Map<String,Object> paramMap);

    /**
     * 运营商查询商户让利
     * @param query
     * @param condition
     * @return
     */
    List<TbTradingDetailDto> msfManagerList(Query<TbTradingDetailDto> query,Map<String , Object> condition);

    /**
     * 下载报表查询总条数
     * @param map
     * @return
     */
    int msfTotal(Map<String , Object> map);

    /**
     * 运营商查询商户让利下载报表
     * liaozeyong
     * @param map
     * @return
     */
    List<TbTradingDetailDto> msf(Map<String , Object> map);

    /**
     * 商户让利总金额
     * @param map
     * @return
     */
    BigDecimal merchantTotalAmount(Map<String , Object> map);

    //    运营商交易总额
    int TotalMoney(Map<String,Object> map);

    List<TbTradingDetailDto> findSettlementDetailInfo(Query<TbTradingDetailDto> query, Map<String,Object> condition);

    /**
     * 平台交易记录分页查询
     * @param query
     * @param condition
     * @return
     */
    List <TransactionDto>transaction(Query<TransactionDto> query,Map<String,Object> condition);

    List<TransactionDto> transaction(Map<String,Object> map);


    //    运营商交易总额
    public  Map<String,Object> SUM(Map<String,Object> map);


    List<UnoutSettmentDto> findNoSettlementPageInfo(Query<UnoutSettmentDto> query, Map<String,Object> condition);

    List<TbTradingDetailDto> findMerchantSettlementDetail(Query<TbTradingDetailDto> query, Map<String,Object> condition);

    Map<String,Object> findNoBillTotalMoney(Map<String,Object> paramMap);
}
