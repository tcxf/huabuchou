package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbUpMoney;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 授信提额表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
public interface TbUpMoneyMapper extends BaseMapper<TbUpMoney> {

}
