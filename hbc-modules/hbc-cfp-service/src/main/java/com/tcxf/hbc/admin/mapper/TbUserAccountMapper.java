package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户账户表（包含消费者、商户） Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbUserAccountMapper extends BaseMapper<TbUserAccount> {

}
