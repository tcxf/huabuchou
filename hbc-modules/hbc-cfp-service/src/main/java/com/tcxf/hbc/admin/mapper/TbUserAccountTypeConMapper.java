package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户类型表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbUserAccountTypeConMapper extends BaseMapper<TbUserAccountTypeCon> {

}
