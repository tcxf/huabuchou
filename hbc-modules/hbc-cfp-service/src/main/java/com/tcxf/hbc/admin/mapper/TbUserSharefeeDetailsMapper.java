package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbUserSharefeeDetails;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户分润明细表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface TbUserSharefeeDetailsMapper extends BaseMapper<TbUserSharefeeDetails> {

}
