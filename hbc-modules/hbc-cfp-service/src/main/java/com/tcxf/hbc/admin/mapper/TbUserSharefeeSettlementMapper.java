package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbUserSharefeeSettlement;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户分润结算表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface TbUserSharefeeSettlementMapper extends BaseMapper<TbUserSharefeeSettlement> {

}
