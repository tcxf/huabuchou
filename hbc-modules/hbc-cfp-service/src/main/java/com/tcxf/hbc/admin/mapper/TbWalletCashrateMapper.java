package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
public interface TbWalletCashrateMapper extends BaseMapper<TbWalletCashrate> {

}
