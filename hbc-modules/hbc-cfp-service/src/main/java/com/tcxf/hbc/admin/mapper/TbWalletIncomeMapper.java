package com.tcxf.hbc.admin.mapper;


import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbWalletIncomeMapper extends BaseMapper<TbWalletIncome> {
    /**
     * 查询钱包的收入支出记录
     */
    public List<TbWalletIncome> getWalletIncomeList(Query<TbWalletIncome> query, Map<String, Object> condition);
    /**
     * 查询收支明细 1.为入金 2.为出金
     * @param type
     * @return
     */
    TbWalletIncome getamount(Map<String, Object> condition);
}
