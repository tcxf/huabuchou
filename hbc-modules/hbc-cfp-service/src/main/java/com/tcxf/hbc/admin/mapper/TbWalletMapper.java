package com.tcxf.hbc.admin.mapper;

import com.tcxf.hbc.common.entity.TbWallet;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbWalletMapper extends BaseMapper<TbWallet> {
    /**
     * 查询所有钱包
     */
    public List<TbWallet> getWalletList(Query<TbWallet> query, Map<String, Object> condition);

    /**
     * 查询指定钱包
     */
    public TbWallet getfindWallet(Map<String, Object> condition);
}
