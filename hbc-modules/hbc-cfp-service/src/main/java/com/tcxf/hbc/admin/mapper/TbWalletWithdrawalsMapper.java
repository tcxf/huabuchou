package com.tcxf.hbc.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.admin.model.dto.TbWalletWithdrawalsDto;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbWalletWithdrawalsMapper extends BaseMapper<TbWalletWithdrawals> {
    /**
     * 查询钱包的提现记录
     */
     List<TbWalletWithdrawalsDto> findWalletwithdrawalsList(Query<TbWalletWithdrawalsDto> query, Map<String, Object> condition);



}
