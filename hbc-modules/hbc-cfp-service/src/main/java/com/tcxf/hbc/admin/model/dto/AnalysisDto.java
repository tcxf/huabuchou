package com.tcxf.hbc.admin.model.dto;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 19:37 2018/9/26
 */
public class AnalysisDto {
    private  String name;
    private BigDecimal value;
    private  int sort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    @Override
    public String toString() {
        return "AnalysisDto{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", sort=" + sort +
                '}';
    }
}
