package com.tcxf.hbc.admin.model.dto;

/**
 * @author Jiangyong
 * @describe
 * @time 2018/10/10
 */
public class BindFODto {
    private String fid;
    private String oid;
    private String fname;
    private String oname;
    private String countpople;
    private String countpoples;
    private String sunmoeny;
    private String sunmoenys;

    public String getCountpople() {
        return countpople;
    }

    public void setCountpople(String countpople) {
        this.countpople = countpople;
    }

    public String getCountpoples() {
        return countpoples;
    }

    public void setCountpoples(String countpoples) {
        this.countpoples = countpoples;
    }

    public String getSunmoeny() {
        return sunmoeny;
    }

    public void setSunmoeny(String sunmoeny) {
        this.sunmoeny = sunmoeny;
    }

    public String getSunmoenys() {
        return sunmoenys;
    }

    public void setSunmoenys(String sunmoenys) {
        this.sunmoenys = sunmoenys;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }
}
