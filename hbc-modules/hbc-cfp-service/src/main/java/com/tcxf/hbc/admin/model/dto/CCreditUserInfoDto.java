package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.CCreditUserInfo;

/**
 * @author Jiangyong
 * @describe
 * @time 2018/9/19
 */
public class CCreditUserInfoDto extends CCreditUserInfo {
    private String corporatename;
    private String money;
    private String counts;
    private  String operatorType;

    @Override
    public String getOperatorType() {
        return operatorType;
    }

    @Override
    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getCorporatename() {
        return corporatename;
    }

    public void setCorporatename(String corporatename) {
        this.corporatename = corporatename;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }
}
