package com.tcxf.hbc.admin.model.dto;

/**
 * @author YWT_tai
 * @Date :Created in 15:23 2018/9/17
 */
public class CreditPageInfoDto {

    private  String  id;//用户id
    private  String  mobile;//手机号
    private  String  realName;//用户姓名
    private  String  authStatus;//授信状态
    private  String  grade;//分数
    private  String  maxMoney;//授信额度
    private  String  balance; //可用授信余额
    private  String  creditTime; //用户授信时间
    private  String  userAttribute; //用户类型


    public String getUserAttribute() {
        return userAttribute;
    }

    public void setUserAttribute(String userAttribute) {
        this.userAttribute = userAttribute;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(String maxMoney) {
        this.maxMoney = maxMoney;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCreditTime() {
        return creditTime;
    }

    public void setCreditTime(String creditTime) {
        this.creditTime = creditTime;
    }

    @Override
    public String toString() {
        return "CreditPageInfoDto{" +
                "id='" + id + '\'' +
                ", mobile='" + mobile + '\'' +
                ", realName='" + realName + '\'' +
                ", authStatus='" + authStatus + '\'' +
                ", grade='" + grade + '\'' +
                ", maxMoney='" + maxMoney + '\'' +
                ", balance='" + balance + '\'' +
                ", creditTime='" + creditTime + '\'' +
                ", userAttribute='" + userAttribute + '\'' +
                '}';
    }
}
