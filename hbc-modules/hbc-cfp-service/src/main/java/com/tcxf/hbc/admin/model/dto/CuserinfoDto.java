package com.tcxf.hbc.admin.model.dto;
import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.CUserInfo;

import java.math.BigDecimal;
import java.util.Date;

public class CuserinfoDto extends CUserInfo {

    private  String name;
    private String authStatus;
    private String  bindCardStatus;
    private String attribute;
    private  String oid;
    private  String umobile;
    private  String mid;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField("auth_date")
    private Date authDate;

    private BigDecimal authMax;
    private String mname;//商户名称
    private String mmoblie;//商户手机号码
    private String area;//地区
    private String address;//地址
    private String uarea;//地区
    private String uaddress;//地址
    private String ooaDate;//出账时间
    private String repayDate;//还款时间
    private String rapidMoney;
    private String upMoney;
    private String totalMoney;
    private  String grade;

    public String getUarea() {
        return uarea;
    }

    public void setUarea(String uarea) {
        this.uarea = uarea;
    }

    public String getUaddress() {
        return uaddress;
    }

    public void setUaddress(String uaddress) {
        this.uaddress = uaddress;
    }

    public Date getAuthDate() {
        return authDate;
    }

    public void setAuthDate(Date authDate) {
        this.authDate = authDate;
    }

    public String getRapidMoney() {
        return rapidMoney;
    }

    public void setRapidMoney(String rapidMoney) {
        this.rapidMoney = rapidMoney;
    }

    public String getUpMoney() {
        return upMoney;
    }

    public void setUpMoney(String upMoney) {
        this.upMoney = upMoney;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getMmoblie() {
        return mmoblie;
    }

    public void setMmoblie(String mmoblie) {
        this.mmoblie = mmoblie;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getBindCardStatus() {
        return bindCardStatus;
    }

    public void setBindCardStatus(String bindCardStatus) {
        this.bindCardStatus = bindCardStatus;
    }

    public BigDecimal getAuthMax() {
        return authMax;
    }

    public void setAuthMax(BigDecimal authMax) {
        this.authMax = authMax;
    }
}
