package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.FFundend;

import java.math.BigDecimal;
import java.util.Date;

public class FFundendDto extends FFundend {

    private  String ffid;

    private String ooid;

    private String oname;

    private String omobile;

    private String simpleName;

    private String legalName;

    private String idCard;

    private String area;

    private String oaddress;

    private BigDecimal platfromShareRatio;

    private Integer scorePercent;

    private Long messageFee;
    private  String settlementLoop;

    public String getFfid() {
        return ffid;
    }

    public void setFfid(String ffid) {
        this.ffid = ffid;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getOaddress() {
        return oaddress;
    }

    public void setOaddress(String oaddress) {
        this.oaddress = oaddress;
    }

    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    public Integer getScorePercent() {
        return scorePercent;
    }

    public void setScorePercent(Integer scorePercent) {
        this.scorePercent = scorePercent;
    }

    public Long getMessageFee() {
        return messageFee;
    }

    public void setMessageFee(Long messageFee) {
        this.messageFee = messageFee;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getOmobile() {
        return omobile;
    }

    public void setOmobile(String omobile) {
        this.omobile = omobile;
    }

    /**
     * 逾期还款费率（百分比）
     */
    private BigDecimal yqRate;

    /**
     * 手续费利率（分期才有手续费）
     */
    private BigDecimal serviceFee;

    /**
     * 启用状态 （1：启用，0：不启用 ）
     */

    private String statusone;

    /**
     * 运营商状态
     */
    private String ostatus;

    public String getOstatus() {
        return ostatus;
    }

    public void setOstatus(String ostatus) {
        this.ostatus = ostatus;
    }

    public BigDecimal getYqRate() {
        return yqRate;
    }

    public void setYqRate(BigDecimal yqRate) {
        this.yqRate = yqRate;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getStatusone() {
        return statusone;
    }

    public void setStatusone(String statusone) {
        this.statusone = statusone;
    }
}
