package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.MHotSearch;

public class MHotSearchDto extends MHotSearch {
    private String mname;//商户名称

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }
}
