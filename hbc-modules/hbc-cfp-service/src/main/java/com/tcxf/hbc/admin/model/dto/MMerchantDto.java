package com.tcxf.hbc.admin.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.MMerchantInfo;

import java.math.BigDecimal;
import java.util.Date;

public class MMerchantDto extends MMerchantInfo {
    private String mname;

    public String getMmid() {
        return mmid;
    }

    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    private String bbid;

    public String getBbid() {
        return bbid;
    }

    public void setBbid(String bbid) {
        this.bbid = bbid;
    }

    /**
     * 主键id
     */
    private String mmid;
    /**
     * 商户主键id，唯一
     */
    private String mid;
    /**
     * 商户性质
     */
    private String nature;
    /**
     * 商户行业类型名称
     */
    private String mcname;


    private  String bstatus;

    private String bauthmax;

    private String bcreatedate;

    private String bbstatus;

    private String approverName;

    private String bbmoblie;

    private String endMoeny;

    private String bid;

    private String bbcreatedate;

    private BigDecimal minmoney;

    private String latestday;

    public BigDecimal getMinmoney() {
        return minmoney;
    }

    public void setMinmoney(BigDecimal minmoney) {
        this.minmoney = minmoney;
    }

    public String getLatestday() {
        return latestday;
    }

    public void setLatestday(String latestday) {
        this.latestday = latestday;
    }

    public String getBauthmax() {
        return bauthmax;
    }

    public void setBauthmax(String bauthmax) {
        this.bauthmax = bauthmax;
    }



    public String getBbstatus() {
        return bbstatus;
    }

    public void setBbstatus(String bbstatus) {
        this.bbstatus = bbstatus;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getBbmoblie() {
        return bbmoblie;
    }

    public void setBbmoblie(String bbmoblie) {
        this.bbmoblie = bbmoblie;
    }

    public String getEndMoeny() {
        return endMoeny;
    }

    public void setEndMoeny(String endMoeny) {
        this.endMoeny = endMoeny;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBcreatedate() {
        return bcreatedate;
    }

    public void setBcreatedate(String bcreatedate) {
        this.bcreatedate = bcreatedate;
    }

    public String getBbcreatedate() {
        return bbcreatedate;
    }

    public void setBbcreatedate(String bbcreatedate) {
        this.bbcreatedate = bbcreatedate;
    }

    public String getBstatus() {
        return bstatus;
    }

    public void setBstatus(String bstatus) {
        this.bstatus = bstatus;
    }

    public String getMcname() {
        return mcname;
    }

    public void setMcname(String mcname) {
        this.mcname = mcname;
    }

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public Integer getOperArea() {
        return operArea;
    }

    public void setOperArea(Integer operArea) {
        this.operArea = operArea;
    }

    public Integer getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Integer regMoney) {
        this.regMoney = regMoney;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }



    public Long getAuthMax() {
        return authMax;
    }

    public void setAuthMax(Long authMax) {
        this.authMax = authMax;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public Integer getClearDay() {
        return clearDay;
    }

    public void setClearDay(Integer clearDay) {
        this.clearDay = clearDay;
    }

    public Integer getShareFlag() {
        return shareFlag;
    }

    public void setShareFlag(Integer shareFlag) {
        this.shareFlag = shareFlag;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }

    public String getExamineFailReason() {
        return examineFailReason;
    }

    public void setExamineFailReason(String examineFailReason) {
        this.examineFailReason = examineFailReason;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRecommendGoods() {
        return recommendGoods;
    }

    public void setRecommendGoods(String recommendGoods) {
        this.recommendGoods = recommendGoods;
    }

    public String getSyauthExamine() {
        return syauthExamine;
    }

    public void setSyauthExamine(String syauthExamine) {
        this.syauthExamine = syauthExamine;
    }

    public String getSyexamineFailReason() {
        return syexamineFailReason;
    }

    public void setSyexamineFailReason(String syexamineFailReason) {
        this.syexamineFailReason = syexamineFailReason;
    }

    public Date getSyDate() {
        return syDate;
    }

    public void setSyDate(Date syDate) {
        this.syDate = syDate;
    }

    public String getZlStatus() {
        return zlStatus;
    }

    public void setZlStatus(String zlStatus) {
        this.zlStatus = zlStatus;
    }

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getBorrowingMultiJson() {
        return borrowingMultiJson;
    }

    public void setBorrowingMultiJson(String borrowingMultiJson) {
        this.borrowingMultiJson = borrowingMultiJson;
    }

    public String getBorrowingMultiStatus() {
        return borrowingMultiStatus;
    }

    public void setBorrowingMultiStatus(String borrowingMultiStatus) {
        this.borrowingMultiStatus = borrowingMultiStatus;
    }

    public String getInvestmentTenureJson() {
        return investmentTenureJson;
    }

    public void setInvestmentTenureJson(String investmentTenureJson) {
        this.investmentTenureJson = investmentTenureJson;
    }

    public String getInvestmentTenureStatus() {
        return investmentTenureStatus;
    }

    public void setInvestmentTenureStatus(String investmentTenureStatus) {
        this.investmentTenureStatus = investmentTenureStatus;
    }

    public String getBankCardJson() {
        return bankCardJson;
    }

    public void setBankCardJson(String bankCardJson) {
        this.bankCardJson = bankCardJson;
    }

    public String getBankCardStatus() {
        return bankCardStatus;
    }

    public void setBankCardStatus(String bankCardStatus) {
        this.bankCardStatus = bankCardStatus;
    }

    /**
     * 品牌经营年限
     */

    private Integer operYear;
    /**
     * 经营面积（㎡）
     */
    private Integer operArea;
    /**
     * 注册资金（万）
     */
    private Integer regMoney;
    /**
     * 场地照片资源路径（c端显示图片）
     */
    private String localPhoto;
    /**
     * 营业执照图片资源路径
     */
    private String licensePic;
    /**
     * 法人身份照片资源路径
     */
    private String legalPhoto;
    /**
     * 借记卡照片资源路径
     */
    private String normalCard;
    /**
     * 信用卡照片资源路径
     */
    private String creditCard;
    /**
     * 法人正面照资源路径
     */
    private String lagalFacePhoto;
    /**
     * 结算方式 0- t+1 , 1 - t+0
     */
    private String settlementType;

    public String getAuthMaxFlag() {
        return authMaxFlag;
    }

    public void setAuthMaxFlag(String authMaxFlag) {
        this.authMaxFlag = authMaxFlag;
    }

    /**
     * 授信是否有上限
     */
    private String authMaxFlag;
    /**
     * 授信上限值
     */
    private Long authMax;
    /**
     * 结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年）
     */
    private String settlementLoop;
    /**
     * 清算日期（结算日）0 开启 1 关闭
     */
    private Integer clearDay;
    /**
     * 运营商抽取商户服务费率（百分之）
     */
    private Integer shareFlag;
    /**
     * 商户让利费率（百分之）
     */
    private BigDecimal shareFee;
    /**
     * 创建时间
     */
    private Date mcreateDate;
    /**
     * 修改时间
     */

    private Date mmodifyDate;
    /**
     * 开店时间（运营商审核的时间）
     */
    private String openDate;
    /**
     * 0-未提交 1-待审核 2-审核失败 3-审核成功
     */
    private String authExamine;
    /**
     * 审核失败原因
     */
    private String examineFailReason;
    /**
     * 商家电话
     */
    private String phoneNumber;
    /**
     * 店铺推荐商品简介
     */
    private String recommendGoods;
    /**
     *  授信状态 0-未提交 1-待审核 2-审核中 3-审核成功 4审核失败5线下审核
     */
    private String syauthExamine;
    /**
     * 授信审核失败原因
     */
    private String syexamineFailReason;
    /**
     * 申请授信时间
     */
    private Date syDate;
    /**
     * 商家资料填写状态 0.未完成 1.已完成
     */
    private String zlStatus;
    /**
     *  出账日期
     */
    private String ooaDate;
    /**
     * 还款日期
     */
    private String repayDate;
    /**
     * 多头借贷接口json
     */
    private String borrowingMultiJson;
    /**
     * 多头借贷接口状态（0失败 1成功）
     */
    private String borrowingMultiStatus;
    /**
     * 个人投资任职接口json
     */
    private String investmentTenureJson;
    /**
     * 个人投资任职接口状态（0失败 1成功）
     */
    private String investmentTenureStatus;
    /**
     * 银行卡接口json
     */
    private String bankCardJson;
    /**
     * 银行卡接口状态（0失败 1成功）
     */
    private String bankCardStatus;

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Date getMcreateDate() {
        return mcreateDate;
    }

    public void setMcreateDate(Date mcreateDate) {
        this.mcreateDate = mcreateDate;
    }

    public Date getMmodifyDate() {
        return mmodifyDate;
    }

    public void setMmodifyDate(Date mmodifyDate) {
        this.mmodifyDate = mmodifyDate;
    }

    @Override
    public String toString() {
        return "MMerchantDto{" +
                "mname='" + mname + '\'' +
                ", mmid='" + mmid + '\'' +
                ", mid='" + mid + '\'' +
                ", nature='" + nature + '\'' +
                ", operYear=" + operYear +
                ", operArea=" + operArea +
                ", regMoney=" + regMoney +
                ", localPhoto='" + localPhoto + '\'' +
                ", licensePic='" + licensePic + '\'' +
                ", legalPhoto='" + legalPhoto + '\'' +
                ", normalCard='" + normalCard + '\'' +
                ", creditCard='" + creditCard + '\'' +
                ", lagalFacePhoto='" + lagalFacePhoto + '\'' +
                ", settlementType='" + settlementType + '\'' +
                ", authMaxFlag='" + authMaxFlag + '\'' +
                ", authMax=" + authMax +
                ", settlementLoop='" + settlementLoop + '\'' +
                ", clearDay=" + clearDay +
                ", shareFlag=" + shareFlag +
                ", shareFee=" + shareFee +
                ", mcreateDate=" + mcreateDate +
                ", mmodifyDate=" + mmodifyDate +
                ", openDate='" + openDate + '\'' +
                ", authExamine='" + authExamine + '\'' +
                ", examineFailReason='" + examineFailReason + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", recommendGoods='" + recommendGoods + '\'' +
                ", syauthExamine='" + syauthExamine + '\'' +
                ", syexamineFailReason='" + syexamineFailReason + '\'' +
                ", syDate=" + syDate +
                ", zlStatus='" + zlStatus + '\'' +
                ", ooaDate='" + ooaDate + '\'' +
                ", repayDate='" + repayDate + '\'' +
                ", borrowingMultiJson='" + borrowingMultiJson + '\'' +
                ", borrowingMultiStatus='" + borrowingMultiStatus + '\'' +
                ", investmentTenureJson='" + investmentTenureJson + '\'' +
                ", investmentTenureStatus='" + investmentTenureStatus + '\'' +
                ", bankCardJson='" + bankCardJson + '\'' +
                ", bankCardStatus='" + bankCardStatus + '\'' +
                '}';
    }
}