package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.MMerchantInfo;

/**
 * @author YWT_tai
 * @Date :Created in 16:30 2018/6/15
 */
public class MMerchantInfoDto extends MMerchantInfo {
    private String oname;//运营商名字
    private String merchantCate; //行业类型

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getMerchantCate() {
        return merchantCate;
    }

    public void setMerchantCate(String merchantCate) {
        this.merchantCate = merchantCate;
    }

    @Override
    public String toString() {
        return "MMerchantInfoDto{" +
                "oname='" + oname + '\'' +
                ", merchantCate='" + merchantCate + '\'' +
                '}';
    }
}
