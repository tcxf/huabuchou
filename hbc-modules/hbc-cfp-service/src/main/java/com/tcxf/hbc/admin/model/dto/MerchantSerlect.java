package com.tcxf.hbc.admin.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.OOperaInfo;

import java.math.BigDecimal;
import java.util.Date;

public class MerchantSerlect extends OOperaInfo {

    public String regMoney;

    public Integer merchantNum;

    public Integer userNum;

    public String oname;

    public String settlementLoop;

    public String getSettlementLoop() {
        return settlementLoop;
    }


    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    /**
     * 主键id
     */
    private String orherid;
    /**
     * 运营商主键id，唯一

     */
    private String oid;
    /**
     * 品牌经营年限
     */
    private Integer operYear;
    /**
     *  经营面积（㎡）
     */
    private Integer operArea;
    /**
     * 营业执照图片资源路径
     */
    private String licensePic;
    /**
     * 借记卡照片资源路径
     */
    private String normalCard;
    /**
     * 平台抽取运营商服务费率（百分之）
     */
    private BigDecimal platfromShareRatio;
    /**
     * 场地照片资源路径
     */
    private String legalPhoto;
    /**
     *  信用卡照片资源路径
     */
    private String creditCard;
    /**
     * 运营商性质
     */
    private String nature;
    /**
     * 场地照片资源路径
     */
    private String localPhoto;
    /**
     * 法人正面照资源路径
     */
    private String lagalFacePhoto;
    /**
     * 短信费用/条
     */
    private Long messageFee;
    /**
     * 积分返利比例 %（根据消费金额获取积分）
     */
    private BigDecimal scorePercent;
    /**
     * 记录修改时间
     */
    private Date modifyDate;
    /**
     * 记录创建时间
     */
    private Date createDate;


    private String bid;

    private String boid;

    private String utype;

    private String cardType;

    private String bankSn;

    private String mobile;

    private String bname;


    private String idCard;

    private String bankCard;
    private String valiDate;

    private String cvv;
    private String isDefault;


    private String bankName;

    private String contractId;


    private Date bcreateDate;

    private Date bmodifyDate;

    public String getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(String regMoney) {
        this.regMoney = regMoney;
    }

    public Integer getMerchantNum() {
        return merchantNum;
    }

    public void setMerchantNum(Integer merchantNum) {
        this.merchantNum = merchantNum;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public String getOrherid() {
        return orherid;
    }

    public void setOrherid(String orherid) {
        this.orherid = orherid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public Integer getOperArea() {
        return operArea;
    }

    public void setOperArea(Integer operArea) {
        this.operArea = operArea;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }

    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }

    public Long getMessageFee() {
        return messageFee;
    }

    public void setMessageFee(Long messageFee) {
        this.messageFee = messageFee;
    }

    public BigDecimal getScorePercent() {
        return scorePercent;
    }

    public void setScorePercent(BigDecimal scorePercent) {
        this.scorePercent = scorePercent;
    }

    @Override
    public Date getModifyDate() {
        return modifyDate;
    }

    @Override
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBoid() {
        return boid;
    }

    public void setBoid(String boid) {
        this.boid = boid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBankSn() {
        return bankSn;
    }

    public void setBankSn(String bankSn) {
        this.bankSn = bankSn;
    }

    @Override
    public String getMobile() {
        return mobile;
    }

    @Override
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    @Override
    public String getIdCard() {
        return idCard;
    }

    @Override
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Date getBcreateDate() {
        return bcreateDate;
    }

    public void setBcreateDate(Date bcreateDate) {
        this.bcreateDate = bcreateDate;
    }

    public Date getBmodifyDate() {
        return bmodifyDate;
    }

    public void setBmodifyDate(Date bmodifyDate) {
        this.bmodifyDate = bmodifyDate;
    }

    @Override
    public String toString() {
        return "MerchantSerlect{" +
                "regMoney='" + regMoney + '\'' +
                ", merchantNum=" + merchantNum +
                ", userNum=" + userNum +
                ", orherid='" + orherid + '\'' +
                ", oid='" + oid + '\'' +
                ", operYear=" + operYear +
                ", operArea=" + operArea +
                ", licensePic='" + licensePic + '\'' +
                ", normalCard='" + normalCard + '\'' +
                ", platfromShareRatio=" + platfromShareRatio +
                ", legalPhoto='" + legalPhoto + '\'' +
                ", creditCard='" + creditCard + '\'' +
                ", nature='" + nature + '\'' +
                ", localPhoto='" + localPhoto + '\'' +
                ", lagalFacePhoto='" + lagalFacePhoto + '\'' +
                ", messageFee=" + messageFee +
                ", scorePercent=" + scorePercent +
                ", modifyDate=" + modifyDate +
                ", createDate=" + createDate +
                ", bid='" + bid + '\'' +
                ", boid='" + boid + '\'' +
                ", utype='" + utype + '\'' +
                ", cardType='" + cardType + '\'' +
                ", bankSn='" + bankSn + '\'' +
                ", mobile='" + mobile + '\'' +
                ", bname='" + bname + '\'' +
                ", idCard='" + idCard + '\'' +
                ", bankCard='" + bankCard + '\'' +
                ", valiDate='" + valiDate + '\'' +
                ", cvv='" + cvv + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", bankName='" + bankName + '\'' +
                ", contractId='" + contractId + '\'' +
                ", bcreateDate=" + bcreateDate +
                ", bmodifyDate=" + bmodifyDate +
                '}';
    }
}
