package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.OOperaInfo;

public class OperaInfoByFundend extends OOperaInfo {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getTypeStr(){
        if(type!=null){
            if(type=="1"){
                return "已绑定";
            }else{
                return "立即绑定";
            }
        }
        return null;
    }
}
