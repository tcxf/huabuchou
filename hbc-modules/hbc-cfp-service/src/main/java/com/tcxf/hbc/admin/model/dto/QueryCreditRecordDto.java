package com.tcxf.hbc.admin.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.CUserInfo;

import java.util.Date;

/**
 * @author Jiangyong
 * @describe
 * @time 2018/9/17
 */
public class QueryCreditRecordDto extends CUserInfo {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date refCreateDate;
    private String maxMoney;
    private String balance;
    private String sumMoney;
    private String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(String sumMoney) {
        this.sumMoney = sumMoney;
    }

    public Date getRefCreateDate() {
        return refCreateDate;
    }

    public void setRefCreateDate(Date refCreateDate) {
        this.refCreateDate = refCreateDate;
    }

    public String getMaxMoney() {
        return maxMoney;
    }

    public void setMaxMoney(String maxMoney) {
        this.maxMoney = maxMoney;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
