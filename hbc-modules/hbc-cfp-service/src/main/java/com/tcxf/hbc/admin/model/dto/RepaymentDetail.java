package com.tcxf.hbc.admin.model.dto;

/**
 * @author YWT_tai
 * @Date :Created in 19:48 2018/6/11
 */
public class RepaymentDetail {

    private String id;
    private String repayMentSn;//还款详情流水号
    private Integer mid;
    private String legalName; //商户简称
    private String nature; //行业类型
    private Integer uid;
    private String realName;
    private String tradingType;
    private String tradingDate;
    private Integer actualAmount;//实付
    private String paymentStatus;//支付状态
    private Integer discountAmount;//折扣价格
    private Integer tradeAmount;//交易金额
    private String paymentSn;//支付编号 三方
    private String serialNo;//业务/交易 单号
    private String telPhone;//手机号
    private String paymentType;//支付产品类型:(1 授信 2 钱包 3 微信 4 快捷)

    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Integer tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepayMentSn() {
        return repayMentSn;
    }

    public void setRepayMentSn(String repayMentSn) {
        this.repayMentSn = repayMentSn;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTradingType() {
        return tradingType;
    }

    public void setTradingType(String tradingType) {
        this.tradingType = tradingType;
    }

    public String getTradingDate() {
        return tradingDate;
    }

    public void setTradingDate(String tradingDate) {
        this.tradingDate = tradingDate;
    }

    public Integer getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(Integer actualAmount) {
        this.actualAmount = actualAmount;
    }

    @Override
    public String toString() {
        return "RepaymentDetail{" +
                "id='" + id + '\'' +
                ", repayMentSn='" + repayMentSn + '\'' +
                ", mid=" + mid +
                ", legalName='" + legalName + '\'' +
                ", nature='" + nature + '\'' +
                ", uid=" + uid +
                ", realName='" + realName + '\'' +
                ", tradingType='" + tradingType + '\'' +
                ", tradingDate='" + tradingDate + '\'' +
                ", actualAmount=" + actualAmount +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", discountAmount=" + discountAmount +
                ", tradeAmount=" + tradeAmount +
                ", paymentSn='" + paymentSn + '\'' +
                ", serialNo='" + serialNo + '\'' +
                ", telPhone='" + telPhone + '\'' +
                '}';
    }
}
