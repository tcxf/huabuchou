package com.tcxf.hbc.admin.model.dto;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 9:55 2018/6/9
 */
public class RepaymentDetailDto {


    private  String id;
    private String rdid;
    private BigDecimal currentCorpus;
    private BigDecimal currentFee;
    private BigDecimal lateFee;
    private String repaymentDate;
    private int totalTime;
    private int currentTime;
    private String userId;
    private String telPhone;
    private String realName;
    private String oname;
    private String serialNo;
    private String margin;//相差天数
    private String repaymentType;//还款方式
    private String processDate;//还款日期

    private  String repayType;
    private BigDecimal DhSum;
    private BigDecimal YqSum;
    private BigDecimal YhSum;

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getDhSum() {
        return DhSum;
    }

    public void setDhSum(BigDecimal dhSum) {
        DhSum = dhSum;
    }

    public BigDecimal getYqSum() {
        return YqSum;
    }

    public void setYqSum(BigDecimal yqSum) {
        YqSum = yqSum;
    }

    public BigDecimal getYhSum() {
        return YhSum;
    }

    public void setYhSum(BigDecimal yhSum) {
        YhSum = yhSum;
    }

    public String getRepaymentType() {
        return repaymentType;
    }

    public void setRepaymentType(String repaymentType) {
        this.repaymentType = repaymentType;
    }

    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getRdid() {
        return rdid;
    }

    public void setRdid(String rdid) {
        this.rdid = rdid;
    }

    public BigDecimal getCurrentCorpus() {
        return currentCorpus;
    }

    public void setCurrentCorpus(BigDecimal currentCorpus) {
        this.currentCorpus = currentCorpus;
    }

    public BigDecimal getCurrentFee() {
        return currentFee;
    }

    public void setCurrentFee(BigDecimal currentFee) {
        this.currentFee = currentFee;
    }

    public BigDecimal getLateFee() {
        return lateFee;
    }
    public void setLateFee(BigDecimal lateFee) {
        this.lateFee = lateFee;
    }
    public String getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(String repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String toString() {
        return "RepaymentDetailDto{" +
                "id=" + id +
                ", rdid=" + rdid +
                ", currentCorpus=" + currentCorpus +
                ", currentFee=" + currentFee +
                ", lateFee=" + lateFee +
                ", repaymentDate='" + repaymentDate + '\'' +
                ", totalTime=" + totalTime +
                ", currentTime=" + currentTime +
                ", userId=" + userId +
                ", telPhone='" + telPhone + '\'' +
                ", realName='" + realName + '\'' +
                ", oname='" + oname + '\'' +
                ", serialNo='" + serialNo + '\'' +
                ", margin='" + margin + '\'' +
                ", repaymentType='" + repaymentType + '\'' +
                ", processDate='" + processDate + '\'' +
                '}';
    }

}
