package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.TbRepaymentPlan;

/**
 * @Auther: liaozeyong
 * @Date: 2018/9/18 15:41
 * @Description:
 */
public class RepaymentDto extends TbRepaymentPlan {
    private String realName;//姓名
    private String mobile;//手机号
    private String threeDay;//前三日提醒状态
    private String oneDay;//前一日提醒状态
    private String repaymentSum;//应还金额

    public String getRepaymentSum() {
        return repaymentSum;
    }

    public void setRepaymentSum(String repaymentSum) {
        this.repaymentSum = repaymentSum;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getThreeDay() {
        return threeDay;
    }

    public void setThreeDay(String threeDay) {
        this.threeDay = threeDay;
    }

    public String getOneDay() {
        return oneDay;
    }

    public void setOneDay(String oneDay) {
        this.oneDay = oneDay;
    }
}
