package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.RmBaseCreditInitial;

/**
 * @author YWT_tai
 * @Date :Created in 17:10 2018/9/27
 */
public class RmBaseCreditInitialDto extends RmBaseCreditInitial {
   private String typeCode;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @Override
    public String toString() {
        return "RmBaseCreditInitialDto{" +
                "typeCode='" + typeCode + '\'' +
                '}';
    }
}
