package com.tcxf.hbc.admin.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.entity.RmBaseCreditType;

import java.util.Date;
import java.util.List;

/**
 * @program: Workspace3
 * @description: 授信条件类别DTO
 * @author: JinPeng
 * @create: 2018-09-21 14:17
 **/
public class RmBaseCreditTypeDto extends RmBaseCreditType {

    /**
     * 主键id
     */
    private String id;
    /**
     * 条件描述
     */
    private String name;
    /**
     * 条件代码
     */
    private String code;
    /**
     * 条件状态（0.初始授信类别,1.授信提额类别,2.初始授信内置类别）
     */
    private String status;
    /**
     * 权重
     */
    private String value;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date modifyDate;

    private List<RmBaseCreditInitial> rbcilist;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public Date getModifyDate() {
        return modifyDate;
    }

    @Override
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public List<RmBaseCreditInitial> getRbcilist() {
        return rbcilist;
    }

    public void setRbcilist(List<RmBaseCreditInitial> rbcilist) {
        this.rbcilist = rbcilist;
    }
}
