package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.TbRepayLog;

/**
 * @author YWT_tai
 * @Date :Created in 14:00 2018/6/13
 */
public class TbRepayLogDto extends TbRepayLog {

    private String realName;//真实姓名

    private String oname;//运营商名称

    private String fname;//资金端名称

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String toString() {
        return "TbRepayLogDto{" +
                "realName='" + realName + '\'' +
                ", oname='" + oname + '\'' +
                ", fname='" + fname + '\'' +
                '}';
    }
}
