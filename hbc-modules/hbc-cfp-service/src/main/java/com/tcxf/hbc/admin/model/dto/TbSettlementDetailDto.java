package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.TbSettlementDetail;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author YWT_tai
 * @Date :Created in 11:25 2018/6/20
 */
public class TbSettlementDetailDto extends TbSettlementDetail {
    private String bankCard; //卡号
    private String bankName; //开户行
    private String legalName; //商户名称
    private String simpleName; //商户简称
    private String bankSn; //卡ping
    private String clearDay;//清算日
    private String settlementLoop;//清算周期
    private String mmoblie;//商户手机号
    private String wname;//钱包名字


    //资金端结算记录
    private String mname;//商户名
    private String oname;//运营商名字
    private BigDecimal oamount;//商户结算金额
    private BigDecimal operatorShareFee;//运营商结算
    private BigDecimal platfromShareFee;//平台结算
    private BigDecimal sumamount;//结算总金额
    private BigDecimal opaShareAmount;//运营商返佣
    private BigDecimal redirectShareAmount;//直接上级
    private BigDecimal inderectShareAmount;//间接上级
    private BigDecimal summoeny;//合计总额
    private String ooid;//运营商ID
    private BigDecimal dsumamount;//待结算金额
    private BigDecimal ysumamount; //已结算金额

//    private String operatorShareFee;//运营商结算金额
//    private String redirectShareAmount;//直接上级返佣
//    private String inderectShareAmount;//间接上级返佣
//    private String opaShareAmount;//运营商返佣
    private String mobile;
    private BigDecimal tradeAmount;
    private BigDecimal discountAmount;
    private BigDecimal actualAmount;
    private Date payDate;
    private String type;
    private String pstatus;
    private String dserialNo;
    private  String paymentSn;

    private String cumobile;
    private  String realName;

    public String getCumobile() {
        return cumobile;
    }

    public void setCumobile(String cumobile) {
        this.cumobile = cumobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getBankSn() {
        return bankSn;
    }

    public void setBankSn(String bankSn) {
        this.bankSn = bankSn;
    }

    public String getClearDay() {
        return clearDay;
    }

    public void setClearDay(String clearDay) {
        this.clearDay = clearDay;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public String getMmoblie() {
        return mmoblie;
    }

    public void setMmoblie(String mmoblie) {
        this.mmoblie = mmoblie;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public BigDecimal getOamount() {
        return oamount;
    }

    public void setOamount(BigDecimal oamount) {
        this.oamount = oamount;
    }

    public BigDecimal getOperatorShareFee() {
        return operatorShareFee;
    }

    public void setOperatorShareFee(BigDecimal operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public BigDecimal getPlatfromShareFee() {
        return platfromShareFee;
    }

    public void setPlatfromShareFee(BigDecimal platfromShareFee) {
        this.platfromShareFee = platfromShareFee;
    }

    public BigDecimal getSumamount() {
        return sumamount;
    }

    public void setSumamount(BigDecimal sumamount) {
        this.sumamount = sumamount;
    }

    public BigDecimal getOpaShareAmount() {
        return opaShareAmount;
    }

    public void setOpaShareAmount(BigDecimal opaShareAmount) {
        this.opaShareAmount = opaShareAmount;
    }

    public BigDecimal getRedirectShareAmount() {
        return redirectShareAmount;
    }

    public void setRedirectShareAmount(BigDecimal redirectShareAmount) {
        this.redirectShareAmount = redirectShareAmount;
    }

    public BigDecimal getInderectShareAmount() {
        return inderectShareAmount;
    }

    public void setInderectShareAmount(BigDecimal inderectShareAmount) {
        this.inderectShareAmount = inderectShareAmount;
    }

    public BigDecimal getSummoeny() {
        return summoeny;
    }

    public void setSummoeny(BigDecimal summoeny) {
        this.summoeny = summoeny;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    public BigDecimal getDsumamount() {
        return dsumamount;
    }

    public void setDsumamount(BigDecimal dsumamount) {
        this.dsumamount = dsumamount;
    }

    public BigDecimal getYsumamount() {
        return ysumamount;
    }

    public void setYsumamount(BigDecimal ysumamount) {
        this.ysumamount = ysumamount;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPstatus() {
        return pstatus;
    }

    public void setPstatus(String pstatus) {
        this.pstatus = pstatus;
    }

    public String getDserialNo() {
        return dserialNo;
    }

    public void setDserialNo(String dserialNo) {
        this.dserialNo = dserialNo;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }
}
