package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.TbTradingDetail;
import javafx.scene.layout.BackgroundImage;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 9:43 2018/6/14
 */
public class TbTradingDetailDto extends TbTradingDetail {
    private String  legalName;//商户名称

    private String nature;//行业类型

    private String realName;//用户名称

    private String telPhone;//手机号

    private String oname;//运营商名称

    private String fname;//资金端名称

    private BigDecimal pshareProfit;//分润

    private String mid;//商户id

    private String mname;//商户名称

    private String clearDay;//结算周期

    private String shareRadio;//运营商分润比例

    private String mmobile;//商户手机号

    private String umobile;//用户手机号

    private String nmobile;//用户手机号

    private String operatorShareFee;//商户让利金额

    private String legalTel;//商户手机

    private String name;

    private String OperaName;

    private String redirectUserType;//直接上级类型（1.商户，2.用户）

    private BigDecimal redirectShareAmount;// 间接上级返佣

    private String inderectUserType;//间接上级类型（1.商户，2.用户）

    private BigDecimal inderectShareAmount;//直接上级返佣

    private BigDecimal opaShareAmount;//运营商返佣

    private String pShareAmount;//平台返佣

    private String tpsCreateDate;//交易记录创建时间

    private String tpsModifyDate;//交易记录结算时间

    private String paymentType;//支付产品类型:(1 授信 2 钱包 3 微信 4 快捷)

    private String paymentSn;//三方支付返回支付单号

    private String operaShareFee;//商户让利金额，运营商通道费

    private String totalTradeAmount;//实付金额

    private String tids;//交易记录id 逗号隔开字符串

    private BigDecimal shareFee;

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public String getTids() {
        return tids;
    }

    public void setTids(String tids) {
        this.tids = tids;
    }

    public String getOperaShareFee() {
        return operaShareFee;
    }

    public void setOperaShareFee(String operaShareFee) {
        this.operaShareFee = operaShareFee;
    }

    public String getTotalTradeAmount() {
        return totalTradeAmount;
    }

    public void setTotalTradeAmount(String totalTradeAmount) {
        this.totalTradeAmount = totalTradeAmount;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public BigDecimal getPshareProfit() {
        return pshareProfit;
    }

    public void setPshareProfit(BigDecimal pshareProfit) {
        this.pshareProfit = pshareProfit;
    }

    @Override
    public String getMid() {
        return mid;
    }

    @Override
    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getClearDay() {
        return clearDay;
    }

    public void setClearDay(String clearDay) {
        this.clearDay = clearDay;
    }

    public String getShareRadio() {
        return shareRadio;
    }

    public void setShareRadio(String shareRadio) {
        this.shareRadio = shareRadio;
    }

    public String getMmobile() {
        return mmobile;
    }

    public void setMmobile(String mmobile) {
        this.mmobile = mmobile;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    public String getNmobile() {
        return nmobile;
    }

    public void setNmobile(String nmobile) {
        this.nmobile = nmobile;
    }

    public String getOperatorShareFee() {
        return operatorShareFee;
    }

    public void setOperatorShareFee(String operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public String getLegalTel() {
        return legalTel;
    }

    public void setLegalTel(String legalTel) {
        this.legalTel = legalTel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperaName() {
        return OperaName;
    }

    public void setOperaName(String operaName) {
        OperaName = operaName;
    }

    public String getRedirectUserType() {
        return redirectUserType;
    }

    public void setRedirectUserType(String redirectUserType) {
        this.redirectUserType = redirectUserType;
    }

    public BigDecimal getRedirectShareAmount() {
        return redirectShareAmount;
    }

    public void setRedirectShareAmount(BigDecimal redirectShareAmount) {
        this.redirectShareAmount = redirectShareAmount;
    }

    public String getInderectUserType() {
        return inderectUserType;
    }

    public void setInderectUserType(String inderectUserType) {
        this.inderectUserType = inderectUserType;
    }

    public BigDecimal getInderectShareAmount() {
        return inderectShareAmount;
    }

    public void setInderectShareAmount(BigDecimal inderectShareAmount) {
        this.inderectShareAmount = inderectShareAmount;
    }

    public BigDecimal getOpaShareAmount() {
        return opaShareAmount;
    }

    public void setOpaShareAmount(BigDecimal opaShareAmount) {
        this.opaShareAmount = opaShareAmount;
    }

    public String getpShareAmount() {
        return pShareAmount;
    }

    public void setpShareAmount(String pShareAmount) {
        this.pShareAmount = pShareAmount;
    }

    public String getTpsCreateDate() {
        return tpsCreateDate;
    }

    public void setTpsCreateDate(String tpsCreateDate) {
        this.tpsCreateDate = tpsCreateDate;
    }

    public String getTpsModifyDate() {
        return tpsModifyDate;
    }

    public void setTpsModifyDate(String tpsModifyDate) {
        this.tpsModifyDate = tpsModifyDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    @Override
    public String toString() {
        return "TbTradingDetailDto{" +
                "legalName='" + legalName + '\'' +
                ", nature='" + nature + '\'' +
                ", realName='" + realName + '\'' +
                ", telPhone='" + telPhone + '\'' +
                ", oname='" + oname + '\'' +
                ", fname='" + fname + '\'' +
                ", pshareProfit=" + pshareProfit +
                ", mname='" + mname + '\'' +
                ", clearDay='" + clearDay + '\'' +
                ", shareRadio='" + shareRadio + '\'' +
                ", legalTel='" + legalTel + '\'' +
                ", name='" + name + '\'' +
                ", OperaName='" + OperaName + '\'' +
                ", paymentSn='" + paymentSn + '\'' +
                '}';
    }
}
