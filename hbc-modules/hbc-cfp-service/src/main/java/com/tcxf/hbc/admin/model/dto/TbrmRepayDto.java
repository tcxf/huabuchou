package com.tcxf.hbc.admin.model.dto;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 16:42 2018/9/20
 */
public class TbrmRepayDto {
    private  String id;
    private  String waitpay;//、逾期待还 、正常待还、逾期还款、正常还款
    private  String repaymentDate;//还款日
    private  String payType;//还款类型
    private BigDecimal totalMoney;//本息
    private BigDecimal lateFee;//滞纳金

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaitpay() {
        return waitpay;
    }

    public void setWaitpay(String waitpay) {
        this.waitpay = waitpay;
    }

    public String getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(String repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getLateFee() {
        return lateFee;
    }

    public void setLateFee(BigDecimal lateFee) {
        this.lateFee = lateFee;
    }

    @Override
    public String toString() {
        return "TbrmRepayDto{" +
                "id='" + id + '\'' +
                ", waitpay='" + waitpay + '\'' +
                ", repaymentDate='" + repaymentDate + '\'' +
                ", payType='" + payType + '\'' +
                ", totalMoney=" + totalMoney +
                ", lateFee=" + lateFee +
                '}';
    }
}
