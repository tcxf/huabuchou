package com.tcxf.hbc.admin.model.dto;

import com.tcxf.hbc.common.entity.TbTradingDetail;

import java.math.BigDecimal;

public class TransactionDto extends TbTradingDetail {
    private String realName;
    private String mobile;
    private String name;
    private String mmobile;
    private BigDecimal operatorShareFee;
    private BigDecimal redirectShareAmount;
    private BigDecimal inderectShareAmount;
    private BigDecimal shareFee;
    private BigDecimal settlementAmounts;
    private  String isUseDiscout;
    private  String type;
    private  String paymentSn;
    private BigDecimal sumamount;
    private BigDecimal sumsharefee;
    private BigDecimal sumredirectshareamount;
    private BigDecimal suminderectshareamount;
    private  String  opname;
    private  String opid;
    private  BigDecimal platfromShareRatio;
    private String mname;
    private BigDecimal opaShareAmount;
    private String types;

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public BigDecimal getOpaShareAmount() {
        return opaShareAmount;
    }

    public void setOpaShareAmount(BigDecimal opaShareAmount) {
        this.opaShareAmount = opaShareAmount;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    public String getOpid() {
        return opid;
    }

    public void setOpid(String opid) {
        this.opid = opid;
    }

    public String getOpname() {
        return opname;
    }

    public void setOpname(String opname) {
        this.opname = opname;
    }

    public void setOperatorShareFee(BigDecimal operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public void setRedirectShareAmount(BigDecimal redirectShareAmount) {
        this.redirectShareAmount = redirectShareAmount;
    }

    public void setInderectShareAmount(BigDecimal inderectShareAmount) {
        this.inderectShareAmount = inderectShareAmount;
    }

    public void setSettlementAmounts(BigDecimal settlementAmounts) {
        this.settlementAmounts = settlementAmounts;
    }

    public BigDecimal getSumamount() {
        return sumamount;
    }

    public void setSumamount(BigDecimal sumamount) {
        this.sumamount = sumamount;
    }

    public BigDecimal getSumsharefee() {
        return sumsharefee;
    }

    public void setSumsharefee(BigDecimal sumsharefee) {
        this.sumsharefee = sumsharefee;
    }

    public BigDecimal getSumredirectshareamount() {
        return sumredirectshareamount;
    }

    public void setSumredirectshareamount(BigDecimal sumredirectshareamount) {
        this.sumredirectshareamount = sumredirectshareamount;
    }

    public BigDecimal getSuminderectshareamount() {
        return suminderectshareamount;
    }

    public void setSuminderectshareamount(BigDecimal suminderectshareamount) {
        this.suminderectshareamount = suminderectshareamount;
    }

    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getIsUseDiscout() {
        return isUseDiscout;
    }

    @Override
    public void setIsUseDiscout(String isUseDiscout) {
        this.isUseDiscout = isUseDiscout;
    }

    public BigDecimal getOperatorShareFee() {
        return operatorShareFee;
    }

    public BigDecimal getRedirectShareAmount() {
        return redirectShareAmount;
    }

    public BigDecimal getInderectShareAmount() {
        return inderectShareAmount;
    }

    public BigDecimal getSettlementAmounts() {
        return settlementAmounts;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMmobile() {
        return mmobile;
    }

    public void setMmobile(String mmobile) {
        this.mmobile = mmobile;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }
}
