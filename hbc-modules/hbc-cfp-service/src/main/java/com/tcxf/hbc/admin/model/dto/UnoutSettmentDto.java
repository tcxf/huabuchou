package com.tcxf.hbc.admin.model.dto;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 16:38 2018/8/14
 */
public class UnoutSettmentDto {

    private String mid;
    private String oid;
    private String tids;
    private String legalName;
    private String legalTel;
    private String clearDay;
    private BigDecimal pShareAmount;
    private BigDecimal opaShareAmount;
    private BigDecimal actualAmount;
    private BigDecimal shareFee;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getTids() {
        return tids;
    }

    public void setTids(String tids) {
        this.tids = tids;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getLegalTel() {
        return legalTel;
    }

    public void setLegalTel(String legalTel) {
        this.legalTel = legalTel;
    }

    public String getClearDay() {
        return clearDay;
    }

    public void setClearDay(String clearDay) {
        this.clearDay = clearDay;
    }

    public BigDecimal getpShareAmount() {
        return pShareAmount;
    }

    public void setpShareAmount(BigDecimal pShareAmount) {
        this.pShareAmount = pShareAmount;
    }

    public BigDecimal getOpaShareAmount() {
        return opaShareAmount;
    }

    public void setOpaShareAmount(BigDecimal opaShareAmount) {
        this.opaShareAmount = opaShareAmount;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }
}
