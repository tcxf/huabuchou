package com.tcxf.hbc.admin.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.PAdmin;

import java.util.Date;

public class roleDto extends PAdmin {
    /**
     * 资源表ID
     */
    private String resourcesId;

    /**
     * 用户类型1：商户，2：运营商，3：平台
     */
    private String peopleUserType;
    /**
     * 用户类型1：商户，2：运营商，3：平台
     */
    private String utype;
    /**
     * 角色名
     */
    private String roleName;
    /**
     * 备注
     */
    private String remark;
    /**
     * 资源名称
     */
    private String resourceName;
    /**
     * 资源类型(1:模块 2:普通菜单 )
     */
    private String resourceType;
    /**
     * 资源路径
     */
    private String resourceUrl;
    /**
     * 图标路径
     */
    private String imgUrl;
    /**
     * 父资源id
     */
    private String parentId;
    /**
     * 排序号
     */
    private Integer sortNo;

    public String getResourcesId() {
        return resourcesId;
    }

    public void setResourcesId(String resourcesId) {
        this.resourcesId = resourcesId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPeopleUserType() {
        return peopleUserType;
    }

    public void setPeopleUserType(String peopleUserType) {
        this.peopleUserType = peopleUserType;
    }
}
