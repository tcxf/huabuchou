package com.tcxf.hbc.admin.model.vo;

import com.tcxf.hbc.common.entity.FFundend;

import java.math.BigDecimal;

public class FFundendVo extends FFundend {

    private BigDecimal serviceFee;
    private BigDecimal yqRate;
    /**
     * 所属用户类型（1-用户 2-商户 3-运营商 4-资金端）
     */
    private String utype;
    /**
     * 卡片类型（0-储蓄卡 ）
     */
    private String cardType;
    /**
     * 银行卡编码
     */
    private String bank_bankSn;
    /**
     * 持卡人手机号
     */
    private String bank_mobile;
    /**
     * 持卡人姓名
     */
    private String bname;
    /**
     * 银行卡行
     */
    private String bank_name;
    /**
     * 持卡人证件号
     */
    private String bank_idCard;
    /**
     * 银行卡号
     */
    private String bank_bankCard;

    /**
     * 有效期
     */
    private String valiDate;

    /**
     * 是否为主卡
     */
    private String bank_isDefault;

    /**
     * 地址
     */
    private String areaId;

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public BigDecimal getYqRate() {
        return yqRate;
    }

    public void setYqRate(BigDecimal yqRate) {
        this.yqRate = yqRate;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBank_bankSn() {
        return bank_bankSn;
    }

    public void setBank_bankSn(String bank_bankSn) {
        this.bank_bankSn = bank_bankSn;
    }

    public String getBank_mobile() {
        return bank_mobile;
    }

    public void setBank_mobile(String bank_mobile) {
        this.bank_mobile = bank_mobile;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_idCard() {
        return bank_idCard;
    }

    public void setBank_idCard(String bank_idCard) {
        this.bank_idCard = bank_idCard;
    }

    public String getBank_bankCard() {
        return bank_bankCard;
    }

    public void setBank_bankCard(String bank_bankCard) {
        this.bank_bankCard = bank_bankCard;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getBank_isDefault() {
        return bank_isDefault;
    }

    public void setBank_isDefault(String bank_isDefault) {
        this.bank_isDefault = bank_isDefault;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
}
