package com.tcxf.hbc.admin.model.vo;

import com.tcxf.hbc.common.entity.MHotSearch;

public class HotSearchVo extends MHotSearch {
    private String rname;

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }
}
