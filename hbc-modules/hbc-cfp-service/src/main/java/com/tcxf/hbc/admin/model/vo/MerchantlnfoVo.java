package com.tcxf.hbc.admin.model.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.MMerchantInfo;

import java.math.BigDecimal;
import java.util.Date;

public class MerchantlnfoVo extends MMerchantInfo {

    /**
     * 行业类型
     */

    private String bank_oid;

    private String utype;

    private String cardType;

    private String bank_bankSn;

    private String bank_mobile;

    private String bank_name;

    private String bank_idCard;

    private String bank_bankCard;

    private String valiDate;

    private String cvv;

    private String bank_isDefault;

    private String bank_bankName;

    private BigDecimal minmoney;

    private String latestday;

    public BigDecimal getMinmoney() {
        return minmoney;
    }

    public void setMinmoney(BigDecimal minmoney) {
        this.minmoney = minmoney;
    }

    public String getLatestday() {
        return latestday;
    }

    public void setLatestday(String latestday) {
        this.latestday = latestday;
    }

    public String getBank_oid() {
        return bank_oid;
    }

    public void setBank_oid(String bank_oid) {
        this.bank_oid = bank_oid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBank_bankSn() {
        return bank_bankSn;
    }

    public void setBank_bankSn(String bank_bankSn) {
        this.bank_bankSn = bank_bankSn;
    }

    public String getBank_mobile() {
        return bank_mobile;
    }

    public void setBank_mobile(String bank_mobile) {
        this.bank_mobile = bank_mobile;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_idCard() {
        return bank_idCard;
    }

    public void setBank_idCard(String bank_idCard) {
        this.bank_idCard = bank_idCard;
    }

    public String getBank_bankCard() {
        return bank_bankCard;
    }

    public void setBank_bankCard(String bank_bankCard) {
        this.bank_bankCard = bank_bankCard;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getBank_isDefault() {
        return bank_isDefault;
    }

    public void setBank_isDefault(String bank_isDefault) {
        this.bank_isDefault = bank_isDefault;
    }

    public String getBank_bankName() {
        return bank_bankName;
    }

    public void setBank_bankName(String bank_bankName) {
        this.bank_bankName = bank_bankName;
    }

    private String hname;

    private String hid;

    /**
     * 商户主键id，唯一
     */
    private String mid;
    /**
     * 商户性质
     */
    private String nature;
    /**
     * 品牌经营年限
     */
    private Integer operYear;
    /**
     * 经营面积（㎡）
     */
    private Integer operArea;
    /**
     * 注册资金（万）
     */
    private Integer regMoney;
    /**
     * 场地照片资源路径（c端显示图片）
     */
    private String localPhoto;
    /**
     * 营业执照图片资源路径
     */
    private String licensePic;
    /**
     * 法人身份照片资源路径
     */
    private String legalPhoto;
    /**
     * 借记卡照片资源路径
     */
    private String normalCard;
    /**
     * 信用卡照片资源路径
     */
    private String creditCard;
    /**
     * 法人正面照资源路径
     */
    private String lagalFacePhoto;
    /**
     * 结算方式 0- t+1 , 1 - t+0
     */
    private String settlementType;
    /**
     * 授信是否有上限
     */
    private String authMaxFlag;
    /**
     * 授信上限值
     */
    private Long authMax;
    /**
     * 结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年）
     */
    private String settlementLoop;
    /**
     * 清算日期（结算日）
     */
    private Integer clearDay;
    /**
     * 商户是否开启让利（给运营商的让利）
     */
    private Integer shareFlag;
    /**
     * 商户让利费率（百分之）
     */
    private BigDecimal shareFee;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date modifyDate;
    /**
     * 开店时间（运营商审核的时间）
     */
    private String openDate;
    /**
     * 0-未提交 1-待审核 2-审核失败 3-审核成功
     */
    private String authExamine;
    /**
     * 审核失败原因
     */
    private String examineFailReason;
    /**
     * 商家电话
     */
    private String phoneNumber;

    /**
     * 店铺推荐商品简介
     */
    private String recommendGoods;


    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public String getHname() {
        return hname;
    }

    public void setHname(String hname) {
        this.hname = hname;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public Integer getOperArea() {
        return operArea;
    }

    public void setOperArea(Integer operArea) {
        this.operArea = operArea;
    }

    public Integer getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Integer regMoney) {
        this.regMoney = regMoney;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getAuthMaxFlag() {
        return authMaxFlag;
    }

    public void setAuthMaxFlag(String authMaxFlag) {
        this.authMaxFlag = authMaxFlag;
    }

    public Long getAuthMax() {
        return authMax;
    }

    public void setAuthMax(Long authMax) {
        this.authMax = authMax;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public Integer getClearDay() {
        return clearDay;
    }

    public void setClearDay(Integer clearDay) {
        this.clearDay = clearDay;
    }

    public Integer getShareFlag() {
        return shareFlag;
    }

    public void setShareFlag(Integer shareFlag) {
        this.shareFlag = shareFlag;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    @Override
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public Date getModifyDate() {
        return modifyDate;
    }

    @Override
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }

    public String getExamineFailReason() {
        return examineFailReason;
    }

    public void setExamineFailReason(String examineFailReason) {
        this.examineFailReason = examineFailReason;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRecommendGoods() {
        return recommendGoods;
    }

    public void setRecommendGoods(String recommendGoods) {
        this.recommendGoods = recommendGoods;
    }



}
