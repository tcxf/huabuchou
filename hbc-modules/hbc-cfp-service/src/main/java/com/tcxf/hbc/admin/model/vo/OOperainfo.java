package com.tcxf.hbc.admin.model.vo;

import com.tcxf.hbc.common.entity.OOperaInfo;

import java.math.BigDecimal;

/*
   运行商所有信息vo
 */
public class OOperainfo extends OOperaInfo {
    private Integer operYear;//品牌经营年限
    private String operArea;//经营面积
    private String nature;//运行商性质
    private Long regMoney;//注册资金
    private String legalPhoto;//场地照片
    private String localPhoto;//营业执照
    private String lagalFacePhoto;
    private String licensePic;//
    private String normalCard;
    private String creditCard;
    private BigDecimal scorePercent;//积分比例
    private Long messageFee; //短信费用
    private String name;//开户人姓名
    private String bankName;//开户银行
    private String mobile;//预留手机号码
    private String idCard;//身份证号
    private String bankCard;//银行卡号
    private String isDefault;//是否为主卡
    private String yname;//运行商名字
    private String ymobile;//运营商号码
    private String settlementLoop;//结算周期

    private String settlementStatus;//是否开通结算单功能

    @Override
    public String getSettlementStatus() {
        return settlementStatus;
    }

    @Override
    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    private BigDecimal platfromShareRatio;



    private String bank_oid;

    private String utype;

    private String cardType;

    private String bank_bankSn;

    private String bank_mobile;

    private String bank_name;

    private String bank_idCard;

    private String bank_bankCard;

    private String valiDate;

    private String cvv;

    private String bnak_isDefault;

    private String bnak_bankName;

    private String contractId;

    public Integer getOperYear() {
        return operYear;
    }

    public void setOperYear(Integer operYear) {
        this.operYear = operYear;
    }

    public String getOperArea() {
        return operArea;
    }

    public void setOperArea(String operArea) {
        this.operArea = operArea;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Long getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Long regMoney) {
        this.regMoney = regMoney;
    }

    public String getLegalPhoto() {
        return legalPhoto;
    }

    public void setLegalPhoto(String legalPhoto) {
        this.legalPhoto = legalPhoto;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLagalFacePhoto() {
        return lagalFacePhoto;
    }

    public void setLagalFacePhoto(String lagalFacePhoto) {
        this.lagalFacePhoto = lagalFacePhoto;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getNormalCard() {
        return normalCard;
    }

    public void setNormalCard(String normalCard) {
        this.normalCard = normalCard;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public BigDecimal getScorePercent() {
        return scorePercent;
    }

    public void setScorePercent(BigDecimal scorePercent) {
        this.scorePercent = scorePercent;
    }

    public Long getMessageFee() {
        return messageFee;
    }

    public void setMessageFee(Long messageFee) {
        this.messageFee = messageFee;
    }

    public BigDecimal getPlatfromShareRatio() {
        return platfromShareRatio;
    }

    public void setPlatfromShareRatio(BigDecimal platfromShareRatio) {
        this.platfromShareRatio = platfromShareRatio;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String getMobile() {
        return mobile;
    }

    @Override
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String getIdCard() {
        return idCard;
    }

    @Override
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getYname() {
        return yname;
    }

    public void setYname(String yname) {
        this.yname = yname;
    }

    public String getYmobile() {
        return ymobile;
    }

    public void setYmobile(String ymobile) {
        this.ymobile = ymobile;
    }

    public String getBank_oid() {
        return bank_oid;
    }

    public void setBank_oid(String bank_oid) {
        this.bank_oid = bank_oid;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBank_bankSn() {
        return bank_bankSn;
    }

    public void setBank_bankSn(String bank_bankSn) {
        this.bank_bankSn = bank_bankSn;
    }

    public String getBank_mobile() {
        return bank_mobile;
    }

    public void setBank_mobile(String bank_mobile) {
        this.bank_mobile = bank_mobile;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_idCard() {
        return bank_idCard;
    }

    public void setBank_idCard(String bank_idCard) {
        this.bank_idCard = bank_idCard;
    }

    public String getBank_bankCard() {
        return bank_bankCard;
    }

    public void setBank_bankCard(String bank_bankCard) {
        this.bank_bankCard = bank_bankCard;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getBnak_isDefault() {
        return bnak_isDefault;
    }

    public void setBnak_isDefault(String bnak_isDefault) {
        this.bnak_isDefault = bnak_isDefault;
    }

    public String getBnak_bankName() {
        return bnak_bankName;
    }

    public void setBnak_bankName(String bnak_bankName) {
        this.bnak_bankName = bnak_bankName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

}