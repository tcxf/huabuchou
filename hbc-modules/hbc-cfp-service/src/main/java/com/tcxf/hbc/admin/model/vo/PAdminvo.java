package com.tcxf.hbc.admin.model.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.PAdmin;

import java.io.Serializable;
import java.util.Date;

public class PAdminvo extends Model<PAdmin> {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 状态(0:启用 1:停用 )
     */
    private String status;
    /**
     * 注册邮箱
     */
    @TableField("reg_email")
    private String regEmail;
    /**
     * 管理员头像
     */
    @TableField("head_uri")
    private String headUri;
    /**
     * 用户姓名
     */
    @TableField("admin_name")
    private String adminName;
    /**
     * 密码,md5加密
     */
    private String pwd;
    /**
     * 帐号
     */
    @TableField("user_name")
    private String userName;
    /**
     * 记录创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 记录修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegEmail() {
        return regEmail;
    }

    public void setRegEmail(String regEmail) {
        this.regEmail = regEmail;
    }

    public String getHeadUri() {
        return headUri;
    }

    public void setHeadUri(String headUri) {
        this.headUri = headUri;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PAdmin{" +
                ", id=" + id +
                ", status=" + status +
                ", regEmail=" + regEmail +
                ", headUri=" + headUri +
                ", adminName=" + adminName +
                ", pwd=" + pwd +
                ", userName=" + userName +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                "}";
    }
}
