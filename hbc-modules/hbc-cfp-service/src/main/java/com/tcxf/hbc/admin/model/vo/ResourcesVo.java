package com.tcxf.hbc.admin.model.vo;


import com.tcxf.hbc.common.entity.TbResources;

public class ResourcesVo extends TbResources {
    private String page;

    public void setPage(String page){
        this.page = page;
    }
    public String getPage(){
        return page;
    }
}
