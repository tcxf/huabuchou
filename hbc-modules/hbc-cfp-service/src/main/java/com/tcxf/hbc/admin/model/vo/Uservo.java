package com.tcxf.hbc.admin.model.vo;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;
import java.util.Date;

public class Uservo {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户姓名
     */
    @TableField("real_name")
    private String realName;
    /**
     * 用户手机号码
     */
    private String mobile;
    /**
     * 状态（0-启用 1-禁用）
     */
    private String status;
    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;
    /**
     * 最后登录ip
     */
    @TableField("login_ip")
    private String loginIp;
    /**
     * 用户头像资源访问路径
     */
    @TableField("head_img")
    private String headImg;
    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 微信公众号openid
     */
    @TableField("wx_openid")
    private String wxOpenid;
    /**
     * 微信唯一id
     */
    @TableField("wx_unionid")
    private String wxUnionid;
    /**
     * 性别 1-男 2-女
     */
    private String sex;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 创建时间
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 修改时间
     */
    @TableField("modify_date")
    private Date modifyDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getWxOpenid() {
        return wxOpenid;
    }

    public void setWxOpenid(String wxOpenid) {
        this.wxOpenid = wxOpenid;
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }


    @Override
    public String toString() {
        return "CUserInfo{" +
                ", id=" + id +
                ", realName=" + realName +
                ", mobile=" + mobile +
                ", status=" + status +
                ", lastLoginTime=" + lastLoginTime +
                ", loginIp=" + loginIp +
                ", headImg=" + headImg +
                ", idCard=" + idCard +
                ", wxOpenid=" + wxOpenid +
                ", wxUnionid=" + wxUnionid +
                ", sex=" + sex +
                ", birthday=" + birthday +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                "}";
    }
}
