package com.tcxf.hbc.admin.model.vo;

import com.tcxf.hbc.common.entity.TbWalletWithdrawals;

public class WalletWithdrawalsVo extends TbWalletWithdrawals {

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    /**
     * 用户类型1：用户，2：商户，3：运营商，4：资金端
     */
    private String utype;
}
