package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CreditScore;
import com.tcxf.hbc.common.entity.RefusingCredit;
import com.tcxf.hbc.common.entity.RefusingVersion;

import java.util.List;
import java.util.Map;

/**
 * 授信提额综合分值
 * @Auther: liuxu
 * @Date: 2018/9/19 16:14
 * @Description:
 */
public interface CreditScoreService extends IService<CreditScore> {

    public void createCreditScore(List<CreditScore> list, String fid);

    public  List<CreditScore> selectCreditScoreByfId(Map<String, Object> map);
}
