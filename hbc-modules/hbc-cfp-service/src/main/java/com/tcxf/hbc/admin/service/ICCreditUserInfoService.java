package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.CCreditUserInfoDto;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
public interface ICCreditUserInfoService extends IService<CCreditUserInfo>{

    public Page <CCreditUserInfoDto> Bank (Query<CCreditUserInfoDto> query );

    public Page Operatoryd (Query<CCreditUserInfoDto> query );

    public Page Operatorlt (Query<CCreditUserInfoDto> query );

    public Page  Operatordx (Query<CCreditUserInfoDto> query );

    public Page  Multipleliabilities (Query<CCreditUserInfoDto> query );

    public Page  Phone (Query<CCreditUserInfoDto> query );

    public Page  Getloginmode (Query<CCreditUserInfoDto> query );

    public Page  Operatorlogin (Query<CCreditUserInfoDto> query );

    public Page  Socialsecurity (Query<CCreditUserInfoDto> query );

    public Page  Accumulationfund (Query<CCreditUserInfoDto> query );

    public Page  Learningletter (Query<CCreditUserInfoDto> query );

    public Page  TaoBao (Query<CCreditUserInfoDto> query );

    public Page  JD (Query<CCreditUserInfoDto> query );

    public Page  DDZMF (Query<CCreditUserInfoDto> query );


    public List<CCreditUserInfoDto> Bank (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatoryd (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlt (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatordx (Map<String , Object> map);

    public List<CCreditUserInfoDto> Multipleliabilities (Map<String , Object> map);

    public List<CCreditUserInfoDto> Phone (Map<String , Object> map);

    public List<CCreditUserInfoDto> Getloginmode (Map<String , Object> map);

    public List<CCreditUserInfoDto> Operatorlogin (Map<String , Object> map);

    public List<CCreditUserInfoDto> Socialsecurity (Map<String , Object> map);

    public List<CCreditUserInfoDto> Accumulationfund (Map<String , Object> map);

    public List<CCreditUserInfoDto> Learningletter (Map<String , Object> map);

    public List<CCreditUserInfoDto> TaoBao (Map<String , Object> map);

    public List<CCreditUserInfoDto> JD (Map<String , Object> map);

    public List<CCreditUserInfoDto> DDZMF (Map<String , Object> map);

}
