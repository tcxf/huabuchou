package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.COperaSwitch;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 运营商开关表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
public interface ICOperaSwitchService extends IService<COperaSwitch> {

}
