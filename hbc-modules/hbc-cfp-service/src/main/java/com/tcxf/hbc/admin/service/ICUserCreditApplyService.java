package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提额申请审核记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ICUserCreditApplyService extends IService<CUserCreditApply> {

}
