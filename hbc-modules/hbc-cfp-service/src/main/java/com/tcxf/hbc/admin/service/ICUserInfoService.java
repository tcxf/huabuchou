package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.CreditPageInfoDto;
import com.tcxf.hbc.admin.model.dto.CuserinfoDto;
import com.tcxf.hbc.admin.model.dto.QueryCreditRecordDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ICUserInfoService extends IService<CUserInfo> {
    /**
     * 查询消费者
     */
    Page <CuserinfoDto> userlist(Query<CuserinfoDto> condition);

    int selectConsumercount(Map<String,Object> mmp);

    /**
     * 消费者详情
     */
    public CuserinfoDto QueryAll(@Param("id") String id);

    int findAlreadyAuthNum(Map<String,Object> paramMap);

    /**
     * 分页查询所有运营商下的消费者信息
     * @param objectQuery
     * @return
     */
    Page OQueryAll(Query<CuserinfoDto> objectQuery);

    /**
     * 今日新增消费者人数
     * @param start
     * @param end
     * @return
     */
    public int CreateQueryAllDate(String start ,String end);

    /**
     * 资金端今日新增消费者人数
     * @param start
     * @param end
     * @return
     */
    int FCreateQueryAllDate(String start ,String end,String fid);
    /**
     * 风控系统授信管理查询
     */
    Page  QueryCreditRecord (Query<QueryCreditRecordDto> condition);

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:21 2018/9/17
     * 查询用户授信明细(已授信、已拒绝)
     * @param paramMap
    */
    Page findCuserCreditPageInfo(Query<CreditPageInfoDto> paramMap);


    /**
     * 授信管理下载报表
     * @param map
     * @return
     */
    List<QueryCreditRecordDto> xz(Map<String,Object> map);
}
