package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 消费者用户其他信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ICUserOtherInfoService extends IService<CUserOtherInfo> {

}
