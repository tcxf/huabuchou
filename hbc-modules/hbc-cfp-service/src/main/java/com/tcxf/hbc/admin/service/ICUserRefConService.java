package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.CUserRefCon;
import com.baomidou.mybatisplus.service.IService;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ICUserRefConService extends IService<CUserRefCon> {

    /**
     * 查询资金端绑定的用户
     * @param uid
     * @param fid
     * @return
     */
    CUserRefCon finduserCon(Map<String,Object> map);

    int findthisVersionNumByRefusingCode(HashMap<String, Object> paramMap);
}
