package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.CreditDaily;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 授信日报表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
public interface ICreditDailyService extends IService<CreditDaily> {

    /**
     * 查询授信日报
     */
    Page findPlist(Map<String, Object> condition);



}
