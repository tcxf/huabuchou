package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IFFundendBindService extends IService<FFundendBind> {

    /**
     * 按条件查询资金端资源表集合
     * @param fid 资金端id
     * @return
     */
    public List<FFundendBind> getOperaIdList(String fid);
}
