package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.FFundendDto;
import com.tcxf.hbc.admin.model.vo.FFundendVo;
import com.tcxf.hbc.common.entity.FFundend;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金端信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IFFundendService extends IService<FFundend> {

    public Page queryAll( );


    public Page selectPages();

    R addFundend(FFundendVo fFundendVo);

    /**
     * 根据运营商id查询已绑定资金端
     * @param map
     * @return
     */
    public Page selectFunendByOid(Map<String , Object> map);

    /**
     * 查询资金端详情信息
     * @param id
     * @return
     */
    public List<FFundendDto> selectFundendDetail(String id,String oid);

    /**
     * 查询运行商下的资金端
     * @param condition
     * @return
     */
       Page QueryFO(Query<FFundendDto> condition);


    /**
     * 更具Id查询资金端下的运营商详情
     * @return
     */

    public FFundendDto QueeryFOone( String id);
}
