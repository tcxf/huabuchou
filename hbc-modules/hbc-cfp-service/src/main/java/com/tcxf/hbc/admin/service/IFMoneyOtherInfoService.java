package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 授信总金额出入明细表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
public interface IFMoneyOtherInfoService extends IService<FMoneyOtherInfo> {
    /**
     * 当前资金池额度
     * @return
     */
    BigDecimal Summoney(String fid);


    /**
     * 根据时间查询今日授信总额
     * @param start 开始时间 （截取系统当前时间 +00:00:00）
     * @param end 结束时间（第二天系统当前时间 +00:00:00）DaySummoney
     * @return 返回int类型结果集
     */
    BigDecimal DaySummoney(@Param("start")String start, @Param("end") String end,@Param("fid") String fid);


    /**
     * 当前今日授信余额（当前SQL查询今日授信限额的总额度）
     * @return 返回int类型结果集
     */
    BigDecimal DayYUmoney(String fid);

    //累计授信总额
    BigDecimal LJSummoney(String fid);


    /**
     * 当前资金池额度
     * @return
     */
    BigDecimal Summoneys(String fid);

    BigDecimal DayYUmoneys(String fid);

    BigDecimal DayYUmoneyss(String fid);
}
