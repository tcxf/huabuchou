package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 首页轮播图表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IIndexBannerService extends IService<IndexBanner> {


}
