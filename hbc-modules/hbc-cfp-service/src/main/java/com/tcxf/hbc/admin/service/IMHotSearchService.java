package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.MHotSearchDto;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.Map;

/**
 * <p>
 * 热搜店铺表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMHotSearchService extends IService<MHotSearch> {

    /**
     * 查询热门搜索
     * @param map
     * @return
     */
    public Page selectHotSearchByRid(Map<String , Object> map);

    MHotSearchDto selectHotSearch(Map<String , Object> map);
}
