package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

/**
 * <p>
 * 商户行业分类表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMMerchantCategoryService extends IService<MMerchantCategory> {

}
