package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.admin.model.dto.MMerchantDto;
import com.tcxf.hbc.admin.model.dto.MMerchantInfoDto;
import com.tcxf.hbc.admin.model.vo.MerchantlnfoVo;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 商户信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMMerchantInfoService extends IService<MMerchantInfo> {

    Page findMerchantManagerPageInfo(Query<MMerchantInfoDto> objectQuery);

    void addMerchantInfo(Map<String,Object> paramMap);
    Page selectPage(Map<String,Object> map);



    /**
     * 分页查询所有商户信息
     * @param objectQuery
     * @return
     */
    Page QueryAll(Query<MMerchantDto> objectQuery);

    /**
     * 用户详情
     * @return
     */
    public MMerchantDto QueryOne(@Param("id") String id);

    /**
     * 热门搜索查询商户列表
     * liaozeyong
     * @param map
     * @return
     */
    public Page findByOid(Map<String , Object> map);
    int findAlreadyAuthNum(Map<String,Object> paramMap);

    void updateMerchantInfo(Map<String,Object> paramMap);

    void updateById(MerchantlnfoVo m, HttpServletRequest request);
}
