package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商户其他信息 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMMerchantOtherInfoService extends IService<MMerchantOtherInfo> {

}
