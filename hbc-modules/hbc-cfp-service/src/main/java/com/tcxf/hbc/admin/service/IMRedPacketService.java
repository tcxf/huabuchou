package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户领取优惠券记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMRedPacketService extends IService<MRedPacket> {

}
