package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 优惠券表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMRedPacketSettingService extends IService<MRedPacketSetting> {

    /**
     * 按条件查询优惠
     * @param map
     * @return
     */
    public Page findredlist(Map<String,Object> map);

}
