package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.admin.model.vo.OOperainfo;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 运营商信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IOOperaInfoService extends IService<OOperaInfo> {
    /**
     * 今日新增人数
     * @param start
     * @param end
     * @return
     */
    public int CreateQueryAllDate(String start ,String end);

    /**
     * 查询所有可用商户
     * @return
     */
    public int CountQueryOperator();

    /**
     * 分页查询商户信息
     * @param status
     * @return
     */
    public Page queryList2(String status);
    /**
     * 查询所有商户
     * @return
     */
    public Page QueryAll( Query<MerchantSerlect> query);

    /**
     * 分页查询商户
     * @param params
     * @return
     */
    public Page selectOperatorPage(Map<String, Object> params);

    /**
     * 根据资金端id查询运营商信息
     * @param map
     * @return
     */
    public Page getOperaInfoByFid(Map<String,Object> map,String id);

    /**
     * 查询运营商下的所有资料
     * @return
     */
    public MerchantSerlect QueeryAllOperaInfoDetail(String id);

    /***
     * 查寻运行商下的银行卡信息
     *
     */
    public Page QueryBanks (Query<MerchantSerlect> query);

    /**
     * 根据资金端查询已绑定和未绑定运营商
     * @param params
     * @return
     */
    Page selectOperaByFundend(Map<String, Object> params);

    List<OOperaInfo> selectOperaByfid(Map<String,Object> paramMap);

    void insertOperainfo(OOperainfo oop,HttpServletRequest request);


    void updateOperainfo(OOperainfo oop,HttpServletRequest request);
}
