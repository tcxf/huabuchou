package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.admin.model.dto.roleDto;
import com.tcxf.hbc.common.entity.PAdmin;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台用户信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IPAdminService extends IService<PAdmin> {


//    public Page getAdminList();
//
//    public Integer deleteByid(String id);
//
//    public Integer updateByid(String id);

    //public Integer insert(PAdmin admin);

    /**
     * 登陆
     * @param pAdmin
     * @return
     */
    public PAdmin login(PAdmin pAdmin);

    public Page findAdminCondition(Map<String,Object> map) ;

}
