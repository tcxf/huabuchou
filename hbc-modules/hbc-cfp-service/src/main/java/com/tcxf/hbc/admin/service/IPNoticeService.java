package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.PNotice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
public interface IPNoticeService extends IService<PNotice> {

}
