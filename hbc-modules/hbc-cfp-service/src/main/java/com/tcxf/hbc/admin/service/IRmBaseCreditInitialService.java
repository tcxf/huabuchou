package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.admin.model.dto.RmBaseCreditInitialDto;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.baomidou.mybatisplus.service.IService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 初始授信条件模板表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
public interface IRmBaseCreditInitialService extends IService<RmBaseCreditInitial> {


    public List<RmBaseCreditInitial> getRmBaseCreditInitialList(RmBaseCreditInitial rmBaseCreditInitial);

    public void createRmBaseCreditInitial(List<RmBaseCreditInitial> list, String fid);

    List<RmBaseCreditInitialDto> findBaseCreditInitialInfo(HashMap<String,Object> paramMap);

    int findNumberOverDueByDetail(HashMap<String,Object> paramMap);

    int findNumberOverDueByInterval(HashMap<String, Object> paramMap);

    BigDecimal findNumberOverDueMoneyByDetail(HashMap<String,Object> paramMap);

    BigDecimal findNumberOverDueMoneyByInterval(HashMap<String, Object> paramMap);

    int findpeopleNumByGradingSegment(HashMap<String, Object> paramMap);

    int findoverDueNumByGradingSegment(HashMap<String, Object> paramMap);

    BigDecimal findoverDueMoneyByGradingSegment(HashMap<String,Object> paramMap);
}
