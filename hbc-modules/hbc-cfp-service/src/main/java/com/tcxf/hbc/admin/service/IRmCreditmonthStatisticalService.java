package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.BindFODto;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信月份统计表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-25
 */
public interface IRmCreditmonthStatisticalService extends IService<RmCreditmonthStatistical> {
     Page QueryCredituserstatistics(Query<RmCreditmonthStatistical> query);
     List<BindFODto> QueryfOs(Map<String, Object> params);
     BindFODto sumzsx();
     BindFODto sumsx();
     List<BindFODto>  sumzsxed();
     List<BindFODto>  sumsxed();


}
