package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.RmRepaymentSituation;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.List;

/**
 * <p>
 * 还款情况表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-10-10
 */
public interface IRmRepaymentSituationService extends IService<RmRepaymentSituation> {
    Page QueryRepaymentsituation (Query<RmRepaymentSituation> query);
}
