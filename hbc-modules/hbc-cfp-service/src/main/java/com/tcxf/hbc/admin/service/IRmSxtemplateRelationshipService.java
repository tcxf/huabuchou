package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 授信模板关系表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
public interface IRmSxtemplateRelationshipService extends IService<RmSxtemplateRelationship> {

    /**
     * 克隆版本
     */
    public String cloneRmSxtemplateRelationship(String  id,String fid);

    public void updateStatus(RmSxtemplateRelationship rmSxtemplateRelationship);

    public void deleteRmSxtemplateRelationshipById(String id);
}
