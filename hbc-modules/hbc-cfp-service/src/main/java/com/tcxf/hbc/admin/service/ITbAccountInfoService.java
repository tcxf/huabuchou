package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户授信资金额度表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbAccountInfoService extends IService<TbAccountInfo> {

}
