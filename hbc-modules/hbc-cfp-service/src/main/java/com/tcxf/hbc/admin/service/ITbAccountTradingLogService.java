package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 授信资金流水 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbAccountTradingLogService extends IService<TbAccountTradingLog> {

}
