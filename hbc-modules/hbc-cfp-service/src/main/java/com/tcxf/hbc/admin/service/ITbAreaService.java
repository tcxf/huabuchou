package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.admin.model.dto.TbAreaDto;
import com.tcxf.hbc.common.entity.TbArea;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 地区表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbAreaService extends IService<TbArea> {

    /**
     * 根据id获取地区表内容
     * @param id
     * @return
     */
    public TbArea getById(String id);


    /**
     * 查询省
     */
    public List<TbAreaDto> QueryS();

    /**
     * 查询市
     * @param parent_id
     * @return
     */
    public List<TbAreaDto> QuerySS(@Param("parent_id")String parent_id);

    /**
     * 查询区
     * @param parent_id
     * @return
     */
    public List<TbAreaDto> QueryQ(@Param("parent_id")String parent_id);

}
