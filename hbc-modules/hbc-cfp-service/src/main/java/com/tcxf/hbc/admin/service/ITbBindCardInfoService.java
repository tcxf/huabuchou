package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;

/**
 * <p>
 * 绑卡信息 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbBindCardInfoService extends IService<TbBindCardInfo> {
    /**
     * 查询所有银行卡信息
     */
    public List<TbBindCardInfo> QueryAllBank();

    /**
     * 资金端编辑，查看绑卡信息详情
     * liaozeyong
     * @return
     */
    public Page selectBindCardList(Map<String,Object> map);

    /**
     * 根据用户ID查看默认绑卡信息详情
     * liaozeyong
     * @return
     */
    public List<TbBindCardInfo> selectBindCard(Map<String,Object> map);

    /**
     * 运营商绑卡
     * @return
     */
    TbBindCardInfo banko(@PathVariable("oid")String oid);

    /**
     * 商户帮卡
     * @return
     */
    TbBindCardInfo bankm(@PathVariable("oid")String oid);

}
