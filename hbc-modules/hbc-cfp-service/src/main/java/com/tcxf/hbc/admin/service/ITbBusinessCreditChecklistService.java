package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbBusinessCreditChecklist;

/**
 * <p>
 * 商业授信审核表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
public interface ITbBusinessCreditChecklistService extends IService<TbBusinessCreditChecklist> {

}
