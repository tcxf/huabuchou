package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbCollect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 我的收藏表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface ITbCollectService extends IService<TbCollect> {

}
