package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 快速授信评分项 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
public interface ITbCreditGradesService extends IService<TbCreditGrade> {

    int findcountByDetailNameAndVersion(Map<String,Object> paramMap);

    int findcountByDetailNumAndVersion(Map<String, Object> paramMap);

    int findcountByDetailMinNumAndVersion(Map<String, Object> paramMap);
}
