package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 短信记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbMessageLogService extends IService<TbMessageLog> {

}
