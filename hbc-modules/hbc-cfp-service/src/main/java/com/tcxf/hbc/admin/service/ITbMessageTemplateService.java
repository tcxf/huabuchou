package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbMessageTemplate;

/**
 * 短信模板
 */

public interface ITbMessageTemplateService extends IService<TbMessageTemplate> {
}
