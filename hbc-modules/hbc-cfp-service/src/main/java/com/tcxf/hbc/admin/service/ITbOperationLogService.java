package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbOperationLogService extends IService<TbOperationLog> {

}
