package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbPayment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 支付记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbPaymentService extends IService<TbPayment> {

}
