package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 后台用户角色关系表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbPeopleRoleService extends IService<TbPeopleRole> {

}
