package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.TbRepayLogDto;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.Map;

/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRepayLogService extends IService<TbRepayLog> {

    Page findAdvancePayPageInfo(Query<TbRepayLogDto> tbRepayLogDtoQuery);

    Map<String,Object> findAdvancePayTotalMoney(Map<String,Object> paramMap);
}
