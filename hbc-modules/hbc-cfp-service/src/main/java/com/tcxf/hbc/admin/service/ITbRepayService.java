package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbRepay;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
public interface ITbRepayService extends IService<TbRepay> {

}
