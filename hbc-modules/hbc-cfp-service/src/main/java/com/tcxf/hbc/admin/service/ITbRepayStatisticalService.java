package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 还款统计表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
public interface ITbRepayStatisticalService extends IService<TbRepayStatistical> {

}
