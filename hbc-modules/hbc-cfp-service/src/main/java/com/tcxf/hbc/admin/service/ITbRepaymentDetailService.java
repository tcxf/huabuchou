package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.RepaymentDetail;
import com.tcxf.hbc.admin.model.dto.RepaymentDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRepaymentDetailService extends IService<TbRepaymentDetail> {

    BigDecimal findAlreadyPayBackPlan(Map<String, Object> paramMap);

   int findAlreadyPayBacknotPlan(String startDate, String endDate);

    Page findPaybackPageInfo(Query<RepaymentDetailDto> repaymentDetailDtoQuery);

    Page findpaybackDetailInfo(Query<RepaymentDetail> repaymentDetailQuery);

    BigDecimal findWaitPay(Map<String,Object> paramMap);

    Page repaymentPlanManagerList(Query<RepaymentDetailDto> repaymentDetailQuery);

    //消费者还款记录
    Page repaymentPlanManagerLists(Query<RepaymentDetailDto> repaymentDetailQuery);

    Map<String,Object> DHYQSUM(Map<String , Object> map);

    /**
     * 平台消费已还
     * @param map
     * @return
     */
    public  Map<String,Object> YHSUM (Map<String,Object> map);
}
