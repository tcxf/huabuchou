package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.RepaymentDto;
import com.tcxf.hbc.admin.model.dto.TbrmRepayDto;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款计划表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRepaymentPlanService extends IService<TbRepaymentPlan> {

    Map<String,Object> findwaitPayByCondiction(Map<String,Object> paramMap);

    Map<String,Object> findoverDuePayByCondiction(Map<String,Object> paramMap);

    Map<String,Object> findBillAlreadyPayByCondition(Map<String,Object> paramMap);

    int findoverDueNum(Map<String,Object> totalMap);

    /**
     * 本月应还分页
     * liaozeyong
     * @return
     */
    Page repaymentPlanDetail(Query<RepaymentDto> query);

    /**
     * 本月应还表单导出
     * liaozeyong
     * @param paramMap
     * @return
     */
    List<RepaymentDto> excelRepaymentPlanDetail(Map<String , Object> paramMap);

    List<TbrmRepayDto> findUserPaymentList(Map<String,Object> paramMap);
}
