package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 还款比率字典表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRepaymentRatioDictService extends IService<TbRepaymentRatioDict> {

}
