package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.mapper.TbResourcesMapper;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbResources;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资源信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbResourcesService extends IService<TbResources> {


    public Page getResourcesList();

    /**
     * 根据角色id和type查询权限
     * @param id
     * @return
     */
    public List<TbResources> selectResources(String id,int type);

    /**
     * 根据条件分页查询
     * @param map
     * @return
     */
    public  Page getPage(Map<String,Object> map );


}
