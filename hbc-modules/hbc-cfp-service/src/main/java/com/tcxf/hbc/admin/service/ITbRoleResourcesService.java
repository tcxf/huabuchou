package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbRoleResources;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色资源关系表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRoleResourcesService extends IService<TbRoleResources> {

    public List<TbRoleResources> findByResourceId(Map<String,Object> map);

    public void role(String id,String ids);
}
