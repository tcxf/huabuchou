package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbRole;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbRoleService extends IService<TbRole> {


    public Page selectPage(String aid);

    /**
     * 查询角色
     * @param map
     * @return
     */
    public List<TbRole> selectRole(Map<String , Object> map);

}
