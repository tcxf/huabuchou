package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbScoreLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户积分明细表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbScoreLogService extends IService<TbScoreLog> {

}
