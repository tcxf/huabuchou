package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.common.util.Query;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  交易结算表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbSettlementDetailService extends IService<TbSettlementDetail> {

    BigDecimal findPAlreadySettled(Map<String, Object> paramMap);

    Page findClearDetailPageInfo(Query<TbSettlementDetailDto> tbSettlementDetailDtoQuery);

    Map findclearMoneyInfo(Map<String,Object> paramMap);

    BigDecimal findWaitSettled(Map<String,Object> paramMap);

    /**
     * 运营商查询商户结算
     * @param condition
     * @return
     */
    Page findOperaSettlement( Map<String,Object> condition);

    /**
     * 查询已结算总额和未结算总额
     * @param map
     * @return
     */
    int totalAmount(Map<String , Object> map);

    Page findSettlementPageInfo(Query<TbSettlementDetailDto> tbSettlementDetailDtoQuery);

    Map<String,Object> fanYongInfo(Map<String,Object> paramMap);

    Map<String,Object> findSettlementHeadInfo(Map<String,Object> paramMap);

    Map<String,Object> findPFanYongWaitSet(Map<String,Object> paramMap);

    Map<String,Object> findPFanYongAlreadySet(Map<String,Object> paramMap);


    /**
     * 资金端查询商户结算记录
     * @param condition
     * @return
     */
    Page QueryALLSettlement( Query<TbSettlementDetailDto> condition);

    List<TbSettlementDetailDto> QueryALLSettlementx(Map<String,Object> map);
    /**
     * 资金端结算管理待结算金额
     * @param paramMap
     * @return
     */
    Map<String,Object> DAmount (Map<String,Object> paramMap);

    /**
     * 资金端结算管理已结算金额
     * @param paramMap
     * @return
     */
    Map<String,Object> YAmount (Map<String,Object> paramMap);

    /**
     * 资金端结算管理运营商返佣 商户返佣 会员返佣
     * @param paramMap
     * @return
     */
    Map<String,Object> Maids (Map<String,Object> paramMap);


    /**
     * 资金端查询商户结算记录
     * @param condition
     * @return
     */
    Page TransactionDetails( Query<TbSettlementDetailDto> condition);

    /**
     * 资金端线下结算
     */
    void doSettlement(HttpServletRequest request);

}
