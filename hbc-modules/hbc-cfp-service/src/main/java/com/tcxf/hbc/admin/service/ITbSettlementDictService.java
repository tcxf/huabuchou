package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbSettlementDict;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 结算方式比例字典表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbSettlementDictService extends IService<TbSettlementDict> {

}
