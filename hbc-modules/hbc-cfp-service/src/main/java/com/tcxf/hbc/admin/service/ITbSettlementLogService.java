package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbSettlementLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  结算支付记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbSettlementLogService extends IService<TbSettlementLog> {

}
