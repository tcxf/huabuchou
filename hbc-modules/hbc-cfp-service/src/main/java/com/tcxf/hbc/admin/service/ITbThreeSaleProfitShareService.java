package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 三级分销利润分配表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbThreeSaleProfitShareService extends IService<TbThreeSaleProfitShare> {

}
