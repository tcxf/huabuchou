package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 三级分销利润分配比例表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbThreeSalesRateService extends IService<TbThreeSalesRate> {

}
