package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbThreeSalesRef;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 三级分销用户关系绑定表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbThreeSalesRefService extends IService<TbThreeSalesRef> {

}
