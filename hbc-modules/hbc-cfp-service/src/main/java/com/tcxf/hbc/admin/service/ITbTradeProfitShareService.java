package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbTradeProfitShare;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 交易利润分配表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbTradeProfitShareService extends IService<TbTradeProfitShare> {

}
