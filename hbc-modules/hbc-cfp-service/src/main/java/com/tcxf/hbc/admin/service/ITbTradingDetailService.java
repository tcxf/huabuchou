package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.model.dto.UnoutSettmentDto;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户交易记录表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbTradingDetailService extends IService<TbTradingDetail> {
    public List<Map<String,Object>> findsevendaytrade( Map<String, Object> paramMap);

    public BigDecimal selectTotalMoney(Map<String,Object> paramMap);
            BigDecimal selectTotalMoneyx(Map<String,Object> paramMap);
    Map<String,Object> findPtotalData( Map<String, Object> totalMap);

    Page findAdvancePayDetailInfo(Query<TbTradingDetailDto> paramMap);

    Page findNoBillPageInfo(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);

    List<TbTradingDetailDto> findNoBillPageInfoB(Map<String,Object> map);

    Page findTradingPageInfo(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);

    BigDecimal findTradingtotalAmount(Map<String,Object> paramMap);

    Page uncreateSettlementDetail(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);

    /**
     * 查询已还账单明细
     * @param map
     * @return
     */
    Page findTradingDetail(Map<String , Object> map);

    /**
     * 查询未出账单金额总额
     * @param map
     * @return
     */
    BigDecimal uncreateAmount(Map<String , Object> map);

    Page findOperaPageInfo(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);

    Map<String,Object> findTotalProfit(Map<String,Object> paramMap);

    /**
     * 运营商查询商户让利
     * @param query
     * @return
     */
    Page msfManagerList(Query<TbTradingDetailDto> query);

    /**
     * 运营商查询商户让利下载报表
     * liaozeyong
     * @param map
     * @return
     */
    List<TbTradingDetailDto> msf(Map<String , Object> map);

    BigDecimal metchatTotalAmount(Map<String , Object> map);

//    运行商交易总额
    int TotalAmount(Map<String,Object> paramMap);

    Page findNoBillDetail(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);

    Page findSettlementDetailInfo(Query<TbTradingDetailDto> tbTradingDetailDtoQuery);


    /**
     * 查询平台交易记录分页
     * @param condition
     * @return
     */
    Page transaction(Query<TransactionDto> condition);

    /**
     * 查询平台交易下载报表
     * @param map
     * @return
     */
    List<TransactionDto> transactionB(Map<String,Object> map);

    /**
     * 查询交易总额 运营商抽成总额 	直接返佣总额	间接级返佣总额
     * @param map
     * @return
     */
    Map<String,Object> SUM(Map<String , Object> map);

    Page findNoSettlementPageInfo(Query<UnoutSettmentDto> query);

    Page findMerchantSettlementDetail(Query<TbTradingDetailDto> query);

    Map<String,Object> findNoBillTotalMoney(Map<String,Object> paramMap);
}
