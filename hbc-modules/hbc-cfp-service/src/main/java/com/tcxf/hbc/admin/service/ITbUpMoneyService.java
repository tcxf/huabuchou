package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbUpMoney;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 授信提额表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
public interface ITbUpMoneyService extends IService<TbUpMoney> {

}
