package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbUserAccountService extends IService<TbUserAccount> {

}
