package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户类型表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbUserAccountTypeConService extends IService<TbUserAccountTypeCon> {

}
