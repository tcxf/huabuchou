package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbUserSharefeeSettlement;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户分润结算表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface ITbUserSharefeeSettlementService extends IService<TbUserSharefeeSettlement> {

}
