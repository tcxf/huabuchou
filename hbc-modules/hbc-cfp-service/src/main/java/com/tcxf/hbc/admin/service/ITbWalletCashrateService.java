package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
public interface ITbWalletCashrateService extends IService<TbWalletCashrate> {

}
