package com.tcxf.hbc.admin.service;

import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbWalletIncomeService extends IService<TbWalletIncome> {
    /**
     * 查询钱包的收入支出记录
     */
    public Page getWalletIncomeList(Map<String,Object> map);

    /**
     * 查询收支明细 1.为入金 2.为提现出金 3.消费出金
     * @param type
     * @return
     */
    TbWalletIncome getamount(String type , String wid);
}
