package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbWalletWithdrawalsService extends IService<TbWalletWithdrawals> {

    /**
     * 钱包提现分页查询
     */
    public Page findWalletwithdrawalsList(Map<String,Object> map);

    /**
     * 钱包提现
     */
    public int savaWallet(TbWalletWithdrawals t);

}
