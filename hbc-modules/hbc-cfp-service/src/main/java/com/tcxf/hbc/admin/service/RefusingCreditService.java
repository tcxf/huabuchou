package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RefusingCredit;

import java.util.List;
import java.util.Map;

/**
 * 拒绝授信
 * @Auther: liuxu
 * @Date: 2018/9/19 16:14
 * @Description:
 */
public interface RefusingCreditService extends IService<RefusingCredit> {

    public void createRefusingCredit(List<RefusingCredit> list,String fid);

    public List<RefusingCredit> selectRefusingCreditByfId(Map<String, Object> map);

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    public RefusingCredit selectRefusingCreditById(String id);
}
