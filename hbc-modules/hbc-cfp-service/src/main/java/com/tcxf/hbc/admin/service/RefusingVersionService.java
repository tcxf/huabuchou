package com.tcxf.hbc.admin.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RefusingVersion;

/**
 * 授信条件版本
 * @Auther: liuxu
 * @Date: 2018/9/19 16:14
 * @Description:
 */
public interface RefusingVersionService extends IService<RefusingVersion> {

    /**
     * 产生新的版本号
     * @param fid
     * @return
     */
    public String createVersion(String fid,String type);

}
