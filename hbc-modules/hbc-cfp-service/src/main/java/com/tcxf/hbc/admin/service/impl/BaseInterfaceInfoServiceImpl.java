package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.BaseInterfaceInfoMapper;
import com.tcxf.hbc.admin.service.IBaseInterfaceInfoService;
import com.tcxf.hbc.common.entity.BaseInterfaceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class BaseInterfaceInfoServiceImpl extends ServiceImpl<BaseInterfaceInfoMapper, BaseInterfaceInfo> implements IBaseInterfaceInfoService {

    protected org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    @Autowired
    BaseInterfaceInfoMapper baseInterfaceInfoMapper;

    public HashMap<String, Object> getBaseInterfaceInfoList(){
        HashMap<String, Object> ibfismap = new HashMap<>();

        try {
            List<BaseInterfaceInfo> list = selectList((new EntityWrapper<BaseInterfaceInfo>()));
            for (BaseInterfaceInfo baseInterfaceInfo:list) {
                ibfismap.put(baseInterfaceInfo.getName(),baseInterfaceInfo.getValue());
            }

        }catch (Exception e){
            logger.info(e.getMessage());
        }

        return ibfismap;
    }
}
