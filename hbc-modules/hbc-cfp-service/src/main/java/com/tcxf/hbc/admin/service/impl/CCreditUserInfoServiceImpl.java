package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.CCreditUserInfoDto;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.tcxf.hbc.admin.mapper.CCreditUserInfoMapper;
import com.tcxf.hbc.admin.service.ICCreditUserInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
@Service
public class CCreditUserInfoServiceImpl extends ServiceImpl<CCreditUserInfoMapper, CCreditUserInfo> implements ICCreditUserInfoService {
    @Autowired
    private CCreditUserInfoMapper cCreditUserInfoMapper;

    @Override
    public Page Bank(Query <CCreditUserInfoDto> query) {
        List<CCreditUserInfoDto> list = new ArrayList<CCreditUserInfoDto>();
        List<CCreditUserInfoDto> bank=cCreditUserInfoMapper.Bank(query,query.getCondition());
        List<CCreditUserInfoDto> Operatoryd=cCreditUserInfoMapper.Operatoryd(query,query.getCondition());
        List<CCreditUserInfoDto> Operatorlt=cCreditUserInfoMapper.Operatorlt(query,query.getCondition());
        List<CCreditUserInfoDto> Operatordx=cCreditUserInfoMapper.Operatordx(query,query.getCondition());
        List<CCreditUserInfoDto> Multipleliabilities=cCreditUserInfoMapper.Multipleliabilities(query,query.getCondition());
        List<CCreditUserInfoDto> Phone=cCreditUserInfoMapper.Phone(query,query.getCondition());
        List<CCreditUserInfoDto> Getloginmode=cCreditUserInfoMapper.Getloginmode(query,query.getCondition());
        List<CCreditUserInfoDto> Operatorlogin=cCreditUserInfoMapper.Operatorlogin(query,query.getCondition());
        List<CCreditUserInfoDto> Socialsecurity=cCreditUserInfoMapper.Socialsecurity(query,query.getCondition());
        List<CCreditUserInfoDto> Accumulationfund=cCreditUserInfoMapper.Accumulationfund(query,query.getCondition());
        List<CCreditUserInfoDto> Learningletter=cCreditUserInfoMapper.Learningletter(query,query.getCondition());
        List<CCreditUserInfoDto> TaoBao=cCreditUserInfoMapper.TaoBao(query,query.getCondition());
        List<CCreditUserInfoDto> JD=cCreditUserInfoMapper.JD(query,query.getCondition());
        List<CCreditUserInfoDto> DDZMF=cCreditUserInfoMapper.DDZMF(query,query.getCondition());
        list.addAll(bank);
        list.addAll(Operatoryd);
        list.addAll(Operatorlt);
        list.addAll(Operatordx);
        list.addAll(Multipleliabilities);
        list.addAll(Phone);
        list.addAll(Getloginmode);
        list.addAll(Operatorlogin);
        list.addAll(Socialsecurity);
        list.addAll(Accumulationfund);
        list.addAll(Learningletter);
        list.addAll(TaoBao);
        list.addAll(JD);
        list.addAll(DDZMF);
        return query.setRecords(list);
    }

    @Override
    public Page Operatoryd(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Operatorlt(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Operatordx(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Multipleliabilities(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Phone(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Getloginmode(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Operatorlogin(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Socialsecurity(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Accumulationfund(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page Learningletter(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page TaoBao(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page JD(Query <CCreditUserInfoDto> query) {
        return null;
    }

    @Override
    public Page DDZMF(Query <CCreditUserInfoDto> query) {
        return null;
    }

    //以下是poi导出
    @Override
    public List <CCreditUserInfoDto> Bank(Map<String, Object> map) {
        List<CCreditUserInfoDto> list = new ArrayList<CCreditUserInfoDto>();
        List<CCreditUserInfoDto> bank=cCreditUserInfoMapper.Bank(map);
        List<CCreditUserInfoDto> Operatoryd=cCreditUserInfoMapper.Operatoryd(map);
        List<CCreditUserInfoDto> Operatorlt=cCreditUserInfoMapper.Operatorlt(map);
        List<CCreditUserInfoDto> Operatordx=cCreditUserInfoMapper.Operatordx(map);
        List<CCreditUserInfoDto> Multipleliabilities=cCreditUserInfoMapper.Multipleliabilities(map);
        List<CCreditUserInfoDto> Phone=cCreditUserInfoMapper.Phone(map);
        List<CCreditUserInfoDto> Getloginmode=cCreditUserInfoMapper.Getloginmode(map);
        List<CCreditUserInfoDto> Operatorlogin=cCreditUserInfoMapper.Operatorlogin(map);
        List<CCreditUserInfoDto> Socialsecurity=cCreditUserInfoMapper.Socialsecurity(map);
        List<CCreditUserInfoDto> Accumulationfund=cCreditUserInfoMapper.Accumulationfund(map);
        List<CCreditUserInfoDto> Learningletter=cCreditUserInfoMapper.Learningletter(map);
        List<CCreditUserInfoDto> TaoBao=cCreditUserInfoMapper.TaoBao(map);
        List<CCreditUserInfoDto> JD=cCreditUserInfoMapper.JD(map);
        List<CCreditUserInfoDto> DDZMF=cCreditUserInfoMapper.DDZMF(map);
        list.addAll(bank);
        list.addAll(Operatoryd);
        list.addAll(Operatorlt);
        list.addAll(Operatordx);
        list.addAll(Multipleliabilities);
        list.addAll(Phone);
        list.addAll(Getloginmode);
        list.addAll(Operatorlogin);
        list.addAll(Socialsecurity);
        list.addAll(Accumulationfund);
        list.addAll(Learningletter);
        list.addAll(TaoBao);
        list.addAll(JD);
        list.addAll(DDZMF);
        return list;
    }

    @Override
    public List <CCreditUserInfoDto> Operatoryd(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Operatorlt(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Operatordx(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Multipleliabilities(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Phone(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Getloginmode(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Operatorlogin(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Socialsecurity(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Accumulationfund(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> Learningletter(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> TaoBao(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> JD(Map <String, Object> map) {
        return null;
    }

    @Override
    public List <CCreditUserInfoDto> DDZMF(Map <String, Object> map) {
        return null;
    }
}
