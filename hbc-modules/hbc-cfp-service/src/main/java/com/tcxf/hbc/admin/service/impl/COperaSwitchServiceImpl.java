package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.COperaSwitch;
import com.tcxf.hbc.admin.mapper.COperaSwitchMapper;
import com.tcxf.hbc.admin.service.ICOperaSwitchService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商开关表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
@Service
public class COperaSwitchServiceImpl extends ServiceImpl<COperaSwitchMapper, COperaSwitch> implements ICOperaSwitchService {

}
