package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.tcxf.hbc.admin.mapper.CUserCreditApplyMapper;
import com.tcxf.hbc.admin.service.ICUserCreditApplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提额申请审核记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class CUserCreditApplyServiceImpl extends ServiceImpl<CUserCreditApplyMapper, CUserCreditApply> implements ICUserCreditApplyService {

}
