package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.CreditPageInfoDto;
import com.tcxf.hbc.admin.model.dto.CuserinfoDto;
import com.tcxf.hbc.admin.model.dto.QueryCreditRecordDto;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.admin.mapper.CUserInfoMapper;
import com.tcxf.hbc.admin.service.ICUserInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.secure.DES;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class CUserInfoServiceImpl extends ServiceImpl<CUserInfoMapper, CUserInfo> implements ICUserInfoService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public CUserInfoMapper cUserInfoMapper;


    @Override
    public Page <CuserinfoDto> userlist(Query <CuserinfoDto> query) {
        List<CuserinfoDto> list=cUserInfoMapper.userlist(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:19 2018/6/20
     *根据条件查询消费者的数量
    */
    @Override
    public int selectConsumercount(Map<String, Object> mmp) {
        return cUserInfoMapper.selectConsumercount(mmp);
    }


    /**
     * 更具id查询消费者信息
     * @param uid
     * @return
     */
    @Override
    public CuserinfoDto QueryAll(String uid) {
        return cUserInfoMapper.QueryAll(uid);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:13 2018/6/26
     * 根据条件 授信/非授信 查询用户数量
    */
    @Override
    public int findAlreadyAuthNum(Map<String, Object> paramMap) {
        return cUserInfoMapper.findAlreadyAuthNum(paramMap);
    }

    @Override
    public Page OQueryAll(Query <CuserinfoDto> query) {
        List<CuserinfoDto> list = cUserInfoMapper.OQueryAll(query,query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public int CreateQueryAllDate(String start, String end) {
        return cUserInfoMapper.CreateQueryAllDate(start,end);
    }

    @Override
    public int FCreateQueryAllDate(String start, String end, String fid) {
        return cUserInfoMapper.FCreateQueryAllDate(start,end,fid);
    }

    @Override
    public Page  QueryCreditRecord(Query <QueryCreditRecordDto> query) {
        BigDecimal balance;
        BigDecimal maxMoney;
        List<QueryCreditRecordDto> list = cUserInfoMapper.QueryCreditRecord(query,query.getCondition());
        for(QueryCreditRecordDto attribute : list) {
            try {
                balance =new BigDecimal(DES.decryptdf(attribute.getBalance()));
                maxMoney= new BigDecimal(DES.decryptdf(attribute.getMaxMoney()));
                balance  = balance.setScale(2,BigDecimal.ROUND_FLOOR);
                maxMoney = maxMoney.setScale(2,BigDecimal.ROUND_FLOOR);
                BigDecimal sumMoeny = maxMoney.subtract(balance);
                attribute.setSumMoney(String.valueOf(sumMoeny));
                attribute.setMaxMoney(String.valueOf(maxMoney));
                attribute.setBalance(String.valueOf(balance));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return query.setRecords(list);
    }
    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:21 2018/9/17
     * 查询用户授信明细(已授信、已拒绝)
     */
    @Override
    public Page findCuserCreditPageInfo(Query<CreditPageInfoDto> query) {
        BigDecimal balance;
        BigDecimal maxMoney;
        List<CreditPageInfoDto> list = cUserInfoMapper.findCuserCreditPageInfo(query,query.getCondition());
        //解密可用授信余额 以及授信总额度
        try {
            for (CreditPageInfoDto creditPageInfoDto:list) {
                 /* String balance = creditPageInfoDto.getBalance();
                String maxMoney = creditPageInfoDto.getMaxMoney();*/

                balance =new BigDecimal(DES.decryptdf(creditPageInfoDto.getBalance()));
                maxMoney =new BigDecimal(DES.decryptdf( creditPageInfoDto.getMaxMoney()));

                balance  = balance.setScale(2,BigDecimal.ROUND_FLOOR);
                maxMoney = maxMoney.setScale(2,BigDecimal.ROUND_FLOOR);

                creditPageInfoDto.setBalance(balance.toString());
                creditPageInfoDto.setMaxMoney(maxMoney.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("解密失败",e);
        }
        return query.setRecords(list);
    }

    @Override
    public List <QueryCreditRecordDto> xz(Map <String, Object> map) {
        BigDecimal balance;
        BigDecimal maxMoney;
        List <QueryCreditRecordDto> list= cUserInfoMapper.QueryCreditRecord(map);
        for(QueryCreditRecordDto attribute : list) {
            try {
                balance =new BigDecimal(DES.decryptdf(attribute.getBalance()));
                maxMoney= new BigDecimal(DES.decryptdf(attribute.getMaxMoney()));
                BigDecimal sumMoeny = maxMoney.subtract(balance);
                attribute.setSumMoney(String.valueOf(sumMoeny));
                attribute.setMaxMoney(String.valueOf(maxMoney));
                attribute.setBalance(String.valueOf(balance));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

}
