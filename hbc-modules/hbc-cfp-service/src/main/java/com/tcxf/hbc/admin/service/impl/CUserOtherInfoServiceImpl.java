package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.tcxf.hbc.admin.mapper.CUserOtherInfoMapper;
import com.tcxf.hbc.admin.service.ICUserOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消费者用户其他信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class CUserOtherInfoServiceImpl extends ServiceImpl<CUserOtherInfoMapper, CUserOtherInfo> implements ICUserOtherInfoService {

}
