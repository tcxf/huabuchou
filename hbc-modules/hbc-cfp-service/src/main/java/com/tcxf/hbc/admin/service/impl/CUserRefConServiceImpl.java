package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.admin.mapper.CUserInfoMapper;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.admin.mapper.CUserRefConMapper;
import com.tcxf.hbc.admin.service.ICUserRefConService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class CUserRefConServiceImpl extends ServiceImpl<CUserRefConMapper, CUserRefCon> implements ICUserRefConService {

    @Autowired
    CUserRefConMapper cUserRefConMapper;

    @Override
    public CUserRefCon finduserCon(Map<String,Object> map) {
        return cUserRefConMapper.finduserCon(map);
    }

    @Override
    public int findthisVersionNumByRefusingCode(HashMap<String, Object> paramMap) {
        return cUserRefConMapper.findthisVersionNumByRefusingCode(paramMap);
    }
}
