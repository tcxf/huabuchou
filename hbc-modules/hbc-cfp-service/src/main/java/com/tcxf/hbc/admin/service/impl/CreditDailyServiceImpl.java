package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.CreditDaily;
import com.tcxf.hbc.admin.mapper.CreditDailyMapper;
import com.tcxf.hbc.admin.service.ICreditDailyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信日报表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-18
 */
@Service
public class CreditDailyServiceImpl extends ServiceImpl<CreditDailyMapper, CreditDaily> implements ICreditDailyService {

    @Autowired
    private CreditDailyMapper creditDailyMapper;

    @Override
    public Page findPlist(Map<String, Object> condition) {
        Query<CreditDaily> query = new Query(condition);
        List<CreditDaily> list = creditDailyMapper.getList(query, query.getCondition());
        query.setRecords(list);
        return query;
    }

}
