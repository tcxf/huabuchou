package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.CreditScoreMapper;
import com.tcxf.hbc.admin.service.CreditScoreService;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.admin.service.RefusingVersionService;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 授信提额综合分值service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class CreditScoreServiceImpl extends ServiceImpl<CreditScoreMapper, CreditScore>  implements CreditScoreService {

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired

    private CreditScoreMapper creditScoreMapper;

    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;

    /**
     * 创建综合分值条件
     * @param list
     * @param fid
     */
    @Override
    @Transactional
    public void createCreditScore(List<CreditScore> list, String fid) {
        //1、数据验证
        validateCreateCreditScore(list,fid);
        //3、创建新版本
        String refusingVersionId = insertRefusingVersion(fid);
        //4、创建综合分值条件
        for(CreditScore r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);

        //修改大版本关联小版本id
        updateRmSxtemplateRelationshipById(list, refusingVersionId);
    }


    /**
     * @Description: 修改大版本关联小版本id
     * @Param: [list, refusingVersionId]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/10/24
     */
    public void updateRmSxtemplateRelationshipById(List<CreditScore> list, String refusingVersionId){

        RmSxtemplateRelationship rmSxtemplateRelationship = new RmSxtemplateRelationship();
        rmSxtemplateRelationship.setId(list.get(0).getRelationshipId());
        rmSxtemplateRelationship.setSxUpmoneyFocusId(refusingVersionId);

        RmSxtemplateRelationship rsrelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", list.get(0).getRelationshipId()));
        String updateModule = rsrelationship.getUpdateModule();
        if (!updateModule.contains("授信提额综合分值")){
            updateModule = updateModule + "授信提额综合分值,";
        }
        rmSxtemplateRelationship.setUpdateModule(updateModule);

        iRmSxtemplateRelationshipService.updateById(rmSxtemplateRelationship);
    }


    private void validateCreateCreditScore(List<CreditScore> list, String fid){
        int  i  =1;
        for(CreditScore r : list){
            if(ValidateUtil.isEmpty(r.getMinScore())||ValidateUtil.isEmpty(r.getMaxScore())){
                throw new CheckedException("第"+i+"行条件不能为空");
            }
            i++;
        }

        Map<String, Object> tmap = new HashMap<>();
        tmap.put("fid", fid);
        tmap.put("id", list.get(0).getRelationshipId());

        List<CreditScore> selectList = selectCreditScoreByfId(tmap);
        if(selectList. size()!=list.size()){
            return;
        }
        //分组
        Map<Integer ,CreditScore> map = new HashMap<Integer, CreditScore>();
        for(CreditScore r : list){
            map.put(r.getSeq(),r);
        }
        for(CreditScore r : selectList) {
            if(ValidateUtil.isEmpty(map.get(r.getSeq()))){
                return;
            }
            if(!r.getMinScore().equals(map.get(r.getSeq()).getMinScore())||
                    !r.getMaxScore().equals(map.get(r.getSeq()).getMaxScore())||
                    !r.getAmount().equals(map.get(r.getSeq()).getAmount())){
                return;
            }
        }
        throw new CheckedException("未修改条件，无需更新");

    }

    /**
     * 根据资金端ID查询最新版本授信拒绝条件
     * @return
     */
    @Override
    public List<CreditScore> selectCreditScoreByfId(Map<String, Object> map){
        List<CreditScore> list = creditScoreMapper.selectCreditScoreByfId(map);
        return list;
    }

    /**
     * 创建新版本拒绝授信条件
     * @param fid
     * @return
     */
    private String insertRefusingVersion(String fid){
        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_4);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_4));
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }
}
