package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.admin.mapper.FFundendBindMapper;
import com.tcxf.hbc.admin.service.IFFundendBindService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class FFundendBindServiceImpl extends ServiceImpl<FFundendBindMapper, FFundendBind> implements IFFundendBindService {

    @Autowired
    private FFundendBindMapper fFundendBindMapper;

    /**
     * 按条件查询资金端资源表集合
     * @param fid 资金端id
     * @return
     */
    @Override
    public List<FFundendBind> getOperaIdList(String fid) {
        List<FFundendBind> list = fFundendBindMapper.getOperaIdList(fid);
        return list;
    }
}
