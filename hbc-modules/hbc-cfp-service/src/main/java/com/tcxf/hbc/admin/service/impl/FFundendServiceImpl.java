package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.model.dto.FFundendDto;
import com.tcxf.hbc.admin.model.vo.FFundendVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.admin.mapper.FFundendMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.vo.CreditVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金端信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class FFundendServiceImpl extends ServiceImpl<FFundendMapper, FFundend> implements IFFundendService {

    @Autowired
    private FFundendMapper fFundendMapper;
    @Autowired
    private ThreelementController threelementController;
    @Autowired
    private ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    private ITbWalletService iTbWalletService;
    @Autowired
    private ITbPeopleRoleService iTbPeopleRoleService;
    @Autowired
    private IFMoneyInfoService ifMoneyInfoService;

    /**
     * 分页查询fundend
     * liaozeyong
     * @return
     */
    @Override
    public Page queryAll() {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query<FFundend> query = new  Query(params);
//        List<FFundend> list = fFundendMapper.selectPage();
//        query.setRecords(list);
        return query;
    }

    @Override
    public Page selectPages(){
        Map<String, Object> params =new HashMap<>();
        params.put("status","0");
        Query<FFundend> query = new  Query(params);
        List<FFundend> admin= fFundendMapper.selectPage(query,new EntityWrapper<FFundend>());
        query.setRecords(admin);
        return query;
    }

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Override
    @Transactional
    public R addFundend(FFundendVo fFundendVo) {
        R r = new R();
        FFundend fundend =new FFundend();
        FFundend fundend1 = selectOne(new EntityWrapper<FFundend>().eq("mobile",fFundendVo.getMobile()));
        if(fundend1 == null){
            //把vo里面的值拿出来放到entity里面
            fundend.setMobile(fFundendVo.getMobile());
            fundend.setAccount(fFundendVo.getMobile());
            fundend.setFname(fFundendVo.getFname());
            fundend.setPwd(ENCODER.encode(fFundendVo.getMobile().substring(5, 11)));
            fundend.setName(fFundendVo.getName());
            fundend.setBankcard(fFundendVo.getBankcard());
            fundend.setAddress(fFundendVo.getAreaId());
            fundend.setCreateDate(new Date());
            fundend.setAddressInfo(fFundendVo.getAddressInfo());
            fundend.setStatus("1");
            CreditVo creditVo1 = new CreditVo();
            creditVo1.setMobile(fundend.getMobile());
            creditVo1.setIdCard(fundend.getBankcard());
            creditVo1.setUserName(fundend.getName());
//            R r2 = threelementController.operatorsCheck(creditVo1);
//            if (r2.getCode() == 1)
//            {
//                r2.setMsg("身份证校验失败");
//                return r2;
//            }
            TbBindCardInfo tbBindCardInfo = new TbBindCardInfo();
            tbBindCardInfo.setUtype("4");
            tbBindCardInfo.setCardType("0");
            tbBindCardInfo.setBankSn(fFundendVo.getBank_bankSn());
            tbBindCardInfo.setMobile(fFundendVo.getBank_mobile());
            tbBindCardInfo.setName(fFundendVo.getBname());
            tbBindCardInfo.setBankName(fFundendVo.getBank_name());
            tbBindCardInfo.setIdCard(fFundendVo.getBank_idCard());
            tbBindCardInfo.setBankCard(fFundendVo.getBank_bankCard());
            tbBindCardInfo.setIsDefault(fFundendVo.getBank_isDefault());
            CreditVo creditVo2 = new CreditVo();
            creditVo2.setMobile(tbBindCardInfo.getMobile());
            creditVo2.setAccountNO(tbBindCardInfo.getBankCard());
            creditVo2.setIdCard(tbBindCardInfo.getIdCard());
            creditVo2.setUserName(tbBindCardInfo.getName());
//            R r1 = threelementController.threelementCheck(creditVo2);
//            if (r1.getCode() == 1){
//                r1.setMsg("银行卡验证失败");
//                return r1;
//            }
            //添加资金端方法
            insert(fundend);
            //添加资金端授信上限表
            FMoneyInfo fMoneyInfo = new FMoneyInfo();
            fMoneyInfo.setFid(fundend.getId());
            fMoneyInfo.setStatus(FMoneyInfo.STATUS_NO);
            fMoneyInfo.setCreateDate(new Date());
            ifMoneyInfoService.insert(fMoneyInfo);
            //绑定银行卡
            FFundend  f = selectOne(new EntityWrapper<FFundend>().eq("mobile",fundend.getMobile()));
            tbBindCardInfo.setOid(f.getId());
            iTbBindCardInfoService.insert(tbBindCardInfo);
            //添加权限
            TbPeopleRole tbPeopleRole = new TbPeopleRole();
            tbPeopleRole.setPeopleUserType("4");
            tbPeopleRole.setRoleId("3");
            FFundend fFundend = selectOne(new EntityWrapper<FFundend>().eq("mobile",fundend.getMobile()));
            tbPeopleRole.setPeopleUserId(fFundend.getId());
            tbPeopleRole.setCreateDate(new Date());
            iTbPeopleRoleService.insert(tbPeopleRole);
            //绑定钱包
            iTbWalletService.savewallet(f.getId(),4,fFundendVo.getName(),fFundendVo.getMobile(),fFundendVo.getFname());
            r.setMsg("添加成功");
        }else {
            return R.newErrorMsg("开通失败，手机号已经注册");
        }
        return r;
    }

    /**
     * 根据运营商id查询已绑定资金端
     * @param map
     * @return
     */
    @Override
    public Page selectFunendByOid(Map<String, Object> map) {
        Map<String , Object> params = new HashMap<>();
        params.put("page",map.get("page"));
        Query<FFundendDto> query = new Query<>(params);
        List<FFundendDto> fFundends = fFundendMapper.fundendByOidList(query , map);
        query.setRecords(fFundends);
        return query;
    }

    /**
     * 查询资金端详情信息
     * @param id
     * @return
     */
    @Override
    public List<FFundendDto> selectFundendDetail(String id,String oid) {
        Map<String , Object> map = new HashMap<>();
        map.put("id",id);
        map.put("oid",oid);
        List<FFundendDto> list = fFundendMapper.selectFundendDetail(map);
        return list;
    }

    @Override
    public Page QueryFO(Query <FFundendDto> query) {
        List<FFundendDto> list=fFundendMapper.QueryFO(query,query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public FFundendDto QueeryFOone(String id) {
        return fFundendMapper.QueeryFOone(id);
    }

}
