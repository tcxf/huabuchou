package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.FMoneyInfoMapper;
import com.tcxf.hbc.admin.mapper.FMoneyOtherInfoMapper;
import com.tcxf.hbc.admin.service.IFMoneyOtherInfoService;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 授信总金额出入明细表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@Service
public class FMoneyOtherInfoServiceImpl extends ServiceImpl<FMoneyOtherInfoMapper, FMoneyOtherInfo> implements IFMoneyOtherInfoService {
    @Autowired
    private FMoneyOtherInfoMapper fMoneyOtherInfoMapper;

    @Override
    public BigDecimal Summoney(String fid) {
        return fMoneyOtherInfoMapper.Summoney(fid);
    }

    @Override
    public BigDecimal DaySummoney(String start, String end,String fid) {
        return fMoneyOtherInfoMapper.DaySummoney(start,end,fid);
    }

    @Override
    public BigDecimal DayYUmoney(String fid) {
        return fMoneyOtherInfoMapper.DayYUmoney(fid);
    }

    @Override
    public BigDecimal LJSummoney(String fid) {
        return fMoneyOtherInfoMapper.LJSummoney(fid);
    }

    @Override
    public BigDecimal Summoneys(String fid) {
        return fMoneyOtherInfoMapper.Summoneys(fid);
    }

    @Override
    public BigDecimal DayYUmoneys(String fid) {
        return fMoneyOtherInfoMapper.DayYUmoneys(fid);
    }

    @Override
    public BigDecimal DayYUmoneyss(String fid) {
        return fMoneyOtherInfoMapper.DayYUmoneyss(fid);
    }
}
