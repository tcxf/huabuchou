package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.admin.mapper.IndexBannerMapper;
import com.tcxf.hbc.admin.service.IIndexBannerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页轮播图表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class IndexBannerServiceImpl extends ServiceImpl<IndexBannerMapper, IndexBanner> implements IIndexBannerService {


}
