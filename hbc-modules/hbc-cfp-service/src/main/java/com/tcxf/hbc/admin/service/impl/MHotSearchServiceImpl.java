package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.MHotSearchDto;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.tcxf.hbc.admin.mapper.MHotSearchMapper;
import com.tcxf.hbc.admin.service.IMHotSearchService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 热搜店铺表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MHotSearchServiceImpl extends ServiceImpl<MHotSearchMapper, MHotSearch> implements IMHotSearchService {

    @Autowired
    private MHotSearchMapper mHotSearchMapper;
    @Override
    public Page selectHotSearchByRid(Map<String, Object> map) {
        Query query = new Query(map);
        List<MHotSearchDto> list = mHotSearchMapper.selectHotSearchByRid(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    /**
     * 修改前的查询
     * @param map
     * @return
     */
    @Override
    public MHotSearchDto selectHotSearch(Map<String, Object> map) {
        MHotSearchDto mHotSearchDto = mHotSearchMapper.selectHotSearchByRid(map);
        return mHotSearchDto;
    }
}
