package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.admin.mapper.MMerchantCategoryMapper;
import com.tcxf.hbc.admin.service.IMMerchantCategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 商户行业分类表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MMerchantCategoryServiceImpl extends ServiceImpl<MMerchantCategoryMapper, MMerchantCategory> implements IMMerchantCategoryService {
}
