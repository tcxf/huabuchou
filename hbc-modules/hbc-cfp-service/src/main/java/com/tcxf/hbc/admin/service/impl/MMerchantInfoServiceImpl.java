package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.JavaType;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.mapper.MMerchantInfoMapper;
import com.tcxf.hbc.admin.mapper.MMerchantOtherInfoMapper;
import com.tcxf.hbc.admin.mapper.TbBindCardInfoMapper;
import com.tcxf.hbc.admin.model.dto.MMerchantDto;
import com.tcxf.hbc.admin.model.dto.MMerchantInfoDto;
import com.tcxf.hbc.admin.model.vo.MerchantlnfoVo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonUtils;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.util.secure.MD5Util;
import com.tcxf.hbc.common.vo.CreditVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 商户信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MMerchantInfoServiceImpl extends ServiceImpl <MMerchantInfoMapper, MMerchantInfo> implements IMMerchantInfoService {

    @Autowired
    private MMerchantInfoMapper mMerchantInfoMapper;

    @Autowired
    private MMerchantOtherInfoMapper mMerchantOtherInfoMapper;

    @Autowired
    private TbBindCardInfoMapper tbBindCardInfoMapper;

    @Autowired
    private ThreelementController threelementController;

    @Autowired
    public IMMerchantInfoService imMerchantInfoService;

    @Autowired
    public IMMerchantOtherInfoService imMerchantOtherInfoService;

    @Autowired
    public ITbBindCardInfoService iTbBindCardInfoService;

    @Autowired
    public ICUserOtherInfoService icUserOtherInfoService;

    @Autowired
    public ITbUserAccountTypeConService iTbUserAccountTypeConService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Override
    public Page findMerchantManagerPageInfo(Query <MMerchantInfoDto> query) {
        List <MMerchantInfoDto> list = mMerchantInfoMapper.findMerchantManagerPageInfo(query, query.getCondition());
        return query.setRecords(list);
    }


    /**
     * 查询运营商下所有的商户
     *
     * @param map
     * @return
     */
    @Override
    public Page selectPage(Map <String, Object> map) {
        Map <String, Object> params = new HashMap <>();
        params.put("status", "0");
        params.put("page", map.get("page"));
        Query <MMerchantInfo> query = new Query(params);
        List <MMerchantInfo> merchantInfos = mMerchantInfoMapper.findByLike(query, map);
        query.setRecords(merchantInfos);
        return query;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:28 2018/6/19
     * 新增商户信息
     */
    @Override
    @Transactional
    public void addMerchantInfo(Map <String, Object> paramMap) {
        //添加基础信息
        try {
            MMerchantInfo mMerchantInfo = new MMerchantInfo();
            MapUtil.getBeanForMap(mMerchantInfo, paramMap);
            mMerchantInfo.setCreateDate(new Date());
            MD5Util md5Util = new MD5Util();
            mMerchantInfo.setPwd(md5Util.MD5Encode(mMerchantInfo.getPwd(), "utf-8"));
            mMerchantInfoMapper.insert(mMerchantInfo);
            String mid = mMerchantInfo.getId(); //商户主键id

            //添加卡信息
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbBindCardInfo.class);
            List <TbBindCardInfo> list = (List <TbBindCardInfo>) JsonUtils.fromJson(MapUtil.getStringForMapObj(paramMap, "bind_card", true), javaType);
            for (TbBindCardInfo TbBindCardInfo : list) {
                TbBindCardInfo.setOid(mid);
                tbBindCardInfoMapper.insert(TbBindCardInfo);
            }

            //添加其它信息
            MMerchantOtherInfo mMerchantOtherInfo = new MMerchantOtherInfo();
            mMerchantOtherInfo.setMid(mid);
            MapUtil.getBeanForMap(mMerchantOtherInfo, paramMap);
            mMerchantOtherInfoMapper.insert(mMerchantOtherInfo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常！请稍收" + e);
        }

    }


    @Override
    @Transactional
    public void updateMerchantInfo(Map <String, Object> paramMap) {
        //添加基础信息
        MMerchantInfo mMerchantInfo = new MMerchantInfo();
        MapUtil.getBeanForMap(mMerchantInfo, paramMap);
        MD5Util md5Util = new MD5Util();
        mMerchantInfo.setPwd(md5Util.MD5Encode(mMerchantInfo.getPwd(), "utf-8"));

        CreditVo creditCto = new CreditVo();
        creditCto.setMobile(mMerchantInfo.getMobile());
        creditCto.setIdCard(mMerchantInfo.getIdCard());
        creditCto.setUserName(mMerchantInfo.getLegalName());
        R r2 = threelementController.threelementCheck(creditCto);
        if (r2.getCode() == 1) {
            throw new CheckedException("请输入正确的法人信息！");
        }
        mMerchantInfoMapper.updateById(mMerchantInfo);
        String mid = mMerchantInfo.getId(); //商户主键id

        //添加卡信息
        tbBindCardInfoMapper.delete(new EntityWrapper <TbBindCardInfo>().eq("oid", mid));
        JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbBindCardInfo.class);
           /* List<TbBindCardInfo> list = (List<TbBindCardInfo>) JsonUtils.fromJson(MapUtil.getStringForMapObj(paramMap, "bind_card", true), javaType);
            for (TbBindCardInfo TbBindCardInfo:list){
                CreditVo creditVo = new CreditVo();
                creditVo.setMobile(TbBindCardInfo.getMobile());
                creditVo.setAccountNO(TbBindCardInfo.getBankCard());
                creditVo.setIdCard(TbBindCardInfo.getIdCard());
                creditVo.setUserName(TbBindCardInfo.getName());
                R r1 = threelementController.threelementCheck(creditVo);
                if (r1.getCode() == 1){
                    throw new CheckedException("请输入正确的绑卡信息！");
                }
                TbBindCardInfo.setOid(mid);
                tbBindCardInfoMapper.insert(TbBindCardInfo);
            }*/
        //添加其它信息
        MMerchantOtherInfo mMerchantOtherInfo = new MMerchantOtherInfo();
        MapUtil.getBeanForMap(mMerchantOtherInfo, paramMap);
        mMerchantOtherInfoMapper.update(mMerchantOtherInfo, new EntityWrapper <MMerchantOtherInfo>().eq("mid", mid));
    }

    @Override
    public Page QueryAll(Query <MMerchantDto> query) {
        List <MMerchantDto> list = mMerchantInfoMapper.QueryAll(query, query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public MMerchantDto QueryOne(String id) {
        return mMerchantInfoMapper.QueryOne(id);
    }

    /**
     * 热门搜索查询商户列表
     * liaozeyong
     *
     * @param map
     * @return
     */
    @Override
    public Page findByOid(Map <String, Object> map) {
        Query query = new Query(map);
        List <MMerchantDto> list = mMerchantInfoMapper.findByOid(query, query.getCondition());
        query.setRecords(list);
        return query;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:34 2018/6/26
     * 查询授信/非授信 的商户数量
     */
    @Override
    public int findAlreadyAuthNum(Map <String, Object> paramMap) {
        return mMerchantInfoMapper.findAlreadyAuthNum(paramMap);
    }


    /**
     * 修改商户信息
     * jiangy
     */
    @Override
    @Transactional
    public void updateById(MerchantlnfoVo m, HttpServletRequest request) {
        Map <String, Object> paramMap = MapUtil.getMapForRequest(request);
        String minmoney = request.getParameter("minmoney");
        String latestday = request.getParameter("latestday");
        MMerchantInfo minfo = imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id", m.getId()));
        MMerchantOtherInfo mm = imMerchantOtherInfoService.selectOne(new EntityWrapper <MMerchantOtherInfo>().eq("mid", m.getId()));
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper <TbUserAccountTypeCon>().eq("con_id", minfo.getId()));
        minfo.setName(m.getName());
        minfo.setSimpleName(m.getSimpleName());
        if (m.getPwd() == null || m.getPwd().equals("")) {
        } else {
            minfo.setPwd(ENCODER.encode(m.getPwd()));
        }
        CreditVo creditCto = new CreditVo();
        creditCto.setMobile(m.getMobile());
        creditCto.setIdCard(m.getIdCard());
        creditCto.setUserName(m.getLegalName());
        R r2 = threelementController.threelementCheck(creditCto);
        if (r2.getCode() == 1) {
            throw new CheckedException("请输入正确的法人信息！");
        }
        minfo.setMicId(m.getMicId());
        minfo.setLegalName(m.getLegalName());
        minfo.setIdCard(m.getIdCard());
        minfo.setMobile(m.getMobile());
        minfo.setArea(m.getArea());
        minfo.setAddress(m.getAddress());
        minfo.setStatus(m.getStatus());
        mm.setNature(m.getNature());
        mm.setOperArea(m.getOperArea());
        mm.setOperYear(m.getOperYear());
        mm.setRegMoney(m.getRegMoney());
        mm.setPhoneNumber(m.getPhoneNumber());
        mm.setRecommendGoods(m.getRecommendGoods());
        mm.setLocalPhoto(m.getLocalPhoto());
        mm.setLicensePic(m.getLicensePic());
        mm.setLegalPhoto(m.getLegalPhoto());
        mm.setNormalCard(m.getNormalCard());
        mm.setCreditCard(m.getCreditCard());
        mm.setShareFee(m.getShareFee());
        if (minmoney.equals("0"))  {
            mm.setSettlementLoop(m.getSettlementLoop());
            mm.setMinmoney(new BigDecimal("0"));
            mm.setLatestday("");
        } else if (!mm.getSettlementLoop().equals("8")) {
            mm.setSettlementLoop("8");
            mm.setMinmoney(new BigDecimal(minmoney));
            mm.setLatestday(latestday);
        } else if(!m.getSettlementLoop().equals("0")){
            mm.setSettlementLoop(m.getSettlementLoop());
            mm.setMinmoney(new BigDecimal("0"));
            mm.setLatestday("");
        }else {
            mm.setSettlementLoop("8");
            mm.setMinmoney(new BigDecimal(minmoney));
            mm.setLatestday(latestday);
        }

        imMerchantOtherInfoService.updateById(mm);
        imMerchantInfoService.updateById(minfo);

    }

}
