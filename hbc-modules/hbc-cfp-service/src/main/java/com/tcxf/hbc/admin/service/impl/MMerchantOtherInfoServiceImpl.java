package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.admin.mapper.MMerchantOtherInfoMapper;
import com.tcxf.hbc.admin.service.IMMerchantOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户其他信息 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MMerchantOtherInfoServiceImpl extends ServiceImpl<MMerchantOtherInfoMapper, MMerchantOtherInfo> implements IMMerchantOtherInfoService {

}
