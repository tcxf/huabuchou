package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.admin.mapper.MRedPacketMapper;
import com.tcxf.hbc.admin.service.IMRedPacketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户领取优惠券记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MRedPacketServiceImpl extends ServiceImpl<MRedPacketMapper, MRedPacket> implements IMRedPacketService {

}
