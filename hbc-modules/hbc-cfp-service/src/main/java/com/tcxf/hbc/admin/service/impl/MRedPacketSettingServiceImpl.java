package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.OredpacketDto;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.admin.mapper.MRedPacketSettingMapper;
import com.tcxf.hbc.admin.service.IMRedPacketSettingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优惠券表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MRedPacketSettingServiceImpl extends ServiceImpl<MRedPacketSettingMapper, MRedPacketSetting> implements IMRedPacketSettingService {

    @Autowired
    private MRedPacketSettingMapper mRedPacketSettingMapper;



    @Override
    public Page findredlist(Map<String, Object> map) {
        Query<OredpacketDto> query = new Query(map);
        List<OredpacketDto> list = mRedPacketSettingMapper.findredlistQuery(query,query.getCondition());
        query.setRecords(list);
        return query;
    }
}
