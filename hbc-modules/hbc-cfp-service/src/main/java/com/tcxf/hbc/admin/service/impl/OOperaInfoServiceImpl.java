package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.JavaType;
import com.tcxf.hbc.admin.controller.admin.ThreelementController;
import com.tcxf.hbc.admin.mapper.OOperaInfoMapper;
import com.tcxf.hbc.admin.mapper.OOperaOtherInfoMapper;
import com.tcxf.hbc.admin.mapper.TbPeopleRoleMapper;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.admin.model.vo.OOperainfo;
import com.tcxf.hbc.admin.service.*;
import com.tcxf.hbc.common.bean.config.OsnPropertiesConfig;
import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonUtils;
import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.CreditVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 * 运营商信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class OOperaInfoServiceImpl extends ServiceImpl<OOperaInfoMapper, OOperaInfo> implements IOOperaInfoService {

    @Autowired
    private OOperaInfoMapper oOperaInfoMapper;

    @Autowired
    private OOperaOtherInfoMapper oOperaOtherInfoMapper;

    @Autowired
    private TbPeopleRoleMapper tbPeopleRoleMapper;

    @Autowired
    private ThreelementController threelementController;

    @Autowired
    private IOOperaInfoService operaInfo;

    @Autowired
    private IOOperaOtherInfoService operaOtherInfo;

    @Autowired
    private ITbBindCardInfoService bBindCardInfo;

    @Autowired
    private ITbWalletService iTbWalletService;

    @Autowired
    private ICOperaSwitchService icOperaSwitchService;

    @Autowired
    OsnPropertiesConfig osnPropertiesConfig;

    //密码加密
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 查询今日新增商户
     * @param start 开始时间 当天的00:00:00
     * @param end 结束时间  第二天的00:00:00
     * @return
     */
    @Override
    public int CreateQueryAllDate(String start, String end) {
        return oOperaInfoMapper.CreateQueryAllDate(start, end);
    }


    /**
     * 查询所有可用商户
     * @return
     */
    @Override
    public int CountQueryOperator() {
        return oOperaInfoMapper.CountQueryOperator();
    }

    /**
     * 分页查询
     * @param status
     * @return
     */
    @Override
    public Page queryList2(String status) {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query<MerchantSerlect> query = new  Query(params);
        return  this.QueryAll(query);
    }

    /**
     * 分页查询所有用户
     * @param query
     * @return
     */
    @Override
    public Page QueryAll(Query<MerchantSerlect> query) {
        List<MerchantSerlect> list=oOperaInfoMapper.QueryAll(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    /**
     * 没什么叼用
     * @param params
     * @return
     */
    @Override
    public Page selectOperatorPage(Map<String, Object> params) {
        params.put("status",CommonConstant.STATUS_NORMAL);
        // return this.QueryAll(new Query<MerchantSerlect>(params),new EntityWrapper<MerchantSerlect>());
        return  null;
    }

    /**
     * 根据资金端id查询运营商信息
     * @param map
     * @return
     */
    @Override
    public Page getOperaInfoByFid(Map<String, Object> map,String id) {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query query = new  Query(params);
        List<OOperaInfo> list= oOperaInfoMapper.selectOperaInfoByFid(query,map,id);
        query.setRecords(list);
        return query;
    }

    /**
     * 根据Id查询商户的详情
     * @param id
     * @return
     */
    @Override
    public MerchantSerlect QueeryAllOperaInfoDetail(String id) {
        return oOperaInfoMapper.QueeryAllOperaInfoDetail(id);
    }

    @Override
    public Page QueryBanks(Query<MerchantSerlect> query) {
        List<MerchantSerlect> list=oOperaInfoMapper.QueryBanks(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    /**
     * 根据资金端查询已绑定和未绑定运营商
     * @param params
     * @return
     */
    @Override
    public Page selectOperaByFundend(Map<String, Object> params) {
        Query query = new Query(params);
        List<OOperaInfo> list= oOperaInfoMapper.selectOperaByFundend(query,params);
        return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:24 2018/7/2
     *查询资金端下面的运营商
    */
    @Override
    public List<OOperaInfo> selectOperaByfid(Map<String, Object> paramMap) {
        return oOperaInfoMapper.selectOperaByfid(paramMap);
    }

    @Override
    @Transactional
    public void insertOperainfo(OOperainfo oop,HttpServletRequest request) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
        // MD5Util md5Util = new MD5Util();
        OOperaOtherInfo other = new OOperaOtherInfo();
        OOperaInfo oper = new OOperaInfo();
        TbPeopleRole role=new TbPeopleRole();
        //添加运行商基本信息
        //二级域名
        oper.setOsn(oop.getOsn()+"."+osnPropertiesConfig.getOsn_url());
        List <OOperaInfo> mobile = oOperaInfoMapper.selectList(new EntityWrapper <OOperaInfo>().ne("id", oop.getId()));
        //校验手机号码
        for (OOperaInfo opp:mobile) {
            if(oop.getYmobile().equals(opp.getMobile())){
                throw new CheckedException("手机号码已被注册！");
            }
        }

        CreditVo creditCto1 = new CreditVo();
        creditCto1.setMobile(oop.getYmobile());
        creditCto1.setIdCard(oop.getIdCard());
        creditCto1.setUserName(oop.getLegalName());
        R r2 = threelementController.operatorsCheck(creditCto1);
        if (r2.getCode() == 1){
            throw new CheckedException("请输入正确的法人信息！");
        }

        oper.setMobile(oop.getYmobile());
        oper.setAcount(oop.getYmobile());
        //oper.setPwd(md5Util.MD5Encode(oop.getYmobile().substring(5,11),"utf-8"));
        oper.setPwd(ENCODER.encode(oop.getYmobile().substring(5,11)));
        oper.setName(oop.getYname());
        oper.setSimpleName(oop.getSimpleName());
        oper.setLegalName(oop.getLegalName());
        oper.setIdCard(oop.getIdCard());
        oper.setAddress(oop.getAddress());
        oper.setStatus(oop.getStatus());
        oper.setArea(oop.getArea());
        oper.setAppid(oop.getAppid());
        oper.setAppsecret(oop.getAppsecret());
        oper.setSettlementStatus(oop.getSettlementStatus());
        oper.setCreateDate(new Date());

        oOperaInfoMapper.insert(oper);

        COperaSwitch cOperaSwitch = new COperaSwitch();
        cOperaSwitch.setOid(oper.getId());
        cOperaSwitch.setPhoneState(COperaSwitch.ipState_YES);
        cOperaSwitch.setIdcardState(COperaSwitch.idcard_state_YES);
        cOperaSwitch.setIpState(COperaSwitch.ipState_YES);
        icOperaSwitchService.insert(cOperaSwitch);

        //添加运行商其他信息
        other.setOid(oper.getId());
        other.setOperYear(oop.getOperYear());
        other.setOperArea(oop.getOperArea());
        other.setLegalPhoto(oop.getLegalPhoto());
        other.setLocalPhoto(oop.getLocalPhoto());
        other.setLicensePic(oop.getLicensePic());
        other.setLagalFacePhoto(oop.getLagalFacePhoto());
        other.setCreditCard(oop.getCreditCard());
        other.setNormalCard(oop.getNormalCard());
        other.setRegMoney(oop.getRegMoney());
        other.setNature(oop.getNature());
        other.setScorePercent(oop.getScorePercent());
//        other.setMessageFee(oop.getMessageFee());
        other.setCreateDate(new Date());
        other.setPlatfromShareRatio(oop.getPlatfromShareRatio());
        //费用及结算信息
        other.setScorePercent(oop.getScorePercent());
        other.setMessageFee(oop.getMessageFee());
        other.setSettlementLoop(oop.getSettlementLoop());
        other.setOoaDate("05");
        other.setRepayDate("21");
        oOperaOtherInfoMapper.insert(other);

        //运营商默认权限
        role.setPeopleUserType("2");
        role.setPeopleUserId(oper.getId());
        role.setRoleId("2");
        role.setCreateDate(new Date());
        tbPeopleRoleMapper.insert(role);

        //绑定银行卡信息
        // bank.setId();
        //添加卡信息
        boolean oid =  bBindCardInfo.delete(new EntityWrapper <TbBindCardInfo>().eq("oid",oper.getId()));
        if(oid) {
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbBindCardInfo.class);
            List <TbBindCardInfo> list = (List <TbBindCardInfo>) JsonUtils.fromJson(MapUtil.getStringForMapObj(paramMap, "bind_card", true), javaType);
            if(list.size()==0){
                throw new CheckedException("请输入绑卡信息！");
            }
            for (TbBindCardInfo TbBindCardInfo : list) {
                /**
                 * 银行卡四要素验证
                 */
                CreditVo creditVo = new CreditVo();
                creditVo.setMobile(TbBindCardInfo.getMobile());
                creditVo.setAccountNO(TbBindCardInfo.getBankCard());
                creditVo.setIdCard(TbBindCardInfo.getIdCard());
                creditVo.setUserName(TbBindCardInfo.getName());
                R r1 = threelementController.threelementCheck(creditVo);
                if (r1.getCode() == 1){
                    throw new CheckedException("请输入正确的绑卡信息！");
                }
                if(TbBindCardInfo.getBankSn().equals("GDB")){
                    TbBindCardInfo.setBankName("广发银行");
                }
                if(TbBindCardInfo.getBankSn().equals("HXB")){
                    TbBindCardInfo.setBankName("华夏银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CNCB")){
                    TbBindCardInfo.setBankName("中信银行");
                }
                if(TbBindCardInfo.getBankSn().equals("ICBC")){
                    TbBindCardInfo.setBankName("中国工商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOC")){
                    TbBindCardInfo.setBankName("中国银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOS")){
                    TbBindCardInfo.setBankName("上海银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BCCB")){
                    TbBindCardInfo.setBankName("北京银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CCB")){
                    TbBindCardInfo.setBankName("中国建设银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CIB")){
                    TbBindCardInfo.setBankName("兴业银行");
                }
                if(TbBindCardInfo.getBankSn().equals("GZCB")){
                    TbBindCardInfo.setBankName("广州银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CMBC")){
                    TbBindCardInfo.setBankName("中国民生银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BTCB")){
                    TbBindCardInfo.setBankName("包商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("PSBC")){
                    TbBindCardInfo.setBankName("中国邮政储蓄银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CCCB")){
                    TbBindCardInfo.setBankName("长沙银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOCOM")){
                    TbBindCardInfo.setBankName("交通银行");
                }
                if(TbBindCardInfo.getBankSn().equals("PAB")){
                    TbBindCardInfo.setBankName("长沙银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CMB")){
                    TbBindCardInfo.setBankName("招商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("ABC")){
                    TbBindCardInfo.setBankName("中国农业银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CEB")){
                    TbBindCardInfo.setBankName("中国光大银行");
                }
                if(TbBindCardInfo.getBankSn().equals("JXCCB")){
                    TbBindCardInfo.setBankName("嘉兴银行");
                }
                if(TbBindCardInfo.getBankSn().equals("SPDB")){
                    TbBindCardInfo.setBankName("浦发银行");
                }
                if(TbBindCardInfo.getBankSn().equals("HNRCC")){
                    TbBindCardInfo.setBankName("湖南省农村信用社");
                }
                TbBindCardInfo.setOid(oper.getId());
                TbBindCardInfo.setUtype("3");
                TbBindCardInfo.setCardType("0");
                TbBindCardInfo.setCreateDate(new Date());
                bBindCardInfo.insert(TbBindCardInfo);
            }
        }
        //绑定钱包
        /**
         * 添加钱包
         * * 参数：ID 用户的ID
         * type 用户的类型1：用户，2：商户，3：运营商，4：资金端,
         * name 当前开通钱包用户的名字(用户类型是普通用户名字就是自己的名字。用户类型是商户称or运营商or资金端就是自己的姓名)
         * wname 钱包的名字(用户姓名or商户名称or运营商名称or资金端名称)
         * mobil 用户手机
         * String id,int type,String name,String mobil,String wname
         */
        iTbWalletService.savewallet(oper.getId(), 3, oop.getLegalName(), oop.getYmobile(), oop.getYname());
    }

    @Override
    @Transactional
    public void updateOperainfo(OOperainfo oop, HttpServletRequest request) {
        Map<String, Object> paramMap = MapUtil.getMapForRequest(request);
//        boolean bo = operaInfo.updateById(oop);
        OOperaInfo oper = operaInfo.selectOne(new EntityWrapper <OOperaInfo>().eq("id", oop.getId()));
        OOperaOtherInfo other = operaOtherInfo.selectOne(new EntityWrapper <OOperaOtherInfo>().eq("oid", oop.getId()));
        TbBindCardInfo bank = bBindCardInfo.selectOne(new EntityWrapper <TbBindCardInfo>().eq("oid", oop.getId()));

        CreditVo creditCto1 = new CreditVo();
        creditCto1.setMobile(oop.getYmobile());
        creditCto1.setIdCard(oop.getIdCard());
        creditCto1.setUserName(oop.getLegalName());
        R r2 = threelementController.operatorsCheck(creditCto1);
        if (r2.getCode() == 1){
            throw new CheckedException("请输入正确的法人信息！");
        }

        oper.setMobile(oop.getYmobile());
        oper.setName(oop.getYname());
        oper.setSimpleName(oop.getSimpleName());
        oper.setLegalName(oop.getLegalName());
        oper.setIdCard(oop.getIdCard());
        oper.setAddress(oop.getAddress());
        oper.setArea(oop.getArea());
        oper.setStatus(oop.getStatus());
        oper.setAppid(oop.getAppid());
        oper.setAppsecret(oop.getAppsecret());
        oper.setSettlementStatus(oop.getSettlementStatus());
        oper.setModifyDate(new Date());
        //添加运行商其他信息
        // other.setId(IdWorker.getIdStr());
        other.setOperYear(oop.getOperYear());
        other.setOperArea(oop.getOperArea());
        other.setLegalPhoto(oop.getLegalPhoto());
        other.setLocalPhoto(oop.getLocalPhoto());
        other.setLicensePic(oop.getLicensePic());
        other.setLagalFacePhoto(oop.getLagalFacePhoto());
        other.setCreditCard(oop.getCreditCard());
        other.setNormalCard(oop.getNormalCard());
        other.setRegMoney(oop.getRegMoney());
        other.setNature(oop.getNature());
        other.setSettlementLoop(oop.getSettlementLoop());
        other.setScorePercent(oop.getScorePercent());
//        other.setMessageFee(oop.getMessageFee());
        other.setPlatfromShareRatio(oop.getPlatfromShareRatio());
       other.setModifyDate(new Date());
        //费用及结算信息
        other.setScorePercent(oop.getScorePercent());
        other.setMessageFee(oop.getMessageFee());
        //绑定银行卡信息
        // bank.setId();
        //添加卡信息
        boolean oid =  bBindCardInfo.delete(new EntityWrapper <TbBindCardInfo>().eq("oid",oop.getId()));
        if(oid) {
            JavaType javaType = JsonUtils.createCollectionType(ArrayList.class, TbBindCardInfo.class);
            List <TbBindCardInfo> list = (List <TbBindCardInfo>) JsonUtils.fromJson(MapUtil.getStringForMapObj(paramMap, "bind_card", true), javaType);
            if(list.size()==0){
                throw new CheckedException("请输入绑卡信息！");
            }
            for (TbBindCardInfo TbBindCardInfo : list) {
                /**
                 * 银行卡四要素验证
                 */
                CreditVo creditVo = new CreditVo();
                creditVo.setMobile(TbBindCardInfo.getMobile());
                creditVo.setAccountNO(TbBindCardInfo.getBankCard());
                creditVo.setIdCard(TbBindCardInfo.getIdCard());
                creditVo.setUserName(TbBindCardInfo.getName());
                R r1 = threelementController.operatorsCheck(creditVo);
                if (r1.getCode() == 1){
                    throw new CheckedException("请输入正确的绑卡信息！");
                }

                if(TbBindCardInfo.getBankSn().equals("GDB")){
                    TbBindCardInfo.setBankName("广发银行");
                }
                if(TbBindCardInfo.getBankSn().equals("HXB")){
                    TbBindCardInfo.setBankName("华夏银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CNCB")){
                    TbBindCardInfo.setBankName("中信银行");
                }
                if(TbBindCardInfo.getBankSn().equals("ICBC")){
                    TbBindCardInfo.setBankName("中国工商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOC")){
                    TbBindCardInfo.setBankName("中国银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOS")){
                    TbBindCardInfo.setBankName("上海银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BCCB")){
                    TbBindCardInfo.setBankName("北京银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CCB")){
                    TbBindCardInfo.setBankName("中国建设银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CIB")){
                    TbBindCardInfo.setBankName("兴业银行");
                }
                if(TbBindCardInfo.getBankSn().equals("GZCB")){
                    TbBindCardInfo.setBankName("广州银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CMBC")){
                    TbBindCardInfo.setBankName("中国民生银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BTCB")){
                    TbBindCardInfo.setBankName("包商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("PSBC")){
                    TbBindCardInfo.setBankName("中国邮政储蓄银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CCCB")){
                    TbBindCardInfo.setBankName("长沙银行");
                }
                if(TbBindCardInfo.getBankSn().equals("BOCOM")){
                    TbBindCardInfo.setBankName("交通银行");
                }
                if(TbBindCardInfo.getBankSn().equals("PAB")){
                    TbBindCardInfo.setBankName("长沙银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CMB")){
                    TbBindCardInfo.setBankName("招商银行");
                }
                if(TbBindCardInfo.getBankSn().equals("ABC")){
                    TbBindCardInfo.setBankName("中国农业银行");
                }
                if(TbBindCardInfo.getBankSn().equals("CEB")){
                    TbBindCardInfo.setBankName("中国光大银行");
                }
                if(TbBindCardInfo.getBankSn().equals("JXCCB")){
                    TbBindCardInfo.setBankName("嘉兴银行");
                }
                if(TbBindCardInfo.getBankSn().equals("SPDB")){
                    TbBindCardInfo.setBankName("浦发银行");
                }
                if(TbBindCardInfo.getBankSn().equals("HNRCC")){
                    TbBindCardInfo.setBankName("湖南省农村信用社");
                }
                TbBindCardInfo.setOid(oop.getId());
                TbBindCardInfo.setUtype("3");
                TbBindCardInfo.setCardType("0");
                TbBindCardInfo.setModifyDate(new Date());
                bBindCardInfo.insert(TbBindCardInfo);
            }
        }

        //修改绑定钱包
        /**
         * 添加钱包
         * * 参数：ID 用户的ID
         * type 用户的类型1：用户，2：商户，3：运营商，4：资金端,
         * name 当前开通钱包用户的名字(用户类型是普通用户名字就是自己的名字。用户类型是商户称or运营商or资金端就是自己的姓名)
         * wname 钱包的名字(用户姓名or商户名称or运营商名称or资金端名称)
         * mobil 用户手机
         * String id,int type,String name,String mobil,String wname
         */
        TbWallet uid = iTbWalletService.selectOne(new EntityWrapper <TbWallet>().eq("uid", oop.getId()));
        uid.setUname( oop.getLegalName());
        uid.setWname(oop.getYname());
        uid.setUmobil(oop.getYmobile());
        uid.setModifyDate(new Date());
        iTbWalletService.updateById(uid);
        operaInfo.updateById(oper);
        operaOtherInfo.updateById(other);
    }


}
