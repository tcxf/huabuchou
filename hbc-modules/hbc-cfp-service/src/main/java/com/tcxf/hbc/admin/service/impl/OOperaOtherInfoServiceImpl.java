package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.OOperaOtherInfo;
import com.tcxf.hbc.admin.mapper.OOperaOtherInfoMapper;
import com.tcxf.hbc.admin.service.IOOperaOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商其他信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class OOperaOtherInfoServiceImpl extends ServiceImpl<OOperaOtherInfoMapper, OOperaOtherInfo> implements IOOperaOtherInfoService {


    @Override
    public int CreateQueryAllDat(String strat, String end) {
        return 0;
    }
}
