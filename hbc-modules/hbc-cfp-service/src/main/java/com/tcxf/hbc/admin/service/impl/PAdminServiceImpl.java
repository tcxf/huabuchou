package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.model.dto.roleDto;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.admin.mapper.PAdminMapper;
import com.tcxf.hbc.admin.service.IPAdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import com.baomidou.mybatisplus.plugins.Page;

import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台用户信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class PAdminServiceImpl extends ServiceImpl<PAdminMapper, PAdmin> implements IPAdminService{

    @Autowired
    private PAdminMapper pAdminMapper;

    @Override
    public PAdmin login(PAdmin pAdmin) {
        return  pAdminMapper.selectOne(pAdmin);
    }

    @Override
    public Page findAdminCondition(Map<String,Object> map) {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        params.put("page",map.get("page"));
        Query<PAdmin> query = new  Query(params);
        String name = null;
        Map<String,Object> map1 = new HashMap<>();
        map1.put("name",map.get("name"));
        map1.put("aid",map.get("aid"));
        List<PAdmin> admin= pAdminMapper.findByLike(query,map1);
        query.setRecords(admin);
        return query;
    }
}
