package com.tcxf.hbc.admin.impl;

import com.tcxf.hbc.common.entity.PNotice;
import com.tcxf.hbc.admin.mapper.PNoticeMapper;
import com.tcxf.hbc.admin.service.IPNoticeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
@Service
public class PNoticeServiceImpl extends ServiceImpl<PNoticeMapper, PNotice> implements IPNoticeService {

}
