package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.RefusingCreditMapper;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.admin.service.RefusingCreditService;
import com.tcxf.hbc.admin.service.RefusingVersionService;
import com.tcxf.hbc.common.entity.CreditScore;
import com.tcxf.hbc.common.entity.RefusingCredit;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.JsonUtils;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拒绝授信条件service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class RefusingCreditServiceImpl extends ServiceImpl<RefusingCreditMapper, RefusingCredit>  implements RefusingCreditService {

    @Autowired
    private RefusingVersionService refusingVersionService;
    @Autowired
    private RefusingCreditMapper refusingCreditMapper;
    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;
    /**
     * 创建拒绝授信条件
     * @param list
     * @param fid
     */
    @Override
    @Transactional
    public void createRefusingCredit(List<RefusingCredit> list,String fid) {
        //1、数据验证
        validatecreateRefusingCredit(list,fid);
        //3、创建新版本
        String refusingVersionId = insertRefusingVersion(fid);
        //4、创建新的拒绝授信条件
        for(RefusingCredit r : list){
            r.setVersionId(refusingVersionId);
            r.setCreateDate(new Date());
        }
        insertBatch(list);

        //修改大版本关联小版本id
        updateRmSxtemplateRelationshipById(list, refusingVersionId);
    }

    /**
     * @Description: 修改大版本关联小版本id
     * @Param: [list, refusingVersionId]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/10/24
     */
    public void updateRmSxtemplateRelationshipById(List<RefusingCredit> list, String refusingVersionId){

        RmSxtemplateRelationship rmSxtemplateRelationship = new RmSxtemplateRelationship();
        rmSxtemplateRelationship.setId(list.get(0).getRelationshipId());
        rmSxtemplateRelationship.setSxRefusingId(refusingVersionId);

        RmSxtemplateRelationship rsrelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", list.get(0).getRelationshipId()));
        String updateModule = rsrelationship.getUpdateModule();
        if (!updateModule.contains("拒绝授信")){
            updateModule = updateModule + "拒绝授信,";
        }
        rmSxtemplateRelationship.setUpdateModule(updateModule);

        iRmSxtemplateRelationshipService.updateById(rmSxtemplateRelationship);
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRefusingVersion(String fid){
        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_1);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_1));
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }

    /**
     * 数据验证
     */
    private void validatecreateRefusingCredit(List<RefusingCredit> list,String fid){
        for(RefusingCredit r : list){
            if(ValidateUtil.isEmpty(r.getNumber())){
                throw new CheckedException(r.getName()+"的条件个数不能为空");
            }
        }

        Map<String, Object> tmap = new HashMap<>();
        tmap.put("fid", fid);
        tmap.put("id", list.get(0).getRelationshipId());
        List<RefusingCredit> selectList = selectRefusingCreditByfId(tmap);
        if(selectList.size()!=list.size()){
            return;
        }
        //分组
        Map<String ,RefusingCredit> map = new HashMap<String,RefusingCredit>();
        for(RefusingCredit r : list){
            map.put(r.getCode(),r);
        }
        for(RefusingCredit r : selectList) {
            if(ValidateUtil.isEmpty(map.get(r.getCode()))){
                return;
            }
            if(r.getNumber()!=map.get(r.getCode()).getNumber()||
                    !r.getTwoapplications().equals(map.get(r.getCode()).getTwoapplications()) || !r.getStatus().equals(map.get(r.getCode()).getStatus())){
                return;
            }
        }
        throw new CheckedException("未修改条件，无需更新");
    }

    /**
     * 根据资金端ID查询最新版本的条件
     * @return
     */
    @Override
    public List<RefusingCredit> selectRefusingCreditByfId(Map<String, Object> map){
        List<RefusingCredit> list = refusingCreditMapper.selectRefusingCreditByfId(map);
        return list;
    }


    /**
     * 根据拒绝授信条件ID查询
     * @return
     */
    @Override
    public RefusingCredit selectRefusingCreditById(String id){
        return null;
    }


}
