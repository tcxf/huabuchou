package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.RmBaseCreditVersionMapper;
import com.tcxf.hbc.admin.service.RefusingVersionService;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拒绝授信条件service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class RefusingVersionServiceImpl extends ServiceImpl<RmBaseCreditVersionMapper, RefusingVersion>  implements RefusingVersionService {

    @Autowired
    RmBaseCreditVersionMapper rmBaseCreditVersionMapper;

    @Override
    public String createVersion(String fid,String type) {
        Map<String, Object> map = new HashMap<>();
        map.put("fid",fid);
        map.put("type",type);

        String version = rmBaseCreditVersionMapper.getRefusingVersionByFid(map);

        if(ValidateUtil.isEmpty(version)){
            return DateUtil.format(new Date(),DateUtil.FORMAT_YYYYMMDD)+"001";
        }
        Long v = Long.parseLong(version) + 1;
        return v.toString();
    }
}
