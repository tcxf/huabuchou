package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.model.dto.RmBaseCreditInitialDto;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.admin.service.RefusingVersionService;
import com.tcxf.hbc.common.entity.RefusingCredit;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.admin.mapper.RmBaseCreditInitialMapper;
import com.tcxf.hbc.admin.service.IRmBaseCreditInitialService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 初始授信条件模板表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
@Service
public class RmBaseCreditInitialServiceImpl extends ServiceImpl<RmBaseCreditInitialMapper, RmBaseCreditInitial> implements IRmBaseCreditInitialService {

    @Autowired
    RmBaseCreditInitialMapper rmBaseCreditInitialMapper;

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;

    @Override
    public List<RmBaseCreditInitial> getRmBaseCreditInitialList(RmBaseCreditInitial rmBaseCreditInitial) {
        return rmBaseCreditInitialMapper.getListByType(rmBaseCreditInitial);
    }


    /**
     * 创建初始授信条件
     * @param list
     * @param fid
     */
    @Override
    @Transactional
    public void createRmBaseCreditInitial(List<RmBaseCreditInitial> list, String fid) {
        //1、数据验证
        validatecreateRefusingCredit(list,fid);
        //3、创建新版本
        String refusingVersionId = insertRmBaseCreditInitial(fid);
        //4、创建新的拒绝授信条件
        for(RmBaseCreditInitial r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);

        //修改大版本关联小版本id
        updateRmSxtemplateRelationshipById(list, refusingVersionId);

    }

    /**
     * @Description: 修改大版本关联小版本id
     * @Param: [list, refusingVersionId]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/10/24
    */
    public void updateRmSxtemplateRelationshipById(List<RmBaseCreditInitial> list, String refusingVersionId){

        RmSxtemplateRelationship rmSxtemplateRelationship = new RmSxtemplateRelationship();
        rmSxtemplateRelationship.setId(list.get(0).getRelationshipId());
        rmSxtemplateRelationship.setSxInitId(refusingVersionId);

        RmSxtemplateRelationship rsrelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", list.get(0).getRelationshipId()));
        String updateModule = rsrelationship.getUpdateModule();
        if (!updateModule.contains("初始授信")){
            updateModule = updateModule + "初始授信,";
        }
        rmSxtemplateRelationship.setUpdateModule(updateModule);

        iRmSxtemplateRelationshipService.updateById(rmSxtemplateRelationship);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:12 2018/9/27
     * 根据版本号以及typeId查询数据
    */
    @Override
    public List<RmBaseCreditInitialDto> findBaseCreditInitialInfo(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findBaseCreditInitialInfo(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:57 2018/9/27
     * 根据detial查询数量
    */
    @Override
    public int findNumberOverDueByDetail(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findNumberOverDueByDetail(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:57 2018/9/27
     * 根据区间以及code查询数量
    */
    @Override
    public int findNumberOverDueByInterval(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findNumberOverDueByInterval(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:36 2018/9/28
     * 根据detial查询逾期的金额
    */
    @Override
    public BigDecimal findNumberOverDueMoneyByDetail(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findNumberOverDueMoneyByDetail(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:44 2018/9/28
     * 根据区间以及code查询逾期的金额
    */
    @Override
    public BigDecimal findNumberOverDueMoneyByInterval(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findNumberOverDueMoneyByInterval(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:28 2018/9/29
     * 每个评分段授信人数统计
    */
    @Override
    public int findpeopleNumByGradingSegment(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findpeopleNumByGradingSegment(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:57 2018/9/29
     * 每个评分段违约授信人数统计
    */
    @Override
    public int findoverDueNumByGradingSegment(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findoverDueNumByGradingSegment(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:04 2018/9/29
     * 每个评分段 违约金额统计
    */
    @Override
    public BigDecimal findoverDueMoneyByGradingSegment(HashMap<String, Object> paramMap) {
        return rmBaseCreditInitialMapper.findoverDueMoneyByGradingSegment(paramMap);
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRmBaseCreditInitial(String fid){

        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_2);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_2));
        //新增新版本
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }


    /**
     * 数据验证
     */
    private void validatecreateRefusingCredit(List<RmBaseCreditInitial> list, String fid){

        //当前版本
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_YES).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).eq("fid", fid));

        //查询初始化授信记录列表
        List<RmBaseCreditInitial> rbcilist = this.selectList(new EntityWrapper<RmBaseCreditInitial>().eq("versionId", dqlist.getSxInitId()));


        //分组
        Map<String ,RmBaseCreditInitial> map = new HashMap<>();
        for(RmBaseCreditInitial r : list){
            map.put(r.getCode(),r);
        }

        for(RmBaseCreditInitial r : rbcilist) {
            if (ValidateUtil.isEmpty(r.getMinScore()) && ValidateUtil.isEmpty(r.getMaxScore())){
                if(!r.getValue().equals(map.get(r.getCode()).getValue()) || !r.getStatus().equals(map.get(r.getCode()).getStatus())){
                    return;
                }
            }else if (!ValidateUtil.isEmpty(r.getMinScore()) && ValidateUtil.isEmpty(r.getMaxScore())){
                if(!r.getMinScore().equals(map.get(r.getCode()).getMinScore()) || !r.getValue().equals(map.get(r.getCode()).getValue()) || !r.getStatus().equals(map.get(r.getCode()).getStatus())){
                    return;
                }
            }else {
                if(!r.getMinScore().equals(map.get(r.getCode()).getMinScore()) || !r.getMaxScore().equals(map.get(r.getCode()).getMaxScore())
                        || !r.getValue().equals(map.get(r.getCode()).getValue()) || !r.getStatus().equals(map.get(r.getCode()).getStatus())){
                    return;
                }
            }
        }
        throw new CheckedException("未修改条件，无需更新");
    }

}
