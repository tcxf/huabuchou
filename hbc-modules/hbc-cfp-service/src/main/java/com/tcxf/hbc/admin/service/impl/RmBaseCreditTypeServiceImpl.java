package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.RmBaseCreditType;
import com.tcxf.hbc.admin.mapper.RmBaseCreditTypeMapper;
import com.tcxf.hbc.admin.service.IRmBaseCreditTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信条件类别表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
@Service
public class RmBaseCreditTypeServiceImpl extends ServiceImpl<RmBaseCreditTypeMapper, RmBaseCreditType> implements IRmBaseCreditTypeService {

}
