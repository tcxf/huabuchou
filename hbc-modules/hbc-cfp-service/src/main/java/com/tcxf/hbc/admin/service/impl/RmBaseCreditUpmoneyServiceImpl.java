package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.admin.service.RefusingVersionService;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;
import com.tcxf.hbc.admin.mapper.RmBaseCreditUpmoneyMapper;
import com.tcxf.hbc.admin.service.IRmBaseCreditUpmoneyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 提额授信条件模板表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
@Service
public class RmBaseCreditUpmoneyServiceImpl extends ServiceImpl<RmBaseCreditUpmoneyMapper, RmBaseCreditUpmoney> implements IRmBaseCreditUpmoneyService {

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IRmSxtemplateRelationshipService iRmSxtemplateRelationshipService;

    /**
     * 创建初始授信条件
     * @param list
     * @param fid
     */
    @Override
//    @Transactional
    public void createRmBaseCreditUpmoney(List<RmBaseCreditUpmoney> list, String fid) {
        //1、数据验证
        validatecreateRefusingCredit(list,fid);
        //3、创建新版本
        String refusingVersionId = insertRmBaseCreditUpmoney(fid);
        //4、创建新的拒绝授信条件
        for(RmBaseCreditUpmoney r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);

        //修改大版本关联小版本id
        updateRmSxtemplateRelationshipById(list, refusingVersionId);

    }

    /**
     * @Description: 修改大版本关联小版本id
     * @Param: [list, refusingVersionId]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/10/24
     */
    public void updateRmSxtemplateRelationshipById(List<RmBaseCreditUpmoney> list, String refusingVersionId){

        RmSxtemplateRelationship rmSxtemplateRelationship = new RmSxtemplateRelationship();
        rmSxtemplateRelationship.setId(list.get(0).getRelationshipId());
        rmSxtemplateRelationship.setSxUpmoneyId(refusingVersionId);

        RmSxtemplateRelationship rsrelationship = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("id", list.get(0).getRelationshipId()));
        String updateModule = rsrelationship.getUpdateModule();
        if (!updateModule.contains("提额授信")){
            updateModule = updateModule + "提额授信,";
        }
        rmSxtemplateRelationship.setUpdateModule(updateModule);

        iRmSxtemplateRelationshipService.updateById(rmSxtemplateRelationship);
    }

    /**
     * 数据验证
     */
    private void validatecreateRefusingCredit(List<RmBaseCreditUpmoney> list, String fid){

        //当前版本
        RmSxtemplateRelationship dqlist = iRmSxtemplateRelationshipService.selectOne(new EntityWrapper<RmSxtemplateRelationship>().eq("status", RmSxtemplateRelationship.STATUS_YES).eq("is_publish", RmSxtemplateRelationship.ISPUBLISH_YES).eq("fid", fid));

        //查询初始化授信记录列表
        List<RmBaseCreditUpmoney> rbcilist = this.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("versionId", dqlist.getSxUpmoneyId()));


        //分组
        Map<String ,RmBaseCreditUpmoney> map = new HashMap<>();
        for(RmBaseCreditUpmoney r : list){
            map.put(r.getCode(),r);
        }
        for(RmBaseCreditUpmoney r : rbcilist) {
            if(!r.getValue().equals(map.get(r.getCode()).getValue()) || !r.getStatus().equals(map.get(r.getCode()).getStatus())){
                return;
            }
        }
        throw new CheckedException("未修改条件，无需更新");
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRmBaseCreditUpmoney(String fid){

        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_3);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_3));
        //新增新版本
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }
}
