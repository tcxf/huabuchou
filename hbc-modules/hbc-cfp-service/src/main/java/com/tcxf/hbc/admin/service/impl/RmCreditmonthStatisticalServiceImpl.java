package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.mapper.RmRepaymentSituationMapper;
import com.tcxf.hbc.admin.model.dto.BindFODto;
import com.tcxf.hbc.admin.model.dto.MerchantSerlect;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.tcxf.hbc.admin.mapper.RmCreditmonthStatisticalMapper;
import com.tcxf.hbc.admin.service.IRmCreditmonthStatisticalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RmRepaymentSituation;
import com.tcxf.hbc.common.util.Query;
import jdk.nashorn.internal.runtime.linker.LinkerCallSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信月份统计表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-25
 */
@Service
public class RmCreditmonthStatisticalServiceImpl extends ServiceImpl<RmCreditmonthStatisticalMapper, RmCreditmonthStatistical> implements IRmCreditmonthStatisticalService {
    @Autowired
    private RmCreditmonthStatisticalMapper rmCreditmonthStatisticalMapper;

    @Override
    public Page QueryCredituserstatistics(Query<RmCreditmonthStatistical> query) {
        List<RmCreditmonthStatistical> list=rmCreditmonthStatisticalMapper.QueryCredituserstatistics(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    @Override
    public List <BindFODto> QueryfOs(Map<String, Object> params) {
        return rmCreditmonthStatisticalMapper.QueryfOs(params);
    }

    @Override
    public BindFODto sumzsx() {
        return rmCreditmonthStatisticalMapper.sumzsx();
    }

    @Override
    public BindFODto sumsx() {
        return rmCreditmonthStatisticalMapper.sumsx();
    }

    @Override
    public List <BindFODto> sumzsxed() {
        return rmCreditmonthStatisticalMapper.sumzsxed();
    }

    @Override
    public List <BindFODto> sumsxed() {
        return rmCreditmonthStatisticalMapper.sumsxed();
    }


}
