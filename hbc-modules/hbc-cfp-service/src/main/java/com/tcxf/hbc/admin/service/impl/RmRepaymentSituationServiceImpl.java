package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.tcxf.hbc.common.entity.RmRepaymentSituation;
import com.tcxf.hbc.admin.mapper.RmRepaymentSituationMapper;
import com.tcxf.hbc.admin.service.IRmRepaymentSituationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 还款情况表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-10-10
 */
@Service
public class RmRepaymentSituationServiceImpl extends ServiceImpl<RmRepaymentSituationMapper, RmRepaymentSituation> implements IRmRepaymentSituationService {

    @Autowired
    private RmRepaymentSituationMapper rmRepaymentSituationMapper;
    @Override
    public Page QueryRepaymentsituation(Query<RmRepaymentSituation> query) {
        List<RmRepaymentSituation> list=rmRepaymentSituationMapper.QueryRepaymentsituation(query,query.getCondition());
        query.setRecords(list);
        return query;
    }
}
