package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.admin.mapper.RmSxtemplateRelationshipMapper;
import com.tcxf.hbc.admin.service.IRmSxtemplateRelationshipService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 授信模板关系表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
@Service
public class RmSxtemplateRelationshipServiceImpl extends ServiceImpl<RmSxtemplateRelationshipMapper, RmSxtemplateRelationship> implements IRmSxtemplateRelationshipService {



    @Override
    public String cloneRmSxtemplateRelationship(String id,String fid) {
        //如果有克隆版本先将原来的克隆版本删除
        RmSxtemplateRelationship selectRmSxtemplateRelationship = selectOne(new EntityWrapper<RmSxtemplateRelationship>().
                eq("status",RmSxtemplateRelationship.STATUS_NO).eq("is_publish",RmSxtemplateRelationship.ISPUBLISH_NO).eq("fid",fid));

        if(!ValidateUtil.isEmpty(selectRmSxtemplateRelationship)){
            deleteById(selectRmSxtemplateRelationship);
        }

        RmSxtemplateRelationship cloneRmSxtemplateRelationship = selectById(id);

        //创建新的克隆版本
        cloneRmSxtemplateRelationship.setId(null);
        cloneRmSxtemplateRelationship.setPublishDate(null);
        cloneRmSxtemplateRelationship.setVersion(getVersion(fid));
        cloneRmSxtemplateRelationship.setCreateDate(new Date());
        cloneRmSxtemplateRelationship.setStatus(RmSxtemplateRelationship.STATUS_NO);
        cloneRmSxtemplateRelationship.setIsPublish(RmSxtemplateRelationship.ISPUBLISH_NO);
        insert(cloneRmSxtemplateRelationship);

        return cloneRmSxtemplateRelationship.getId();
    }

    /**
     * 发布
     */
    @Override
    public void updateStatus(RmSxtemplateRelationship rmSxtemplateRelationship){
        //将原来的当前版本改为否
        RmSxtemplateRelationship selectRmSxtemplateRelationship = selectOne(new EntityWrapper<RmSxtemplateRelationship>().
                eq("status",RmSxtemplateRelationship.STATUS_YES).eq("fid",rmSxtemplateRelationship.getFid()));

        selectRmSxtemplateRelationship.setStatus(RmSxtemplateRelationship.STATUS_NO);
        updateById(selectRmSxtemplateRelationship);

        //将状态改为当前版本
        rmSxtemplateRelationship.setStatus(RmSxtemplateRelationship.STATUS_YES);
        //将发布状态改为已发布
        rmSxtemplateRelationship.setIsPublish(RmSxtemplateRelationship.ISPUBLISH_YES);
        //设置发布时间
        rmSxtemplateRelationship.setPublishDate(new Date());

        updateById(rmSxtemplateRelationship);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void deleteRmSxtemplateRelationshipById(String id){
        //执行删除
        deleteById(id);
    }

    /**
     * 获取版本号
     * @return
     */
    private String getVersion(String  fid){
        RmSxtemplateRelationship selectRmSxtemplateRelationship = selectOne(new EntityWrapper<RmSxtemplateRelationship>().
                eq("status",RmSxtemplateRelationship.STATUS_YES).eq("fid",fid));
        if(ValidateUtil.isEmpty(selectRmSxtemplateRelationship)){
            return DateUtil.format(new Date(),DateUtil.FORMAT_YYYYMMDD)+"001";
        }
        Long  v= Long.parseLong(selectRmSxtemplateRelationship.getVersion())+1;
        return v.toString();
    }
}
