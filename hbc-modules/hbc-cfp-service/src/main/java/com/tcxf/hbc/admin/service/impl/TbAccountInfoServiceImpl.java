package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.admin.mapper.TbAccountInfoMapper;
import com.tcxf.hbc.admin.service.ITbAccountInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户授信资金额度表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbAccountInfoServiceImpl extends ServiceImpl<TbAccountInfoMapper, TbAccountInfo> implements ITbAccountInfoService {

}
