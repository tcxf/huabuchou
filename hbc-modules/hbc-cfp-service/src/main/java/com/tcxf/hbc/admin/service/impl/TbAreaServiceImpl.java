package com.tcxf.hbc.admin.service.impl;


import com.tcxf.hbc.admin.model.dto.TbAreaDto;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.admin.mapper.TbAreaMapper;
import com.tcxf.hbc.admin.service.ITbAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 * 地区表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbAreaServiceImpl extends ServiceImpl<TbAreaMapper, TbArea> implements ITbAreaService {

    @Autowired
    private TbAreaMapper tbAreaMapper;
    /**
     * 根据id获取地区表内容
     * @param id
     * @return
     */
    @Override
    public TbArea getById(String id) {
        TbArea tbArea = new TbArea();
        // tbArea.setId(Long.valueOf(id));
        TbArea tbArea1 = tbAreaMapper.selectOne(tbArea);
        return tbArea1;
    }

    @Override
    public List<TbAreaDto> QueryS() {
        return tbAreaMapper.QueryS();
    }

    @Override
    public List<TbAreaDto> QuerySS(String parent_id) {
        return tbAreaMapper.QuerySS(parent_id);
    }

    @Override
    public List<TbAreaDto> QueryQ(String parent_id) {
        return tbAreaMapper.QueryQ(parent_id);
    }
}
