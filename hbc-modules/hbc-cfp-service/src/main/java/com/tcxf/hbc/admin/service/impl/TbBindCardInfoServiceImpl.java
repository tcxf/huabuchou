package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.admin.mapper.TbBindCardInfoMapper;
import com.tcxf.hbc.admin.service.ITbBindCardInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.List;

/**
 * <p>
 * 绑卡信息 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbBindCardInfoServiceImpl extends ServiceImpl<TbBindCardInfoMapper, TbBindCardInfo> implements ITbBindCardInfoService {
    @Autowired
    private TbBindCardInfoMapper bindCardInfo;

    @Override
    public List<TbBindCardInfo> QueryAllBank() {
        return bindCardInfo.QueryAllBank();
    }

    /**
     * 资金端编辑，查看绑卡信息详情
     * liaozeyong
     * @return
     */
    @Override
    public Page selectBindCardList(Map<String, Object> map) {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query query = new  Query(params);
        List<TbBindCardInfo> list = bindCardInfo.selectBindCardList(query,map);
        query.setRecords(list);
        return query;
    }
    /**
     * 根据用户ID查看默认绑卡信息详情
     * liaozeyong
     * @return
     */
    @Override
    public List<TbBindCardInfo> selectBindCard(Map<String, Object> map) {
        return bindCardInfo.selectBindCard(map);
    }

    @Override
    public TbBindCardInfo banko(String oid) {
        return bindCardInfo.banko(oid);
    }

    @Override
    public TbBindCardInfo bankm(String oid) {
        return bindCardInfo.bankm(oid);
    }

}
