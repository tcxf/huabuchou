package com.tcxf.hbc.admin.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.TbBusinessCreditChecklistMapper;
import com.tcxf.hbc.admin.service.ITbBusinessCreditChecklistService;
import com.tcxf.hbc.common.entity.TbBusinessCreditChecklist;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商业授信审核表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
@Service
public class TbBusinessCreditChecklistServiceImpl extends ServiceImpl<TbBusinessCreditChecklistMapper, TbBusinessCreditChecklist> implements ITbBusinessCreditChecklistService {

}
