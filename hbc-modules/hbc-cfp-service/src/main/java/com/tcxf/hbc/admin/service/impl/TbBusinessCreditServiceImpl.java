package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.TbBusinessCreditMapper;
import com.tcxf.hbc.admin.service.ITbBusinessCreditService;
import com.tcxf.hbc.common.entity.TbBusinessCredit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 商业授信表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-03
 */
@Service
public class TbBusinessCreditServiceImpl extends ServiceImpl<TbBusinessCreditMapper, TbBusinessCredit> implements ITbBusinessCreditService {

    @Autowired
    TbBusinessCreditMapper tbBusinessCreditMapper;

    @Override
    public TbBusinessCredit getTbBusinessCreditByUid(HashMap<String, Object> map){
        return tbBusinessCreditMapper.getTbBusinessCreditByUid(map);
    }

    @Override
    public HashMap<String, Object> getTbBusinessCreditDetailsByUid(HashMap<String, Object> map){
        return tbBusinessCreditMapper.getTbBusinessCreditDetailsByUid(map);
    }
}
