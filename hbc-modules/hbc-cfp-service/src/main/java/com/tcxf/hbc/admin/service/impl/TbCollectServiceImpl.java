package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.admin.mapper.TbCollectMapper;
import com.tcxf.hbc.admin.service.ITbCollectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 我的收藏表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@Service
public class TbCollectServiceImpl extends ServiceImpl<TbCollectMapper, TbCollect> implements ITbCollectService {

}
