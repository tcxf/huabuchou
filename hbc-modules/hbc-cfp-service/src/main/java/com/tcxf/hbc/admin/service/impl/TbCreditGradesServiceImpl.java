package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.tcxf.hbc.admin.mapper.TbCreditGradesMapper;
import com.tcxf.hbc.admin.service.ITbCreditGradesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 快速授信评分项 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
@Service
public class TbCreditGradesServiceImpl extends ServiceImpl<TbCreditGradesMapper, TbCreditGrade> implements ITbCreditGradesService {
    @Autowired
    private TbCreditGradesMapper tbCreditGradesMapper;
            
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:26 2018/10/31
    */        
    @Override
    public int findcountByDetailNameAndVersion(Map<String, Object> paramMap) {
        return tbCreditGradesMapper.findcountByDetailNameAndVersion(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:43 2018/10/31
    */
    @Override
    public int findcountByDetailNumAndVersion(Map<String, Object> paramMap) {
        return tbCreditGradesMapper.findcountByDetailNumAndVersion(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:43 2018/10/31
    */
    @Override
    public int findcountByDetailMinNumAndVersion(Map<String, Object> paramMap) {
        return tbCreditGradesMapper.findcountByDetailMinNumAndVersion(paramMap);
    }
}
