package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbImproveQuota;
import com.tcxf.hbc.admin.mapper.TbImproveQuotaMapper;
import com.tcxf.hbc.admin.service.ITbImproveQuotaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消费者用户提额授信结果表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbImproveQuotaServiceImpl extends ServiceImpl<TbImproveQuotaMapper, TbImproveQuota> implements ITbImproveQuotaService {

}
