package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.tcxf.hbc.admin.mapper.TbMessageLogMapper;
import com.tcxf.hbc.admin.service.ITbMessageLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 短信记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbMessageLogServiceImpl extends ServiceImpl<TbMessageLogMapper, TbMessageLog> implements ITbMessageLogService {

}
