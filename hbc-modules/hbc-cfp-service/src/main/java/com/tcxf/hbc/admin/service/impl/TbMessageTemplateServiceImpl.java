package com.tcxf.hbc.admin.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.TbMessageTemplateMapper;

import com.tcxf.hbc.admin.service.ITbMessageTemplateService;
import com.tcxf.hbc.common.entity.TbMessageTemplate;

public class TbMessageTemplateServiceImpl extends ServiceImpl<TbMessageTemplateMapper, TbMessageTemplate> implements ITbMessageTemplateService {
}
