package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.tcxf.hbc.admin.mapper.TbOperationLogMapper;
import com.tcxf.hbc.admin.service.ITbOperationLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbOperationLogServiceImpl extends ServiceImpl<TbOperationLogMapper, TbOperationLog> implements ITbOperationLogService {

}
