package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbPayment;
import com.tcxf.hbc.admin.mapper.TbPaymentMapper;
import com.tcxf.hbc.admin.service.ITbPaymentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbPaymentServiceImpl extends ServiceImpl<TbPaymentMapper, TbPayment> implements ITbPaymentService {

}
