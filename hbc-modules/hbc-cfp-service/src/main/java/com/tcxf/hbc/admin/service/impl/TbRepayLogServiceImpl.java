package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.TbRepayLogDto;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.admin.mapper.TbRepayLogMapper;
import com.tcxf.hbc.admin.service.ITbRepayLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRepayLogServiceImpl extends ServiceImpl<TbRepayLogMapper, TbRepayLog> implements ITbRepayLogService {

    @Autowired
    private TbRepayLogMapper tbRepayLogMapper;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:04 2018/6/13
     * 查询 提前还款分页信息
    */
    @Override
    public Page findAdvancePayPageInfo(Query<TbRepayLogDto> query) {
       List<TbRepayLogDto> list= tbRepayLogMapper.findAdvancePayPageInfo(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:03 2018/6/13
     * 根据条件查询 提前还款总金额
    */
    @Override
    public Map<String, Object> findAdvancePayTotalMoney(Map<String, Object> paramMap) {
        return tbRepayLogMapper.findAdvancePayTotalMoney(paramMap);
    }
}
