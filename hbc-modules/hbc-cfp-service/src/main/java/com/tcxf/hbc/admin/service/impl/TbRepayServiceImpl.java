package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbRepay;
import com.tcxf.hbc.admin.mapper.TbRepayMapper;
import com.tcxf.hbc.admin.service.ITbRepayService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
@Service
public class TbRepayServiceImpl extends ServiceImpl<TbRepayMapper, TbRepay> implements ITbRepayService {

}
