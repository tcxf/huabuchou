package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.tcxf.hbc.admin.mapper.TbRepayStatisticalMapper;
import com.tcxf.hbc.admin.service.ITbRepayStatisticalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款统计表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-19
 */
@Service
public class TbRepayStatisticalServiceImpl extends ServiceImpl<TbRepayStatisticalMapper, TbRepayStatistical> implements ITbRepayStatisticalService {

}
