package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.admin.model.dto.RepaymentDetail;
import com.tcxf.hbc.admin.model.dto.RepaymentDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.admin.mapper.TbRepaymentDetailMapper;
import com.tcxf.hbc.admin.service.ITbRepaymentDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRepaymentDetailServiceImpl extends ServiceImpl<TbRepaymentDetailMapper, TbRepaymentDetail> implements ITbRepaymentDetailService {

    @Autowired
    private TbRepaymentDetailMapper tbRepaymentDetailMapper;

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 13:42 2018/6/6
     */
    @Override
    public BigDecimal findAlreadyPayBackPlan(Map<String, Object> map) {
        return tbRepaymentPlanMapper.findAlreadyPayBackPlan(map); //写sql
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:24 2018/6/9
     */
    @Override
    public int findAlreadyPayBacknotPlan(String startDate, String endDate) {

        HashMap<String, Object> map = new HashMap<>();
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        return tbRepaymentDetailMapper.findAlreadyPayBacknotPlan(map); //写sql
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:15 2018/6/9
     */
    @Override
    public Page findPaybackPageInfo(Query<RepaymentDetailDto> query) {
         List<RepaymentDetailDto> list =tbRepaymentDetailMapper.findPaybackPageInfo(query,query.getCondition());

         //如果stutas:1 逾期  计算逾期的天数
         if(query.getCondition().get("status") instanceof  String){
                if ("1".equals(query.getCondition().get("status"))){
                    for (RepaymentDetailDto repaymentDetailDto:list) {
                        DecimalFormat    df   = new DecimalFormat("######0.00");
                        double margin = DateUtil.getDoubleMargin(DateUtil.getDateTime(), repaymentDetailDto.getRepaymentDate());
                        repaymentDetailDto.setMargin(df.format(margin));
                    }
                }
         }
         return query.setRecords(list);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:50 2018/6/11
     * 查询还款列表详情
    */
    @Override
    public Page findpaybackDetailInfo(Query<RepaymentDetail> query) {
       List<RepaymentDetail> list = tbRepaymentDetailMapper.findpaybackDetailInfo(query,query.getCondition());
       return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 12:00 2018/6/21
     * 根据条件查询 待还款
    */
    @Override
    public BigDecimal findWaitPay(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findWaitPay(paramMap);
    }

    @Override
    public Page repaymentPlanManagerList(Query<RepaymentDetailDto> query) {
        List<RepaymentDetailDto> list =tbRepaymentDetailMapper.findPaybackPageInfo(query,query.getCondition());
        return query.setRecords(list);
    }

    //消费者还款记录
    @Override
    public Page repaymentPlanManagerLists(Query <RepaymentDetailDto> query) {
        List<RepaymentDetailDto> list =tbRepaymentDetailMapper.findPaybackPageInfos(query,query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public Map <String, Object> DHYQSUM(Map <String, Object> map) {
        return tbRepaymentDetailMapper.DHYQSUM(map);
    }

    @Override
    public Map <String, Object> YHSUM(Map <String, Object> map) {
        return tbRepaymentDetailMapper.YHSUM(map);
    }

}
