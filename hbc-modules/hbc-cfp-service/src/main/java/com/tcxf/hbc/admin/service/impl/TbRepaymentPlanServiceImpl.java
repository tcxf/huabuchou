package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.model.dto.RepaymentDto;
import com.tcxf.hbc.admin.model.dto.TbrmRepayDto;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.admin.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.admin.service.ITbRepaymentPlanService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款计划表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRepaymentPlanServiceImpl extends ServiceImpl<TbRepaymentPlanMapper, TbRepaymentPlan> implements ITbRepaymentPlanService {

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;

    @Override
    public Map<String, Object> findwaitPayByCondiction(Map<String, Object> paramMap) {

        return tbRepaymentPlanMapper.findwaitPayByCondiction(paramMap);
    }

    @Override
    public Map<String, Object> findoverDuePayByCondiction(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findoverDuePayByCondiction(paramMap);
    }

    @Override
    public Map<String, Object> findBillAlreadyPayByCondition(Map<String, Object> paramMap) {
        return  tbRepaymentPlanMapper.findBillAlreadyPayByCondition(paramMap);
    }

    @Override
    public int findoverDueNum(Map<String, Object> totalMap) {
        return tbRepaymentPlanMapper.findoverDueNum(totalMap);
    }

    /**
     * 本月应还分页
     * liaozeyong
     * @return
     */
    @Override
    public Page repaymentPlanDetail(Query<RepaymentDto> query) {
        List<RepaymentDto> list = tbRepaymentPlanMapper.findRepaymentDetail(query , query.getCondition());
        return query.setRecords(list);
    }

    /**
     * 本月应还表单导出
     * liaozeyong
     * @param paramMap
     * @return
     */
    @Override
    public List<RepaymentDto> excelRepaymentPlanDetail(Map<String, Object> paramMap) {
        List<RepaymentDto> list = tbRepaymentPlanMapper.findRepaymentDetail(paramMap);
        return list;
    }

    @Override
    public List<TbrmRepayDto> findUserPaymentList(Map<String, Object> paramMap) {
        return  tbRepaymentPlanMapper.findUserPaymentList(paramMap);
    }
}
