package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.tcxf.hbc.admin.mapper.TbRepaymentRatioDictMapper;
import com.tcxf.hbc.admin.service.ITbRepaymentRatioDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款比率字典表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRepaymentRatioDictServiceImpl extends ServiceImpl<TbRepaymentRatioDictMapper, TbRepaymentRatioDict> implements ITbRepaymentRatioDictService {

}
