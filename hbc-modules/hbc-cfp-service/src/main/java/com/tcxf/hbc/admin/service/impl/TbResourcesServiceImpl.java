package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.mapper.TbResourcesMapper;
import com.tcxf.hbc.common.entity.TbResources;

import com.tcxf.hbc.admin.service.ITbResourcesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tcxf.hbc.admin.model.dto.tbResourcesDto;

/**
 * <p>
 * 资源信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbResourcesServiceImpl extends ServiceImpl<TbResourcesMapper, TbResources> implements ITbResourcesService {

    @Autowired
    private TbResourcesMapper resourcesMapper;

    @Override
    public Page getResourcesList() {
        Map<String, Object> params = new HashMap<>();
        params.put("status", "0");
        Query query = new Query(params);
        List<TbResources> resources = resourcesMapper.selectPage(query, new EntityWrapper <TbResources>());
        query.setRecords(resources);
        return query;
    }

    /**
     * 根据角色id查询权限
     * @param id
     * @return
     */
    @Override
    public List<TbResources> selectResources(String id,int type) {
        Map<String , Object> map = new HashMap<>();
        map.put("id",id);
        map.put("type",type);
        List<TbResources> tbResources = resourcesMapper.selectResources(map);
        return tbResources;
    }

    /**
     * 根据条件分页查询
     * @param pmap
     * @return
     */
    @Override
    public Page getPage(Map<String,Object> pmap){
        Map<String,Object> map = new HashMap<>();
        map.put("status","0");
        map.put("page",pmap.get("page"));
        Query<tbResourcesDto> query = new Query(map);
        List<tbResourcesDto> tb= resourcesMapper.getList(query,pmap);
        query.setRecords(tb);
        return query;
    }


//    @Override
//    public Integer deleteById(String id){
//        return resourcesMapper.deleteById(id);
//    }
////    @Override
//    public Integer updateById(TbResources re){
//        return resourcesMapper.updateById(re);
//
//    }

    /**
     * 获取tbResources所有
     * @param tbResources
     * @return
     */
//    @Override
////    public TbResources getAll(TbResources tbResources) {
////        return resourcesMapper.selectOne(tbResources);
////    }

}
