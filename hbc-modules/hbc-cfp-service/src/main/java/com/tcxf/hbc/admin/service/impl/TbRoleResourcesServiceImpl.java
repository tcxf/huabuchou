package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.admin.mapper.TbResourcesMapper;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.admin.mapper.TbRoleResourcesMapper;
import com.tcxf.hbc.admin.service.ITbRoleResourcesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色资源关系表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRoleResourcesServiceImpl extends ServiceImpl<TbRoleResourcesMapper, TbRoleResources> implements ITbRoleResourcesService {

    @Autowired
    private TbRoleResourcesMapper tbRoleResourcesMapper;

    @Autowired
    private TbResourcesMapper tbResourcesMapper;
    @Override
    public List<TbRoleResources> findByResourceId(Map<String,Object> map){
        return  tbRoleResourcesMapper.findByResourceId(map);
    }

    @Override
    @Transactional
    public void role(String id,String ids){
        tbRoleResourcesMapper.delete(new EntityWrapper<TbRoleResources>().eq("role_id",id));
        String [] idse = ids.split(",");
        List<TbResources> t = tbResourcesMapper.selectList(new EntityWrapper<TbResources>().in("id",idse));
        List<String> olist = new ArrayList<>();
        for ( TbResources tb: t ) {
            if(tb.getParentId()!=null) {
                olist.add(tb.getParentId().toString());
            }
        }
        //去重 模块
        HashSet h = new HashSet(olist);
        olist.clear();
        olist.addAll(h);
        //添加模块权限
        for (String ob: olist  ) {
            TbRoleResources tb = new TbRoleResources();
            tb.setRoleId(id);
            tb.setResourceId(ob);
            tbRoleResourcesMapper.insert(tb);
        }

        //添加普通菜单权限
        String [] ids_ = ids.split(",");
        for (String str: ids_){
            TbRoleResources tb = new TbRoleResources();
            tb.setRoleId(id);
            tb.setResourceId(str);
            tbRoleResourcesMapper.insert(tb);
        }
    }
}
