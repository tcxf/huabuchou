package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.admin.mapper.TbRoleMapper;
import com.tcxf.hbc.admin.service.ITbRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbRoleServiceImpl extends ServiceImpl<TbRoleMapper, TbRole> implements ITbRoleService {

    @Autowired
    private TbRoleMapper tbRoleMapper;
    @Override
    public Page selectPage(String aid) {
        Map<String, Object> params = new HashMap<>();
        params.put("status", "0");
        Query query = new Query(params);
        List<TbRole> resources = tbRoleMapper.selectPage(query, new EntityWrapper<TbRole>().eq("uid",aid));
        query.setRecords(resources);
        return query;
    }
    @Override
    public List<TbRole> selectRole(Map<String , Object> map) {
        List<TbRole> tbRole = tbRoleMapper.selectRole(map);
        return tbRole;
    }
}
