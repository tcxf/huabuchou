package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbScoreLog;
import com.tcxf.hbc.admin.mapper.TbScoreLogMapper;
import com.tcxf.hbc.admin.service.ITbScoreLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户积分明细表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbScoreLogServiceImpl extends ServiceImpl<TbScoreLogMapper, TbScoreLog> implements ITbScoreLogService {

}
