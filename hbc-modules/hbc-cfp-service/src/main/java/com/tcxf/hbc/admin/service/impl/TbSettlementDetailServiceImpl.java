package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.feign.ISettlementInfoService;
import com.tcxf.hbc.admin.model.dto.TbSettlementDetailDto;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.admin.mapper.TbSettlementDetailMapper;
import com.tcxf.hbc.admin.service.ITbSettlementDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  交易结算表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbSettlementDetailServiceImpl extends ServiceImpl<TbSettlementDetailMapper, TbSettlementDetail> implements ITbSettlementDetailService {

    @Autowired
    private TbSettlementDetailMapper tbSettlementDetailMapper;

    @Autowired
    public ITbSettlementDetailService iTbSettlementDetailService;

    @Override
    public BigDecimal findPAlreadySettled(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.findAlreadySettled( paramMap);
    }

    @Override
    public Page findClearDetailPageInfo(Query<TbSettlementDetailDto> query) {
        List<TbSettlementDetailDto> list =tbSettlementDetailMapper.findClearDetailPageInfo(query,query.getCondition());
        return query.setRecords(list);
    }

    
    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:43 2018/6/20
     * 商户管理 已结算 待结算
    */
    @Override
    public Map findclearMoneyInfo(Map<String, Object> paramMap) {
        Map<String, Object> map = new HashMap<>();
        BigDecimal alreadySettled = tbSettlementDetailMapper.findAlreadySettled(paramMap);
        BigDecimal waitSettled = tbSettlementDetailMapper.findWaitSettled(paramMap);
        map.put("alreadySettled",alreadySettled);
        map.put("waitSettled",waitSettled);
        return map;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:56 2018/6/21
     * 运营商端-主页 根据条件查询:待结算金额
    */
    @Override
    public BigDecimal findWaitSettled(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.findWaitSettled(paramMap);
    }

    /**
     * 运营商查询商户结算
     * @param condition
     * @return
     */
    @Override
    public Page findOperaSettlement(Map<String, Object> condition) {
        Query query = new Query(condition);
        List<TbSettlementDetailDto> list = tbSettlementDetailMapper.findOperaSettlement(query,condition);
        query.setRecords(list);
        return query;
    }

    /**
     * 查询已结算总额和未结算总额
     * @param map
     * @return
     */
    @Override
    public int totalAmount(Map<String, Object> map) {
        int shareFee = tbSettlementDetailMapper.totalAmount(map);
        return shareFee;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:56 2018/6/28
     * 查询结算管理分页查询
    */
    @Override
    public Page findSettlementPageInfo(Query<TbSettlementDetailDto> query) {
         List<TbSettlementDetailDto> list=tbSettlementDetailMapper.findSettlementPageInfo(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:24 2018/6/28
     * 查询返佣(运营商 直接上级 间接上级) 根据结算表主键id查询
     */
    @Override
    public Map<String, Object> fanYongInfo(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.fanYongInfo(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:15 2018/6/30
     * 结算管理 (头部信息 运营商返佣 直接上级 间接上级返佣)
    */
    @Override
    public Map<String, Object> findSettlementHeadInfo(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.findSettlementHeadInfo(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:14 2018/6/30
     * 结算管理 平台返佣(待结算)
    */
    @Override
    public Map<String, Object> findPFanYongWaitSet(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.findPFanYongWaitSet(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:14 2018/6/30
     *  结算管理 平台返佣(已结算)
    */
    @Override
    public Map<String, Object> findPFanYongAlreadySet(Map<String, Object> paramMap) {
        return tbSettlementDetailMapper.findPFanYongAlreadySet(paramMap);
    }

    /**
     * 资金端商户结算记录
     * @return
     */
    @Override
    public Page QueryALLSettlement(Query<TbSettlementDetailDto> query) {
        List<TbSettlementDetailDto> list= tbSettlementDetailMapper.QueryALLSettlement(query,query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public List <TbSettlementDetailDto> QueryALLSettlementx(Map <String, Object> map) {
        return tbSettlementDetailMapper.QueryALLSettlement(map);
    }

    /**
     * 资金端结算管理待结算总额
     * @param paramMap
     * @return
     */
    @Override
    public Map <String, Object> DAmount(Map <String, Object> paramMap) {
        return tbSettlementDetailMapper.DAmount(paramMap);
    }

    /**
     * 资金端结算管理已结算金额
     * @param paramMap
     * @return
     */
    @Override
    public Map <String, Object> YAmount(Map <String, Object> paramMap) {
        return tbSettlementDetailMapper.YAmount(paramMap);
    }

    /***
     * 资金端结算管理 运营商返佣 直接上级返佣  间接上级返佣总额
     * @param paramMap
     * @return
     */
    @Override
    public Map <String, Object> Maids(Map <String, Object> paramMap) {
        return tbSettlementDetailMapper.Maids(paramMap);
    }

    /**
     * 资金端交易记录明细
     * @param query
     * @return
     */
    @Override
    public Page TransactionDetails(Query <TbSettlementDetailDto> query) {
        List<TbSettlementDetailDto> list= tbSettlementDetailMapper.TransactionDetails(query,query.getCondition());
        return query.setRecords(list);
    }
    /**
     * 线下结算
     */
    @Override
    @Transactional
    public void doSettlement(HttpServletRequest request) {
        String id = request.getParameter("id");
        TbSettlementDetail tb = iTbSettlementDetailService.selectOne(new EntityWrapper<TbSettlementDetail>().eq("id", id));
        if(tb!=null) {
            tb.setStatus("1");
            tb.setIsPay("0");
            tb.setSettlementTime(new Date());
            iTbSettlementDetailService.updateById(tb);
            if(tb==null){
                throw new CheckedException("结算失败！");
            }
        }
    }


}
