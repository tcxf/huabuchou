package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbSettlementDict;
import com.tcxf.hbc.admin.mapper.TbSettlementDictMapper;
import com.tcxf.hbc.admin.service.ITbSettlementDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 结算方式比例字典表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbSettlementDictServiceImpl extends ServiceImpl<TbSettlementDictMapper, TbSettlementDict> implements ITbSettlementDictService {

}
