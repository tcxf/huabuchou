package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.tcxf.hbc.admin.mapper.TbThreeSaleProfitShareMapper;
import com.tcxf.hbc.admin.service.ITbThreeSaleProfitShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销利润分配表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbThreeSaleProfitShareServiceImpl extends ServiceImpl<TbThreeSaleProfitShareMapper, TbThreeSaleProfitShare> implements ITbThreeSaleProfitShareService {

}
