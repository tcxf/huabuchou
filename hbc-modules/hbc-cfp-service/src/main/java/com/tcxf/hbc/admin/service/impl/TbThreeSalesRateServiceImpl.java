package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.tcxf.hbc.admin.mapper.TbThreeSalesRateMapper;
import com.tcxf.hbc.admin.service.ITbThreeSalesRateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销利润分配比例表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbThreeSalesRateServiceImpl extends ServiceImpl<TbThreeSalesRateMapper, TbThreeSalesRate> implements ITbThreeSalesRateService {

}
