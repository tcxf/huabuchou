package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSalesRef;
import com.tcxf.hbc.admin.mapper.TbThreeSalesRefMapper;
import com.tcxf.hbc.admin.service.ITbThreeSalesRefService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销用户关系绑定表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbThreeSalesRefServiceImpl extends ServiceImpl<TbThreeSalesRefMapper, TbThreeSalesRef> implements ITbThreeSalesRefService {

}
