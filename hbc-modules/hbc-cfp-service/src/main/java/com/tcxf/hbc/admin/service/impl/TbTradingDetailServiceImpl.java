package com.tcxf.hbc.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.admin.common.util.DateUtil;
import com.tcxf.hbc.admin.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.admin.mapper.TbSettlementDetailMapper;
import com.tcxf.hbc.admin.model.dto.TbTradingDetailDto;
import com.tcxf.hbc.admin.model.dto.TransactionDto;
import com.tcxf.hbc.admin.model.dto.UnoutSettmentDto;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.admin.mapper.TbTradingDetailMapper;
import com.tcxf.hbc.admin.service.ITbTradingDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 用户交易记录表 服务实现类
 * </p>
 *
 * @author YWT_tai
 * @since 2018-05-30
 */
@Service
public class TbTradingDetailServiceImpl extends ServiceImpl<TbTradingDetailMapper, TbTradingDetail> implements ITbTradingDetailService {
    private static final String DATE_FORMAT      = "yyyy-MM-dd";
    protected org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TbTradingDetailMapper tbTradingDetailMapper;

    @Autowired
    private TbSettlementDetailMapper tbSettlementDetailMapper;

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;




    @Override
    public List<Map<String, Object>> findsevendaytrade( Map<String, Object> mmp) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, -6);
        Date startDate = c.getTime();

        //开始时间
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String startT= sdf.format(startDate);
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> paramMap = new HashMap<>();
        for (int i = 0; i <7 ; i++) {
            String daytime = DateUtil.addDay(startT, i);
            paramMap.put("daytime",daytime);
            paramMap.put("oid",mmp.get("oid"));
            paramMap.put("isFund",mmp.get("isFund"));
            Map<String,Object> map=tbTradingDetailMapper.findsevendaytrade(paramMap);//写sqli
            list.add(map);
        }
        return list;
    }

    @Override
    public BigDecimal selectTotalMoney(Map<String, Object> map) {
        return  tbTradingDetailMapper.selectTotalMoney(map);//写sql
    }

    @Override
    public BigDecimal selectTotalMoneyx(Map <String, Object> map) {
        return  tbTradingDetailMapper.selectTotalMoneyx(map);//写sql
    }

    @Override
    public Map<String, Object> findPtotalData(Map<String, Object> totalMap) {

        Map<String, Object> map = new HashMap<>();

        //已结算
        BigDecimal pAlreadySettled = tbSettlementDetailMapper.findAlreadySettled(totalMap);

        //待结算
        BigDecimal pWaitSettled = tbSettlementDetailMapper.findWaitSettled(totalMap);

        // 已还款 :(用户分期已还款 + 用户未分期已还款)
        BigDecimal pPayback = tbRepaymentPlanMapper.findAlreadyPayBackPlan(totalMap);//用户分期还款金额
        /*int alreadyPayBacknotPlan = tbRepaymentDetailMapper.findAlreadyPayBacknotPlan(map);//用户未分期已还款
        int ppayback=paybackplan+alreadyPayBacknotPlan;*/

        //逾期 :(用户分期 逾期 + 用户未分期 逾期)
        BigDecimal overdue = tbRepaymentPlanMapper.findoverDuePlan(totalMap);
        BigDecimal overDuePlan =  overdue==null ? new BigDecimal("0"): overdue;//用户分期 未还款
      /*  Integer overDuenotplan = tbRepaymentDetailMapper.findnotPayBacknotPlan(map);//用户未分期 未还款
        int overDue=overDueplan+overDuenotplan;*/

        //待还款 (用户分期 待还款 + 用户未分期 待还款)
        BigDecimal waitPay = tbRepaymentPlanMapper.findWaitPay(totalMap);
        BigDecimal waitPayPlan =  waitPay==null ? new BigDecimal("0"):waitPay;//用户分期 待还
       /* Integer waitPaynotplan = tbRepaymentDetailMapper.findwaitPaynotPlan(map);
        int  waitPay =waitPayplan+waitPaynotplan;*/

        map.put("pPayback",pPayback);
        map.put("pAlreadySettled",pAlreadySettled);
        map.put("overDue",overDuePlan);
        map.put("waitPay",waitPayPlan);
        map.put("pWaitSettled",pWaitSettled);

        return map;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:15 2018/6/14
     * 查询交易详情记录分页  分页信息待定
    */
    @Override
    public Page findAdvancePayDetailInfo(Query<TbTradingDetailDto> query) {

        String ids = (String) query.getCondition().get("ids");
        String[] split = ids.split(",");
        List allList = new ArrayList<TbTradingDetailDto>();
        Map mmp = new HashMap<String,Object>();
        if(split.length!=0){
            for (int i = 0; i < split.length; i++) {
                String s = split[i];
                if(!"".equals(s )){
                    mmp.put("id",s);
                    List<TbTradingDetailDto> list=tbTradingDetailMapper.findAdvancePayDetailInfo(query,mmp);
                    allList.addAll(list);
                }
            }
        }
        return  query.setRecords(allList);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:36 2018/6/14
     * 查询未出账单 分页
    */
    @Override
    public Page findNoBillPageInfo(Query<TbTradingDetailDto> query) {
        List<TbTradingDetailDto> list=tbTradingDetailMapper.findNoBillPageInfo(query,query.getCondition());
        return query.setRecords(list);
    }
    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:36 2018/6/14
     * 查询未出账单 下载报表
     */
    @Override
    public  List<TbTradingDetailDto> findNoBillPageInfoB(Map<String,Object> map) {
        return tbTradingDetailMapper.findNoBillPageInfo(map);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 20:41 2018/6/19
     * 商户管理 详情-交易记录列表
    */
    @Override
    public Page findTradingPageInfo(Query<TbTradingDetailDto> query) {

        //logger.info("========================查询交易记录   平台端======入参====================="+JSON.toJSONString(query));
        List<TbTradingDetailDto> list=tbTradingDetailMapper.findTradingPageInfo(query,query.getCondition());
        //logger.info("========================查询交易记录   平台端=======出参===================="+JSON.toJSONString(list));
        return query.setRecords(list);
    }

    @Override
    public BigDecimal findTradingtotalAmount(Map<String, Object> paramMap) {
        return tbTradingDetailMapper.selectTotalMoney(paramMap);
    }

    /**
     * 查询未生成结算单
     * @param tbTradingDetailDtoQuery
     * @return
     */
    @Override
    public Page uncreateSettlementDetail(Query<TbTradingDetailDto> tbTradingDetailDtoQuery) {
        List<TbTradingDetailDto> list = tbTradingDetailMapper.uncreateSettlementDetail(tbTradingDetailDtoQuery,tbTradingDetailDtoQuery.getCondition());
        return tbTradingDetailDtoQuery.setRecords(list);
    }

    /**
     * 运营商获取详细明细
     * @param map
     * @return
     */
    @Override
    public Page findTradingDetail(Map<String, Object> map) {
        Query query = new Query(map);
        List<TbTradingDetailDto> list = tbTradingDetailMapper.findTradingDetail(query,map);
        query.setRecords(list);
        return query;
    }

    /**
     * 查询未出账单金额总额
     * @param map
     * @return
     */
    @Override
    public BigDecimal uncreateAmount(Map<String, Object> map) {
        BigDecimal i = tbTradingDetailMapper.uncreateAmount(map);
        return i;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:06 2018/6/23
     * 查询运营商让利分页
    */
    @Override
    public Page findOperaPageInfo(Query<TbTradingDetailDto> query) {
         List<TbTradingDetailDto> list=tbTradingDetailMapper.findOperaPageInfo(query,query.getCondition());
        /*for (TbTradingDetailDto tbTradingDetailDto:list) {
            String shareProfit = tbTradingDetailDto.getPshareProfit();
            BigDecimal d = BigDecimal.valueOf(Long.parseLong(shareProfit)).divide(BigDecimal.valueOf(100l), new MathContext(2));
            tbTradingDetailDto.setPshareProfit(d.toString());
        }*/
         return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:41 2018/6/23
     * 根据条件查询总分润
    */
    @Override
    public Map<String, Object> findTotalProfit(Map<String, Object> paramMap) {
        return tbTradingDetailMapper.findTotalProfit(paramMap);
    }

    /**
     * 运营商查询商户让利
     * @param query
     * @return
     */
    @Override
    public Page msfManagerList(Query<TbTradingDetailDto> query) {
        List<TbTradingDetailDto>  list = tbTradingDetailMapper.msfManagerList(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
     * 运营商查询商户让利下载报表
     * liaozeyong
     * @param map
     * @return
     */
    @Override
    public List<TbTradingDetailDto> msf(Map<String, Object> map) {
        return tbTradingDetailMapper.msf(map);
    }

    @Override
    public BigDecimal metchatTotalAmount(Map<String , Object> map) {
        return tbTradingDetailMapper.merchantTotalAmount(map);
    }

    @Override
    public int TotalAmount(Map <String, Object> paramMap) {
        return tbTradingDetailMapper.TotalMoney(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:59 2018/6/27
     * 查询未出账单 详情
    */
    @Override
    public Page findNoBillDetail(Query<TbTradingDetailDto> query) {
         List<TbTradingDetailDto> list=tbTradingDetailMapper.findAdvancePayDetailInfo(query,query.getCondition());
         return query.setRecords(list);
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:39 2018/6/28
     * 查询交易管理 明细查询
     */
    @Override
    public Page findSettlementDetailInfo(Query<TbTradingDetailDto> query) {
        List<TbTradingDetailDto> list=tbTradingDetailMapper.findSettlementDetailInfo(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
     * 平台交易记录分页
     * @param query
     * @return
     */
        @Override
        public Page transaction(Query <TransactionDto> query) {
        List<TransactionDto> list=tbTradingDetailMapper.transaction(query,query.getCondition());
        return query.setRecords(list);
    }

    @Override
    public List<TransactionDto> transactionB(Map<String,Object> map) {
        return  tbTradingDetailMapper.transaction(map);
    }

    @Override
    public Map<String,Object> SUM(Map <String, Object> map) {
        return  tbTradingDetailMapper.SUM(map);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:41 2018/6/30
     * 查询未出结算单 分页管理
    */
    @Override
    public Page findNoSettlementPageInfo(Query<UnoutSettmentDto> query) {
         List<UnoutSettmentDto> list=tbTradingDetailMapper.findNoSettlementPageInfo(query,query.getCondition());
         return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:18 2018/6/30
     * 查询未出结算单 (每个商户)交易明细
    */
    @Override
    public Page findMerchantSettlementDetail(Query<TbTradingDetailDto> query) {
         List<TbTradingDetailDto> list= tbTradingDetailMapper.findMerchantSettlementDetail(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 16:02 2018/7/11
     * 未出账单的总金额
    */
    @Override
    public Map<String, Object> findNoBillTotalMoney(Map<String, Object> paramMap) {
        return tbTradingDetailMapper.findNoBillTotalMoney(paramMap);
    }

}
