package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.admin.mapper.TbUpMoneyMapper;
import com.tcxf.hbc.admin.service.ITbUpMoneyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信提额表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-17
 */
@Service
public class TbUpMoneyServiceImpl extends ServiceImpl<TbUpMoneyMapper, TbUpMoney> implements ITbUpMoneyService {

}
