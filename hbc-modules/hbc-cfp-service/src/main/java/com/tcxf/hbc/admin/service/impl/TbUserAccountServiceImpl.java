package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.admin.mapper.TbUserAccountMapper;
import com.tcxf.hbc.admin.service.ITbUserAccountService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbUserAccountServiceImpl extends ServiceImpl<TbUserAccountMapper, TbUserAccount> implements ITbUserAccountService {

}
