package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.tcxf.hbc.admin.mapper.TbUserAccountTypeConMapper;
import com.tcxf.hbc.admin.service.ITbUserAccountTypeConService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户类型表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbUserAccountTypeConServiceImpl extends ServiceImpl<TbUserAccountTypeConMapper, TbUserAccountTypeCon> implements ITbUserAccountTypeConService {

}
