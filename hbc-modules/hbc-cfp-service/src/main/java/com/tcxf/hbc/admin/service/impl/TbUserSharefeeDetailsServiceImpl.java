package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbUserSharefeeDetails;
import com.tcxf.hbc.admin.mapper.TbUserSharefeeDetailsMapper;
import com.tcxf.hbc.admin.service.ITbUserSharefeeDetailsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户分润明细表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@Service
public class TbUserSharefeeDetailsServiceImpl extends ServiceImpl<TbUserSharefeeDetailsMapper, TbUserSharefeeDetails> implements ITbUserSharefeeDetailsService {

}
