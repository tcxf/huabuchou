package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbUserSharefeeSettlement;
import com.tcxf.hbc.admin.mapper.TbUserSharefeeSettlementMapper;
import com.tcxf.hbc.admin.service.ITbUserSharefeeSettlementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户分润结算表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@Service
public class TbUserSharefeeSettlementServiceImpl extends ServiceImpl<TbUserSharefeeSettlementMapper, TbUserSharefeeSettlement> implements ITbUserSharefeeSettlementService {

}
