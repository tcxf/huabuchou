package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.tcxf.hbc.admin.mapper.TbWalletCashrateMapper;
import com.tcxf.hbc.admin.service.ITbWalletCashrateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
@Service
public class TbWalletCashrateServiceImpl extends ServiceImpl<TbWalletCashrateMapper, TbWalletCashrate> implements ITbWalletCashrateService {

}
