package com.tcxf.hbc.admin.service.impl;

import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.admin.mapper.TbWalletIncomeMapper;
import com.tcxf.hbc.admin.service.ITbWalletIncomeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbWalletIncomeServiceImpl extends ServiceImpl<TbWalletIncomeMapper, TbWalletIncome> implements ITbWalletIncomeService {

    @Autowired
    private TbWalletIncomeMapper tbWalletIncome;
    @Override
    public Page getWalletIncomeList(Map<String,Object> map) {
        Query query = new  Query(map);
        List<TbWalletIncome> list= tbWalletIncome.getWalletIncomeList(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    @Override
    public TbWalletIncome getamount(String type, String wid) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("type",type);
        map.put("wid",wid);
        return tbWalletIncome.getamount(map);

    }
}
