package com.tcxf.hbc.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.admin.mapper.TbWalletWithdrawalsMapper;
import com.tcxf.hbc.admin.model.dto.TbWalletWithdrawalsDto;
import com.tcxf.hbc.admin.service.ITbWalletWithdrawalsService;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbWalletWithdrawalsServiceImpl extends ServiceImpl<TbWalletWithdrawalsMapper, TbWalletWithdrawals> implements ITbWalletWithdrawalsService {
    @Autowired
    private TbWalletWithdrawalsMapper tbWalletWithdrawalsMapper;

    /**
     * 钱包提现分页查询
     */
    @Override
    public Page findWalletwithdrawalsList(Map<String, Object> map) {
        Query<TbWalletWithdrawalsDto> query = new  Query(map);
        List<TbWalletWithdrawalsDto> list= tbWalletWithdrawalsMapper.findWalletwithdrawalsList(query,query.getCondition());
        query.setRecords(list);
        return query;
    }

    @Override
    public int savaWallet(TbWalletWithdrawals t) {
        return tbWalletWithdrawalsMapper.insert(t);
    }




}
