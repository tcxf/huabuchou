/**
 * pager 分页控件
 * @author caizongyou
 */

$(function(){
	$.fn.pager = function(data) {
    	var $this = $(this);
    	var pager = {
    		formId: "searchForm",
    		pageSize: 10, //每页大小
    		pageNumber: 1, //当前页码
    		totalCount: 10, //总记录数
    		pageCount: 1, //总页数
    		headerTemplate:"", //显示数据头部模版
    		foreachTemplate:"", //显示数据模版
    		footerTemplate:"", //显示数据底部模版
    		noDataTemplate:"", //没有数据显示模版
    		data:"",
    		callbak:null, //分页数据加载完成的回调函数
    		pagerPages: 7, //分页控件显示的页码数量
    		pagerTemplate:0, //分页控件模版
    		showPages:true, //是否显示分页控件
    		url:"", //获取数据url
    		initData: function(d){ //重新加载数据
    			initData(d);
    		},
    		gotoPage: function(pageNumber){ //加载指定页码的数据
    			goToPage(pageNumber);
    		},
    		removeUri:"",
    		newUri:""
    	};
    	if(data.pageSize != null) pager.pageSize = data.pageSize;
    	if(data.pageNumber != null) pager.pageNumber = data.pageNumber;
    	if(data.template.body != null) pager.foreachTemplate = data.template.body;
    	if(data.template.header != null) pager.headerTemplate = data.template.header;
    	if(data.template.footer != null) pager.footerTemplate = data.template.footer;
    	if(data.template.noData != null) pager.noDataTemplate = data.template.noData;
    	if(data.callbak != null) pager.callbak = data.callbak;
    	if(data.url != null) pager.url = data.url;
    	if(data.data != null) pager.data = data.data;
    	if(data.pagerTemplate != null) pager.pagerTemplate = data.pagerTemplate;
    	if(data.pagerPages != null) pager.pagerPages = data.pagerPages;
    	if(data.showPages != null) pager.showPages = data.showPages;
    	if(data.removeUri != null) pager.removeUri = data.removeUri;
    	if(data.newUri != null) pager.newUri = data.newUri;
    	if(data.formId != null) pager.formId = data.formId;
    	$("#"+pager.formId).append('<input type="hidden" value="'+pager.pageNumber+'" name="page" id="pageNumber"/><input type="hidden" value="'+pager.pageSize+'" name="limit" id="pageSize"/>');
    	var d = $("#"+pager.formId).serialize();
    	initData(d);
    	
		
		$("#checkall").unbind("click");
		$("#checkall").click(function(){
			var _ids = $(".ids");
			var $idsCheckedCheck = $("#"+pager.formId+" input[name='ids']:checked");
			if(_ids.length == $idsCheckedCheck.length) _ids.prop("checked", false);
			else {
				_ids.prop("checked", true);
			}
		});

        function renderLink(buttonText, pagenumber, pagecount, linkClickCallback) {
        	var $link = $('<li class="pagination"><a href="javascript:void(0);" aria-label="Previous"><span aria-hidden="true">' + buttonText + '</span></a></li>');
        	if (buttonText == "上一页") {
                if(pagenumber <= 0){
                	$link.click(function(){});
                }else{
                	$link.click(function(){linkClickCallback(pagenumber);});
                }
            }else if(buttonText == "下一页"){
            	if(pagenumber > pagecount){
                	$link.click(function(){});
                }else{
                	$link.click(function(){linkClickCallback(pagenumber);});
                }
            }
            return $link;
        }
        
        function initData(d){
//			var $load = msgutil.loading.simple("加载中...");
        	//加载中显示
        	$(".reqLoading").css("display","block");
        	$.ajax({
				url:pager.url,
			    type:'POST', //GET
			    data:d,
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    error:function(e){
			    	var _h = pager.headerTemplate;
			    	_h += pager.noDataTemplate;
			    	_h += pager.footerTemplate;
			    	$this.html(_h);
    	    		$this.parent().find(".page-size").remove();
    	    		$this.parent().append('<span class="text-muted small pull-right page-size">总记录数：'+0+'条</span>');
			    },
			    success:function(results){
			    	$(".reqLoading").css("display","none");
//	        		msgutil.close($load);
		    		var _h = pager.headerTemplate;
	        		if(results.code == 0){
	    	    		pager.totalCount = results.data.total;
	    	    		pager.pageSize = results.data.size;
	    	    		pager.pageNumber = results.data.current;
	    	    		pager.pageCount = results.data.pages;
	    	    		
	    	    		if(pager.showPages){ //是否显示分页控件
		    	    		$this.parent().find(".pager-pg").remove();
		    	    		if(pager.pageCount > 1){
			    	    		var $pager = $('<center class="pager-pg"></center>');
				    	        var $pagerContent = $('<ul class="pagination"></ul>');
				    	        $pagerContent.append(renderLink("上一页", pager.pageNumber - 1, pager.pageCount, goToPage));
			    	    		var startPoint = 1;
				    	        var endPoint = pager.pagerPages;
				    	        var pagenumber = pager.pageNumber;
				    	        var pagecount = pager.pageCount;
				    	        console.log("pagecount="+pagecount);
				    	        
				    	        if (pagenumber > parseInt((pager.pagerPages-1)/2)) {
				    	            startPoint = pagenumber - parseInt((pager.pagerPages-1)/2);
				    	            endPoint = pagenumber + parseInt((pager.pagerPages-1)/2);
				    	        }
				    	        if (endPoint > pagecount) {
				    	            startPoint = pagecount - (pager.pagerPages - 1);
				    	            endPoint = pagecount;
				    	        }
		
				    	        if (startPoint < 1) {
				    	            startPoint = 1;
				    	        }
				    	        
				    	        
			    	        	if(startPoint > 1){
			    	        		var currentLink = $('<li data="'+1+'"><a href="javascript:void(0);">' + (1) + '</a></li>');
			    	        		currentLink.click(function() { goToPage($(this).attr("data")); });
			    	        		$pagerContent.append(currentLink);
			    	        		$pagerContent.append('<li><a href="javascript:void(0);">...</a></li>');
			    	        	}
			
				    	        // loop thru visible pages and render buttons
				    	        for (var page = startPoint; page <= endPoint; page++) {
				    	            var currentLink = $('<li data="'+page+'"><a href="javascript:void(0);">' + (page) + '</a></li>');
				    	            var selclass = "active";
				    	            page == pagenumber ? currentLink.addClass(selclass) : currentLink.click(function() { goToPage($(this).attr("data")); });
				    	            currentLink.appendTo($pagerContent);
				    	        }
			    	        	if(endPoint < pagecount){
			    	        		$pagerContent.append('<li><a href="javascript:void(0);">...</a></li>');
			    	        		var currentLink = $('<li data="'+pagecount+'"><a href="javascript:void(0);">' + (pagecount) + '</a></li>');
			    	        		currentLink.click(function() { goToPage($(this).attr("data")); });
			    	        		$pagerContent.append(currentLink);
			    	        	}
			    	        	$pagerContent.append(renderLink("下一页", pager.pageNumber + 1, pager.pageCount, goToPage));
			    	        	$pager.append($pagerContent);
			    	        	$pager.append("&nbsp;");
			    	    		$("#pnumber").click(function(){
			    	    			$(this).select();
			    	    		});
			    	    		$this.parent().append($pager);
		    	    		}
	    	    		}
	    	    		var list = results.data.records;
	    	    		var pattern =/{(.+?)}/g;
	    	    		var text=pager.foreachTemplate.match(pattern);
	        			var p = text.toString().replace(/({)/g, "").replace(/(})/g, "").split(",");
	    	    		for(var i = 0 ; i < list.length ; i++){
	    	    			_h += pager.foreachTemplate;
	    	    			if(p != null && p.length > 0){
	    		    			for(var b = 0 ; b < p.length ; b++){
	    		    				var t = p[b];
	    		    				var m = 9999999;
	    		    				if(p[b].indexOf("_") != -1){
	    		    					t = p[b].substring(0, p[b].indexOf("_"));
	    		    					try{m = parseInt(p[b].substring(p[b].indexOf("_")+1));}catch(e){}
	    		    				}
	    		    				
	    		    				var v = list[i][t];
	    		    				try{
		    		    				if(v.length > m){
		    		    					v = v.substring(0,m);
		    		    				}
	    		    				}catch(e){
	    		    					v = ""
	    		    				}
	    		    				
	    		    				_h = _h.replace("{"+p[b]+"}", v);
	    		    			}
	    	    			}
	    	    		}

	    	    		if(pager.totalCount == 0){
	    	    			_h += pager.noDataTemplate;
	    	    		}
	    	    		_h += pager.footerTemplate;
	    	    		$this.html(_h);
	    	    		$this.parent().find(".page-size").remove();
	    	    		$this.parent().append('<span class="text-muted small pull-right page-size">总记录数：'+pager.totalCount+'条</span>');
	    	    		if(pager.callbak != null) pager.callbak(results);
	        		}
			    }
			});
        }
        
        function goToPage(pageNumber){
        	if(pageNumber != null){
        		$("#"+pager.formId).find("#pageNumber").val(pageNumber);
        	}
        	initData($("#"+pager.formId).serialize());
        }
    	
    	$("#search").click(function(){
    		goToPage(1);
    	});
    	
    	$("#pageSize").change(function(){
    		goToPage(1);
    	})
        
		return pager;
    };
});