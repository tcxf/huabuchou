var dialogBox;
function getDialogBox(){
	if(dialogBox == null) dialogBox = api.require('dialogBox');
	return dialogBox;
}

var dialog = {
	toast:function(title){
		api.toast({
		    msg: title,
		    duration: 2000,
		    location: 'middle'
		});
	},
	confirm:function(option,okcallback){
		getDialogBox().alert({
	        texts : {
	            title : option.title,
	            content : option.content,
	            leftBtnTitle : '取消',
	            rightBtnTitle : '确定'
	        },
	        styles : {
	            bg : '#fff',
	            corner : 6,
	            w : 260,
	            title : {
	                marginT : 20,
	                icon : 'widget://image/gou.png',
	                iconSize : 32,
	                titleSize : 18,
	                titleColor : '#000'
	            },
	            content : {
	                color : '#000',
	                size : 16
	            },
	            left : {
	                marginB : 15,
	                marginL : 40,
	                w : 40,
	                h : 35,
	                corner : 2,
	                color : "#000",
	                bg : '#fff',
	                size : 16
	            },
	            right : {
	                marginB : 15,
	                marginL : 88,
	                w : 40,
	                h : 35,
	                corner : 2,
	                color : "#000",
	                bg : '#fff',
	                size : 16
	            }
	        },
	        tapClose : true
	    }, function(ret) {
            dialogBox.close({
                dialogName: 'alert'
            });
	        if (ret.eventType == 'right') {
	            okcallback();
	        }
	    });
	}
}