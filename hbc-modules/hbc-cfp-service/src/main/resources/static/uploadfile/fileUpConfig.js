$(function () {
	//验证是否有修改显示文件
		$(".fileUpload").each(function(i,e){
			//当前上传对象
			var object = $(e).parent();
			//获取修改文件路经
			var ids = $(e).parent().attr("data-list");
			//获取当前工程地址
			var http = $(e).parent().attr("data-http");
			//获取上传地址
			var url = $(e).parent().attr("data-url");
			//多文件上传 true：是 flase否
			var dataMorethan = $(e).parent().attr("data-morethan");
			//上传完整地址
			//var dataServicePicPath=http+url;
			//获取图片显示地址
			var dataServicePicPath = $(e).parent().attr("data-servicePicPath");
			//显示文件集合
			var dataList = $(e).parent().attr("data-list");
			//获取文件上传大小
			var dataSize = $(e).parent().attr("data-size");
			//获取文件上传name type="file"
			var fileName = $(e).parent().find(".fileUpload").attr("name");
			//上传控件d
			var fileId = $(e).parent().find(".fileUpload").attr("id");
			//文件类型
			var fileType = $(e).parent().attr("data-fileType");
			
			if (ids !=undefined) {
				//如果为文件则
				if ($(e).parent().attr("data-fileType") =="file") {
					var ar = ids.split(",");
					//获取文件名
					for (var i=0; i<ar.length; i++){
						if(ar[i].indexOf('/')>0) {
							var fname = ar[i].split("/");
							$(e).parent().find(".fileName").prepend('<span id="filexName">'+fname[fname.length-1]+
				       		'<img src="'+http+'/uploadfile/images/close.gif" class="del-filex lpointer" onclick="delFile(this)" style="padding-bottom: 3px;">'+
				       		'</span>');
							
						}
					}
				}
				//如果为图片
				if ($(e).parent().attr("data-fileType") =="image") {
					var ar = ids.split(",");
					//获取文件名
					for (var i=0; i<ar.length; i++){
						if(ar[i].length >0)
						$(e).parent().find(".fileName").prepend('<div style="max-width:200px; position: relative; float:left;" class="xstpd"><img src="'+dataServicePicPath+'/'+ar[i]+'" class="img-rounded" style="max-width:200px; padding-bottom:5px;"/><div style="position: absolute; top:-18px; right:-15px; z-index:9999;"><img src="'+http+'/uploadfile/images/del.png" class="del-img" data-path="'+ar[i]+'" onclick="delFileImg(this)"></div></div>');
					}
					//$(e).parent().find(".fileName").append('<div style="clear:both;"></div>');
				}
			}
				
			//绑定上传事件
			$("#"+fileId).uploadify({
                //指定swf文件
                'swf': http+'/uploadfile/uploadify.swf',
                //后台处理的页面
                'uploader': http+url+"?m="+dataSize+"&t="+fileType,
                'fileObjName':fileName,
                'buttonClass':'btn btn-success btn-height',
                //显示的高度和宽度，默认 height 30；width 120
                //'height': 15,
                'width': 110,
                
                'fileSizeLimit' : dataSize+'KB',
                'buttonText':'选择文件',
                //上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
                //在浏览窗口底部的文件类型下拉菜单中显示的文本
                'fileTypeDesc': 'Image Files',
                //允许上传的文件后缀
                'fileTypeExts': '*.*',
                //发送给后台的其他参数通过formData指定
                //'formData': { 'someKey': 'someValue', 'someOtherKey': 1 },
                //上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
                //'queueID': 'fileQueue',
                //选择文件后自动上传
                'auto': true,
                //设置为true将允许多文件上传
                'multi': true,
                'onUploadProgress' : function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
                	//上传回调事件
                	//$(object).find(".uploadify").css("display","none");
                	//$(object).find(".uploadify-queue").css("display","none");
                	
                	jd = (bytesUploaded/bytesTotal)*100;
                	$(object).find(".progress-bar").css("width",parseInt(jd)-1+"%");
                	$(object).find(".progress-bar").html(parseInt(jd)-1+"%");
                	//上传中 暂停 取消
                	$(object).find(".upzz").css("display","block");
                	//上传进度条
                	$(object).find(".progress").css("display","block");
                	
                	//console.log(parseInt(jd));
                	if(jd==100) {
                		
                	}
		        },
		        
		        'onUploadSuccess': function (file, data, response) {
		        	
		        	
		        	//上传中 暂停 取消
            		$(object).find(".upzz").css("display","none");
            		//上传进度条
            		$(object).find(".progress").css("display","none");
            		$(object).find(".progress-bar").css("width","0%");
            		$(object).find(".progress-bar").html("0%");
            		
            		//上传按钮
            		$(object).find(".uploadify").css("display","block");
            		$(object).find(".uploadify-queue").css("display","block");
		        	
		        	var obj = $.parseJSON(data);
					
					//获取上传成功文件名
					var fileName =obj.filePath;
					
					
					//显示文件名称
					var filehtml="";
					fileName = fileName.substr(fileName.lastIndexOf("/")+1,fileName.length);
					
					if(fileType=="file") {
						filehtml='<span id="filexName">'+fileName+
		        		 '<img src="'+http+'/uploadfile/images/close.gif" class="del-filex lpointer" onclick="delFile(this)" style="padding-bottom: 3px;">'+
		        		 '</span>';
					} else if(fileType=="image"){
						filehtml='<div style="max-width:200px; position: relative; float:left;margin-right:10px;">'+
							'<img src="'+obj.resultMsg+'" class="img-rounded" style="max-width:200px; "/> '+
							'<div class="xstpd" style="position: absolute; top:-18px; right:-15px; z-index:9999;"><img src="'+http+'/uploadfile/images/del.png" class="del-img" data-path="'+obj.resultMsg+'" onclick="delFileImg(this)"></div>'+
							'</div>';
					}
					
					//如果为单文件上传
					
					if (dataMorethan=="false") {
						$(object).parent().find(".files").val(obj.filePath);
						$(object).find(".fileName").html(filehtml);
					} else {
					
						//处理分隔线
						if ($(object).parent().find(".files").val().length<=0) {
							$(object).parent().find(".files").val($(object).parent().find(".files").val()+obj.filePath);
						} else {
							$(object).parent().find(".files").val($(object).parent().find(".files").val()+","+obj.filePath);
						}
						
						$(object).find(".fileName").append(filehtml);
					}
					
					
					
					
		        }
            });
			
		});
            
            
            
});


//删除文件
function delFile(obj){
	var str = $(obj).parents(".filex").find(".files").val();
	var temp = $(obj).parent().text();
	var arr = str.split(",");
	var txt ="";
	for (var i=0; i<arr.length; i++) {
		if (arr[i].indexOf(temp)<=0) {
			txt = txt+arr[i]+",";
		}
	}
	$(obj).parents(".filex").find(".files").val(txt.substr(0,txt.length-1));
	$(obj).parent().remove();
}

//删除文件图片
function delFileImg(obj){
	var str = $(obj).parents(".filex").find(".files").val();
	var temp = $(obj).attr("data-path").substr(3,$(obj).attr("data-path").length);
	var arr = str.split(",");
	var txt ="";
	for (var i=0; i<arr.length; i++) {
		if (arr[i].indexOf(temp)<=0) {
			txt = txt+arr[i]+",";
		}
	}
	$(obj).parents(".filex").find(".files").val(txt.substr(0,txt.length-1));
	
	$(obj).parent().parent().remove();
}