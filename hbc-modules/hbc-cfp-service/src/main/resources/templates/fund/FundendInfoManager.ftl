
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	 <style>
        .dingdan-list{
            padding: 10px;
        }
         .dingdan-list .row{
         	padding-bottom:20px;
         }
    </style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>授信管理</h5>
					</div>
					<div class="ibox-content">
					<div id="xfz">
						<span>已授信：${YCount }人</span>
					</div>
					<div id="xfz1">
						<span>待授信：${DCount }人</span>
					</div>
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="dingdan-list">
								<div class="container">
								   <div class="row">
								   		<div class="form-group col-md-4 col-lg-4 col-sm-4 form-inline">
									      <label class="control-label">用户名称:</label>
										  <input type="text" name="name" placeholder="请输入用户名称" id="name" class="form-control">
								        </div>
								
								        <div class="form-group col-md-4 col-lg-4 col-sm-4">
								    	  <label class="control-label">授信时间:</label>
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:30%" maxlength="30" placeholder="">
											-
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:30%" maxlength="30" placeholder="">
										</div>
										<div class="col-md-3 col-lg-3 col-sm-3">
									      <label class="control-label">绑卡情况:</label>
										    <select id=card name="card">
											  <option value="">全部</option>
										 	  <option value="0">未绑卡</option>
											  <option value="1">已绑卡</option>
										    </select>
								        </div>
								        <div class="form-group col-lg-1 col-md-1 col-sm-1" style="text-align:center">
										   <input type="button" id="search" class="btn btn-warning" value="搜索" />
									    </div>
									</div>
									<div class="row">
										<div class="form-group col-md-4 col-lg-4 col-sm-4">
									       <label class="control-label">账户状态:</label>
										   <select id=authStatus name="authStatus">
											<option value="">全部</option>
											<option value="1">已授信</option>
											<option value="0">未授信</option>
										   </select>
									    </div>
								   </div>
								 </div>  
							</div>
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
   		//$("#oid-span").operaSelect(function(){});
   		$("#parentId").select();
   		$("#rzStatus").select();
   		$("#authStatus").select();
   		$("#card").select();
   		$("#status").select();
   		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		
		var $pager = $("#data").pager({
			url:"${hsj}/fundend/fundendManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
						    '<th>账户姓名</th>'+
						    '<th>手机号码</th>'+
							'<th>所属运营商</th>'+
							'<th>账户状态</th>'+
							'<th>绑卡情况</th>'+
							'<th>授信金额</th>'+
							'<th>授信时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
				            '<td>{realName}</td>'+
				            '<td>{mobile}</td>'+
							'<td>{operaName}</td>'+
							 '<td id="authStatus_{id}"></td>'+
							'<td id="card_{id}"></td>'+
							'<td>{authMax}</td>'+
							'<td>{authDate}</td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id},{realName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					
					var $btn;
					if(list[i].isFreeze || list[i].isFreeze == "true"){
						$("#authStatus_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>冻结</span>");
						$btn = $('<a href="javascript:void(0);" data-id="'+list[i].aid+'" class="btn btn-primary btn-sm lk"><i class="fa fa-unlock"></i> <span>启用</span> </a>');
					}else{
						if(list[i].authStatus || list[i].authStatus == "true")
							$("#authStatus_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>已授信</span>");
						else
							$("#authStatus_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>未授信 </span>");
						$btn =  $('<a href="javascript:void(0);" data-id="'+list[i].aid+'" class="btn btn-primary btn-sm lk"><i class="fa fa-lock"></i> <span>冻结</span> </a>');
					}
					if(list[i].bankCard == null || list[i].bankCard == "")
						$("#card_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>未绑卡</span>");
					else
						$("#card_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>已绑卡</span>");
					
					
					var authExamine = list[i].authExamine;
					
					$btn.bind("click", function(){
						var isFreeze = "";
						if($(this).find("i").hasClass("fa-lock")){
							$(this).find("i").removeClass("fa-lock");
							$(this).find("i").addClass("fa-unlock");
							$(this).find("i").parent().find("span").html("启用");
							isFreeze = "true";
						}else{
							$(this).find("i").removeClass("fa-unlock");
							$(this).find("i").addClass("fa-lock");
							$(this).find("i").parent().find("span").html("冻结");
							isFreeze = "false";
						}
						var id =$(this).attr("data-id");
						$.post('<%=path%>/userInfo/updateFreezeStatus.htm',{isFreeze:isFreeze,id:id},function(r){
							parent.messageModel(r.resultMsg);
							if(r.resultCode=="1"){
								$pager.gotoPage($pager.pageNumber);
							}
						},"json");
					});
					/* if(authExamine == 3 || (list[i].authStatus || list[i].authStatus == "true")){
						$("#oper_"+list[i].id).append($btn);
					}
					if(authExamine ==  2){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('授信审核','${hsj}/fundend/showApp.htm?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list[i].id).append($examine);
					} */
					
					if(authExamine == 4 || authExamine == 5 || (list[i].authStatus || list[i].authStatus == "true")){
						$("#oper_"+list[i].id).append($btn);
					}
					if(authExamine == 4){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>提额</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('提额审核','${hsj}/fundend/showApp.htm?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list[i].id).append($examine);
					}
					if(authExamine == 6){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>提额未通过</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('提额审核','${hsj}/fundend/showApp.htm?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list[i].id).append($examine);
					}
				}
				
				//查看
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('消费者详情-'+title,'${hsj}/fundend/showApply.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
	});
	
</script>

	</body>
</html>

