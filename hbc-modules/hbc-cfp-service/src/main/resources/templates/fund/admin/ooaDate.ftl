<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.css" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>出账日和还款日</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>

                                 <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script>

    </script>
</body>

</html>
