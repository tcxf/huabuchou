<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
		 	<c:if test="${c==null }">
				<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
		 	</c:if>
			<input class="btn btn-white cancel" id="cancel" type="button" value="返回" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox-title">
				<h5>现金贷产品</h5>
			</div>
			<div class="col-sm-12">
				<div class="tabs-container">
				<form id="_f" method="post" class="form-horizontal">
					<div class="tab-content">
							<div class="hr-line-dashed" style="clear:both"></div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">是否开启 :</label>
									<div class="col-sm-8">
										<label><input name="enabled" <c:if test="${c.enabled==1 }"> checked="checked"</c:if> type="radio" value="1" />开启现金贷 </label> 
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">可借金额 :</label>
									<div class="col-sm-8">
										<input type='text' validate-msg="required:请填写最小金额,number:金额只能输入数字"; validate-rule="required,ffstring" validate-msg="required:最小金额不能为空,maxLength:最小金额3位数" id="moneyMini" name="moneyMini" value="${c.moneyMini}" maxlength="15" placeholder="最小金额" />
										<input type='text'  validate-rule="required,maxLength[6],ffstring"; validate-msg="required:最大金额不能为空,maxLength:最大金额7位数,number:金额只能输入数字" id="moneyMax" name="moneyMax" value="${c.moneyMax}" maxlength="15" placeholder="最大金额" />
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">借款期限 :</label>
									<div class="col-sm-8">
											<div >
												<label><input type="checkbox" name="time" value="0" ID="Checkbox0">1个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="1" ID="Checkbox1">3个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="2" ID="Checkbox2">6个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="3" ID="Checkbox3">9个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="4" ID="Checkbox4">12个月</label>&nbsp&nbsp
												
											</div>
											<br />
											<div >
												<label><input type="checkbox" name="time" value="5" ID="Checkbox5">15个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="6" ID="Checkbox6">18个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="7" ID="Checkbox7">24个月</label>&nbsp&nbsp
												<label><input type="checkbox" name="time" value="8" ID="Checkbox8">36个月</label>&nbsp&nbsp
											</div>
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">分期利息 :</label>
									<div class="col-sm-8">
										<input type='text' value="${c.interest}" validate-msg="required:请填写分期利息,number:分期利息只能输入数字" validate-rule="required,ffstring" validate-msg="required:最大金额不能为空,maxLength:最大金额7位数,number:金额只能输入数字"   id="interest" name="interest"  placeholder="利息" />%每月
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">分期方式 :</label>
									<div class="col-sm-8">
											<label><input id="stagesType" name="stagesType" <c:if test="${c.stagesType==1 }"> checked="checked"</c:if> type="radio" value="1" />等额本金 </label> 
											<label><input id="stagesType1" name="stagesType" <c:if test="${c.stagesType==0 }"> checked="checked"</c:if> type="radio" value="0" />等额本息 </label> 
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">逾期利息 :</label>
									<div class="col-sm-8">
										<input type='text'  id="overdue" name="overdue" value="${c.overdue}" value="${c.interest}" validate-msg="required:请填写逾期利息,number:逾期利息只能输入数字"  validate-rule="required,ffstring" placeholder="利息" />%每月
									</div>
									
									<div class="hr-line-dashed" style="clear:both;"></div>
									
									<label class="col-sm-4 control-label">申请条件 :</label>
									<div class="col-sm-8">
											<label><input name="apply" type="checkbox" value="0" checked="checked"/>基本资料 </label> 
											<label><input name="apply" type="checkbox" value="1" checked="checked"/>工作信息 </label> 
											<label><input name="apply" type="checkbox" value="2" checked="checked"/>紧急联系人 </label> 
									</div>
								</div>
					</div>
				</div>
			</form>
		</div>
 	</div>
 </div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript">
	    var a="${c.time}";
	    var c=a.split(",");
	    for(var i=0;i<=8;i++)
	    {
			var flag=document.getElementById("Checkbox"+i).value;
			for(var j=0;j<c.length;j++){ 
				if(c[j]==flag)  
			{
			  document.getElementById("Checkbox"+i).checked=true;
			}
		  
		} 
	   }
   </script>
	<script>
		$(document).ready(function () {
			$(".cancel").click(function(){parent.closeLayer();});
			$(".submit-save").click(function (){
				if(!$("#Checkbox0").is(':checked') && !$("#Checkbox1").is(':checked') && !$("#Checkbox2").is(':checked') && !$("#Checkbox3").is(':checked') &&
				   !$("#Checkbox4").is(':checked') && !$("#Checkbox5").is(':checked') && !$("#Checkbox6").is(':checked') &&!$("#Checkbox7").is(':checked') && !$("#Checkbox8").is(':checked') ){
					parent.confirm("请选择借款期限",function(){});
				}else if(!$("#stagesType").is(':checked') && !$("#stagesType1").is(':checked')) {
					parent.confirm("请选择分期方式",function(){});
				}else{
					var $this = $(this);
					if(!$("#_f").validate()) return;
					$this.html("保 存 中");
					$this.attr("disabled", true);
					var index = layer.load(2, {time: 10*1000});
					$.ajax({
						url:'${hsj}/cashloan/<c:choose><c:when test="${id == null}">cashfoAddAjaxSubmit</c:when><c:otherwise>cashfoAddAjaxSubmit</c:otherwise></c:choose>.htm',
						type:'post', //数据发送方式
						dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
						data:$("#_f").serialize(),
						success: function(data){ //成功
							layer.close(index);
							var obj = data;
							if (obj.resultCode == "-1") {
								parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
								$this.html("保存内容");
								$this.attr("disabled", false);
							}
							
							if (obj.resultCode == "1") {
								//消息对话框
								parent.messageModel("保存成功!");
								parent.c.gotoPage(null);
								parent.closeLayer();
							}
						}
					});
				}
				
			});
		}); 
	</script>
</body>

</html>
