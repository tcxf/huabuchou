<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>现金贷产品</h5>
						<form id="searchForm" class="form-horizontal">
						</form>
						<div class="ibox-tools">
								<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 添加产品</a>
						</div>
					</div>
				</div>
				<div class="project-list pager-list" id="data"></div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		var $pager = $("#data").pager({
			url:"${hsj}/cashloan/cashloanbList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>可借最小金额</th>'+
							'<th>最大金额</th>'+
							'<th>借款期限</th>'+
							'<th>分期利息</th>'+
							'<th>分期方式</th>'+
							'<th>逾期利率</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td id="rr">{moneyMini}</td>'+
							'<td>{moneyMax}</td>'+
							'<td>{timeStr}</td>'+
							'<td>{interest}%</td>'+
							'<td id="stagesType_{id}">{stagesType}</td>'+
							'<td>{overdue}%</td>'+
							'<td>'+
							'<a id="enabled_{id}"  href="javascript:void(0);" data="{enabled}" class="btn btn-success btn-sm hk"><i class="fa fa-edit"></i> </a>'+
							' <a href="javascript:void(0);" data="{id},{userName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 查看</a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					var type = list[i].stagesType;
					if(type == 0){
						$("#stagesType_"+list[i].id).html("等额本息");
					}else if(type == 1){
						$("#stagesType_"+list[i].id).html("等额本金");
					}
					var enabled = list[i].enabled;
					if(enabled == 1){
						$("#enabled_"+list[i].id).html("冻结");
					}else if(enabled == 0){
						$("#enabled_"+list[i].id).html("开启");
					}
					
				}
				$(".hk").click(function(){
					var enabled =$(this).attr("data");
					if(enabled==1){
						parent.confirm("是否冻结现金贷产品？",function(){
							$.post("<%=path%>/cashloan/doRepayment.htm",{enabled:enabled},function(d){
								parent.messageModel(d.resultMsg);
								$pager.gotoPage($pager.pageNumber);
							},"json");
						});
					}else if(enabled==0){
						parent.confirm("是否开启现金贷产品？",function(){
							$.post("<%=path%>/cashloan/doRepayment.htm",{enabled:enabled},function(d){
								parent.messageModel(d.resultMsg);
								$pager.gotoPage($pager.pageNumber);
							},"json");
						});
						
					}
				});
				//编辑页面
				$(".edit").click(function (){
					parent.openLayer('现金贷产品详情','${hsj}/cashloan/cashloanInfoIput.htm',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				
				
			}
		});
		//打开新增页面
		$("#new").click(function (){
			if($("#rr").html()==null){
				parent.openLayer('添加现金贷产品','${hsj}/cashloan/cashloanInfoIput.htm','1000px','700px',$pager);
			}else{
				parent.confirm("不能重复添加现金贷产品",function(){});
				
			}
		});
		
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	
	});
	
</script>


	</body>
</html>
