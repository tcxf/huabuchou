<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div></div>
	</div>
	<form id="_f">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#xq" aria-expanded="true"> 详情</a></li>
				</ul>
				<div class="tab-content">
						<div class="hr-line-dashed" style="clear:both;"></div>
						
					<div id="xq" class="ibox-content active tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">本期应还本金</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${plan.currentCorpus}元
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">利息</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${plan.currentFee}元
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">滞纳金</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${plan.lateFeeAct}元
								</label>
							</div>
							<div class="form-group col-sm-6">
								<c:if test="${plan.repaymentType==null}">
									<label class="col-sm-4 control-label">本期应还</label>
									<label class="col-sm-8 control-label" id="yh" style="text-align: left;">
									${plan.totalAmount }元
									</label>
								</c:if>
								<c:if test="${plan.repaymentType!=null}">
									<label class="col-sm-4 control-label">已还金额</label>
									<label class="col-sm-8 control-label" id="yh" style="text-align: left;">
									${plan.totalAmount }元
									</label>
								</c:if>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<c:if test="${plan.repaymentType==null}">
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">待还款时间</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										${plan.repaymentDateStr}
									</label>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">还款期数</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										${plan.currentTime}/${plan.totalTime}期
									</label>
								</div>
							</div>
						</c:if>
						<c:if test="${plan.repaymentType!=null}">
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">还款时间</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										${plan.processDateStr}
									</label>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">还款方式</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										${plan.repaymentTypeStr}
									</label>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
			$(".cancel").click(function(){parent.closeLayer();});
			function findfund(){
				$.ajax({
					url:"${hsj}/userInfo/findfund.htm",
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:{
						
					},
					success: function(d){ 
						  $("#hStatus").html("");
							for(var i = 0 ; i < d.data.length ; i++){
								
								$tr =$('<option value='+d.data[i].id+'>'+d.data[i].fname+'</option>')	;	
							
							$("#hStatus").append($tr);
						  }
			}
					}); 
	
			}
		});
	</script>
</body>

</html>
