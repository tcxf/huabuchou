<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white doExamine" id="doExamine" type="button" value="审核" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
		</div>
	</div>
	<form id="_f">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
					<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>
					<li><a data-toggle="tab" href="#company" aria-expanded="true"> 公司信息</a></li>
					<li><a data-toggle="tab" href="#owner" aria-expanded="true"> 产权信息</a></li>
					<li><a data-toggle="tab" href="#jk" aria-expanded="true"> 借款信息</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="process" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${id}"/>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">审核结果</label>
								<div class="col-sm-8">
									<select class="dfinput" id="ret" style="width:100px;">
										<option value="pass" <c:if test="${centity.status == 3}">selected</c:if>>审核通过</option>
										<option value="nopass" <c:if test="${centity.status == 5}">selected</c:if>>审核拒绝</option>
									</select>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						<div class="nopass nopass process" style="display:none;">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">原因</label>
								<div class="col-sm-10">
									<textarea id="cause" rows="10" style="width:80%"></textarea>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed nopass process" style="clear:both;display:none;"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="base" class="ibox-content tab-pane">
					<input type="hidden" name="id" value="${id}"/>
							<c:if test="${id != null}">
						  	<div class="form-group">
								<label class="col-sm-2 control-label">账户id</label>
								<label id="unum" class="col-sm-10 control-label" style="text-align: left;">
									${entity.unum}
								</label>
							</div>
							</c:if>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">账户姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.realName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">身份照号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">开户行</label>
								<div class="col-sm-8" id="bank" input-name="bankSn" input-val="${entity.bankSn}"></div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">银行卡</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.bankCard}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机卡密码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.deMobilePwd}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">紧急联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">紧急联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">所属地区</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.displayName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">婚姻状况</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${id != null && entity.isMarry}">已婚</c:if>
									<c:if test="${id == null || !entity.isMarry}">未婚</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<div id="company" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyTel}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位性质</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyType}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">工作时长</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.workTimeStr}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">职位</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyPosition}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">薪资</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								<c:if test="${entity.wages =='1'}">2000元以下</c:if>
									<c:if test="${entity.wages =='2'}">2000-3000元</c:if>
									<c:if test="${entity.wages =='3'}">3000-4500元</c:if>
									<c:if test="${entity.wages =='4'}">4500-6000元</c:if>
									<c:if test="${entity.wages =='5'}">6000-8000元</c:if>
									<c:if test="${entity.wages =='6'}">8000-10000元</c:if>
									<c:if test="${entity.wages =='7'}">10000-15000元</c:if>
									<c:if test="${entity.wages =='8'}">15000-20000元</c:if>
									<c:if test="${entity.wages =='9'}">20000元以上</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyDisplayName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyAddress}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<!--借款信息  -->
						<div id="jk" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">借款人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.realName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">借款金额</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${centity.fmoney}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">借款期限</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${centity.periods}个月
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">借款利息</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${centity.settlement}%
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">还款方式</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${centity.stagesType =='1'}">等额本金</c:if>
									<c:if test="${centity.stagesType =='0'}">等额本息</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">申请时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${centity.applyDateStr}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">借款时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
									${centity.createDateStr}
								</label>
							</div>
						</div>
					</div>
					
					
					<div id="owner" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车牌号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.carNumber}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.drivingLicense}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">驾驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.driverLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车辆归属人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">${entity.carRelationship}</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房产证编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房屋归属人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseBelong}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseRelate}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</div>
	</form>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
		$(document).ready(function () {
			findfund();
			$("#ret").select();
			var ret = $("#ret").val();
			if(ret == 'nopass'){
				$(".process").show(); 
				$("#cause").html("");
			
				ret = $(this).val();
			}
			$("#ret").change(function(){
				$(".process").hide(); 
				$("."+$(this).val()).show();
				$("#cause").html("");
			
				ret = $(this).val();
			});
			
			$("#bank").initBank();
			$("#ooaDate").select();
			$("#repayDate").select();
			$("#hStatus").trigger("change");
			$("#front").css('width','100%');
			$("#front").css('height',$(document).width()/6);
			$("#front").click(function(){
				window.open($(this).attr('src'));
			})
			$("#back").css('width','100%');
			$("#back").css('height',$(document).width()/6);
			$("#back").click(function(){
				window.open($(this).attr('src'));
			});
			$(".doExamine").click(function(){
				if(!$("#_f").validate()) return;
				var data = {};
				if(!$("#cause").is(":hidden")){
					var cause = $.trim($("#cause").val());
					if(cause.length < 10){
						layer.alert('失败原因至少需要10个字');
						$("#cause").focus();
						return;
					}
					if(cause.length > 100){
						layer.alert('失败原因最多只能100个字');
						$("#cause").focus();
						return;
					}
					if(!isNaN(cause)){
						layer.alert('失败原因不能为数字');
						$("#cause").focus();
						return;
					}
					var regex = /^[\u4E00-\u9FA5\uF900-\uFA2D\x20，。？…!,.;'\[\]{}~!@#$%^&*()_+|\-\\~！@#￥%……&*（）——+|、]+$/;
					if(!regex.test(cause)){
						layer.alert('失败原因不能包含特殊字符');
						$("#cause").focus();
						return;
					}
					data.cause = cause;
				}
				data.ret = ret;
				data.id = '${id}';
				$.post('${hsj}/cashloanForUser/doExamine.htm',data,function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != -1){
						parent.closeLayer();
						parent.c.gotoPage(null);
					}
				},'json');
			});
			
			$(".cancel").click(function(){parent.closeLayer();});
			
			function findfund(){
				$.ajax({
					url:"${hsj}/userInfo/findfund.htm",
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:{
						
					},
					success: function(d){ 
						  $("#hStatus").html("");
							for(var i = 0 ; i < d.data.length ; i++){
								
								$tr =$('<option value='+d.data[i].id+'>'+d.data[i].fname+'</option>')	;	
							
							$("#hStatus").append($tr);
						  }
			}
					}); 
	
			}
		});
	</script>
</body>

</html>
