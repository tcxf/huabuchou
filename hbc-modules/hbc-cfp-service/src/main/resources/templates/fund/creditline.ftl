<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

    <title>授信额度管理</title>
    <style>
        .sx_gl{
            margin-top: 20px;
            width: 94%;
            margin-left: 3%;
            border: 1px solid #ededed;
            height: 700px;
        }
        .sx_one{
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #f5f7f7;
        }
        .sx_one .col-sm-8{
            text-align: right;
        }
        .one_menu{
            padding:30px 20px;
        }
        .one_menu .col-sm-2>a{
            text-decoration: none;
            padding: 5px 10px;
            background: #7266bb;
            color: #fff;
            border-radius: 3px;
        }
        .one_menu .col-sm-2:last-child{
            padding: 0;
            text-align: left;
        }
        .sx_content{
            position: relative;
            display: none;
        }
        .tow_sx{
            margin-top: 60px;
        }
        .tow_sx .col-sm-3>.col-sm-2{
            padding-right: 0;
            width: 10%;
        }
        .tow_sx .col-sm-3>.col-sm-10{
            padding-left: 0;
            font-weight: 600;
        }
        .red{
            width: 6px;
            height: 20px;
            background: #f15363;
        }
        .blue{
            width: 6px;
            height: 20px;
            background: #23c7ca;
        }
        .sx_money{
            margin-top: 40px;
            padding-left: 10px;
        }
        .sx_money>h3{
            font-size: 30px;
            font-weight: 600;
        }
        .tishi_foot>ul{
            padding: 0;
            margin: 0;
        }
        .tishi_foot li{
            list-style: none;
            float: right;
            margin-left: 5px;
        }
        #kq{
            text-align: right;
        }
        .no_pass{
            margin-top: 20%;
            display: none;
            text-align: center;
        }
        .no_pass>img{
            width: 100px;
            height: 100px;
        }
        .no_pass p{
            font-size: 20px;
            margin-top: 10px;
        }
    </style>
</head>
<body>
<div class="sx_gl">
    <div class="sx_one">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <b>授信额度管理</b>
                </div>
                <div class="col-sm-8">
                    <div id="kq">
                            <select name="status" id="status" value="${fmoenyinfo.status!}">
                                <option value="1" <#if (((fmoenyinfo.status)!'') == '1')>selected="selected"</#if> >开启授信限额</option>
                                <option value="0" <#if (((fmoenyinfo.status)!'') == '0')>selected="selected"</#if> >关闭授信限额</option>
                            </select>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="no_pass">
        <img src="${basePath!}/img/sx_icon_sb_gruy.png" alt="">
        <p>授信限额未开启</p>
    </div>
    <div class="sx_content">
        <div class="one_menu">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 zong_e">
                        <a href="${basePath!}/fund/creditfundmanagement/settingupcreditfunds">设置授信资金池总额</a>
                    </div>
                    <div class="col-sm-2 xian_e">
                        <a href="${basePath!}/fund/creditfundmanagement/settingupcreditfundsday">设置每日授信限额</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tow_sx">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <div class="red"></div>
                        </div>
                        <div class="col-sm-10">
                            今日已授信总额(含提额)
                        </div>
                        <div class="sx_money">
                            <h3 id="DaySummoney"></h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <div class="blue"></div>
                        </div>
                        <div class="col-sm-10">
                            今日授信余额
                        </div>
                        <div class="sx_money">
                            <h3>￥${b2!}元</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tow_sx">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <div class="red"></div>
                        </div>
                        <div class="col-sm-10">
                            累计已授信总额(含提额)
                        </div>
                        <div class="sx_money">
                            <h3 id="LJSummoney"></h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <div class="blue"></div>
                        </div>
                        <div class="col-sm-10">
                            授信资金池余额
                        </div>
                        <div class="sx_money">
                            <h3>￥${LJS!}元</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<!-- iCheck -->
<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script>
    $(document).ready(function (){
        $("select").select();
        $("#status").change(function () {
            //开启授信状态
            var status = $("#status").val();

            layer.confirm('确定'+(status==1?'开启"授信限额"开关':'关闭"授信限额"开关')+'吗？', {icon: 3, title:'提示',yes: function(index){
                    $.ajax({
                        url : "${basePath!}/fund/creditfundmanagement/insertStatus?status="+status,
                        type : "POST",
                        success:function(data) {
                            window.location.reload();
                        }
                    });
                    layer.close(index);
                },
                cancel: function(index, layero){
                    layer.close(index);
                    location.reload();// 可以在这里刷新窗口
                }
            });
        })

        var ret=${fmoenyinfo.status!};
            if(ret==0){
                $(".sx_content").css("display","none");
                $(".no_pass").css("display","block");
            }
            if(ret==1){
                $(".sx_content").css("display","block");
            }
            if(ret==null || ret == ""){
                $(".sx_content").css("display","none");
                $(".no_pass").css("display","block");
            }

            //今日授信总额
            $.ajax({
                url : "${basePath!}/fund/creditfundmanagement/DaySummoney",
                type : "POST",
                dataType : "json",
                success : function(d){
                    console.log(d);
                    $("#DaySummoney").html("￥"+d.data +"元");
                }
            });

            //累计授信总额
            $.ajax({
                url : "${basePath!}/fund/creditfundmanagement/LJSummoney",
                type : "POST",
                dataType : "json",
                success : function(d){
                    console.log(d);
                    $("#LJSummoney").html("￥"+d.data+"元");
                }
            });

    });

</script>
</body>
</html>