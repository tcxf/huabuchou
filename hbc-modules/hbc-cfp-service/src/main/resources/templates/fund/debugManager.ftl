<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 交易记录调试</a></li>
					<li><a data-toggle="tab" href="#repayment" aria-expanded="true"> 账单调试</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 结算单调试</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>交易明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">交易时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户名称</label>
											<div class="col-sm-9">
												<input type="text" id="shname" class="form-control" name="manme" maxlength="30" placeholder="请填写商户名称">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">用户名称</label>
											<div class="col-sm-9">
												<input type="text" id="yhname" class="form-control" name="uname" maxlength="30" placeholder="请填写用户名称">
											</div>
										</div>
										
		<!-- 								<div class="form-group col-sm-6 col-lg-4"> -->
		<!-- 									<label class="col-sm-3 control-label">交易状态</label> -->
		<!-- 									<div class="col-sm-9"> -->
		<!-- 										<select id="tradeType" name="tradeType"> -->
		<!-- 											<option value="">全部</option> -->
		<!-- 											<option value="1">未支付</option> -->
		<!-- 											<option value="2">已支付</option> -->
		<!-- 											<option value="3">已退款</option> -->
		<!-- 											<option value="4">退款中</option> -->
		<!-- 										</select> -->
		<!-- 									</div> -->
		<!-- 								</div> -->
									</div>
									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
											<input id="ccs" class="btn btn-info" type="button" value=" 下 载 报 表 " />
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data"></div>
							</div>
						</div>
					</div>
					<div id="repayment" class="ibox-content tab-pane">
						<div class="ibox-title">
							<h5>生成账单</h5>
						</div>
						<div class="ibox-content form-horizontal">
							<div class=" col-sm-12">
	                            <div class="form-group col-sm-6">
	                                <label class="col-sm-4 control-label">选则出账日</label>
	                                <div class="col-sm-8">
		                                <input type="text" class="form-control" name="outdate" id="outdate" style="width:100%" maxlength="30" placeholder="">
	                                </div>
	                            </div>
	                            <div class="form-group col-sm-6">
	                                <label class="col-sm-4 control-label"></label>
	                                <div class="col-sm-8">
		                                 <input class="btn btn-primary" id="create-repay" type="button" value="生成账单" />
	                                </div>
	                            </div>
							</div>
							
							<div class="ibox-content">
								<form id="repayForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">消费者名称</label>
											<div class="col-sm-9">
												<input type="text" id="shname" class="form-control" name="uname" maxlength="30" placeholder="请填写消费者名称">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">消费者id</label>
											<div class="col-sm-9">
												<input type="text" id="yhname" class="form-control" name="unum" maxlength="30" placeholder="请填写消费者id">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">消费者手机</label>
											<div class="col-sm-9">
												<input type="text" id="yhname" class="form-control" name="mobile" maxlength="30" placeholder="请填写消费者手机">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">所属运营商</label>
											<div class="col-sm-9">
												<span id="oid-span" ele-id="oid" ele-name="oid"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="repaydate-search" type="button" value=" 查 询 " />
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="repay-data" style="padding:20px;"></div>
							</div>
							
                        </div>
					</div>
					<div id="settlement" class="ibox-content tab-pane">
						<div class="ibox-title">
								<h5>生成结算单</h5>
							</div>
							<div class="ibox-content form-horizontal">
								<div class=" col-sm-12">
		                            <div class="form-group col-sm-6">
		                                <label class="col-sm-4 control-label">选则结账日</label>
		                                <form id="Formts" class="form-horizontal">
		                                <div class="col-sm-8">
			                                <input type="text" class="form-control" name="sdate" id="sdate" style="width:100%" maxlength="30" placeholder="">
		                                </div>
		                                </form>
		                            </div>
		                            <div class="form-group col-sm-6">
		                                <label class="col-sm-4 control-label"></label>
		                                <div class="col-sm-8">
			                                 <input class="btn btn-primary" id="create-settlement" type="button" value="生成结算单" />
		                                </div>
		                                 
		                            </div>
		                           
								</div>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data_ts" style="padding:20px;"></div>
	                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#tradeType").select();
		var $pager = $("#data").pager({
	
			url:"${hsj}/debug/debugManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>所属运营商</th>'+
							'<th>商户id</th>'+
							'<th>商户名称</th>'+
							'<th>行业类型</th>'+
							'<th>用户id</th>'+
							'<th>用户名称</th>'+
							'<th>交易类型</th>'+
							'<th>交易时间</th>'+
							'<th>交易金额</th>'+
							'<th>平台让利</th>'+
							'<th>商家让利</th>'+
							'<th>结算金额</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{operaName}</td>'+
							'<td>{mnum}</td>'+
							'<td>{mname}</td>'+
							'<td>{mcname}</td>'+
							'<td>{unum}</td>'+
							'<td>{uname}</td>'+
							'<td>{tradingTypeStr}</td>'+
							'<td class="edit" data="{id}" data-val="{tradingDateStr}">{tradingDateStr}</td>'+
							'<td>{amount}</td>'+
							'<td class="platform" data="{id}" data-val="{platformShareFee}">{platformShareFee}</td>'+
							'<td class="shops" data="{id}" data-val="{shareFee}">{shareFee}</td>'+
							'<td>{settlementAmount}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				
				$(".edit").click(function(){
					if($(this).hasClass("ee")) return;
					var id = randomString(32);
					$(this).addClass("ee");
					$input  = $('<div class="form-group"><input id="'+id+'" readonly type="text" class="form-control" value="'+$(this).attr("data-val")+'" maxlength="30" placeholder="请填写变更后的交易时间" /></div>');
					$div = $('<div class="form-group"></div>')
					$sure  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-info btn-sm "><i class="fa fa-edit"></i> <span>保存</span> </a></div>');
					$cancel  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-danger btn-sm "><i class="fa fa-remove"></i> 取消 </a></div>');
					$(this).empty()
					$(this).append($input);
					$div.append($sure);
					$div.append($cancel);
					$(this).append($div);
					setTimeout(function(){
						laydate({
							elem: '#'+id, 
							event: 'click', //触发事件
							istime: true,
						  	format: 'YYYY-MM-DD hh:mm:ss' //日期格式
						});
					},100);
					$cancel.click(function(){
						var topdiv = $(this).parent().parent();
						$(this).parent().parent().html($(this).parent().parent().attr("data-val"));
						setTimeout(function(){
							topdiv.removeClass("ee");
						},100);
					});
					$sure.click(function(){
						$this = $(this);
						if($this.hasClass("disabled")) return;
						$this.addClass("disabled");
						var id = $this.parent().parent().attr("data");
						$this.find("span").html("处理中");
						$.post("<%=path%>/debug/updateTime.htm",{id:id,time:$this.parent().parent().find("input").val()},function(d){
							parent.messageModel(d.resultMsg);
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}
							$this.removeClass("disabled");
							$this.find("span").html("保存");
						},"json");
					});
				});
				$(".platform").click(function(){
					if($(this).hasClass("ee")) return;
					var id = randomString(32);
					$(this).addClass("ee");
					$input  = $('<div class="form-group"><input id="'+id+'" type="text" class="form-control" value="'+$(this).attr("data-val")+'"  placeholder="请填写变更后的结算金额" /></div>');
					$div = $('<div class="form-group"></div>');
					$sure  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-info btn-sm "><i class="fa fa-edit"></i> <span>保存</span> </a></div>');
					$cancel  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-danger btn-sm "><i class="fa fa-remove"></i> 取消 </a></div>');
					$(this).empty()
					$(this).append($input);
					$div.append($sure);
					$div.append($cancel);
					$(this).append($div);
					
					$cancel.click(function(){
						var topdiv = $(this).parent().parent();
						$(this).parent().parent().html($(this).parent().parent().attr("data-val"));
						setTimeout(function(){
							topdiv.removeClass("ee");
						},100);
					});
					$sure.click(function(){
						var money = $(this).parent().parent().find("input").val();
						var id = $(this).parent().parent().attr("data");
						$.ajax({
							url:"${hsj}/debug/updatePlatform.htm",
							type:"post",
							data:"money="+money+"&id="+id,
							success:function(d){
								
								if(d.resultCode ==1){
									parent.messageModel("修改成功");
								$pager.gotoPage($pager.pageNumber);
								}else{
									parent.messageModel(d.resultMsg);
								}		
							},dataType:"json"
						});
					});
				});
				$(".shops").click(function(){
					if($(this).hasClass("ee")) return;
					var id = randomString(32);
					$(this).addClass("ee");
					$input  = $('<div class="form-group"><input id="'+id+'" type="text" class="form-control" value="'+$(this).attr("data-val")+'"  placeholder="请填写变更后的结算金额" /></div>');
					$div = $('<div class="form-group"></div>');
					$sure  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-info btn-sm "><i class="fa fa-edit"></i> <span>保存</span> </a></div>');
					$cancel  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-danger btn-sm "><i class="fa fa-remove"></i> 取消 </a></div>');
					$(this).empty()
					$(this).append($input);
					$div.append($sure);
					$div.append($cancel);
					$(this).append($div);
					
					$cancel.click(function(){
						var topdiv = $(this).parent().parent();
						$(this).parent().parent().html($(this).parent().parent().attr("data-val"));
						setTimeout(function(){
							topdiv.removeClass("ee");
						},100);
					});
					$sure.click(function(){
						var money = $(this).parent().parent().find("input").val();
						var id = $(this).parent().parent().attr("data");
						$.ajax({
							url:"${hsj}/debug/updateShops.htm",
							type:"post",
							data:"money="+money+"&id="+id,
							success:function(d){
								
								if(d.resultCode ==1){
									parent.messageModel("修改成功");
								$pager.gotoPage($pager.pageNumber);
								}else{
									parent.messageModel(d.resultMsg);
								}		
							},dataType:"json"
						});
					});
				});
				
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
		
		
		
		/***********************账单调试选项卡下的所有js事件  start*******************************/
		//账单调试---选项卡，查询按钮事件
		$("#repaydate-search").click(function(){
			//跳转到第一页
			$pager_repay.gotoPage(1);
		});
		
		//加载账单调试选项卡下的账单列表
		var $pager_repay = $("#repay-data").pager({
			
			url:"${hsj}/debug/repaymentDetailManagerList.htm",
			formId:"repayForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>业务单号</th>'+
							'<th>所属运营商</th>'+
							'<th>消费者名称</th>'+
							'<th>消费者id</th>'+
							'<th>消费者手机号</th>'+
							'<th>账单金额</th>'+
							'<th>是否分期</th>'+
							'<th>还款计划</th>'+
							'<th>最近还款日</th>'+
							'<th>账单周期</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{repaymentSn}</td>'+
							'<td>{operaName}</td>'+
							'<td>{uname}</td>'+
							'<td>{unum}</td>'+
							'<td>{mobile}</td>'+
							'<td>{totalCorpus}</td>'+
							'<td id="split_{id}"></td>'+
							'<td>{currentTime}/{totalTime}</td>'+
							'<td>{repayDate}</td>'+
							'<td>{loopStartStr} - {loopEndStr}</td>'+
							'<td>'+
							'<span id="split2_{id}"></span> '+
							'<a href="javascript:void(0);" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> <span>删除</span> </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="11"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					d = list[i];
					if(d.isSplit == "true" || d.isSplit){
						$("#split_"+d.id).html("已分期");
					}else{
						$("#split_"+d.id).html("未分期");
						$("#split2_"+d.id).html('<a href="javascript:void(0);" data-id="'+d.id+'" class="btn btn-success btn-sm split"><i class="fa fa-edit"></i> <span>分期</span> </a>');
					}
				}
				
				
				$("#repay-data").find(".remove").click(function(){//给删除按钮添加点击事件
					if($(this).hasClass("disabled")) return;
					$this = $(this);
					$this.addClass("disabled"); //给当前按钮添加禁用的属性，防止重复点击
					var id = $this.attr("data-id");
					l = parent.layer.confirm('您确认要删除该帐单吗?', {
						btn: ['确定','取消'] //按钮
					}, function(){
						parent.layer.close(l); //移除confirm询问框
						$this.find("span").html("处理中"); //更改删除按钮的文字
						$this.attr("disabled"); //禁用删除按钮
						//请求后台删除账单
						$.post("<%=path%>/debug/removeRepayment.htm",{id:id},function(d){ 
							parent.messageModel(d.resultMsg); //提示信息
							$this.removeClass("disabled"); //移除禁用属性
							$this.find("span").html("删除"); //将按钮文字变为删除
							$this.removeAttr("disabled","disabled"); //将按钮的禁用属性改为非禁用
							if(d.resultCode == 1){//如果操作成功，刷新当前列表
								$pager_repay.gotoPage($pager_repay.pageNumber);
							}
						},"json");
					}, function(){
						parent.layer.close(l);
						$this.removeClass("disabled");
					});
				});

				$(".split").click(function(){ //分期按钮的点击事件
					//点击分期按钮，显示分期期数，以及确认取消按钮
					$split_span = $('<span class="split-span"></span>');
					$sels = $('<span></span>');
					//分期期数的 下拉框
					$sel = $('<select id="split_select_'+$(this).attr("data-id")+'"></select>');
					$sel.append('<option value="3">三期</option>');
					$sel.append('<option value="6">六期</option>');
					$sel.append('<option value="9">九期</option>');
					$sel.append('<option value="12">十二期</option>');
					$sel.append('<option value="18">十八期</option>');
					$sel.append('<option value="24">二十四期</option>');
					var p = $(this).parent();
					var id = $(this).attr("data-id");
					//取消按钮
					$cancel = $('<a href="javascript:void(0);" data-id="'+id+'" class="btn btn-danger btn-sm cancel"><i class="fa fa-remove"></i> <span>取消</span> </a>');
					//取消按钮绑定点击事件
					$cancel.click(function(){
						$(this).parent().parent().find(".split").show();
						$(this).parent().parent().parent().find(".remove").show();
						$(this).parent().remove();
					});
					//确认分期按钮
					$sure = $('<a href="javascript:void(0);" data-id="'+id+'" class="btn btn-success btn-sm sure"><i class="fa fa-check"></i> <span>确定</span> </a>');
					//确认分期按钮绑定点击事件
					$sure.click(function(){
						//此处按钮的防止重复点击的处理同上 删除事件
						if($(this).hasClass("disabled")) return;
						$(this).addClass("disabled");
						$(this).find("span").html("处理中");
						$(this).attr("disabled","disabled");
						var month = $(this).parent().find("select").val();
						var id = $(this).attr("data-id");
						$this = $(this);
						$.post("<%=path%>/debug/split.htm",{id:id,month:month},function(d){
							parent.messageModel(d.resultMsg);
							$this.removeClass("disabled");
							$this.find("span").html("确定");
							$this.removeAttr("disabled");
							if(d.resultCode == 1){
								$pager_repay.gotoPage($pager_repay.pageNumber);
							}
						},"json");
					});
					
					$sels.append($sel);
					$split_span.append($sels);
					$split_span.append($sure);
					$split_span.append(" ");
					$split_span.append($cancel);
					p.append($split_span);
					$("#split_select_"+$(this).attr("data-id")).select();
					$(this).hide();
					$(this).parent().parent().find(".remove").hide();
				});
				
			}
		});
		
		

		$("#oid-span").operaSelect();
		$("#create-repay").click(function(){ //生成账单按钮点击事件
			$this = $(this);
			if($this.hasClass("disabled")) return;
			$this.addClass("disabled");
			$this.val("处理中");
			$.post("<%=path%>/debug/createRepay.htm",{time:$("#outdate").val()},function(d){
				parent.messageModel(d.resultMsg);
				$this.removeClass("disabled");
				$this.val("生成账单");
				$pager_repay.gotoPage($pager_repay.pageNumber);
			},"json");
		});
		
		
		
		laydate({
			elem: '#outdate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		

		/***********************账单调试选项卡下的所有js事件  end*******************************/
		
		
		
		/***********************结算单调试选项卡下的所有js事件  start*******************************/
		
		$("#create-settlement").click(function(){//生成结算单按钮点击事件
			$this = $(this);
			if($this.hasClass("disabled")) return;
			$this.addClass("disabled");
			$this.val("处理中");
			$.post("<%=path%>/debug/createSettlement.htm",{time:$("#sdate").val()},function(d){
				parent.messageModel(d.resultMsg);
				$this.removeClass("disabled");
				$this.val("生成结算单");
				$ts.gotoPage($ts.pageNumber);
			},"json");
		});

		laydate({
			elem: '#sdate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});

		
		var $ts = $("#data_ts").pager({
			url:"${hsj}/debug/settlementDetailList.htm",
			formId:"Formts",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>结算单号</th>'+
							'<th>结算金额</th>'+
							'<th>所属运营商</th>'+
							'<th>出账时间</th>'+
							'<th>结算单类型</th>'+
							'<th>结算状态</th>'+
							'<th>结算时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td id="amount_{id}">{amount}</td>'+
							'<td>{operaName}</td>'+
							'<td>{outSettlementDateStr}</td>'+
							'<td id="sets_{id}"></td>'+
							'<td id="set_{id}"></td>'+
							'<td>{settlementTimeStr}</td>'+
							'<td>'+
							'<span id="split2_{id}"></span> '+
							'<a  id="sp_{id}" href="javascript:void(0);" data-id="{id}" class="btn btn-danger btn-sm remove"><i  class="fa fa-remove"></i> <span>删除</span> </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="11"><center>暂无数据</center></tr>'
			},
				callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					d = list[i];
					//（0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）
					 if(d.status == 0) $("#set_"+d.id).html("待确认");
					 if(d.status == 1) $("#set_"+d.id).html("待结算");
					 if(d.status == 2) $("#set_"+d.id).html("已结算");
					 if(d.status == 3) $("#set_"+d.id).html("未发送");
					 if(d.status == 4) $("#set_"+d.id).html("未生成");
					 if(d.status == 5) $("#set_"+d.id).html("遗漏交易");
					 if(d.settlementType == 0){
						 $("#sets_"+d.id).html("商户");
					 }else if(d.settlementType == 1) {
						 $("#sets_"+d.id).html("运营商");
					 }
					 if(d.status == 2) $("#sp_"+d.id).css("display","none");
					 
				}
				
				//点击删除
				 $("#data_ts").find(".remove").click(function(){
					var data =  $(this).attr("data-id");
					console.log(data);
					l = parent.layer.confirm('您确认要删除该帐单吗?', {
						btn: ['确定','取消'] //按钮
					}, function(){
						parent.layer.close(l); //移除confirm询问框
						$this.find("span").html("处理中"); //更改删除按钮的文字
						$this.attr("disabled"); //禁用删除按钮
						//请求后台删除账单
						$.post("<%=path %>/debug/removeset.htm",{data_id:data},function(d){ 
							parent.messageModel(d.resultMsg); //提示信息
							$this.removeClass("disabled"); //移除禁用属性
							$this.find("span").html("删除"); //将按钮文字变为删除
							$this.removeAttr("disabled","disabled"); //将按钮的禁用属性改为非禁用
							if(d.resultCode == 1){//如果操作成功，刷新当前列表
								setTimeout(function(){
									$ts.gotoPage($ts.pageNumber);
								},1000);
								
							}
						},"json");
					}, function(){
						parent.layer.close(l);
						$this.removeClass("disabled");
					});
					
				});
				
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		}); 
		
		laydate({
			elem: '#outdate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
	});
	
</script>

	</body>
</html>
