<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href=${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
        <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <style>
        body{
            background: #fff!important;
        }
        .data_analyse{
            width: 98%;
            height: auto;
            line-height: 120px;
            margin-left: 2%;
            padding-bottom: 20px;
        }
        .data_analyse ul{
            padding: 0;
            margin: 0;
        }
        .data_analyse>.tab-container>ul>li{
            list-style: none;
            float: left;
            width: 10%;
            height: 30px;
            line-height: 30px;
            text-align: center;
            border: 1px solid #ededed;
            border-radius: 30px;
            color: #000;
        }
        .data_analyse .dtfz{
            width: 12%;
        }
        #typeList .active{
            background: #f49110;
            color: #fff;
        }
        .tab-content{
            clear: both;
        }
        .bzt .col-sm-6 .col-sm-6{
            padding-left: 0;
            text-align: left;
            height: 20px;
            line-height: 20px;
        }
        .bzt .col-sm-6 img{
            width: 16px;
        }
        .bzt .col-sm-6 .col-sm-6 .col-sm-2{
            padding-left: 0;
        }
        .bzt .col-sm-6 .col-sm-6 .col-sm-3{
            padding-left: 0;
        }
        .t1{
            width: 100%;
            height: 12px;
            border-radius: 8px;
            background: #F5F5F5;
            margin-top: 6px;
        }
        .t2{
            width: 100%;
            height: 12px;
            border-radius: 8px;
            background: #1ab394;
            margin-top: 26px;
        }
        .data_a{
            position: absolute;
            top:10%;
            left:50%;
            width: 100%;
            height: auto;
            z-index: -999;
        }
        .data_a li{
            list-style: none;
            line-height: 24px;
        }

    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content  animated fadeInRight article">
    <div class="top_text" style="overflow: auto">
        <div class="top_cont" style="margin-bottom: 20px">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        数据分析
                    </div>
                </div>
            </div>
        </div>
        <div class="data_analyse">
            <div class="tab-container">
                <ul id="typeList">
                <#--<li class="active">
                    <a href="#tab-1" data-toggle="tab">性别</a>
                </li>
                <li>
                    <a href="#tab-2" data-toggle="tab">年龄</a>
                </li>
                <li class="dtfz">
                    <a href="#tab-3" data-toggle="tab">多头负账模型评分</a>
                </li>
                <li>
                    <a href="#tab-4" data-toggle="tab">被拒绝类型</a>
                </li>
                <li>
                    <a href="#tab-5" data-toggle="tab">地区</a>
                </li>
                <li>
                    <a href="#tab-6" data-toggle="tab">评分段</a>
                </li>-->
                </ul>
                <div class="tab-content">
                    <div>
                        <div class="bzt">
                            <div class="container-fluid">
                                <div class="row" style="height:420px;border-bottom: 1px solid #ededed">


                                    <div class="col-sm-6" style="margin-top: 20px">
                                        <p>
                                            <img src="${basePath!}/img/u534.png" alt="">
                                            人数占比
                                            <span style="margin-left: 70px">人数</span>
                                        </p>
                                        <div class="col-sm-6">
                                            <div id="flot-chart"  style="width: 600px;height:350px;"></div>
                                            <div class="data_a">
                                                <ul id="peopleNum">
                                                <#-- <li>10</li>
                                                        <li>20</li>
                                                        <li>30</li>
                                                        <li>10</li>
                                                        <li>20</li>
                                                        <li>30</li>
                                                        <li>10</li>
                                                        <li>20</li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 overDuePeople" style="margin-top: 20px">
                                        <p>
                                            <img src="${basePath!}/img/u561.png" alt="">
                                            违约人数占比
                                            <span style="margin-left: 40px">人数</span>
                                        </p>
                                        <div class="col-sm-6">
                                            <div id="overDue-chart"  style="width: 600px;height:350px;"></div>
                                            <div class="data_a">
                                                <ul id="overDuepeopleNum">
                                                <#--<li>10</li>
                                                     <li>20</li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row san_ping">
                                    <div class="col-sm-6 overDuePeople" style="margin-top: 20px">
                                        <p>
                                            <img src="${basePath!}/img/u73.png" alt="">
                                            违约金额占比
                                            <span style="margin-left: 40px">金额</span>
                                        </p>
                                        <div class="col-sm-6">
                                            <div id="overDueMoney-chart"  style="width: 600px;height:350px;"></div>
                                            <div class="data_a">
                                                <ul id="overDueMoney">
                                                <#-- <li>10</li>
                                                        <li>20</li>
                                                        <li>30</li>
                                                        <li>10</li>
                                                        <li>20</li>
                                                        <li>30</li>
                                                        <li>10</li>
                                                        <li>20</li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<!-- Sparkline -->
<#--<script type="text/javascript" src="${basePath!}/js/jsapi.js"></script>
<script type="text/javascript" src="${basePath!}/js/corechart.js"></script>
<script type="text/javascript" src="${basePath!}/js/jquery.gvChart-1.0.1.min.js"></script>-->

<script type="text/javascript" src="${basePath!}/js/plugins/echarts/echarts-all.js"></script>

<script>
    $(document).ready(function(){

        //加载typeList
        $.ajax({
            url:"${basePath!}/fund/fAnalysis/categorys",
            type:'GET', //GET
            success:function(d){
                console.log(d)
                var typeList='';
                for(var i = 0 ; i < d.data.length ; i++){
                    typeList+= '<li id='+d.data[i].id+'>'+d.data[i].name+'</li>';
                }
                $('#typeList').html(typeList);
                $('#typeList li:first-child').addClass('active');

                $("#typeList li").click(function () {
                      $("#typeList li").removeClass("active");
                       $(this).addClass("active");
                      console.log( $(this).attr('id'))
                      loadPie($(this).attr('id'))
                });

            }
        });

        initPie();//初始化饼图

        function initPie(){
            $.ajax({
                url:"${basePath!}/fund/fAnalysis/firstType",
                type:'GET', //GET
                success:function(d){
                    console.log(d)
                    loadPie(d.data);
                }
            });
        }

        //根据不同的typeId加载饼图
        function loadPie(typeId){
            $.ajax({
                url:"${basePath!}/fund/fAnalysis/findRefusingTypeId",
                type:'GET', //GET
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    if(typeId==d.data){
                        //加载拒绝授信的类型——饼图分析
                        $.ajax({
                            url:"${basePath!}/fund/fAnalysis/categorysinfo",
                            type:'GET', //GET
                            data:{typeId:typeId},
                            timeout:30000,    //超时时间
                            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                            success:function(d){
                                $(".overDuePeople").hide();

                                /*加载人数占比图*/
                                initData(d.data.nameList,d.data.valueList);

                                //加载人数占比图对应人数
                                initPeopleNum(d.data.valueList);
                            }
                        });
                    }else{
                        //加载除拒绝授信的类型之外——饼图分析
                        $.ajax({
                            url:"${basePath!}/fund/fAnalysis/categorysinfo",
                            type:'GET', //GET
                            data:{typeId:typeId},
                            timeout:30000,    //超时时间
                            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                            success:function(d){
                                $(".overDuePeople").show();
                                console.log(d.data.valueList);
                                console.log(d.data.overdueList);
                                /*加载人数占比图*/
                                initData(d.data.nameList,d.data.valueList);

                                //加载人数占比图对应人数
                                initPeopleNum(d.data.valueList);

                                /*加载违约占比图*/
                                initOverDuePieData(d.data.nameList,d.data.overdueList);

                                //加载人数占比图对应人数
                                initOverDuePeopleNum(d.data.overdueList);


                                /*加载违约金额占比图*/
                                initOverDueMoneyPieData(d.data.nameList,d.data.overdueMoneyList);

                                //加载对应违约金额
                                initOverDueMoney(d.data.overdueMoneyList);
                            }
                        });
                    }
                }
            });
        }

        //加载人数
        function initPeopleNum(peopleNumList){
           var pepoleData='';
            for(var i = 0 ; i < peopleNumList.length ; i++){
                pepoleData+= '<li>'+peopleNumList[i].value+' 人</li>';
            }
            $('#peopleNum').html(pepoleData);
        }


        //加载违约人数
        function initOverDuePeopleNum(peopleNumList){
            var pepoleData='';
            for(var i = 0 ; i < peopleNumList.length ; i++){
                pepoleData+= '<li>'+peopleNumList[i].value+' 人</li>';
            }
            $('#overDuepeopleNum').html(pepoleData);
        }


        //加载违约金额
        function initOverDueMoney(overdueMoneyList){
            var overdueMoneyData='';
            for(var i = 0 ; i < overdueMoneyList.length ; i++){
                overdueMoneyData+= '<li>'+overdueMoneyList[i].value+' 元</li>';
            }
            $('#overDueMoney').html(overdueMoneyData);
        }


        //人数占比饼图控件
        function initData(nameList,valueList){
            $("#flot-chart").empty();
            var chart = echarts.init(document.getElementById('flot-chart'));
            option = {
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                        data: nameList,
                    x:'left'
                },
                series : [
                    {
                        name: '授信人数',
                        type: 'pie',
                        radius : '55%',
                        center: ['60%', '40%'],
                        data:valueList,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            chart.setOption(option);
        }



        //违约饼图控件
        function initOverDuePieData(nameList,overdueList){
            $("#overDue-chart").empty();
            var chart = echarts.init(document.getElementById('overDue-chart'));
            option = {
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: nameList,
                    x:'left'
                },
                series : [
                    {
                        name: '违约人数',
                        type: 'pie',
                        radius : '55%',
                        center: ['60%', '40%'],
                        data:overdueList,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            chart.setOption(option);
        }



        //违约饼图控件
        function initOverDueMoneyPieData(nameList,overdueMoneyList){
            $("#overDueMoney-chart").empty();
            var chart = echarts.init(document.getElementById('overDueMoney-chart'));
            option = {
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: nameList,
                    x:'left'
                },
                series : [
                    {
                        name: '违约金额',
                        type: 'pie',
                        radius : '55%',
                        center: ['60%', '40%'],
                        data:overdueMoneyList,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            chart.setOption(option);
        }

    });
</script>
</body>

</html>
