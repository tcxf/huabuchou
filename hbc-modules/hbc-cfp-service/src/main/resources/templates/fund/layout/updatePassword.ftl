<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${basePath!}/css/set_pawd.css">
    <title>更改密码</title>
</head>
<body>
<div class="set_password">
    <form class="form-horizontal">
        <div class="top_title">
            更改密码
        </div>
        <div class="top_logo">
            <img src="${basePath!}/images/logo2.png" alt="">
            <p>随时随地，想花就花</p>

        </div>

        <div class="yone_password one_password">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-1 col-xs-2">
                        <img src="${basePath!}/images/icon_password.png" alt="">
                    </div>
                    <div class="col-lg-10 col-xs-10">
                        <input type="password" id="yPassword" class="form-control" placeholder="请输入您的原密码" >
                    </div>
                </div>
            </div>
        </div>

        <div class="one_password">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-1 col-xs-2">
                        <img src="${basePath!}/images/icon_password.png" alt="">
                    </div>
                    <div class="col-lg-10 col-xs-10">
                        <input type="password" id="newPassword" class="form-control" placeholder="请输入您的新密码" >
                    </div>
                </div>
            </div>
        </div>
        <div class="one_password">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-1 col-xs-2">
                        <img src="${basePath!}/images/icon_password.png" alt="">
                    </div>
                    <div class="col-lg-10 col-xs-10">
                        <input type="password" id="towPassword" class="form-control" placeholder="请再次输入您的新密码" >
                    </div>
                </div>
            </div>
        </div>
        <div class="confirm_password">
            <a href="#" id="ok" class="btn btn-info">确认修改</a>
        </div>
    </form>
</div>
<script src="${basePath!}/js/jquery.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<script>
    $(document).ready(function() {
        var type = "${type!}"
        if(type==1){
            $("#yPassword").val("${pwd!}");
            $(".yone_password").css("display","none");
        }

        $("#ok").click(function (){
            var ypwd = $.trim($('#yPassword').val());
            var newpwd = $.trim($('#newPassword').val());
            var password = $.trim($('#towPassword').val());
            if (newpwd.length < 6 || newpwd.length > 16  ) {
                layer.msg("密码必须是6-16位");
                return;
            }
            if (password.length < 6 || password.length > 16  ) {
                layer.msg("密码必须是6-16位");
                return;
            }
            if (newpwd != password) {
                layer.msg("输入的两个密码不一致");
                return;
            }
            if (newpwd == ypwd) {
                layer.msg("新密码不能跟原来相同");
                return;
            }
            var data={};
            data.ypwd=ypwd;
            data.newpwd=newpwd;
            data.password=password;
            data.type=type;
            $.ajax({
                cache: false,
                type: "POST",
                contentType:"application/json",
                url:"${basePath!}/fund/f_index/updatePassWord",
                data: JSON.stringify(data),
                error: function(request) {
                    layer.msg('修改失败');
                },
                success: function(results) {
                    var resultJson = results;
                    var resultCode = resultJson.code;
                    var resultMsg = resultJson.msg;
                    if(resultCode == "0")
                    {
                        if(resultJson.data=="1"){
                            window.location.href = "${basePath!}/fund/fpage/index";
                        }else{
                            layer.msg(resultMsg);
                        }

                    }else{
                        layer.msg(resultMsg);
                    }
                }
            });

        })
    })

</script>
</body>
</html>