<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<meta charset="UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta charset="UTF-8">
<script type="text/javascript">
	$(function() {
		//导航切换
		$(".menuson li").click(function() {
			$(".menuson li.active").removeClass("active")
			$(this).addClass("active");
		});
		// 导航点击
		$(".menuson li a").click(function() {
			addTab($(this).attr("menuname"),$(this).attr("menuurl"),$(this).attr("menuid"))
		});
	})
	
	//主内容tabs增加tab
	function addTab(menuname, menuurl,menuid){
		menuurl = menuurl + '?menuId='+menuid;
		if ($('#centerTabs').tabs('exists', menuname)){
			$('#centerTabs').tabs('select', menuname);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0" id="'+menuid+'"  src="${hsj}'+menuurl+'" style="width:100%;height:99%;"></iframe>';
			$('#centerTabs').tabs('add',{
				title:menuname,
				content:content,
				closable:true
			});
		}
	}

</script>
<div class="easyui-accordion" data-options="fit:true,selected:-1">	
	<c:forEach var="r" items="${menuResources}" varStatus="status">
		<div title="${r.resourceName}">
			<c:if test="${functionList != null && fn:length(functionList) > 0}">
				<c:forEach var="f" items="${functionList}" varStatus="status_1">
					<c:if test="${f == r.id}">
						<c:set var="i" value="${status_1.count}"/>  
					</c:if>
				</c:forEach>
				<c:forEach var="menu" items="${menuList}" varStatus="status_2">
					<c:if test="${status_2.count == i}">
					<c:forEach var="m" items="${menu}" varStatus="status_3">
						<ul class="menuson">
							<li><cite></cite><a href="javascript:void(0)" menuid="${m.id}" menuurl="${m.resourceUrl}" menuname="${m.resourceName}">${m.resourceName}</a><i></i></li>
						</ul>
					</c:forEach>
					</c:if>
				</c:forEach>
			</c:if>
		</div>
	</c:forEach>
</div>
