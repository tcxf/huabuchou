<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

	<#include "common/common.ftl">

    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
    <style>
    	 body{
			background:url(${basePath!}/img/zijinduan_background.png) no-repeat;    
			background-size: cover;
			background-position: center center;
			background-attachment: fixed;
			height:500px;
		 }
		 .t-text{
			 margin-top: 76px;
			 text-align:center;
		 }
		 .t-text>h1{
			 color:#007dc8;
			 font-weight:600;
		 }
		 .a-content{
			 height: 600px;
			 background:url(${basePath!}/img/loding_background.png) no-repeat;
			 background-size:500px 500px;
			 background-position: center;
			 margin-top: 7%;
		 }
		 .t-logo{
			 text-align:center;
		 }
		 .t-logo>img{
			 width:80px;
			 height:40px;
		 } 
		 .row{
			 text-align:center;
			 margin-left:33%
		 }
		 .t-form{
			 padding:10px;
			 width:100%;
			 height:250px;
			 position:relative;
		 }
		 h4{
			 font-size:26px;
			 color:#000;
			 margin-bottom:24px;
		 }
		 .t-form .img1{
			 position: absolute;
			 top:195px;
			 left:12px
		 }
		 .t-form .img2{
			 position: absolute;
			 top:238px;
			 left:13px
		 }
		 .t-btn{
			 width:100%;
			 height:40px;
			 line-height:40px;
			 background:  linear-gradient(to bottom,#21b8ff, #359afe);
			 color:#fff;
			 text-align:center;
			 font-size:16px;
			 margin-top:25px;
		 }
    </style>
</head>

<body>
    <div class="a-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="t-form">
						<form class="m-t" role="form">
                            <div class="t-text">
                                <div class="t-logo">
                                    <img class="img" src="${basePath!}/img/imglogo.png">
                                </div>
                                <h1>花不愁资金方管理系统</h1>
                            </div>
						<div class="form-group">
							 <div style="line-height: 30px; width: 11%;">
				                <img class="img1" src="${basePath!}/img/name.png" style="width: 20px;height: 20px">
				            </div>
				            <div class="wkt-flex">
								<input type="text" id="account" class="form-control" style="padding-left:25px"
									placeholder="用户名">
							</div>	
						</div>
						<div class="form-group">
							<div style="line-height: 30px; width: 11%;">
			                   <img class="img2" src="${basePath!}/img/mima.png" style="width: 20px;height: 20px">
			                </div>
			                 <div class="wkt-flex">
								<input type="password" id="pwd" class="form-control" style="padding-left:25px"
									placeholder="密码">
							</div>		
						</div>
						<div class="t-btn" id="loginButton">登 录</div>	
			           </form>
					</div>
				</div> 
			</div>
		</div>
		

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
	
</body>


<script type="text/javascript">
    $(document).ready(function () {
		$(document).keydown(function(event){
			if(event.keyCode == 13){ //绑定回车
				loginAjaxSubmit();
			};
		});

		$(function() {
			//iframe页面session超时跳到顶层登陆页面
			$("#loginButton").click(function(){
				if(validLogin() == true)
				{
					loginAjaxSubmit();
				}
			});
		});

		function validLogin(){
			if($("#mobile").val() == "")
			{
				layer.msg('请填写登录名');
				return false;
			}
			if($("#pwd").val() == "")
			{
				layer.msg('请填写密码');
				return false;
			}
			return true;
		}

		function loginAjaxSubmit(){
			$.ajax({
				cache: false,
				type: "POST",
				url:"${basePath!}/fund/flogin/login",
				data: {account:$("#account").val(),pwd:$("#pwd").val()},
				error: function(request) {
					layer.msg('登陆发生错误');
				},
				success: function(results) {
					var resultJson = results;
					var resultCode = resultJson.code;
					var resultMsg = resultJson.msg;
					if(resultCode == "1")
					{
					  if(resultJson.data!=null&&resultJson.data=="true"){
							var pwd = $("#pwd").val();
							window.location.href = "${basePath!}/fund/f_index/goToUpdatePassWordPage?type=1&pwd="+pwd;
						}else{
							window.location.href = "${basePath!}/fund/fpage/index";
						}

					} else {
						layer.msg(resultMsg);
					}
				}
			});
		}

		function validRestPwd(){
			if($("#userNameForRest").val() == "")
			{
				jNotify("请输入用户名!",{
					ShowOverlay : false,
					MinWidth : 100,
					VerticalPosition : "bottom",
					HorizontalPosition : "right"
				});
				return false;
			}
			if($("#regEmail").val() == "")
			{
				jNotify("请输入注册邮箱!",{
					ShowOverlay : false,
					MinWidth : 100,
					VerticalPosition : "bottom",
					HorizontalPosition : "right"
				});
				return false;
			}
			return true;
		}

		function loginRestPwdToggle(toogleFlag){
			if(toogleFlag == 1){
				$("#loginDiv").show();
				$("#resetPwdDiv").hide();
			}
			else{
				$("#resetPwdDiv").show();
				$("#loginDiv").hide();
			}
		}
    });
</script>

</html>
