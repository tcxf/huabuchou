<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
					<!-- <li><a data-toggle="tab" href="#grgs" aria-expanded="true"> 个人工商信息</a></li> -->
					<li><a data-toggle="tab" href="#dtjd" aria-expanded="true"> 多头借贷</a></li>
					<li><a data-toggle="tab" href="#yhk" aria-expanded="true"> 银行卡</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${id}"/>
						<input type="hidden" name="mid" id="mid" value="${mid}"/>
					  	<div class="form-group">
							<label class="col-sm-2 control-label">商户id</label>
							<label class="col-sm-10 control-label" style="text-align: left;">
								${entity.mnum}
							</label>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.name}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行业类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:forEach items="${clist }" var="is">
										<c:if test="${is.id == entity.micId}">${is.name}</c:if>
									</c:forEach>
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.legalName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人证件号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group">
								<label class="col-sm-2 control-label">所属地区</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.displayName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;"> 
									<c:if test="${id != null && entity.status == 1}">启用</c:if>
									<c:if test="${id != null && entity.status == 0}">禁用</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="examine" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户性质</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.nature}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">品牌经营年限</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operYear}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">经营面积</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operArea}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">注册资金（万）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.regMoney}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">场地照片</label>
							<c:if test="${entity.localPhoto != null && entity.localPhoto != ''}">
							<img src="${res}${entity.localPhoto}" style="width:400px;height:400px;" />
							</c:if>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">营业执照</label>
							<c:if test="${entity.licensePic != null && entity.licensePic != ''}">
							<img src="${res}${entity.licensePic}" style="width:250px;height:400px;" />
							</c:if>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">法人身份证</label>
							<c:if test="${entity.legalPhoto != null && entity.legalPhoto != ''}">
							<img src="${res}${entity.legalPhoto}" style="width:400px;height:250px;" />
							</c:if>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">借记卡照片</label>
							<c:if test="${entity.normalCard != null && entity.normalCard != ''}">
							<img src="${res}${entity.normalCard}" style="width:400px;height:250px;" />
							</c:if>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">信用卡照片</label>
							<c:if test="${entity.creditCard != null && entity.creditCard != ''}">
							<img src="${res}${entity.creditCard}" style="width:400px;height:250px;" />
							</c:if>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					
					<!-- 个人工商信息 -->
					<div id="grgs" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">法人姓名：<b id="gs_frxm"></b></label>
									<label class="col-sm-3 control-label">企业名称：<b id="gs_qymc"></b></label>
									<label class="col-sm-3 control-label">注册号：<b id="gs_zch"></b></label>
									<label class="col-sm-3 control-label">注册资本：<b id="gs_zczb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">企业状态：<b id="gs_qyzt"></b></label>
									<label class="col-sm-3 control-label">被执行人姓名：<b id="gs_bzxrxm"></b></label>
									<label class="col-sm-3 control-label">被执行人身份证：<b id="gs_bzxrsfz"></b></label>
									<label class="col-sm-3 control-label">被执行人案件状态：<b id="gs_bzxrajzt"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">失信被执行人名称：<b id="gs_sxmc"></b></label>
									<label class="col-sm-3 control-label">失信被执行人身份证/组织机构代码：<b id="gs_sxsfz"></b></label>
									<label class="col-sm-3 control-label">失信被执行人法人姓名：<b id="gs_sxfr"></b></label>
									<label class="col-sm-3 control-label">失信具体情形：<b id="gs_sxqx"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">行政处罚当事人：<b id="gs_xzcfdsr"></b></label>
									<label class="col-sm-3 control-label">行政处罚证件号：<b id="gs_xzcfzjh"></b></label>
									<label class="col-sm-5 control-label">行政处罚主要违法事实：<b id="gs_xzcfwfss"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
					<!-- 多头借贷 -->
					<div id="dtjd" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
                               <div class="panel-collapse">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>黑名单类型</td>
			                                        <td>黑名单事实类型</td>
			                                        <td>黑名单事实</td>
			                                        <td>事件涉及金额</td>
			                                        <td>事件发生日期</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="sb_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 银行卡信息 -->
					<div id="yhk" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">银行简称：<b id="yh_yhjc"></b></label>
									<label class="col-sm-3 control-label">银行卡号：<b id="yh_yhkh"></b></label>
									<label class="col-sm-3 control-label">银行logo：<img id="yh_logo" src=""></label>
									<label class="col-sm-3 control-label">银行名称：<b id="yh_yhqc"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">银行网址：<b id="yh_yhwz"></b></label>
									<label class="col-sm-3 control-label">卡类型：<b id="yh_klx"></b></label>
									<label class="col-sm-3 control-label">银行卡种类：<b id="yh_yhkzl"></b></label>
									<label class="col-sm-3 control-label">英文全称：<b id="yh_ywqc"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script type="text/javascript" src="<%=path%>/js/improveQuotaMerchant.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
		$(document).ready(function () {
			$("#area").area();
			examine = false;
			$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
				  var target =e.target.toString(); //$(e.target)[0].hash;
				  if( target.indexOf('examine')>0 && !examine){
					$(".img").uploadImg();
					examine = true;
				  }
		  	});
			
			
			
		});
	</script>
</body>

</html>
