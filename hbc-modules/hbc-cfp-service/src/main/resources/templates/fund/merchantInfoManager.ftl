<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<style>
        .dingdan-list{
            padding: 20px;
        }
         .dingdan-list .row{
         	padding-bottom:20px;
         }
    </style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户管理</h5>
						<div style="display:none;" class="ibox-tools">
							<a  href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商户</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="dingdan-list">
						      <div class="container">
							     <div class="row">
							     	<div class="form-group col-md-3 col-sm-3 col-lg-4 form-inline">
									  <label class="control-label">商户名称</label>
										<input id="name" type="text" class="form-control" name="name" maxlength="30" placeholder="请输入名称或者手机号">
								    </div>
					
									<div class="form-group col-md-3 col-sm-3 col-lg-3">
								     	<label class="control-label">商户状态</label>
									    	<select id=status name="status">
											   <option value="">全部</option>
											   <option value="0">禁用</option>
											   <option value="1">启用</option>
										   </select>
								    </div>
								    <div class="form-group col-lg-1" style="text-align:center">
										<input type="button" id="search" class="btn btn-warning" value="搜索" />
								    </div>
								  </div>
								  <div class="row">
							
								     <div class="form-group col-md-3 col-sm-3 col-lg-3">
								     	<label class="control-label">审核状态</label>
									    	<select id="authExamine" name="authExamine">
											   <option value="">全部</option> 
											    <option value="0">未提交</option>
											   <option value="1">待审核</option>
											   <option value="2">审核失败</option>
											   <option value="3">审核成功</option> 
										   </select>
								    </div>
								  </div>
								 
							     </div>
							    
							    
							    
							   </div>
							    
							 </div> 
							  <div class="project-list" id="data"></div>   
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path %>/js/jquery.3-3.3.1.min.js"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
   		$("#status").select();
   		$("#authExamine").select();  
		var $pager = $("#data").pager({
			url:"${hsj}/fundend/fundendmerchantList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							
							'<th>法人名称</th>'+
							'<th>商户手机</th>'+
							'<th>商户名称</th>'+
							'<th>申请日期</th>'+
							'<th>审核日期</th>'+
							'<th>商户状态</th>'+
							'<th>审核状态</th>'+
							'<th>授信状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							
							'<td>{legalName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{simpleName}</td>'+   
							'<td>{syDateStr}</td>'+
							'<td>{modifyDate}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="authExamine_{id}"></td>'+
							'<td id="syauthExamine_{id}"></td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+ 
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					console.log(list[i].status);
					if(list[i].status == 1)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
					else if(list[i].status == 0)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
					else
						$("#status_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
						
					//$btn = $('<a href="javascript:void(0);" data="'+list[i].id+','+list[i].simpleName+'" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a>');
					$btn = "";
					var authExamine = list[i].authExamine;
					//审核状态
					if(authExamine == 1)
						$("#authExamine_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
					else if(authExamine == 2)  
						$("#authExamine_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>审核失败</span>");
					else if(authExamine == 3) 
						$("#authExamine_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>审核成功</span>"); 
					else 
						$("#authExamine_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>未提交</span>");
					var syauthExamine = list[i].syauthExamine;
					//授信审核状态
					if(syauthExamine == 1)
						$("#syauthExamine_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
					else if(syauthExamine == 2)  
						$("#syauthExamine_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>审核中</span>");
					else if(syauthExamine == 3) 
						$("#syauthExamine_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>授信成功</span>"); 
					else if(syauthExamine == 4) 
						$("#syauthExamine_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>已驳回</span>"); 
					else 
						$("#syauthExamine_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>未提交</span>");
					
				
			/* 		if(authExamine == 3){
						$("#oper_"+list[i].id).append($btn);
						$xzht = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>查看协议</span> </a>');
						$("#oper_"+list[i].id).append($xzht);
						$xzht.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('查看协议','${hsj}/merchantInfo/forwardMerchantInfoht.htm?id='+id,'1000px','700px',$pager);
						});
					}
					if(authExamine == 1){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('商户资料审核','${hsj}/merchantInfo/showApply.htm?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list[i].id).append($examine);
					} */
					if(syauthExamine == 2){
						$("#oper_"+list[i].id).append($btn);
						$sysh = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].mid+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>授信审核</span> </a>');
						$("#oper_"+list[i].id).append($sysh);
						$sysh.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('授信审核','${hsj}/fundend/showApplysh.htm?id='+id,'1000px','700px',$pager);
						});
					}
					
				}
				
		
				//编辑页面
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('商户详情-'+title,'${hsj}/fundend/forwardMerchantInfoDetail.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${hsj}/merchantInfo/delete.htm',param,$pager);
			}
		});
		
	});
	
</script>

	</body>
</html>
