<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
 <style>
        .dingdan-list{
            padding: 20px;
        }
         .dingdan-list .row{
         	padding-bottom:20px;
         }
    </style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户交易明细</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">交易时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4" >
								 <label class="col-sm-3 control-label">运营商</label>
								   <div class="col-sm-9">
								      <label for="exampleInputEmail2" class="sr-only">所属运营商</label>
								      <select id="oid-span" ele-id="oid" ele-name="oid" name="oid"> 
										<option value="" >--请选择--</option> 
										  <c:forEach var="o" items="${olist}"> 
										    <option name="oid" value="${o.id}">${o.simpleName}</option> 
										  </c:forEach> 
										</select>
								   </div>   
							   </div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">商户名称</label>
									<div class="col-sm-9">
										<input type="text" id="shname" class="form-control" name="uname" maxlength="30" placeholder="请填写商户名称">
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">交易类型</label>
									<div class="col-sm-9">
										<select id="tradingType" name="tradingType">
											<option value="">全部</option>
											<option value="0">授信金消费</option>
											<option value="1">支付通道消费</option>
										</select>
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
   		$("#status").select();
   		$("#orderType").select();
   		$("#coupon").select();
   		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		var $pager = $("#data").pager({
			url:"${hsj}/merchantInfo/morderInfoManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>所属运营商</th>'+
							'<th>商户id</th>'+
							'<th>商户名称</th>'+
							'<th>下单商户</th>'+
							'<th>交易类型</th>'+
							'<th>交易金额</th>'+
							'<th>平台通道费</th>'+
							'<th>商家让利</th>'+
							'<th>交易时间</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{oname}</td>'+
							'<td>{id}</td>'+
							'<td>{mname}</td>'+
							'<td>{uname}</td>'+
							'<td>{tradingTypeStr}</td>'+
							'<td>{amount}</td>'+
							'<td>{platformShareFee}</td>'+
							'<td>{shareFee}</td>'+
							'<td data1="{sumount}">{tradingDateStr}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="7"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				
				
			}
		});
	});
	
</script>

	</body>
</html>
