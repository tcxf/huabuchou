
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div id="cl">
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
					
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${entity.id!}"/>
						<div>
						  	<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">运营商ID</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ooid!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.omobile!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">运营商名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.oname!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">运营商简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.legalName!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人证件号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard!}
								</label>
							</div>
						</div>
                        <div class="hr-line-dashed" style="clear:both"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="padding-left: 30px">所属地区</label>
                            <div class="col-sm-10" style="margin-bottom: 15px" id="area" data-name="area" data-value="${entity.area!}"></div>
                        </div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.oaddress!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					<div id="settlement" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">平台服务费（%）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.platfromShareRatio!}
								</label>
							</div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">滞纳金（%）</label>
                                <div class="col-sm-8" style="text-align: left;">
								${entity.yqRate!}%
                                </div>
                            </div>
						</div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">积分比例（%）</label>
								<div class="col-sm-8" style="text-align: left;">
									${entity.scorePercent!}%
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">综合费率（%）</label>
								<div class="col-sm-8" style="text-align: left;">
									${entity.serviceFee!}%
								</div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">结算周期</label>
								<label class="col-sm-8 control-label">
                                  ${entity.settlementLoop!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
        var context= "${basePath!}";
        var context2= "${basePath!}";
		$(document).ready(function () {
			$("#area").area();
			examine = false;
			$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
				  var target =e.target.toString(); //$(e.target)[0].hash;
				  if( target.indexOf('examine')>0 && !examine){
					$(".img").uploadImg();
					examine = true;
				  }
		  	});

			$("#cl").click(function () {
				window.history.back();
            })
        });
	</script>
</body>

</html>
