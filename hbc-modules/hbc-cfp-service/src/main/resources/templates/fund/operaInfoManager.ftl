<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>运营商管理</h5>
						<div class="ibox-tools">
							
						</div>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
						
				<span>今日新增：${it }人</span>
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">运营商名称</label>
								<input type="text" name="name" placeholder="请输入运营商名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${hsj}/operaInfo/operaInfoManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>运营商id</th>'+
							'<th>运营商名称</th>'+
							'<th>手机</th>'+
							'<th>简称</th>'+
							'<th>规模（万）</th>'+
							'<th>注册日期</th>'+
							'<th>所属地域</th>'+
							'<th>状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{osn}</td>'+
							'<td>{name}</td>'+
							'<td>{mobile}</td>'+
							'<td>{simpleName}</td>'+
							'<td>{regMoney}</td>'+
							'<td>{createDateStr}</td>'+
							'<td>{displayName}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="opera_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$("#yyh").html("运营商共计："+result.totalCount+"家");
			
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].status == 1)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
					else if(list[i].status == 0){
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>未开通</span>");
						$("#opera_"+list[i].id).find(".detail").hide();
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm open"><i class="fa fa-key"></i> <span>立即开通</span> </a>');
					}
					else if(list[i].status == 2)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
					else
						$("#status_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>未知</span>");
						
				}
				
				$(".open").click(function(){
					if($(this).hasClass("disabled")) return;
					var oid = $(this).attr("data");
					$(this).addClass("disabled");
					$(this).find("i").removeClass("fa-key");
					$(this).find("i").addClass("fa-spinner");
					$(this).find("i").addClass("fa-spin");
					$(this).find("span").html("正在处理...");
					setTimeout(function(){
						$(this).find("span").html("权限初始化...");
						$.post('<%=path%>/operaInfo/initData.htm',{oid:oid},function(d){
							parent.messageModel(d.resultMsg);
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}else{
								$(this).removeClass("disabled");
								$(this).find("i").addClass("fa-key");
								$(this).find("i").removeClass("fa-spinner");
								$(this).find("i").removeClass("fa-spin");
								$(this).find("span").html("立即开通");
							}
						},"json");
					},1500);
				});
				
			
				
			
				//编辑页面
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('运营商详情-'+title,'${hsj}/operaInfo/forwardOperaInfoDetail.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
		
		
	});
	
</script>

	</body>
</html>
