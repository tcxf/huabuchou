<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>运营商管理</h5>
						<div class="ibox-tools">
							
						</div>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
						
				<span>今日新增：${number!}人</span>
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
                                        <a href="${basePath!}/fund/operaInfo/qu"><button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">运营商名称</label>
								<input type="text" name="oname" placeholder="请输入运营商名称" id="oname" class="form-control">

                                <label for="exampleInputEmail2" class="sr-only">运营商手机号码</label>
                                <input type="text" name="omobile" placeholder="请输入运营商手机号码" id="omobile" class="form-control">
							</div>

							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var context= "${basePath!}";
        var context2= "${basePath!}";
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/fund/operaInfo/operaInfoManagerList?fid=${fid!}",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
                			'<th>编号</th>'+
							'<th>运营商ID</th>'+
							'<th>运营商名称</th>'+
							'<th>手机号码</th>'+
							'<th>综合费率</th>'+
							'<th>滞纳金</th>'+
							'<th>所属地区</th>'+
							'<th>创建日期</th>'+
							'<th>状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
				            '<td>{ffid}</td>'+
							'<td>{ooid}</td>'+
							'<td>{oname}</td>'+
							'<td>{omobile}</td>'+
							'<td>{serviceFee}%</td>'+
							'<td>{yqRate}%</td>'+
							'<td>{address}</td>'+
							'<td>{createDate}</td>'+
							'<td >{ostatus}</td>'+
				            '<td><a href="javascript:void(0);"  data="{ffid}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> </td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$("#yyh").html("运营商共计："+result.data.total+"家");
                $(".detail").click(function () {
                    var id=$(this).attr("data");
                    window.location.href="${basePath!}/fund/operaInfo/forwardOperaInfoDetail?ffid="+id;
                })



            }
		});
        $("#search").click(function(){
            // var mobile = $("#omobile").val().trim();
            // if(mobile==null||mobile=="" && !(/^1[34578]\d{9}$/.test(mobile))){
            //     layer.msg("手机号码不能为空");
            //     return;
            // }
            // else {
            //     $pager.gotoPage($pager.pageNumber);
            //
            // }
        });

		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
		
		
	});
	
</script>

	</body>
</html>
