<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

	<#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>待还列表</h5>
                </div>
                <div class="ibox-content">
                    <form method ="post" id="searchForm" class="form-horizontal">
                        <input id = "status" type="hidden" name="status" value="2"/>
                        <input id = "isPay" type="hidden" name="isPay" value="0"/>
                        <div>
                            <div class="form-group col-sm-12">
                                <h3>待还款金额：¥ <span id="totalAmount">--</span> 元  = ¥ <span id="currentCorpus">--</span> 元 (本金)+ ¥ <span id="currentFee">--</span> 元(利息)<#-- + ¥ <span id="lateFee">--</span> 元(滞纳金)--></h3>
                            </div>
                        </div>
                        <div>
                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">还款日</label>
                                <div class="col-sm-9">
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
                                    </div>
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">流水单号</label>
                                <div class="col-sm-9">
                                    <input id="ls" type="text" class="form-control" name="serialNo" maxlength="30" placeholder="请填写流水单号">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">用户id</label>
                                <div class="col-sm-9">
                                    <input id="yhid" type="text" class="form-control" name="uid" maxlength="30" placeholder="请填写用户id">
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">用户姓名</label>
                                <div class="col-sm-9">
                                    <input id="yhname" type="text" class="form-control" name="realName" maxlength="30" placeholder="请填写用户姓名">
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">手机号码</label>
                                <div class="col-sm-9">
                                    <input id="phone" type="text" class="form-control" name="telphone" maxlength="30" placeholder="请填写用户手机号码">
                                </div>
                            </div>

                            <!-- 								<div class="form-group col-sm-6 col-lg-4"> -->
                            <!-- 									<label class="col-sm-3 control-label">出账状态</label> -->
                            <!-- 									<div class="col-sm-9"> -->
                            <!-- 										<select id="isOut" name="isOut"> -->
                            <!-- 											<option value="">全部</option> -->
                            <!-- 											<option value="true">已出帐</option> -->
                            <!-- 											<option value="false">未出账</option> -->
                            <!-- 										</select> -->
                            <!-- 									</div> -->
                            <!-- 								</div> -->

                            <div class="form-group col-sm-6 col-lg-4" >
                                <label class="col-sm-3 control-label">运营商</label>
                                <div class="col-sm-9">
									<span id="oid-span" ele-id="oid" ele-name="oid">
                                </div>
                            </div>
                        </div>



                        <div>
                            <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                <input class="btn btn-success" id="search" type="button" value=" 查 询 " />
                                <input id="huankuan" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                            </div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                    <div class="project-list pager-list" id="data"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $("#oid-span").operaSelectByfid(); //加载下拉选项

        //下载报表
        $("#huankuan").click(function(){
            $("#searchForm").attr("action","${basePath!}/fund/f_payback/PaybackPoi");
            $("#searchForm").submit();
        });

        laydate({
            elem: '#startDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
        });
        laydate({
            elem: '#endDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
        });
        $("#isOut").select();
        $("#tradeType").select();
        var $pager = $("#data").pager({
            url:"${basePath!}/fund/f_payback/pageInfo",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>所属运营商</th>'+
                '<th>流水单号</th>'+
                '<th>用户id</th>'+
                '<th>用户手机</th>'+
                '<th>用户姓名</th>'+
                '<th>应还本金</th>'+
                '<th>分期利息</th>'+
                '<th>本期应还</th>'+
                '<th>期数</th>'+
                '<th>还款日</th>'+
                '<th>操作</th>'+
                '<th></th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{oname}</td>'+
                '<td>{serialNo}</td>'+
                '<td>{userId}</td>'+
                '<td>{telPhone}</td>'+
                '<td>{realName}</td>'+
                '<td>¥ {currentCorpus} 元</td>'+
                '<td>¥ {currentFee} 元</td>'+
                '<td id="yh_{id}"></td>'+
                '<td>{currentTime}/{totalTime}</td>'+
                '<td>{repaymentDate}</td>'+
                '<td><a href="javascript:void(0);" data="{rdid}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查询明细 </a></td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="12"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                for(var i = 0 ; i < result.data.records.length ; i++){
                    var item = result.data.records[i];
                    $("#yh_"+item.id).html("¥ " + ((item.currentCorpus == null ? 0:item.currentCorpus)+(item.lateFee == null ? 0:item.lateFee)+(item.currentFee == null ? 0 : item.currentFee)) + " 元");
                }

                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });

                loadAmount();//待还款

                $(".mx").click(function(){
                    window.location.href="${basePath!}/fund/f_payback/jumpWaitDetail?id="+$(this).attr("data");
                });
            }
        });


        function loadAmount(){//查询待还款
            $.ajax({
                url:"${basePath!}/fund/f_payback/restPay",
                type:'POST', //GET
                data:$("#searchForm").serialize(),
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    $("#totalAmount").html(d.data.totalMoney);
                    $("#currentCorpus").html(d.data.currentCorpus);
                    $("#currentFee").html(d.data.currentFee);
                    $("#lateFee").html(d.data.lateFee);
                }
            });
        }


        $("#loading-example-btn").click(function(){
            $pager.gotoPage($pager.pageNumber);
        });
    });
</script>

</body>
</html>
