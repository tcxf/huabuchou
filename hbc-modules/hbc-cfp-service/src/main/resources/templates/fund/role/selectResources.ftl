<!DOCTYPE html>
<html>
<head>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<style type="">
body {
	background-image: none;
}
</style>
</head>


<body>
	<div class="mar-top col-lg-12 main-tabls display:none;" id="resourcesDiv" style="margin-left: 0px;">
		<div id="distribution">
			<div class="form-group" style="padding:10px;">
             	<div class="field">




                    <#list pList as p>
                        <div class="form-group" style="padding:10px;">
                            <div><label> ${p.resourceName}</label></div>
                            <div class="field">
								<#list list as l>
									<#if l.parentId??  &&   l.parentId==p.id>
									  <label><input type="checkbox" class="resources" name="ids" id="${l.id}" value="${l.id}"/> ${l.resourceName}</label>
									</#if>
								</#list>

                            </div>
                        </div>
                    </#list>

						</div>
					</div>

				</div>
			</div>
	  	</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$.post("${basePath!}/fund/fRole/loadRoleResourcesStr",{id:"${id!}"},function(r){
			console.log(r);
			var resources = r.data;
			if(resources != null){
				for(var i = 0 ; i < resources.length ; i++){
					$("#"+resources[i].resourceId).iCheck('check');
				}
			}

			$("#resourcesDiv").show();
		},"json");
		
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
		    increaseArea: '20%' // optional
		});
	});
	
	</script>
</body>
</html>