<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .zongjine{
            margin-top: 20px;
            width: 96%;
            padding: 60px 100px;
            height: 400px;
            margin-left: 4%;
            background: #fff;
        }
        .zongjine .col-sm-2{
            margin-top: 34px;
        }
        .zongjine .col-sm-10{
            padding-left: 0;
            text-align: left;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>设置授信资金池总额</h5>
                    <div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
                        <div>
                            <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
                            <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
                        </div>
                    </div>
                </div>
                <div class="zongjine">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-2">
                                增加资金池总额（万元）
                            </div>
                            <div class="col-sm-10">
                                <p>当前总额：¥<span id="sunmoeny"></span>万元</p>
                                <input type="text" id="moeny"/>
                                <h6>
                                    您可以设置授信资金池总额，总额一旦设置不可减额，只能增额。
                                    <br>
                                    当授信或提额到达资金池上限时，将不能再进行任何授信或提额。
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<!-- iCheck -->
<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function () {

        $("#moeny").on('keyup', function (event) {
            var $amountInput = $(this);
            //响应鼠标事件，允许左右方向键移动
            event = window.event || event;
            if (event.keyCode == 37 | event.keyCode == 39) {
                return;
            }
            //先把非数字的都替换掉，除了数字和.
            $amountInput.val($amountInput.val().replace(/[^\d.]/g, "").
            //只允许一个小数点
            replace(/^\./g, "").replace(/\.{2,}/g, ".").
            //只能输入小数点后两位
            replace(".", "$#$").replace(/\./g, "").replace("$#$", ".").replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'));
        });
        $("#moeny").on('blur', function () {
            var $amountInput = $(this);
            //最后一位是小数点的话，移除
            $amountInput.val(($amountInput.val().replace(/\.$/g, "")));
        });

    $("#cancel").click(function(){
            window.history.go(-1);
        });
        var moeny;
        $("#submit-save").click(function () {
           moeny= $("#moeny").val();
           if(moeny < 0.5){
               layer.msg("资金池额度必须大于5000！");
               return;
           }
            $.ajax({
                url : "${basePath!}/fund/creditfundmanagement/insertMoeny?moeny="+moeny,
                type : "POST",
                dataType : "json",
                success : function(d){
                    if(d.code ==0){
                        parent.messageModel(d.msg);
                        window.location = '${basePath!}/fund/creditfundmanagement/fundmanagement';
                        parent.closeLayer();
                    }else{
                        parent.messageModel(d.msg);
                    }
                }
            });
        })

        //当前资金池总额
        $.ajax({
            url : "${basePath!}/fund/creditfundmanagement/SumMoeny",
            type : "POST",
            dataType : "json",
            success : function(d){
              console.log(d);
              $("#sunmoeny").html(d.data);
            }
        });


    });
</script>
</body>

</html>
