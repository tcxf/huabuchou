<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>交易明细<a  href="javascript:history.back(-1)" >返回上一级</a></h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
        var context= "${basePath!}";
        var context2="${basePath!}";
		
		var $pager = $("#data").pager({
			url:"${basePath!}/fund/settlementDetail/tradingDetailManagerList?id=${id}",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>支付编号</th>'+
							'<th>下单用户</th>'+
							'<th>用户手机</th>'+
							'<th>商户名称</th>'+
							'<th>交易总额</th>'+
							'<th>优惠金额</th>'+
							'<th>实付金额</th>'+
							'<th>运营商抽成</th>'+
							'<th>直接上级返佣</th>'+
							'<th>间接上级返佣</th>'+
							'<th>支付时间</th>'+
							'<th>支付方式</th>'+
							'<th>支付状态</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{dserialNo}</td>'+
							'<td>{paymentSn}</td>'+
							'<td>{realName}</td>'+
							'<td>{cumobile}</td>'+
							'<td>{mname}</td>'+
							'<td>{tradeAmount}</td>'+
							'<td>{discountAmount}</td>'+
							'<td>{actualAmount}</td>'+
							'<td>{operatorShareFee}</td>'+
							'<td>{redirectShareAmount}</td>'+
							'<td>{inderectShareAmount}</td>'+
							'<td >{payDate}</td>'+
							'<td >{type}</td>'+
							'<td >{pstatus}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
