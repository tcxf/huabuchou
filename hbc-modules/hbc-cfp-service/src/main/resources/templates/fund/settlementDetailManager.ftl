<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <style>
        .f_div{
            padding: 0 30px;
        }
        .f_div .col-lg-4 h3{
            font-size: 14px!important;
        }
        select{
            height: 30px;
        }
    </style>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <#include "common/common.ftl">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <!-- <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
                    <li><a data-toggle="tab" href="#uncreate" aria-expanded="true"> 未生成结算单</a></li> -->
                </ul>
                <div class="tab-content">
                    <div id="base" class="ibox-content active tab-pane">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>授信结算管理</h5>
                            </div>
                            <div class="ibox-content">
                                <form id="searchForm" class="form-horizontal">

                                    <div class="row m-b-sm m-t-sm">
                                        <div class="col-md-1">
                                            <a href="${basePath!}/fund/settlementDetail/qu">   <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
                                        </div>
                                        <div class="col-sm-8">

                                        </div>
                                    </div>

                                    <div class="row f_div">
                                        <div class="form-group col-sm-6 col-lg-4">
                                            <h3>待结算总额：¥ <span id="DAmount">--</span> 元</h3>
                                        </div>
                                        <div class="form-group col-sm-6 col-lg-4"">
                                            <h3>已结算总额：¥ <span id="YAmount">--</span> 元</h3>
                                        </div>
                                        <#--<div class="form-group col-sm-6 col-lg-4">-->
                                            <#--<h3>运营商返佣：¥ <span id="opaShareAmount">--</span> 元</h3>-->
                                        <#--</div>-->
                                        <#--<div class="form-group col-sm-6 col-lg-4">-->
                                            <#--<h3>直接上级返佣：¥ <span id="redirectShareAmount">--</span> 元</h3>-->
                                        <#--</div>-->
                                        <#--<div class="form-group col-sm-6 col-lg-4">-->
                                            <#--<h3>间接上级返佣：¥ <span id="inderectShareAmount">--</span> 元</h3>-->
                                        <#--</div>-->
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">商户名称</label>
                                            <div class="col-sm-9">
                                                <input id="mname" type="text" class="form-control" name="mname" maxlength="30" placeholder="请填写商户名称">
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6 col-lg-4" style="margin-left: -30px">
                                            <label class="col-sm-3 control-label">出账时间</label>
                                            <div class="col-sm-9">
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="startDate" id="startDate" style="width:100%;padding-left: 5px" maxlength="30" placeholder="">
                                                </div>
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">所属运营商</label>
                                            <div class="col-sm-9">
                                                <select name="ooid" id="ooid">
                                                    <option value="">请选择运营商</option>
                                                    <#list list as list >
                                                        <option id="id" value="${list.id!}">${list.name!}</option>
                                                    </#list>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="form-group col-sm-6 col-lg-4" style="margin-left: -30px">
                                            <label class="col-sm-3 control-label">结算状态</label>
                                            <div class="col-sm-9">
                                                <select id="status" name="status">
                                                    <option value="">全部</option>
                                                    <option value="0">待确认</option>
                                                    <option value="1">待结算</option>
                                                    <option value="2">已结算</option>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">结算时间</label>
                                            <div class="col-sm-9">
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="createDate2" id="createDate2" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="endDate2" id="endDate2" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                            <input class="btn btn-success" id="search" type="button" value=" 查 询 " />
                                            <input id="ccs" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                                        </div>
                                    </div>
                                </form>
                                <div style="clear:both;"></div>
                                <div class="project-list pager-list" id="data"></div>
                            </div>
                        </div>
                <div id="uncreate" class="ibox-content  tab-pane">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>结算明细</h5>
                        </div>
                        <div class="ibox-content">
                            <form id="searchForm" class="form-horizontal">
                                <div>
                                    <div class="form-group col-sm-6">
                                        <h3>待结算总额：¥ <span id="ua">--</span> 元</h3>
                                    </div>
                                </div>
                            </form>
                            <div style="clear:both;"></div>
                            <div class="project-list pager-list" id="data-uncreate"></div>
                        </div>
                    </div>
                </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $("select").select();

            //下载报表
            $("#ccs").click(function(){
                $("#searchForm").attr("action","${basePath!}/fund/settlementDetail/SettlementPoi");
                $("#searchForm").submit();
            });
            laydate({
                elem: '#startDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });

            laydate({
                elem: '#createDate2',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDate2',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });


            $("#paymentType").select();

            var $pager = $("#data").pager({
                url:"${basePath!}/fund/settlementDetail/settlementDetailManagerList?fid=${fid!}",
                formId:"searchForm",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>交易单号</th>'+
                    '<th>商户名称</th>'+
                    '<th>所属运营商</th>'+
                    '<th>出账时间</th>'+
                    '<th>结算时间</th>'+
                    '<th>结算状态</th>'+
                    '<th>结算状态(平台)</th>'+
                    '<th>结算状态(运营商)</th>'+
                    '<th>商户结算</th>'+
                    '<th>运营商结算</th>'+
                    '<th>平台结算</th>'+
                    '<th>结算金额</th>'+
                    '<th>操作</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{mname}</td>'+
                    '<td>{oname}</td>'+
                    '<td>{outSettlementDate}</td>'+
                    '<td>{settlementTime}</td>'+
                    '<td>{status}</td>'+
                    '<td>{platStatus}</td>'+
                    '<td>{operStatus}</td>'+
                    '<td>{oamount}</td>'+
                    '<td>{operatorShareFee} </td>'+
                    '<td>{platfromShareFee}</td>'+
                    '<td>{sumamount}</td>'+
                    '<td id="opera_{id}"><a href="javascript:void(0);"  data="{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 交易明细 </a> </td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="17"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    $(".detail").click(function () {
                        var id=$(this).attr("data");
                        window.location.href="${basePath!}/fund/settlementDetail/qux?id="+id;
                    })
                    var list = result.data.records;
                    console.log(list)
                    for(var i = 0 ; i < list.length ; i++){
                        if(list[i].status == '待确认' && list[i].operStatus == '待结算' && list[i].platStatus == '待结算'){
                            $("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm online"><i class="fa fa-arrow-circle-up"></i> 线上结算 </a> ');
                        }
                    }
                    //线上结算
                    $(".online").click(function () {
                        var id=$(this).attr("data");
                        $.ajax({
                            url:'${basePath!}/fund/settlementDetail/underlineSettlement?id='+id,
                            type:'post', //数据发送方式
                            dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                            success: function(data){ //成功
                                //消息对话框
                                if(data.code==0) {
                                    parent.messageModel(data.msg);
                                    window.history.go(0);
                                    parent.closeLayer();
                                }else {
                                    layer.msg(data.msg)
                                }
                            }
                        });
                            for(var i = 0 ; i < list.length ; i++){
                                if(list[i].status == '已结算' && list[i].operStatus == '已结算' && list[i].platStatus == '已结算'){
                                    $("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-up"></i> 结算成功 </a> ');
                                }
                            }
                    })
                    // loadAmount();
                    loadAmount2();
                    loadAmount3();
                }
            });

            <#--function loadAmount() {-->
                <#--$.ajax({-->
                    <#--url: "${basePath!}/fund/settlementDetail/Maids?fid=${fid!}",-->
                    <#--type: 'POST', //GET-->
                    <#--data: $("#searchForm").serialize(),-->
                    <#--timeout: 30000,    //超时时间-->
                    <#--dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text-->
                    <#--success: function (dd) {-->
                        <#--$("#opaShareAmount").html(dd.data.opa_share_amount);-->
                        <#--$("#redirectShareAmount").html(dd.data.redirect_share_amount);-->
                        <#--$("#inderectShareAmount").html(dd.data.inderect_share_amount);-->
                        <#--console.log(dd);-->
                    <#--}-->
                <#--});-->
            <#--}-->

            function loadAmount2() {
                $.ajax({
                    url: "${basePath!}/fund/settlementDetail/DAmount?fid=${fid!}",
                    type: 'POST', //GET
                    data: $("#searchForm").serialize(),
                    timeout: 30000,    //超时时间
                    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success: function (ddd) {
                        $("#DAmount").html(ddd.data.dsumamount);
                        console.log(ddd);
                    }
                });
            }

            function loadAmount3() {
                $.ajax({
                    url: "${basePath!}/fund/settlementDetail/YAmount?fid=${fid!}",
                    type: 'POST', //GET
                    data: $("#searchForm").serialize(),
                    timeout: 30000,    //超时时间
                    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success: function (dddd) {
                        $("#YAmount").html(dddd.data.ysumamount);
                        console.log(dddd);
                    }
                });
            }
            $("#loading-example-btn").click(function(){
                $pager.gotoPage($pager.pageNumber);
            });

        });

    </script>

</body>
</html>
