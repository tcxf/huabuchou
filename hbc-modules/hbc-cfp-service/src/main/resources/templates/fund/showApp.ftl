<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<style>
		#demo{
			font-size:14px;
			font-weight:600;
			margin-left:15px;
		}
		.ibox-content li{
			float:left;
			width:20%;
			heigth:40px;
			list-style:none;
			
			
		}
		#yys li{
        	list-style:none;
	</style>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white doExamine" id="doExamine" type="button" value="审核" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
		</div>
	</div>
	<form id="_f">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<!-- 提额总额度 -->
				<input type="hidden" id="totalQuota" name="totalQuota" />
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
					<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>
					<li><a data-toggle="tab" href="#company" aria-expanded="true"> 公司信息</a></li>
					<li><a data-toggle="tab" href="#ugren" aria-expanded="true"> 紧急联系人</a></li>
<!-- 					<li><a data-toggle="tab" href="#zmxy" aria-expanded="true"> 芝麻信用</a></li> -->
					<!-- <li><a data-toggle="tab" href="#owner" aria-expanded="true"> 产权信息</a></li> -->
					<!--  <li><a data-toggle="tab" href="#gjj" aria-expanded="true"> 公积金</a></li>
					<li><a data-toggle="tab" href="#sb" aria-expanded="true"> 社保</a></li>
					<li><a data-toggle="tab" href="#jd" aria-expanded="true"> 京东数据</a></li>-->
					<li><a data-toggle="tab" href="#xyk" aria-expanded="true"> 信用卡</a></li>
					<li><a data-toggle="tab" href="#yys" aria-expanded="true"> 运营商数据</a></li>
					<li><a data-toggle="tab" href="#xx" aria-expanded="true"> 学信数据</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="process" class="ibox-content active tab-pane">
						<input type="hidden" id="uid"; name="id" value="${id}"/>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-4 control-label">审核结果</label>
								<div class="col-sm-8">
									<select class="dfinput" id="ret" style="width:100px;">
										<option value="pass">审核通过</option>
										<option value="nopass">审核拒绝</option>
									</select>
								</div>
							</div>
							<!-- <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div> -->
						</div>
						<!-- <div class="hr-line-dashed" style="clear:both;"></div> -->
						<div class="pass process">
							<div class="form-group col-sm-4">
								<label class="col-sm-4 control-label">额度状态</label>
								<div class="col-sm-8">
									<select name="authStatus" id="authStatus">
										<option value="true" <c:if test="${id != null && entity.authStatus}">selected</c:if>>已授信</option>
										<option value="false" <c:if test="${id == null || !entity.authStatus}">selected</c:if>>未授信</option>
									</select>
								</div>
							</div>
							<!--<div class="form-group col-sm-4">
								<label class="col-sm-4 control-label">额度上限</label>
								<div class="col-sm-8"> 
									<input type="text" id="authMax" class="form-control" value="" validate-rule="required,money,maxLength[8]" validate-msg="money:最多8位数字且只能为数字,maxlength:8" style="margin-bottom: 20px;" placeholder="请填写授信金额">
								</div>
							</div>  -->
						</div>
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						<div class="pass process">
							<div class="form-group col-sm-4">
								<label class="col-sm-4 control-label">出账日期</label>
								<div class="col-sm-8">
									<select name="ooaDate" id="ooaDate">
										<c:forEach begin="3" end="27" var="i">
										<option value="${i}" <c:if test="${i == 5}">selected</c:if>>每月${i}号</option>
										</c:forEach>
									</select>
								</div>
							</div>
							
							<div class="form-group col-sm-4">
								<label class="col-sm-4 control-label">还款日期</label>
								<div class="col-sm-8">
									<select name="repayDate" id="repayDate" validate-rule="largeTo[#ooaDate]" validate-msg="largeTo:还款日必须大于出账日">
										<c:forEach begin="3" end="27" var="i">
										<option value="${i}" <c:if test="${i == 27}">selected</c:if>>每月${i}号</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
							<div class="form-group col-sm-4">		
								<input type="hidden" id="max" value="${Max}"> 
								<div id="demo">授信综合额度:<span id="max_span">${Max}</span></div>
							</div>
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						
						
						
						<div class="nopass nopass process" style="display:none;">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">原因</label>
								<div class="col-sm-10">
									<textarea id="reason" rows="10" style="width:80%"></textarea>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed nopass process" style="clear:both;display:none;"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="base" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">真实姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.realName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">身份证号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">学历</label>
								<label id="a-cultural" class="col-sm-8 control-label" style="text-align: left;">
									${entity.education}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">婚姻状态</label>
								<label id="hunyin" class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${id != null && entity.isMarry}">已婚</c:if>
									<c:if test="${id == null || !entity.isMarry}">未婚</c:if>
								</label>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">现居住地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.liveDisplayName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.liveNow}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">居住时间</label>
								<label id="residence" class="col-sm-8 control-label" style="text-align: left;">
									${entity.liveTime} 年
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">所属运营商</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operaName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">身份证正面</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${res}${entity.idcardFront}" id="front"/>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">身份证反面</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${res}${entity.idcardBack}" id="back"/>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<div id="company" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyTel}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位性质</label>
								<label id="unit" class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyType}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">工作时长</label>
								<label id="work-time" class="col-sm-8 control-label" style="text-align: left;">
									${entity.workTimeStr}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">职位</label>
								<label id="a-job" class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyPosition}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">薪资</label>
								<label id="salary" class="col-sm-8 control-label" style="text-align: left;">
								<c:if test="${entity.wages =='1'}">2000元以下</c:if>
									<c:if test="${entity.wages =='2'}">2000-3000元</c:if>
									<c:if test="${entity.wages =='3'}">3000-4500元</c:if>
									<c:if test="${entity.wages =='4'}">4500-6000元</c:if>
									<c:if test="${entity.wages =='5'}">6000-8000元</c:if>
									<c:if test="${entity.wages =='6'}">8000-10000元</c:if>
									<c:if test="${entity.wages =='7'}">10000-15000元</c:if>
									<c:if test="${entity.wages =='8'}">15000-20000元</c:if>
									<c:if test="${entity.wages =='9'}">20000元以上</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyDisplayName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyAddress}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<div id="ugren" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">直系联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">直系联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenRelationship}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">非直系联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenNameNormal}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">非直系联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobileNormal}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenRelationshipNormal}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					<div id="owner" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车牌号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.carNumber}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.drivingLicense}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">驾驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.driverLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车辆归属人</label>
								<label id="a-car" class="col-sm-8 control-label" style="text-align: left;">${entity.carRelationship}</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房产证编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房屋归属人</label>
								<label id="a-home" class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseBelong}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseRelate}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<!-- 公积金 -->
					<div id="gjj" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">公积金账号：<b id="gjj_sbzh"></b></label>
									<label class="col-sm-3 control-label">身份证号：<b id="gjj_sfzh"></b></label>
									<label class="col-sm-3 control-label">姓名：<b id="gjj_xm"></b></label>
									<label class="col-sm-3 control-label">性别：<b id="gjj_xb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">出生日期：<b id="gjj_csrq"></b></label>
									<label class="col-sm-3 control-label">手机号码：<b id="gjj_sjhm"></b></label>
									<label class="col-sm-3 control-label">邮件地址：<b id="gjj_yjdz"></b></label>
									<label class="col-sm-3 control-label">通信地址：<b id="gjj_txdz"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">缴纳企业名称：<b id="gjj_jnqy"></b></label>
									<label class="col-sm-3 control-label">账户余额：<b id="gjj_zhye"></b></label>
									<label class="col-sm-3 control-label">月缴存：<b id="gjj_yjc"></b></label>
									<label class="col-sm-3 control-label">账号状态：<b id="gjj_zhzt"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">贷款期限：<b id="gjj_dkqx"></b></label>
									<label class="col-sm-3 control-label">贷款总额：<b id="gjj_dkze"></b></label>
									<label class="col-sm-3 control-label">贷款余额：<b id="gjj_dkye"></b></label>
									<label class="col-sm-3 control-label">还款方式：<b id="gjj_hkfs"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                               	<div class="panel-heading">
                                  	<h4 class="panel-title" style="text-align: center">
                                  	<a href="#cc3" data-toggle="collapse">
                                    	<img src="<%=path %>/img/logo_down.png" style="width:25px;height:22px" alt=""/>
                                  	</a>
                                  	</h4>
                               </div>
                               <div class="panel-collapse collapse" id="cc3">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>日期</td>
			                                        <td>单位名称</td>
			                                        <td>发生金额（收入为正、支出为负）</td>
			                                        <td>余额</td>
			                                        <td>缴费基数</td>
			                                        <td>缴费基年月</td>
			                                        <td>业务描述</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="gjj_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 社保 -->
					<div id="sb" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">社保账号：<b id="sb_sbzh"></b></label>
									<label class="col-sm-3 control-label">身份证号：<b id="sb_sfzh"></b></label>
									<label class="col-sm-3 control-label">姓名：<b id="sb_xm"></b></label>
									<label class="col-sm-3 control-label">性别：<b id="sb_xb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">出生日期：<b id="sb_csrq"></b></label>
									<label class="col-sm-3 control-label">手机号码：<b id="sb_sjhm"></b></label>
									<label class="col-sm-3 control-label">邮件地址：<b id="sb_yjdz"></b></label>
									<label class="col-sm-3 control-label">通信地址：<b id="sb_txdz"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">缴纳企业名称：<b id="sb_jnqy"></b></label>
									<label class="col-sm-3 control-label">工资基数：<b id="sb_gzjs"></b></label>
									<label class="col-sm-3 control-label">账号状态：<b id="sb_zhzt"></b></label>
									<label class="col-sm-3 control-label">开户日期：<b id="sb_khrq"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">余额：<b id="sb_ye"></b></label>
									<label class="col-sm-3 control-label">缴存基数：<b id="sb_jcjs"></b></label>
									<label class="col-sm-3 control-label">累计缴费月数：<b id="sb_ljjfys"></b></label>
									<label class="col-sm-3 control-label">最近一次缴费日期：<b id="sb_zjjfrq"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                               	<div class="panel-heading">
                                  	<h4 class="panel-title" style="text-align: center">
                                  	<a href="#cc4" data-toggle="collapse">
                                    	<img src="<%=path %>/img/logo_down.png" style="width:25px;height:22px" alt=""/>
                                  	</a>
                                  	</h4>
                               </div>
                               <div class="panel-collapse collapse" id="cc4">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>日期</td>
			                                        <td>单位名称</td>
			                                        <td>发生金额（收入为正、支出为负）</td>
			                                        <td>缴费基数</td>
			                                        <td>缴费基年月</td>
			                                        <td>业务描述</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="sb_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 信用卡 -->
					<div id="xyk" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">姓名：<b id="xyk_xm"></b></label>
									<label class="col-sm-3 control-label">清算币种：<b id="xyk_qsbz"></b></label>
									<label class="col-sm-3 control-label">信用额度：<b id="xyk_xyed"></b></label>
									<label class="col-sm-3 control-label">可取现额度：<b id="xyk_kqxed"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
					<!-- @@@@@@@@@@@@@@@  运营商数据 @@@@@@@@@@@@@@@@@@-->
					<div id="yys" class="ibox-content tab-pane">
						<!-- <div class="hr-line-dashed" style="clear:both;"></div> -->
						<ul>
							<c:forEach items="${tolist }" var="t">
							<li>
								<label class="col-sm-4 control-label">最新通话时间</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.callTime}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">对方号码归属地</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.location}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">通话类型</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.callTypeStr}
								</label>
							</li>
							<li>
							<label class="col-sm-4 control-label">电话号码</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.oppositeNo}
								</label>
								
							</li>
							<li>
							<label class="col-sm-4 control-label">次数</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.duration}
								</label>
								
							</li>
							<div class="hr-line-dashed" style="clear:both;"></div>
							</c:forEach>
						</ul>
								
					</div>
					<!-- @@@@@@@@@@@@@@@  京东数据 @@@@@@@@@@@@@@@@@@-->
					<div id="jd" class="ibox-content tab-pane">
						<ul>
							<c:forEach items="${jdList }" var="t">
							<li>
								<label class="col-sm-4 control-label">收货人姓名</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.receiver}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">收货人电话</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.phoneNumber}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">收货地址</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.location}
								</label>
							</li>
							<li>
							<label class="col-sm-4 control-label">收货详细地址</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.receiveAddress}
								</label>
								
							</li>
							<div class="hr-line-dashed" style="clear:both;"></div>
							</c:forEach>
						</ul>
					</div>
					<div id="xx" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">学历</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${xxw}
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</div>
	</form>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<%-- <script type="text/javascript" src="<%=path%>/js/improveQuota.js"></script> --%>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
		$(document).ready(function () {
			$("#ret").select();
			var ret = 'pass';
			$("#ret").change(function(){
				$(".process").hide();
				$("."+$(this).val()).show();
				ret = $(this).val();
			});
			$("#authStatus").select();
			$("#authStatus").change(function(){
				if($(this).val() == "true"){
					$(".sx").removeAttr("disabled");
				}else{
					$(".sx").attr("disabled",true);
				}
			});
			$("#ooaDate").select();
			$("#repayDate").select();
			$("#authStatus").trigger("change");
			$("#front").css('width','100%');
			$("#front").css('height',$(document).width()/6);
			$("#front").click(function(){
				window.open($(this).attr('src'));
			})
			$("#back").css('width','100%');
			$("#back").css('height',$(document).width()/6);
			$("#back").click(function(){
				window.open($(this).attr('src'));
			});
			$(".doExamine").click(function(){
				if(!$("#_f").validate()) return;
				var data = {};
				if(!$("#authMax").is(":hidden")){
					var authMax = $.trim($("#max").val());
					var authStatus = $.trim($("#authStatus").val());
					var ooaDate = $.trim($("#ooaDate").val());
					var repayDate = $.trim($("#repayDate").val());
					
					if(parseInt(ooaDate) > parseInt(repayDate)){
						layer.alert('还款日必须大于出账日');
						$("#repayDate").focus();
						return;
					}
					alert(authMax);
					data.authMax = authMax;
					data.authStatus = authStatus;
					data.ooaDate = ooaDate;
					data.repayDate = repayDate;
				}
				
				if(!$("#reason").is(":hidden")){
					var reason = $.trim($("#reason").val());
					if(reason.length < 10){
						layer.alert('失败原因至少需要10个字');
						$("#reason").focus();
						return;
					}
					if(reason.length > 100){
						layer.alert('失败原因最多只能100个字');
						$("#reason").focus();
						return;
					}
					if(!isNaN(reason)){
						layer.alert('失败原因不能为数字');
						$("#reason").focus();
						return;
					}
					/*var regex = /^[\u4E00-\u9FA5\uF900-\uFA2D\x20，。？…!,.;'\[\]{}~!@#$%^&*()_+|\-\\~！@#￥%……&*（）——+|、]+$/;
					if(!regex.test(reason)){
						layer.alert('失败原因不能包含特殊字符');
						$("#reason").focus();
						return;
					}*/
					data.reason = reason;
				} 
				data.ret = ret;
				data.id = '${id}';
				$.post('${hsj}/fundend/doExamine.htm',data,function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != -1){ 
						parent.c.gotoPage(null);  
						parent.closeLayer();  
					}
				},'json');
			});
			
			$(".cancel").click(function(){parent.closeLayer();});
		});
	</script>
</body>

</html>
