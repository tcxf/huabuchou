<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">
		<style type="text/css">
			#yys li{
        	list-style:none;
		</style>
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#auth" aria-expanded="true"> 授信信息</a></li>
					<li><a data-toggle="tab" href="#company" aria-expanded="true"> 公司信息</a></li>
					<li><a data-toggle="tab" href="#ugren" aria-expanded="true"> 紧急联系人</a></li>
					<li><a data-toggle="tab" href="#owner" aria-expanded="true"> 产权信息</a></li>
			<!-- 	<li><a data-toggle="tab" href="#gjj" aria-expanded="true"> 公积金</a></li>
					<li><a data-toggle="tab" href="#sb" aria-expanded="true"> 社保</a></li>
					<li><a data-toggle="tab" href="#jd" aria-expanded="true"> 京东数据</a></li> -->
					<li><a data-toggle="tab" href="#xyk" aria-expanded="true"> 信用卡</a></li>
					<li><a data-toggle="tab" href="#yys" aria-expanded="true"> 运营商数据</a></li>
					<li><a data-toggle="tab" href="#xx" aria-expanded="true"> 学信数据</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${id}"/>
							<c:if test="${id != null}">
						  	<div class="form-group">
								<label class="col-sm-2 control-label">账户id</label>
								<label id="unum" class="col-sm-10 control-label" style="text-align: left;">
									${entity.unum}
								</label>
							</div>
							</c:if>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">账户姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.realName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">身份照号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">开户行</label>
								<div class="col-sm-8" id="bank" input-name="bankSn" input-val="${entity.bankSn}"></div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">银行卡</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.bankCard}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机卡密码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.deMobilePwd}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">紧急联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">紧急联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">所属地区</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.displayName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">婚姻状况</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${id != null && entity.isMarry}">已婚</c:if>
									<c:if test="${id == null || !entity.isMarry}">未婚</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">账户状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${id != null && entity.status}">启用</c:if>
									<c:if test="${id == null || !entity.status}">禁用</c:if>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">所属运营商</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operaName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="auth" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">额度状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<c:if test="${id != null && entity.authStatus}">已授信</c:if>
									<c:if test="${id == null || !entity.authStatus}">未授信</c:if>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">额度上限</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.authMax}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">出账日期</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ooaDate}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">还款日期</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.repayDate}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					<div id="company" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyTel}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位性质</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyType}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">工作时长</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.workTimeStr}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">职位</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyPosition}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">薪资</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.wagesStr}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">单位地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyDisplayName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.companyAddress}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<div id="ugren" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">直系联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">直系联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobile}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenRelationship}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">非直系联系人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenNameNormal}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">非直系联系人电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenMobileNormal}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.ugrenRelationshipNormal}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					<div id="owner" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车牌号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.carNumber}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.drivingLicense}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">驾驶证号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.driverLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">车辆归属人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">${entity.carRelationship}</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房产证编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseLicense}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">房屋归属人</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseBelong}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">与本人关系</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.houseRelate}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;"></label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
						<!-- 公积金 -->
					<div id="gjj" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">公积金账号：<b id="gjj_sbzh"></b></label>
									<label class="col-sm-3 control-label">身份证号：<b id="gjj_sfzh"></b></label>
									<label class="col-sm-3 control-label">姓名：<b id="gjj_xm"></b></label>
									<label class="col-sm-3 control-label">性别：<b id="gjj_xb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">出生日期：<b id="gjj_csrq"></b></label>
									<label class="col-sm-3 control-label">手机号码：<b id="gjj_sjhm"></b></label>
									<label class="col-sm-3 control-label">邮件地址：<b id="gjj_yjdz"></b></label>
									<label class="col-sm-3 control-label">通信地址：<b id="gjj_txdz"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">缴纳企业名称：<b id="gjj_jnqy"></b></label>
									<label class="col-sm-3 control-label">账户余额：<b id="gjj_zhye"></b></label>
									<label class="col-sm-3 control-label">月缴存：<b id="gjj_yjc"></b></label>
									<label class="col-sm-3 control-label">账号状态：<b id="gjj_zhzt"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">贷款期限：<b id="gjj_dkqx"></b></label>
									<label class="col-sm-3 control-label">贷款总额：<b id="gjj_dkze"></b></label>
									<label class="col-sm-3 control-label">贷款余额：<b id="gjj_dkye"></b></label>
									<label class="col-sm-3 control-label">还款方式：<b id="gjj_hkfs"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                               	<div class="panel-heading">
                                  	<h4 class="panel-title" style="text-align: center">
                                  	<a href="#cc3" data-toggle="collapse">
                                    	<img src="<%=path %>/img/logo_down.png" style="width:25px;height:22px" alt=""/>
                                  	</a>
                                  	</h4>
                               </div>
                               <div class="panel-collapse collapse" id="cc3">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>日期</td>
			                                        <td>单位名称</td>
			                                        <td>发生金额（收入为正、支出为负）</td>
			                                        <td>余额</td>
			                                        <td>缴费基数</td>
			                                        <td>缴费基年月</td>
			                                        <td>业务描述</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="gjj_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 社保 -->
					<div id="sb" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">社保账号：<b id="sb_sbzh"></b></label>
									<label class="col-sm-3 control-label">身份证号：<b id="sb_sfzh"></b></label>
									<label class="col-sm-3 control-label">姓名：<b id="sb_xm"></b></label>
									<label class="col-sm-3 control-label">性别：<b id="sb_xb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">出生日期：<b id="sb_csrq"></b></label>
									<label class="col-sm-3 control-label">手机号码：<b id="sb_sjhm"></b></label>
									<label class="col-sm-3 control-label">邮件地址：<b id="sb_yjdz"></b></label>
									<label class="col-sm-3 control-label">通信地址：<b id="sb_txdz"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">缴纳企业名称：<b id="sb_jnqy"></b></label>
									<label class="col-sm-3 control-label">工资基数：<b id="sb_gzjs"></b></label>
									<label class="col-sm-3 control-label">账号状态：<b id="sb_zhzt"></b></label>
									<label class="col-sm-3 control-label">开户日期：<b id="sb_khrq"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">余额：<b id="sb_ye"></b></label>
									<label class="col-sm-3 control-label">缴存基数：<b id="sb_jcjs"></b></label>
									<label class="col-sm-3 control-label">累计缴费月数：<b id="sb_ljjfys"></b></label>
									<label class="col-sm-3 control-label">最近一次缴费日期：<b id="sb_zjjfrq"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                               	<div class="panel-heading">
                                  	<h4 class="panel-title" style="text-align: center">
                                  	<a href="#cc4" data-toggle="collapse">
                                    	<img src="<%=path %>/img/logo_down.png" style="width:25px;height:22px" alt=""/>
                                  	</a>
                                  	</h4>
                               </div>
                               <div class="panel-collapse collapse" id="cc4">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>日期</td>
			                                        <td>单位名称</td>
			                                        <td>发生金额（收入为正、支出为负）</td>
			                                        <td>缴费基数</td>
			                                        <td>缴费基年月</td>
			                                        <td>业务描述</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="sb_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 信用卡 -->
					<div id="xyk" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">姓名：<b id="xyk_xm"></b></label>
									<label class="col-sm-3 control-label">清算币种：<b id="xyk_qsbz"></b></label>
									<label class="col-sm-3 control-label">信用额度：<b id="xyk_xyed"></b></label>
									<label class="col-sm-3 control-label">可取现额度：<b id="xyk_kqxed"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
					<!-- @@@@@@@@@@@@@@@  运营商数据 @@@@@@@@@@@@@@@@@@-->
					<div id="yys" class="ibox-content tab-pane">
						<!-- <div class="hr-line-dashed" style="clear:both;"></div> -->
						<ul>
							<c:forEach items="${tolist }" var="t">
							<li>
								<label class="col-sm-4 control-label">最新通话时间</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.callTime}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">对方号码归属地</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.location}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">通话类型</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.callTypeStr}
								</label>
							</li>
							<li>
							<label class="col-sm-4 control-label">电话号码</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.oppositeNo}
								</label>
								
							</li>
							<li>
							<label class="col-sm-4 control-label">次数</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.duration}
								</label>
								
							</li>
							<div class="hr-line-dashed" style="clear:both;"></div>
							</c:forEach>
						</ul>
								
					</div>
					<!-- @@@@@@@@@@@@@@@  京东数据 @@@@@@@@@@@@@@@@@@-->
					<div id="jd" class="ibox-content tab-pane">
						<ul>
							<c:forEach items="${jdList }" var="t">
							<li>
								<label class="col-sm-4 control-label">收货人姓名</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.receiver}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">收货人电话</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.phoneNumber}
								</label>
							</li>
							<li>
								<label class="col-sm-4 control-label">收货地址</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.location}
								</label>
							</li>
							<li>
							<label class="col-sm-4 control-label">收货详细地址</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
									${t.receiveAddress}
								</label>
								
							</li>
							<div class="hr-line-dashed" style="clear:both;"></div>
							</c:forEach>
						</ul>
					</div>
					<div id="xx" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">学历</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${xxw}
								</label>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	
	<script type="text/javascript" src="<%=path%>/js/improveQuota.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
		$(document).ready(function () {
			$("#oid-span").operaSelect();
			$("#authStatus").select();
			$("#authStatus").change(function(){
				if($(this).val() == "true"){
					$(".sx").removeAttr("disabled");
				}else{
					$(".sx").attr("disabled",true);
				}
			});
			$("#authStatus").trigger("change");
			$("#area").area();
			$("#bank").initBank();
			$("#status").select();
			$("#isMarry").select();
			$(".cancel").click(function(){parent.closeLayer();});

			
			<c:if test="${id != null}">
			laydate({
				elem: '#startDateJYJL', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDateJYJL', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			
			laydate({
				elem: '#startDateYQJL', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDateYQJL', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			$("#repayType").select();
			$("#tradeingTpye").select();
			
			//授信审核
			var $pager_jyjl = $("#jyjl-list").pager({
				url:"${hsj}/userInfo/tradingDetailManagerList.htm",
				formId:"sh-form",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>交易单号</th>'+
								'<th>商户id</th>'+
								'<th>商户名称</th>'+
								'<th>行业类型</th>'+
								'<th>用户id</th>'+
								'<th>用户名称</th>'+
								'<th>交易类型</th>'+
								'<th>交易状态</th>'+
								'<th>交易时间</th>'+
								'<th>交易金额</th>'+
								'<th>结算金额</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{serialNo}</td>'+
								'<td>{mnum}</td>'+
								'<td>{mname}</td>'+
								'<td>{mcname}</td>'+
								'<td>{unum}</td>'+
								'<td>{uname}</td>'+
								'<td>{tradingTypeStr}</td>'+
								'<c:if test="${tradingType==0}"><td>未支付</td></c:if><c:if test="${tradingType==0}"><td>已支付</td></c:if><td>已退款</td>'+
								'<td>{tradingDateStr}</td>'+
								'<td>{amount}</td>'+
								'<td>{settlementAmount}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					
					$("#jysel").click(function(){
						var jylx_edate =$("#endDateJYJL").val();	//结束时间
						var jylx_sdate =$("#startDateJYJL").val();	//开始时间
						 var mname = $("#manme").val();	//商户名称
						var uname = $("#uname").val();	//用户名称
						var tradeingTpye = $("#tradingType").val();	//交易状态
						var unum = $("#unum").html();
						unum =$.trim(unum);
					
						
						if (typeof(mname) == "undefined") mname="";
						if (typeof(uname) == "undefined") uname="";
							
						alert(mname);
						
						window.location.href = "<%=path %>/userInfo/Consumerdaochu.htm?startDate="+jylx_sdate+"&endDate="+jylx_edate+"&mname="+mname+"&uname="+uname+"&tradeingTpye="+tradeingTpye+"&unum="+unum;
					});
					/*if(${tradingType}==0){
						$("#tradingType").html("未支付");
					}else if(${tradingType}==1){
						$("#tradingType").html("已支付");
					}*/
					$("#go_yq").click(function(){
						var jylx_edate =$("#endDateYQJL").val();	//结束时间
						var jylx_sdate =$("#startDateYQJL").val();	//开始时间
						var yhid = $("#yhid").val();	//用户id
						var yhname = $("#yhname").val();	//用户姓名
						var yhphone = $("#yhphone").val();	//用户手机
						var unum = $("#unum").html();
						unum =$.trim(unum);
						window.location.href = "<%=path%>/userInfo/yqdaochu.htm?startDate="+jylx_sdate+"&endDate="+jylx_edate+"&unum="+unum+"&uname="+yhname+"&mobile="+yhphone;
					});
					
					$("#go_hk").click(function(){
						var tradeingTpye = $("#repayType").val();	//交易状态
						var unum = $("#unum").html();
						unum =$.trim(unum);
						window.location.href = "<%=path%>/userInfo/repaydaochu.htm?&tradeingTpye="+tradeingTpye+"&status=2"+"&unum="+unum;
					});

					loadJyjlAmount();
				}
			});
			
			$("#jyjl-search").click(function(){
				$pager_jyjl.gotoPage(1);
			});
			
			function loadJyjlAmount(){
				$.ajax({
					url:"<%=path%>/userInfo/totalJyjlAmount.htm",
				    type:'POST', //GET
				    data:$("#jyjl-form").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#totalJyjlAmount").html(d.data);
				    }
				});
			}
			
			
			//还款纪录
			var $pager_hkjl = $("#hkjl-list").pager({
				url:"${hsj}/userInfo/repaymentPlanManagerList.htm",
				formId:"hkjl-form",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>流水单号</th>'+
								'<th>用户id</th>'+
								'<th>还款状态</th>'+
								'<th>用户姓名</th>'+
								'<th>应还本金</th>'+
								'<th>滞纳金</th>'+
								'<th>分期利息</th>'+
								'<th>本期应还</th>'+
								'<th>期数</th>'+
								'<th>还款日</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{repaymentSn}</td>'+
								'<td>{unum}</td>'+
								'<td id="status_{id}"></td>'+
								'<td>{uname}</td>'+
								'<td>¥ {currentCorpus} 元</td>'+
								'<td>¥ {lateFeeAct} 元</td>'+
								'<td>¥ {currentFee} 元</td>'+
								'<td id="yh_{id}">¥ {currentCorpus + lateFee + currentFee} 元</td>'+
								'<td>{currentTime}/{totalTime}</td>'+
								'<td>{repaymentDateStr}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					for(var i = 0 ; i < result.list.length ; i++){
						var item = result.list[i];
						$("#yh_"+item.id).html("¥ " + (parseFloat(item.currentCorpus)+parseFloat(item.lateFeeAct)+parseFloat(item.currentFee)).toFixed(2) + " 元");
					
						if(item.status==1){
							$("#status_"+item.id).html("已还");
						}
						if(item.status==2){
							$("#status_"+item.id).html("未还");
						}
						if(item.status==3){
							$("#status_"+item.id).html("已分期");
						}
					}
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					loadHkAmount();
				}
			});

			function loadHkAmount(){
				$.ajax({
					url:"<%=path%>/userInfo/loadHkAmount.htm",
				    type:'POST', //GET
				    data:$("#hkjl-form").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#yhAmount").html(d.data.yhAmount);
				    	$("#whAmount").html(d.data.whAmount);
				    	$("#yqAmount").html(d.data.yqAmount);
				    }
				});
			}
			

			$("#hkjl-search").click(function(){
				$pager_hkjl.gotoPage(1);
			});
			
			var $pager_yqjl = $("#yqjl-list").pager({
				url:"${hsj}/userInfo/yqManagerList.htm",
				formId:"yqjl-form",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>流水单号</th>'+
								'<th>用户id</th>'+
								'<th>期数</th>'+
								'<th>逾期本金</th>'+
								'<th>滞纳金</th>'+
								'<th>利息费</th>'+
								'<th>逾期应还</th>'+
								'<th>还款日</th>'+
								'<th>逾期天数</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{repaymentSn}</td>'+
								'<td>{unum}</td>'+
								'<td>{currentTime}/{totalTime}</td>'+
								'<td>¥ {currentCorpus} 元</td>'+
								'<td>¥ {lateFeeAct} 元</td>'+
								'<td>¥ {currentFee} 元</td>'+
								'<td>¥ {currentCorpus + lateFeeAct + currentFee} 元</td>'+
								'<td>{repaymentDateStr}</td>'+
								'<td>{yqDateCount} 天</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					loadYqAmount();
				}
			});
			

			function loadYqAmount(){
				$.ajax({
					url:"<%=path%>/userInfo/loadYqAmount.htm",
				    type:'POST', //GET
				    data:$("#yqjl-form").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#totalAmount").html(d.data.formatTotalAmount);
				    	$("#currentCorpus").html(d.data.formatCurrentCorpus);
				    	$("#currentFee").html(d.data.formatCurrentFee);
				    	$("#lateFee").html(d.data.formatLateFee);
				    }
				});
			}
			

			$("#yqjl-search").click(function(){
				$pager_yqjl.gotoPage(1);
			});
			</c:if>
		});
	</script>
</body>

</html>
