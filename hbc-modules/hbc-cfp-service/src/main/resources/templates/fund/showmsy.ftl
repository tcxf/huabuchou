<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white doExamine" id="doExamine" type="button" value="审核" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
					<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>
					<li><a data-toggle="tab" href="#grgs" aria-expanded="true"> 个人工商信息</a></li>
					<li><a data-toggle="tab" href="#dtjd" aria-expanded="true"> 多头借贷</a></li>
					<li><a data-toggle="tab" href="#yhk" aria-expanded="true"> 银行卡</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="process" class="ibox-content active tab-pane">
						<input type="hidden" name="id" id="mid" value="${id}"/>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">审核结果</label>
								<div class="col-sm-8">
									<select class="dfinput" id="ret" style="width:100px;">
										<option value="pass">审核通过</option>
										<option value="nopass">审核拒绝</option>
									</select>
								</div>
							</div>
							
							
							<div class="col-sm-6">
								授信金额：<input type="text" id="je" value="${entity.authMax}"/>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
				<div>
							<div class="form-group col-sm-6">
						               出账日<input type="text" id="ooaDate" value=""/>
								
							</div>
							
							
							<div class="col-sm-6">
								还款日：<input type="text" id="repayDate" value=""/>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
				
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						
						
						
						<div class="nopass nopass process" style="display:none;">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">原因</label>
								<div class="col-sm-10">
									<textarea id="reason" rows="10" style="width:80%"></textarea>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed nopass process" style="clear:both;display:none;"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="base" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mcName}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.phoneNumber}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺介绍</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.recommendGoods}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">开店时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.openDate}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">位置</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.displayName}
								</label>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">地图位置</label>
								<div class="col-sm-10" id="container" style="height:350px;">
									
								</div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">门店照片</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${res}${entity.localPhoto}" id="front"/>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">营业执照</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${res}${entity.licensePic}" id="back"/>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
					
					<!-- 个人工商信息 -->
					<div id="grgs" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">法人姓名：<b id="gs_frxm"></b></label>
									<label class="col-sm-3 control-label">企业名称：<b id="gs_qymc"></b></label>
									<label class="col-sm-3 control-label">注册号：<b id="gs_zch"></b></label>
									<label class="col-sm-3 control-label">注册资本：<b id="gs_zczb"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">企业状态：<b id="gs_qyzt"></b></label>
									<label class="col-sm-3 control-label">被执行人姓名：<b id="gs_bzxrxm"></b></label>
									<label class="col-sm-3 control-label">被执行人身份证：<b id="gs_bzxrsfz"></b></label>
									<label class="col-sm-3 control-label">被执行人案件状态：<b id="gs_bzxrajzt"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">失信被执行人名称：<b id="gs_sxmc"></b></label>
									<label class="col-sm-3 control-label">失信被执行人身份证/组织机构代码：<b id="gs_sxsfz"></b></label>
									<label class="col-sm-3 control-label">失信被执行人法人姓名：<b id="gs_sxfr"></b></label>
									<label class="col-sm-3 control-label">失信具体情形：<b id="gs_sxqx"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">行政处罚当事人：<b id="gs_xzcfdsr"></b></label>
									<label class="col-sm-3 control-label">行政处罚证件号：<b id="gs_xzcfzjh"></b></label>
									<label class="col-sm-5 control-label">行政处罚主要违法事实：<b id="gs_xzcfwfss"></b></label>
								</div>
							
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
					<!-- 多头借贷 -->
					<div id="dtjd" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
                               <div class="panel-collapse">
                                 	<div class="panel-body">
                                   		<table border="1" class="table table-hover table-bordered">
                                      		<thead class="thd">
                                        		<tr>
			                                    	<td>黑名单类型</td>
			                                        <td>黑名单事实类型</td>
			                                        <td>黑名单事实</td>
			                                        <td>事件涉及金额</td>
			                                        <td>事件发生日期</td>
		                                        </tr>
                                      		</thead>
                                      		<tbody class="tbd" id="sb_tbd">
                                        		
						                	</tbody>
						            	</table>
                                	</div>
                            	</div>
                          	</div>
						</div>
					</div>
					
					<!-- 银行卡信息 -->
					<div id="yhk" class="ibox-content tab-pane">
						<div id=m-menu6>
							<div class="panel panel-default">
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">银行简称：<b id="yh_yhjc"></b></label>
									<label class="col-sm-3 control-label">银行卡号：<b id="yh_yhkh"></b></label>
									<label class="col-sm-3 control-label">银行logo：<img id="yh_logo" src=""></label>
									<label class="col-sm-3 control-label">银行名称：<b id="yh_yhqc"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-12">
									<label class="col-sm-3 control-label">银行网址：<b id="yh_yhwz"></b></label>
									<label class="col-sm-3 control-label">卡类型：<b id="yh_klx"></b></label>
									<label class="col-sm-3 control-label">银行卡种类：<b id="yh_yhkzl"></b></label>
									<label class="col-sm-3 control-label">英文全称：<b id="yh_ywqc"></b></label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
                          	</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script type="text/javascript" src="<%=path%>/js/improveQuotaMerchant.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script>
		$(document).ready(function () {
		
			var lat = "${(entity.lat == null || entity.lat == '')?'39.90923':entity.lat}";
			var lng = "${(entity.lng == null || entity.lng == '')?'116.397428':entity.lng}";
			var sellat = "${(entity.lat == null || entity.lat == '')?'':entity.lat}";
			var sellng = "${(entity.lng == null || entity.lng == '')?'':entity.lng}";
			map = new AMap.Map('container', {
	            center: [lng, lat],
	            zoom: 13
	        });
	        map.plugin(["AMap.ToolBar"], function() {
	            map.addControl(new AMap.ToolBar());
	        });
	        
	        var _onClick = function(e){
	        	map.clearMap();
	        	
	        	sellat = e.lnglat.lat;
	        	sellng = e.lnglat.lng;
	            new AMap.Marker({
	                position : e.lnglat,
	                map : map
	            });
	        };
	        map.on('click', _onClick);//绑定
	        
	        if(sellat != "" && sellat != ""){
				new AMap.Marker({
	                position : new AMap.LngLat(sellng,sellat),
	                map : map
	            });
	        }
			
			$("#shareFlag").select();
			$("#ret").select();
			var ret = 'pass';
			$("#ret").change(function(){
				$(".process").hide();
				$("."+$(this).val()).show();
				ret = $(this).val();
			});
			$("#settlementLoop").select();
			$("#front").css('width','100%');
			$("#front").css('height',$(document).width()/6);
			$("#front").click(function(){
				window.open($(this).attr('src'));
			})
			$("#back").css('width','100%');
			$("#back").css('height',$(document).width()/6);
			$("#back").click(function(){
				window.open($(this).attr('src'));
			});
		 	$(".doExamine").click(function(){
				var data = {};
				if($("#ret").val()=='pass'){
					var fid = $.trim($("#hStatus").val());
					var je = $.trim($("#je").val());
					var ooaDate = $.trim($("#ooaDate").val());
					var repayDate = $.trim($("#repayDate").val());
					data.fid = fid;
					data.je = je;
					data.ooaDate = ooaDate;
					data.repayDate = repayDate;
				} 
				
				if($("#ret").val()=='nopass'){
					var reason = $.trim($("#reason").val());
					if(reason.length < 10){
						layer.alert('失败原因至少需要10个字');
						$("#reason").focus();
						return;
					}
					if(reason.length > 100){
						layer.alert('失败原因最多只能100个字');
						$("#reason").focus();
						return;
					}
					if(!isNaN(reason)){
						layer.alert('失败原因不能为数字');
						$("#reason").focus();
						return;
					}
					data.reason = reason;
				}
				data.ret = ret;
				data.lat = sellat;
				data.lng = sellng;
				data.id = '${id}';
				$.post('${hsj}/fundend/sydoExamine.htm',data,function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != -1){
						parent.closeLayer();
						parent.c.gotoPage(null);
					}
				},'json');
			});
			
			$(".cancel").click(function(){parent.closeLayer();});
	
			
		});
	</script>
</body>

</html>
