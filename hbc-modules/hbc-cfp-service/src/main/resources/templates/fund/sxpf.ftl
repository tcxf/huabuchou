<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <style>
        table{
            width: 80%!important;
            margin-left: 2%;
        }
        #bj{
            padding:5px 8px;
            background: #f49110;
            border-radius: 8px;
            float: right;
            color: #fff!important;
        }
        .left_mume .xuanzhong>a{
            background: #f49110;
            color: #fff;
            display: block;
        }
        .left_mume .xuanzhong>a:hover{
            background: #f49110;
            color: #fff;
            display: block;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
        <div class="all_content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 left_mume">
                        <ul>
                            <li>
                                <a href="${basePath!}/fund/creditCustom/goToRefusingCreditPage?id=${id!}&type=${type!}">拒绝授信</a>
                            </li>
                            <li>
                                <a href="${basePath!}/fund/InitialCredit/goChushiSx?id=${id!}&type=${type!}">初始授信</a>
                            </li>
                            <li>
                                <a href="${basePath!}/fund/creditCustom/goCreditCustomViews?id=${id!}&type=${type!}">提额授信</a>
                            </li>
                            <li class="xuanzhong">
                                <a href="#">授信提额综合分值</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>授信提额综合分值</h5>
                                </div>
                                <div class="ibox-content">
                                    <div id="yyh"></div>
                                    <div id="yyh1">
                                    </div>

                                    <form id="searchForm" class="form-inline" style="text-align: right;padding-right: 10%">
                                        <div class="form-group">
                                            <div class="row">
                                                <#if type != 'hidden'>
                                                <a type="button" id="bj" >编  辑
                                                </a>
                                                </#if>
                                                <div class="sx_pingfen_modal">
                                                    <div class="modal_content">
                                                        <div class="modal_head">
                                                            <div class="modal_head">
                                                                <p>编辑授信提额综合分值</p>
                                                                <b><a href="#">×</a></b>
                                                            </div>
                                                        </div>
                                                        <div class="modal_body">
                                                            <div class="age_list">
                                                                <ul>
                                                      <#list sx as s>
                                                          <li class="kk">
                                                              <div class="container-fluid">
                                                                  <div class="row">
                                                                      <div class="col-sm-4">
                                                                          <input type="hidden" value="${s.id!}" name="id">
                                                                          <input type="hidden" value="${s.seq!}" name="seq">
                                                                          分值
                                                                          &nbsp
                                                                          <input type="text" value="${s.minScore!}" id="minScore_${s.seq!}" name="minScore" onblur ="setnextValue(this.value,${s.seq!})">

                                                                          ～
                                                                          <input type="text" value="${s.maxScore!}" id="maxScore_${s.seq!}" name="maxScore" onblur="setnextValue2(this.value,${s.seq!})">
                                                                      </div>
                                                                      <div class="col-sm-6">
                                                                          额度（元）
                                                                          &nbsp
                                                                          <input type="text" value="${s.amount!?c}"  name="amount">
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </li>
                                                      </#list>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="modal_foot">
                                                            <div class="sx_zf">
                                                                <ul>
                                                                    <li class="sx_sore">
                                                                        取消
                                                                    </li>
                                                                    <li class="sx_ok" id="tj">
                                                                        <span>确定</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="project-list pager-list" id="data">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>授信评分</th>
                                                <th>分值</th>
                                                <th>额度标准（元）</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr style="height: 0">
                                                <td rowspan="11">授信评分</td>
                                            </tr>
                          <#list sx as s>
                                <tr>
                                    <td>${s.minScore!}分-${s.maxScore!}分</td>
                                    <td>${s.amount!?c}</td>
                                </tr>
                          </#list>

                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>


<script type="text/javascript">
    function setnextValue(val,seq){
        if(seq==1){
            return;
        }
        $("#maxScore_"+(seq*1-1)).val(val*1-1);
    }
    function setnextValue2(val,seq){
        $("#minScore_"+(seq*1+1)).val(val*1+1);
    }
    $(document).ready(function () {
        $("#bj").click(function () {
            $(".sx_pingfen_modal").css("display","block")
        });
        $(".sx_sore").click(function () {
            $(".sx_pingfen_modal").css("display","none")
        });
        $(".modal_head b").click(function (){
            $(".sx_pingfen_modal").css("display","none")
        });
        $("#tj").click(function () {
            var i = 0;
            var array = new Array();
            $(".kk").each(function(){
                n = /^([1-9]\d*|[0]{1,1})$/;
                var ss = {};
                var minScore = $(this).find("input[name='minScore']").val();
                var maxScore = $(this).find("input[name='maxScore']").val();
                var amount = $(this).find("input[name='amount']").val();
                var seq = $(this).find("input[name='seq']").val();
                if(!n.test(amount)){
                  i=1;
                }
                if(!n.test(minScore)){
                    i=1;
                }
                if(!n.test(maxScore)){
                    i=1;
                }
                ss.seq=seq;
                ss.minScore=minScore;
                ss.maxScore=maxScore;
                ss.amount=amount;
                ss.relationshipId = '${id!}';
                array.push(ss);
            });
            if (i == 0) {
                $.ajax({
                    url:'${basePath!}/fund/creditCustom/createCreditScore',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    contentType:"application/json",
                    data:JSON.stringify(array),
                    success: function(data){ //成功
                        //消息对话框
                        parent.messageModel(data.msg);
                        if (data.code == 0){
                            location.reload();
                        }
                    }
                });
            }else {
                parent.messageModel("输入有误,请仔细检查只能输入正整数");
            }

        });
    });

</script>

</body>
</html>
