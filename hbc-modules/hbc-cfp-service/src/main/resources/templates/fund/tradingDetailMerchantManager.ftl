<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<!-- <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
					<li><a data-toggle="tab" href="#uncreate" aria-expanded="true"> 未生成结算单</a></li> -->
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>授信结算管理</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6">
											<h3>结算总额：¥ <span id="s">--</span> 元</h3>
										</div>
										<div class="form-group col-sm-6">
											<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户名称</label>
											<div class="col-sm-9">
												<input id="manme" type="text" class="form-control" name="manme" maxlength="30" placeholder="请填写商户名称">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户id</label>
											<div class="col-sm-9">
												<input id="mnum"  type="text" class="form-control" name="mnum" maxlength="30" placeholder="请填写商户id">
											</div>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
											<!-- <input id="jsmx" class="btn btn-info" type="button" value=" 下 载 报 表 " /> -->
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			
			laydate({
				elem: '#startDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			$("#status").select();
			var $pager = $("#data").pager({
				url:"${hsj}/settlementDetail/tradingDetailMerchantManagerList.htm",
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>运营商名称</th>'+
								'<th>商户名称</th>'+
								'<th>商户结算</th>'+
								'<th>运营商结算</th>'+
								'<th>平台结算</th>'+
								'<th>合计</th>'+
								'<th>操作</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
							'<td>{operaName}</td>'+
							'<td>{mname}</td>'+
							'<td  >{amount} </td>'+
							'<td id="fee_{id}"></td>'+
							'<td>{platformShareFee}</td>'+
							'<td id="aunt_{id}"></td>'+
							'<td id="opera_{id}"><a href="javascript:void(0);" data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-list-ul"></i> 账单明细 </a> </td>'+
						'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					var list = result.list;
					for(var i = 0 ; i < list.length ; i++){
						var fee = parseFloat(list[i].shareFee);
						$("#fee_"+list[i].id).html(fee < 0 ? 0 : fee.toFixed(2));
						
						var  aunt = parseFloat(list[i].amount) + parseFloat(list[i].shareFee)+parseFloat(list[i].platformShareFee);
						$("#aunt_"+list[i].id).html(aunt.toFixed(2));
					}
				}
			});
		});
	
</script>

	</body>
</html>
