<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#auth" aria-expanded="true"> 授信信息</a></li>
				</ul>
				
				<form id="_f" method="post" class="form-horizontal">
					<div class="tab-content">
						<div id="base" class="ibox-content active tab-pane">
							<input type="hidden" name="id" value="${id}"/>
								<c:if test="${id != null}">
							  	<div class="form-group">
									<label class="col-sm-2 control-label">账户id</label>
									<div class="col-sm-10">
										${entity.unum}
									</div>
								</div>
								</c:if>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">账户姓名</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,CHS,maxLength[10]"  validate-msg="required:姓名不能为空,maxLength:姓名长度不能超过10"  id="realName" name="realName" value="${entity.realName}" maxlength="30" placeholder="请填写账户姓名">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">手机号码</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:手机号码不能为空" id="mobile" name="mobile" value="${entity.mobile}" maxlength="30" placeholder="请填写手机号码">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">登陆密码</label>
									<div class="col-sm-8">
										<input type="password" class="form-control" validate-rule="pwdss<c:if test="${id == null}">,required,ffstring</c:if>" validate-msg="required:登陆密码不能为空" id="pwd" name="pwd" value="" maxlength="30" placeholder="<c:if test="${id == null}">请填写登陆密码</c:if><c:if test="${id != null}">不输入，则不修改密码</c:if>">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">重复密码</label>
									<div class="col-sm-8">
										<input type="password" class="form-control" validate-rule="equalTo[#pwd],pwdss<c:if test="${id == null}">,required</c:if>" validate-msg="required:重复密码不能为空,equalTo:两次输入的密码不一致" id="rePwd" maxlength="30">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">交易密码</label>
									<div class="col-sm-8">
										<input type="password" class="form-control" validate-rule="pwdssa<c:if test="${id == null}">,required,number</c:if>" validate-msg="required:交易密码不能为空" id="tranPwd" name="tranPwd" value="" maxlength="30" placeholder="<c:if test="${id == null}">请填写交易密码</c:if><c:if test="${id != null}">不输入，则不修改密码</c:if>">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">重复密码</label>
									<div class="col-sm-8">
										<input type="password" class="form-control" validate-rule="equalTo[#tranPwd],pwdssa<c:if test="${id == null}">,required,number</c:if>" validate-msg="required:重复密码不能为空,equalTo:两次输入的交易密码不一致" id="reTranPwd" maxlength="30">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">身份照号</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:身份证不能为空" id="idCard" name="idCard" value="${entity.idCard}" maxlength="30" placeholder="请填写身份照号">
									</div>
								</div>
								
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户行</label>
									<div class="col-sm-8" id="bank" input-name="bankSn" input-val="${entity.bankSn}"></div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">银行卡</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,number,maxLength[19]" validate-msg="required:银行卡号不能为空,maxLength:银行卡号长度不能超过19" id="bankCard" name="bankCard" value="${entity.bankCard}" maxlength="30" placeholder="请填写银行卡">
									</div>
								</div>
								
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">手机卡密码</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required" validate-msg="required:手机卡密码不能为空" id="mobilePwd" name="mobilePwd" value="${entity.deMobilePwd}" maxlength="30" placeholder="手机卡密码">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">紧急联系人</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,CHS,maxLength[10]" validate-msg="required:联系人不能为空,maxLength:联系人长度不能超过10" id="ugrenName" name="ugrenName" value="${entity.ugrenName}" maxlength="30" placeholder="请填写紧急联系人">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">紧急联系人电话</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:联系人电话不能为空" id="ugrenMobile" name="ugrenMobile" value="${entity.ugrenMobile}" maxlength="30" placeholder="请填写紧急联系人电话">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-12">
									<label class="col-sm-2 control-label">所属地区</label>
									<div class="col-sm-10" id="area" data-name="areaId" data-value="${entity.areaId}" >
										
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">详细地址</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required" validate-msg="required:详情地址不能为空" id="address" name="address" value="${entity.address}" maxlength="30" placeholder="请填写详细地址">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">婚姻状况</label>
									<div class="col-sm-8">
										<select name="isMarry" id="isMarry">
											<option value="true" <c:if test="${id != null && entity.isMarry}">selected</c:if>>已婚</option>
											<option value="false" <c:if test="${id == null || !entity.isMarry}">selected</c:if>>未婚</option>
										</select>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">账户状态</label>
									<div class="col-sm-8"> 
										<select name="status" id="status">
											<option value="true" <c:if test="${id != null && entity.status}">selected</c:if>>启用</option>
											<option value="false" <c:if test="${id == null || !entity.status}">selected</c:if>>禁用</option>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">所属运营商</label>
									<div <c:if test="${id != null}">style="display:none;"</c:if> class="col-sm-8">
										<span id="oid-span" ele-id="oid" ele-name="oid" data-val="${entity.oid}"></span>
									</div>
									<c:if test="${id != null}">
									<label class="col-sm-8 control-label" style="text-align: left;">
										${entity.operaName}
									</label>
									</c:if>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
						</div>
						<!-- 商户审核信息 -->
						<div id="auth" class="ibox-content tab-pane">
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">额度状态</label>
									<div class="col-sm-8">
										<select name="authStatus" id="authStatus">
											<option value="true" <c:if test="${id != null && entity.authStatus}">selected</c:if>>已授信</option>
											<option value="false" <c:if test="${id == null || !entity.authStatus}">selected</c:if>>未授信</option>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">额度上限</label>
									<div class="col-sm-8">
										<input type="text" class="form-control sx" id="authMax" validate-rule="required" name="authMax" value="${entity.authMax}" maxlength="30" placeholder="请填额度上限">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">出账日期</label>
									<div class="col-sm-8">
										<select name="ooaDate" id="ooaDate">
											<c:forEach begin="3" end="27" var="i">
											<option value="${i}" <c:if test="${id != null && entity.ooaDate == i}">selected</c:if>>每月${i}号</option>
											</c:forEach>
										</select>
									</div>
								</div>
								
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">还款日期</label>
									<div class="col-sm-8">
										<select name="repayDate" id="repayDate">
											<c:forEach begin="3" end="27" var="i">
											<option value="${i}" <c:if test="${id != null && entity.repayDate == i}">selected</c:if>>每月${i}号</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script>
		$(document).ready(function () {
			$("#oid-span").operaSelect();
			$("#authStatus").select();
			$("#authStatus").change(function(){
				if($(this).val() == "true"){
					$(".sx").removeAttr("disabled");
				}else{
					$(".sx").attr("disabled",true);
				}
			});
			$("#ooaDate").select();
			$("#repayDate").select();
			$("#authStatus").trigger("change");
			$("#area").area();
			$("#bank").initBank();
			$("#status").select();
			$("#isMarry").select();
			$(".cancel").click(function(){parent.closeLayer();});
			$(".submit-save").click(function (){
				if(!$("#_f").validate()) return;
				var $this = $(this);
				$this.html("保 存 中");
				$this.attr("disabled", true);
				var index = layer.load(2, {time: 10*1000});
				$.ajax({
					url:'${hsj}/userInfo/<c:choose><c:when test="${id != null}">userInfoEditAjaxSubmit</c:when><c:otherwise>userInfoAddAjaxSubmit</c:otherwise></c:choose>.htm',
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:$("#_f").serialize(),
					success: function(data){ //成功
						layer.close(index);
						var obj = data;
						if (obj.resultCode == "-1") {
							parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
							$this.html("保存内容");
							$this.attr("disabled", false);
						}
						
						if (obj.resultCode == "1") {
							//消息对话框
							parent.messageModel("保存成功!");
							parent.c.gotoPage(null);
							parent.closeLayer();
						}
					}
				});
			});
		});
	</script>
</body>

</html>
