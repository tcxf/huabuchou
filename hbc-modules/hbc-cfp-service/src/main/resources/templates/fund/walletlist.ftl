
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">

    <title></title>
    <style>
        body{
            background: #eee;
        }
        *{
            font-size:12px
        }
        #p-top{
            color: #42bbf6;
            padding: 20px;
            font-size: 20px;
            font-weight: 600;
        }
        #p-content li{
            float: left;
            list-style: none;
            padding: 20px;
        }
        #p-content a{
            text-decoration: none;
            color: #333;
            font-size: 16px;
        }
        #p-content .text-right{
            padding-top:19px;
            margin-right: 20px;
        }
        #p-content .text-right b{
            font-size: 18px;
        }
        #p-content .tab-content table{
            width: 98%;
            margin: 0 auto;
        }
        #p-content .tab-content table thead td{
            text-align: center;
            color: #fff;
            background:rgba(0,0,0,0.5);
            border-right: 1px solid #fff;
        }
        #p-content .tab-content table tbody td{
            text-align: center;
            color: #000;
            border-right: 1px solid #fff;
        }
        .form-group li{
            list-style: none;
            float: left;
            width: 20%;
        }
    </style>
    <#include "common/common.ftl">
</head>
<body>

<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>收入支出记录</h5>
                </div>
                <div class="ibox-content">
                    <div id="yyh"></div>
                    <div id="yyh1">
                    </div>

                    <form id="searchForm" >
                        <div class="form-group">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <input type="text" style="display:none"  id="wid" name="wid" value=""/>
                                    <input type="text" style="display:none"  id="type" name="type" value=""/>
                                    <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
                                </div>
                                <div class="col-sm-8">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group col-sm-12" style="text-align:right;margin-top:10px;">
                                <button class="btn btn-info" type="button"  id='txsq'>发起提现</button>
                            </div>
                            <ul>
                                <li class="active" id="sr">
                                    <a class="btn btn-info" href="#shouru" data-toggle="tab">
                                        收入记录
                                    </a>
                                </li>
                                <li id="tx" style="margin-left: -160px">
                                    <a class="btn btn-info"href="#tixian" data-toggle="tab">
                                        支出记录
                                    </a>
                                </li>
                            </ul>

                            <div class="text-right">
                                账户余额：<b>￥${w.totalAmount!}</b>
                            </div>
                        </div>
                    </form>
                    <div class="tab-content">
                        <div class="tab-pane project-list pager-list active " id="shouru">

                        </div>
                        <div class="tab-pane project-list pager-list" id="tixian">

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $("#wid").val('${w.id}');
        $("#type").val('99');
        var $pager =    $("#shouru").pager({
            url:"${basePath!}/fund/fwallet/FfindWalletIncome",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>用户名称</th>'+
                '<th>用户手机</th>'+
                '<th>用户类型</th>'+
                '<th>收入来源</th>'+
                '<th>入账时间</th>'+
                '<th>入账金额</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{pname}</td>'+
                '<td>{pmobile}</td>'+
                '<td id="ptepy_{id}"></td>'+
                '<td id="status_{id}"></td>'+
                '<td>{createDate}</td>'+
                '<td>{amount}</td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
            },
            callbak: function(result) {
                var list = result.data.records;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].type == 1) {
                        $("#status_" + list[i].id).html('<p>还款入金</p>  ');
                    } else if (list[i].type == 2) {
                        $("#status_" + list[i].id).html('<p>现金消费入金</p>  ');
                    } else if (list[i].type == 3) {
                        $("#status_" + list[i].id).html('<p>授信金结算</P>');
                    } else if (list[i].type == 4) {
                        $("#status_" + list[i].id).html('<p>返佣入金</P>');
                    }
                    else if (list[i].type == 5) {
                        $("#status_" + list[i].id).html('<p>提现驳回入金</P>');
                    }

                    if (list[i].ptepy == 1) {
                        $("#ptepy_" + list[i].id).html('<p>消费者用户</p>  ');
                    } else if (list[i].ptepy == 2) {
                        $("#ptepy_" + list[i].id).html('<p>商户</p>  ');
                    } else if (list[i].ptepy == 3) {
                        $("#ptepy_" + list[i].id).html('<p>运营商</P>');
                    } else if (list[i].ptepy == 4) {
                        $("#ptepy_" + list[i].id).html('<p>资金端</P>');
                    }

                }
            }


        });

        $("#sr").click(function(){
            $("#wid").val('${w.id}');
            $("#type").val('99');

            var $pager =    $("#shouru").pager({
                url:"${basePath!}/fund/fwallet/FfindWalletIncome",
                formId:"searchForm",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>用户名称</th>'+
                    '<th>用户手机</th>'+
                    '<th>用户类型</th>'+
                    '<th>收入来源</th>'+
                    '<th>入账时间</th>'+
                    '<th>入账金额</th>'+
                    '<th>结算状态</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{pname}</td>'+
                    '<td>{pmobile}</td>'+
                    '<td id="ptepy_{id}"></td>'+
                    '<td id="status_{id}"></td>'+
                    '<td>{createDate}</td>'+
                    '<td>{amount}</td>'+
                    '<td id="settlementStatus_{id}"></td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    var list = result.data.records;
                    for(var i = 0 ; i < list.length ; i++){
                        if(list[i].type == 1){
                            $("#status_"+list[i].id).html('<p>还款入金</p>  ');
                        }else if(list[i].type == 2){
                            $("#status_"+list[i].id).html('<p>现金消费入金</p>  ');
                        }else if(list[i].type == 3){
                            $("#status_"+list[i].id).html('<p>授信金结算</P>');
                        }else if(list[i].type == 4){
                            $("#status_"+list[i].id).html('<p>返佣入金</P>');
                        }else if (list[i].type == 5) {
                            $("#status_" + list[i].id).html('<p>提现驳回入金</P>');
                        }

                        if(list[i].ptepy == 1){
                            $("#ptepy_"+list[i].id).html('<p>消费者用户</p>  ');
                        }else if(list[i].ptepy == 2){
                            $("#ptepy_"+list[i].id).html('<p>商户</p>  ');
                        }else if(list[i].ptepy == 3){
                            $("#ptepy_"+list[i].id).html('<p>运营商</P>');
                        }else if(list[i].ptepy == 4){
                            $("#ptepy_"+list[i].id).html('<p>资金端</P>');
                        }
                        if(list[i].settlementStatus == 0){
                            $("#settlementStatus_"+list[i].id).html('<p>未结算</p>  ');
                        }else if(list[i].settlementStatus == 1){
                            $("#settlementStatus_"+list[i].id).html('<p>已结算</p>  ');
                        }
                    }
                }



            });
        });

        $("#tx").click(function(){
            $("#wid").val('${w.id}');
            $("#type").val('0');
            var $pager =    $("#tixian").pager({
                url:"${basePath!}/fund/fwallet/FfindWalletIncome",
                formId:"searchForm",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>用户名称</th>'+
                    '<th>用户手机</th>'+
                    '<th>用户类型</th>'+
                    '<th>支出来源</th>'+
                    '<th>支出时间</th>'+
                    '<th>支出金额</th>'+
                    '<th>结算状态</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{pname}</td>'+
                    '<td>{pmobile}</td>'+
                    '<td id="ptepy_{id}"></td>'+
                    '<td id="status_{id}"></td>'+
                    '<td>{createDate}</td>'+
                    '<td>{amount}</td>'+
                    '<td id="settlementStatus_{id}"></td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    var list = result.data.records;
                    for(var i = 0 ; i < list.length ; i++){
                        if(list[i].type == 6){
                            $("#status_"+list[i].id).html('<p>提现支出</p>  ');
                        }else if(list[i].type == 7){
                            $("#status_"+list[i].id).html('<p>消费支出</p>  ');
                        }else if(list[i].type == 8){
                            $("#status_"+list[i].id).html('<p>平台抽成费扣取</p>  ');
                        }

                        if(list[i].ptepy == 1){
                            $("#ptepy_"+list[i].id).html('<p>消费者用户</p>  ');
                        }else if(list[i].ptepy == 2){
                            $("#ptepy_"+list[i].id).html('<p>商户</p>  ');
                        }else if(list[i].ptepy == 3){
                            $("#ptepy_"+list[i].id).html('<p>运营商</P>');
                        }else if(list[i].ptepy == 4){
                            $("#ptepy_"+list[i].id).html('<p>资金端</P>');
                        }

                        if(list[i].settlementStatus == 0){
                            $("#settlementStatus_"+list[i].id).html('<p>未结算</p>  ');
                        }else if(list[i].settlementStatus == 1){
                            $("#settlementStatus_"+list[i].id).html('<p>已结算</p>  ');
                        }

                    }


                }
            });
        });
        $("#txsq").click(function (){
            parent.openLayer('钱包提现','${basePath!}/fund/fwallet/FwallettxManager','300px','50px',$pager);
        });
    });

</script>

</body>
</html>