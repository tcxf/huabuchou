

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>

        #p-content{
            width: 600px;
            height: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow:10px 10px 10px 10px #dadada;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 13px;
            font-weight: bold;
        }
        #p-content .p-footer{
            margin-top: 50%;
        }
    </style>
	  <#include "common/common.ftl">
</head>
<body>
 <div id="p-content">
     <form>
         <div class="form-group">
             <label>已向手机号${info.mobile}发送验证码</label>
             <div>
                 <input type="text" placeholder="输入验证码" width="50%" id='yCode'>
             </div>
             
             <div>
				<a  id='hq' class="btn btn-default sms-btn"
					style=" font-size: 0.8em; padding: 5px; " href="#" role="button">获取验证码</a>
			</div>
         </div>
     </form>
     <div class="p-footer">
         <button type="submit" class="btn btn-info btn-block" id="tj">完成</button>
     </div>
 </div>

 
 <script>
		$(document).ready(function() {
			
	
			$("#hq").click(function(){
				$.ajax({
					url:"${basePath!}/fund/fwallet/getsys",
				    type:'POST', //GET
				    data:{mobile:${info.mobile},
						type:5},
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	if(d.code == 1){
							startTime();
							
						}
				    }
				});
			
				
			});
			var i = 60;
			function startTime(){
				$("#hq").html("获取验证码("+i+"秒)");
				if(i > 0){
					i--;
					setTimeout(function(){
						startTime();
					},1000);
				}else{
					$("#hq").html("获取验证码");
					$("#hq").removeClass("disabled");
					i = 60;
				}
			}
		
			$("#tj").on("click",function(){
            var yCode =   $("#yCode").val();
            if (yCode == '' ) {
                parent.messageModel("验证码不能为空");
                return;
			}
				$.ajax({
					type:"post",
					url:"${basePath!}/fund/fwallet/walletdoReg",
					data:{txje:${txje },
                        sjtxje:${sjtxje},
                        txsxje:${txsxje},
                        yCode:yCode,
                        bid:'${info.id}'
					},
					   success:function(msg){

					    if(msg.code == 0){
                            parent.messageModel(msg.msg);
                            parent.closeLayer();
                        }else {
                            parent.messageModel(msg.msg);
                        }

							
					},
					dataType : "json"
				});
				
				
				
			}); 
		});
	
</script>
</body>
</html>
