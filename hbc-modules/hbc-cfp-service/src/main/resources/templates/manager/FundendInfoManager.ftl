<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>资金端管理</h5>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
					</div>
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/ofundend/selectFundendByOid",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>机构ID</th>'+
							'<th>机构名称</th>'+
							'<th>机构电话</th>'+
                			'<th>逾期还款费率</th>'+
                			'<th>分期利率</th>'+
							'<th>创建日期</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{id}</td>'+
							'<td>{fname}</td>'+
							'<td>{mobile}</td>'+
                			'<td>{yqRate}%</td>'+
							'<td>{serviceFee}%</td>'+
							'<td>{createDate}</td>'+
							'<td id="opera_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 查看 </a> '+
								
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$("#yyh").html("资金机构共计："+result.data.total+"家");
		 		var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].statusone == "1"){
                        $("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-sm"><i class="fa fa-key"></i> <span>默认</span> </a>');
					}
					else{
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm bind"><i class="fa fa-key"></i> <span>默认绑定</span> </a>');
					}
				}
				
				//设置默认绑定资金端
				$(".bind").click(function (){
					window.location.href = '${basePath!}/opera/ofundend/updateForFundo?id='+$(this).attr("data");
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('资金机构信息-'+title,'${basePath!}/opera/ofundend/loadfundupfind?id='+id,'1000px','700px',$pager);
				});

				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		}); 
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
