<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>运营活动</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建活动</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3></h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">活动时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="terminalDate" id="terminalDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">活动名称</label>
									<div class="col-sm-9">
										<input type="text" id="name" class="form-control" name="name" maxlength="30" placeholder="请填写用户名称">
									</div>
								</div>
								
							<div class="form-group col-sm-6 col-lg-4"> 
									<label class="col-sm-3 control-label">活动状态</label> 
									<div class="col-sm-9"> 
										<select id="status" name="status">
											<option value="99">全部</option>
											<option value="0">已发布</option>
											<option value="1">未发布</option>
											<option value="2">已结束</option>
										</select>
									</div>
								</div> 
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
	
		$("#ccs").click(function(){
			var status = $("#status option:selected").val();
			alert(status);
			window.location.href = "${hsj}/tradingDetail/daochu.htm?startDate="+$("#startDate").val()+"&terminalDate="+$("#terminalDate").val()+"&name="+$("#name").val()+"&status="+status;
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#terminalDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#status").select();
		var $pager = $("#data").pager({
	
			url:"${hsj}/activity/activityManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>活动名称</th>'+
							'<th>活动类型</th>'+
							'<th>状态</th>'+
							'<th>领取人数</th>'+
							'<th>开始时间</th>'+
							'<th>结束时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{name}</td>'+
							'<td id="type_{id}"></td>'+
							'<td id="status_{id}"></td>'+
							'<td>{count}</td>'+
							'<td>{startDate}</td>'+
							'<td>{terminalDate}</td>'+
							'<td>'+
							'<a id="status1_{id}" href="javascript:void(0);" data="{id}" data1="{status}" class="btn btn-success btn-sm hk"><i class="fa fa-edit"></i> </a>'+
							' <a href="javascript:void(0);" data="{id},{userName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					var type = list[i].type;
					if(type == 0){
						$("#type_"+list[i].id).html("注册活动");
					}else if(type == 1){
						$("#type_"+list[i].id).html("分享活动");
					}else if(type == 2){
						$("#type_"+list[i].id).html("授信活动");
					}

					var status = list[i].status;
					if(status == 0){
						$("#status_"+list[i].id).html("已发布");
						$("#status1_"+list[i].id).html("结束");
					}else if(status==1){
						$("#status1_"+list[i].id).html("发布");
						$("#status_"+list[i].id).html("未发布");
					}else if(status==2){
						$("#status_"+list[i].id).html("已结束");
						$("#status1_"+list[i].id).html("无操作");
					}
				}
				$(".distribution").unbind("click");
				$(".distribution").bind("click", function(){
					var id = $(this).attr("data-id");
					parent.openLayerWithBtn('添加奖品','${hsj}/role/selectResources.htm?id='+id,'800px','500px', null, ['选好了','不选了'], function(index, layero){
						var body = parent.layer.getChildFrame('body', index);
						var $checked = body.find(".resources:checked");
						var ids = "";
						if($checked != null && $checked.length > 0){
							$checked.each(function(i,e){
								ids += ","+$(e).val();
							});
							ids = ids.substring(1);
						}
						$.post("${hsj}/role/distribution.htm?",{id:id,ids:ids},function(r){
							if(r.resultCode=="1"){
								parent.messageModel("添加成功");
							}else{
								parent.messageModel(r.msg);
							}
							parent.layer.close(index);
						},"json");
					});
				});
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改活动-'+title,'${hsj}/activity/activityEdit.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				
				$(".hk").click(function(){
					var id =$(this).attr("data");
					var status = $(this).attr("data1");
					if(status==0){
						parent.confirm("是否确认结束活动？",function(){
							$.post("${basePath!}/activity/doRepayment.htm",{id:id,status:status},function(d){
								parent.messageModel(d.resultMsg);
								$pager.gotoPage($pager.pageNumber);
							},"json");
						});
					}else if(status==1){
						parent.confirm("是否确认发布活动？",function(){
							$.post("${basePath!}/activity/doRepayment.htm",{id:id,status:status},function(d){
								parent.messageModel(d.resultMsg);
								$pager.gotoPage($pager.pageNumber);
							},"json");
						});
						
					}else if(status==2){
						parent.confirm("该活动已结束，暂无操作",function(){
							$.post("${basePath!}/activity/doRepayment.htm",{id:id,status:status},function(d){
								parent.messageModel(d.resultMsg);
								$pager.gotoPage($pager.pageNumber);
							},"json");
						});
					}
				});
			}
		});
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加活动','${hsj}/activity/activityAdd.htm','1000px','700px',$pager);
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	
	});
	
</script>

	</body>
</html>
