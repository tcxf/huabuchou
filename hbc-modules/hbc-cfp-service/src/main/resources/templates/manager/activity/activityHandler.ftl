<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
  <style>
  	.ibox .open > .dropdown-menu {
    left: auto;
    right: -36px;
}
  </style>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>活动信息</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
								<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value="${id}"/>
                        	<input type="hidden" id="status" name="status" value="${entity.status}"/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">活动名称 </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,maxLength[20],loginName" validate-msg="required:商品名称不能为空,loginName:商品名称不能有特殊字符" id="name" name="name" value="${entity.name}" maxlength="30" placeholder="请填写活动名称">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">活动类型</label>
									<div class="col-sm-8">
										<select class="dfinput" name="type" id="type" style="width:100px;">
											<option value="0" <c:if test="${entity != null && entity.type==0}">selected</c:if>>注册活动</option>
											<option value="1" <c:if test="${entity != null && entity.type==1}">selected</c:if>>分享活动</option>
											<option value="2" <c:if test="${entity != null && entity.type==2}">selected</c:if>>授信活动</option>
										</select>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开始时间</label>
									<div class="col-sm-8">
										<input type="text" style="background-color:#ffffff" readonly class="form-control" validate-rule="required" 
										validate-msg="required:请选择开始时间," id="startDate" name="startDate" value="<fmt:formatDate value="${entity.startDate }" pattern="yyyy-MM-dd" />" maxlength="30" placeholder="请选择结束时间">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结束时间</label>
									<div class="col-sm-8">
										<input type="text" style="background-color:#ffffff" readonly class="form-control" validate-rule="required" 
										validate-msg="required:请选择结束时间," id="terminalDate" name="terminalDate" value="<fmt:formatDate value="${entity.terminalDate }" pattern="yyyy-MM-dd" />" maxlength="30" placeholder="请选择结束时间">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
										<label class="col-sm-4 control-label">活动奖品1</label>
									<div class="col-sm-8">
                                	<select class="dfinput" name="pid" id="pid" style="width:100px;">
                                		<option value="99">请选择优惠券</option>
                                		<c:forEach items="${prize}" var="i">
											<option value="${i.id}" <c:if test="${entity != null && pid1 == i.id}">selected</c:if>>${i.name}</option>
										</c:forEach>
									</select>
                                </div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">活动奖品2</label>
									<div class="col-sm-8">
										<select class="dfinput" name="pid1" id="pid1" style="width:100px;">
                                		<option value="99">请选择优惠券</option>
                                		<c:forEach items="${prize}" var="i">
											<option value="${i.id}" <c:if test="${entity != null && pid2 == i.id}">selected</c:if>>${i.name}</option>
										</c:forEach>
									</select>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">活动奖品3</label>
									<div class="col-sm-8">
										<select class="dfinput" name="pid2" id="pid2" style="width:100px;">
                                		<option value="99">请选择优惠券</option>
                                		<c:forEach items="${prize}" var="i">
											<option value="${i.id}" <c:if test="${entity != null && pid3 == i.id}">selected</c:if>>${i.name}</option>
										</c:forEach>
									</select>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">banner图</label>
									<div class="col-sm-8">
										<div class="img" file-id="img" file-value="${entity.img}" file-size="300*150"></div>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">活动描述</label>
									<div class="col-sm-8">
										<textarea style="width:100%;height:100px;" name="describe">${entity.describe }</textarea>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
    
    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	 <script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
    <script>
        $(document).ready(function () {
        	laydate({
    			elem: '#startDate', 
    			event: 'click', //触发事件
    		  	format: 'YYYY-MM-DD' //日期格式
    		});
    		laydate({
    			elem: '#terminalDate', 
    			event: 'click', //触发事件
    		  	format: 'YYYY-MM-DD' //日期格式
    		});
        	$(".img").uploadImg();
            $("#type").select();
            $("#pid").select();
            $("#pid1").select();
            $("#pid2").select();
            $("#cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
    			if(!$("#_f").validate()) return;
    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
    			var index = layer.load(2, {time: 10*1000});
    			$.ajax({
    				url:'${hsj}/activity/<c:choose><c:when test="${id != null}">activityEditAjaxSubmit</c:when><c:otherwise>activityAddAjaxSubmit</c:otherwise></c:choose>.htm',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:$("#_f").serialize(),
    				success: function(data){ //成功
    					layer.close(index);
    					var obj = data;
    					if (obj.resultCode == "-1") {
    						parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
    						$this.html("保存内容");
    						$this.attr("disabled", false);
    					}
    					
    					if (obj.resultCode == "1") {
    						//消息对话框
    						parent.messageModel("保存成功!");
    						parent.c.gotoPage(null);
    						parent.closeLayer();
    					}
    				}
    			});
    		});
			
          
            
        });
    </script>
</body>

</html>
