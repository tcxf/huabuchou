<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=path %>/css/animate.css" rel="stylesheet">
    <link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
  <style>
  	.ibox .open > .dropdown-menu {
    left: auto;
    right: -36px;
}


  </style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>首页banner图</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 添加</a> 
						</div>
					</div>
					<div class="ibox-content">
						<div class="fixed-table form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>banner图片</th>
										<th>建议尺寸</th>
										<th>排序</th>
										<th>链接地址</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody id="tbody"  >
								</tbody>
							</table>
						</div>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>

	<script type="text/javascript">

     $(document).ready(function () {
    	 var i=0;
    	 var nodata = false;
			<c:choose>
				<c:when test="${id == null}">
					$("#new").click(function(){
						if(nodata) $("#tbody").html("");
						nodata = false;
						$tr = $('<tr>'+
								'<td class="bank-select" >'+
									'<div>'+
						 			'<div class="img"  file-id="img" file-value="${entity.img}"></div>' +
						 		'</div'+
								'</td>'+
								'<td>300px*150px</td>'+
								'<td><input name="bank-idCard" type="text" value="${entity.order}" /></td>'+
								'<td>'+
									'<p>${entity.liikName}</p>'+
									'<input type="button" class="btn btn-info goods" value="商品" /> '+
								'</td>'+
								'<td><input type="button" class="btn btn-info save-banner" value="保存" /> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
							'</tr>');
						$("#tbody").append($tr);
						$tr.find(".img").uploadImg();
						$tr.find(".del-banner").click(function(){
							$this = $(this);
							li = layer.confirm('您确认删除吗?', {
								btn: ['确定','取消'] //按钮
							}, function(){
								$this.parent().parent().remove();
								location.reload(); 
								layer.close(li);
							}, function(){});
						});
					});
				</c:when>
				<c:otherwise>

		 		function banner(){
					$("#tbody").empty();
					$.post("<%=path%>/indexbanner/hotelBannerList.htm",function(d){
						for(var i = 0 ; i < d.data.length ; i++){
							$tr = $('<tr>'+
									'<td class="bank-select" >'+
									'<div>'+
		 				 			'<div class="img'+d.data[i].id+'"  file-id="img'+d.data[i].id+'" file-value="'+d.data[i].img+'"></div>' +
		 				 		'</div'+
								'</td>'+
								'<td>300px*150px</td>'+
								'<td><input id="order" name="bank-idCard" type="text" value="'+d.data[i].sort+'" /></td>'+
								'<td>'+
									'<p id="linkName'+d.data[i].id+'" value="'+d.data[i].linkName+'" >'+d.data[i].linkName+'</p>'+
									'<p id="href'+d.data[i].id+'" hidden="hidden" value="'+d.data[i].href+'" >'+d.data[i].href+'</p>'+
									'<input data-id1="'+d.data[i].id+'" type="button" class="btn btn-info shop" value="商户" /> '+
								'</td>'+
								'<td><input type="button"  data-id="'+d.data[i].id+'" class="btn btn-info update-banner" value="更新" /> <input type="button"  data-id3="'+d.data[i].id+'" class="del-banner btn btn-danger" value="删除" /></td>'+
							'</tr>');
							
							$("#tbody").append($tr);
							$(".img"+d.data[i].id).uploadImg();
				
							
							$tr.find(".shop").bind("click", function(){  
								$this = $(this); 
								parent.openLayerWithBtn('选择链接地址','${hsj}/merchantInfo/hotelbannerHref.htm?id='+12,'800px','500px', null, ['选好了','不选了'], function(index, layero){
									var body = parent.layer.getChildFrame('body', index);
									var $checked = body.find(".resources:checked");
									var name = "";
							
										$checked.each(function(i,e){
											name = $(e).val();
										});
										
										$.post("${hsj}/indexbanner/bannerHref.htm?",{
											name:name,
											type:1,
											mid:$this.attr("data-id1"),
											},function(r){
											if(r.resultCode=='1'){
												$("#linkName"+r.data.mid).html(r.data.mName);
												$("#href"+r.data.mid).html(r.data.id);
											}else{
												parent.messageModel(r.resultMsg);
											}
											parent.layer.close(index);
										},"json");
								});
							});
							
							$tr.find(".update-banner").click(function(){
								$this = $(this);
								$p = $this.parent().parent();
								$.post("<%=path%>/indexbanner/updateHotelBanner.htm",{
									id:$this.attr("data-id"),
									img:$p.find("#img"+$this.attr("data-id")).val(),
									linkName:$p.find("#linkName"+$this.attr("data-id")).html(),
									href:$p.find("#href"+$this.attr("data-id")).html(),
									order:$p.find("#order").val(),
								},function(d){
									layer.alert(d.resultMsg);
									if(d.resultCode != -1){
										banner();
									}
								},"json"); 
							});
							
							$tr.find(".del-banner").click(function(){
								$this = $(this);
								li = layer.confirm('您确认删除吗?', {
									btn: ['确定','取消'] //按钮
								}, function(){
									$.post("<%=path%>/indexbanner/deleteBanner.htm",{uid:$this.attr("data-id3")},function(d){
											parent.messageModel(d.resultMsg);
											$this.parent().parent().remove();
											layer.close(li);
											location.reload(); 
									},"json");
								}, function(){});
							});
							
							
							
						}
					},"json");
				}
		 		banner();
				$("#new").click(function(){
					if(i!=0){
						layer.alert("请先完成添加");
					}else{
					if(nodata) $("#tbody").html("");
					nodata = false;
					$tr = $('<tr>'+
							'<td class="bank-select" >'+
								'<div>'+
					 			'<div class="img'+i+'"  file-id="img'+i+'" file-value="${entity.img}"></div>' +
					 		'</div'+
							'</td>'+
							'<td>300px*150px</td>'+ 
							'<td><input id="order" type="text" value="" /></td>'+
							'<td>'+
								'<p id="linkName'+i+'"></p>'+
								'<p id="href'+i+'" hidden="hidden"> </p>'+
								'<input type="button" class="btn btn-info shop" value="商户" /> '+
							'</td>'+
							'<td><input type="button" class="btn btn-info save-banner" value="保存" /> <input type="button" class="del-banner btn btn-danger" value="删除" /></td>'+
						'</tr>');
					$("#tbody").append($tr);
					$(".img"+i).uploadImg();
				
					$tr.find(".shop").bind("click", function(){
						parent.openLayerWithBtn('选择链接地址','${hsj}/merchantInfo/hotelbannerHref.htm?id='+12,'800px','500px', null, ['选好了','不选了'], function(index, layero){
							var body = parent.layer.getChildFrame('body', index);
							var $checked = body.find(".resources:checked");
							var name = "";
					
								$checked.each(function(i,e){
									name = $(e).val();
								});
								$.post("${hsj}/indexbanner/bannerHref.htm?",{name:name,type:1},function(r){
									if(r.resultCode=='1'){
										$("#linkName"+(i-1)).html(r.data.mName);
										$("#href"+(i-1)).html(r.data.id);
										
									}else{
										parent.messageModel(r.resultMsg);
									}
									parent.layer.close(index);
								},"json");
								
						});
					});
					i++;
					$tr.find(".save-banner").click(function(){
						$this = $(this);
						$p = $this.parent().parent();
						if($p.find("#img"+(i-1)).val().length==0){
							layer.alert("请选择图片");
							return;
						}
						if($p.find("#linkName"+(i-1)).html().length==0){
							layer.alert("请选择链接地址");
							return;
						}
						if($p.find("#order").val().length==0){
							layer.alert("请选择排序号");
							return;
						}
						$.post("<%=path%>/indexbanner/saveHotelBanner.htm",{
							img:$p.find("#img"+(i-1)).val(),
							linkName:$p.find("#linkName"+(i-1)).html(),
							href:$p.find("#href"+(i-1)).html(),      
							order:$p.find("#order").val(),
						},function(d){
							layer.alert(d.resultMsg);
							if(d.resultCode != -1){
								location.reload(); 
							}
						},"json");
					});
					
					$tr.find(".del-banner").click(function(){
						$this = $(this);
						li = layer.confirm('您确认删除吗?', {
							btn: ['确定','取消'] //按钮
						}, function(){
							$this.parent().parent().remove();
							layer.close(li);
							location.reload(); 
							layer.alert("删除成功");
							
						}, function(){
						});
					});
					$tr.find("select").select();
					}});
				
				
				</c:otherwise>
			</c:choose>
			
			var uploader = WebUploader.create({
		  		 // 选完文件后，是否自动上传。
		  		 auto: true,
		  		 // swf文件路径
		  		 swf: '/grant-manager/js/plugins/webuploader/Uploader.swf',
		  		 // 文件接收服务端。
		  		 server: '/grant-manager/fileUpload/upload.htm',
		  		 // 选择文件的按钮。可选。
		  		 // 内部根据当前运行是创建，可能是input元素，也可能是flash.
		  		 pick: '#img',
		  		 // 只允许选择图片文件。
		  		 accept: {
		  			  title: 'Images',
		  			  extensions: 'gif,jpg,jpeg,bmp,png',
		  			  mimeTypes: 'image/*'
		  		 }
		  	});
    	 
   
     });
     
 </script>

	</body>
</html>
