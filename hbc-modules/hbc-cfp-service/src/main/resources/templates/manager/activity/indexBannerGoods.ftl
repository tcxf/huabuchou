<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<style>
		.a-ridio input[type=radio]{
            display: inline-block;
            vertical-align: middle;
            width: 20px;
            height: 20px;
            margin-left: 5px;
            -webkit-appearance: none;
            background-color: transparent;
            border: 0;
            outline: 0 !important;
            line-height: 20px;
            color: #d8d8d8;
        }
       .a-ridio input[type=radio]:after  {
            content: "";
            display:block;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            text-align: center;
            line-height: 14px;
            font-size: 16px;
            color: #fff;
            border: 2px solid #ddd;
            background-color: #fff;
            box-sizing:border-box;
        }
        .a-ridio input[type=radio]:checked:after  {
            content: "L";
            transform:matrix(-0.766044,-0.642788,-0.642788,0.766044,0,0);
            -webkit-transform:matrix(-0.766044,-0.642788,-0.642788,0.766044,0,0);
            border-color: #37AF6E;
            background-color: #37AF6E;
        }
		
	</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">商品名称</label>
								<input type="text" name="name" placeholder="请输入商品名称" id="name" class="form-control">
								<input type="text" name="merchantName" placeholder="请输入商户名称" id="merchantName" class="form-control">
<!-- 								<select class="dfinput" name="gcId" id="parentId" style="width:100px;"> -->
<!--                                		<option value="">全部分类</option> -->
<!-- 							    	<c:forEach items="${categoryList}" var="i"> -->
<!-- 										<option value="${i.id}">${i.name}</option> -->
<!-- 									</c:forEach> -->
<!-- 								</select> -->
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${hsj}/goodsInfo/goodsInfoManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>所属商户</th>'+
							'<th>商品名称</th>'+
							'<th>市场价</th>'+
							'<th>销售价</th>'+
							'<th>图片</th>'+
 							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{merchantName}</td>'+
							'<td>{name}</td>'+
							'<td>{marketPrice}</td>'+
							'<td>{salePrice}</td>'+
							'<td id="img_{id}"></td>'+
 							'<td style="width:200px;">'+
 							'<label class="a-ridio"><input type="radio" class="resources" name="ids"  value="{name},{id}"/> </label>'+
 							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="6"><center>暂无数据</center></tr>'
			},
			
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					$("#img_"+list[i].id).html('<img src="${res}'+list[i].img+'" style="width:75px;height:75px;"/>');
				}
				
			}
			
		});

		$('input').iCheck({
			checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
		    increaseArea: '20%' // optional
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${hsj}/goodsInfo/delete.htm',param,$pager);
			}
		});
		
		//打开新增页面
		/* $("#new").click(function (){
			parent.openLayer('添加商品','${hsj}/goodsInfo/forwardGoodsInfoAdd.htm','1000px','700px',$pager);
		}); */
	});
	
</script>

	</body>
</html>
