<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.css" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>出账日和还款日</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>

                                 <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value=""/>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">出账日</label>
                                <div class="col-sm-10">
                                    <input type="text"  onkeyup="value=value.replace(/[^\d]/g,'')"  class="form-control"   id="ooaDate" name="ooaDate" value="${oo.ooaDate!}"  maxlength="30" >

                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">还款日</label>
                                <div class="col-sm-10">
                                    <input type="text"  onkeyup="value=value.replace(/[^\d]/g,'')" id="repayDate" name="repayDate" value="${oo.repayDate!}"   class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#submit-save").click(function (){
                var repayDate =$("#repayDate").val();
                var ooaDate =$("#ooaDate").val();
                if(${type}==1){
                    parent.messageModel("该运营商已运营，不能修改");
                    return;
                }
                if(${type}==2){
                    parent.messageModel("只能修改一次，不能在进行修改");
                    return;
                }
                if(ooaDate.length==0){
                    parent.messageModel("请填写出账日");
                    return;
                }
                if(repayDate.length==0){
                    parent.messageModel("请填写还款日");
                    return;
                }
                if(ooaDate.length>2){
                    parent.messageModel("出账日最多为两位数");
                    return;
                }
                if(repayDate.length>2){
                    parent.messageModel("还款日最多为两位数");
                    return;
                }
                 if(parseInt(repayDate)<parseInt(ooaDate)){
                     parent.messageModel("还款日必须大于出账日");
                     return;
                 }


    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
                var data=   $("#_f").serializeObject();
    			$.ajax({
    				url:'${basePath}/opera/merchantInfo/updateOoaDate',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:data,
    				success: function(r){ //成功
    					var obj = r;
    						//消息对话框
    						parent.messageModel(r.msg);
                        window.location.href = "${basePath!}/opera/merchantInfo/ooaDate";
    				}
    			});
    		});
            
        });
    </script>
</body>

</html>
