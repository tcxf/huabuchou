<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>活动优惠券</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建优惠券</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3></h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">活动时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="terminalDate" id="terminalDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">优惠券名称</label>
									<div class="col-sm-9">
										<input type="text" id="name" class="form-control" name="name" maxlength="30" placeholder="请填写用户名称">
									</div>
								</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
	
		$("#ccs").click(function(){
			var status = $("#status option:selected").val();
			window.location.href = "${hsj}/tradingDetail/daochu.htm?startDate="+$("#startDate").val()+"&terminalDate="+$("#terminalDate").val()+"&name="+$("#name").val()+"&status="+status;
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#terminalDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#status").select();
		var $pager = $("#data").pager({
	
			url:"${hsj}/prize/prizeManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>&nbsp&nbsp&nbsp&nbsp优惠券名称</th>'+
							'<th>优惠金额</th>'+
							'<th>满减条件</th>'+
							'<th>状态</th>'+
							'<th>优惠范围</th>'+
							'<th>开始时间</th>'+
							'<th>结束时间</th>'+
							'<th>领取/使用</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>&nbsp&nbsp&nbsp&nbsp{name}</td>'+
							'<td>{fullMoney}</td>'+
							'<td>{money}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="mid_{id}"></td>'+
							'<td>{startDate}</td>'+
							'<td>{terminalDate}</td>'+
							'<td>{count}</td>'+
							'<td>'+
								' <a href="javascript:void(0);" data="{id},{userName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data-id="{id}" class="btn btn-success btn-sm distribution"><i class="fa fa-plus"></i> 使用范围 </a>'+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					
					var status = list[i].status;
					if(status == 0){
						$("#status_"+list[i].id).html("进行中");
					}else if(status==1){
						$("#status_"+list[i].id).html("已失效");
					}else if(status==2){
						$("#status_"+list[i].id).html("已结束");
					}
					var status = list[i].mid;
					if(status == null){
						$("#mid_"+list[i].id).html("通用");
					}else{
						$("#mid_"+list[i].id).html("部分");
					}
				}
				
				//分配使用的商家
				$(".distribution").bind("click", function(){
					var id = $(this).attr("data-id");
					parent.openLayerWithBtn('使用范围','${hsj}/prize/selectResources.htm?id='+id,'800px','500px', null, ['选好了','不选了'], function(index, layero){
						var body = parent.layer.getChildFrame('body', index);
						var $checked = body.find(".resources:checked");
						var ids = "";
						if($checked != null && $checked.length > 0){
							var t =0;
							$checked.each(function(i,e){
								if($checked.length==0){
									ids += $(e).val();
								}
								if((t+1)<$checked.length){
									ids += $(e).val()+",";
								}else{
									ids += $(e).val();
								}
								t++;
							});
						}
						
						$.post("${hsj}/prize/distribution.htm?",{id:id,ids:ids},function(r){
							if(r.resultCode=="1"){
								parent.messageModel("配置成功");
							}else{
								parent.messageModel(r.msg);
							}
							parent.layer.close(index);
						},"json");
					});
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改优惠券-'+title,'${hsj}/prize/prizeEdit.htm?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				loadAmount();
			}
		});
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加优惠券','${hsj}/prize/prizeAdd.htm','1000px','700px',$pager);
		});
		
		function loadAmount(){
			$.ajax({
				url:"<%=path%>/tradingDetail/totalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
	
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${hsj}/tradingDetail/delete.htm',param,$pager);
			}
		});
	});
	
</script>

	</body>
</html>
