<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=path %>/css/animate.css" rel="stylesheet">
    <link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
  <style>
  	.ibox .open > .dropdown-menu {
    left: auto;
    right: -36px;
}
  </style>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>活动信息</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
								<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value="${id}"/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠券名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,maxLength[20],loginName" validate-msg="required:优惠券名称不能为空,loginName:优惠券名称不能有特殊字符" id="name" name="name" value="${entity.name}" maxlength="30" placeholder="请填写优惠券名称">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开始时间</label>
									<div class="col-sm-8">
										<input type="text" style="background-color:#ffffff" readonly class="form-control" validate-rule="required" 
										validate-msg="required:请选择开始时间," id="startDate" name="startDate" value="<fmt:formatDate value="${entity.startDate }" pattern="yyyy-MM-dd" />" maxlength="30" placeholder="请选择开始时间">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结束时间</label>
									<div class="col-sm-8">
										<input type="text" style="background-color:#ffffff" readonly class="form-control" validate-rule="required" 
										validate-msg="required:请选择结束时间," id="terminalDate" name="terminalDate" value="<fmt:formatDate value="${entity.terminalDate }" pattern="yyyy-MM-dd" />" maxlength="30" placeholder="请选择结束时间">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">满足金额</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,money,maxLength[7]" validate-msg="required:请输入满足金额,money:数字小数点后不能超过两位,maxLength:金额长度不能超过8位数字" id="money" name="money" value="${entity.money }" maxlength="8" placeholder="请输入满足金额">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠金额</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,money,maxLength[6],largeThan[#fullMoney]" validate-msg="required:请输入优惠金额,money:数字小数点后不能超过两位,maxLength:金额长度不能超过7位数字,largeThan:金额不能大于原价" id="fullMoney" name="fullMoney" value="${entity.fullMoney }" maxlength="7" placeholder="请输入优惠金额">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
    
    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	 <script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
    <script>
        $(document).ready(function () {
        	laydate({
    			elem: '#startDate', 
    			event: 'click', //触发事件
    		  	format: 'YYYY-MM-DD' //日期格式
    		});
    		laydate({
    			elem: '#terminalDate', 
    			event: 'click', //触发事件
    		  	format: 'YYYY-MM-DD' //日期格式
    		});
            $("#cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
    			var index = layer.load(2, {time: 10*1000});
    			$.ajax({
    				url:'${hsj}/prize/<c:choose><c:when test="${id != null}">prizeEditAjaxSubmit</c:when><c:otherwise>prizeAddAjaxSubmit</c:otherwise></c:choose>.htm',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:$("#_f").serialize(),
    				success: function(data){ //成功
    					layer.close(index);
    					var obj = data;
    					if (obj.resultCode == "-1") {
    						parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
    						$this.html("保存内容");
    						$this.attr("disabled", false);
    					}
    					
    					if (obj.resultCode == "1") {
    						//消息对话框
    						parent.messageModel("保存成功!");
    						parent.c.gotoPage(null);
    						parent.closeLayer();
    					}
    				}
    			});
    		});
			
          
            
        });
    </script>
</body>

</html>
