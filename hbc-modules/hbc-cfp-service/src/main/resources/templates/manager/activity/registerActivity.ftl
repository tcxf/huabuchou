<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>活动领取名单</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3></h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">领取人名字</label>
									<div class="col-sm-9">
										<input type="text" id="realName" class="form-control" name="realName" maxlength="30" placeholder="请填写用户名称">
									</div>				
								</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">店铺</label>
									<div class="col-sm-9">
										<input type="text" id="simpleName" class="form-control" name="simpleName" maxlength="30" placeholder="请填写用户名称">
									</div>
								</div>
								
							<div class="form-group col-sm-6 col-lg-4"> 
									<label class="col-sm-3 control-label">状态</label> 
									<div class="col-sm-9"> 
										<select id="status" name="status">
											<option value="99">全部</option>
											<option value="0">已使用</option>
											<option value="1">未使用</option>
										</select>
									</div>
								</div> 
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
	
		$("#ccs").click(function(){
			var status = $("#status option:selected").val();
			alert(status);
			window.location.href = "${hsj}/tradingDetail/daochu.htm?startDate="+$("#startDate").val()+"&terminalDate="+$("#terminalDate").val()+"&name="+$("#name").val()+"&status="+status;
		});
		
   		$("#status").select();
		var $pager = $("#data").pager({
	
			url:"${hsj}/receive/receiveManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>领取人</th>'+
							'<th>电话</th>'+
							'<th>商铺名称</th>'+
							'<th>商铺电话</th>'+
							'<th>状态</th>'+
							'<th>消费金额</th>'+
							'<th>优惠金额</th>'+
							'<th>实际金额</th>'+
							'<th>使用时间</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{realName}</td>'+
							'<td>{umobile}</td>'+
							'<td>{simpleName}</td>'+
							'<td>{mmobile}</td>'+
							'<td id="status_{id}"></td>'+
							'<td>{totalAmount}</td>'+
							'<td>{amount}</td>'+
							'<td>{samount}</td>'+
							'<td>{useDate}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					var status = list[i].status;
					if(status == 0){
						$("#status_"+list[i].id).html("已使用");
					}else if(status==1){
						$("#status_"+list[i].id).html("未使用");
					}
				}
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				loadAmount();
			}
		});
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加活动','${hsj}/activity/activityAdd.htm','1000px','700px',$pager);
		});
		
		function loadAmount(){
			$.ajax({
				url:"<%=path%>/tradingDetail/totalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		
	
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${hsj}/tradingDetail/delete.htm',param,$pager);
			}
		});
	});
	
</script>

	</body>
</html>
