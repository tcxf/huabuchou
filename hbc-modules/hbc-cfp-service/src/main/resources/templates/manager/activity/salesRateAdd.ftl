<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>分润</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input  class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id"<#if type?? && type=="1">value="${salesRate.id!}"</#if>"/>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">直接上级分润比率%</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control"   onkeyup="value=value.replace(/[^\d]/g,'')"  id="redirectRate" name="redirectRate" <#if type?? && type=="1">value="${salesRate.redirectRate!}"</#if>  maxlength="30" placeholder="请填写上级分润比率">

                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">间接上级分润比率%</label>
                                <div class="col-sm-10">
                                    <input type="text" id="indirectRate" name="indirectRate"  onkeyup="value=value.replace(/[^\d]/g,'')"  <#if type?? && type=="1">value="${salesRate.indirectRate}"</#if>   class="form-control" placeholder="请填写间接上级分润比率">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">运营商抽取折扣比率%</label>
                                <div class="col-sm-10">
                                    <input type="text" id="opaRate"  name="opaRate"  onkeyup="value=value.replace(/[^\d]/g,'')" <#if type?? && type=="1">value="${salesRate.opaRate!}"</#if>   class="form-control" placeholder="请填写运营商抽取三级分销分润比例">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#submit-save").click(function (){
                var opaRate =parseFloat($("#opaRate").val())+parseFloat($("#indirectRate").val())+parseFloat($("#redirectRate").val());
                if(opaRate!=100){
                    parent.messageModel("三个分润比率必须等于100");
                    return
                }


    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
                var data=   $("#_f").serializeObject();
    			$.ajax({
    				url:'${basePath}/opera/saleRate/insert',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:data,
    				success: function(r){ //成功
    					var obj = r;
    						//消息对话框
    						parent.messageModel(r.msg);
                        window.location.href = "${basePath!}/opera/saleRate/findSaleRate";
    				}
    			});
    		});
            
        });
    </script>
</body>

</html>
