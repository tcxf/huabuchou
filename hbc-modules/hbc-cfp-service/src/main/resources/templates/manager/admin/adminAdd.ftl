<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>管理员信息</h5>
						<div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
							 <div>
								<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
								<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
							</div>
						</div>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value=""/>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">登录用户名</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" validate-rule="required,maxLength[20],ffstring,loginName"  id="userName" name="userName" value="" maxlength="30" placeholder="请填写管理员登录名">

                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">登录密码</label>
                                <div class="col-sm-10">
                                    <input type="password" name="pwd" id="pwd"  validate-rule="required,maxLength[6],ffstring,safepass"class="form-control" placeholder="密码，6位，必须包含数字和字母">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">真实姓名</label>
                                <div class="col-sm-10">
                                    <input type="text" id="adminName" name="adminName" validate-rule="required,maxLength[4],ffstring,CHS" value="" class="form-control" placeholder="管理员真实姓名">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">联系邮箱</label>
                                <div class="col-sm-10">
                                    <input type="text" id="regEmail" validate-rule="required,email" name="regEmail" value="" class="form-control" placeholder="管理员联系邮箱">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">状态</label>
                                <div class="col-sm-10">
                                    <select id="status" name="status">
                                    	<option value="1">启用</option>
                                    	<option value="0" >禁用</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#state").select();
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
            $("#state").select();
            $("#cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
    			var $this = $(this);
    			$this.html("保 存 中");
    			$this.attr("disabled", true);
    			var index = layer.load(2, {time: 10*1000});
                var data=   $("#_f").serializeObject();
    			$.ajax({
    				url:'${basePath}/opera/mAdmin/insertAdmin',
    				type:'post', //数据发送方式
    				dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
    				data:data,
    				success: function(r){ //成功
    					var obj = r;
    						//消息对话框
    						parent.messageModel(r.msg);
                        if(r.code!=1) {
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }
    				}
    			});
    		});
            
        });
    </script>
</body>

</html>
