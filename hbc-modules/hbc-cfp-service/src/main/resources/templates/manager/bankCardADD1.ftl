<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 基本表单</title>
<meta name="keywords" content="">
<meta name="description" content="">

<link rel="shortcut icon" href="favicon.ico">
<link href="<%=path%>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="<%=path%>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="<%=path%>/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<%=path%>/css/animate.css" rel="stylesheet">
<link href="<%=path%>/css/style.css?v=4.1.0" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/plugins/webuploader/webuploader.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>银行卡信息</h5>
						<div class="form-group"
							style="position: absolute;right:20px;top:5px;z-index: 99999;">
							<div>
								<input class="btn btn-primary submit-save" id="submit-save"
									type="button" value="保存内容" /> <input
									class="btn btn-white cancel" id="cancel" type="button"
									value="取消" />
							</div>
						</div>
					</div>
					<div class="ibox-content">
						<form id="_f" method="post" class="form-horizontal">
							<input type="hidden" name="id" value="${id}" />
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户行名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="20"
											placeholder="请输入开户行名称" id="bname">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">请输入卡号</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="19"
											placeholder="请输入卡号" id="bcard">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户人姓名</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="5"
											placeholder="请输入开户人姓名" id="buname">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">请输入手机号</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="11"
											placeholder="请输入手机号" id="bm">
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">开户人身份证</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" maxlength="18"
											placeholder="请输入开户人身份证" id="idCard">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">默认卡</label>
									<div class="col-sm-8">
										<select class=input id="isDefault">
											<option selected value=1>是</option>
											<option value=2>否</option>
										</select>
									</div>
								</div>

							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path%>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path%>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path%>/js/plugins/webuploader/webuploader.js"></script>
	<script>
	$(document).ready(function() {
        $("select").select();
		$("#submit-save").click(function() {
			$.ajax({
				type: "post",
				url: "<%=path%>/orderInfo/newBindCardadd.htm",
				data: "bname=" + $("#bname").val() + "&bcard=" + $("#bcard").val() + "&buname=" + $("#buname").val() + "&bm=" + $("#bm").val() + "&idCard=" + $("#idCard").val() + "&isDefault=" + $("#isDefault").val(),
				success: function(msg) {
					if (msg.resultCode == "1") {
						parent.messageModel(msg.resultMsg);
						setTimeout(function() {
							history.go(-1);
						},2000);
						return;
					} else {
						parent.messageModel(msg.resultMsg);
					}
				},
				dataType: "json"
			});
		});
		 $("#cancel").click(function (){
		    parent.close();
			// window.close();
		 });

	});
	</script>