<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>现金贷产品</h5>
						<div class="ibox-tools">
<!-- 							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商品</a> -->
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">金融机构</label>
								<input type="text" name="name" placeholder="请输入机构名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${hsj}/cashLoan/cashLoanManagerSel.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>金融机构</th>'+
							'<th>额度范围</th>'+
							'<th>利息</th>'+
							'<th>时间范围</th>'+
 							'<th>分期方式</th>'+
 							'<th>逾期利息</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{name}</td>'+
							'<td>{moneyMini}-{moneyMax}</td>'+
							'<td>{interest}%天</td>'+
							'<td>{timeStr}</td>'+
							'<td id="stagesType_{id}"></td>'+
							'<td>{overdue}%天</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="6"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					$("#date_"+list[i].id).html('<img src="${res}'+list[i].img+'" style="width:75px;height:75px;"/>');
				}
				
				for(var i = 0 ; i < result.list.length ; i++){
					
					if(list[i].stagesType==1){
						$("#stagesType_"+list[i].id).html('等额本金');
					}else if(list[i].stagesType==0){
						$("#stagesType_"+list[i].id).html('等额本息');
					}
					
				}
				
				$("#loading-example-btn").click(function(){
					$pager.gotoPage($pager.pageNumber);
				});
				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('${hsj}/goodsInfo/forwardGoodsInfoDel.htm',param,$pager);
				});
		
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
			}
		});
		
	});
	
</script>

	</body>
</html>
