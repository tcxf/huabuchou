<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
<style>
        .dingdan-list{
            padding: 20px;
        }
         .dingdan-list .row{
         	padding-bottom:20px;
         }
    </style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>现金贷审核</h5>
						<div style="display:none;" class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建消费者</a>
						</div>
					</div>
					<div class="ibox-content">
					<form id="searchForm" class="form-inline">
						<div class="form-group">
							<div class="row m-b-sm m-t-sm">
								<div class="col-md-1">
									<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
								</div>
								<div class="col-sm-8">
									 
								</div>
							</div>
						</div>
						<div class="dingdan-list">
						  <div class="container">
						    <div class="row">
                               <div class="form-group col-md-4 col-lg-4">
								<label class="control-label">账号姓名</label>
									<input id="realName" type="text" class="form-control" name="realName" maxlength="30" placeholder="请输入账号名称">
							   </div>
							  <div class="form-group col-md-4 col-lg-4">
								<label class="control-label">资金方</label>
									<input id="fname" type="text" class="form-control" name="fname" maxlength="30" placeholder="请输入资金方名称">
							  </div>
							   <div class="form-group col-md-4 col-lg-4">
								<label class="control-label">申请时间</label>
										<input type="text" class="form-control" name="ratifyDate" id="ratifyDate" style="width:30%" maxlength="30" placeholder="">
								           -
										<input type="text" class="form-control" name="applyDate" id="applyDate" style="width:30%" maxlength="30" placeholder="">
								</div>
							</div>
							<div class="row">
							    <div class="form-group col-md-4 col-lg-4">
								  <label class="control-label">审核时间</label>
										<input type="text" class="form-control" name="createDate" id="createDate" style="width:30%" maxlength="30" placeholder="">
									      -
										<input type="text" class="form-control" name="modifyDate" id="modifyDate" style="width:30%" maxlength="30" placeholder="">
								</div>
							    <div class="form-group col-md-4 col-lg-4">
								  <label class="control-label">审核状态</label>
									<select id=status name="status">
										<option value="">全部</option>
										<option value="1">待审核</option>
										<option value="2">审核中</option>
										<option value="3">审核通过</option>
										<option value="5">审核不通过</option>
										<option value="4">审核失败</option>
									</select>
							    </div>
								<div class="form-group col-lg-1" style="text-align:center">
								   <input type="button" id="search" class="btn btn-warning" value="搜索" />
								</div>			  
						    </div>
						 </div>
						</div>
					</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
   		$("#status").select();
   		laydate({
			elem: '#ratifyDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#applyDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#createDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#modifyDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		
		var $pager = $("#data").pager({
			url:"${hsj}/cashloanForUser/userInfoList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>订单号</th>'+
							'<th>账号名称</th>'+
							'<th>资金方</th>'+
							'<th>手机号码</th>'+
							'<th>申请金额</th>'+
							'<th>申请时间</th>'+
							'<th>审核时间</th>'+
							'<th>审核状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{id}</td>'+
							'<td>{realName}</td>'+
							'<td>{fname}</td>'+
							'<td>{mobile}</td>'+
							'<td>{fmoney}</td>'+
							'<td>{applyDate}</td>'+
							'<td>{ratifyDate}</td>'+
							'<td id="status_{id}">{status}</td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{uid},{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					var status = list[i].status;
					if(status == 1){
						$("#status_"+list[i].id).html('待审核');
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-uid="'+list[i].uid+'" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核</span> </a>');
						$("#oper_"+list[i].id).append($examine);
						$examine.click(function(){
							var uid=$(this).attr("data-uid");
							var id=$(this).attr("data-id");
							parent.openLayer('授信审核','${hsj}/cashloanForUser/showApply.htm?id='+id+'&uid='+uid,'1000px','700px',$pager);
						});
					} else if(status == 2){
						$("#status_"+list[i].id).html('审核中');
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核中</span> </a>');
						$("#oper_"+list[i].id).append($examine);
					} else if(status == 3){
						$("#status_"+list[i].id).html('审核通过');
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核成功</span> </a>');
						$("#oper_"+list[i].id).append($examine);
					}else if(status == 4){
						$("#status_"+list[i].id).html('审核失败');
						$("#oper_"+list[i].id).append("");
					}else{
						$("#status_"+list[i].id).html('审核不通过');
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核未通过</span> </a>');
						$("#oper_"+list[i].id).append($examine);
					} 
					
				}
				
				$("#loading-example-btn").click(function(){
					$pager.gotoPage($pager.pageNumber);
				});
				//编辑页面
				$(".detail").click(function (){
					var id = $(this).attr("data").split(",")[1];
					var uid = $(this).attr("data").split(",")[0];
					parent.openLayer('贷款详情-','${hsj}/cashloanForUser/shDetails.htm?id='+id+'&uid='+uid,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
	
		
	});
	
</script>

	</body>
</html>