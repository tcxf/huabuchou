<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <#include "common/common.ftl">
    <title>初始授信</title>
    <style>
        input[type="number"]{
            width: 80px;
            text-align: center;
        }
        .left_mume .xuanzhong>a{
            background: #f49110;
            color: #fff;
            display: block;
        }
        .left_mume .xuanzhong>a:hover{
            background: #f49110;
            color: #fff;
            display: block;
        }
        .numb_modal .col-sm-4:first-child{
            padding-left: 20px;
            text-align: left;
        }
    </style>

</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="all_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 left_mume">
                    <ul>
                        <li>
                            <a href="">拒绝授信</a>
                        </li>
                        <li class="xuanzhong">
                            <a href="">初始授信</a>
                        </li>
                        <li>
                            <a href="">提额授信</a>
                        </li>
                        <li>
                            <a href="">授信提额综合分值</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-10">
                    <div class="top_text">
                        <div class="top_cont">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-4 no_sx">
                                        提额授信
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tow_table">
                            <!--表单内容-->
                            <table class="table" >
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>条件</th>
                                    <th>权重</th>
                                    <th>状态</th>
                                    <th class="operation">操作</th>
                                </tr>
                                </thead>
                                <tbody name="table_tt" id="table_tt">
            <#list map?keys as key>
            <tr>
                    <#if key == "1">
                        <td rowspan="6">户籍</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="6">
                            <a href="#" class="city edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
                    <#if key == "2">
                        <td rowspan="3">性别</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="3">
                            <a href="#" class="sex edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
                    <#if key == "3">
                        <td rowspan="7">年龄</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="7">
                            <a href="#" class="age edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
                    <#if key == "4">
                        <td rowspan="6">多头负债个数</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="6">
                            <a href="#" class="fuzai edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
                    <#if key == "5">
                        <td rowspan="6">详单非标签类号码数量</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="6">
                            <a href="#" class="numb edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
                    <#if key == "6">
                        <td rowspan="6">催收类别号码数量</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="6">
                            <a href="#" class="cuishou edit_btn">
                                <img src="${basePath!}/img/u1733.png" alt="">
                                编辑
                            </a>
                        </td>
                    </#if>
            <td>
                    <#assign list = map[key]>
                    <#list list.rbcilist as lists>
                        <tr>
                            <td name="listName" id="${lists.code}">${lists.name!}</td>
                            <td name="listValue" id="${lists.code}${lists_index}">${lists.value!}</td>
                            <td name="listCode" hidden>${lists.code}</td>
                            <td name="listTypeId" id="${lists.typeId}_${lists_index}" hidden>${lists.typeId}</td>
                            <td name="listStatus" id="${lists.code}_${lists_index}">
                                <#if lists.status == "0">
                                    禁用
                                <#else>
                                    启用
                                </#if>
                            </td>
                            <td name="listMinScore" id="minScore_${lists.code}" hidden>${lists.minScore!}</td>
                            <td name="listMinScore" id="maxScore_${lists.code}" hidden>${lists.maxScore!}</td>
                        </tr>
                    </#list>
            </td>
            </tr>
            </#list>
                                </tbody>
                            </table>
                            <!--编辑户籍城市-->
                            <div class="sore_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑户籍城市</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="tab-container">
                                            <ul>
                                                <li class="active"><a data-toggle="tab" href="#tab-a">一线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-b">二线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-c">三线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-d">四线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-e">其他城市</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab-a" class="tab-pane active">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="city_1" id="city_1">
                                                                        <option value="0">0</option>
                                                                        <option value="0.1">0.1</option>
                                                                        <option value="0.2">0.2</option>
                                                                        <option value="0.3">0.3</option>
                                                                        <option value="0.4">0.4</option>
                                                                        <option value="0.5">0.5</option>
                                                                        <option value="0.6">0.6</option>
                                                                        <option value="0.7">0.7</option>
                                                                        <option value="0.8">0.8</option>
                                                                        <option value="0.9">0.9</option>
                                                                        <option value="1">1</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="cityRadio1" id="cityRadio1_yes" value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="cityRadio1" id="cityRadio1_no" value="0">
                                                                    <label for="">禁用</label>
                                                                </li>

                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-b" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="city_2" id="city_2">
                                                                        <option value="0">0</option>
                                                                        <option value="0.1">0.1</option>
                                                                        <option value="0.2">0.2</option>
                                                                        <option value="0.3">0.3</option>
                                                                        <option value="0.4">0.4</option>
                                                                        <option value="0.5">0.5</option>
                                                                        <option value="0.6">0.6</option>
                                                                        <option value="0.7">0.7</option>
                                                                        <option value="0.8">0.8</option>
                                                                        <option value="0.9">0.9</option>
                                                                        <option value="1">1</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="cityRadio2" id="cityRadio2_yes" value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="cityRadio2" id="cityRadio2_yes" value="0">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-c" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="city_3" id="city_3">
                                                                        <option value="0">0</option>
                                                                        <option value="0.1">0.1</option>
                                                                        <option value="0.2">0.2</option>
                                                                        <option value="0.3">0.3</option>
                                                                        <option value="0.4">0.4</option>
                                                                        <option value="0.5">0.5</option>
                                                                        <option value="0.6">0.6</option>
                                                                        <option value="0.7">0.7</option>
                                                                        <option value="0.8">0.8</option>
                                                                        <option value="0.9">0.9</option>
                                                                        <option value="1">1</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="cityRadio3" id="cityRadio3_yes" value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="cityRadio3" id="cityRadio3_yes" value="0">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-d" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="city_4" id="city_4">
                                                                        <option value="0">0</option>
                                                                        <option value="0.1">0.1</option>
                                                                        <option value="0.2">0.2</option>
                                                                        <option value="0.3">0.3</option>
                                                                        <option value="0.4">0.4</option>
                                                                        <option value="0.5">0.5</option>
                                                                        <option value="0.6">0.6</option>
                                                                        <option value="0.7">0.7</option>
                                                                        <option value="0.8">0.8</option>
                                                                        <option value="0.9">0.9</option>
                                                                        <option value="1">1</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="cityRadio4" id="cityRadio4_yes" value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="cityRadio4" id="cityRadio4_no" value="0">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-e" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="city_5" id="city_5">
                                                                        <option value="0">0</option>
                                                                        <option value="0.1">0.1</option>
                                                                        <option value="0.2">0.2</option>
                                                                        <option value="0.3">0.3</option>
                                                                        <option value="0.4">0.4</option>
                                                                        <option value="0.5">0.5</option>
                                                                        <option value="0.6">0.6</option>
                                                                        <option value="0.7">0.7</option>
                                                                        <option value="0.8">0.8</option>
                                                                        <option value="0.9">0.9</option>
                                                                        <option value="1">1.0</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="cityRadio5" id="cityRadio5_yes" value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="cityRadio5" id="cityRadio5_no" value="0">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok" id="city_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑性别-->
                            <div class="sex_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑性别</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="tab-container" style="margin-left: 10px">
                                            <ul>
                                                <li class="active"><a data-toggle="tab" href="#tab-e1">男</a>
                                                </li>
                                                <li><a data-toggle="tab" href="#tab-f">女</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <div id="tab-e1" class="tab-pane active">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                性别
                                                            </div>
                                                            <div class="col-sm-4">
                                                                男
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                评分权重
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select name="sex_1" id="sex_1">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                状态
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="radio" name="sexRadio1" id="sexRadio1_yes" value="1">
                                                                <label for="">启用</label>
                                                                <input type="radio" name="sexRadio1" id="sexRadio1_no" value="0">
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab-f" class="tab-pane">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                性别
                                                            </div>
                                                            <div class="col-sm-4">
                                                                女
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                评分权重
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select name="sex_2" id="sex_2">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                状态
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="radio" name="sexRadio2" id="sexRadio2_yes" value="1">
                                                                <label for="">启用</label>
                                                                <input type="radio" name="sexRadio2" id="sexRadio2_no" value="0">
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok"  id="sex_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑年龄权重-->
                            <div class="age_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑年龄权重</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_start" type="text">
                                                                ～
                                                                <input id="age_input_1" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_1" id="age_1">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio1" id="ageRadio1_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio1" id="ageRadio1_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_2" type="text">
                                                                ～
                                                                <input id="age_input_3" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_2" id="age_2">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio2" id="ageRadio2_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio2" id="ageRadio2_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_4" type="text">
                                                                ～
                                                                <input id="age_input_5" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_3" id="age_3">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio3" id="ageRadio3_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio3" id="ageRadio3_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_6" type="text">
                                                                ～
                                                                <input id="age_input_7" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_4" id="age_4">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio4" id="ageRadio4_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio4" id="ageRadio4_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_8" type="text">
                                                                ～
                                                                <input id="age_input_9" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_5" id="age_5">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio5" id="ageRadio5_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio5" id="ageRadio5_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input id="age_input_10" type="text">
                                                                ～
                                                                <input id="age_input_end" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="age_6" id="age_6">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio6" id="ageRadio6_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="ageRadio6" id="ageRadio6_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li id="age_ok" class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑多头负债-->
                            <div class="fuzai_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑多头负债</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input id="fuzai_input_start" type="text">
                                                                ～
                                                                <input id="fuzai_input_1" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="module_1" id="module_1">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio1" id="fuzaiRadio1_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio1" id="fuzaiRadio1_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input id="fuzai_input_2" type="number">
                                                                ～
                                                                <input id="fuzai_input_3" type="number">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="module_2" id="module_2">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio2" id="fuzaiRadio2_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio2" id="fuzaiRadio2_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input id="fuzai_input_4" type="number">
                                                                ～
                                                                <input id="fuzai_input_5" type="number">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="module_3" id="module_3">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio3" id="fuzaiRadio3_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio3" id="fuzaiRadio3_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input id="fuzai_input_6" type="number">
                                                                ～
                                                                <input id="fuzai_input_7" type="number">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="module_4" id="module_4">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio4" id="fuzaiRadio4_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio4" id="fuzaiRadio4_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input id="fuzai_input_8" type="number">
                                                                ～
                                                                <input id="fuzai_input_end" type="number">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="module_5" id="module_5">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio5" id="fuzaiRadio5_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzaiRadio5" id="fuzaiRadio5_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok" id="fuzai_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑详单非标签类号码数量-->
                            <div class="numb_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑详单非标签类号码数量</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="numb_input_start">
                                                                及以上
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="numb_1" id="numb_1">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio1" id="numbRadio1_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio1" id="numbRadio1_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="numb_input_2">
                                                                ～
                                                                <input type="text" id="numb_input_3">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="numb_2" id="numb_2">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio2" id="numbRadio2_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio2" id="numbRadio2_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="numb_input_4">
                                                                ～
                                                                <input type="text" id="numb_input_5">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="numb_3" id="numb_3">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio3" id="numbRadio3_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio3" id="numbRadio3_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="numb_input_6">
                                                                ～
                                                                <input type="text" id="numb_input_7">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="numb_4" id="numb_4">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio4" id="numbRadio4_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio4" id="numbRadio4_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" style="text-align: left">
                                                                号码数量&nbsp
                                                                <input type="text" id="numb_input_8">
                                                                ～
                                                                <input type="text" id="numb_input_end">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="numb_5" id="numb_5">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio5" id="numbRadio5_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="numbRadio5" id="numbRadio5_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok" id="numb_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑催收类别号码数量-->
                            <div class="cuishou_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑催收类别号码数量</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="number" id="cuishou_input_1">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="cuishou_1" id="cuishou_1">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio1" id="cuishou1_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio1" id="cuishou1_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="cuishou_input_2">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="cuishou_2" id="cuishou_2">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio2" id="cuishou2_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio2" id="cuishou2_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input type="text" id="cuishou_input_3">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="cuishou_3" id="cuishou_3">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio3" id="cuishou3_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio3" id="cuishou3_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input id="cuishou_input_4" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="cuishou_4" id="cuishou_4">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio4" id="cuishou4_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio4" id="cuishou4_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                号码数量&nbsp
                                                                <input id="cuishou_input_5" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="cuishou_5" id="cuishou_5">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio5" id="cuishou5_yes" value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="cuishouRadio5" id="cuishou5_no" value="0">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok" id="cuishou_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--头部信息，编辑确认取消-->


</div>
</body>
</html>
<script>
    $(document).ready(function(){
        $("#fuzai_input_1").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_2").val(Number(text)+1)
        });
        $("#fuzai_input_2").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_1").val(Number(text)-1)
        });
        $("#fuzai_input_3").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_4").val(Number(text)+1)
        });
        $("#fuzai_input_4").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_3").val(Number(text)-1)
        });
        $("#fuzai_input_5").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_6").val(Number(text)+1)
        });
        $("#fuzai_input_6").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_5").val(Number(text)-1)
        });
        $("#fuzai_input_7").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_8").val(Number(text)+1)
        });
        $("#fuzai_input_8").bind("input propertychange",function(){
            var text = $(this).val();
            $("#fuzai_input_7").val(Number(text)-1)
        });
        $("#age_input_1").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_2").val(Number(text)+1)
        });
        $("#age_input_2").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_1").val(Number(text)-1)
        });
        $("#age_input_3").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_4").val(Number(text)+1)
        });
        $("#age_input_4").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_3").val(Number(text)-1)
        });
        $("#age_input_5").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_6").val(Number(text)+1)
        });
        $("#age_input_6").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_5").val(Number(text)-1)
        });
        $("#age_input_7").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_8").val(Number(text)+1)
        });
        $("#age_input_8").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_7").val(Number(text)-1)
        });
        $("#age_input_9").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_10").val(Number(text)+1)
        });
        $("#age_input_10").bind("input propertychange",function(){
            var text = $(this).val();
            $("#age_input_9").val(Number(text)-1)
        });
        $(".edit").click(function () {
            $(".confirms").css("display","inline");
            $(".cancel").css("display","inline");
            $(".edit").css("display","none");
            $(".edit_btn").css("display","block");
            $('.operation').css("color","#676a6c");
        });
        $(".confirms").click(function () {
            $(".edit").css("display","inline");
            $(".confirms").css("display","none");
            $(".cancel").css("display","none");
            $(".edit_btn").css("display","none");
            $('.operation').css("color","transparent");
        });
        $(".cancel").click(function () {
            $(".edit").css("display","inline");
            $(".confirms").css("display","none");
            $(".cancel").css("display","none");
            $(".edit_btn").css("display","none");
            $('.operation').css("color","transparent");
        });
        $(".sx_ok").click(function (){
            $(".sore_modal").css("display","none");
            $(".sex_modal").css("display","none");
            $(".age_modal").css("display","none");
            $(".fuzai_modal").css("display","none");
            $(".numb_modal").css("display","none");
            $(".cuishou_modal").css("display","none")
        });
        $(".sx_sore").click(function (){
            $(".sore_modal").css("display","none");
            $(".sex_modal").css("display","none");
            $(".age_modal").css("display","none");
            $(".fuzai_modal").css("display","none");
            $(".numb_modal").css("display","none");
            $(".cuishou_modal").css("display","none")
        });
        $(".modal_head b").click(function (){
            $(".sore_modal").css("display","none");
            $(".sex_modal").css("display","none");
            $(".age_modal").css("display","none");
            $(".fuzai_modal").css("display","none");
            $(".numb_modal").css("display","none");
            $(".cuishou_modal").css("display","none")
        });
        <!--加载编辑户籍城市-->
        $(".city").click(function () {
            $(".sore_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=1",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstCityWeight"){
                            $("#city_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cityRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "secondCityWeight") {
                            $("#city_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cityRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "threeCityWeight") {
                            $("#city_3").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cityRadio3][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fourCityWeight") {
                            $("#city_4").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cityRadio4][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "otherCityWeight") {
                            $("#city_5").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cityRadio5][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--加载编辑性别-->
        $(".sex").click(function () {
            $(".sex_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=2",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstSexWeight"){
                            $("#sex_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=sexRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else {
                            $("#sex_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=sexRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--加载编辑年龄权重-->
        $(".age").click(function () {
            $(".age_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=3",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstAgeWeight"){
                            $("#age_input_start").attr("value",list[i].minScore);
                            $("#age_input_1").attr("value",list[i].maxScore);
                            $("#age_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "secondAgeWeight") {
                            var name = list[i].name.split("-");
                            $("#age_input_2").attr("value",list[i].minScore);
                            $("#age_input_3").attr("value",list[i].maxScore);
                            $("#age_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "threeAgeWeight") {
                            var name = list[i].name.split("-");
                            $("#age_input_4").attr("value",list[i].minScore);
                            $("#age_input_5").attr("value",list[i].maxScore);
                            $("#age_3").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio3][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fourAgeWeight") {
                            $("#age_input_6").attr("value",list[i].minScore);
                            $("#age_input_7").attr("value",list[i].maxScore);
                            $("#age_4").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio4][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fiveAgeWeight") {
                            $("#age_input_8").attr("value",list[i].minScore);
                            $("#age_input_9").attr("value",list[i].maxScore);
                            $("#age_5").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio5][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "sixAgeWeight") {
                            $("#age_input_10").attr("value",list[i].minScore);
                            $("#age_input_end").attr("value",list[i].maxScore);
                            $("#age_6").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=ageRadio6][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--加载编辑多头负债-->
        $(".fuzai").click(function () {
            $(".fuzai_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=4",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstModuleWeight"){
                            $("#fuzai_input_start").attr("value",list[i].minScore);
                            $("#fuzai_input_1").attr("value",list[i].maxScore);
                            $("#module_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=fuzaiRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "secondModuleWeight") {
                            $("#fuzai_input_2").attr("value",list[i].minScore);
                            $("#fuzai_input_3").attr("value",list[i].maxScore);
                            $("#module_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=fuzaiRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "threeModuleWeight") {
                            $("#fuzai_input_4").attr("value",list[i].minScore);
                            $("#fuzai_input_5").attr("value",list[i].maxScore);
                            $("#module_3").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=fuzaiRadio3][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fourModuleWeight") {
                            $("#fuzai_input_6").attr("value",list[i].minScore);
                            $("#fuzai_input_7").attr("value",list[i].maxScore);
                            $("#module_4").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=fuzaiRadio4][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "otherModuleWeight") {
                            $("#fuzai_input_8").attr("value",list[i].minScore);
                            $("#fuzai_input_end").attr("value",list[i].maxScore);
                            $("#module_5").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=fuzaiRadio5][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--加载编辑详单非标签类号码数量-->
        $(".numb").click(function () {
            $(".numb_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=5",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstTitleWeight"){
                            $("#numb_input_start").attr("value",list[i].minScore);
                            $("#numb_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=numbRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "secondTitleWeight") {
                            $("#numb_input_2").attr("value",list[i].minScore);
                            $("#numb_input_3").attr("value",list[i].maxScore);
                            $("#numb_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=numbRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "threeTitleWeight") {
                            $("#numb_input_4").attr("value",list[i].minScore);
                            $("#numb_input_5").attr("value",list[i].maxScore);
                            $("#numb_3").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=numbRadio3][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fourTitleWeight") {
                            $("#numb_input_6").attr("value",list[i].minScore);
                            $("#numb_input_7").attr("value",list[i].maxScore);
                            $("#numb_4").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=numbRadio4][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "otherTitleWeight") {
                            $("#numb_input_8").attr("value",list[i].minScore);
                            $("#numb_input_end").attr("value",list[i].maxScore);
                            $("#numb_5").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=numbRadio5][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--加载编辑催收类别号码数量-->
        $(".cuishou").click(function () {
            $(".cuishou_modal").css("display","block");
            $.ajax({
                url : "${basePath!}/opera/InitialCredit/goUpdateInitialCredit?typeId=6",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                error : function() { //失败

                },
                success : function(d) { //成功
                    var list = d.data;
                    for (var i = 0; i < list.length ; i++){
                        if (list[i].code == "firstMobileWeight") {
                            $("#cuishou_input_1").attr("value",list[i].minScore);
                            $("#cuishou_1").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cuishouRadio1][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "secondMobileWeight") {
                            $("#cuishou_input_2").attr("value",list[i].minScore);
                            $("#cuishou_2").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cuishouRadio2][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "threeMobileWeight") {
                            $("#cuishou_input_3").attr("value",list[i].minScore);
                            $("#cuishou_3").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cuishouRadio3][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "fourMobileWeight") {
                            $("#cuishou_input_4").attr("value",list[i].minScore);
                            $("#cuishou_4").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cuishouRadio4][value="+list[i].status+"]").attr("checked",'checked');
                        } else if (list[i].code == "otherMobileWeight") {
                            $("#cuishou_input_5").attr("value",list[i].minScore);
                            $("#cuishou_5").find("option[value = '"+list[i].value+"']").attr("selected","selected");
                            $("input[type=radio][name=cuishouRadio5][value="+list[i].status+"]").attr("checked",'checked');
                        }
                    }
                }
            });
        });
        <!--户籍城市确认渲染界面-->
        $("#city_ok").click(function () {
            $("#firstCityWeight0").html($("#city_1").find("option:selected").text());
            $("#secondCityWeight1").html($("#city_2").find("option:selected").text());
            $("#threeCityWeight2").html($("#city_3").find("option:selected").text());
            $("#fourCityWeight3").html($("#city_4").find("option:selected").text());
            $("#otherCityWeight4").html($("#city_5").find("option:selected").text());

            if ($("input[name='cityRadio1']:checked").val() == "1") {
                $("#firstCityWeight_0").html("启用");
            }else {
                $("#firstCityWeight_0").html("禁用");
            }
            if ($("input[name='cityRadio2']:checked").val() == "1") {
                $("#secondCityWeight_1").html("启用");
            }else {
                $("#secondCityWeight_1").html("禁用");
            }
            if ($("input[name='cityRadio3']:checked").val() == "1") {
                $("#threeCityWeight_2").html("启用");
            }else {
                $("#threeCityWeight_2").html("禁用");
            }
            if ($("input[name='cityRadio4']:checked").val() == "1") {
                $("#fourCityWeight_3").html("启用");
            }else {
                $("#fourCityWeight_3").html("禁用");
            }
            if ($("input[name='cityRadio5']:checked").val() == "1") {
                $("#otherCityWeight_4").html("启用");
            }else {
                $("#otherCityWeight_4").html("禁用");
            }
        });
        <!--性别确认渲染界面-->
        $("#sex_ok").click(function () {
            $("#firstSexWeight0").html($("#sex_2").find("option:selected").text());
            $("#secondSexWeight1").html($("#sex_1").find("option:selected").text());
            if ($("input[name='sexRadio1']:checked").val() == "1") {
                $("#secondSexWeight_1").html("启用");
            }else {
                $("#secondSexWeight_1").html("禁用");
            }
            if ($("input[name='sexRadio2']:checked").val() == "1") {
                $("#firstSexWeight_0").html("启用");
            }else {
                $("#firstSexWeight_0").html("禁用");
            }
        });
        <!--年龄权重确认渲染界面-->
        $("#age_ok").click(function () {
            $("#minScore_firstAgeWeight").html($("#age_input_start").val());
            $("#minScore_secondAgeWeight").html($("#age_input_2").val());
            $("#minScore_threeAgeWeight").html($("#age_input_4").val());
            $("#minScore_fourAgeWeight").html($("#age_input_6").val());
            $("#minScore_fiveAgeWeight").html($("#age_input_8").val());
            $("#minScore_sixAgeWeight").html($("#age_input_10").val());

            $("#maxScore_firstAgeWeight").html($("#age_input_1").val());
            $("#maxScore_secondAgeWeight").html($("#age_input_3").val());
            $("#maxScore_threeAgeWeight").html($("#age_input_5").val());
            $("#maxScore_fourAgeWeight").html($("#age_input_7").val());
            $("#maxScore_fiveAgeWeight").html($("#age_input_9").val());

            $("#firstAgeWeight").html($("#age_input_start").val()+"～"+$("#age_input_1").val());
            $("#secondAgeWeight").html($("#age_input_2").val()+"～"+$("#age_input_3").val());
            $("#threeAgeWeight").html($("#age_input_4").val()+"～"+$("#age_input_5").val());
            $("#fourAgeWeight").html($("#age_input_6").val()+"～"+$("#age_input_7").val());
            $("#fiveAgeWeight").html($("#age_input_8").val()+"～"+$("#age_input_9").val());
            $("#sixAgeWeight").html($("#age_input_10").val()+"+");

            $("#firstAgeWeight0").html($("#age_1").find("option:selected").text());
            $("#secondAgeWeight1").html($("#age_2").find("option:selected").text());
            $("#threeAgeWeight2").html($("#age_3").find("option:selected").text());
            $("#fourAgeWeight3").html($("#age_4").find("option:selected").text());
            $("#fiveAgeWeight4").html($("#age_5").find("option:selected").text());
            $("#sixAgeWeight5").html($("#age_6").find("option:selected").text());
            if ($("input[name='ageRadio1']:checked").val() == "1") {
                $("#firstAgeWeight_0").html("启用");
            }else {
                $("#firstAgeWeight_0").html("禁用");
            }
            if ($("input[name='ageRadio2']:checked").val() == "1") {
                $("#secondAgeWeight_1").html("启用");
            }else {
                $("#secondAgeWeight_1").html("禁用");
            }
            if ($("input[name='ageRadio3']:checked").val() == "1") {
                $("#threeAgeWeight_2").html("启用");
            }else {
                $("#threeAgeWeight_2").html("禁用");
            }
            if ($("input[name='ageRadio4']:checked").val() == "1") {
                $("#fourAgeWeight_3").html("启用");
            }else {
                $("#fourAgeWeight_3").html("禁用");
            }
            if ($("input[name='ageRadio5']:checked").val() == "1") {
                $("#fiveAgeWeight_4").html("启用");
            }else {
                $("#fiveAgeWeight_4").html("禁用");
            }
            if ($("input[name='ageRadio6']:checked").val() == "1") {
                $("#sixAgeWeight_5").html("启用");
            }else {
                $("#sixAgeWeight_5").html("禁用");
            }
        });
        <!--多头负债确认渲染界面-->
        $("#fuzai_ok").click(function () {
            $("#minScore_firstModuleWeight").html($("#fuzai_input_start").val());
            $("#minScore_secondModuleWeight").html($("#fuzai_input_2").val());
            $("#minScore_threeModuleWeight").html($("#fuzai_input_4").val());
            $("#minScore_fourModuleWeight").html($("#fuzai_input_6").val());
            $("#minScore_otherModuleWeight").html($("#fuzai_input_8").val());

            $("#maxScore_firstModuleWeight").html($("#fuzai_input_1").val());
            $("#maxScore_secondModuleWeight").html($("#fuzai_input_3").val());
            $("#maxScore_threeModuleWeight").html($("#fuzai_input_5").val());
            $("#maxScore_fourModuleWeight").html($("#fuzai_input_7").val());
            $("#maxScore_otherModuleWeight").html($("#fuzai_input_end").val());

            $("#firstModuleWeight").html("小于"+$("#fuzai_input_1").val()+"个");
            $("#secondModuleWeight").html($("#fuzai_input_2").val()+"～"+$("#fuzai_input_3").val()+"个");
            $("#threeModuleWeight").html($("#fuzai_input_4").val()+"～"+$("#fuzai_input_5").val()+"个");
            $("#fourModuleWeight").html($("#fuzai_input_6").val()+"～"+$("#fuzai_input_7").val()+"个");
            $("#otherModuleWeight").html($("#fuzai_input_8").val()+"～"+$("#fuzai_input_end").val()+"个");

            $("#firstModuleWeight1").html($("#module_1").find("option:selected").text());
            $("#secondModuleWeight2").html($("#module_2").find("option:selected").text());
            $("#threeModuleWeight3").html($("#module_3").find("option:selected").text());
            $("#fourModuleWeight4").html($("#module_4").find("option:selected").text());
            $("#otherModuleWeight0").html($("#module_5").find("option:selected").text());
            if ($("input[name='moduleRadio1']:checked").val() == "1") {
                $("#firstModuleWeight_1").html("启用");
            }else {
                $("#firstModuleWeight_1").html("禁用");
            }
            if ($("input[name='moduleRadio2']:checked").val() == "1") {
                $("#secondModuleWeight_2").html("启用");
            }else {
                $("#secondModuleWeight_2").html("禁用");
            }
            if ($("input[name='moduleRadio3']:checked").val() == "1") {
                $("#threeModuleWeight_3").html("启用");
            }else {
                $("#threeModuleWeight_3").html("禁用");
            }
            if ($("input[name='moduleRadio4']:checked").val() == "1") {
                $("#fourModuleWeight_4").html("启用");
            }else {
                $("#fourModuleWeight_4").html("禁用");
            }
            if ($("input[name='moduleRadio5']:checked").val() == "1") {
                $("#otherModuleWeight_0").html("启用");
            }else {
                $("#otherModuleWeight_0").html("禁用");
            }
        });
        <!--详单非标签类号码数量确认渲染界面-->
        $("#numb_ok").click(function () {
            $("#minScore_firstTitleWeight").html($("#numb_input_start").val());
            $("#minScore_secondTitleWeight").html($("#numb_input_2").val());
            $("#minScore_threeTitleWeight").html($("#numb_input_4").val());
            $("#minScore_fourTitleWeight").html($("#numb_input_6").val());
            $("#minScore_otherTitleWeight").html($("#numb_input_8").val());

            $("#maxScore_firstTitleWeight").html($("#numb_input_1").val());
            $("#maxScore_secondTitleWeight").html($("#numb_input_3").val());
            $("#maxScore_threeTitleWeigh").html($("#numb_input_4").val());
            $("#maxScore_fourTitleWeigh").html($("#numb_input_5").val());

            var str = $("#numb_input_start").val();
            str = str-1;
            $("#firstTitleWeight").html(str+"个以上");
            $("#secondTitleWeight").html($("#numb_input_2").val()+"-"+$("#numb_input_3").val()+"个");
            $("#threeTitleWeigh").html($("#numb_input_4").val()+"-"+$("#numb_input_5").val()+"个");
            $("#fourTitleWeigh").html($("#numb_input_6").val()+"-"+$("#numb_input_7").val()+"个");
            $("#otherTitleWeight").html($("#numb_input_8").val()+"-"+$("#numb_input_end").val()+"个");

            $("#firstTitleWeight0").html($("#numb_1").find("option:selected").text());
            $("#secondTitleWeight1").html($("#numb_2").find("option:selected").text());
            $("#threeTitleWeigh2").html($("#numb_3").find("option:selected").text());
            $("#fourTitleWeigh3").html($("#numb_4").find("option:selected").text());
            $("#otherTitleWeight4").html($("#numb_5").find("option:selected").text());
            if ($("input[name='numbRadio1']:checked").val() == "1") {
                $("#firstTitleWeight_0").html("启用");
            }else {
                $("#firstTitleWeight_0").html("禁用");
            }
            if ($("input[name='numbRadio2']:checked").val() == "1") {
                $("#secondTitleWeight_1").html("启用");
            }else {
                $("#secondTitleWeight_1").html("禁用");
            }
            if ($("input[name='numbRadio3']:checked").val() == "1") {
                $("#threeTitleWeight_2").html("启用");
            }else {
                $("#threeTitleWeight_2").html("禁用");
            }
            if ($("input[name='numbRadio4']:checked").val() == "1") {
                $("#fourTitleWeight_3").html("启用");
            }else {
                $("#fourTitleWeight_3").html("禁用");
            }
            if ($("input[name='numbRadio5']:checked").val() == "1") {
                $("#otherTitleWeight_4").html("启用");
            }else {
                $("#otherTitleWeight_4").html("禁用");
            }
        });
        <!--催收类别号码数量确认渲染界面-->
        $("#cuishou_ok").click(function () {
            $("#minScore_firstMobileWeight").html($("#cuishou_input_1").val());
            $("#minScore_secondMobileWeight").html($("#cuishou_input_2").val());
            $("#minScore_threeMobileWeight").html($("#cuishou_input_3").val());
            $("#minScore_fourMobileWeight").html($("#cuishou_input_4").val());
            $("#minScore_otherMobileWeight").html($("#cuishou_input_5").val());

            $("#firstMobileWeight").html("催收类别号码数量"+$("#cuishou_input_1").val()+"个");
            $("#secondMobileWeight").html("催收类别号码数量"+$("#cuishou_input_2").val()+"个");
            $("#threeMobileWeight").html("催收类别号码数量"+$("#cuishou_input_3").val()+"个");
            $("#fourMobileWeight").html("催收类别号码数量"+$("#cuishou_input_4").val()+"个");
            $("#otherMobileWeight").html("催收类别号码数量"+$("#cuishou_input_5").val()+"个");

            $("#firstMobileWeight0").html($("#cuishou_1").find("option:selected").text());
            $("#secondMobileWeight1").html($("#cuishou_2").find("option:selected").text());
            $("#threeMobileWeight2").html($("#cuishou_3").find("option:selected").text());
            $("#fourMobileWeight3").html($("#cuishou_4").find("option:selected").text());
            $("#otherMobileWeight4").html($("#cuishou_5").find("option:selected").text());
            if ($("input[name='cuishouRadio1']:checked").val() == "1") {
                $("#firstMobileWeight_0").html("启用");
            }else {
                $("#firstMobileWeight_0").html("禁用");
            }
            if ($("input[name='cuishouRadio2']:checked").val() == "1") {
                $("#secondMobileWeight_1").html("启用");
            }else {
                $("#secondMobileWeight_1").html("禁用");
            }
            if ($("input[name='cuishouRadio3']:checked").val() == "1") {
                $("#threeMobileWeight_2").html("启用");
            }else {
                $("#threeMobileWeight_2").html("禁用");
            }
            if ($("input[name='cuishouRadio4']:checked").val() == "1") {
                $("#fourMobileWeight_3").html("启用");
            }else {
                $("#fourMobileWeight_3").html("禁用");
            }
            if ($("input[name='cuishouRadio5']:checked").val() == "1") {
                $("#otherMobileWeight_4").html("启用");
            }else {
                $("#otherMobileWeight_4").html("禁用");
            }
        });
        $("#no_ok").click(function () {
            window.location.reload();
        });
        <!--提交-->
        $("#all_ok").click(function () {
            var tempTr = $("#table_tt").children("tr");
            var array = new Array();
            for (var i = 0; i <tempTr.length ; i++) {
                var data = {};
                var tempTd = tempTr.eq(i).find("td");
                var td1= tempTd.eq(0);
                var td2= tempTd.eq(1);
                var td3= tempTd.eq(2);
                var td4= tempTd.eq(3);
                var td5= tempTd.eq(4);
                var td6= tempTd.eq(5);
                var td7= tempTd.eq(6);
                if (td1.attr("id") == undefined ) {

                }else {
                    td1=eval(td1);
                    var name =td1.text();
                    data.name = name;
                    td2=eval(td2);
                    var value = td2.text();
                    data.value = value;
                    td3=eval(td3);
                    var code = td3.text();
                    data.code = code;
                    td4=eval(td4);
                    var typeId = td4.text();
                    data.typeId = typeId;
                    td5=eval(td5);
                    var status = td5.text();
                    if (status.indexOf("启用") != -1){
                        status = "1";
                    } else {
                        status = "0";
                    }
                    data.status = status;
                    var minScore = td6.text();
                    data.minScore = minScore;
                    var maxScore = td7.text();
                    data.maxScore = maxScore;
                    array.push(data);
                }
            }

            $.ajax({
                url : "${basePath!}/opera/InitialCredit/commitInitialCredit",
                type : 'post', //数据发送方式
                dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                contentType:"application/json",
                data : JSON.stringify(array),
                error : function() { //失败

                },
                success : function(d) { //成功
                    if (d.code == 1){
                        parent.messageModel(d.msg);
                    }
                }
            });
        });
    });
</script>
