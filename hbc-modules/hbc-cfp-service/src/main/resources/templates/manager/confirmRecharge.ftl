<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<%=path%>/css/bootstrap.css" />
    <title></title>
    <style>

        #p-content{
            width: 600px;
            height: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow:10px 10px 10px 10px #dadada;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 13px;
            font-weight: bold;
        }
        #p-content .p-footer{
            margin-top: 50%;
        }
    </style>
</head>
<body>
 <div id="p-content">
     <form>
         <div class="form-group">
             <label>已向手机号${info.mobile}发送验证码</label>
             <div>
                 <input type="text" placeholder="输入验证码" width="50%" id='yCode'>
             </div>
             
             <div>
				<a  id='hq' class="btn btn-default sms-btn"
					style=" font-size: 0.8em; padding: 5px; " href="#" role="button">获取验证码</a>
			</div>
         </div>
     </form>
     <div class="p-footer">
         <button type="submit" class="btn btn-info btn-block" id="tj">完成</button>
     </div>
 </div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	
 
 <script>
	/*已过期 */	
 	$(document).ready(function() {
				
	
			$("#hq").click(function(){
			<%-- 	$.post('<%=path%>/system/getValiCode.htm',{mobile:${info.mobile},
					type:5},function(d){
					if(d.resultCode == 1){
						startTime();
						
					}
				},"json");
				 --%>
				$.ajax({
					url:"<%=path%>/system/getValiCode.htm",
				    type:'POST', //GET
				    data:{mobile:${info.mobile},
						type:5},
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	if(d.resultCode == 1){
							startTime();
							
						}
				    }
				});
			
				
			});
			var i = 60;
			function startTime(){
				$("#hq").html("获取验证码("+i+"秒)");
				if(i > 0){
					i--;
					setTimeout(function(){
						startTime();
					},1000);
				}else{
					$("#hq").html("获取验证码");
					$("#hq").removeClass("disabled");
					i = 60;
				}
			}
		
			$("#tj").on("click",function(){
				$.ajax({
					type:"post",
					url:"<%=path%>/interfacePurse/doRecharge.htm",
					data:{rechargeBalance:${txje },
						 sjje:${sjje},
						yCode:$("#yCode").val(),
					},
					   success:function(msg){
						   parent.messageModel(msg.resultMsg); 
						   /* var index = layer.load(2, {time: 10*1000});
						   layer.close(index);  */ 
						   // parent.c.gotoPage(null); 
						   parent.closeLayer();     
							if(msg.resultCode == "1"){  
								<%-- window.location.href = "<%=path%>/page/index.htm";     --%> 
	 							return; 
							}
					},
					dataType : "json"
				});
			}); 
		});
	
</script>
</body>
</html>
