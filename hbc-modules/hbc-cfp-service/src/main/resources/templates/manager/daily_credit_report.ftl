<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <style>
        body{
            background: #fff!important;
        }
        .sx_date_form {
            margin-top: 10px;
            margin-left: 20px;
        }

        .sx_date_form .col-sm-4 {
            height: 60px;
        }

        .sx_date_form .col-sm-3 input {
            width: 120px;
            height: 30px;
        }

        .sx_date_form .col-sm-2 > select {
            width: 160px;
            height: 30px;
        }

        .sx_date_form .col-sm-5{
            text-align: right;
        }

        .sx_date_form .col-sm-5>div{
            color: #f49110;
            width: 60px;
            height: 30px;
            line-height: 30px;
            display: inline-block;
            border: 1px solid #f49110;
            border-radius: 3px;
            text-align: center;
        }

        .sx_date_form .col-sm-5 img {
            width: 16px;
        }

    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        授信日报查询
                    </div>
                </div>
            </div>
        </div>
        <div class="sx_date_form">
            <form id="searchForm" class="form-inline">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4" style="padding-left:0px;">
                            <input type="text" class="form-control" name="startDate" id="startDate" style="width:200px" maxlength="30" placeholder="开始时间">
                            <input type="text" class="form-control" name="endDate" id="endDate" style="width:200px" maxlength="30" placeholder="结束时间">
                        </div>
                        <!--    <div class="col-sm-2">
                                <select name="" id="">
                                    <option value="">筛选评分规则</option>
                                    <option value="">2018-09-10调整</option>
                                    <option value="">2018-09-01调整</option>
                                </select>
                            </div>-->
                        <div class="col-sm-1">
                            <input type="button" id="search" class="btn btn-warning" value="搜索"/>
                        </div>
                        <div class="col-sm-5 col-sm-offset-2">
                            <div id="exportExcle">
                                <img src="${basePath!}/img/u4205.png" alt="">
                                <span>导出</span>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
            <div class="date_form" id="date">

            </div>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>


<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script>
    $(document).ready(function () {
        //下载报表
        $("#exportExcle").click(function(){
            $("#searchForm").attr("action","${basePath!}/opera/CreditDaily/exportExcle");
            $("#searchForm").submit();
        });
        laydate({
            elem: '#startDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD' //日期格式
        });
        laydate({
            elem: '#endDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD' //日期格式
        });
        $("#date").pager({
            url: "${basePath!}/opera/CreditDaily/findcrdelist",
            formId: "searchForm",
            pagerPages: 3,
            template: {
                header: '<div class="fixed-table-container form-group">' +
                '<table class="table table-hover table-bordered">' +
                '<thead>' +
                '<tr>' +
                ' <th>授信时间</th>' +
                '<th>新增授信人数</th>' +
                ' <th>新增授信额度 (元)</th>' +
                '<th>授信通过率</th>' +
                '<th>申请提额人数</th>' +
                '<th>人均提额金额 (元)</th>' +
                ' <th>人均授信额度 (元)</th>' +
                '</tr>' +
                '</thead><tbody>',
                body: '<tr>' +
                '<td>{createTime}</td>' +
                '<td>{sxNumberPeople}</td>' +
                '<td >{creditLine}</td>' +
                '<td>{passingRate}</td>' +
                '<td>{upmoneyNumberPeople}</td>' +
                '<td>{percapitaUpMoney}</td>' +
                '<td>{percapitaSxMoney}</td>' +
                '</tr>',
                footer: '</tbody></table>',
                noData: '<tr><td colspan="9"><center>暂无数据</center></tr>'
            },
            callbak: function (result) {


            }


        });
    })
    ;

</script>
</body>

</html>
