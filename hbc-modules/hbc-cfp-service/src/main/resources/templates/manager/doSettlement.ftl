<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=path %>/css/animate.css" rel="stylesheet">
    <link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>线上结算</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="_f" method="post" class="form-horizontal">
                        	<input type="hidden" name="id" value="${id}"/>
                            <div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">商家结算金额</label>
									<label id="unum" class="col-sm-8 control-label" style="text-align: left;">
										${amount - platformShareFee}
									</label>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">平台结算金额</label>
									<label id="unum" class="col-sm-8 control-label" style="text-align: left;">
										${platformShareFee}
									</label>
								</div>
								<div class="hr-line-dashed" style="clear:both;"></div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结算方式</label>
									<label id="unum" class="col-sm-8 control-label" style="text-align: left;">
										<select id="paymentType">
<!-- 	                                    	<option value="aliPayment">支付宝扫码</option> -->
	                                    	<option value="wechatPayment">微信扫码</option>
	                                    	<option value="directPayment">网银快捷</option>
	                                    </select>
									</label>
								</div>
								<div class="ercode form-group col-sm-6">
									<label class="col-sm-4 control-label">获取支付码</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										<a href="javascript:void(0);" class="btn btn-info btn-sm getCode"><i class="fa fa-edit"></i> 点击获取 </a>
									</label>
								</div>
								<div class="ercode hr-line-dashed" style="clear:both;"></div>
								<div class="ercode form-group col-sm-6">
									<label class="col-sm-4 control-label"></label>
									<label id="erCode" class="col-sm-10 control-label">
										
									</label>
								</div>
								<div class="card form-group col-sm-6" style="display:none;">
									<label class="col-sm-4 control-label">支付卡</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										<select id="cardInfo">
											<option value="">请选择支付卡</option>
											<c:forEach items="${cardList}" var="i">
											<option value="${i.id}">${i.name} ${i.bankCard}</option>
											</c:forEach>
										</select>
									</label>
								</div>
								<div class="card hr-line-dashed" style="clear:both;display:none;"></div>
								<div class="card form-group col-sm-6" style="display:none;">
									<label class="col-sm-4 control-label">短信验证码</label>
									<label class="col-sm-8 control-label" style="text-align: left;">
										<input type="text" class="form-control" id="code" validate-rule="required,number,maxLength[7]" placeholder="请填写短信验证码">
										<a href="javascript:void(0);" class="btn btn-info btn-sm sms-btn"><i class="fa fa-edit"></i> 点击获取 </a>
									</label>
								</div>
								<div class="card hr-line-dashed" style="clear:both;display:none;"></div>
	                            <div class="card form-group">
	                                <div class="col-sm-4 col-sm-offset-2">
	                                    <input class="btn btn-primary pay-btn" id="submit-save" type="button" value="确认结算" />
	                                </div>
	                            </div>
							</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
        	$("#paymentType").select();
        	$("#cardInfo").select();
        	$("#paymentType").change(function(){
        		if($(this).val() == "directPayment"){
        			$(".ercode").hide();
        			$(".card").show();
        		}else{
        			$(".card").hide();
        			$(".ercode").show();
        		}
        	});
            $(".getCode").click(function(){
            	if($(this).hasClass("disabled")){
            		return;
            	}
            	$(this).addClass("disabled");
            	$(this).html("获取中..");
            	$this = $(this);
            	$.post('<%=path%>/settlementDetail/createSettlement.htm',{id:'${id}',paymentType:$("#paymentType").val()},function(d){
					var paymentInfo = d.data.paymentInfo.obj[0];
					if(paymentInfo.errorMsg != null){
						parent.messageModel(paymentInfo.errorMsg);
					}else{
						$("#erCode").html('<img src="'+paymentInfo.code_img_url+'" style="width:100%;height:auto;"/>');
					}
					$this.removeClass("disabled");
					$this.html("点击获取");
            	},'json');
            });
            
            
            $(".sms-btn").click(function(){
            	var cid = $("#cardInfo").val();
            	if(cid == ""){
            		parent.messageModel("请选择支付卡");
            		return;
            	}
				if($(this).hasClass("disabled")) return;
				$(".sms-btn").addClass("disabled");
// 				发送短信验证码
				$.post('<%=path%>/settlementDetail/getDirectPaymentVcode.htm',{
					id:cid,
					sid:'${id}'
				},function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != 1){
						$(".sms-btn").removeClass("disabled");
					}else{
						startTime();
					}
				},'json');
			});
			
			var i = 60;
			function startTime(){
				$(".sms-btn").html('<i class="fa fa-edit"></i> 点击获取('+i+'秒)');
				if(i > 0){
					i--;
					setTimeout(function(){
						startTime();
					},1000);
				}else{
					$(".sms-btn").html('<i class="fa fa-edit"></i> 点击获取');
					$(".sms-btn").removeClass("disabled");
					i = 60;
				}
			}
			
			$(".pay-btn").click(function(){
				if($(this).hasClass('disabled')) return;
				$this = $(this);
				var code = $.trim($("#code").val());
				if(code == ""){
					parent.messageModel('请输入验证码');
					return;
				}
				$(this).val('支付处理中...');
				$(this).addClass('disabled');
				$.post('<%=path%>/settlementDetail/payWithDirect.htm',{
					smsCode:code
				},function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != -1){
						parent.messageModel('结算成功');
					}else{
						$this.removeClass('disabled');
						$this.val('确认结算');
					}
				},'json');
			});
        });
    </script>
</body>

</html>
