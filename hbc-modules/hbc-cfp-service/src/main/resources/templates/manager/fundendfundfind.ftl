<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<style>
		.form-horizontal .control-label {
			padding-top: 0;
		}
        .form-horizontal .col-sm-8{
			padding-top: 7px;
		}
	</style>
	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 资金机构基本信息</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
				</ul>
				<form id="_f" method="post" class="form-horizontal">
					<div class="tab-content">
						<div id="base" class="ibox-content active tab-pane">
							<input type="hidden" name="id" value="${id}"/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">手机号码</label>
									<div class="col-sm-8">
									<span>${entity.mobile}</span>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">资金机构名称</label>
									<div class="col-sm-8">
									<span>${entity.fname}</span>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">法人名称</label>
									<div class="col-sm-8">
									<span>${entity.name}</span>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">法人身份证</label>
									<div class="col-sm-8">
									<span>${entity.bankcard}</span>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<div class="form-group">
									<label class="col-sm-2 control-label">所属地区</label>
									<div class="col-sm-10" id="area" data-name="areaId" data-value="${entity.address!}">
									</div>
								</div>
							</div>
                            <div class="hr-line-dashed" style="clear:both"></div>
							<div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">详情地址</label>
                                    <div class="col-sm-8">
                                        <span>${entity.addressInfo!}</span>
                                    </div>
                                </div>
							</div>
						</div>

						<div id="settlement" class="ibox-content tab-pane">
							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">分期利率</label>
									<div class="col-sm-8">
										<span>${tbRepaymentRatioDict.serviceFee!}%</span>
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">逾期还款费率</label>
									<div class="col-sm-8">
										<span>${tbRepaymentRatioDict.yqRate!}%</span>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<script>
		$(document).ready(function () {
			$("#shareFlag").change(function(){
				var t = $(this).val();
				if(t == "false"){
					$("#shareFee").attr("disabled","disabled");
				}else{
					$("#shareFee").removeAttr("disabled");
				}
			});

			$("#area").area();
			examine = false;
			$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
				  var target =e.target.toString(); //$(e.target)[0].hash;
				  if( target.indexOf('examine')>0 && !examine){
					$(".img").uploadImg();
					examine = true;
				  }
			});
			var nodata = false;

			$("select").select();
			$(".cancel").click(function(){parent.closeLayer();});
		});
	</script>
</body>

</html>
