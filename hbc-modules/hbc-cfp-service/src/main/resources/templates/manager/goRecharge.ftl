<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<%=path%>/css/bootstrap.css" />
    <title></title>
    <style>

        #p-content{
            width: 600px;
            height: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow:10px 10px 10px 10px #dadada;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 13px;
            font-weight: bold;
        }
        .p-dz{
            margin-top: 10px;
        }
        #p-content .p-footer{
            margin-top: 50%; 
        }
    </style>
</head>
<body>
    <div id="p-content">
        <form id="_f">
            <div class="form-group">
                <label>充值金额</label>
                <div>
                    <input type="text" placeholder="请输入金额" width="50%" id='money' name="money" class="form-control"  validate-rule="txmoney"   value="" />
                </div>
                <!-- <div class="p-dz" id='sjje'></div>  -->
            </div>
           <%--  <div class="form-group">
                <label>请选择银行卡</label>
                <div>
                    <select class="form-control" id="micId" name="micId">
	                    <c:forEach items="${list}" var="i">
							<option id="id" value="${i.id}">${i.bankCard } ${i.bankName }</option>
						</c:forEach>
                    </select>
                </div>
                <!-- <div class="p-dz">根据银行规定每日提现金额最高5万，T+1到账（24小时内）每笔提现手续费 2 元</div> -->
            </div> --%>
            <div class="p-footer">
                <button type="button" class="btn btn-info btn-block" id='wc'>下一步</button>
            </div>
        </form>
    </div>
    
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
 
    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	
	 
	<script type="text/javascript">
	$(document).ready(function(){
		var sjje=null;
		/* $('#txje').bind('input propertychange', function() {
			if($(this).val().length==0){
				$("#sjje").html("实际到账：￥0");
			}else{
			 sjje=   $(this).val()-'${w.sxje}' ;
			$("#sjje").html("实际到账：￥"+sjje);	
			}
		}); */
		
		
		$("#wc").click(function (){
			if($("#_f").validate()){
				
				//var ye='${w.zje}'; 
				var money = $("#money").val();
				api.tools.ajax({
					url:'<%=path%>/interfacePurse/recharge.htm',
					data:{
						money:money
					}
				},function(d){
					//跳支付页面
					window.location = d.data;
					
				});
				//var id = $("#id").val();
				//window.location.href = '${hsj}/interfacePurse/confirmRecharge.htm?id='+id+'&money='+money+'&sjje='+sjje;  
				// parent.openLayer('钱包提现','${hsj}/wallet/wallettxwcManager.htm?id='+id+'&txje='+txje+'&sjje='+sjje,'1000px','700px'); 
			}
		});
	
	});
	
</script>
</body>
</html>
