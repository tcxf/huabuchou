<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			- 基本表单
		</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="shortcut icon" href="favicon.ico">
		<link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
		<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
		<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
		<link href="<%=path %>/css/animate.css" rel="stylesheet">
		<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	</head>
	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-sm-12">
					<div class="tabs-container">
						<form id="_f" method="post" class="form-horizontal">
							<c:choose>
								<c:when test="${type == 1}">
									<div class="ibox-title">
				                        <h5>基本信息</h5>
				                    </div>
									<div class="ibox-content active tab-pane">
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">账户id</label>
			                                <div class="col-xs-8">
				                                ${dto.unum}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">账户姓名</label>
			                                <div class="col-xs-8">
				                                ${dto.realName}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">手机号码</label>
			                                <div class="col-xs-8">
				                                ${dto.mobile}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">账户状态</label>
			                                <div class="col-xs-8">
				                                ${dto.status?"可用":"不可用"}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">身份照号码</label>
			                                <div class="col-xs-8">
				                                ${dto.idCard}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">是否绑卡</label>
			                                <div class="col-xs-8">
				                                ${dto.bankCard == null ? "未绑卡" : "已绑卡"}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">开户行</label>
			                                <div class="col-xs-8" id="bank-sn" data="${dto.bankSn}">
				                                --
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">银行卡号</label>
			                                <div class="col-xs-8">
				                                ${dto.bankCard}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">开户姓名</label>
			                                <div class="col-xs-8">
				                                ${dto.name}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">账户类型</label>
			                                <div class="col-xs-8">
				                                ${dto.type == 1 ? "个人" : "企业"}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">手机卡密码</label>
			                                <div class="col-xs-8">
				                                ${dto.deMobilePwd}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">婚姻状况</label>
			                                <div class="col-xs-8">
				                                ${dto.isMarry ? "已婚" : "未婚"}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">所在地区</label>
			                                <div class="col-xs-8">
				                                ${dto.displayName}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">家庭地址</label>
			                                <div class="col-xs-8">
				                                ${dto.address}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label">紧急联系人</label>
			                                <div class="col-xs-8">
				                                ${dto.ugrenName} ${dto.ugrenMobile}
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
									</div>
									<div style="height:20px;"></div>
									<div class="ibox-title">
				                        <h5>授信信息</h5>
				                    </div>
									<div class="ibox-content active tab-pane">
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label" style="line-height: 30px;">授信金额</label>
			                                <div class="col-xs-8">
				                               <input type="text" id="authMax" class="form-control" value="${dto.authMax}" validate-rule="required,money,maxLength[8]" validate-msg="money:最多8位数字且只能为数字,maxlength:" style="margin-bottom: 20px;" placeholder="请填写授信金额">
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label" style="line-height: 30px;">出账日期</label>
			                                <div class="col-xs-8">
				                               <select name="ooaDate" id="ooaDate">
													<c:forEach begin="3" end="27" var="i">
													<option value="${i}" <c:if test="${dto.ooaDate == i}">selected</c:if>>每月${i}号</option>
													</c:forEach>
												</select>
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div>
											<label class="col-xs-4 control-label" style="line-height: 30px;">还款日期</label>
			                                <div class="col-xs-8">
												<select name="repayDate" id="repayDate" validate-rule="largeTo[#ooaDate]" validate-msg="largeTo:还款日必须大于出账日">
													<c:forEach begin="3" end="27" var="i">
													<option value="${i}" <c:if test="${dto.repayDate == i}">selected</c:if>>每月${i}号</option>
													</c:forEach>
												</select>
			                                </div>
										</div>
										<div class="hr-line-dashed" style="clear:both"></div>
										<div class="form-group">
											<div class="col-sm-4 col-sm-offset-2" style="text-align: center">
												<input class="btn btn-primary submit-save gr-grant" type="button" value=" 授 信 " />
											</div>
										</div>
									</div>
								</c:when>
								<c:otherwise>
								<div class="ibox-title">
			                        <h5>基本信息</h5>
			                    </div>
								<div class="ibox-content active tab-pane">
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">商户id</label>
		                                <div class="col-xs-8">
			                                ${dto.mnum}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">手机号码</label>
		                                <div class="col-xs-8">
			                                ${dto.mobile}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">商户名称</label>
		                                <div class="col-xs-8">
			                                ${dto.name}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">商户简称</label>
		                                <div class="col-xs-8">
			                                ${dto.simpleName}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">法人名称</label>
		                                <div class="col-xs-8">
			                                ${dto.legalName}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">法人身份照</label>
		                                <div class="col-xs-8">
			                                ${dto.idCard}
		                                </div>
									</div>
 									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">所在地区</label>
		                                <div class="col-xs-8">
			                                ${dto.displayName}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label">详细地址</label>
		                                <div class="col-xs-8">
			                                ${dto.address}
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
								</div>
								<div style="height:20px;"></div>
								<div class="ibox-title">
			                        <h5>授信信息</h5>
			                    </div>
								<div class="ibox-content active tab-pane">
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label" style="line-height: 30px;">授信上限</label>
		                                <div class="col-xs-8">
			                               <select name="authMaxFlag" id="authMaxFlag">
			                               		<option value="true" <c:if test="${dto.authMaxFlag}">selected</c:if>>开启</option>
			                               		<option value="false" <c:if test="${!dto.authMaxFlag}">selected</c:if>>关闭</option>
			                               </select>
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div>
										<label class="col-xs-4 control-label" style="line-height: 30px;">授信金额</label>
		                                <div class="col-xs-8">
			                               <input type="text" id="authMax" class="form-control" value="${dto.authMax}" validate-rule="required,money" style="margin-bottom: 20px;" placeholder="请填写授信金额">
		                                </div>
									</div>
									<div class="hr-line-dashed" style="clear:both"></div>
									<div class="form-group">
										<div class="col-sm-4 col-sm-offset-2" style="text-align: center">
											<input class="btn btn-primary submit-save qy-grant" type="button" value=" 授 信 " />
										</div>
									</div>
								</div>
								</c:otherwise>
							</c:choose>
						</form>
					</div>
				</div>
			</div>
			<!-- 全局js -->
			<script src="<%=path %>/js/jquery.min.js?v=2.1.4">
			</script>
			<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6">
			</script>
			<!-- 自定义js -->
			<script src="<%=path %>/js/content.js?v=1.0.0">
			</script>
			<script type="text/javascript" src="<%=path%>/js/common.js">
			</script>
			<script src="<%=path %>/js/plugins/layer/layer.min.js">
			</script>
			<!-- iCheck -->
			<script src="<%=path %>/js/plugins/iCheck/icheck.min.js">
			</script>
			<script src="<%=path %>/js/plugins/webuploader/webuploader.js">
			</script>
			<script>
			 $(document).ready(function () {

				$("#ooaDate").select();
				$("#repayDate").select();
				 $.ajax({
					url:context+"/js/bank/bank_info.json",
				    type:'POST', //GET
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    async:false,
				    success:function(data){
				    	var $bank = $("#bank-sn");
				    	for(var i = 0 ; i < data.length ; i++){
				    		if(data[i].sn == $bank.attr("data")){
				    			$bank.html(data[i].name);
				    			break;
				    		}
				    	}
				    }
				});
				 
				 
				$("#authMaxFlag").select();
				$("#authMaxFlag").change(function(){
					if($(this).val() == "true"){
						$("#authMax").removeAttr("disabled");
					}else{
						$("#authMax").attr("disabled",true);
					}
				});
				$("#authMaxFlag").trigger("change");
				var index;
				$(".qy-grant").click(function(){
					if(!$("#_f").validate()) return;
	    			var $this = $(this);
	    			$this.html(" 处 理 中 ");
	    			$this.attr("disabled", true);
	    			index = layer.load(2, {time: 10*1000});
					var flag = $("#authMaxFlag").val();
					var authMax = $("#authMax").val();
					$.post("<%=path%>/grant/grant.htm",{authMaxFlag:flag,authMax:authMax,type:2,id:"${dto.id}"},function(obj){
    					layer.close(index);
						window.parent.layer.alert(obj.resultMsg==null?"系统异常":obj.resultMsg);
    					if (obj.resultCode == "-1") {
    						$this.html(" 授 信 ");
    						$this.attr("disabled", false);
    					}else{
    						window.parent.closeGrant();
    					}
					},"json");
				});
				$(".gr-grant").click(function(){
					if(!$("#_f").validate()) return;
	    			var $this = $(this);
	    			$this.html(" 处 理 中 ");
	    			$this.attr("disabled", true);
					var authMax = $("#authMax").val();
					var ooaDate = $("#ooaDate").val();
					var repayDate = $("#repayDate").val();
	    			index = layer.load(2, {time: 10*1000});
					$.post("<%=path%>/grant/grant.htm",{repayDate:repayDate,ooaDate:ooaDate,authMax:authMax,type:1,id:"${dto.id}"},function(obj){
						layer.close(index);
						window.parent.layer.alert(obj.resultMsg==null?"系统异常":obj.resultMsg);
    					if (obj.resultCode == "-1") {
    						$this.html(" 授 信 ");
    						$this.removeAttr("disabled");
    					}else{
    						window.parent.closeGrant();
    					}
					},"json");
				});
				
			 });
			</script>
	</body>

</html>