<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>授信管理</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">申请时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6">
											<input type="text" class="form-control" id="startDate" name="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="endDate" name="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户名称</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="name" name="name" maxlength="30" placeholder="请填写用户名称">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户手机</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="mobile" name="mobile" maxlength="30" placeholder="请填写用户手机">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户id</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="num" name="num" maxlength="30" placeholder="请填写用户id">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">授信状态</label>
									<div class="col-sm-9">
										<select id="status" name="status">
											<option value="">全部</option>
											<option value="true">已授信</option>
											<option value="false">待授信</option>
										</select>
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	var $pager;
	$(document).ready(function(){
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#status").select();
		$pager = $("#data").pager({
			url:"${hsj}/grant/grantInfoManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>用户id</th>'+
							'<th>用户名称</th>'+
							'<th>手机号码</th>'+
							'<th>用户类型</th>'+
							'<th>状态</th>'+
							'<th>申请时间</th>'+
							'<th>授信金额</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{num}</td>'+
							'<td>{name}</td>'+
							'<td>{mobile}</td>'+
							'<td id="type_{id}"></td>'+
							'<td id="status_{id}">{status}</td>'+
							'<td>{applyDateStr}</td>'+
							'<td id="flag_{id}"></td>'+
							'<td id="opera_{id}"></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="8"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].type == 1)
						$("#type_"+list[i].id).html("用户");
					else
						$("#type_"+list[i].id).html("商户");

					if(list[i].status == "true" || list[i].status)
						$("#status_"+list[i].id).html("已授信");
					else
						$("#status_"+list[i].id).html("待授信");	
					
					if(list[i].type != 1){
						if(list[i].flag == "true" || list[i].flag)
							$("#flag_"+list[i].id).html(list[i].money);
						else
							$("#flag_"+list[i].id).html("无上限");
					}else{
						$("#flag_"+list[i].id).html(list[i].money);
					}
					var $btn = null;
					if(list[i].status == "true" || list[i].status){
						$btn = $('<a href="javascript:void(0);" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> 查看 </a>');
					}else{
						$btn = $('<a href="javascript:void(0);" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> 授信 </a>');
					}
					$btn.attr("data",JSON.stringify(list[i]));
					$btn.click(function(){
						var d = JSON.parse($(this).attr("data"));
						console.log(d);
						//自定页
						var height = $(window).height();
						var width = $(window).width();
						l = layer.open({
						  type: 2,
						  title:"授信",
						  skin: 'layui-layer-demo', //样式类名
						  closeBtn: 1, //不显示关闭按钮
						  offset:['0px',(width-400)+'px'],
						  area:['400px',height+'px'],
						  anim: 2,
						  shadeClose: false, //开启遮罩关闭
						  content: '<%=path%>/grant/load.htm?type='+d.type+'&id='+d.id
						});
						
					});
					$("#opera_"+list[i].id).append($btn);
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});

	var l;
	function closeGrant(){
		layer.close(l);
		$pager.gotoPage($pager.pageNumber);
	}
	
</script>

	</body>
</html>
