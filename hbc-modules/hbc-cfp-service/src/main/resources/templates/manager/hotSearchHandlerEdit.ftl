
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

	<#include "common/common.ftl">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>商家分类编辑</h5>
                    <div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
                        <div>
                            <input type="button" class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
                            <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="_f" method="post" class="form-horizontal">
                        <input type="hidden" id="id" name="id" value="${entity.id}"/>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">名称</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" validate-msg="required:请输入名称" id="name" name="name" value="${entity.name}" maxlength="30" placeholder="请填写热门搜索名称">
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">热门搜索类型</label>
                                <div class="col-sm-8">
                                    <select name="type" id="type" value="">
                                        <option value="0" selected>商户信息</option>
                                        <option value="1" >商品信息</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">选择资源</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="rid" value="${entity.rid!}" id="${entity.rid!}"/>
                                    <span id="rname">${entity.mname!}</span>
                                    <input type="button" class="btn btn-success submit-save" id="choose" value="选择资源" />
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">排序号</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"  validate-msg="required:请输入排序号,number:排序号必须为数字" id="orderList" name="orderList" value="${entity.orderList!}" maxlength="30" placeholder="请填写排序号">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<script>
    $(document).ready(function () {
        $("#type").select();
        $("#cancel").click(function(){parent.closeLayer();});
        $("#submit-save").click(function (){
            if(!$("#_f").validate()) return;
            var $this = $(this);
            $this.html("保 存 中");
            $this.attr("disabled", true);
            var index = layer.load(2, {time: 10*1000});
            $.ajax({
                url:'${basePath!}/opera/hotSearch/hotSearchEditAjaxSubmit',
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:$("#_f").serialize(),
                success: function(data){ //成功
                    layer.close(index);
                    var obj = data;
                    //消息对话框
                    parent.messageModel(data.msg);
                    parent.c.gotoPage(null);
                    parent.closeLayer();
                }
            });
        });

        $("#choose").click(function(){
            var t = $("#type").val();
            var uri = "";
            if(t == 0){
                uri = "${basePath!}/opera/hotSearch/merchantInfoManager";
            }else{
                uri = "${basePath!}/opera/hotSearch/goodsInfoManager";
            }
            parent.openLayerWithBtn('分配角色',uri,'900px','600px', null, ['选好了','不选了'], function(index, layero){
                var body = parent.layer.getChildFrame('body', index);
                var data = body.find('.fa-check').parent().attr('data');
                $("#rid").val(data.split(',')[0]);
                $("#rname").html(data.split(',')[1]);
                parent.layer.close(index);
            });
        });
    });
</script>
</body>

</html>
