<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 主页示例</title>

    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="<%=path %>/css/animate.css" rel="stylesheet">
    <link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row row-sm">
                            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content yellow-bg">
				                        <h5>总成交额</h5>
				                        <h1 class="no-margins" id="zcje">¥ 0</h1>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox">
				                    <div class="ibox-content bg-info">
				                        <h5>总订单量</h5>
				                        <h1 class="no-margins" id="zddl">0</h1>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content lazur-bg">
				                        <h5>总成交订单</h5>
				                        <h1 class="no-margins" id="zcjdd">0</h1>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content red-bg">
				                        <h5>总订单转化率</h5>
				                        <h1 class="no-margins" id="zddzhl">0%</h1>
				                    </div>
				                </div>
				            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row row-sm">
                            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content lazur-bg">
				                        <h5>今日成交额</h5>
				                        <h1 class="no-margins" id="jrcje">¥ 0</h1>
				                        <div class="stat-percent font-bold text-primary" id="bjcje">0% <i class="fa fa-level-up"></i></div>
				                        <small id="zrcje">较昨日¥ 0</small>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox">
				                    <div class="ibox-content red-bg">
				                        <h5>今日订单量</h5>
				                        <h1 class="no-margins" id="jrddl">0</h1>
				                        <div class="stat-percent font-bold text-primary" id="bjddl">0% <i class="fa fa-level-up"></i></div>
				                        <small id="zrddl">较昨日 0</small>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content yellow-bg">
				                        <h5>今日成交订单</h5>
				                        <h1 class="no-margins" id="jrcjdd">0</h1>
				                        <div class="stat-percent font-bold text-primary" id="bjcjdd">0% <i class="fa fa-level-up"></i></div>
				                        <small id="zrcjdd">较昨日0</small>
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-3">
				                <div class="ibox ">
				                    <div class="ibox-content bg-info">
				                        <h5>今日订单转化率</h5>
				                        <h1 class="no-margins" id="jrddzhl">0%</h1>
				                        <div class="stat-percent font-bold text-primary" id="bjzhl">0% <i class="fa fa-level-up"></i></div>
				                        <small id="zrddzhl">较昨日0%</small>
				                    </div>
				                </div>
				            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-12">
                      <div id="flot-chart" style="height:300px;"></div>
                   </div>
                </div>
                <div class="row">
                	<div class="col-sm-12">
                      <div id="flot-chart-order" style="height:300px;"></div>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 全局js -->
    <script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
    <!-- Flot -->
    <script src="<%=path %>/js/plugins/flot/jquery.flot.js"></script>
    <script src="<%=path %>/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<%=path %>/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<%=path %>/js/plugins/flot/jquery.flot.pie.js"></script>
    <!-- 自定义js -->
    <script src="<%=path %>/js/content.js"></script>
    <script src="<%=path %>/js/plugins/echarts/echarts.min.js"></script>
	<script>
	// 第二个参数可以指定前面引入的主题
        $(function() {
        	
        	
        	$.post("<%=path%>/index/orderData.htm",{},function(d){
        		var data = d.data;
        		$("#zcje").html("¥ "+formatNum(data.totalOrderAmount+""));
        		$("#zddl").html(data.totalOrderCount);
        		$("#zcjdd").html(data.totalSuccessOrderCount);
        		$("#zddzhl").html((parseFloat(data.totalSuccessOrderCount * 100)/parseFloat(data.totalOrderCount)).toFixed(2)+"%");
        		$("#jrcje").html("¥ "+formatNum(data.todayOrderAmount+""));
        		$("#jrddl").html(data.todayOrderCount);
        		$("#jrcjdd").html(data.todaySuccessOrderCount);

        		var jrzhl = data.todayOrderCount == 0?0:((parseFloat(data.todaySuccessOrderCount * 100)/parseFloat(data.todayOrderCount)).toFixed(2));
        		$("#jrddzhl").html(jrzhl+"%");
        		$("#zrcje").html("较昨日：¥ "+formatNum(data.yestedayOrderAmount+""));
        		$("#zrddl").html("较昨日："+data.yestedayOrderCount);
        		$("#zrcjdd").html("较昨日："+data.yestedaySuccessOrderCount);

        		var zrzhl = data.yestedayOrderCount == 0?0:((parseFloat(data.yestedaySuccessOrderCount * 100)/parseFloat(data.yestedayOrderCount)).toFixed(2));
        		$("#zrddzhl").html("较昨日："+zrzhl+"%");
        		var bjcje = data.yestedayOrderAmount == 0 ? 0 : ((data.todayOrderAmount - data.yestedayOrderAmount) * 100 / data.yestedayOrderAmount).toFixed(2);
        		var bjddl = data.yestedayOrderCount == 0 ? 0 : ((data.todayOrderCount - data.yestedayOrderCount) * 100 / data.yestedayOrderCount).toFixed(2);
        		var bjcjdd = data.yestedaySuccessOrderCount == 0 ? 0 : ((data.todaySuccessOrderCount - data.yestedaySuccessOrderCount) * 100 / data.yestedaySuccessOrderCount).toFixed(2);
        		var bjzhl = zrzhl == 0 ? 0 : (((jrzhl - zrzhl) * 100) / zrzhl).toFixed(2);

        		$("#bjcje").html((bjcje+"").replace("-","") +"% "+ (bjcje > 0?'<i class="fa fa-level-up"></i>':'<i class="fa fa-level-down"></i>'));
        		$("#bjddl").html((bjddl+"").replace("-","") +"% "+ (bjddl > 0?'<i class="fa fa-level-up"></i>':'<i class="fa fa-level-down"></i>'));
        		$("#bjcjdd").html((bjcjdd+"").replace("-","") +"% "+ (bjcjdd > 0?'<i class="fa fa-level-up"></i>':'<i class="fa fa-level-down"></i>'));
        		$("#bjzhl").html((bjzhl+"").replace("-","") +"% "+ (bjzhl > 0?'<i class="fa fa-level-up"></i>':'<i class="fa fa-level-down"></i>'));
        		
        		
        	},"json");
        	
        	function formatNum(str){
        		var newStr = "";
        		var count = 0;

        		if(str.indexOf(".")==-1){
        		   for(var i=str.length-1;i>=0;i--){
        		 if(count % 3 == 0 && count != 0){
        		   newStr = str.charAt(i) + "," + newStr;
        		 }else{
        		   newStr = str.charAt(i) + newStr;
        		 }
        		 count++;
        		   }
        		   str = newStr + ".00"; //自动补小数点后两位
        		   console.log(str)
        		}
        		else
        		{
        		   for(var i = str.indexOf(".")-1;i>=0;i--){
        		 if(count % 3 == 0 && count != 0){
        		   newStr = str.charAt(i) + "," + newStr;
        		 }else{
        		   newStr = str.charAt(i) + newStr; //逐个字符相接起来
        		 }
        		 count++;
        		   }
        		   str = newStr + (str + "00").substr((str + "00").indexOf("."),3);
        		 }
        		return str;
			}
        	
        	$.post("<%=path%>/index/diagramData.htm",{},function(d){
        		var data = d.data;
        		var chart = echarts.init(document.getElementById('flot-chart'));
            	var chart_order = echarts.init(document.getElementById('flot-chart-order'));
            	
            	option = {
           		    title: {
           		        text: '今日交易额'
           		    },
           		    tooltip: {
           		        trigger: 'axis'
           		    },
           		    grid: {
           		        left: '3%',
           		        right: '4%',
           		        bottom: '3%',
           		        containLabel: true
           		    },
           		    legend: {
           		        data:['交易额']
           		    },
           		    toolbox: {
           		        show: true,
           		        feature: {
           		            dataZoom: {
           		                yAxisIndex: 'none'
           		            },
           		            dataView: {readOnly: false},
           		            magicType: {type: ['line', 'bar']},
           		            restore: {},
           		            saveAsImage: {}
           		        }
           		    },
           		    xAxis:  {
           		        type: 'category',
           		        boundaryGap: false,
           		        data: data.time
           		    },
           		    yAxis: {
          		    		axisLabel: {
          		                formatter: function (val) {
          		                    return '¥ '+val;
          		                }
          		            },
           		        type: 'value'
           		    },
           		    series: [
           		        {
           		            name:'交易额',
           		            type:'line',
           		            data:data.amount
           		        }
           		    ]
           		};
            	chart.setOption(option);
            	option = {
          		    title: {
          		        text: '今日订单量'
          		    },
          		    tooltip: {
          		        trigger: 'axis'
          		    },
          		    grid: {
          		        left: '3%',
          		        right: '4%',
          		        bottom: '3%',
          		        containLabel: true
          		    },
          		    legend: {
          		        data:['订单量','成交量']
          		    },
          		    toolbox: {
          		        show: true,
          		        feature: {
          		            dataZoom: {
          		                yAxisIndex: 'none'
          		            },
          		            dataView: {readOnly: false},
          		            magicType: {type: ['line', 'bar']},
          		            restore: {},
          		            saveAsImage: {}
          		        }
          		    },
          		    xAxis:  {
          		        type: 'category',
          		        boundaryGap: false,
          		        data: data.time
          		    },
          		    yAxis: {
          		        type: 'value'
          		    },
          		    series: [
          		        {
          		            name:'订单量',
          		            type:'line',
          		            data:data.orderCount
          		        },
          		        {
          		            name:'成交量',
          		            type:'line',
          		            data:data.successOrderCount
          		        }
          		    ]
          		};
            	chart_order.setOption(option);
        	},"json");
        	
//         	var websocket;
//             if ('WebSocket' in window) {
//             	console.log("WebSocket");
//                 websocket = new WebSocket("ws://localhost:7070/takeout-manager/echo.ws");
//             } else if ('MozWebSocket' in window) {
//             	console.log("MozWebSocket");
//                 websocket = new MozWebSocket("ws://echo.ws");
//             } else {
//             	console.log("SockJS");
//                 websocket = new SockJS("http://localhost:7070/takeout-manager/sockjs/echo.ws");
//             }
//             websocket.onopen = function (evnt) {
//                 console.log("链接服务器成功!");
//             };
//             websocket.onmessage = function (evnt) {
//             	console.log("消息来了!");
//             	console.log(evnt);
//             };
//             websocket.onerror = function (evnt) {
//             };
//             websocket.onclose = function (evnt) {
//             	console.log("与服务器断开了链接!");
//             }
//             $('#send').bind('click', function() {
//                 send();
//             });
//             function send(){
//                 if (websocket != null) {
//                     var message = document.getElementById('message').value;
//                     websocket.send(message);
//                 } else {
//                     alert('未与服务器链接.');
//                 }
//             }
        	
		});
    </script>
</body>

</html>
