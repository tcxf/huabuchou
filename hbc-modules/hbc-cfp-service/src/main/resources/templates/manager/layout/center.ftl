<html>
<meta charset="UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="shortcut icon" href="favicon.ico">
<link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<#--<link href="<${basePath!}/css/animate.css" rel="stylesheet">-->
<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
<link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="${basePath!}/css/animate.css" rel="stylesheet">
<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
<style>
    .tab{
        padding:10px;
    }
    .activity{
        border-bottom: 2px solid #33CCFF
    }
    .bg-warning {
        color: #f4f3f9;
        background-color: #FF9900;
    }
    .bg-pink {
        color: #f4f3f9;
        background-color: Fuchsia;
    }
    .bg-white {
        color: black;
        background-color: #cccccc;
    }
    @media only screen and (min-width:550px ) {
        ._font{
            font-size:20px;
        }
    }
</style>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox-content row">
                <div class="col-sm-12">
                    <div class="col-sm-6 text-center" id="sta">
                        <div id="tab-div" style="font-size:14px;font-weight:bold;">
                            <a href="javascript:void(0)" style="color:black"><div data="1" class="col-sm-2 tab activity" id="today">今日新增</div></a>
                            <a href="javascript:void(0)" style="color:black"><div data="2" class="col-sm-2 tab">昨日新增</div></a>
                            <a href="javascript:void(0)" style="color:black"><div data="3" class="col-sm-2 tab">本周新增</div></a>
                            <a href="javascript:void(0)" style="color:black"><div data="4" class="col-sm-2 tab">本月新增</div></a>
                        </div>
                        <div class="row row-sm text-center">
                            <div class="col-sm-12" style="margin-top:20px;">
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-error">
                                        <div class="h1 text-fff h1 _font num" id="s_1">0</div>
                                        <span class="text-fff _font">消费者</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-success">
                                        <div class="h1 text-fff h1 _font num" id="s_2">0</div>
                                        <span class="text-fff _font">商户</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-info">
                                        <div class="h1 text-fff h1 _font" id="s_3">¥ 0</div>
                                        <span class="text-fff _font">交易</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-warning">
                                        <div class="h1 text-fff h1 _font" id="s_4">¥ 0</div>
                                        <span class="text-fff _font">待还款</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-warning">
                                        <div class="h1 text-fff h1 _font" id="s_5">¥ 0</div>
                                        <span class="text-fff _font">已还款</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-pink">
                                        <div class="h1 text-fff h1 _font" id="s_6">¥ 0</div>
                                        <span class="text-fff _font">已结算</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel padder-v item bg-pink">
                                        <div class="h1 text-fff h1 _font" id="s_7">¥ 0</div>
                                        <span class="text-fff _font">待结算</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row row-sm text-center">
                            <div id="flot-chart" style="width:100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12" style="margin-top:50px;">
                    <div style="font-size:14px;font-weight:bold;">
                        <div class="col-sm-12 tab">数据总和</div>
                    </div>
                    <div class="row row-sm text-center">
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-pink">
                                <div class="h1 text-fff h1 _font" id="ss_4">¥ 0</div>
                                <span class="text-fff _font">交易金额</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-warning">
                                <div class="h1 text-fff h1 _font" id="ss_5">¥ 0</div>
                                <span class="text-fff _font">已还款</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-info">
                                <div class="h1 text-fff h1 _font" id="ss_7">¥ 0</div>
                                <span class="text-fff _font">待还款</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-primary">
                                <div class="h1 text-fff h1 _font" id="ss_8">¥ 0</div>
                                <span class="text-fff _font">逾期</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-success">
                                <div class="h1 text-fff h1 _font" id="ss_6">¥ 0</div>
                                <span class="text-fff _font">已结算</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel padder-v item bg-error">
                                <div class="h1 text-fff h1 _font" id="ss_9">¥ 0</div>
                                <span class="text-fff _font">待结算</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript" src="${basePath!}/js/plugins/echarts/echarts-all.js"></script>
<!-- Flot -->
<!--flotdemo-->
<script type="text/javascript">
    $(document).ready(function(){
        startmethod(1);//初始化

        $("#flot-chart").height($("#sta").height());
        var chart = echarts.init(document.getElementById('flot-chart'));

        $("#today").trigger("click");

        $("#tab-div").find("div").click(function(){
            $("#tab-div").find("div").removeClass("activity");
            $(this).addClass("activity");
            var  dateTime = $(this).attr("data");
            startmethod(dateTime);
        });

        function startmethod(dateTime){
            $.ajax({
                url: '${basePath!}/opera/o_index/info',
                data: {"dateTime":dateTime},
                type: 'get',
                success: function (data) {
                    console.log(data.data)
                    initsevenData(data.data)
                    initliData(data.data)
                    inittotalData(data.data)
                },
            });
        }

        // JSON.stringify(arr)
        function initsevenData(data) {
            option = {
                title: {
                    text: '近七日交易额'
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    containLabel: true
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                legend: {
                    data:['交易额']
                },
                xAxis:  {
                    type: 'category',
                    boundaryGap: false,
                    data: data.dTimeArr
                },
                yAxis: {
                    axisLabel: {
                        formatter: function (val) {
                            return '¥ '+val;
                        }
                    },
                    type: 'value'
                },
                series: [
                    {
                        name:'交易额',
                        type:'line',
                        data: data.tMoneyArr
                    }
                ]
            };
            chart.setOption(option);

        }

        //加载顶部标签商的值
        function initliData(data) {
            $('#s_1').html(data.consumerNum)
            $('#s_2').html(data.merchantNum)
            $('#s_3').html(data.tradeMoney)
            $('#s_4').html(data.waitPayplan)//待还款
            $('#s_5').html(data.payBackMoney)
            $('#s_6').html(data.SettledMoney)
            $('#s_7').html(data.pWaitSettledByDateTime)//待结算
        }


        //加载数据总和
        function inittotalData(data) {
            $('#ss_4').html(data.pTradeMoney)//交易金额
            $('#ss_5').html(data.pPayback)//已还款
            $('#ss_6').html(data.pAlreadySettled)//已结算
            $('#ss_7').html(data.waitPay)//待还
            $('#ss_8').html(data.overDue)//逾期
            $('#ss_9').html(data.pWaitSettled)//待结算
        }

    });
</script>
</body>

</html>