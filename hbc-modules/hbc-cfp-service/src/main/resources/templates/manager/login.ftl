<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>- 登录</title>
<meta name="keywords" content="">
<meta name="description" content="">

<link rel="shortcut icon" href="favicon.ico">

	<#include "common/common.ftl">

<script>if(window.top !== window.self){ window.top.location = window.location;}</script>
<style>
	  body{
			background:url(${basePath!}/img/yunyinshang_background.png) no-repeat;
			background-size: cover;
			background-position: center center;
			background-attachment: fixed;
			height:500px;
		  }
		  .a-content{
			text-align: center;
		  	margin-top:10%;
			  margin-left: 36%;
		  }
		  .t-logo{
              margin-top:2%;
			  text-align: center;
		  }
		 .t-logo>img{
             width:80px;
             height:40px;
		 }
		.row{
				text-align:center;
			}
			.col-sm-3{
				padding-right:0
			}
			.col-sm-5{
				padding-left:0
			}
		 .t-form{
		    padding:10px;
		 	width:80%;
		 	height:337px;
		 	background:#fff;
		 	position:relative;
		 }
		 h4{
		 	font-size:26px;
		 	color:#5c9dd3;
		 	margin-bottom:24px;
		 }
		 .t-form .img1{
			position: absolute;
			top:125px;
			left:55px
		}
		.t-form .img2{
			position: absolute;
			top:170px;
			left:55px
		}
		 .t-btn{
			width:80%;
			height:40px;
			line-height:40px;
			background-color:#5c9dd3;
			color:#fff;
			text-align:center;
			font-size:16px;
			margin-top:25px;
			margin-left: 11%;
		 }
</style>
</head>

<body>

	<div class="a-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<div class="t-form">
						<form class="m-t" role="form">
                            <div class="t-logo">
                                <img class="img" src="${basePath!}/img/imglogo.png">
                            </div>
						<h4>花不愁运营商管理系统</h4>
						<div class="form-group">
							 <div style="line-height: 30px; width: 11%;">
				                <img class="img1" src="${basePath!}/img/name.png" style="width: 20px;height: 20px">
				            </div>
				            <div class="wkt-flex">
								<input type="text" id="mobile" class="form-control" style="padding-left:25px;width: 80%;margin-left: 40px"
									placeholder="用户名">
							</div>
						</div>
						<div class="form-group">
							<div style="line-height: 30px; width: 11%;">
			                   <img class="img2" src="${basePath!}/img/mima.png" style="width: 20px;height: 20px">
			                </div>
			                 <div class="wkt-flex">
								<input type="password" id="pwd" class="form-control" style="padding-left:25px;width: 80%;margin-left: 40px"
									placeholder="密码">
							</div>
						</div>
						<div class="t-btn" id="loginButton">登 录</div>
			           </form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>

</body>


<script type="text/javascript">

	$(function(){
		$("#loginButton").click(function(){
			if(validLogin() == true)
			{
				loginAjaxSubmit();
			}
		});
	});

	function validLogin(){
		if($("#mobile").val() == "")
		{
			layer.msg('请填写登录名');
			return false;
		}
		if($("#pwd").val() == "")
		{
			layer.msg('请填写密码');
			return false;
		}
		return true;
	}

	$(document).keydown(function(event){
		if(event.keyCode == 13)
		{ //绑定回车
			if(validLogin() == true)
			{
				loginAjaxSubmit();
			}
			console.log(2);
		}
	});

	function loginAjaxSubmit(){
		$.ajax({
		    cache: false,
		    type: "POST",
		    url:"${basePath!}/opera/opera/login",
		    data: {mobile:$("#mobile").val(),pwd:$("#pwd").val()},
		    error: function(request) {
				layer.msg('登陆发生错误');
		    },
		    success: function(results) {
                var resultJson = results;
                var resultCode = resultJson.code;
                var resultMsg = resultJson.msg;
                if(resultCode == "1")
		    	{
		    	    if(resultJson.data!=null&&resultJson.data=="true"){
						var pwd = $("#pwd").val();
						window.location.href = "${basePath!}/opera/o_index/goToUpdatePassWordPage?type=1&pwd="+pwd;
				}else{
                    window.location.href = "${basePath!}/opera/oPage/index";
                    }
				} else {
					layer.msg(resultMsg);
				}
			}
		});
	}

	function validRestPwd() {
		if ($("#userNameForRest").val() == "") {
			jNotify("请输入用户名!", {
				ShowOverlay : false,
				MinWidth : 100,
				VerticalPosition : "bottom",
				HorizontalPosition : "right"
			});
			return false;
		}
		if ($("#regEmail").val() == "") {
			jNotify("请输入注册邮箱!", {
				ShowOverlay : false,
				MinWidth : 100,
				VerticalPosition : "bottom",
				HorizontalPosition : "right"
			});
			return false;
		}
		return true;
	}

	function loginRestPwdToggle(toogleFlag) {
		if (toogleFlag == 1) {
			$("#loginDiv").show();
			$("#resetPwdDiv").hide();
		} else {
			$("#resetPwdDiv").show();
			$("#loginDiv").hide();
		}
	}
</script>

</html>
