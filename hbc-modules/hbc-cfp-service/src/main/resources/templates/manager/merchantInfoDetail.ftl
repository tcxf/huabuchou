
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
		<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
					<li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
					<li><a data-toggle="tab" href="#sysx" aria-expanded="true"> 商业授信信息</a></li>
					<#--<li><a data-toggle="tab" href="#jsmx" aria-expanded="true"> 结算明细</a></li>-->
					<#--<li><a data-toggle="tab" href="#grgs" aria-expanded="true"> 个人工商信息</a></li>-->
					<#--<li><a data-toggle="tab" href="#dtjd" aria-expanded="true"> 多头借贷</a></li>-->
					<#--<li><a data-toggle="tab" href="#yhk" aria-expanded="true"> 银行卡</a></li>-->
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" id="mid" value="${entity.id!}"/>
					  	<div class="form-group col-sm-6">
							<label class="col-sm-2 control-label">商户id</label>
							<label class="col-sm-10 control-label" style="text-align: left;">
								${entity.mid!}
							</label>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.name!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行业类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
										${entity.mname!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.legalName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人证件号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.idCard!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-2 control-label">所属地区</label>
                                <div class="col-sm-10" id="area" data-name="area" data-value="${entity.area!}"></div>
                            </div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;"> 
									<#--<c:if test="${id != null && entity.status == 1}">启用</c:if>-->
									<#--<c:if test="${id != null && entity.status == 0}">禁用</c:if>-->
										<#if (entity.status)??>
											<#if entity.status ='1'>
												 启用
											<#else >
												禁用
											</#if>
										</#if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					
					<!-- 商户审核信息 -->
					<div id="examine" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户性质</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.nature!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">品牌经营年限</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operYear!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">经营面积</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.operArea!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">注册资金（万）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.regMoney!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">场地照片</label>
							<img src="${entity.localPhoto!}" style="width:400px;height:400px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">营业执照</label>
							<img src="${entity.licensePic!}" style="width:250px;height:400px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">法人身份证</label>
							<img src="${entity.legalPhoto!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">借记卡照片</label>
							<img src="${entity.normalCard!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">信用卡照片</label>
							<img src="${entity.creditCard!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
					</div>
					<div id="settlement" class="ibox-content tab-pane">
						<#--<div class="hr-line-dashed" style="clear:both"></div>-->
						<#--<div>-->
							<#--&lt;#&ndash;<div class="form-group col-sm-6">&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-4 control-label">授信额度上限</label>&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-8 control-label" style="text-align: left;">&ndash;&gt;-->
									<#--&lt;#&ndash;&lt;#&ndash;<c:if test="${id != null && entity.authMaxFlag}">开启</c:if>&ndash;&gt;&ndash;&gt;-->
									<#--&lt;#&ndash;&lt;#&ndash;<c:if test="${id != null && !entity.authMaxFlag}">关闭</c:if>if&ndash;&gt;&ndash;&gt;-->
									<#--&lt;#&ndash;${entity.authMaxFlag!}&ndash;&gt;-->

								<#--&lt;#&ndash;</label>&ndash;&gt;-->
							<#--&lt;#&ndash;</div>&ndash;&gt;-->
							<#--&lt;#&ndash;&ndash;&gt;-->
							<#--&lt;#&ndash;<div class="form-group col-sm-6">&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-4 control-label">额度上限</label>&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-8 control-label" style="text-align: left;">&ndash;&gt;-->
									<#--&lt;#&ndash;${entity.authMax!}&ndash;&gt;-->
								<#--&lt;#&ndash;</label>&ndash;&gt;-->
							<#--&lt;#&ndash;</div>&ndash;&gt;-->
						<#--</div>-->
						<#--<div class="hr-line-dashed" style="clear:both"></div>-->
						<#--<div>-->
							<#--&lt;#&ndash;<div class="form-group col-sm-6">&ndash;&gt;-->
								<#--&lt;#&ndash;&lt;#&ndash;<label class="col-sm-4 control-label">提现手续费开关</label>&ndash;&gt;&ndash;&gt;-->
								<#--&lt;#&ndash;&lt;#&ndash;<label class="col-sm-8 control-label" style="text-align: left;">&ndash;&gt;&ndash;&gt;-->
									<#--&lt;#&ndash;&lt;#&ndash;&lt;#&ndash;<c:if test="${id != null && entity.withdrawFeeFlag}">开启</c:if>&ndash;&gt;&ndash;&gt;&ndash;&gt;-->
									<#--&lt;#&ndash;&lt;#&ndash;&lt;#&ndash;<c:if test="${id != null && !entity.withdrawFeeFlag}">关闭</c:if>&ndash;&gt;&ndash;&gt;&ndash;&gt;-->
								<#--&lt;#&ndash;&lt;#&ndash;</label>&ndash;&gt;&ndash;&gt;-->
							<#--&lt;#&ndash;</div>&ndash;&gt;-->
							<#---->
							<#--&lt;#&ndash;<div class="form-group col-sm-6">&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-4 control-label">提现手续费率（%）</label>&ndash;&gt;-->
								<#--&lt;#&ndash;<label class="col-sm-8 control-label" style="text-align: left;">&ndash;&gt;-->
									<#--&lt;#&ndash;${entity.withdrawFee!}&ndash;&gt;-->
								<#--&lt;#&ndash;</label>&ndash;&gt;-->
							<#--&lt;#&ndash;</div>&ndash;&gt;-->
						<#--</div>-->
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<#--<div class="form-group col-sm-6">-->
								<#--<label class="col-sm-4 control-label">是否开启让利</label>-->
								<#--<label class="col-sm-8 control-label" style="text-align: left;">-->
									<#--&lt;#&ndash;<c:if test="${id != null && entity.shareFlag}">开启</c:if>&ndash;&gt;-->
									<#--&lt;#&ndash;<c:if test="${id != null && !entity.shareFlag}">关闭</c:if>&ndash;&gt;-->
										<#--<#if  (entity.shareFlag)?? >-->
											<#--<#if  entity.shareFlag =1>-->
											<#--开启-->
										<#--<#else>-->
											<#--关闭-->
											<#--</#if>-->
										<#--</#if>-->

								<#--</label>-->
							<#--</div>-->

							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">让利利率（%）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.shareFee!}
								</label>
							</div>
						</div>
						
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">结算方式</label>
								<label class="col-sm-8 control-label">
										<#if (entity.settlementLoop)??>
										<#if  entity.settlementLoop = '1'>
												日结
										<#elseif entity.settlementLoop = '2'>
												周结
										<#elseif entity.settlementLoop = '3'>
												月结
										<#elseif  entity.settlementLoop = '4'>
												季结
										<#elseif  entity.settlementLoop = '5'>
												半年
										<#elseif  entity.settlementLoop = '6'>
												一年
										<#elseif entity.settlementLoop = '7'>
												半月
										<#elseif entity.settlementLoop = '8'>
											<div> <label for="">最低结算金额 (元)</label>
                                                <input type="text" id="minmoney" name="minmoney" readonly value="${entity.minmoney!?c}">
                                            </div>
                                                <div> <label for="">最迟结算时间 (天)</label>
                                                    <select name="latestday" id="latestday" value="${entity.latestday!}">
                                                        <option value="0" <#if (((entity.latestday)!'') == '0')>selected="selected"</#if> >请选择</option>
                                                        <option value="15" <#if (((entity.latestday)!'') == '15')>selected="selected"</#if> >15天</option>
                                                        <option value="30" <#if (((entity.latestday)!'') == '30')>selected="selected"</#if> >30天</option>
                                                        <option value="45" <#if (((entity.latestday)!'') == '45')>selected="selected"</#if> >45天</option>
                                                        <option value="60" <#if (((entity.latestday)!'') == '60')>selected="selected"</#if> >60天</option>
                                                        <option value="90" <#if (((entity.latestday)!'') == '90')>selected="selected"</#if> >90天</option>
                                                    </select>
                                                </div>
										</#if>
									</#if>

								</label>
							</div>
							
							<div class="form-group col-sm-6">
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					<div id="bind" class="ibox-content tab-pane">
						<div class="fixed-table-container form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width:10%;min-width:150px;">开户行</th>
										<th style="width:10%;min-width:60px;">开户人</th>
										<th style="width:10%;min-width:80px;">预留手机</th>
										<th style="width:20%;min-width:80px;">身份证号</th>
										<th>卡号</th>
										<th style="width:5%;min-width:120px;">是否主卡</th>
									</tr>
								</thead>
								<tbody id="tbody" style="text-align:center;">
								
								</tbody>
							</table>
						</div>
					</div>
                    <!-- 商业授信信息 -->
                    <div id="sysx" class="ibox-content tab-pane">
                        <div class="hr-line-dashed" style="clear:both"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">商业授信额度</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.bauthmax!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">报名时间</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.bcreatedate!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">审批人</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.approverName!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">审批人手机号码</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.bbmoblie!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">给予授信额度时间</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.bbcreatedate!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">给予授信额度</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.endMoeny!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both"></div>
								<#--</div>-->
								<#--<div>-->
									<#--<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">-->
										<#--<input class="btn btn-success" id="jymx-search" type="button" value=" 查 询 " />-->
										<#--<input class="btn btn-info" id="daochu" type="button" value=" 下 载 报 表 " />-->
									<#--</div>-->
								<#--</div>-->
							<#--</form>-->
							<#--<div class="project-list" id="jymx-list"></div>-->
						<#--</div>-->
					<#--</div>-->
					<#--<div id="jsmx" class="ibox-content tab-pane">-->
						<#--<div class="ibox-content">-->
							<#--<form id="jsmx-form" class="form-horizontal">-->
								<#--<input type="hidden" name="mid" value="${entity.mid!}"/>-->
								<#--<div>-->
									<#--<div class="form-group col-sm-6">-->
										<#--<h3>结算总额：¥ <span id="s">--</span> 元</h3>-->
									<#--</div>-->
									<#--<div class="form-group col-sm-6">-->
										<#--<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>-->
									<#--</div>-->
								<#--</div>-->
								<#--<div>-->
									<#--<div class="form-group col-sm-4">-->
										<#--<label class="col-sm-3 control-label">交易时间</label>-->
										<#--<div class="col-sm-9">-->
											<#--<div class="col-sm-6" style="padding-left:0px;">-->
												<#--<input type="text" class="form-control" name="startDate" id="startDateJs" style="width:100%" maxlength="30" placeholder="">-->
											<#--</div>-->
											<#--<div class="col-sm-6" style="padding-left:0px;">-->
												<#--<input type="text" class="form-control" name="endDate" id="endDateJs" style="width:100%" maxlength="30" placeholder="">-->
											<#--</div>-->
										<#--</div>-->
									<#--</div>-->

									<#--<div class="form-group col-sm-4">-->
										<#--<label class="col-sm-3 control-label">结算状态</label>-->
										<#--<div class="col-sm-9">-->
											<#--<select id="status" name="status">-->
												<#--<option value="">全部</option>-->
												<#--<option value="0">待确认</option>-->
												<#--<option value="1">待结算</option>-->
												<#--<option value="2">已结算</option>-->
											<#--</select>-->
										<#--</div>-->
									<#--</div>-->
								<#--</div>-->
								<#--<div>-->
									<#--<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">-->
										<#--<input class="btn btn-success" id="jsmx-search" type="button" value=" 查 询 " />-->
									<#--</div>-->
								<#--</div>-->
							<#--</form>-->
							<#--<div class="project-list" id="jsmx-list">-->

							<#--</div>-->
						<#--</div>-->
					<#--</div>-->

					<#--<!-- 个人工商信息 &ndash;&gt;-->
					<#--<div id="grgs" class="ibox-content tab-pane">-->
						<#--<div id=m-menu6>-->
							<#--<div class="panel panel-default">-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">法人姓名：<b id="gs_frxm"></b></label>-->
									<#--<label class="col-sm-3 control-label">企业名称：<b id="gs_qymc"></b></label>-->
									<#--<label class="col-sm-3 control-label">注册号：<b id="gs_zch"></b></label>-->
									<#--<label class="col-sm-3 control-label">注册资本：<b id="gs_zczb"></b></label>-->
								<#--</div>-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">企业状态：<b id="gs_qyzt"></b></label>-->
									<#--<label class="col-sm-3 control-label">被执行人姓名：<b id="gs_bzxrxm"></b></label>-->
									<#--<label class="col-sm-3 control-label">被执行人身份证：<b id="gs_bzxrsfz"></b></label>-->
									<#--<label class="col-sm-3 control-label">被执行人案件状态：<b id="gs_bzxrajzt"></b></label>-->
								<#--</div>-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">失信被执行人名称：<b id="gs_sxmc"></b></label>-->
									<#--<label class="col-sm-3 control-label">失信被执行人身份证/组织机构代码：<b id="gs_sxsfz"></b></label>-->
									<#--<label class="col-sm-3 control-label">失信被执行人法人姓名：<b id="gs_sxfr"></b></label>-->
									<#--<label class="col-sm-3 control-label">失信具体情形：<b id="gs_sxqx"></b></label>-->
								<#--</div>-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">行政处罚当事人：<b id="gs_xzcfdsr"></b></label>-->
									<#--<label class="col-sm-3 control-label">行政处罚证件号：<b id="gs_xzcfzjh"></b></label>-->
									<#--<label class="col-sm-5 control-label">行政处罚主要违法事实：<b id="gs_xzcfwfss"></b></label>-->
								<#--</div>-->

								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
                          	<#--</div>-->
						<#--</div>-->
					<#--</div>-->

					<#--<!-- 多头借贷 &ndash;&gt;-->
					<#--<div id="dtjd" class="ibox-content tab-pane">-->
						<#--<div id=m-menu6>-->
							<#--<div class="panel panel-default">-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
                               	<#--<div class="panel-heading">-->
                                  	<#--<h4 class="panel-title" style="text-align: center">-->
                                  	<#--<a href="#cc4" data-toggle="collapse">-->
                                    	<#--&lt;#&ndash;<img src="${basePath!}/img/logo_down.png" style="width:25px;height:22px" alt=""/>&ndash;&gt;-->
                                  	<#--</a>-->
                                  	<#--</h4>-->
                               <#--</div>-->
                               <#--<div class="panel-collapse collapse" id="cc4">-->
                                 	<#--<div class="panel-body">-->
                                   		<#--<table border="1" class="table table-hover table-bordered">-->
                                      		<#--<thead class="thd">-->
                                        		<#--<tr>-->
			                                    	<#--<td>黑名单类型</td>-->
			                                        <#--<td>黑名单事实类型</td>-->
			                                        <#--<td>黑名单事实</td>-->
			                                        <#--<td>事件涉及金额</td>-->
			                                        <#--<td>事件发生日期</td>-->
		                                        <#--</tr>-->
                                      		<#--</thead>-->
                                      		<#--<tbody class="tbd" id="sb_tbd">-->

						                	<#--</tbody>-->
						            	<#--</table>-->
                                	<#--</div>-->
                            	<#--</div>-->
                          	<#--</div>-->
						<#--</div>-->
					<#--</div>-->

					<#--<!-- 银行卡信息 &ndash;&gt;-->
					<#--<div id="yhk" class="ibox-content tab-pane">-->
						<#--<div id=m-menu6>-->
							<#--<div class="panel panel-default">-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">银行简称：<b id="yh_yhjc"></b></label>-->
									<#--<label class="col-sm-3 control-label">银行卡号：<b id="yh_yhkh"></b></label>-->
									<#--<label class="col-sm-3 control-label">银行logo：<img id="yh_logo" src=""></label>-->
									<#--<label class="col-sm-3 control-label">银行名称：<b id="yh_yhqc"></b></label>-->
								<#--</div>-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
								<#--<div class="form-group col-sm-12">-->
									<#--<label class="col-sm-3 control-label">银行网址：<b id="yh_yhwz"></b></label>-->
									<#--<label class="col-sm-3 control-label">卡类型：<b id="yh_klx"></b></label>-->
									<#--<label class="col-sm-3 control-label">银行卡种类：<b id="yh_yhkzl"></b></label>-->
									<#--<label class="col-sm-3 control-label">英文全称：<b id="yh_ywqc"></b></label>-->
								<#--</div>-->
								<#--<div class="hr-line-dashed" style="clear:both;"></div>-->
                          	<#--</div>-->
						<#--</div>-->
					</div>

				</div>
			</div>
		</div>
	</div>
	<script>
        var context= "${basePath!}";
        var context2= "${basePath!}";
        $(document).ready(function () {
            $("#area").area();
            examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            var nodata = false;

            initCard();//加载卡信息

            function initCard(){
                $("#tbody").empty();
                $.post("${basePath!}/opera/merchantInfo/loadBindCardInfo?id=${entity.id!}",function(d){
                    for(var i = 0 ; i < d.data.length ; i++){
                        select = d.data[i].isDefault?"是":"否";
                        $tr = $('<tr style="text-align:left;">'+
                                '<td class="bank-select" input-name="bank-bankSn" input-val="'+d.data[i].bankSn+'"></td>'+
                                '<td>'+d.data[i].name+'</td>'+
                                '<td>'+d.data[i].mobile+'</td>'+
                                '<td>'+d.data[i].idCard+'</td>'+
                                '<td>'+d.data[i].bankCard+'</td>'+
                                '<td>'+select+'</td>'+
                                '</tr>');
                        $("#tbody").append($tr);
                        $tr.find("select").select();
                        $tr.find(".bank-select").initBank();

                    }
                },"json");
            }

            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});
            var uploader = WebUploader.create({
                // 选完文件后，是否自动上传。
                auto: true,
                // swf文件路径
                swf: '${basePath!}/grant-manager/js/plugins/webuploader/Uploader.swf',
                // 文件接收服务端。
                server: '${basePath!}/otherinfo/upload',
                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: '#img',
                // 只允许选择图片文件。
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }
            });

            laydate({
                elem: '#startDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#endDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#startDateJs',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#endDateJs',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            $("#repayType").select();
            $("#tradeType").select();


            //交易明细里列表
            var $pager_jymx = $("#jymx-list").pager({
                url:"${basePath!}/p_merchant/tradingDetailManagerList?oid=${id!}",
                formId:"jymx-form",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>交易单号</th>'+
                    '<th>商户id</th>'+
                    '<th>商户名称</th>'+
                    '<th>行业类型</th>'+
                    '<th>用户id</th>'+
                    '<th>用户名称</th>'+
                    '<th>交易类型</th>'+
                    '<th>交易时间</th>'+
                    '<th>交易金额</th>'+
                    '<th>结算金额</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{mid}</td>'+
                    '<td>{legalName}</td>'+
                    '<td>{nature}</td>'+
                    '<td>{uid}</td>'+
                    '<td>{realName}</td>'+
                    '<td>{tradingType}</td>'+
                    '<td>{tradingDate}</td>'+
                    '<td>{tradeAmount}</td>'+
                    '<td>{actualAmount}</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });
                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });

                    loadJymxAmount();//加载交易总金额
                }
            });

            $("#jymx-search").click(function(){
                $pager_jymx.gotoPage(1);
            })


            $("#daochu").click(function(){
                $("#jymx-form").attr("action","${basePath!}/p_merchant/tradingPoi");
                $("#jymx-form").submit();
            });


            function loadJymxAmount(){
                $.ajax({
                    url:"${basePath!}/p_merchant/totalJymxAmount?oid=${id!}",
                    type:'POST', //GET
                    data:$("#jymx-form").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        $("#totalJymxAmount").html(d.data);
                    }
                });
            }


            //结算明细里列表
            var $pager_jsmx = $("#jsmx-list").pager({
                url:"${basePath!}/p_merchant/clearDetail?mid=${id!}",
                formId:"jsmx-form",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>结算单号</th>'+
                    '<th>商户id</th>'+
                    '<th>商户名称</th>'+
                    '<th>出账时间</th>'+
                    '<th>结算时间</th>'+
                    '<th>结算状态</th>'+
                    '<th>结算金额</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{oid}</td>'+
                    '<td>{legalName}</td>'+
                    '<td>{outSettlementDate}</td>'+
                    '<td>{settlementTime}</td>'+
                    '<td>{status}</td>'+
                    '<td>{amount}</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    var binfo = null;
                    $.ajax({
                      //  url:context+"/js/bank/bank_info.json",
                        type:'POST', //GET
                        timeout:30000,    //超时时间
                        dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        async:false,
                        success:function(data){
                            binfo = data;
                        }
                    });
                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });
                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                    loadJsmxAmount();
                }
            });

            //jymx-search

            $("#jsmx-search").click(function(){
                console.log(11);
                $pager_jsmx.gotoPage(1);
            })

            function loadJsmxAmount(){
                $.ajax({
                    url:"${basePath!}/p_merchant/clearMoney?mid=${id!}",
                    type:'POST', //GET
                    data:$("#jsmx-form").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        $("#uns").html(d.data.waitSettled);
                        $("#s").html(d.data.alreadySettled);
                    }
                });
            }
        });
    </script>
</body>

</html>
