<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
     <#include "common/common.ftl">
    <style>
        .money_a{
            display: none;

        }
        #minmoney{
            height: 20px;
        }
    </style>
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
                    <li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
                    <#--<li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>-->
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                    <div class="tab-content">
                        <div id="base" class="ibox-content active tab-pane">
                            <input type="hidden" name="id" value="${entity.id!}"/>
							<#if (entity.mid)??>
							     <div class="form-group">
                                     <label class="col-sm-2 control-label">商户id</label>
                                     <label class="col-sm-10 control-label" style="text-align: left;">
										 ${entity.mid!}
                                     </label>
                                 </div>
							</#if>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:商户名称不能为空" id="name" name="name" value="${entity.name!}" maxlength="30" placeholder="请填写商户名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户简称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,maxLength[6]" validate-msg="required:商户简称不能为空,maxLength:商户简称最大只能输入6个字符" id="simpleName" name="simpleName" value="${entity.simpleName!}" maxlength="30" placeholder="请填写商户简称">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">登陆密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control"validate-rule="pwdss" validate-msg="required:登陆密码不能为空" id="pwd" name="pwd" value="" maxlength="30" placeholder="不输入，则不修改密码">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">重复密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" validate-rule="equalTo[#pwd],pwdss" validate-msg="required:重复密码不能为空,equalTo:两次输入的密码不一致" id="rePwd" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">行业类型</label>
                                    <div class="col-sm-8">
                                        <select name="micId" id="micId">
												<#list list as list >
                                                    <option id="id" value="${list.id!}">${list.name!}</option>
												</#list>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:法人名称不能为空" id="legalName" name="legalName" value="${entity.legalName!}" maxlength="30" placeholder="请填写法人名称">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人证件号</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:证件号不能为空" id="idCard" name="idCard" value="${entity.idCard!}" maxlength="30" placeholder="请填写法人证件号">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">手机号码</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:手机号码不能为空" id="mobile" name="mobile" value="${entity.mobile!}" maxlength="30" placeholder="请填写手机号码">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">所属地区</label>
                                    <div class="col-sm-10" id="area" data-name="area" data-value="${entity.area!}">

                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">详细地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:详情地址不能为空" id="address" name="address" value="${entity.address!}" maxlength="30" placeholder="请填写详细地址">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">状态</label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status">
                                            <option value="1" <#if (((entity.status)!'') == '1')>selected="selected"</#if> >开启</option>
                                            <option value="0" <#if (((entity.status)!'') == '0')>selected="selected"</#if> >关闭</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>

                        <!-- 商户审核信息 -->
                        <div id="examine" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户性质</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:商户性质不能为空" id="nature" name="nature" value="${entity.nature!}" maxlength="30" placeholder="请填写商户性质">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">品牌经营年限</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:品牌经营年限不能为空" id="operYear" name="operYear" value="${entity.operYear!}" maxlength="30" placeholder="请填写品牌经营年限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户电话</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:经营面积不能为空" id="phoneNumber" name="phoneNumber" value="${entity.phoneNumber!}" maxlength="30" placeholder="请填写商户店铺电话">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商家介绍</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:注册资金不能为空" id="recommendGoods" name="recommendGoods" value="${entity.recommendGoods!}" maxlength="30" placeholder="请填写商家介绍">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">经营面积</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:经营面积不能为空" id="operArea" name="operArea" value="${entity.operArea!}" maxlength="30" placeholder="请填写经营面积">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">注册资金（万）</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:注册资金不能为空" id="regMoney" name="regMoney" value="${entity.regMoney!}" maxlength="30" placeholder="请填写注册资金">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">场地照片</label>
                                <div class="img" file-id="localPhoto" file-value="${entity.localPhoto!}" file-size="400*400">
                                    <input type="hidden" name="localPhoto1" value="${entity.localPhoto!}"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">营业执照</label>
                                <div class="img" file-id="licensePic" file-value="${entity.licensePic!}" file-size="400*400">
                                    <input type="hidden" name="licensePic1" value="${entity.licensePic!}"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">法人身份证</label>
                                <div class="img" file-id="legalPhoto" file-value="${entity.legalPhoto!}" file-size="400*400">
                                    <input type="hidden" name="legalPhoto1" value="${entity.legalPhoto!}"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">借记卡照片</label>
                                <div class="img" file-id="normalCard" file-value="${entity.normalCard!}" file-size="400*400">
                                    <input type="hidden" name="normalCard1" value="${entity.normalCard!}"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">信用卡照片</label>
                                <div class="img" file-id="creditCard" file-value="${entity.creditCard!}" file-size="400*400">
                                    <input type="hidden" name="creditCard1" value="${entity.creditCard!}"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                        </div>
                        <div id="settlement" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both;display:none"></div>
                            <div style="display:none">
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">授信额度上限</label>
                                    <div class="col-sm-8">
                                        <select name="authMaxFlag" id="authMaxFlag">
                                            <option value="0" <#if (((entity.authMaxFlag)!'') == '0')>selected="selected"</#if> >关闭</option>
                                            <option value="1" <#if (((entity.authMaxFlag)!'') == '1')>selected="selected"</#if> >开启</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">授信额度上限</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="authMax" name="authMax" value="${entity.authMax!}" maxlength="30" placeholder="请填额度上限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;display:none"></div>
                            <div style="display:none">
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">提现手续费开关</label>
                                    <div class="col-sm-8">
                                        <select name="withdrawFeeFlag" id="withdrawFeeFlag">
                                            <option value="0" <#if (((entity.withdrawFeeFlag)!'') == '0')>selected="selected"</#if> >关闭</option>
                                            <option value="1" <#if (((entity.withdrawFeeFlag)!'') == '1')>selected="selected"</#if> >开启</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">提现手续费率（%）</label>
                                    <div class="col-sm-8">
                                        <input type="text" validate-rule="required,num" validate-msg="required:提现手续费率不能为空" class="form-control" id="withdrawFee" name="withdrawFee" value="${entity.withdrawFee!}" maxlength="30" placeholder="请填写手续费率">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <#--<div class="form-group col-sm-6">-->
                                    <#--<label class="col-sm-4 control-label">是否开启让利</label>-->
                                    <#--<div class="col-sm-8">-->
									<#--&lt;#&ndash;<select name="shareFlag" id="shareFlag">&ndash;&gt;-->
                                        <#--&lt;#&ndash;<option value="0" <#if (((entity.shareFlag)!'') == 0)>selected="selected"</#if> >关闭</option>&ndash;&gt;-->
                                        <#--&lt;#&ndash;<option value="1" <#if (((entity.shareFlag)!'') == 1)>selected="selected"</#if> >开启</option>&ndash;&gt;-->
                                    <#--&lt;#&ndash;</select>&ndash;&gt;-->
                                    <#--</div>-->
                                <#--</div>-->
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">让利利率（%）</label>
                                    <div class="col-sm-8">
                                        <input type="text"  validate-rule="required,num" validate-msg="required:让利利率不能为空" class="form-control" id="shareFee" name="shareFee" value="${entity.shareFee!?c}" maxlength="30" placeholder="请填写让利利率">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="pass process">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-2 control-label">结算方式</label>
                                    <div class="col-sm-10" style="line-height: 40px">
                                        <div class="fangshi">
                                            <input type="radio" name="fangshi" id="time_a">
                                            <label for="">按时间结算</label>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <input type="radio" name="fangshi" id="money_a">
                                            <label for="">按金额结算</label>
                                        </div>
                                        <div>
                                            <div class="time_a">
                                                <select name="settlementLoop" id="settlementLoop" value="${entity.settlementLoop!}">
                                                    <option value="0" <#if (((entity.settlementLoop)!'') == '0')>selected="selected"</#if> >请选择</option>
                                                    <option value="1" <#if (((entity.settlementLoop)!'') == '1')>selected="selected"</#if> >日结</option>
                                                    <option value="2" <#if (((entity.settlementLoop)!'') == '2')>selected="selected"</#if> >周结</option>
                                                    <option value="7" <#if (((entity.settlementLoop)!'') == '7')>selected="selected"</#if> >半月</option>
                                                    <option value="3" <#if (((entity.settlementLoop)!'') == '3')>selected="selected"</#if> >月结</option>
                                                    <option value="4" <#if (((entity.settlementLoop)!'') == '4')>selected="selected"</#if> >季结</option>
                                                    <option value="5" <#if (((entity.settlementLoop)!'') == '5')>selected="selected"</#if> >半年</option>
                                                    <option value="6" <#if (((entity.settlementLoop)!'') == '6')>selected="selected"</#if> >一年</option>
                                                    <#--<option value="8" <#if (((entity.settlementLoop)!'') == '8')>selected="selected"</#if> >额度结算</option>-->
                                                </select>
                                            </div>
                                            <div class="money_a">
                                                <div> <label for="">最低结算金额 (元)</label>
                                                    <#if (entity.minmoney)?? >
                                                        <input type="text" id="minmoney" name="minmoney" value="${entity.minmoney!?c}">
                                                    <#else >
                                                        <input type="text" id="minmoney" name="minmoney" value="0">
                                                    </#if>
                                                </div>
                                                <div> <label for="">最迟结算时间 (天)</label>
                                                    <select name="latestday" id="latestday" value="${entity.latestday!}">
                                                        <option value="0" <#if (((entity.latestday)!'') == '0')>selected="selected"</#if> >请选择</option>
                                                        <option value="15" <#if (((entity.latestday)!'') == '15')>selected="selected"</#if> >15天</option>
                                                        <option value="30" <#if (((entity.latestday)!'') == '30')>selected="selected"</#if> >30天</option>
                                                        <option value="45" <#if (((entity.latestday)!'') == '45')>selected="selected"</#if> >45天</option>
                                                        <option value="60" <#if (((entity.latestday)!'') == '60')>selected="selected"</#if> >60天</option>
                                                        <option value="90" <#if (((entity.latestday)!'') == '90')>selected="selected"</#if> >90天</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>
                        <div id="bind" class="ibox-content tab-pane">
                            <input type="button" id="new-card" class="btn btn-success" value="新增" />
                            <div class="fixed-table-container form-group">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:10%;min-width:250px;">开户行</th>
                                        <th style="width:10%;min-width:60px;">开户人</th>
                                        <th style="width:10%;min-width:80px;">预留手机</th>
                                        <th style="width:20%;min-width:80px;">身份证号</th>
                                        <th>卡号</th>
                                        <th style="width:5%;min-width:120px;">是否主卡</th>
                                        <th style="width:20%">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody" style="text-align:center;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        var context= "${basePath!}";
        var context2= "${basePath!}";
        $(document).ready(function () {
            var minmoney = $.trim($("#minmoney").val());
            var latestday = $.trim($("#latestday").val());

            if(minmoney!=null || latestday !=null){
                $("#money_a"). attr("checked","checked");
                $(".time_a").css("display","none");
                $(".money_a").css("display","block");
            }

            if(minmoney==0){
                $("#time_a"). attr("checked","checked");
                $(".time_a").css("display","block");
                $(".money_a").css("display","none");
            }

            $("#status").select();
            $("select").select();

            $("#money_a").click(function () {
                $(".time_a").css("display","none");
                $(".money_a").css("display","block");
            });
            $("#time_a").click(function () {
                $(".time_a").css("display","block");
                $(".money_a").css("display","none");
            });

            $("#minmoney").on('keyup', function (event) {
                var $amountInput = $(this);
                //响应鼠标事件，允许左右方向键移动
                event = window.event || event;
                if (event.keyCode == 37 | event.keyCode == 39) {
                    return;
                }
                //先把非数字的都替换掉，除了数字和.
                $amountInput.val($amountInput.val().replace(/[^\d.]/g, "").
                //只允许一个小数点
                replace(/^\./g, "").replace(/\.{2,}/g, ".").
                //只能输入小数点后两位
                replace(".", "$#$").replace(/\./g, "").replace("$#$", ".").replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'));
            });
            $("#minmoney").on('blur', function () {
                var $amountInput = $(this);
                //最后一位是小数点的话，移除
                $amountInput.val(($amountInput.val().replace(/\.$/g, "")));
            });

            $("#authMaxFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#authMax").attr("disabled","disabled");
                }else{
                    $("#authMax").removeAttr("disabled");
                }
            });
            $("#withdrawFeeFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#withdrawFee").attr("disabled","disabled");
                }else{
                    $("#withdrawFee").removeAttr("disabled");
                }
            });
            $("#shareFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#shareFee").attr("disabled","disabled");
                }else{
                    $("#shareFee").removeAttr("disabled");
                }
            });

            $("#area").area();
            examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;ssss
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            $(".cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
                var totalData=document.getElementById("_f");
                var formData = new FormData(totalData);
                var minmoney = $.trim($("#minmoney").val());
                var latestday = $.trim($("#latestday").val());
                var settlementLoop = $.trim($("#settlementLoop").val());
                if(settlementLoop!=0 && minmoney == 0 && latestday ==0){

                }else if(minmoney != 0 && latestday !=0){

                }else {
                    layer.msg("你可以选择按时间结算（选择结算时间），或者填写最低结算金额不能低于0元,结算时间不能低于15天");
                    return;
                }
                var $this = $(this);
                $this.html("保 存 中");
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                    url:'${basePath!}/opera/merchantInfo/updateById',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:formData,
                    processData: false,                // jQuery不要去处理发送的数据
                    contentType: false,
                    success: function(data){ //成功
                        layer.close(index);
                        //消息对话框
                        if(data.code==0){
                            parent.messageModel(data.msg);
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }else {
                            layer.msg(data.msg)
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>
