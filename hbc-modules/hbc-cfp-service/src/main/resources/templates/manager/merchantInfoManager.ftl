<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<style>
        .dingdan-list{
            padding: 20px;
        }
         .dingdan-list .row{
         	padding-bottom:20px;
         }
    </style>
		<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户管理</h5>
						<div style="display:none;" class="ibox-tools">
							<a  href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商户</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<a href="${basePath!}/opera/merchantInfo/qu"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
									</div>
									<div class="col-sm-8">

									</div>
								</div>
							</div>
							<div class="dingdan-list">
                                <div class="container">
                                    <div class="row">
                                        <div class="form-group col-md-3 col-sm-3 col-lg-4 form-inline">
                                            <label class="control-label">商户名称</label>
                                            <input id="name" type="text" class="form-control" name="name" maxlength="30" placeholder="请输入名称">
                                        </div>
                                        <div class="form-group col-md-5 col-sm-5 col-lg-4 form-inline">
                                            <label class="control-label">手机号</label>
                                            <input id="mobile" type="text" class="form-control" name="mobile" maxlength="30" placeholder="请输入手机号">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4 col-lg-4">
                                            <label class="control-label">申请时间</label>
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:30%" maxlength="30" placeholder="">
									           -
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:30%" maxlength="30" placeholder="">
                                        </div>
								    <#--<div class="form-group col-lg-4" style="text-align:center">
										<input type="button" id="search" class="btn btn-warning" value="搜索" />
								    </div>-->
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4 col-sm-4 col-lg-4">
                                            <label class="control-label">行业类型</label>
                                            <select id="micId" name="micId" >
                                                <option value="">全部</option>
                                                <#list list as list >
                                                    <option id="id" value="${list.id!}">${list.name!}</option>
                                                </#list>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-3 col-lg-3">
                                            <label class="control-label">审核状态</label>
                                            <select id="authExamine" name="authExamine">
                                                <option value="">全部</option>
                                                <option value="0">未提交</option>
                                                <option value="1">待审核</option>
                                                <option value="2">审核失败</option>
                                                <option value="3">审核成功</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-3 col-lg-3" style="text-align: center;padding-left: 90px">
                                            <label class="control-label">商户状态</label>
                                            <select id=status name="status">
                                                <option value="">全部</option>
                                                <option value="0">已禁用</option>
                                                <option value="1">启用中</option>
                                                <option value="2">待审核</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-3 col-lg-2" align="right">
                                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="project-list" id="data"></div>
            </div>
        </div>
    </div>
	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
   		$("#status").select();
   		$("#authExamine").select();
   		$("#micId").select();
        laydate({
            elem: '#startDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
        });
        laydate({
            elem: '#endDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
        });

		var $pager = $("#data").pager({
			url:"${basePath!}/opera/merchantInfo/merchantInfoManagerList?oid=${oid!}",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>法人名称</th>'+
							'<th>商户手机</th>'+
							'<th>商户名称</th>'+
							'<th>行业类型</th>'+
							'<th>申请日期</th>'+
							'<th>审核日期</th>'+
							'<th>商户状态</th>'+
							'<th>审核状态</th>'+
				           	'<th>商业授信审核</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{legalName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{name}</td>'+
							'<td>{mname}</td>'+
							'<td>{createDate}</td>'+
							'<td>{examineDate}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="authExamine_{id}"></td>'+
                            '<td id="bstutas_{id}">'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
                console.log(result)
				for(var i = 0 ; i < result.data.records.length; i++){
                    var list = result.data.records[i];
                    var status = list.status;
					if(list.status == 1){
                        $("#status_"+list.id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
                    }
					else if(list.status == 0){
                        $("#status_"+list.id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
                    }
					else{
                        $("#status_"+list.id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
					}
					$btn = $('<a href="javascript:void(0);" data="'+list.id+','+list.simpleName+'" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a>');
					var authExamine = list.authExamine;
					//审核状态
					if(authExamine == 1){
                        $("#authExamine_"+list.id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
                    }
					else if(authExamine == 2)
						$("#authExamine_"+list.id).html("<span style='color:#ffffff;background-color:red;'>审核失败</span>");
					else if(authExamine == 3)
						$("#authExamine_"+list.id).html("<span style='color:#ffffff;background-color:green;'>审核成功</span>");
					else{
                        $("#authExamine_"+list.id).html("<span style='color:#000000;background-color:yellow;'>未提交</span>");
					}

					var syauthExamine = list.syauthExamine;
					//授信审核状态
					if(syauthExamine == 1){
                        $("#syauthExamine_"+list.id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
                    }
					else if(syauthExamine == 2)
						$("#syauthExamine_"+list.id).html("<span style='color:#ffffff;background-color:red;'>审核中</span>");
					else if(syauthExamine == 3)
						$("#syauthExamine_"+list.id).html("<span style='color:#ffffff;background-color:green;'>审核成功</span>");
					else if(syauthExamine == 3)
						$("#syauthExamine_"+list.id).html("<span style='color:#ffffff;background-color:green;'>已驳回</span>");
					else{
                        $("#syauthExamine_"+list.id).html("<span style='color:#000000;background-color:yellow;'>未提交</span>");
					}

					if(authExamine == 3){
						$("#oper_"+list.id).append($btn);
						$xzht = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list.id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>查看协议</span> </a>');
						$("#oper_"+list.id).append($xzht);
						$xzht.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('查看协议','${basePath!}/opera/merchantInfo/forwardMerchantInfoht?id='+id,'1000px','700px',$pager);
						});
					}
						if(authExamine == 1 || authExamine == 2){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list.id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('商户资料审核','${basePath!}/opera/merchantInfo/showApplyMerchant?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list.id).append($examine);
					}

                    var bstutas=list.bstatus;
                    //商业授信审核状态
                    if(bstutas== 0){
                        $("#bstutas_"+list.id).html("<span style='color:#000000;background-color:yellow;'>未通过</span>");
                    }
                    else if(bstutas == 1){
                        $("#bstutas_"+list.id).html("<span style='color:#ffffff;background-color:red;'>已通过</span>");
                    }
                    else if(bstutas == 2){
                        $("#bstutas_"+list.id).html("<span style='color:#ffffff;background-color:green;'>已报名</span>");
                    }
                    else{
                        $("#bstutas_"+list.id).html("<span style='color:#000000;background-color:yellow;'>未填写</span>");
                    }

					if(bstutas == 2){
						$sysh = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list.id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>商业授信审核</span> </a>');
						$("#bstutas_"+list.id).append($sysh);
						$sysh.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('商业授信审核','${basePath!}/opera/merchantInfo/showApplyshs?id='+id,'1000px','700px',$pager);
						});
                        $("#oper_"+list.id).append($sysh);
					}
				}

				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改商户-'+title,'${basePath!}/opera/merchantInfo/forwardMerchantInfoEdit?id='+id,'1000px','700px',$pager);
				});
				//编辑页面
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('商户详情-'+title,'${basePath!}/opera/merchantInfo/forwardMerchantInfoDetail?id='+id,'1000px','700px',$pager);
				});
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});

		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});

		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}

			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${basePath!}/opera/merchantInfo/delete.htm',param,$pager);
			}
		});

		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加商户','${basePath!}/opera/merchantInfo/forwardMerchantInfoAdd.htm','1000px','700px',$pager);
		});
	});

</script>

	</body>
</html>
