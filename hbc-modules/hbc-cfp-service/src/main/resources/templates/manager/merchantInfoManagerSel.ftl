<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户管理</h5>
						<div style="display:none;" class="ibox-tools">
							<a  href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商户</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">商户名称</label>
								<input type="text" name="name" placeholder="请输入商户名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/hotSearch/merchantInfoManagerList",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>商户手机</th>'+
							'<th>商户名称</th>'+
							'<th>行业类型</th>'+
							'<th>商户状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{mobile}</td>'+
							'<td>{simpleName}</td>'+
							'<td>{mcname}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-primary btn-sm check"><i class="fa fa-close"></i> 选择 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					console.log(list[i].status);
					if(list[i].status == 1)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
					else if(list[i].status == 0)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
					else
						$("#status_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");

				}

				$(".check").click(function(){
					$(".check").removeClass('btn-primary');
					$(".check").removeClass('btn-success');
					$(".check").find('i').removeClass('fa-close');
					$(".check").find('i').removeClass('fa-check');
					$(".check").addClass('btn-primary');
					$(".check").find('i').addClass('fa-close');
					$(this).removeClass('btn-primary');
					$(this).find("i").removeClass('fa-close');
					$(this).addClass('btn-success');
					$(this).find("i").addClass('fa-check');
					console.log(11);
				});

				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
	});
	
</script>

	</body>
</html>
