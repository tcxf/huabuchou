<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>- 基本表单</title>
<meta name="keywords" content="">
<meta name="description" content="">

<link rel="shortcut icon" href="favicon.ico">
<link href="<%=path%>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="<%=path%>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<link href="<%=path%>/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<%=path%>/css/animate.css" rel="stylesheet">
<link href="<%=path%>/css/style.css?v=4.1.0" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/plugins/webuploader/webuploader.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group"
		style="position: absolute;right:20px;top:20px;z-index: 99999;">
		<div>
			<input class="btn btn-white cancel" id="cancel" type="button"
				value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#base"
							aria-expanded="true"> 订单信息</a></li>
							
						<li><a data-toggle="tab" href="#payment" aria-expanded="true">支付记录</a></li>
					</ul>

					<div class="tab-content">
						<div id="base" class="ibox-content active tab-pane">
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单编号</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.serialNo} </label>
								</div>
								
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单总额</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.amount} </label>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">优惠金额</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.rmoneyStr} </label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">下单用户</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.uname} </label>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">用户手机</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										${order.mobile} </label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">订单状态</label> <label
										class="col-sm-8 control-label" style="text-align: left;">
										已完成
									</label>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label"> </label> <label
										class="col-sm-8 control-label" style="text-align: left;">

									</label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both;"></div>
						</div>
						<div id="payment" class="ibox-content tab-pane">
							<div class="ibox-content">
								<div class="project-list pager-list" id="hkjl-list">
									<div class="fixed-table-container form-group pager-content">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>支付编号</th>
													<th>支付方式</th>
													<th>支付金额</th>
													<th>支付时间</th>
													<th>支付状态</th>
												</tr>
											</thead>
											<tbody>
													<tr>
														<td>${order.serialNo}</td>
														<td>${order.tradingType == "0"?"授信金额":"自由金"}支付</td>
														<td>¥${order.amount}</td>
														<td>${order.createDateStr}</td>
														<td>已支付</td>
													</tr>
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 全局js -->
		<script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
		<script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>

		<!-- 自定义js -->
		<script src="<%=path%>/js/content.js?v=1.0.0"></script>
		<script type="text/javascript" src="<%=path%>/js/common.js"></script>
		<script src="<%=path%>/js/plugins/layer/layer.min.js"></script>
		<!-- iCheck -->
		<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
		<script src="<%=path%>/js/plugins/layer/laydate/laydate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
		<script>
			$(document).ready(function() {
				$(".cancel").click(function(){parent.closeLayer();});
			});
		</script>
</body>

</html>
