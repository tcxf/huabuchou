<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>操作日志</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">操作时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">操作人名称</label>
									<div class="col-sm-9">
										<input id="manme" type="text" class="form-control" name="operator" maxlength="30" placeholder="请填写操作人名称">
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		$("#status").select();
		var $pager = $("#data").pager({
			url:"${hsj}/operationLog/operationLogManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>操作类型</th>'+
							'<th style="width:500px;">操作内容</th>'+
							'<th>操作人</th>'+
							'<th>操作人类型</th>'+
							'<th>ip地址</th>'+
							'<th>操作时间</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{type}</td>'+
							'<td style="width:500px;">{descInfo}</td>'+
							'<td>{operator}</td>'+
							'<td id="utype_{id}"></td>'+
							'<td>{operationIp}</td>'+
							'<td>{createDateStr}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].userType == 0){
						$("#utype_"+list[i].id).html("商户");
					}else if(list[i].userType == 1){
						$("#utype_"+list[i].id).html("消费用户");
					}else if(list[i].userType == 2){
						$("#utype_"+list[i].id).html("平台管理员");
					}else if(list[i].userType == 3){
						$("#utype_"+list[i].id).html("运营商管理员");
					}else{
						$("#utype_"+list[i].id).html("未知");
					}
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				

				$(".tlog").click(function(){
					window.location="<%=path%>/operationLog/operationLogLog.htm?id="+$(this).attr("data");
				});

				$(".send").click(function(){
					$.post("<%=path%>/operationLog/send.htm",{id:$(this).attr("data")},function(d){
						parent.messageModel(d.resultMsg);
						if(d.resultCode == "1"){
							$pager.gotoPage($pager.pageNumber);
						}
					},"json");
				});
				
				$(".offline").click(function(){
					var id = $(this).attr("data");
					parent.confirm("是否确认线下结算？",function(){
						$.post('<%=path%>/operationLog/offline.htm',{id:id},function(d){
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}
							parent.messageModel(d.resultMsg);
						},"json");
					});
				});
				
				loadAmount();
			}
		});
		

		function loadAmount(){
			$.ajax({
				url:"<%=path%>/operationLog/totalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#uns").html(d.data.uns);
			    	$("#s").html(d.data.s);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
