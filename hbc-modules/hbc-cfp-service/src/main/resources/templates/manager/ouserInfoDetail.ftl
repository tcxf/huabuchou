<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv ="proma" content = "no-cache"/>
    <meta http-equiv="cache-control" content="no cache" />
    <meta http-equiv="expires" content="0" />

    <title> - 基本表单</title>
    <style>
        h3{
            font-size: 14px!important;
        }
        .letterts_tabel{
            width: 100%;
            padding-left: 20px;
        }
        .letterts_tabel>p{
            margin-left: 10px;
        }
        .letterts_tabel td{
            text-align: center;
            height: 60px;
            width: 33%;
        }
        .letterts_tabel table td:first-child{
            text-align: left;
        }
    </style>
    <meta name="keywords" content="">
    <meta name="description" content="">
<#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#auth" aria-expanded="true"> 授信信息</a></li>
                    <li id="jiaoyi"><a data-toggle="tab" href="#jyjl" aria-expanded="true"> 交易记录</a></li>
                    <li id="huank"><a data-toggle="tab" href="#hkjl" aria-expanded="true"> 还款记录</a></li>
                    <li id="yuqi"><a data-toggle="tab" href="#yqjl" aria-expanded="true"> 逾期记录</a></li>
                </ul>

                <div class="tab-content">
                    <div id="base" class="ibox-content active tab-pane">
                        <div class="form-group col-sm-6">
                            <label class="col-sm-4 control-label"> 会员ID</label>
                            <label id="unum" class="col-sm-8 control-label" style="text-align: left;">
                            ${entity.id!}
                            </label>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>

                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">会员姓名</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.realName!}
                                </label>
                            </div>
                        </div>

                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">商户名称</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                   <#if entity.userAttribute=="2" && (entity.mname)??>
                                       ${entity.mname!}
                                   <#elseif entity.userAttribute=="1">
                                        <p  style="color: red">（未开通商户功能）</p>
                                   <#else >
                                          <p  style="color: red">（未绑定商户）</p>
                                   </#if>
                                </label>
                            </div>

                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">手机号码</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                     <#if entity.userAttribute=="2">
                                         ${entity.mobile!}
                                     <#else >
                                         ${entity.umobile!}
                                     </#if>
                                </label>
                            </div>

                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <#--<div class="form-group">-->
                                <#--<label class="col-sm-2 control-label" style="padding-left: 24px">所属地区</label>-->
                                <#--<#if entity.userAttribute=="2">-->
                                   <#--<div class="col-sm-10" id="area" style="margin-bottom: 15px" data-name="area" data-value="${entity.area!}"></div>-->
                                <#--<#elseif (entity.userAttribute=="1") && (entity.uarea)??>-->
                                      <#--<div class="col-sm-10" id="area" style="margin-bottom: 15px" data-name="area" data-value="${entity.uarea!}"></div>-->
                                <#--<#else >-->
                                 <#--<p  style="color: red">（该用户未填写地区）</p>-->
                                <#--</#if>-->
                            <#--</div>-->

                        </div>

                        <#--<div class="hr-line-dashed" style="clear:both"></div>-->
                        <#--<div class="form-group col-sm-6">-->
                            <#--<label class="col-sm-4 control-label">详细地址</label>-->
                            <#--<label class="col-sm-8 control-label" style="text-align: left;">-->
                                <#--<#if entity.userAttribute=="2">-->
                                    <#--${entity.address!}-->
                                <#--<#elseif (entity.userAttribute=="1") && (entity.uaddress)?? >-->
                                    <#--${entity.uaddress!}-->
                                <#--<#else >-->
                                    <#--<p  style="color: red">（该用户未填写地址）</p>-->
                                <#--</#if>-->
                            <#--</label>-->
                        <#--</div>-->
                        <#--<div class="hr-line-dashed" style="clear:both"></div>-->
                    </div>
                    <!-- 商户审核信息 -->
                    <div id="auth" class="ibox-content tab-pane">
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">额度状态</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.authStatus!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">快速授信额度</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                    <#if (entity.authStatus=="已授信")>
                                        1500
                                    <#else >
                                          0
                                    </#if>
                                </label>
                            </div>

                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">授信提额</label>
                                    <label class="col-sm-8 control-label" style="text-align: left;">
                                    ${upMoney!}
                                    </label>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">授信总额度</label>
                                    <label class="col-sm-8 control-label" style="text-align: left;">
                                    ${totalMoney!?c}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">出账日期</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.ooaDate!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">还款日期</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.repayDate!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div class="letterts_tabel">
                            <p>授信分值</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td>类别</td>
                                        <td>用户情况</td>
                                        <td>分值</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>户籍</td>
                                        <td id="cityScore"></td>
                                        <td id="cityScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>性别</td>
                                        <td id="sexScore"></td>
                                        <td id="sexScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>年龄</td>
                                        <td id="ageScore"></td>
                                        <td id="ageScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>多头负债模型评分</td>
                                        <td id="mutipleScore"></td>
                                        <td id="mutipleScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>详单非标签类号码数量</td>
                                        <td id="tagMobileScore"></td>
                                        <td id="tagMobileScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>催收类别号码数量</td>
                                        <td id="debtCallScore"></td>
                                        <td id="debtCallScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>分期还款</td>
                                        <td id="paymentsCreditPartScore"></td>
                                        <td id="paymentsCreditPartScore_max"></td>
                                    </tr>
                                    <tr>
                                        <td>一次性还款</td>
                                        <td id="singleCreditPartScore"></td>
                                        <td id="singleCreditPartScore_max"></td>
                                    </tr>
                                    <#--<tr>-->
                                    <#--<td>最低额度还款</td>-->
                                    <#--<td></td>-->
                                    <#--<td></td>-->
                                    <#--</tr>-->
                                    <tr>
                                        <td>电商授权（京东及淘宝）</td>
                                        <td id="taobaoJD"></td>
                                        <td id="taobaoJD_max"></td>
                                    </tr>
                                    <tr>
                                        <td>社保公积金授权</td>
                                        <td id="SBGJ"></td>
                                        <td id="SBGJ_max"></td>
                                    </tr>
                                    <tr>
                                        <td>学信网授权</td>
                                        <td id="educationApprove"></td>
                                        <td id="educationApprove_max"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="jyjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="jyjl-form" class="form-horizontal">
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div>
                                    <div class="form-group col-sm-12">
                                        <h3>交易总额：¥ <span id="totalJyjlAmount">--</span> 元</h3>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">交易时间</label>
                                        <div class="col-sm-9">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="startDate" id="startDateJYJL" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="endDate" id="endDateJYJL" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">商户名称</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="mname" id="mname" maxlength="30" placeholder="请填写商户名称">
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">下单用户</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" id="name" maxlength="30" placeholder="请填写下单用户名称">
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                        <input class="btn btn-success" id="jyjl-search" type="button" value=" 查 询 " />
                                        <input id="ccs" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                                    </div>
                                </div>
                            </form>
                            <div class="project-list" id="jyjl-list"></div>
                        </div>
                    </div>
                    <div id="hkjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="hkjl-form" class="form-inline">
                                <input type="hidden" name="status" value="1"/>
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        <h3>已还总计：¥ <span id="yhAmount">--</span> 元</h3>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <h3>待还总计：¥ <span id="whAmount">--</span> 元</h3>
                                    </div>
                                    <#--<div class="form-group col-sm-4">-->
                                        <#--<h3>逾期总计：¥ <span id="yqAmount">--</span> 元</h3>-->
                                    <#--</div>-->
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label" style="padding-left: 0">还款状态</label>
                                        <div class="col-sm-9">
                                            <select id="repayType" name="repayType">
                                                <option value="">全部</option>
                                                <option value="1">已还</option>
                                                <option value="2">未还</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4" style="margin-bottom: 20px">
                                        <input class="btn btn-success" id="hkjl-search" type="button" value=" 查 询 " />
                                        <input id="ccss" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                                    </div>
                                </div>
                            </form>
                            <div class="project-list " id="hkjl-list">

                            </div>
                        </div>
                    </div>
                    <div id="yqjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="yqjl-form" class="form-horizontal">
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div>
                                    <div class="form-group col-sm-12">
                                        <h3>逾期总金额：¥ <span id="totaloverDue">--</span> 元  = ¥ <span id="overcurrentCorpus">--</span> 元 (本金)+ ¥ <span id="overcurrentFee">--</span> 元(利息) + ¥ <span id="overlateFee">--</span> 元(滞纳金)</h3>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">还款时间</label>
                                        <div class="col-sm-9">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" id="startDateYQJL" name="startDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" id="endDateYQJL" name="endDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                        <input class="btn btn-success" type="button" id="yqjl-search" value=" 查 询 " />
                                        <input id="ccsss" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                                    </div>
                                </div>
                            </form>
                            <div style="clear:both;"></div>
                            <div class="project-list" id="yqjl-list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#jyjl-list").empty();
            $("#hkjl-list").empty();
            $("#yqjl-list").empty();
            $("select").select();
            $("#repayType").select();
            $("#area").area();
            //下载报表
            $("#ccs").click(function(){
                $("#jyjl-form").attr("action","${basePath!}/opera/userInfo/tradingPoi");
                $("#jyjl-form").submit();
            });

            //下载报表
            $("#ccss").click(function(){
                $("#hkjl-form").attr("action","${basePath!}/opera/userInfo/tradingPois");
                $("#hkjl-form").submit();
            });

            //下载报表
            $("#ccsss").click(function(){
                $("#yqjl-form").attr("action","${basePath!}/opera/userInfo/tradingPoiss");
                $("#yqjl-form").submit();
            });

            $(".cancel").click(function(){parent.closeLayer();});

            laydate({
                elem: '#startDateJYJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDateJYJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });

            laydate({
                elem: '#startDateYQJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDateYQJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });

            $("#jiaoyi").unbind('click').bind('click',function(){
                  $.when(fjiaoyi()).done(function (data) {
                      console.log(data);
                });
            })
            $("#huank").unbind('click').bind('click',function(){
                $.when(fhuank()).done(function (data) {
                    console.log(data);
                });
            })
            $("#yuqi").unbind('click').bind('click',function(){
                $.when(fyuqi()).done(function (data) {
                    console.log(data);
                });
            })
                //交易明细里列表
            function fjiaoyi(){
                    var $pager_jyjl = $("#jyjl-list").pager({
                        url:"${basePath!}/opera/userInfo/tradingDetailManagerList?uid=${uid!}",
                        formId:"jyjl-form",
                        pagerPages: 3,
                        cache:false,
                        template:{
                            header:'<div class="fixed-table-container form-group pager-content">'+
                            '<table class="table table-hover">'+
                            '<thead>'+
                            '<tr>'+
                            '<th>交易单号</th>'+
                            '<th>商户id</th>'+
                            '<th>商户名称</th>'+
                            '<th>行业类型</th>'+
                            '<th>用户id</th>'+
                            '<th>下单用户</th>'+
                            '<th>交易类型</th>'+
                            '<th>交易时间</th>'+
                            '<th>交易金额</th>'+
                            '<th>实付金额</th>'+
                            '</tr>'+
                            '</thead><tbody>',
                            body:'<tr>'+
                            '<td>{serialNo}</td>'+
                            '<td>{mid}</td>'+
                            '<td>{mname}</td>'+
                            '<td>{nature}</td>'+
                            '<td>{uid}</td>'+
                            '<td>{realName}</td>'+
                            '<td>{tradingType}</td>'+
                            '<td>{tradingDate}</td>'+
                            '<td>{tradeAmount}</td>'+
                            '<td>{actualAmount}</td>'+
                            '</tr>',
                            footer:'</tbody></table>',
                            noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                        },
                        callbak: function(result){
                            $('input').iCheck({
                                checkboxClass: 'icheckbox_square-green',
                                radioClass: 'iradio_square-green',
                                increaseArea: '20%' // optional
                            });
                            // check 全部选中
                            $('.checks').on('ifChecked', function(event){
                                $("input[id^='check-id']").iCheck('check');
                            });

                            // check 全部移除
                            $('.checks').on('ifUnchecked', function(event){
                                $("input[id^='check-id']").iCheck('uncheck');
                            });
                            loadJyjlAmount();
                        }
                    });
                    $("#jyjl-search").unbind('click').bind('click',function(){
                        $("#jyjl-list").empty();
                        $pager_jyjl.gotoPage($pager_jyjl.pageNumber);
                    });

                function loadJyjlAmount(){
                    $.ajax({
                        url:"${basePath!}/opera/userInfo/totalJymxAmount?uid=${uid!}",
                        type:'POST', //GET
                        data:$("#jyjl-form").serialize(),
                        cache:false,
                        timeout:30000,    //超时时间
                        dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success:function(d){
                            $("#totalJyjlAmount").html(d.data);
                        }
                    });
                }
            }

                //还款纪录
            function fhuank(){
                    var $pager_hkjl = $("#hkjl-list").pager({
                        url:"${basePath!}/opera/userInfo/repaymentPlanManagerList?uid=${uid!}",
                        formId:"hkjl-form",
                        pagerPages: 3,
                        cache:false,
                        template:{
                            header:'<div class="fixed-table-container form-group pager-content">'+
                            '<table class="table table-hover">'+
                            '<thead>'+
                            '<tr>'+
                            '<th>流水单号</th>'+
                            '<th>用户id</th>'+
                            '<th>还款状态</th>'+
                            '<th>用户姓名</th>'+
                            '<th>应还本金</th>'+
                            '<th>滞纳金</th>'+
                            '<th>分期利息</th>'+
                            '<th>本期应还</th>'+
                            '<th>期数</th>'+
                            '<th>还款日</th>'+
                            '</tr>'+
                            '</thead><tbody>',
                            body:'<tr>'+
                            '<td>{serialNo}</td>'+
                            '<td>{userId}</td>'+
                            '<td>{repaymentType}</td>'+
                            '<td>{realName}</td>'+
                            '<td>¥ {currentCorpus} 元</td>'+
                            '<td>¥ {lateFee} 元</td>'+
                            '<td>¥ {currentFee} 元</td>'+
                            '<td id="yhs_{id}"></td>'+
                            '<td>{currentTime}/{totalTime}</td>'+
                            '<td>{repaymentDate}</td>'+
                            '</tr>',
                            footer:'</tbody></table>',
                            noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                        },
                        callbak: function(result){
                            // console.log(result)
                            for(var i = 0 ; i < result.data.records.length ; i++){
                                var item = result.data.records[i];
                                $("#yhs_"+item.id).html("¥ " + ((item.currentCorpus == null ? 0:item.currentCorpus)+(item.lateFee == null ? 0:item.lateFee)+(item.currentFee == null ? 0 : item.currentFee)).toFixed(2) + " 元");
                            }

                            $('input').iCheck({
                                checkboxClass: 'icheckbox_square-green',
                                radioClass: 'iradio_square-green',
                                increaseArea: '20%' // optional
                            });
                            // check 全部选中
                            $('.checks').on('ifChecked', function(event){
                                $("input[id^='check-id']").iCheck('check');
                            });

                            // check 全部移除
                            $('.checks').on('ifUnchecked', function(event){
                                $("input[id^='check-id']").iCheck('uncheck');
                            });
                            loadHkAmount();
                            loadHkAmounts();
                        }
                    });
                    $("#hkjl-search").unbind('click').bind('click',function(){
                        $("#hkjl-list").empty();
                        $pager_hkjl.gotoPage($pager_hkjl.pageNumber);
                    });

                function loadHkAmount(){
                    $.ajax({
                        url:"${basePath!}/opera/userInfo/SUM?uid=${uid!}",
                        type:'POST', //GET
                        data:$("#hkjl-form").serialize(),
                        cache:false,
                        timeout:30000,    //超时时间
                        dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success:function(d){
                            // console.log(d);
                            $("#whAmount").html(d.data.DhSum);
                            // $("#yqAmount").html(d.data.YqSum);
                        }
                    });
                }

                function loadHkAmounts(){
                    $.ajax({
                        url:"${basePath!}/opera/userInfo/SUMS?uid=${uid!}",
                        type:'POST', //GET
                        data:$("#hkjl-form").serialize(),
                        cache:false,
                        timeout:30000,    //超时时间
                        dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success:function(d){
                            // console.log(d);
                            $("#yhAmount").html(d.data.YhSum);
                        }
                    });
                }
            }

            //逾期
            function fyuqi() {
                var $pager_yqjl = $("#yqjl-list").pager({
                    url: "${basePath!}/opera/userInfo/pageInfo?uid=${uid!}&status=1&isPay=0",
                    formId: "yqjl-form",
                    pagerPages: 3,
                    cache: false,
                    template: {
                        header: '<div class="fixed-table-container form-group pager-content">' +
                        '<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>流水单号</th>' +
                        '<th>用户id</th>' +
                        '<th>期数</th>' +
                        '<th>逾期本金</th>' +
                        '<th>滞纳金</th>' +
                        '<th>利息费</th>' +
                        '<th>逾期应还</th>' +
                        '<th>还款日</th>' +
                        '<th>逾期天数</th>' +
                        '</tr>' +
                        '</thead><tbody>',
                        body: '<tr>' +
                        '<td>{serialNo}</td>' +
                        '<td>{userId}</td>' +
                        '<td>{currentTime}/{totalTime}</td>' +
                        '<td>¥ {currentCorpus} 元</td>' +
                        '<td>¥ {lateFee} 元</td>' +
                        '<td>¥ {currentFee} 元</td>' +
                        '<td id="yh_{id}"></td>' +
                        '<td>{repaymentDate}</td>' +
                        '<td>{margin} 天</td>' +
                        '</tr>',
                        footer: '</tbody></table>',
                        noData: '<tr><td colspan="10"><center>暂无数据</center></tr>'
                    },
                    callbak: function (result) {
                        // console.log(result);
                        for (var i = 0; i < result.data.records.length; i++) {
                            var item = result.data.records[i];
                            $("#yh_" + item.id).html("¥ " + ((item.currentCorpus == null ? 0 : item.currentCorpus) + (item.lateFee == null ? 0 : item.lateFee) + (item.currentFee == null ? 0 : item.currentFee)).toFixed(2) + " 元");
                        }
                        $('input').iCheck({
                            checkboxClass: 'icheckbox_square-green',
                            radioClass: 'iradio_square-green',
                            increaseArea: '20%' // optional
                        });
                        // check 全部选中
                        $('.checks').on('ifChecked', function (event) {
                            $("input[id^='check-id']").iCheck('check');
                        });

                        // check 全部移除
                        $('.checks').on('ifUnchecked', function (event) {
                            $("input[id^='check-id']").iCheck('uncheck');
                        });
                        loadYqAmount();
                    }
                });
                $("#yqjl-search").unbind('click').bind('click', function () {
                    $("#yqjl-list").empty();
                    $pager_yqjl.gotoPage($pager_yqjl.pageNumber);
                });

                function loadYqAmount() {
                    $.ajax({
                        url: "${basePath!}/opera/userInfo/overDue?uid=${uid!}",
                        type: 'POST', //GET
                        data: $("#searchForm").serialize(),
                        cache: false,
                        timeout: 30000,    //超时时间
                        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success: function (d) {
                            // console.log(d);
                            $("#totaloverDue").html(d.data.totalMoney);
                            $("#overcurrentCorpus").html(d.data.currentCorpus);
                            $("#overcurrentFee").html(d.data.currentFee);
                            $("#overlateFee").html(d.data.lateFee);
                        }
                    });
                }
            }


            $.ajax({
                url:"${basePath!}/opera/userInfo/tbUpMoney?uid=${uid!}",
                type:'POST', //GET
                data:$("#searchForm").serialize(),
                cache:false,
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d) {
                    console.log(d);
                    var tbjd=0;
                    var sbgj=0
                    for ( var i=0; i < d.data.data.length; i++){
                        //$("#"+ d.data.data[i].code).html(d.data.data[i].details);
                        $("#"+ d.data.data[i].code+"_max").html(d.data.data[i].value);
                        if(d.data.data[i].code =="taobaoApprove" || d.data.data[i].code =="jDcomApprove"){
                            tbjd= tbjd+(Number(d.data.data[i].value));
                            tbjd=Number(tbjd);
                        }
                        if(d.data.data[i].code =="accumulationFund" || d.data.data[i].code =="socialInsurance"){
                            sbgj= sbgj+(Number(d.data.data[i].value));
                            sbgj=Number(sbgj);
                        }
                    }
                    $("#taobaoJD_max").html(tbjd);
                    $("#SBGJ_max").html(sbgj);
                }
            });


            $.ajax({
                url:"${basePath!}/opera/userInfo/tbCreditGrade?uid=${uid!}",
                type:'POST', //GET
                data:$("#searchForm").serialize(),
                cache:false,
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    // console.log(d)
                    var max=0;
                    for ( var i=0; i < d.data.data.length; i++) {
                        $("#" + d.data.data[i].code).html(d.data.data[i].details);
                        $("#" + d.data.data[i].code + "_max").html(d.data.data[i].value);
                    }
                }
            });
        });
    </script>
</body>

</html>
