<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>平台服务费</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-6">
									<h3>结算总额：¥ <span id="s">--</span> 元</h3>
								</div>
								<div class="form-group col-sm-6">
									<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">交易时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">结算状态</label>
									<div class="col-sm-9">
										<select id="status" name="status">
											<option value="">全部</option>
											<option value="0">待确认</option>
											<option value="1">待结算</option>
											<option value="2">已结算</option>
										</select>
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="ptfwf" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		$("#ptfwf").click(function(){
				var status = $("#status option:selected").val();
				window.location.href="${hsj}/platformsd/ptfwf.htm?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&status="+status;
		});
		
		
			laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		$("#status").select();
		var $pager = $("#data").pager({
			url:"${hsj}/platformsd/platformsdManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>结算单号</th>'+
							'<th>平台商</th>'+
							'<th>结算金额</th>'+
							'<th>结算状态</th>'+
							'<th>结算时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>授信平台</td>'+
							'<td>{amount}</td>'+
							'<td id="status_{id}"></td>'+
							'<td>{settlementTimeStr}</td>'+
							'<td></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="6"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var binfo = null;
				$.ajax({
					url:context+"/js/bank/bank_info.json",
				    type:'POST', //GET
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    async:false,
				    success:function(data){
				    	binfo = data;
				    }
				});
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].status == 0)
						$("#status_"+list[i].id).html("待确认");
					else if(list[i].status == 1)
						$("#status_"+list[i].id).html("待结算");
					else
						$("#status_"+list[i].id).html("已结算");
					if(binfo != null){
						for(var b = 0 ; b < binfo.length ; b++){
							if(binfo[b].sn == list[i].bankSn) {
								$("#bankname_"+list[i].id).html(binfo[b].name);
								break;
							}
						}
					}else{
						$("#bankname_"+list[i].id).html("未知");
					}
					
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				loadAmount();
			}
		});
		

		function loadAmount(){
			$.ajax({
				url:"<%=path%>/platformsd/totalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#uns").html(d.data.uns);
			    	$("#s").html(d.data.s);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
