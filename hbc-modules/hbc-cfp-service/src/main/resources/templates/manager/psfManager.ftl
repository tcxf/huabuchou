<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>运营商让利</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3>让利总额：¥ <span id="totalAmount">--</span> 元</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">交易时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">商户名称</label>
									<div class="col-sm-9">
										<input id="legalName" type="text" class="form-control" name="legalName" maxlength="30" placeholder="请填写商户名称">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">下单用户</label>
									<div class="col-sm-9">
										<input id="realName" type="text" class="form-control" name="realName" maxlength="30" placeholder="请填写下单用户">
									</div>
								</div>
								
<!-- 								<div class="form-group col-sm-6 col-lg-4"> -->
<!-- 									<label class="col-sm-3 control-label">交易状态</label> -->
<!-- 									<div class="col-sm-9"> -->
<!-- 										<select id="tradeType" name="tradeType"> -->
<!-- 											<option value="">全部</option> -->
<!-- 											<option value="1">未支付</option> -->
<!-- 											<option value="2">已支付</option> -->
<!-- 											<option value="3">已退款</option> -->
<!-- 											<option value="4">退款中</option> -->
<!-- 										</select> -->
<!-- 									</div> -->
<!-- 								</div> -->
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="ptrl" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
	
        //下载报表
        $("#ptrl").click(function(){
            $("#searchForm").attr("action","${basePath!}/opera/o_share/consumeManagerPoi");
            $("#searchForm").submit();
        });
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
		});
   		$("#tradeType").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/o_share/OperaPageInfo",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>商户名称</th>'+
							'<th>行业类型</th>'+
							'<th>下单用户</th>'+
				            '<th>用户手机</th>'+
               				 '<th>交易状态</th>'+
							'<th>交易时间</th>'+
							'<th>交易总额</th>'+
							'<th>优惠金额</th>'+
							'<th>实付总额</th>'+
							'<th>让利金额</th>'+
				            '<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{legalName}</td>'+
							'<td>{nature}</td>'+
							'<td>{realName}</td>'+
               				 '<td>{telPhone}</td>'+
                			'<td>{paymentStatus}</td>'+
							'<td>{tradingDate}</td>'+
							'<td>{tradeAmount}</td>'+
							'<td>{discountAmount}</td>'+
							'<td>{actualAmount}</td>'+
							'<td>{pshareProfit}</td>'+
               		 '<td><a href="javascript:void(0);" data="{id}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查看 </a></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="12"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});

                $(".mx").click(function(){
                    window.location.href="${basePath!}/opera/o_share/jumpMerProfitDetail?id="+$(this).attr("data");
                });

				loadAmount();//加载分润
			}
		});
		
		function loadAmount(){
			$.ajax({
				url:"${basePath!}/opera/o_share/TotalProfit",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data.totalProfit);
			    }
			});
		}
	});
</script>

	</body>
</html>
