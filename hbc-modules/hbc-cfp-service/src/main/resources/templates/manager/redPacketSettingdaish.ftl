
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>优惠券管理</h5>
                </div>
                <div class="ibox-content">
                    <div id="yyh"></div>
                    <div id="yyh1">
                    </div>

                    <form id="searchForm" class="form-inline">
                        <div class="form-group">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
                                </div>
                                <div class="col-sm-8">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2" class="sr-only">优惠券名称</label>
                            <input type="text" name="name" placeholder="请输入优惠券名称" id="name" class="form-control">
                        </div>
                        <input type="button" id="search" class="btn btn-warning" value="搜索" />
                    </form>
                    <div class="project-list pager-list" id="data"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>



<script type="text/javascript">
    $(document).ready(function(){

        var $pager =  $("#data").pager({
            url:"${basePath!}/opera/ored/findredlist",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>商户名称</th>'+
                '<th>优惠券名称</th>'+
                '<th>优惠金额</th>'+
                '<th>满足金额</th>'+
                '<th>用户实际优惠金额</th>'+
                '<th>利差金额</th>'+
                '<th>单次领取的数量</th>'+
                '<th>创建时间</th>'+
                '<th>审核时间</th>'+
                '<th>有效期类型</th>'+
                '<th>有效期</th>'+
                '<th>操作</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{mname}</td>'+
                '<td>{name}</td>'+
                '<td>{money}</td>'+
                '<td>{fullMoney}</td>'+
                '<td>{actualMoney}</td>'+
                '<td>{opMoney}</td>'+
                '<td >{num}</td>'+
                '<td >{createDate}</td>'+
                '<td >{modifyDate}</td>'+
                '<td id="yxq_{id}"></td>'+
                '<td id="sj_{id}"></td>'+
                '<td id="status_{id}">'+
                '<a href="javascript:void(0);" data-id="{id}" class="btn btn-primary btn-sm ck "><i class="fa fa-unlock"></i> <span>查看</span> </a> &nbsp;'+
                '<a href="javascript:void(0);" data-id="{id}"  class="btn btn-primary btn-sm bj "><i class="fa fa-unlock"></i> <span>编辑</span> </a>'+
                '</td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="12"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                    var $btn;
                    var bt=list[i].balanec+list[i].txje;
                    $("#beforeTx_"+list[i].id).html(bt.toFixed(2));
                    if (list[i].timeType==1){
                        $("#yxq_"+list[i].id).html('<p>固定截止时间</p>  ');
                        $("#sj_"+list[i].id).html(list[i].endDate);
                    }else {
                        $("#yxq_"+list[i].id).html('<p>可用天数</p>  ');
                        $("#sj_"+list[i].id).html(list[i].days);
                    }
                }
                $(".ck").click(function (){
                    var id = $(this).attr("data-id");
                    parent.openLayer('优惠券信息','${basePath!}/opera/ored/findupdatared?id='+id,'1000px','700px',$pager);
                });
                $(".bj").click(function (){
                    var id = $(this).attr("data-id");
                    parent.openLayer('优惠券信息','${basePath!}/opera/ored/updataredbj?id='+id,'1000px','700px',$pager);
                });

                $("#loading-example-btn").click(function(){
                    $pager.gotoPage($pager.pageNumber);
                });

            }



        });


    });

</script>

</body>
</html>
