<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${basePath!}/css/bootstrap.min.css" />
    <title>Title</title>
    <style>
        .a_top{
            padding:20px 36px;
        }
        .coupont_list{
            margin-top: 20px;
            padding-left: 20px;
        }

    </style>
    <#include "common/common.ftl">
</head>
<body>
<div class="a_top">
    <div class="row" style="border-bottom: 1px solid #ededed;padding-bottom: 10px">
        <div class="form-group col-sm-2">
            审核操作
        </div>
        <div class="col-sm-6" style="text-align: center">
            <a href="#"  id="cancel" class="btn btn-danger">取消</a>
        </div>
    </div>
</div>
<div class="coupont_list">

    <div class="row">
        <div class="form-group col-sm-4">
            <label class="col-sm-12 control-label">
                商户优惠券: 满${r.fullMoney}减${r.money}元
            </label>

        </div>
        <label class="form-group col-sm-6">利差:${r.opMoney}  元</label>
    </div>
    <div class="row">
        <div class="form-group col-sm-5">
            <label class="col-sm-12 control-label">
                优惠券满足金额 (元): ${r.fullMoney}元
            </label>

        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-5">
            <label class="col-sm-12 control-label" >
                <span style="color: red">*</span>设置优惠券优惠金额 (元) : ${r.actualMoney}
            </label>

        </div>
    </div>
</div>
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script>


    $("#cancel").click(function(){parent.closeLayer();});

</script>

</body>
</html>