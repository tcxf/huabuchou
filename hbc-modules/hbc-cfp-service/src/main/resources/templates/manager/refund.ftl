<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>退单记录</h5>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">用户名称</label>
								<input type="text" name="name" placeholder="请输入用户名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>

	
	
	<script type="text/javascript">
	$(document).ready(function(){
		
   	
		var $pager =  $("#data").pager({
			url:"${hsj}/refund/refundsqlist.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
						    '<th>退款单号</th>'+
							'<th>用户名称</th>'+
							'<th>手机号</th>'+
							'<th>商家</th>'+
							'<th>商家类型</th>'+
							'<th>交易方式</th>'+
							'<th>交易金额</th>'+
							'<th>状态</th>'+
							'<th>退款时间</th>'+
							'<th>审核时间</th>'+
							'<th>退款金额</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
				            '<td>{serialNo}</td>'+
							'<td>{uname}</td>'+
							'<td>{mobile}</td>'+
							'<td>{mname}</td>'+
							'<td>{mcname}</td>'+
							'<td>{tradingTypeStr}</td>'+
							'<td>{amount}</td>'+
							'<td>{refundStatusStr}</td>'+  
							'<td>{refundDate}</td>'+   
							'<td>{refundingDate}</td>'+   
							'<td>{amount}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){  
				var oiid =	list[i].oiid;
					var bt=list[i].balanec+list[i].txje; 
					$("#beforeTx_"+list[i].id).html(bt.toFixed(2));    
					if(list[i].refundStatus == 0){
						$("#zt_"+list[i].id).html('申请中');
					}if(list[i].refundStatus == 1){
						$("#zt_"+list[i].id).html('已完成');
					}	if(list[i].refundStatus == 4){
						$("#zt_"+list[i].id).html('已驳回');
					}	
				}
				//编辑页面
				$(".edit").click(function (){
					var oiid = $(this).attr("data");
					$.ajax({
						url:"${hsj}/refund/refundqr.htm",
					    type:'POST', //GET
					    data:{id:oiid
					    	},
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    success:function(d){
					    	if(d.resultCode==1){
					    		parent.messageModel(d.resultMsg);
					    		window.location.href="<%=path %>/refund/refundlist.htm";
					    	}
					    }
					});
				});
				//编辑页面
				$(".detail").click(function (){
					var id = $(this).attr("data");
					$.ajax({
						url:"${hsj}/refund/refundbh.htm",
					    type:'POST', //GET
					    data:{
					    	id:id
					    },
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    success:function(d){
					    	if(d.resultCode==1){
					    		parent.messageModel(d.resultMsg);
					    		window.location.href="<%=path %>/refund/refundlist.htm";
					    	}
					    }
					});	
				
				});	

				$("#loading-example-btn").click(function(){
					$pager.gotoPage($pager.pageNumber);
				});		
		
				}
			
			
			
		});

	
	});
	
</script>

	</body>
</html>