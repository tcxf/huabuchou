
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>资源信息</h5>
                    <div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
                        <div>
                            <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
                            <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="_f" method="post" class="form-horizontal">
                        <input type="hidden" name="id" value=""/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">资源名称</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" validate-rule="required,maxLength[20],CHS" validate-msg="required:资源名称不能为空,maxLength:资源名称长度不能超过20" id="resourceName"  name="resourceName" value="" maxlength="30" placeholder="请填写资源名称">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">资源类型</label>
                            <div class="col-sm-10">
                                <select id="resourceType" name="resourceType">
                                    <option value="1" >普通菜单</option>
                                    <option value="2" >模块</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed model"></div>
                        <div class="form-group model">
                            <label class="col-sm-2 control-label">所属模块</label>
                            <div class="col-sm-10">
                                <select class="dfinput" name="parentId" id="parentId" style="width:100px;">
                                    <option value="">顶级模块</option>
                                        <#list tList as list>
                                                <option value="${list.getId()!}">${list.getResourceName()!}</option>
                                        </#list>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">资源路径/系统图标</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" validate-rule="required,maxLength[100]" validate-msg="required:资源路径不能为空" id="resourceUrl" name="resourceUrl" value="" maxlength="100" placeholder="请填写资源路径">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">排序号</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:排序号不能为空" id="sortNo" name="sortNo" value="" maxlength="30" placeholder="请填写排序号">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<!-- iCheck -->
<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function () {

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        $("#resourceType").change(function(){
            if($(this).val() == 1){
                $(".model").show();
            }else{
                $(".model").hide();
            }
        });
        $("#resourceType").trigger("change");
        $("#parentId").select();
        $("#resourceType").select();
        $("#cancel").click(function(){parent.closeLayer();});
        $("#submit-save").click(function (){
            if(!$("#_f").validate()) return;
            var $this = $(this);
            $this.html("保 存 中");
            $this.attr("disabled", true);
            var index = layer.load(2, {time: 10*1000});
            var data=   $("#_f").serializeObject();
            $.ajax({
                url:'${basePath!}/opera/mRe/insert',
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:data,
                success: function(data){ //成功
                    layer.close(index);
                    var obj = data;
                    if (obj.code == "-1") {
                        parent.messageModel(obj.resultMsg==null?"系统异常":obj.msg);
                        $this.html("保存内容");
                        $this.attr("disabled", false);
                    }
                    if (obj.code == "0") {
                        //消息对话框
                        parent.messageModel("保存成功");
                        parent.c.gotoPage(null);
                        parent.closeLayer();
                    }
                }
            });
        });
    });
</script>
</body>

</html>
