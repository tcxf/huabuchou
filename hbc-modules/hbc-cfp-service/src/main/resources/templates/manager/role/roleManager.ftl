<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>角色管理</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建角色</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
<!-- 								<label for="exampleInputEmail2" class="sr-only">角色名称</label> -->
<!-- 								<input type="text" validate-rule="required,maxLength[10]" validate-msg="required:角色名称不能为空" name="roleName" placeholder="请输入角色名称" id="roleName" class="form-control"> -->
							</div>
<!-- 							<input type="button" id="search" class="btn btn-warning" value="搜索" /> -->
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/mRole/roleManagerList",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>角色名称</th>'+
							'<th>角色描述</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{roleName}</td>'+
							'<td>{remark}</td>'+
							'<td id="{id}">'+
								'<a href="javascript:void(0);" data="{id},{roleName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> 删除 </a> '+
								'<a href="javascript:void(0);" data-id="{id}" class="btn btn-success btn-sm distribution"><i class="fa fa-plus"></i> 分配资源 </a>'+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="3"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].roleName == "超级管理员"){
						$("#"+list[i].id).empty();
					}
				}
				
				$(".distribution").unbind("click");
				$(".distribution").bind("click", function(){
					var id = $(this).attr("data-id");
					parent.openLayerWithBtn('分配资源','${basePath!}/opera/mRole/selectResources?id='+id,'800px','500px', null, ['选好了','不选了'], function(index, layero){
						var body = parent.layer.getChildFrame('body', index);
						var $checked = body.find(".resources:checked");
						var ids = "";
						if($checked != null && $checked.length > 0){
							$checked.each(function(i,e){
								ids += ","+$(e).val();
							});
							ids = ids.substring(1);
						}
						$.post("${basePath!}/opera/mRole/distribution?",{id:id,ids:ids},function(r){
							parent.messageModel(r.msg);
                            if(r.code!=1)
                            {
                                parent.layer.close(index);
                            }
						},"json");
					});
				});
				

				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('${basePath!}/opera/mRole/deleteById',param,$pager);
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改角色-'+title,'${basePath!}/opera/mRole/updateRole?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${basePath!}/opera/mRole/delete.htm',param,$pager);
			}
		});
		
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加角色','${basePath!}/opera/mRole/addRole.htm','1000px','700px',$pager);
		});
	});
	
</script>

	</body>
</html>
