<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <title>自定义版本</title>
    <style>
        .top_border{
            width: 100%;
            height: auto;
            padding-bottom: 20px;
            border: 1px solid #dadada;
        }
        .top_title{
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #f5f5f5;
            padding-left: 10px;
        }
        .kelong_bb>p{
            padding: 10px;
        }
        .kelong_bb .kl_table th{
            width: 20%;
            color: #000;
        }
        .kelong_bb .kl_table th:nth-child(3){
            width: 40%;
        }
        .kelong_bb .kl_table ul{
            padding: 0;
            margin: 0;
        }
        .caozuo li{
            list-style: none;
            float: left;
            margin-right: 20px;
        }
        .caozuo li img{
            width: 15px;
            height: 15px;
        }
        .caozuo li:last-child>a{
            color: #999;
        }
        .fabu_modal{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            height: 100%;
        }
        .kelong_modal{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            height: 100%;
        }
        .modal_content{
            width: 20%;
            height: 200px;
            overflow: auto;
            background: #fff;
            margin-left: 40%;
            margin-top: 15%;
            border-radius: 8px;
            position: relative;
        }
        .delete_modal{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            height: 100%;
        }
        .modal_body{
            height: 80px;
        }
        .modal_footer li{
            list-style: none;
            float: right;
            margin-right: 10px;
        }
        .dqul li{
            list-style: none;
            float: left;
        }
        .dqbb{
            width: 80px;
            height: 20px;
            line-height: 20px;
            background: #f49110;
            border-radius: 8px;
            text-align: center;
            color: #fff;
            margin-left: 20px;
        }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_border">
        <div class="top_title">
            评分自定义
        </div>
        <div class="kelong_bb">
            <p>已发布版本</p>
            <div class="kl_table">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>版本编号</th>
                        <th>版本发布时间</th>
                        <th>更新模块</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <ul class="dqul">
                                <li>
                                ${dqlist.version!}
                                </li>
                                <li>
                                    <div class="dqbb">当前版本</div>
                                </li>
                            </ul>
                        </td>
                        <td>${(dqlist.publishDate?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                        <td>${dqlist.updateModule!}</td>
                        <td>
                            <ul class="caozuo">
                                <li>
                                    <a href="#" onclick="tolook('${dqlist.id!}')">
                                        <img src="${basePath!}/img/u5949.png" alt="">
                                        查看
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>

                    <#list lslist as lslist>
                        <tr>
                            <td>${lslist.version!}</td>
                            <td>${(lslist.publishDate?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>${lslist.updateModule!}</td>
                            <td>
                                <ul class="caozuo">
                                    <li>
                                        <a href="#" onclick="tolook('${lslist.id!}')">
                                            <img src="${basePath!}/img/u5949.png" alt="">
                                            查看
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </#list>

                    </tbody>
                </table>
            </div>
        </div>


        <input type="hidden" id="id">
        <input type="hidden" id="delid">
        <input type="hidden" id="pubid">
    </div>
</div>
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script>
    $(".modal_head b,.cancel_no").click(function (){
        $(".fabu_modal,.delete_modal,.kelong_modal").css("display","none")
    });
    $(".ok_poss").click(function (){
        $(".fabu_modal,.delete_modal,.kelong_modal").css("display","none");

    });

    //查看
    function tolook(id) {
        window.location.href="${basePath!}/fund/creditCustom/goToRefusingCreditPage?id="+id+"&type=hidden";
    }

</script>
</body>
</html>