<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

<#include "common/common.ftl">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>交易明细</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/settlementDetail/tradingDetailManagerList?id=${id!}",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>支付编号</th>'+
							'<th>下单用户</th>'+
							'<th>用户手机</th>'+
							'<th>商户名称</th>'+
							'<th>交易金额</th>'+
							'<th>优惠金额</th>'+
							'<th>实付金额</th>'+
							'<th>运营商抽成</th>'+
							'<th>商户返佣</th>'+
							'<th>会员返佣</th>'+
							'<th>支付时间</th>'+
							'<th>支付方式</th>'+
							'<th>支付状态</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{serialNo}</td>'+
							'<td>{realName}</td>'+
							'<td>{nmobile}</td>'+
							'<td>{mname}</td>'+
							'<td>{tradeAmount}</td>'+
							'<td>{discountAmount}</td>'+
							'<td>{actualAmount}</td>'+
							'<td id="opaShareAmount_{id}"></td>'+
							'<td id="redirectShareAmount_{id}"></td>'+
							'<td id="inderectShareAmount_{id}"></td>'+
							'<td>{tradingDate}</td>'+
							'<td id="paymentStatus_{id}"></td>'+
							'<td id="tradingType_{id}"></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="14"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
					$("#opaShareAmount_"+list[i].id).html(parseFloat(list[i].opaShareAmount));
					$("#redirectShareAmount_"+list[i].id).html(parseFloat(list[i].redirectShareAmount));
                    $("#inderectShareAmount_"+list[i].id).html(parseFloat(list[i].inderectShareAmount));
                    if(list[i].tradingType == 0){
                        $("#tradingType_"+list[i].id).html("已支付");
                    }else if(list[i].tradingType == 1){
                        $("#tradingType_"+list[i].id).html("未支付");
                    }
                    if(list[i].paymentStatus == 0){
                        $("#paymentStatus_"+list[i].id).html("非授信消费");
                    }else if(list[i].paymentStatus == 1){
                        $("#paymentStatus_"+list[i].id).html("授信消费");
                    }
                }
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});

		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
</script>

	</body>
</html>
