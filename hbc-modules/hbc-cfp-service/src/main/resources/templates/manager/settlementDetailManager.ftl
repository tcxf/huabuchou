<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
					<li><a data-toggle="tab" href="#uncreate" aria-expanded="true" id="opera_unOut_Bill"> 未生成结算单</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-lg-4">
                                            <h3>待结算总额：¥ <span id="DAmount">--</span> 元</h3>
                                        </div>
                                        <div class="form-group col-sm-6 col-lg-4"">
                                        <h3>已结算总额：¥ <span id="YAmount">--</span> 元</h3>
                                    </div>
                            </div>
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">出账时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户名称</label>
											<div class="col-sm-9">
												<input id="mname" type="text" class="form-control" name="mname" maxlength="30" placeholder="请填写商户名称">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">结算状态</label>
											<div class="col-sm-9">
												<select id="status" name="status">
													<option value="">全部</option>
													<option value="0">待确认</option>
													<option value="1">待结算</option>
													<option value="2">已结算</option>
												</select>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">结算时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate2" id="startDate2" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate2" id="endDate2" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
											<input id="jsmx" class="btn btn-info" type="button" value=" 下 载 报 表 " />
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data"></div>
							</div>
						</div>
					</div>
					<div id="uncreate" class="ibox-content  tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6">
											<h3>待结算总额：¥ <span id="ua">--</span> 元</h3>
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data-uncreate"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
            $("#status").select();
            laydate({
                elem: '#startDate2',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#endDate2',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#startDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#endDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });

		$("#jsmx").click(function(){
            $("#searchForm").attr("action","${basePath!}/opera/settlementDetail/jsmx");
            $("#searchForm").submit();
		});

		$("#status").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/opera/settlementDetail/settlementDetailManagerList?ooid=${ooid!}",
			formId:"searchForm",
			pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>交易单号</th>'+
                '<th>商户名称</th>'+
                '<th>所属运营商</th>'+
                '<th>出账时间</th>'+
                '<th>结算时间</th>'+
                '<th>结算状态</th>'+
                '<th>结算状态(平台)</th>'+
                '<th>结算状态(运营商)</th>'+
                '<th>商户结算</th>'+
                '<th>运营商结算</th>'+
                '<th>平台结算</th>'+
                '<th>结算金额</th>'+
                '<th>操作</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{serialNo}</td>'+
                '<td>{mname}</td>'+
                '<td>{oname}</td>'+
                '<td>{outSettlementDate}</td>'+
                '<td>{settlementTime}</td>'+
                '<td>{status}</td>'+
                '<td>{platStatus}</td>'+
                '<td>{operStatus}</td>'+
                '<td>{oamount}</td>'+
                '<td>{operatorShareFee} </td>'+
                '<td>{platfromShareFee}</td>'+
                '<td>{sumamount}</td>'+
                '<td id="opera_{id}"><a href="javascript:void(0);"  data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-file"></i> 交易明细 </a> </td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="17"><center>暂无数据</center></tr>'
            },
			callbak: function(result){
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].operStatus == "待确认"){
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm sure"><i class="fa fa-check"></i> 确认结算 </a>');
					}else if(list[i].status == 1){
						$("#status_"+list[i].id).html("待结算");
					}else if(list[i].status == 2){
						$("#status_"+list[i].id).html("已结算");
					}
				}

				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});

				$(".tlog").click(function(){
					window.location="${basePath!}/opera/settlementDetail/settlementDetailLog?id="+$(this).attr("data");
				});

				$(".sure").click(function(){
				    id = $(this).attr("data");
					li =  layer.confirm('确定进行结算吗？', {
                        btn: ['确定','取消'] //按钮
					}, function(){
						$.ajax({
							url:"${basePath!}/opera/settlementDetail/sureSettlement?id="+id,
							type:'POST', //数据发送方式
							dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
							success: function(d){ //成功
								parent.messageModel(d.msg);
								layer.close(li);
							}
						});
				  });
				});
                loadAmount2();//加载头部信息
                loadAmount3();
			}
		});

		$("#opera_unOut_Bill").click(function(){
			var $pager_uncreate = $("#data-uncreate").pager({
				url:"${basePath!}/opera/settlementDetail/uncreateSettlementDetail",
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
					'<thead>'+
					'<tr>'+
					'<th>商户ID</th>'+
					'<th>商户名称</th>'+
					'<th>商户手机</th>'+
					'<th>结算周期</th>'+
					'<th>平台结算佣金</th>'+
					'<th>运营商佣金</th>'+
					'<th>结算金额</th>'+
					'<th>操作</th>'+
					'</tr>'+
					'</thead><tbody>',
					body:'<tr>'+
					'<td>{mid}</td>'+
					'<td>{legalName}</td>'+
					'<td>{legalTel}</td>'+
					'<td>{clearDay}</td>'+
					'<td>{pShareAmount}</td>'+
					'<td>{opaShareAmount}</td>'+
					'<td>{actualAmount}</td>'+
					'<td><a href="javascript:void(0);" data="{mid}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 交易明细 </a></td>'+
					'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						$("input[id^='check-id']").iCheck('check');
					});

					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						$("input[id^='check-id']").iCheck('uncheck');
					});
					uncreateAmount();
					//交易明细
					$(".mx").click(function(){
						window.location="${basePath!}/opera/settlementDetail/jumpMerchantTradeDetail?mid="+$(this).attr("data");
					});
				}
			});
		});

		function uncreateAmount(){
			$.ajax({
				url:"${basePath!}/opera/settlementDetail/uncreateSettlementAmount",
				type:'POST', //GET
				data:$("#searchForm").serialize(),
				timeout:30000,    //超时时间
				dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				success:function(d){
					$("#ua").html(d.data.ua);
				}
			});
		}

		function loadAmount2() {
			$.ajax({
				url: "${basePath!}/opera/settlementDetail/DAmount",
				type: 'POST', //GET
				data: $("#searchForm").serialize(),
				timeout: 30000,    //超时时间
				dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				success: function (ddd) {
					$("#DAmount").html(ddd.data.dsumamount);
					console.log(ddd);
				}
			});
		}

		function loadAmount3() {
			$.ajax({
				url: "${basePath!}/opera/settlementDetail/YAmount",
				type: 'POST', //GET
				data: $("#searchForm").serialize(),
				timeout: 30000,    //超时时间
				dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				success: function (dddd) {
					$("#YAmount").html(dddd.data.ysumamount);
					console.log(dddd);
				}
			});
		}
	});
</script>

	</body>
</html>
