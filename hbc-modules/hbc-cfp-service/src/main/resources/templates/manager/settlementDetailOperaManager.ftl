<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>结算明细</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-6">
									<h3>结算总额：¥ <span id="s">--</span> 元</h3>
								</div>
								<div class="form-group col-sm-6">
									<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">出账时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">结算状态</label>
									<div class="col-sm-9">
										<select id="status" name="status">
											<option value="99">全部</option>
											<option value="0">待确认</option>
											<option value="1">待结算</option>
											<option value="2">已结算</option>
										</select>
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="jsmx" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
						<div id="cardInfo-div" style="display:none;">
							<div class="fixed-table-container form-group pager-content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>开户行</th>
											<th>开户人</th>
											<th>预留手机</th>
											<th>身份证号</th>
											<th>卡号</th>
											<th>是否主卡</th>
											<th>操作</th>
										</tr>
									</thead>
									<c:forEach items="${cardList}" var="i">
									<tr>
										<td class="bankSn" data="${i.bankSn}"></td>
										<td>${i.name }</td>
										<td>${i.mobile }</td>
										<td>${i.idCard }</td>
										<td>${i.bankCard }</td>
										<td>${i.isDefault?"是":"否" }</td>
										<td><a href="javascript:void(0);" data="${i.id }" class="btn btn-danger btn-sm check"><i class="fa fa-check"></i> 选择此卡 </a></td>
									</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			
			$.ajax({
				url:"<%=path%>/js/bank/bank_info.json",
			    type:'POST', //GET
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    async:false,
			    success:function(data){
					$(".bankSn").each(function(i,e){
						for(var i = 0 ; i < data.length ; i++){
							if(data[i].sn == $(e).attr("data")){
								$(e).html(data[i].name);
							}
						}
					});
			    }
			});
			var l = null;
			var id = null;
			var cardId = null;
			function openSel(){
				var width = $(document).width() * 0.65;
				var height = $(document).height() * 0.3;
				l = parent.layer.open({
					type: 1,
					skin: 'layui-layer-rim', //加上边框
					area: [width+'px', height+'px'], //宽高
					content: $('#cardInfo-div').html()
				});
				$(".check", parent.document).click(function(){
					cardId = $(this).attr("data");
					parent.confirm("确定选择此结算卡进行结算吗？",function(){
						parent.layer.close(l);
						$.post('<%=path%>/settlementDetailOpera/sureSettlement.htm',{
							id:id,
							cardId:cardId
						},function(d){
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}
							parent.messageModel(d.resultMsg);
						},"json");
					});		
				});
			}
			
			
			$("#cardInfo").select();
			$("#jsmx").click(function(){
				var status = $("#status option:selected").val();
				window.location.href = "${hsj}/settlementDetailOpera/jsmx.htm?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&status="+status;
			});
			
			laydate({
				elem: '#startDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			$("#status").select();
			var $pager = $("#data").pager({
				url:"${hsj}/settlementDetailOpera/settlementDetailManagerList.htm",
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>结算单号</th>'+
								'<th>出账时间</th>'+
								'<th>结算时间</th>'+
								'<th>结算状态</th>'+
								'<th>结算卡号</th>'+
								'<th>开户行</th>'+
								'<th>结算金额</th>'+
								'<th>操作</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{serialNo}</td>'+
								'<td>{outSettlementDateStr}</td>'+
								'<td>{settlementTimeStr}</td>'+
								'<td id="status_{id}"></td>'+
								'<td>{bankCard}</td>'+
								'<td id="bankname_{id}"></td>'+
								'<td>{amount}</td>'+
								'<td id="opera_{id}"><a href="javascript:void(0);"  data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-list-ul"></i> 账单明细 </a> </td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					var binfo = null;
					$.ajax({
						url:"<%=path%>/js/bank/bank_info.json",
					    type:'POST', //GET
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    async:false,
					    success:function(data){
					    	binfo = data;
					    }
					});
					var list = result.list;
					for(var i = 0 ; i < list.length ; i++){
						if(list[i].status == 0){
							$("#status_"+list[i].id).html("待确认");
						}else if(list[i].status == 1){
							$("#status_"+list[i].id).html("待结算");
							$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm online"><i class="fa fa-arrow-circle-up"></i> 线上结算 </a> ');
							$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-primary btn-sm offline"><i class="fa fa-arrow-circle-down"></i> 线下结算 </a>');
						}else if(list[i].status == 3){
							$("#status_"+list[i].id).html("未发送");
							$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-info btn-sm send"><i class="fa fa-send"></i> 发送结算单 </a> ');
						}else{
							$("#status_"+list[i].id).html("已结算");
						}
						if(binfo != null){
							for(var b = 0 ; b < binfo.length ; b++){
								if(binfo[b].sn == list[i].bankSn) {
									$("#bankname_"+list[i].id).html(binfo[b].name);
									break;
								}
							}
						}else{
							$("#bankname_"+list[i].id).html("未知");
						}
					}
					
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					loadAmount();
	
					$(".tlog").click(function(){
						window.location="<%=path %>/settlementDetailOpera/settlementDetailLog.htm?id="+$(this).attr("data");
					});

					$(".send").click(function(){
						$.post("<%=path%>/settlementDetail/send.htm",{id:$(this).attr("data")},function(d){
							parent.messageModel(d.resultMsg);
							if(d.resultCode == "1"){
								$pager.gotoPage($pager.pageNumber);
							}
						},"json");
					});
					
					$(".offline").click(function(){
						var id = $(this).attr("data");
						parent.confirm("是否确认线下结算？",function(){
							$.post('<%=path%>/settlementDetail/offline.htm',{id:id},function(d){
								if(d.resultCode == 1){
									$pager.gotoPage($pager.pageNumber);
								}
								parent.messageModel(d.resultMsg);
							},"json");
						});
					});
					
					$(".online").click(function(){
						var id = $(this).attr("data");
						parent.layer.open({
						  type: 2,
						  skin: 'layui-layer-rim', //加上边框
						  area: ['800px', '600px'], //宽高
						  content: '<%=path%>/settlementDetail/doSettlement.htm?id='+id,
						});
					});
				}
			});
			
	
			function loadAmount(){
				$.ajax({
					url:"<%=path%>/settlementDetailOpera/totalAmount.htm",
				    type:'POST', //GET
				    data:$("#searchForm").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#uns").html(d.data.uns);
				    	$("#s").html(d.data.s);
				    }
				});
			}
			
			$("#loading-example-btn").click(function(){
				$pager.gotoPage($pager.pageNumber);
			});
		});
	
	</script>

	</body>
</html>
