<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-white doExamine" id="doExamine" type="button" value="审核" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
                    <#--<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>-->
                </ul>

                <div class="tab-content">
                    <div id="process" class="ibox-content active tab-pane">
                        <input type="hidden" name="id" value="${entity.bbid!}"/>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">审核结果</label>
                                <div class="col-sm-8">
                                    <select class="dfinput" name="bbstatus" id="bbstatus" style="width:100px;">
                                        <option value="1" <#if (((entity.bbstatus)!'') == '1')>selected="selected"</#if> >审核成功</option>
                                        <option value="0" <#if (((entity.bbstatus)!'') == '0')>selected="selected"</#if> >审核失败</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label"></label>
                                <label class="col-sm-8 control-label" style="text-align: left;">

                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div class="pass process">
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">商业授信额度</label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control" id="bauthmax" readonly name="bauthmax" value="${entity.bauthmax!}" maxlength="30">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed pass process" style="clear:both;"></div>
                        <div class="pass process">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">报名时间</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="bcreatedate" readonly name="bcreatedate" value="${entity.bcreatedate!}" maxlength="30">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed pass process" style="clear:both;"></div>


                        <div class="pass process">
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">审批人</label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control" readonly id="approverName" name="approverName" value="${approverName!}" maxlength="30">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed pass process" style="clear:both;"></div>
                        <div class="pass process">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">审批人联系方式</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="bbmobile" readonly name="bbmobile" value="${bbmobile!}" maxlength="30">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed pass process" style="clear:both;"></div>


                        <div class="pass process">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">最后给予授信额度(不能大于授信额度)</label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control" id="endMoeny" name="endMoeny" value="${entity.endMoeny!}" maxlength="30">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed pass process" style="clear:both;"></div>

                    <!-- 商户审核信息 -->
                    <div id="base" class="ibox-content tab-pane">
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">店铺简称</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.simpleName!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">店铺类型</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.mname!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">店铺电话</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.mobile!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">店铺介绍</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.recommendGoods!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">开店时间</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.openDate!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">位置</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.address!}
                                </label>
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">详细地址</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
								${entity.address!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label"></label>
                                <label class="col-sm-8 control-label" style="text-align: left;">

                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">地图位置</label>
                                <div class="col-sm-10" id="container" style="height:350px;">

                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">门店照片</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                    <img src="${entity.localPhoto!}" id="front"/>
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">营业执照</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                    <img src="${entity.licensePic!}" id="back"/>
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
    <script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>
    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
    <script>

        $("#endMoeny").on('keyup', function (event) {
            var $amountInput = $(this);
            //响应鼠标事件，允许左右方向键移动
            event = window.event || event;
            if (event.keyCode == 37 | event.keyCode == 39) {
                return;
            }
            //先把非数字的都替换掉，除了数字和.
            $amountInput.val($amountInput.val().replace(/[^\d.]/g, "").
            //只允许一个小数点
            replace(/^\./g, "").replace(/\.{2,}/g, ".").
            //只能输入小数点后两位
            replace(".", "$#$").replace(/\./g, "").replace("$#$", ".").replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'));
        });
        $("#endMoeny").on('blur', function () {
            var $amountInput = $(this);
            //最后一位是小数点的话，移除
            $amountInput.val(($amountInput.val().replace(/\.$/g, "")));
        });


        $(document).ready(function () {
            var lat;
            var lng;
            var sellat;
            var sellng;
            <#if (entity.lat)?? >
                  lat =${entity.lat!}
			<#else>
					lat = "39.90923"
			</#if>

           	<#if (entity.lng)??  >
				lng = ${entity.lng!}
			<#else >
				lng = "116.397428"
			</#if>

			<#if (entity.lat)??>
				sellat=${entity.lat!}
			<#else >
				sellat =''
			</#if>

			<#if (entity.lng)??>
				sellng=${entity.lng!}
			<#else >
			 sellng=''
			</#if>

            map = new AMap.Map('container', {
                center: [lng, lat],
                zoom: 13
            });
            map.plugin(["AMap.ToolBar"], function() {
                map.addControl(new AMap.ToolBar());
            });

            var _onClick = function(e){
                map.clearMap();

                sellat = e.lnglat.lat;
                sellng = e.lnglat.lng;
                new AMap.Marker({
                    position : e.lnglat,
                    map : map
                });
            };
            map.on('click', _onClick);//绑定

            if(sellat != "" && sellat != ""){
                new AMap.Marker({
                    position : new AMap.LngLat(sellng,sellat),
                    map : map
                });
            }

            $("#shareFlag").select();
            var bbstatus=$("#bbstatus").select();
            var bbstatus = 'pass';
            $("#front").css('width','100%');
            $("#front").css('height',$(document).width()/6);
            $("#front").click(function(){
                window.open($(this).attr('src'));
            })
            $("#back").css('width','100%');
            $("#back").css('height',$(document).width()/6);
            $("#back").click(function(){
                window.open($(this).attr('src'));
            });
            $("#doExamine").click(function(){
                var data = {};
                    var bbstatus = $.trim($("#bbstatus").val());
                    var approverName = $.trim($("#approverName").val());
                    var bbmobile = $.trim($("#bbmobile").val());
                    var endMoeny = $.trim($("#endMoeny").val());
                    var sxed=${entity.bauthmax!};
                    if(sxed<endMoeny){
                        layer.msg("不能大于商业授信额度！");
                        return;
                    }
                    data.bbstatus = bbstatus;
                    data.approverName = approverName;
                    data.bbmobile= bbmobile;
                    data.endMoeny=endMoeny;
                    data.id = '${entity.bbid!}';
                $.post('${basePath}/opera/merchantInfo/creditaudit?bid=${entity.bbid!}',data,function(d){
                    if(d.code ==0){
                        parent.messageModel(d.msg);
                        parent.c.gotoPage(null);
                        parent.closeLayer();
                    }
                },'json');
            });

            $(".cancel").click(function(){parent.closeLayer();});
        });
    </script>
</body>

</html>
