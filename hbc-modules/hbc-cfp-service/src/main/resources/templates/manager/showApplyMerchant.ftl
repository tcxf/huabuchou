<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">
    <style>
        .money_a{
			display: none;

		}
        #minmoney{
            height: 20px;
        }
	</style>
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white" id="doExamine" type="button" value="审核" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
					<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="process" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${id!}"/>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">审核结果</label>
								<div class="col-sm-8">
									<select class="dfinput" name="authExamine" id="ret" style="width:100px;">
                                        <option value="3" <#if (((entity.authExamine)!'') == '3')>selected="selected"</#if> >审核成功</option>
                                        <option value="2" <#if (((entity.authExamine)!'') == '2')>selected="selected"</#if> >审核失败</option>
									</select>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div class="pass process">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">让利利率（%）</label>
									<div class="col-sm-8">
										<input type="text"  validate-rule="required,num" validate-msg="required:让利利率不能为空" class="form-control" id="shareFee" name="shareFee" value="${entity.shareFee!?c}" maxlength="30" placeholder="请填写让利利率">
									</div>
							</div>
						</div>
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						<div class="pass process">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">结算方式</label>
								<div class="col-sm-10" style="line-height: 40px">
									<div class="fangshi">
                                        <input type="radio" name="fangshi" id="time_a" checked>
                                        <label for="">按时间结算</label>
										&nbsp&nbsp&nbsp&nbsp
                                        <input type="radio" name="fangshi" id="money_a">
                                        <label for="">按金额结算</label>
									</div>
									<div>
                                        <div class="time_a">
                                            <select name="settlementLoop" id="settlementLoop" value="${entity.settlementLoop!}">
                                                <option value="0" <#if (((entity.settlementLoop)!'') == '0')>selected="selected"</#if> >请选择</option>
                                                <option value="1" <#if (((entity.settlementLoop)!'') == '1')>selected="selected"</#if> >日结</option>
                                                <option value="2" <#if (((entity.settlementLoop)!'') == '2')>selected="selected"</#if> >周结</option>
                                                <option value="7" <#if (((entity.settlementLoop)!'') == '7')>selected="selected"</#if> >半月</option>
                                                <option value="3" <#if (((entity.settlementLoop)!'') == '3')>selected="selected"</#if> >月结</option>
                                                <option value="4" <#if (((entity.settlementLoop)!'') == '4')>selected="selected"</#if> >季结</option>
                                                <option value="5" <#if (((entity.settlementLoop)!'') == '5')>selected="selected"</#if> >半年</option>
                                                <option value="6" <#if (((entity.settlementLoop)!'') == '6')>selected="selected"</#if> >一年</option>
                                                <#--<option value="8" <#if (((entity.settlementLoop)!'') == '8')>selected="selected"</#if> >额度结算</option>-->
                                            </select>
                                        </div>
                                        <div class="money_a">
                                           <div> <label for="">最低结算金额 (元)</label>
                                                 <input type="text" id="minmoney">
										   </div>
                                            <div> <label for="">最迟结算时间 (天)</label>
                                                <select name="latestday" id="latestday" value="${entity.latestday!}">
                                                    <option value="0" <#if (((entity.latestday)!'') == '0')>selected="selected"</#if> >请选择</option>
                                                    <option value="15" <#if (((entity.latestday)!'') == '15')>selected="selected"</#if> >15天</option>
                                                    <option value="30" <#if (((entity.latestday)!'') == '30')>selected="selected"</#if> >30天</option>
                                                    <option value="45" <#if (((entity.latestday)!'') == '45')>selected="selected"</#if> >45天</option>
                                                    <option value="60" <#if (((entity.latestday)!'') == '60')>selected="selected"</#if> >60天</option>
                                                    <option value="90" <#if (((entity.latestday)!'') == '90')>selected="selected"</#if> >90天</option>
                                                </select>
											</div>
											<p>当商家钱包金额不足最低结算金额时，最晚可在多少天之后允许结算</p>
                                        </div>
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed pass process" style="clear:both;"></div>
						
						
						
						<div class="nopass nopass process" style="display:none;">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">失败原因</label>
								<div class="col-sm-10">
									<textarea id="reason" rows="10" style="width:80%" name="examineFailReason"></textarea>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed nopass process" style="clear:both;display:none;"></div>
					</div>
					<!-- 商户审核信息 -->
					<div id="base" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mname!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mobile!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺介绍</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.recommendGoods!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">开店时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.openDate!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">位置</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address!}
								</label>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">地图位置</label>
								<div class="col-sm-10" id="container" style="height:350px;">

								</div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">门店照片</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${entity.localPhoto!}" id="front"/>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">营业执照</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${entity.licensePic!}" id="back"/>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

        <!-- 全局js -->
        <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
        <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

        <!-- 自定义js -->
        <script src="${basePath!}/js/content.js?v=1.0.0"></script>
        <script type="text/javascript" src="${basePath!}/js/common.js"></script>
        <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
        <!-- iCheck -->
        <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
        <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
        <script type="text/javascript" src="${basePath!}/js/pager.js"></script>
        <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script>
		$(document).ready(function () {
		    $("select").select();
            var lat;
            var lng;
            var sellat;
            var sellng;
            <#if (entity.lat)?? >
                  lat =${entity.lat!}
			<#else>
					lat = "39.90923"
			</#if>

           	<#if (entity.lng)??  >
				lng = ${entity.lng!}
			<#else >
				lng = "116.397428"
			</#if>

			<#if (entity.lat)??>
				sellat=${entity.lat!}
			<#else >
				sellat =''
			</#if>

			<#if (entity.lng)??>
				sellng=${entity.lng!}
			<#else >
			 sellng=''
			</#if>

            map = new AMap.Map('container', {
                center: [lng, lat],
                zoom: 13
            });
            map.plugin(["AMap.ToolBar"], function() {
                map.addControl(new AMap.ToolBar());
            });

            var _onClick = function(e){
                map.clearMap();

                sellat = e.lnglat.lat;
                sellng = e.lnglat.lng;
                new AMap.Marker({
                    position : e.lnglat,
                    map : map
                });
            };
            map.on('click', _onClick);//绑定

            if(sellat != "" && sellat != ""){
                new AMap.Marker({
                    position : new AMap.LngLat(sellng,sellat),
                    map : map
                });
            }

			$("#shareFlag").select();
			var ret=$("#ret").select();
			var ret = 'pass';
			$("#ret").change(function(){
				$(".process").css("display","block");
				$("."+$(this).val()).show();
				ret = $(this).val();
               if(ret==3){
                   $(".nopass").css("display","none");
			   }
			   if(ret==2){
                   $(".pass").css("display","none");
			   }
			});

			$("#settlementLoop").select();
			$("#front").css('width','100%');
			$("#front").css('height',$(document).width()/6);
			$("#front").click(function(){
				window.open($(this).attr('src'));
			})
			$("#back").css('width','100%');
			$("#back").css('height',$(document).width()/6);
			$("#back").click(function(){
				window.open($(this).attr('src'));
			});
            $("#doExamine").click(function(){
                var data = {};
                var settlementLoop= $.trim($("#settlementLoop").val());
                var reason= $.trim($("#reason").val());
                var ret = $.trim($("#ret").val());
                var shareFee = $.trim($("#shareFee").val());
                var minmoney = $.trim($("#minmoney").val());
                var latestday = $.trim($("#latestday").val());
                if(settlementLoop!=0 && minmoney == 0 && latestday ==0){

                }else if(minmoney != 0 && latestday !=0){

                }else {
                    layer.msg("你可以选择按时间结算（选择结算时间），或者填写最低结算金额不能低于0元,结算时间不能低于15天");
                    return;
                }
                data.settlementLoop = settlementLoop;
                data.ret= ret;
                data.reason= reason;
                data.shareFee=shareFee;
                data.minmoney=minmoney;
                data.latestday=latestday;
                data.id = '${entity.bbid!}';
                $.post('${basePath}/opera/merchantInfo/doExamine?mid=${id!}',data,function(d){
                    if(d.code ==0){
                        parent.messageModel(d.msg);
                        parent.c.gotoPage(null);
                        parent.closeLayer();
                    }
                },'json');
            });
			$(".cancel").click(function(){parent.closeLayer();});
			$("#money_a").click(function () {
				$(".time_a").css("display","none");
                $(".money_a").css("display","block");
            });
            $("#time_a").click(function () {
                $(".time_a").css("display","block");
                $(".money_a").css("display","none");
            });

            $("#minmoney").on('keyup', function (event) {
                var $amountInput = $(this);
                //响应鼠标事件，允许左右方向键移动
                event = window.event || event;
                if (event.keyCode == 37 | event.keyCode == 39) {
                    return;
                }
                //先把非数字的都替换掉，除了数字和.
                $amountInput.val($amountInput.val().replace(/[^\d.]/g, "").
                //只允许一个小数点
                replace(/^\./g, "").replace(/\.{2,}/g, ".").
                //只能输入小数点后两位
                replace(".", "$#$").replace(/\./g, "").replace("$#$", ".").replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'));
            });
            $("#minmoney").on('blur', function () {
                var $amountInput = $(this);
                //最后一位是小数点的话，移除
                $amountInput.val(($amountInput.val().replace(/\.$/g, "")));
            });

		});
	</script>
</body>

</html>
