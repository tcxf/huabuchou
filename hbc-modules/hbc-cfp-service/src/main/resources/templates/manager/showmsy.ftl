<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
	 <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white doExamine" id="doExamine" type="button" value="审核" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消审核" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#process" aria-expanded="true"> 审核操作</a></li>
					<li><a data-toggle="tab" href="#base" aria-expanded="true"> 基本资料</a></li>
					<li><a data-toggle="tab" href="#grgs" aria-expanded="true"> 个人工商信息</a></li>
					<li><a data-toggle="tab" href="#dtjd" aria-expanded="true"> 多头借贷</a></li>
					<li><a data-toggle="tab" href="#yhk" aria-expanded="true"> 银行卡</a></li>
				</ul>
				
				<div class="tab-content">
					<div id="process" class="ibox-content active tab-pane">
						<input type="hidden" name="id" id="mid" value="${entity.id!}"/>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">审核结果</label>
								<div class="col-sm-8">
                                        <select class="dfinput" name="authExamine" id="ret" style="width:100px;">
                                            <option value="0" <#if (((entity.authExamine)!'') == '0')>selected="selected"</#if> >未提交</option>
                                            <option value="1" <#if (((entity.authExamine)!'') == '1')>selected="selected"</#if> >待审核</option>
                                            <option value="2" <#if (((entity.authExamine)!'') == '2')>selected="selected"</#if> >审核失败</option>
                                            <option value="3" <#if (((entity.authExamine)!'') == '3')>selected="selected"</#if> >审核成功</option>
                                        </select>
								</div>
							</div>
							
								<div class="col-sm-6">
								<label class="col-sm-4 control-label">选择资金端</label>
							
								<select id="hStatus"   lass="dfinput" style="width:100px;">

								</select>
								
							</div>
							
						<div class="hr-line-dashed" style="clear:both;"></div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">授信综合额度</label>
								<div class="col-sm-8">
									<input type="text" id="max" value="${entity.authMax!}">
								</div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
			
				
						<div class="hr-line-dashed pass process" style="clear:both;"></div>



						<div class="nopass nopass process" style="display:none;">
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">原因</label>
								<div class="col-sm-10">
									<textarea id="reason" rows="10" style="width:80%"></textarea>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed nopass process" style="clear:both;display:none;"></div>
					</div>
					
					<!-- 商户审核信息 -->
					<div id="base" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.simpleName!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.mcName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺电话</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.phoneNumber!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">店铺介绍</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.recommendGoods!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">开店时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.openDate!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">位置</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.displayName!}
								</label>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.address!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"></label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">地图位置</label>
								<div class="col-sm-10" id="container" style="height:350px;">
									
								</div>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">门店照片</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${entity.localPhoto!}" id="front"/>
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">营业执照</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<img src="${entity.licensePic!}" id="back"/>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script type="text/javascript" src="${basePath!}/js/improveQuotaMerchant.js"></script>
	<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=d9704fc9a843b5ad72123799af10f01c"></script>
	<script>
		$(document).ready(function () {
			findfund();
            <#--var lat = "${(entity.lat == null || entity.lat == '')?'39.90923':entity.lat}";-->
            <#--var lng = "${(entity.lng == null || entity.lng == '')?'116.397428':entity.lng}";-->
            <#--var sellat = "${(entity.lat == null || entity.lat == '')?'':entity.lat}";-->
            <#--var sellng = "${(entity.lng == null || entity.lng == '')?'':entity.lng}";-->
            var lat;
			var lng;
			var sellat;
		     var sellng;
            <#if (entity.lat)?? >
                  lat =${entity.lat!}
			<#else>
					lat = "39.90923"
            </#if>

           	<#if (entity.lng)??  >
				lng = ${entity.lng!}
			<#else >
				lng = "116.397428"
           	</#if>

			<#if (entity.lat)??>
				sellat=${entity.lat!}
			<#else >
				sellat =''
			</#if>

			<#if (entity.lng)??>
				sellng=${entity.lng!}
				<#else >
			 sellng=''
			</#if>

			map = new AMap.Map('container', {
	            center: [lng, lat],
	            zoom: 13
	        });
	        map.plugin(["AMap.ToolBar"], function() {
	            map.addControl(new AMap.ToolBar());
	        });
	        
	        var _onClick = function(e){
	        	map.clearMap();
	        	
	        	sellat = e.lnglat.lat;
	        	sellng = e.lnglat.lng;
	            new AMap.Marker({
	                position : e.lnglat,
	                map : map
	            });
	        };
	        map.on('click', _onClick);//绑定
	        
	        if(sellat != "" && sellat != ""){
				new AMap.Marker({
	                position : new AMap.LngLat(sellng,sellat),
	                map : map
	            });
	        }
			
			$("#shareFlag").select();
			$("#ret").select();
			var ret = 'pass';
			$("#ret").change(function(){
				$(".process").hide();
				$("."+$(this).val()).show();
				ret = $(this).val();
			});
			$("#settlementLoop").select();
			$("#front").css('width','100%');
			$("#front").css('height',$(document).width()/6);
			$("#front").click(function(){
				window.open($(this).attr('src'));
			})
			$("#back").css('width','100%');
			$("#back").css('height',$(document).width()/6);
			$("#back").click(function(){
				window.open($(this).attr('src'));
			});
		 	$(".doExamine").click(function(){
				var data = {};
				if($("#ret").val()=='pass'){
					var fid = $.trim($("#hStatus").val());
					data.fid = fid;
				} 
				
				if($("#ret").val()=='nopass'){
					var reason = $.trim($("#reason").val());
					if(reason.length < 10){
						layer.alert('失败原因至少需要10个字');
						$("#reason").focus();
						return;
					}
					if(reason.length > 100){
						layer.alert('失败原因最多只能100个字');
						$("#reason").focus();
						return;
					}
					if(!isNaN(reason)){
						layer.alert('失败原因不能为数字');
						$("#reason").focus();
						return;
					}
					data.reason = reason;
				}
				var max = $("#max").val();
				data.ret = ret;
				data.lat = sellat;
				data.lng = sellng;
				data.id = '${id}';
				data.max = max;
				$.post('${basepath!}/merchantInfo/sydoExamine',data,function(d){
					parent.messageModel(d.resultMsg);
					if(d.resultCode != -1){
						parent.closeLayer();
						parent.c.gotoPage(null);
					}
				},'json');
			});
			
			$(".cancel").click(function(){parent.closeLayer();});
			
			function findfund(){
				$.ajax({
					url:"${basePath}//merchantInfo/findfund?oid=${id!}",
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:{
						
					},
					success: function(d){ 
						  $("#hStatus").html("");
							for(var i = 0 ; i < d.data.length ; i++){
								
								$tr =$('<option value='+d.data[i].id+'>'+d.data[i].fname+'</option>')	;	
							
							$("#hStatus").append($tr);
						  }
			}
					}); 
			}
			
		});
	</script>
</body>

</html>
