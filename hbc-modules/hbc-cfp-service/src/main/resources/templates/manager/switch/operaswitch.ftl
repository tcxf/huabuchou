<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">
    <title>选择</title>
    <style>
        body{
            background: #f5f7f5;
        }
        .change_btn{
            width: 96%;
            height: 400px;
            background: #fff;
            margin-left: 3%;
            margin-top: 20px;
        }
        .sx_manager{
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #f5f7f5;
            border: 1px solid #ededed;
            padding-left: 10px;
        }
        .change_btn>ul{
            padding: 0;
            margin-top: 20px;
        }
        .change_btn .ul>li{
            list-style: none;
            margin-bottom: 10px;
            height: 40px;
            line-height: 40px;
        }
    </style>
</head>
<body>
<div class="change_btn">
    <div class="sx_manager">外地授信管理</div>
    <ul class="ul">
        <li>
           <div class="container">
               <div class="row">
                   <div class="col-sm-2">
                       外地手机号不可授信
                   </div>
                   <div class="col-sm-4">
                       <select name="phoneState" id="phone_switch"  value="${cOperaSwitch.phoneState!}">
                           <option value="1" <#if (((cOperaSwitch.phoneState)!'') == '1')>selected="selected"</#if> >开启</option>
                           <option value="0" <#if (((cOperaSwitch.phoneState)!'') == '0')>selected="selected"</#if> >关闭</option>
                       </select>
                   </div>
               </div>
           </div>

        </li>
        <li>
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        外地IP不可授信
                    </div>
                    <div class="col-sm-4">
                        <select name="ipState" id="ip_switch" value="${cOperaSwitch.ipState!}">
                            <option value="1" <#if (((cOperaSwitch.ipState)!'') == '1')>selected="selected"</#if> >开启</option>
                            <option value="0" <#if (((cOperaSwitch.ipState)!'') == '0')>selected="selected"</#if> >关闭</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        外地户籍不可授信
                    </div>
                    <div class="col-sm-4">
                        <select name="idcardState" id="idCard_switch" value="${cOperaSwitch.idcardState!}">
                            <option value="1" <#if (((cOperaSwitch.idcardState)!'') == '1')>selected="selected"</#if> >开启</option>
                            <option value="0" <#if (((cOperaSwitch.idcardState)!'') == '0')>selected="selected"</#if> >关闭</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->

<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<!-- iCheck -->


<script>
    $(document).ready(function (){
        $("select").select();

        $("#phone_switch").change(function () {
            var  phoneState= $("#phone_switch").val();

            layer.confirm('确定'+(phoneState==1?'开启外地手机号不可授信':'关闭外地手机号不可授信')+'吗？', {icon: 3, title:'提示',yes: function(index){
                    $.ajax({
                        url : "${basePath!}/opera/switch/updateSwitchState?phoneState="+phoneState,
                        type : "POST",
                        success:function(data) {
                            window.location.reload();
                        }
                    });
                    layer.close(index);
                },
                cancel: function(index, layero){
                    layer.close(index);
                    location.reload();// 可以在这里刷新窗口
                }
            });
        })

        $("#ip_switch").change(function () {

            var  ipState= $("#ip_switch").val();

            layer.confirm('确定'+(ipState==1?'开启外地IP不可授信':'关闭外地IP不可授信')+'吗？', {icon: 3, title:'提示',yes: function(index){
                $.ajax({
                    url: "${basePath!}/opera/switch/updateSwitchState?ipState=" + ipState,
                    type: "POST",
                    success: function (d) {
                        window.location.reload();
                    }
                });
                    layer.close(index);
            },
                cancel: function(index, layero){
                    layer.close(index);
                    location.reload();// 可以在这里刷新窗口
                }

            });
        })

        $("#idCard_switch").change(function () {
            var  idcardState= $("#idCard_switch").val();

            layer.confirm('确定'+(idcardState==1?'开启外地户籍不可授信':'关闭外地户籍不可授信')+'吗？', {icon: 3, title:'提示',yes: function(index) {
                    $.ajax({
                        url: "${basePath!}/opera/switch/updateSwitchState?idcardState=" + idcardState,
                        type: "POST",
                        success: function (d) {
                            window.location.reload();
                        }
                    });
                    layer.close(index);
            },
                cancel: function(index, layero){
                    layer.close(index);
                    location.reload();// 可以在这里刷新窗口
                }
            });
        })


    });

</script>

</body>
</html>