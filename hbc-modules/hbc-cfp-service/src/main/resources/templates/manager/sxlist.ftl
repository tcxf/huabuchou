<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<%=path %>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=path %>/css/app.css" />
    <link rel="stylesheet" href="<%=path %>/css/y.css"/>
    <link rel="stylesheet" href="<%=path %>/js/mui/css/mui.min.css" />
    <script src="<%=path %>/js/html5shiv.min.js"></script>
    <script src="<%=path %>/js/respond.min.js"></script>
    <title></title>
    <style type="text/css">
        .table{
            margin-bottom: 0;
        }
        .table>tbody>tr>td{
            padding: 0;
            line-height: 1.42857143;
            vertical-align: middle;
            border-top:none;
        }
        .thd{
            text-align: center;
        }
        .tbd tr{
            text-align: center;
        }
    </style>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title" style="text-align: center">
            <a href="#cc1" data-toggle="collapse">
                <img src="<%=path %>/img/logo_down.png" style="width:25px;height:22px" alt=""/>
            </a>
        </h4>
    </div>
    <div class="panel-collapse collapse" id="cc1">
        <div class="panel-body">
            <table border="1" class="table">
                <thead class="thd">
                <tr>
                    <td>项目</td>
                    <td>满分</td>
                    <td>客户情况</td>
                    <td>标准分</td>
                </tr>
                </thead>
                <tbody class="tbd">
                <tr>
                    <td rowspan="5">年龄</td>
                    <td rowspan="5">10</td>
                    <td><25</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>25-35</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>35-45</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>45-60</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>>60</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td rowspan="2">性别</td>
                    <td rowspan="2">2</td>
                    <td>男</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>女</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td rowspan="2">婚姻状况</td>
                    <td rowspan="2">5</td>
                    <td>已婚</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>未婚</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td rowspan="5">文化程度</td>
                    <td rowspan="5">15</td>
                    <td>高中以下</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>中专</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>大专</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>本科</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>研究生以上</td>
                    <td>15</td>
                </tr>
                <tr>
                    <td rowspan="5">居住时间</td>
                    <td rowspan="5">10</td>
                    <td>1年以下</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>1-3年</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>3-5年</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>5-10年</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>10年以上</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td rowspan="6">单位性质</td>
                    <td rowspan="6">10</td>
                    <td>无单位</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>机关单位</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>事业单位</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>上市公司</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>外企</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>民营企业</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td rowspan="5">工作时长</td>
                    <td rowspan="5">10</td>
                    <td>1年以内</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>1-3年</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>3-5年</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>5-10年</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>10年以上</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td rowspan="5">职位</td>
                    <td rowspan="5">10</td>
                    <td>普通员工</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>基层管理</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>中层管理</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>高层管理</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>企业主</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td rowspan="9">月薪</td>
                    <td rowspan="9">18</td>
                    <td>2000以下</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>2-3000</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>3-4500</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>4500-6000</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td>6k-8k</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>8k-10k</td>
                    <td>12</td>
                </tr>
                <tr>
                    <td>10k-15k</td>
                    <td>14</td>
                </tr>
                <tr>
                    <td>15k-20k</td>
                    <td>16</td>
                </tr>
                <tr>
                    <td>20k以上</td>
                    <td>18</td>
                </tr>
                <tr>
                    <td rowspan="3">车辆归属</td>
                    <td rowspan="3">5</td>
                    <td>本人</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>配偶</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>其他</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td rowspan="3">房产归属</td>
                    <td rowspan="3">5</td>
                    <td>本人</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>配偶</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>其他</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table>
            <table border="1" class="table">
                <thead class="thd">
                    <tr>
                        <td>得分区间(x)</td>
                        <td>x>90</td>
                        <td> 80< x ≤90 </td>
                        <td> 70< x ≤80 </td>
                        <td> 60< x ≤70 </td>
                        <td> 50< x ≤60 </td>
                        <td> 40< x ≤50 </td>
                        <td>  x ≤40 </td>
                    </tr>
                <tbody class="tbd">
                    <tr>
                        <td>等级</td>
                        <td>AAA</td>
                        <td>AA</td>
                        <td>A</td>
                        <td>BBB</td>
                        <td>BB</td>
                        <td>B</td>
                        <td>C</td>
                    </tr>
                </tbody>
                </thead>
            </table>
        </div>
    </div>
</div>

<script src="<%=path %>/js/jquery-1.11.3.js"></script>
<script src="<%=path %>/js/bootstrap.min.js"></script>
</body>
</html>