<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

	<#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>未出账单</h5>
                </div>
                <div class="ibox-content">
                    <form id="searchForm" class="form-horizontal">

                        <div>
                            <div class="form-group col-sm-12">
                                <h3>未出总金额：¥ <span id="totalAmount">--</span>元</h3>
                            </div>
                        </div>

                        <div>
                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">用户姓名</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="realName" maxlength="30" placeholder="请填写用户姓名">
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-lg-4">
                                <label class="col-sm-3 control-label">用户手机</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="telPhone" maxlength="30" placeholder="请填写用户手机">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                <input class="btn btn-success" id="search" type="button" value=" 查 询 " />
                                <input id="weichu" class="btn btn-info" type="button" value=" 下 载 报 表 " />
                            </div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                    <div class="project-list pager-list" id="data"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        //下载报表
        $("#weichu").click(function(){
            $("#searchForm").attr("action","${basePath!}/opera/o_payback/noBillPoi");
            $("#searchForm").submit();
        });

        var $pager = $("#data").pager({
            url:"${basePath!}/opera/o_payback/noBill",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>流水单号</th>'+
                '<th>用户名称</th>'+
                '<th>用户手机</th>'+
                '<th>用户类型</th>'+
                '<th>交易金额</th>'+
                '<th>所属运营商</th>'+
                '<th>所属资金端</th>'+
                '<th>交易时间</th>'+
                '<th>操作</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{serialNo}</td>'+
                '<td>{realName}</td>'+
                '<td>{telPhone}</td>'+
                '<td>{utype}</td>'+
                '<td>{actualAmount}</td>'+
                '<td>{oname}</td>'+
                '<td>{fname}</td>'+
                '<td>{tradingDate}</td>'+
                '<td><a href="javascript:void(0);" data="{id}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查询明细 </a></td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });

                $(".mx").click(function(){
                    window.location.href="${basePath!}/opera/o_payback/jumpNoBillDetail?id="+$(this).attr("data");
                });

                loadAmount();//加载未出账单金额
            }
        });

        function loadAmount(){
            $.ajax({
                url:"${basePath!}/opera/o_payback/noBillTotalMoney",
                type:'POST', //GET
                data:$("#searchForm").serialize(),
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    $("#totalAmount").html(d.data.totalMoney);
                }
            });
        }

        $("#loading-example-btn").click(function(){
            $pager.gotoPage($pager.pageNumber);
        });
    });

</script>

</body>
</html>
