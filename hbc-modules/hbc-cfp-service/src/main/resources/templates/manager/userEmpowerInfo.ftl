<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 报告信息</a></li>
					<li><a data-toggle="tab" href="#examine" aria-expanded="true"> 信息概要</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 信用卡</a></li>
					<li><a data-toggle="tab" href="#df" aria-expanded="true"> 购房贷款</a></li>
					<li><a data-toggle="tab" href="#gg" aria-expanded="true"> 公共记录</a></li>
					<li><a data-toggle="tab" href="#jg" aria-expanded="true"> 机构查询记录明细</a></li>
					<li><a data-toggle="tab" href="#gr" aria-expanded="true"> 个人查询记录明细</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value=""/>
						<div>
						  	<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">报告编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.bgId }${report }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">查询时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.queryTime }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.name }
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">证件类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.cardType }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">证件号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.cardNo }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">婚姻状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${entity.isMarried }
								</label>
							</div>
						</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
			
					
					<!-- 信息概要 -->
					<div id="examine" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label"></label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">信用卡</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">购房贷款</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">其他贷款</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">账户数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${accountNumber.e }</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${accountNumber.s }</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${accountNumber.t }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div><div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">未结清/未注销账户</label>
							</div>
							<<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayAccountNumber.e }</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayAccountNumber.s }</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayAccountNumber.t }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">发生过逾期的账户数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayOver90DayAccountNumber.e }</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayOver90DayAccountNumber.s }</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${delayOver90DayAccountNumber.t }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">发生过90天以上逾期的账户数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${guaranteeQuantity.e }</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${guaranteeQuantity.s }</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${guaranteeQuantity.t }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">为他人担保的笔数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${unSettlement.e }</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${unSettlement.s }</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${unSettlement.t }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					<!-- 信用卡 -->
					<div id="settlement" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">从未逾期过的贷记卡及透支卡未超过60天的准贷记卡账户明细如下</label>
								
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-20 control-label">
									${entity.creditCardInfo }
								</label>	
							</div>
						</div>
					</div>
					<!-- 购房贷款 -->
					<div id="df" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">从未逾期过的账户明细如下</label>
								
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-20 control-label">${entity.purchaseLoanInfo }</label>
								
							</div>
						</div>
					</div>
					<!-- 公共记录 -->
					<div id="gg" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">从未逾期过的账户明细如下</label>
								
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-20 control-label">${entity.publishRecord }</label>
								
							</div>
						</div>
					</div>
					<!-- 机构查询记录明细 -->
					<div id="jg" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-10">
								<label class="col-sm-2 control-label">查询日期</label>
								<label class="col-sm-2 control-label">查询操作员</label>
								<label class="col-sm-2 control-label">查询原因</label>
								
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<c:forEach items="${elist }" var="e" >
							<div>
								<div class="form-group col-sm-10">
									<label class="col-sm-2 control-label">${e.e }</label>
									<label class="col-sm-2 control-label">${e.s }</label>
									<label class="col-sm-2 control-label">${e.t }</label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
						</c:forEach>
						
					</div>
						<!-- 个人查询记录明细 -->
					<div id="gr" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-10">
								<label class="col-sm-2 control-label">查询日期</label>
								<label class="col-sm-2 control-label">查询操作员</label>
								<label class="col-sm-2 control-label">查询原因</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<c:forEach items="${slist }" var="e" >
							<div>
								<div class="form-group col-sm-10">
									<label class="col-sm-2 control-label">${e.e }</label>
									<label class="col-sm-2 control-label">${e.s }</label>
									<label class="col-sm-2 control-label">${e.t }</label>
								</div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	
</body>

</html>
