<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
<#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#auth" aria-expanded="true"> 授信信息</a></li>
                    <li><a data-toggle="tab" href="#jyjl" aria-expanded="true"> 交易记录</a></li>
                    <li><a data-toggle="tab" href="#hkjl" aria-expanded="true"> 还款记录</a></li>
                    <li><a data-toggle="tab" href="#yqjl" aria-expanded="true"> 逾期记录</a></li>
                </ul>

                <div class="tab-content">
                    <div id="base" class="ibox-content active tab-pane">
                        <div class="form-group col-sm-6">
                            <label class="col-sm-2 control-label"> 会员ID</label>
                            <label id="unum" class="col-sm-10 control-label" style="text-align: left;">
                            ${entity.id!}
                            </label>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>

                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">会员姓名</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.realName!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">手机号码</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.mobile!}
                                </label>
                            </div>
                        </div>

                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">商户名称</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.mname!}
                                </label>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">详细地址</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.address!}
                                </label>
                            </div>
                        </div>

                        <div class="hr-line-dashed" style="clear:both"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">所属地区</label>
                            <div class="col-sm-10" id="area" data-name="area" data-value="${entity.area!}"></div>
                        </div>
                    </div>
                    <!-- 商户审核信息 -->
                    <div id="auth" class="ibox-content tab-pane">
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">额度状态</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.authStatus!}
                                </label>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">快速授信额度</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.authMax!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">出账日期</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.ooaDate!}
                                </label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">还款日期</label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                ${entity.repayDate!}
                                </label>
                            </div>
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                    </div>
                    <div id="jyjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="jyjl-form" class="form-horizontal">
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div>
                                    <div class="form-group col-sm-12">
                                        <h3>交易总额：¥ <span id="totalJyjlAmount">--</span> 元</h3>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">交易时间</label>
                                        <div class="col-sm-9">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="startDate" id="startDateJYJL" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="endDate" id="endDateJYJL" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">商户名称</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="mname" id="mname" maxlength="30" placeholder="请填写商户名称">
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">用户名称</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" id="name" maxlength="30" placeholder="请填写用户名称">
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                        <input class="btn btn-success" id="jyjl-search" type="button" value=" 查 询 " />
                                        <a href="${basePath!}/platfrom/user/tradingPoi"><input id="" class="btn btn-info" type="button" value=" 下 载 报 表 " /></a>
                                    </div>
                                </div>
                            </form>
                            <div class="project-list" id="jyjl-list"></div>
                        </div>
                    </div>
                    <div id="hkjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="hkjl-form" class="form-inline">
                                <input type="hidden" name="status" value="1"/>
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div>
                                    <div class="form-group col-sm-4">
                                        <h3>已还总计：¥ <span id="yhAmount">--</span> 元</h3>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <h3>待还总计：¥ <span id="whAmount">--</span> 元</h3>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <h3>逾期总计：¥ <span id="yqAmount">--</span> 元</h3>
                                    </div>
                                </div>
                                <div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">还款状态</label>
                                        <div class="col-sm-9">
                                            <select id="repayType" name="repayType">
                                                <option value="">全部</option>
                                                <option value="1">已还</option>
                                                <option value="2">未还</option>
                                                <option value="3">逾期待还</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <input class="btn btn-success" id="hkjl-search" type="button" value=" 查 询 " />
                                        <a href="${basePath!}/platfrom/user/tradingPois"><input id="" class="btn btn-info" type="button" value=" 下 载 报 表 " /></a>
                                    </div>
                                </div>
                            </form>
                            <div class="project-list " id="hkjl-list">

                            </div>
                        </div>
                    </div>
                    <div id="yqjl" class="ibox-content tab-pane">
                        <div class="ibox-content">
                            <form id="yqjl-form" class="form-horizontal">
                                <input type="hidden" name="unum" value="${entity.id!}"/>
                                <div>
                                    <div class="form-group col-sm-12">
                                        <h3>逾期总金额：¥ <span id="totaloverDue">--</span> 元  = ¥ <span id="overcurrentCorpus">--</span> 元 (本金)+ ¥ <span id="overcurrentFee">--</span> 元(利息) + ¥ <span id="overlateFee">--</span> 元(滞纳金)</h3>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">还款时间</label>
                                        <div class="col-sm-9">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" id="startDateYQJL" name="startDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" id="endDateYQJL" name="endDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <label class="col-sm-3 control-label">用户id</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="unum" maxlength="30" placeholder="请填写用户id">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                        <input class="btn btn-success" type="button" id="yqjl-search" value=" 查 询 " />
                                        <a href="${basePath!}/platfrom/user/tradingPoiss"><input id="" class="btn btn-info" type="button" value=" 下 载 报 表 " /></a>
                                    </div>
                                </div>
                            </form>
                            <div style="clear:both;"></div>
                            <div class="project-list" id="yqjl-list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#oid-span").operaSelect();
            $("#authStatus").select();
            $("#authStatus").change(function(){
                if($(this).val() == "true"){
                    $(".sx").removeAttr("disabled");
                }else{
                    $(".sx").attr("disabled",true);
                }
            });
            $("#authStatus").trigger("change");
            $("#area").area();
            $("#bank").initBank();
            //$("#status").select();
            $("#isMarry").select();
            $(".cancel").click(function(){parent.closeLayer();});
            $(".submit-save").click(function (){
                if(!$("#_f").validate()) return;
                var $this = $(this);
                $this.html("保 存 中");
                $this.attr("disabled", true);
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                <#--url:'${basePath!}/userInfo/<c:choose><c:when test="$!{id != null}">userInfoEditAjaxSubmit</c:when><c:otherwise>userInfoAddAjaxSubmit</c:otherwise></c:choose>.htm',-->
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:$("#_f").serialize(),
                    success: function(data){ //成功
                        layer.close(index);
                        var obj = data;
                        if (obj.resultCode == "-1") {
                            parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
                            $this.html("保存内容");
                            $this.attr("disabled", false);
                        }

                        if (obj.resultCode == "1") {
                            //消息对话框
                            parent.messageModel("保存成功!");
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }
                    }
                });
            });

            // <c:if test="$!{id != null}">
            laydate({
                elem: '#startDateJYJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDateJYJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });

            laydate({
                elem: '#startDateYQJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 00:00:00' //日期格式
            });
            laydate({
                elem: '#endDateYQJL',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD 23:59:59' //日期格式
            });
            $("#repayType").select();
            $("#tradeType").select();

            //交易明细里列表
            var $pager_jyjl = $("#jyjl-list").pager({
                url:"${basePath!}/platfrom/user/tradingDetailManagerList?uid=${uid!}",
                formId:"jyjl-form",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>交易单号</th>'+
                    '<th>商户id</th>'+
                    '<th>商户名称</th>'+
                    '<th>行业类型</th>'+
                    '<th>用户id</th>'+
                    '<th>用户名称</th>'+
                    '<th>交易类型</th>'+
                    '<th>交易时间</th>'+
                    '<th>交易金额</th>'+
                    '<th>结算金额</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{mid}</td>'+
                    '<td>{legalName}</td>'+
                    '<td>{nature}</td>'+
                    '<td>{uid}</td>'+
                    '<td>{realName}</td>'+
                    '<td>{tradingType}</td>'+
                    '<td>{tradingDate}</td>'+
                    '<td>{tradeAmount}</td>'+
                    '<td>{actualAmount}</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });
                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });

                    $("#jysel").click(function(){
                        var jylx_edate =$("#endDateJYJL").val();	//结束时间
                        var jylx_sdate =$("#startDateJYJL").val();	//开始时间
                        var mname = $("#mname").val();	//商户名称
                        var uname = $("#name").val();	//用户名称
                        var unum = $("#unum").html();
                        unum =$.trim(unum);

                        if (typeof(mname) == "undefined") mname="";
                        if (typeof(uname) == "undefined") uname="";


                        window.location.href = "${basePath}/opera/userInfo/Consumerdaochu?startDate="+jylx_sdate+"&endDate="+jylx_edate+"&manme="+mname+"&uname="+uname+"&tradeType="+tradeType+"&unum="+unum;
                    });
                    /*if($!{tradingType}==0){
						$("#tradingType").html("未支付");
					}else if($!{tradingType}==1){
						$("#tradingType").html("已支付");
					}*/
                    $("#go_yq").click(function(){
                        var jylx_edate =$("#endDateYQJL").val();	//结束时间
                        var jylx_sdate =$("#startDateYQJL").val();	//开始时间
                        var yhid = $("#yhid").val();	//用户id
                        var yhname = $("#yhname").val();	//用户姓名
                        var yhphone = $("#yhphone").val();	//用户手机
                        var unum = $("#unum").html();
                        unum =$.trim(unum);
                        window.location.href = "${basePath!}/opera/userInfo/yqdaochu.htm?startDate="+jylx_sdate+"&endDate="+jylx_edate+"&unum="+unum+"&uname="+yhname+"&mobile="+yhphone;
                    });

                    $("#go_hk").click(function(){
                        var repayType = $("#repayType").val();	//交易状态
                        var unum = $("#unum").html();
                        unum =$.trim(unum);
                        window.location.href = "${basePath!}/opera/userInfo/repaydaochu.htm?&repayType="+repayType+"&status=2"+"&unum="+unum;
                    });

                    loadJyjlAmount();
                }
            });

            $("#jyjl-search").click(function(){
                $pager_jyjl.gotoPage(1);
            });

            function loadJyjlAmount(){
                $.ajax({
                    url:"${basePath!}/platfrom/p_merchant/totalJymxAmount?uid=${uid!}",
                    type:'POST', //GET
                    data:$("#jyjl-form").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        $("#totalJyjlAmount").html(d.data);
                    }
                });
            }


            //还款纪录
            var $pager_hkjl = $("#hkjl-list").pager({
                url:"${basePath!}/platfrom/user/repaymentPlanManagerList?uid=${uid!}",
                formId:"hkjl-form",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>流水单号</th>'+
                    '<th>用户id</th>'+
                    '<th>还款状态</th>'+
                    '<th>用户姓名</th>'+
                    '<th>应还本金</th>'+
                    '<th>滞纳金</th>'+
                    '<th>分期利息</th>'+
                    '<th>本期应还</th>'+
                    '<th>期数</th>'+
                    '<th>还款日</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{userId}</td>'+
                    '<td>{repaymentType}</td>'+
                    '<td>{realName}</td>'+
                    '<td>¥ {currentCorpus} 元</td>'+
                    '<td>¥ {lateFee} 元</td>'+
                    '<td>¥ {currentFee} 元</td>'+
                    '<td id="yhs_{id}"></td>'+
                    '<td>{currentTime}/{totalTime}</td>'+
                    '<td>{repaymentDate}</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    console.log(result)
                    for(var i = 0 ; i < result.data.records.length ; i++){
                        var item = result.data.records[i];
                        $("#yhs_"+item.id).html("¥ " + ((item.currentCorpus == null ? 0:item.currentCorpus)+(item.lateFee == null ? 0:item.lateFee)+(item.currentFee == null ? 0 : item.currentFee)).toFixed(2) + " 元");
                    }

                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });
                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                    loadHkAmount();
                    loadHkAmounts();
                }
            });

            function loadHkAmount(){
                $.ajax({
                    url:"${basePath!}/platfrom/user/SUM?uid=${uid!}",
                    type:'POST', //GET
                    data:$("#hkjl-form").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        console.log(d);
                        if(d.data.DhSum == null){
                            $("#whAmount").html("0");
                        }else {
                            $("#whAmount").html(d.data.DhSum);
                        }
                        if(d.data.YqSum==null){
                            $("#yqAmount").html('0');
                        }else {
                            $("#yqAmount").html(d.data.YqSum);
                        }
                    }
                });
            }

            function loadHkAmounts(){
                $.ajax({
                    url:"${basePath!}/platfrom/user/SUMS?uid=${uid!}",
                    type:'POST', //GET
                    data:$("#hkjl-form").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        console.log(d);
                        $("#yhAmount").html(d.data.YhSum);
                    }
                });
            }


            $("#hkjl-search").click(function(){
                $pager_hkjl.gotoPage(1);
            });

            var $pager_yqjl = $("#yqjl-list").pager({
                url:"${basePath!}/platfrom/p_payback/pageInfo?uid=${uid!}&status=1&isPay=0",
                formId:"yqjl-form",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>流水单号</th>'+
                    '<th>用户id</th>'+
                    '<th>期数</th>'+
                    '<th>逾期本金</th>'+
                    '<th>滞纳金</th>'+
                    '<th>利息费</th>'+
                    '<th>逾期应还</th>'+
                    '<th>还款日</th>'+
                    '<th>逾期天数</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{serialNo}</td>'+
                    '<td>{userId}</td>'+
                    '<td>{currentTime}/{totalTime}</td>'+
                    '<td>¥ {currentCorpus} 元</td>'+
                    '<td>¥ {lateFee} 元</td>'+
                    '<td>¥ {currentFee} 元</td>'+
                    '<td id="yh_{id}"></td>'+
                    '<td>{repaymentDate}</td>'+
                    '<td>{margin} 天</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    console.log(result);
                    for(var i = 0 ; i < result.data.records.length ; i++){
                        var item = result.data.records[i];
                        $("#yh_"+item.id).html("¥ " + ((item.currentCorpus == null ? 0:item.currentCorpus)+(item.lateFee == null ? 0:item.lateFee)+(item.currentFee == null ? 0 : item.currentFee)).toFixed(2) + " 元");
                    }
                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });
                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                    loadYqAmount();
                }
            });
            function loadYqAmount(){
                $.ajax({
                    url:"${basePath!}/platfrom/p_payback/overDue?uid=${uid!}",
                    type:'POST', //GET
                    data:$("#searchForm").serialize(),
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        console.log(d);
                        $("#totaloverDue").html(d.data.totalMoney);
                        $("#overcurrentCorpus").html(d.data.currentCorpus);
                        $("#overcurrentFee").html(d.data.currentFee);
                        $("#overlateFee").html(d.data.lateFee);
                    }
                });
            }


            $("#yqjl-search").click(function(){
                $pager_yqjl.gotoPage(1);
            });
            // </c:if>
        });
    </script>
</body>

</html>
