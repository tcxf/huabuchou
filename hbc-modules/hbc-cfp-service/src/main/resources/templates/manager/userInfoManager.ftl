
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <style>
        select{
            height: 25px!important;
            width: 100px!important;
        }
    </style>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>消费者管理</h5>
                </div>
                <div class="ibox-content">
                    <div id="xfz">

                    </div>
                    <form id="searchForm" class="form-inline">
                        <div class="form-group">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <a href="${basePath!}/opera/userInfo/qu"><button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
                                </div>
                                <div class="col-sm-8">

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">用户名称</label>
                                <div class="col-sm-9">
                                    <input type="text" name="realName" placeholder="请输用户名称" id="realName" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">用户手机</label>
                                <div class="col-sm-9">
                                    <input type="text" name="mobile" placeholder="请输用户手机" id="mobile" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;margin-left: -50px;">
                                <label class="col-sm-3 control-label">注册时间</label>
                                <div class="col-sm-9" style="padding-left: 0">
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
                                    </div>
                                    <div class="col-sm-6" style="padding-left:0px;">
                                        <input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">状态</label>
                                <div class="col-sm-9">
                                    <select id=status name="status">
                                        <option value="">全部</option>
                                        <option value="0">启用</option>
                                        <option value="1">禁用</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">授信状态</label>
                                <div class="col-sm-9">
                                    <select id=authStatus name="authStatus">
                                        <option value="">全部</option>
                                        <option value="0">未授信</option>
                                        <option value="1">已授信</option>
                                        <option value="2">已冻结</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">绑卡情况</label>
                                <div class="col-sm-9">
                                    <select  name="bindCardStatus" id="bindCardStatus">
                                        <option value="">全部</option>
                                        <option value="1">已绑卡</option>
                                        <option value="0">未绑卡</option>
                                    </select>
                                </div>
                            </div>

                            <div style="clear:both"></div>
                            <div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">用户类型</label>
                                <div class="col-sm-9">
                                    <select id="userAttribute" name="userAttribute">
                                        <option value="">全部</option>
                                        <option value="1">会员</option>
                                        <option value="2">商户</option>
                                    </select>
                                </div>
                            </div>

                            <div>
                                <div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
                                    <input type="button" id="search" class="btn btn-warning" value="搜索" />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="project-list" id="data"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("select").select();
        laydate({
            elem: '#startDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
        });
        laydate({
            elem: '#endDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
        });

        var $pager = $("#data").pager({
            url:"${basePath!}/opera/userInfo/userInfoManagerList?oid=${oid!}",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group">'+
                '<table class="table table-hover">'+
                '<thead>'+
                '<tr>'+
                '<th>用户id</th>'+
                '<th>用户名称</th>'+
                '<th>用户手机</th>'+
                '<th>用户类型</th>'+
                '<th>所属运营商</th>'+
                '<th>注册时间</th>'+
                '<th>授信状态</th>'+
                '<th>绑卡情况</th>'+
                '<th>状态</th>'+
                '<th>操作</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{id}</td>'+
                '<td>{realName}</td>'+
                '<td>{mobile}</td>'+
                '<td>{attribute}</td>'+
                '<td>{name}</td>'+
                '<td>{createDate}</td>'+
                '<td>{authStatus}</td>'+
                '<td>{bindCardStatus}</td>'+
                '<td>{status}</td>'+
                '<td><a href="javascript:void(0);"  data="{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> </td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
            },
            callbak: function(result){

                //编辑页面
                $(".detail").click(function (){
                    var title = $(this).attr("data").split(",")[1];
                    var id = $(this).attr("data").split(",")[0];
                    parent.openLayer('消费者详情-'+id,'${basePath!}/opera/userInfo/forwardUserInfoDetail?uid='+id,'1000px','700px',$pager);
                });

                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });
                // check 全部选中
                $('.checks').on('ifChecked', function(event){
                    $("input[id^='check-id']").iCheck('check');
                });

                // check 全部移除
                $('.checks').on('ifUnchecked', function(event){
                    $("input[id^='check-id']").iCheck('uncheck');
                });
            }
        });

        $("#loading-example-btn").click(function(){
            $pager.gotoPage($pager.pageNumber);
        });

    });

</script>

</body>
</html>
