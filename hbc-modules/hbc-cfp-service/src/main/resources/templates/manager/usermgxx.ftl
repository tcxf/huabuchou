<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">
	<style>
		.row{
			line-height:40px;
		}
	</style>
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#examine" aria-expanded="true"> 用户灰分度</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true">黑名单信息</a></li>
					<li><a data-toggle="tab" href="#zc" aria-expanded="true"> 用户注册过的机构</a></li>
					<li><a data-toggle="tab" href="#df" aria-expanded="true"> 身份证存疑</a></li>
					<li><a data-toggle="tab" href="#gg" aria-expanded="true"> 手机号码存疑</a></li>
					<li><a data-toggle="tab" href="#jg" aria-expanded="true"> 用户被机构查询历史</a></li>
					<li><a data-toggle="tab" href="#p2p" aria-expanded="true"> p2p数据</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value=""/>
						<div>
						  	<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">报告编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${bh }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">电话号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${jbxx.user_phone }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${jbxx.user_name }
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">户籍地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${jbxx.user_province }${jbxx.user_region }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">证件号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${jbxx.user_idcard }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-8 control-label">电话号码归属地及运营商</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
								${jbxx.user_phone_province } ${jbxx.user_phone_operator }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						</div>
						
			
					
					<!-- 灰分度 -->
					<div id="examine" class="ibox-content tab-pane">
					
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">用户评分（分数越高，用户征信记录优秀）</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hjl.phone_gray_score }</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">一阶联系人总数</label>
							</div>
							<<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hjl.contacts_class1_cnt }</label>
							</div>
							
					</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">直接联系人在黑名单的数量</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hjl.contacts_class1_blacklist_cnt }</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">间接联系人在黑名单的数量</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hjl.contacts_class2_blacklist_cnt }</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">用户手机号码</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hjl.user_phone }</label>
							</div>
							
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
						<!-- 黑名单信息 -->
				 	<div id="settlement" class="ibox-content tab-pane">
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">黑名单类型</label>
							</div>
							<c:forEach var="hmd" items="${hmd.blacklist_category }">
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hmd }</label>
							</div>
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">黑名单信息</label>
							</div>
							
							<div class="form-group col-sm-2">
							<c:forEach var="hmd1" items="${hmd1.blacklist_details }">
							<label class="col-sm-6 control-label">
								${hmd1.details_key }
								</label>
								<label class="col-sm-6 control-label">
								${hmd1.details_value }
								</label>
								</c:forEach>
							</div>
							
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	
					
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">身份证和姓名是否在黑名单</label>
							</div>
							<c:forEach var="hmd" items="${hmd.blacklist_name_with_idcard }">
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${hmd==false }">不在黑名单</c:if>
								<c:if test="${hmd==true }">在黑名单</c:if>
								</label>
							</div>
							</c:forEach>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">手机号和姓名是否在黑名单</label>
							</div>
							<c:forEach var="hmd" items="${hmd.blacklist_name_with_phone }">
							<<div class="form-group col-sm-2">
								<c:if test="${hmd==false }">不在黑名单</c:if>
								<c:if test="${hmd==true }">在黑名单</c:if>
							</div>
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">更新时间</label>
							</div>
							<c:forEach var="hmd" items="${hmd.blacklist_update_time_name_idcard }">
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${hmd }</label>
							</div>
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div> 
					
							<!-- 用户注册过的机构 -->
				 	<div id="zc" class="ibox-content tab-pane">
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">注册过的机构总数</label>
							</div>
							<c:forEach var="zc" items="${zc.register_cnt }">
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">${zc }</label>
							</div>
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">注册机构信息</label>
							</div>
							
							<div class="form-group col-sm-2">
						<c:forEach var="zc1" items="${zc1.register_orgs_statistics }">
							<label class="col-sm-6 control-label">
								${zc1.label}
								</label>
								<label class="col-sm-6 control-label">
								${zc1.count }
								</label>
								</c:forEach>
							</div>
							
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	
					
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">注册的电话号码</label>
							</div>
							<c:forEach var="zc" items="${zc.phone_num }">
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								${zc }
								</label>
							</div>
							</c:forEach>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div> 
					
					
					
					<!-- 身份证存疑 -->
			 		<div id="df" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">身份证在那些类型的机构中使用过</label>
							</div>
						</div>
						
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sfz" items="${sfz.idcard_applied_in_orgs }">
						
						 <div class="row">
							<div class="col-sm-4">
								机构类型
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_org_type} </label>
							</div>
						</div>
						<div class="row" style="border-bottom:1px solid #dadada">
							<div class="col-sm-4">
									查询时间
							</div>
							<div class="col-sm-4">
								 <label class="col-sm-20 control-label">${sfz.susp_updt} </label>
							</div>
						</div>
							</c:forEach>
							
								<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">用这个身份证号码绑定的其他姓名</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sfz" items="${sfz.idcard_with_other_names }">
						<div class="row">
							<div class="col-sm-4">
								绑定姓名
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_name} </label>
							</div>
						</div>
							</c:forEach>
							
										<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">用这个身份证绑定的其他手机号码</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sfz" items="${sfz.idcard_with_other_phones }">
						<div>
							<div class="form-group col-sm-20">
							<c:if test="${sfz==null }">
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">暂无记录 </label>
							</div></c:if>	
							
							<c:if test="${sfz.susp_phone_province!=null }">
							<div class="row">
							<div class="col-sm-4">
							手机号归属地
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_phone_province} </label>
							</div>
						    </div>
						     </c:if>
						     	
							<c:if test="${sfz.susp_phone!=null }">
							<div class="row">
							<div class="col-sm-4">
								手机号
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_phone} </label>
							</div>
						    </div>
						     </c:if>
						     	
							<c:if test="${sfz.susp_phone_operator!=null }">
							<div class="row">
							<div class="col-sm-4">
								运营商
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_phone_operator} </label>
							</div>
						    </div>
						     </c:if>
						     	
							<c:if test="${sfz.susp_phone_city!=null }">
							<div class="row">
							<div class="col-sm-4">
									手机号城市
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sfz.susp_phone_city} </label>
							</div>
						    </div>
						     </c:if>
							</div>
						</div>
							</c:forEach>
					</div>
				
			<!-- 手机号码存疑 -->
			 		<div id="gg" class="ibox-content tab-pane">
						
								<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">电话号码在那些类型的机构中使用过</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sjcy" items="${sjcy.phone_applied_in_orgs }">
						<div>
							<div class="form-group col-sm-20">
							 <div class="row">
							<div class="col-sm-4">
								机构类型
							</div>
							<div class="col-sm-4">
								<label class="col-sm-20 control-label">${sjcy.susp_org_type} </label>
							</div>
						</div>
										<div class="row" style="border-bottom:1px solid #dadada">
							<div class="col-sm-4">
									查询时间
							</div>
							<div class="col-sm-4">
								 <label class="col-sm-20 control-label">${sjcy.susp_updt} </label>
							</div>
						</div>
							</div>
						</div>
							</c:forEach>
							
										<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">用这个手机号码绑定的其他姓名</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sjcy" items="${sjcy.phone_with_other_names }">
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-20 control-label">${sjcy.susp_name} </label>
							</div>
						</div>
							</c:forEach>
							
							<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">用这个手机号码绑定的其他身份证</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<c:forEach var="sjcy" items="${sjcy.phone_with_other_idcards }">
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-20 control-label">${sjcy.susp_idcard} </label>
							</div>
						</div>
							</c:forEach>
							
					</div>
				<!-- 用户被机构查询历史 -->
			 		<div id="jg" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<c:forEach var="cxjl" items="${cxjl }">
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">主动查询用户信息的机构类型</label>
								<label class="col-sm-20 control-label">${cxjl.searched_org} </label>
							</div>
								<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">查询时间</label>
								<label class="col-sm-20 control-label">${cxjl.searched_date} </label>
							</div>
								<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">是否是本机构查询</label>
								<label class="col-sm-20 control-label"> 
								<c:if test="${cxjl.org_self==true}">是</c:if>
								<c:if test="${cxjl.org_self!=true}">不是</c:if> 
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							</c:forEach>
					
					</div>
					
				 		<!-- p2p数据 -->
			 		<div id="p2p" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<c:forEach var="p2p" items="${p2p }">
						<div>
							<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">关联P2P网贷平台名称</label>
								<label class="col-sm-20 control-label">${p2p.webloan_hit_pname} </label>
							</div>
								<div class="form-group col-sm-20">
								<label class="col-sm-4 control-label">关联P2P网贷平台个数</label>
								<label class="col-sm-20 control-label">${p2p.webloan_hit_count} </label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							</c:forEach>
					
					</div> 
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	
</body>

</html>