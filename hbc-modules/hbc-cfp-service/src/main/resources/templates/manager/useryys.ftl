<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">
	<style>
		.row{
			line-height:40px;
		}
	</style>
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true">通话记录</a></li>
					<li><a data-toggle="tab" href="#zc" aria-expanded="true"> 欺诈风险</a></li>
					<li><a data-toggle="tab" href="#df" aria-expanded="true"> 短信记录</a></li>
					<li><a data-toggle="tab" href="#gg" aria-expanded="true"> 通话短信分析</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value=""/>
						<div>
						  	<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">报告编号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${bh }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">电话号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${jbxx.phoneNo }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">姓名</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${jbxx.name }
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">查询时间</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
							${jbxx.reportTime }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">证件号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
							${jbxx.certNo }
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-8 control-label">电话号码归属地</label>
								<label class="col-sm-4 control-label" style="text-align: left;">
							${jbxx.phoneBelongArea }
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					
						<!--通话记录信息 -->
				 	<div id="settlement" class="ibox-content tab-pane">
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div class="row">
							<div class="form-group col-sm-12">
								<label class="col-sm-6 control-label">通话记录</label>
							</div>
						</div>	
							
						<div class=row>							
							<div class="form-group col-sm-2">
								<div class="control-label">地区</div>
							</div>	
							<div class="col-sm-2">
								<div class="control-label">电话号码</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">主叫次数</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">主叫时间</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">通话时间</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">被叫次数</div>
							</div>
						</div>
						<c:forEach var="hmd" items="${thjx.call }">
						<div class=row>	
							<div class="col-sm-2">
								<div class="control-label">${hmd.belongArea }</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">${hmd.phoneNo }</div>
							</div>	
							<div class="col-sm-2">
								<div class="control-label">${hmd.callTimes }</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">${hmd.calledTimes }</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">${hmd.connTime }</div>
							</div>
							<div class="col-sm-2">
								<div class="control-label">${hmd.connTimes }</div>
							</div>
							
						</div>
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					
					
							<!-- 欺诈风险 -->
				 	<div id="zc" class="ibox-content tab-pane">
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">身份证号码有效性</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${zc.certNoIsValid==true  }">有效</c:if>
								<c:if test="${zc.certNoIsValid!=true  }">无效</c:if>
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">运营商是否实名</label>
							</div>
							
							<div class="form-group col-sm-2">
							<label class="col-sm-6 control-label">
								<c:if test="${zc.certNoIsValid==true  }">是</c:if>
								<c:if test="${zc.certNoIsValid!=true  }">不是</c:if>
								</label>
							</div>
							
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
				 	
				 	
					
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">运营商实名是否与登记人一致</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${zc.samePeople ==true  }">是</c:if>
								<c:if test="${zc.samePeople !=true  }">不是</c:if>
								</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">是否出现长时间关机（5天以上无短信记录，无通话记录）</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${zc.longTimePowerOff ==true  }">有</c:if>
								<c:if test="${zc.longTimePowerOff !=true  }">没有</c:if>
								</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">申请人信息是否命中网贷黑名单</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${zc.inBlacklist ==unKnow  }">有</c:if>
								<c:if test="${zc.inBlacklist !=unKnow  }">没有</c:if>
								</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
							<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-4">
								<label class="col-sm-6 control-label">是否出现法院相关号码呼叫</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="col-sm-6 control-label">
								<c:if test="${zc.calledByCourtNo ==unKnow  }">有</c:if>
								<c:if test="${zc.calledByCourtNo !=unKnow  }">没有</c:if>
								</label>
							</div>
						
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
					</div> 
					
					
					
					<!-- 身份证存疑 -->
			 		<div id="df" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
				   <div>
							<div class="form-group col-sm-12">
								<label class="col-sm-6 control-label">短息记录</label>
							</div>
							
							<div class="form-group col-sm-2">
								<label class="control-label">号码</label>
							</div>
							 <div class="form-group col-sm-2">
								<label class="control-label">地区</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="control-label">总条数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="control-label">号码标识</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="control-label">接受短信条数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="control-label">发送短信条数</label>
							</div>
							<c:forEach var="hmd" items="${dx.dx }">
							<div class="col-sm-2">
								<label class="control-label">${hmd.phoneNo }</label>
							</div>
							<div class="col-sm-2">
								<label class="control-label">${hmd.belongArea }</label>
							</div>
							<div class="col-sm-2">
								<label class="control-label">${hmd.totalSmsNumber }</label>
							</div>
							<div class="col-sm-2">
								<label class="control-label">${hmd.identifyInfo }</label>
							</div>
							<div class="col-sm-2">
								<label class="control-label">${hmd.smsInTimes }</label>
							</div>
							<div class="col-sm-2">
								<label class="control-label">${hmd.smsOutTimes }</label>
							</div>
						
						</c:forEach>	
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
				
			<!-- 手机号码存疑 -->
			 		<div id="gg" class="ibox-content tab-pane">
								<div class="hr-line-dashed" style="clear:both"></div>
				
							<div class="form-group col-sm-12">
								<label class="control-label">特殊号码记录</label>
							</div>
							
							<div class="form-group col-sm-1">
								 <label class="control-label">通话次数</label>
							</div>
							<div class="form-group col-sm-2">
								<label class="control-label">对方标识</label>
							</div>
							<div class="form-group col-sm-1">
								 <label class="control-label">月份</label>
							</div>
							<div class="form-group col-sm-1">
								<label class="control-label">对方号码</label>
							</div>
							<div class="form-group col-sm-1">
								 <label class="control-label">短信联系次数</label>
							</div>
							<div class="form-group col-sm-1">
								<label class="control-label">主叫次数</label>
							</div>
							<div class="form-group col-sm-1">
								<label class="control-label">被叫次数</label>
							</div>
							<div class="form-group col-sm-1">
								 <label class="control-label">主叫时间</label>
							</div>
							<div class="form-group col-sm-1">
								 <label class="control-label">被叫时间</label>
							</div>
							<div class="form-group col-sm-1">
								 <label class="control-label">短信接收条数</label>
							</div>
							<div class="form-group col-sm-1">
								<label class="control-label">短信发送条数</label>
							</div>
							<c:forEach var="hmd" items="${sfz.sp }">
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.connTimes  }</label>
							</div>
							<div class="form-group col-sm-2">
							   <label class="control-label">${hmd.identityInfo }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.month  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.phoneNo }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.smsTimes  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.callTimes  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.calledTimes  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.callTime  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.calledTime   }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.smsInTimes  }</label>
							</div>
							<div class="form-group col-sm-1">
							   <label class="control-label">${hmd.smsOutTimes  }</label>
							</div>
						</c:forEach>	
					
						<div class="hr-line-dashed" style="clear:both"></div>
							
					</div>
			
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/layer/layer.min.js"></script>
	<!-- iCheck -->
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="<%=path %>/js/plugins/webuploader/webuploader.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	
</body>

</html>