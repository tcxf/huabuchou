<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>交易明细</h5>
					</div>
					<div class="form-group" style="position: absolute;right:20px;top:10px;z-index: 99999;">
						 <div>
							<input class="btn btn-primary in" id="submit-save" type="button" value="收入记录" />
							<input class="btn btn-white out" id="cancel" type="button" value="提现记录" />
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-12">
									<h3 ">钱包余额：<b id="qbzje">￥${w.zje}</b></h3>
								</div>
							</div>	
						</form>
						<div class="form-group col-sm-12" style="text-align:left;margin-top:10px;">
							<button class="btn btn-info" type="button"  id='txsq'>发起提现</button>
						</div>  
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>  
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	<script type="text/javascript" src="<%=path %>/js/mui/mui.min.js"></script> 
	<script type="text/javascript" src="<%=path %>/js/jquery.js"></script> 
	<script type="text/javascript" src="<%=path %>/js/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="<%=path %>/js/bootstrap.js"></script>  
	<script type="text/javascript" src="<%=path %>/js/info.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery-qrcode-0.14.0.min.js"></script>
	<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		var $pager;
	    
	  
	
		
		$(".in").click(function(){
				$pager = $("#data").pager({
				url:"${hsj}/wallet/walletsrlist.htm",
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
							    '<th>用户名称</th>'+ 
		                        '<th>用户手机</th>'+
		                        '<th>用户类型</th>'+
		                       '<th>收入来源</th>'+
		                        '<th>入账时间</th>'+
		                        '<th>交易金额</th>'+
		                        '<th>入账金额</th>'+
								
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{pname}</td>'+
								'<td>{pmobile}</td>'+
								'<td>{ptepy}</td>'+
								'<td>{source}</td>'+
								'<td>{modifyDate}</td>'+
								'<td>{tradeTotalAmount}</td>'+
								'<td >{amount}</td>'+
								
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
						api.tools.ajax({
							url:'<%=path%>/wallet/getWallet.htm',
							data:{
								
							}
						},function(d){ 
							$("#qbzje").html("￥ "+d.resultMsg); 
						});
					}
			});
		});
		
		$(".out").click(function(){ 
			$("#data").pager({
				url:"${hsj}/wallet/wallettxlist.htm", 
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
							 '<th>收款人</th>'+
	                          '<th>用户手机</th>'+
	                          '<th>开户行</th>'+
	                          '<th>银行卡号</th>'+
	                          '<th>提现时间</th>'+
	                          '<th>提现金额</th>'+
	                         '<th>到账金额</th>'+
	                         '<th>提现状态</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{uname}</td>'+
								'<td>{umobile}</td>'+
								'<td>{khh}</td>'+
								'<td>{bank}</td>'+
								'<td >{modifyDate}</td>'+
								'<td>{txje}</td>'+  
								'<td >{dzje}</td>'+
								'<td id="status_{id}">{state}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
							var list = result.list;
							for(var i = 0 ; i < list.length ; i++){
								if(list[i].state == 3){
									$("#status_"+list[i].id).html("提现失败");
								}else if(list[i].state == 1){ 
									$("#status_"+list[i].id).html("提现中");
								}else if(list[i].state == 2){
									$("#status_"+list[i].id).html("提现成功 ");
								}
							}
							
						  	
							api.tools.ajax({
								url:'<%=path%>/wallet/getWallet.htm',
								data:{
									 
								}
							},function(d){
								$("#qbzje").html("￥ "+d.resultMsg);  
							});	
							
					}
			});
		});
		
		
		$(".in").trigger('click');
		$("#txsq").click(function (){
			parent.openLayer('钱包提现','${hsj}/wallet/wallettxManager.htm','300px','50px',$pager); 
		});
	});
	
</script>

	</body>
</html>
