<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>交易明细</h5>
					</div>
					<div class="form-group" style="display:none;position: absolute;right:20px;top:10px;z-index: 99999;">
						 <div>
							<input class="btn btn-primary out" id="submit-save" type="button" value="使用记录" />
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<%-- <div>
								<div class="form-group col-sm-12">
									<h3>余额：<b>${inp.balance}</b></h3>
								</div> 
							</div> --%>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">接口</label>
									<div class="col-sm-9">
										<select id="interfaceName" name="interfaceName">
											<option value="">全部</option>
											<option value="蜜罐网贷征信接口">蜜罐网贷征信接口</option>
											<option value="央行征信接口">央行征信接口</option>
											<option value="P2P网贷黑名单接口">P2P网贷黑名单接口</option>
											<option value="电信运营商分析征信接口">电信运营商分析征信接口</option>
										</select>
									</div>
								</div> 
								<div class="form-group col-sm-6 col-lg-4">
									<label for="exampleInputEmail2" class="sr-only">所属运营商</label>
									<span id="oid-span" ele-id="oid" ele-name="oid"></span>
								</div>  
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户姓名</label>
									<div class="col-sm-9">
										<input id="uname" type="text" class="form-control" name="uname" maxlength="30" placeholder="请填写用户姓名">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">手机号码</label>
									<div class="col-sm-9">
										<input id="umobile" type="text" class="form-control" name="umobile" maxlength="30" placeholder="请填写用户手机号码">
									</div>
								</div>
							</div>  
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<!-- <input id="huankuan" class="btn btn-info" type="button" value=" 下 载 报 表 " /> -->
								</div>
							</div>  	
						</form> 
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>

 
	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#interfaceName").select();  
		$("#oid-span").operaSelect();
		var $pager;
		$pager=$(".out").click(function(){
				$pager = $("#data").pager({
				url:"${hsj}/interfacePurse/consumptionlist.htm",
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
							    '<th>用户名称</th>'+ 
		                        '<th>用户手机</th>'+
		                        '<th>所属运营商</th>'+
		                        '<th>使用接口</th>'+
		                        '<th>使用时间</th>'+
		                        '<th>接口扣款</th>'+
		                        '<th>余额</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{uname}</td>'+
								'<td>{umobile}</td>'+
								'<td>{oname}</td>'+  
								'<td>{interfaceName}</td>'+
								'<td >{modifyDate}</td>'+
								'<td>{interfaceMoney}</td>'+
								'<td >{balance}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					 
					}
			});
		});
		 
		$(".out").trigger('click'); 
	});
	
</script>

	</body>
</html>
