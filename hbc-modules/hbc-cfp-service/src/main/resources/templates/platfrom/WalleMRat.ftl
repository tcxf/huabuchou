<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
  <#include "common/common.ftl">

  <style>
  	.ibox .open > .dropdown-menu {
    left: auto;
    right: -36px;
}


  </style>

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户提现费率设置</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 添加</a> 
						</div>
					</div>
					<div class="ibox-content">
						<div class="fixed-table form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>阶梯</th>
										<th>提现区间</th>
										<th>提现费用</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody id="tbody"  >
								</tbody>
							</table>
						</div>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	 <script src="${basePath!}/js/plugins/webuploader/webuploader.js"></script>

	<script type="text/javascript">
        var context= "${basePath!}"
        var context2= "${basePath!}";
     $(document).ready(function () {
         $("#resourceType").change(function(){
             if($(this).val() == 1){
                 $(".model").show();
                 $(".model2").hide();
             }else{
                 $(".model").hide();
                 $(".mode2").show();
             }
         });

    	 var i=0;
    	 var nodata = false;


		 		function banner(){
					$("#tbody").empty();
					$.post("${basePath!}/platfrom/rat/mRatList",function(d){
						for(var i = 0 ; i < d.data.length ; i++){
						    if(d.data[i].presentType==2){

						        var type1 =' <select id="resourceType'+d.data[i].id+'" name="resourceType">'+
                                        '<option selected value="2">元/笔</option>'+
                                        '<option value="1" type>%/笔</option>'+
                                        '</select>'
							}else{
                                var type1 =' <select id="resourceType'+d.data[i].id+'" name="resourceType">'+
                                        '<option  value="2">元/笔</option>'+
                                        '<option selected value="1" type>%/笔</option>'+
                                        '</select>'
							}
							$tr = $('<tr>'+
									'<td class="bank-select" >'+
									'<div>'+
		 				 			'<input onkeyup="value=value.replace(/[^\\d]/g,\'\') " id="ladderClass'+d.data[i].id+'" name="bank-idCard" type="text" value="'+d.data[i].ladderClass+'" />' +
		 				 		'</div'+
								'</td>'+
								'<td>'+
									'<input onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')"  id="min'+d.data[i].id+'"" name="bank-idCard" type="text" value="'+d.data[i].minimumIntervalValue+'" /> --'+
									'<input onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')"  id="max'+d.data[i].id+'"" name="bank-idCard" type="text" value="'+d.data[i].maximumIntervalValue+'" />'+
									'</td>'+
								'<td>'+
									'<input onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')" id="content'+d.data[i].id+'" name="bank-idCard" type="text" value="'+d.data[i].presentContent+'" />'+
									type1+

								'</td>'+
								'<td><input type="button"  data-id="'+d.data[i].id+'" class="btn btn-info update-banner" value="更新" /> <input type="button"  data-id3="'+d.data[i].id+'" class="del-banner btn btn-danger" value="删除" /></td>'+
							'</tr>');
							
							$("#tbody").append($tr);
							
							$tr.find(".update-banner").click(function(){
								$this = $(this);
								$p = $this.parent().parent();
								$.post("${basePath!}/platfrom/rat/mrat",{
									 id:$this.attr("data-id"),
                                    ladderClass:$p.find("#ladderClass"+$this.attr("data-id")).val(),
                                    min:$p.find("#min"+$this.attr("data-id")).val(),
                                    max:$p.find("#max"+$this.attr("data-id")).val(),
                                    type:$p.find("#resourceType"+$this.attr("data-id")).val(),
                                    content:$p.find("#content"+$this.attr("data-id")).val(),
								},function(d){
									layer.alert(d.msg);
										banner();

								},"json");
							});
							
							$tr.find(".del-banner").click(function(){
								$this = $(this);
								li = layer.confirm('您确认删除吗?', {
									btn: ['确定','取消'] //按钮
								}, function(){
									$.post("${basePath!}/platfrom/rat/delete",{id:$this.attr("data-id3")},function(d){
											parent.messageModel(d.msg);
											$this.parent().parent().remove();
											layer.close(li);
											location.reload(); 
									},"json");
								}, function(){});
							});
							
							
							
						}
					},"json");
				}
		 		banner();
				$("#new").click(function(){
					if(i!=0){
						layer.alert("请先完成添加");
					}else{
					if(nodata) $("#tbody").html("");
					nodata = false;
                        $tr = $('<tr>'+
                                '<td class="bank-select" >'+
                                '<div>'+
                                '<input onkeyup="value=value.replace(/[^\\d]/g,\'\') " id="ladderClass" name="bank-idCard" type="text" value="" />' +
                                '</div'+
                                '</td>'+
                                '<td>'+
                                '<input id="min" onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')"  name="bank-idCard" type="text" value="" /> --'+
                                '<input id="max" onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')"  name="bank-idCard" type="text" value="" />'+
                                '</td>'+
                                '<td>'+
                                '<input onkeyup="value=value.replace(/[^\\d^\\.]+/g,\'\')" id="content" name="bank-idCard" type="text"  />'+
                                ' <select id="resourceType" name="resourceType">'+
                                '<option value="2" >元/笔</option>'+
                                '<option value="1" >%/笔</option>'+
                                '</select>'+
                                '</td>'+
                                '<td><input type="button" class="btn btn-info save-banner" value="保存" /> <input type="button" class="del-banner btn btn-danger" value="删除" /></td>'+
                                '</tr>');
					$("#tbody").append($tr);

					i++;
					$tr.find(".save-banner").click(function(){
						$this = $(this);
						$p = $this.parent().parent();
						if($p.find("#ladderClass").val().length==0){
							layer.alert("请填写区间阶级");
							return;
						}
						if($p.find("#min").val().length==0){
							layer.alert("请填写最低区间值");
							return;
						}
                        if($p.find("#content").val().length==0){
                            layer.alert("请填写提现费用");
                            return;
                        }
						$.post("${basePath!}/platfrom/rat/mrat",{
                            ladderClass:$p.find("#ladderClass").val(),
                            min:$p.find("#min").val(),
							max:$p.find("#max").val(),
                            type:$p.find("#resourceType").val(),
                            content:$p.find("#content").val(),
							 id:null,
						},function(d){
                            parent.messageModel(d.msg);
								location.reload(); 

						},"json");
					});
					
					$tr.find(".del-banner").click(function(){
						$this = $(this);
						li = layer.confirm('您确认删除吗?', {
							btn: ['确定','取消'] //按钮
						}, function(){
							$this.parent().parent().remove();
							layer.close(li);
							location.reload(); 
							layer.alert("删除成功");
							
						}, function(){
						});
					});

					}});

    	 
   
     });
     
 </script>

	</body>
</html>
