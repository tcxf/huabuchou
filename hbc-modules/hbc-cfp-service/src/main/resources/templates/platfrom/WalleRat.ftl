
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 项目</title>
    <style>
        select{
            height: 25px!important;
            width: 100px!important;
        }
    </style>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>提现费率设置</h5>
                </div>
                <div class="ibox-content">
                    <div id="xfz">

                    </div>
                    <form id="searchForm" class="form-inline">
                        <div class="row">
                            <div class="form-group col-sm-10 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">用户提现费用%</label>
                                <div class="col-sm-9">
                                    <input type="text" id="content1" onkeyup="value=value.replace(/[^\d^\.]+/g,'')" value="${tb1.presentContent!}" placeholder="请输用户提现费率" id="realName" class="form-control">
                                    <a id="a1" href="javascript:void(0);" class="btn btn-success btn-sm detail">保存 </a>
                                    <input hidden="hidden" id="id1" value="${tb1.id}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-10 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">运营商提现费用%</label>
                                <div class="col-sm-9">
                                    <input type="text" id="content3" onkeyup="value=value.replace(/[^\d^\.]+/g,'')" value="${tb3.presentContent!}"  placeholder="请输运营商提现费率" id="mobile" class="form-control">
                                    <a id="a3" href="javascript:void(0);" class="btn btn-success btn-sm detail">保存 </a>
                                    <input hidden="hidden" id="id3" value="${tb3.id}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-10 col-lg-4" style="margin-bottom:10px;">
                                <label class="col-sm-3 control-label">资金端提现费用%</label>
                                <div class="col-sm-9">
                                    <input type="text" id="content4" onkeyup="value=value.replace(/[^\d^\.]+/g,'')"  value="${tb4.presentContent!}"  placeholder="请输资金端提现费率" id="mobile" class="form-control">
                                    <a id="a4" href="javascript:void(0);" class="btn btn-success btn-sm detail">保存 </a>
                                    <input hidden="hidden" id="id4" value="${tb4.id}">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#a1").click(function (){
            f(1);
        })
        $("#a3").click(function (){
            f(3);
        })
        $("#a4").click(function (){
            f(4);
        })
        function f(w) {
            var id = $("#id"+w).val();
            var content=$("#content"+w).val();
            $.ajax({
                url:'${basePath}/platfrom/rat/rat',
                type:'post', //数据发送方式
                dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                data:{
                    id:id,
                    content:content
                },
                success: function(r){ //成功
                    var obj = r;
                    //消息对话框
                    parent.messageModel(r.msg);
                    window.location.href = "${basePath!}/platfrom/rat/urat";
                }
            });
        }
    })

</script>

</body>
</html>
