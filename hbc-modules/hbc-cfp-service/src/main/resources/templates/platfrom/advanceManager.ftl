<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>未出账还款列表</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<input type="hidden" name="type" value="2"/> <#--2：提前还款 -->
							<input type="hidden" name="status" value="1"/> <#--还款状态 1： 已还 -->
							<div>
								<div class="form-group col-sm-12">
									<h3>未出帐还款金额：¥ <span id="totalAmount">--</span>元</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">还款时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
											<input type="text" class="form-control" id="startDate" name="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
											<input type="text" class="form-control" id="endDate" name="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">还款用户</label>
									<div class="col-sm-9">
										<input id="realName" type="text" class="form-control" name="realName" maxlength="30" placeholder="请填写用户姓名">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4" >
									<label class="col-sm-3 control-label">运营商</label>
									<div class="col-sm-9">
										<span id="oid-span" ele-id="oid" ele-name="oid">
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="yhdb" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script type="text/javascript">
	$(document).ready(function(){
		$("#oid-span").operaSelect();//加载平台下面的所有运营商

        $("#yhdb").click(function(){
            $("#searchForm").attr("action","${basePath!}/platfrom/p_payback/advancePayPoi");
            $("#searchForm").submit();
        });

		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
		});

		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
		});

		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/p_payback/advancePay",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							/*'<th>流水单号</th>'+*/
							'<th>还款用户</th>'+
               				 '<th>用户类型</th>'+
							'<th>还款金额</th>'+
				            '<th>还款方式</th>'+
				            '<th>所属运营商</th>'+
               				 '<th>所属资金端</th>'+
							'<th>还款时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							/*'<td>{repaySn}</td>'+*/
							'<td>{realName}</td>'+
                			'<td>{utype}</td>'+
							'<td>{amount}</td>'+
                			'<td>{paymentType}</td>'+
               				 '<td>{oname}</td>'+
               				 '<td>{fname}</td>'+
							'<td>{payDate}</td>'+
							'<td><a href="javascript:void(0);" data="{tids}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查询明细 </a></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});

				loadAmount();//加载提前还款总金额
			}		
		});

		function loadAmount(){
			$.ajax({
				url:"${basePath!}/platfrom/p_payback/advancePayTotalMoney",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
				    console.log(d);
			    	$("#totalAmount").html(d.data.totalAmount);
			    }
			});
			$(".mx").click(function(){
				window.location.href="${basePath!}/platfrom/p_payback/jumpAdvancePayDetail?id="+$(this).attr("data");
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
