<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">

    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <style>
        body{
            background: #fff!important;
        }
        .month_pay{
            margin-top: 10px;
            margin-left: 10px;
        }
        .month_pay .col-sm-2>select{
            width: 160px;
            height: 30px;
        }
        .month_pay .col-sm-3{
            padding-left: 0;
            text-align: left;
        }
        .month_pay .col-sm-3 input{
            width: 100%;
            height: 30px;
            padding-left: 10px;
        }
        .month_pay .col-sm-6{
            text-align: right;
        }
        .month_pay .col-sm-6>a{
            text-decoration: none;
            color: #f49110;
            padding: 8px 10px;
            border: 1px solid #f49110;
            border-radius: 3px;
        }
        .month_pay .col-sm-6>a>img{
            width: 16px;
        }
        .month_form{
            margin-top: 20px;
            margin-left: 10px;
        }
        .repayment_state{
            position: relative;
        }
        .a1{
            position: absolute;
            width: 30px;
            height: 30px;
            line-height: 30px;
            background:rgb(102, 153, 0);
            top:0;
            right: 0;
            color: #fff;
            text-align: center;
        }
        .records{
            position: relative;
        }
        .add_div{
            width: 220px;
            height: 160px;
            border: 1px solid #ededed;
            position: absolute;
            top:35px;
            left: 0;
            display: none;
            border-radius: 8px;
            z-index: 999;

        }
        .select_div{
            margin-top: 10px;
            width: 100%;
            text-align: center;
            height: 60px;
            line-height: 30px;
            border-bottom: 1px solid #ededed;
            background: #f5f5f5!important;
        }
        .select_div select{
            width: 140px;
            height: 30px;
        }
        .btn-info{
            margin-top: -5px;
        }
        .add_content{
            width: 100%;
            height: 130px;
            background: #f5f5f5;
            overflow: auto;
        }
        .add_content>p{
            padding: 10px;
        }
        .month_titie{
            position: fixed;
            bottom:60px
        }
        .month_titie .col-sm-10>ul{
            padding: 0;
            margin: 0;
        }
        .a2{
            width: 30px;
            height: 30px;
            line-height: 30px;
            background:rgb(102, 153, 0);
            color: #fff;
            text-align: center;
        }
    </style>
    <#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        本月应还明细
                    </div>
                </div>
            </div>
        </div>
        <div class="month_pay">
            <form id="searchForm" class="form-inline">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-2">
                            <select name="status" id="status">
                                <option value="">所有还款状态</option>
                                <option value="1">已还款</option>
                                <option value="2">未还款</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input name="nameOrMobile" type="text" placeholder="搜索姓名/手机号">
                        </div>
                        <div class="col-sm-1">
                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                        </div>
                        <div class="col-sm-6">
                            <a id="xzbb">
                                <img src="${basePath!}/img/u4205.png" alt="">
                                <span>导出</span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="month_form" id="data">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>姓名</th>
                        <th>手机号</th>
                        <th>使用金额 (元)</th>
                        <th>还款金额 (元)</th>
                        <th class="repayment_state">
                            还款状态
                        </th>
                        <th>前3日电话提醒</th>
                        <th>前1日电话提醒</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>张三</td>
                        <td>151 1234 5678</td>
                        <td>100.00</td>
                        <td>20.00</td>
                        <td>未还款</td>
                        <td class="records">
                            <a href="#">记录▼</a>
                            <div class="add_div">
                                <div class="select_div">
                                    <select name="" id="">
                                        <option value="">请选择联系状态</option>
                                        <option value="">已通知</option>
                                        <option value="">未接通</option>
                                        <option value="">占线</option>
                                        <option value="">空号</option>
                                        <option value="">停机</option>
                                    </select>
                                    <a href="#" class="btn btn-info">确定</a>
                                </div>
                                <div class="add_content">
                                    <p>2018-09-19 16:20:22 &nbsp&nbsp<span>未接通</span></p>
                                </div>
                            </div>
                        </td>
                        <td class="records">
                            <a href="#">记录▼</a>
                            <div class="add_div">
                                <div class="select_div">
                                    <select name="" id="">
                                        <option value="">请选择联系状态</option>
                                        <option value="">已通知</option>
                                        <option value="">未接通</option>
                                        <option value="">占线</option>
                                        <option value="">空号</option>
                                        <option value="">停机</option>
                                    </select>
                                    <a href="#" class="btn btn-info">确定</a>
                                </div>
                                <div class="add_content">
                                    <p>2018-09-19 16:20:22&nbsp&nbsp<span>未接通</span></p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        $("select").select()
        $("#xzbb").click(function () {
            $("#searchForm").attr("action","${basePath!}/platfrom/RMRepaymentPlanDetail/excelRepaymentPlan");
            $("#searchForm").submit();
        });

        var $pager = $("#data").pager({
            url:"${basePath!}/platfrom/RMRepaymentPlanDetail/RepaymentPlanDetail",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-bordered">'+
                '<thead>'+
                '<tr>'+
                '<th>流水号</th>'+
                '<th>姓名</th>'+
                '<th>手机号</th>'+
                '<th>使用金额(元)</th>'+
                '<th>还款金额(元)</th>'+
                '<th class="repayment_state">还款状态</th>'+
                '<th>前3日电话提醒</th>'+
                '<th>前1日电话提醒(平台)</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{serialNo}</td>'+
                '<td>{realName}</td>'+
                '<td>{mobile}</td>'+
                '<td>{totalCorpus}</td>'+
                '<td>{repaymentSum}</td>'+
                '<td id="status_{id}"></td>'+
                '<td id="threeDay_{id}" class="records"></td>'+
                '<td id="oneDay_{id}" class="records"></td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="17"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                    var pid = list[i].id;
                    if (list[i].threeDay == null){
                        $("#threeDay_"+list[i].id).html("未通知");
                    }else if (list[i].threeDay == 0){
                        $("#threeDay_"+list[i].id).html("未接通");
                    }
                    else if (list[i].threeDay == 1){
                        $("#threeDay_"+list[i].id).html("已通知");
                    }else if (list[i].threeDay == 2){
                        var pid = list[i].id;
                        $("#threeDay_"+list[i].id).html("占线");
                    }else if (list[i].threeDay == 3){
                        $("#threeDay_"+list[i].id).html("空号");
                    }else{
                        $("#threeDay_"+list[i].id).html("停机");
                    }
                    if (list[i].oneDay == null){
                        $("#oneDay_"+list[i].id).html("未通知");
                    } else if (list[i].oneDay == 0){
                        $("#oneDay_"+list[i].id).html("未接通");
                    } else if (list[i].oneDay == 1){
                        $("#oneDay_"+list[i].id).html("已通知");
                    } else if (list[i].oneDay == 2){
                        $("#oneDay_"+list[i].id).html("占线");
                    } else if (list[i].oneDay == 3){
                        $("#oneDay_"+list[i].id).html("空号");
                    } else if (list[i].oneDay == 4) {
                        $("#oneDay_"+list[i].id).html("停机");
                    }

                    if (list[i].status == 1){
                        $("#status_"+list[i].id).html("已还款");
                        $("#oneDay_"+list[i].id).html("/");
                        $("#threeDay_"+list[i].id).html("/");
                    }else {
                        $("#status_"+list[i].id).html("未还款");
                    }
                }

                $(".btn-info").click(function () {
                    var s=  $(".select_div").find("option:selected").text();
                    $(".add_div").css("display","none");
                    var threeday= $("#three_"+$(this).attr("data")).val();
                    var oneday= $("#one_"+$(this).attr("data")).val();
                    $.ajax({
                        url: "${basePath!}/platfrom/RMRepaymentPlanDetail/RecordNotice",
                        type: 'POST', //GET
                        data: {id:$(this).attr("data"),threeDay:threeday,oneDay:oneday},
                        timeout: 30000,    //超时时间
                        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success: function (data) {

                        }
                    });
                });

                $(".records>a").click(function () {
                    $(this).nextAll(".add_div").eq(0).slideToggle();
                    $.ajax({
                        url: "${basePath!}/platfrom/RMRepaymentPlanDetail/selectNoticeByOneDay",
                        type: 'POST', //GET
                        data: {id:$(this).attr("data")},
                        timeout: 30000,    //超时时间
                        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success: function (d) {
                            var list = d.data;
                            var ids = $(this).attr("data");
                            var aaa = ids.split("=");
                            var aa =aaa[1];
                            $("#notice2_"+aa).empty();
                            if (list.length == 0)
                            {
                                $("#notice2_"+aa).append('<p>无查询联系状态</p>');
                            }else {
                                for(var i = 0 ; i < list.length ; i++){
                                    if (list[i].oneDay == null) {

                                    } else if (list[i].oneDay == 0) {
                                        $("#notice2_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>未接通</span></p>');
                                    } else if (list[i].oneDay == 1) {
                                        $("#notice2_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>已通知</span></p>');
                                    } else if (list[i].oneDay == 2) {
                                        $("#notice2_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>占线</span></p>');
                                    } else if (list[i].oneDay == 3) {
                                        $("#notice2_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>空号</span></p>');
                                    } else if (list[i].oneDay == 4) {
                                        $("#notice2_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>停机</span></p>');
                                    }
                                }
                            }
                        }
                    });
                    $.ajax({
                        url: "${basePath!}/platfrom/RMRepaymentPlanDetail/selectNoticeByThreeDay",
                        type: 'POST', //GET
                        data: {id:$(this).attr("data")},
                        timeout: 30000,    //超时时间
                        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                        success: function (d) {
                            var list = d.data;
                            var ids = $(this).attr("data");
                            var aaa = ids.split("=");
                            var aa =aaa[1];
                            $("#notice_"+aa).empty();
                            if (list.length == 0)
                            {
                                $("#notice_"+aa).append('<p>无查询联系状态</p>');
                            }else {
                                for(var i = 0 ; i < list.length ; i++) {
                                    if (list[i].threeDay == null) {

                                    } else if (list[i].threeDay == 0){
                                        $("#notice_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>未接通</span></p>');
                                    } else if (list[i].threeDay == 1) {
                                        $("#notice_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>已通知</span></p>');
                                    } else if (list[i].threeDay == 2) {
                                        $("#notice_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>占线</span></p>');
                                    } else if (list[i].threeDay == 3) {
                                        $("#notice_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>空号</span></p>');
                                    } else if (list[i].threeDay == 4) {
                                        $("#notice_"+list[i].planId).append('<p>'+list[i].createTime+'&nbsp&nbsp<span>停机</span></p>');
                                    }
                                }
                            }
                        }
                    });
                });
            }
        });
    });
</script>
</body>
</html>
