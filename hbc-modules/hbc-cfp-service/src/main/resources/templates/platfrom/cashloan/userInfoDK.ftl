<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>现金贷放款</h5>
						<div style="display:none;" class="ibox-tools"></div>
					</div>
					<div class="ibox-content">
					<form id="searchForm" class="form-inline">
						<div class="form-group">
							<div class="row m-b-sm m-t-sm">
								<div class="col-md-1">
									<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
								</div>
								<div class="col-sm-8">
								</div>
							</div>
						</div>
						<div>
							<div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
								<label class="col-sm-3 control-label">账号姓名</label>
								<div class="col-sm-9">
									<input id="realName" type="text" class="form-control" name="realName" maxlength="30" placeholder="请输入账号名称">
								</div>
							</div>
							<div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
								<label class="col-sm-3 control-label">资金方</label>
								<div class="col-sm-9">
									<input id="fname" type="text" class="form-control" name="fname" maxlength="30" placeholder="请输入资金方名称">
								</div>
							</div>		
							<div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
								<label class="col-sm-3 control-label">放款时间</label>
								<div class="col-sm-9">
									<div class="col-sm-6" style="padding-left:0px;">
										<input type="text" class="form-control" name="creditDate" id="creditDate" style="width:100%" maxlength="30" placeholder="">
									</div>
									<div class="col-sm-6" style="padding-left:0px;">
										<input type="text" class="form-control" name="fkDate" id="fkDate" style="width:100%" maxlength="30" placeholder="">
									</div>
								</div>
							</div>
							<div class="form-group col-sm-6 col-lg-4" style="margin-bottom:10px;">
								<label class="col-sm-3 control-label">放款状态</label>
								<div class="col-sm-9">
									<select id=cashflowstatus name="cashflowstatus">
										<option value="99">全部</option>
										<option value="1">已放款</option>
										<option value="0">待放款</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
								<input type="button" id="search" class="btn btn-warning" value="搜索" />
								</div>
							</div>
							
						</div>
					</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
   		$("#cashflowstatus").select();
		laydate({
			elem: '#creditDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#fkDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		
		var $pager = $("#data").pager({
			url:"${hsj}/cashloanForUser/userInfoDKList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>订单号</th>'+
							'<th>账号名称</th>'+
							'<th>资金方</th>'+
							'<th>运营商</th>'+
							'<th>手机号码</th>'+
							'<th>放款金额</th>'+
							'<th>放款状态</th>'+
							'<th>开户行</th>'+
							'<th>银行卡账号</th>'+
							'<th>放款时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{id}</td>'+
							'<td>{realName}</td>'+
							'<td>{fname}</td>'+
							'<td>{simpleName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{fmoney}</td>'+
							'<td id="cashflowstatus_{id}"></td>'+
							'<td>{bankName}</td>'+
							'<td>{bankCard}</td>'+
							'<td>{creditDate}</td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{uid},{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].cashflowstatus == 1){
						$("#cashflowstatus_"+list[i].id).html('已放款');
					}else if(list[i].cashflowstatus == 0){
						$("#cashflowstatus_"+list[i].id).html("待放款");
					}
					
				}
				
				//编辑页面
				$(".detail").click(function (){
					var id = $(this).attr("data").split(",")[1];
					var uid = $(this).attr("data").split(",")[0];
					parent.openLayer('放款详情-','${hsj}/cashloanForUser/userInfoLose.htm?id='+id+'&uid='+uid,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
	
		
	});
	
</script>

	</body>
</html>