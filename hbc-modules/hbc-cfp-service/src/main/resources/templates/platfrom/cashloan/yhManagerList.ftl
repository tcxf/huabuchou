<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>已还记录</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<input type="hidden" name="status" value="1"/>
							<div>
								<div class="form-group col-sm-12">
									<h3>已还款金额：¥ <span id="totalAmount">--</span> 元  = ¥ <span id="currentCorpus">--</span> 元 (本金)+ ¥ <span id="currentFee">--</span> 元(利息) + ¥ <span id="lateFee">--</span> 元(滞纳金)</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">还款时间</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
											<input type="text" class="form-control" id="startDate" name="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
											<input type="text" class="form-control" id="endDate" name="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
						
								<div class="form-group col-sm-6 col-lg-5">
									<label class="col-sm-3 control-label">还款方式</label>
									<div class="col-sm-9">
										<select id="repaymentType" name="repaymentType">
											<option value="">全部</option>
											<option value="1">网银</option>
											<option value="2">微信</option>
											<option value="3">支付宝</option>
											<option value="4">线下</option>
										</select>
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户姓名</label>
									<div class="col-sm-9">
										<input id="cname" type="text" class="form-control" name="cname" maxlength="30" placeholder="请填写用户姓名">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">资金端名称</label>
									<div class="col-sm-9">
										<input id="fname" type="text" class="form-control" name="fname" maxlength="30" placeholder="资金方名称">
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="huankuan" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#repaymentType").select();
		$("#huankuan").click(function(){
			$("#searchForm").attr("action","${hsj}/rmRepaymentDetail/huankuan.htm");
			$("#searchForm").submit();
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		$("#tradeType").select();
		var $pager = $("#data").pager({
			url:"${hsj}/rmRepaymentDetail/yhManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>流水单号</th>'+
							'<th>资金方</th>'+
							'<th>用户手机</th>'+
							'<th>用户姓名</th>'+
							'<th>应还本金</th>'+
							'<th>滞纳金</th>'+
							'<th>分期利息</th>'+
							'<th>已还</th>'+
							'<th>期数</th>'+
							'<th>还款日</th>'+
							'<th>还款时间</th>'+
							'<th>还款方式</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{id}</td>'+
							'<td>{fname}</td>'+
							'<td>{mobile}</td>'+
							'<td>{uname}</td>'+
							'<td>¥ {currentCorpus} 元</td>'+
							'<td>¥ {lateFeeAct} 元</td>'+
							'<td>¥ {currentFee} 元</td>'+
							'<td id="yh_{id}"></td>'+
							'<td>{currentTime}/{totalTime}</td>'+
							'<td>{repaymentDateStr}</td>'+
							'<td>{processDateStr}</td>'+
							'<td>{repaymentTypeStr}</td>'+
							'<td><a href="javascript:void(0);" data="{id}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查看详情 </a></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					console.log(list[i]);
					var yh = (parseFloat(list[i].currentCorpus) + parseFloat(list[i].currentFee) + parseFloat(list[i].lateFeeAct)).toFixed(2);
					$("#yh_"+list[i].id).html('¥ '+yh+' 元');
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				loadAmount();
				
				$(".mx").click(function(){
					window.location.href ="<%=path%>/rmRepaymentDetail/rmRepaymentXQ.htm?id="+$(this).attr("data");		
				});
				}	
			});

		function loadAmount(){
			$.ajax({
				url:"<%=path%>/rmRepaymentDetail/yhTotalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data.formatTotalAmount);
			    	$("#currentCorpus").html(d.data.formatCurrentCorpus);
			    	$("#currentFee").html(d.data.formatCurrentFee);
			    	$("#lateFee").html(d.data.formatLateFee);
			    }
			});
		}
		
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
