<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

	<#include "common/common2.ftl">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 资金机构基本信息</a></li>
                    <li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                    <div class="tab-content">
                        <div id="base" class="ibox-content active tab-pane">
                            <input type="hidden" name="id" value=""/>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">手机号码</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:请填写手机号码,mobile:请输入正确的手机号码" id="mobile" name="mobile" value="" maxlength="30" placeholder="请填写手机号码">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">资金机构名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:请填写资金机构名称" id="fname" name="fname" value="" maxlength="30" placeholder="请填写运营商名称">
                                    </div>
                                </div>

                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:请填写法人名称" id="name" name="name" value="" maxlength="30" placeholder="请填写法人名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人身份证</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:请填写法人证件号" id="bankcard" name="bankcard" value="" maxlength="30" placeholder="请填写法人身份证">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">所属地区</label>
                                    <div class="col-sm-10" name="areaId" id="area" data-name="areaId" data-value="">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">详情地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:请填写详情地址" name="addressInfo" id="addressInfo" value="" maxlength="30" placeholder="请填写详情地址">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>
                        <div id="bind" class="ibox-content tab-pane">
                            <input type="button" id="new-card" class="btn btn-success" value="新增" />
                            <div class="fixed-table-container form-group">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>开户行</th>
                                        <th>开户人</th>
                                        <th>预留手机</th>
                                        <th>身份证号</th>
                                        <th>卡号</th>
                                        <th></th>
                                        <th>是否主卡</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <script>
        $(document).ready(function () {
            var context2 = "${basePath!}";
            $("#shareFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#shareFee").attr("disabled","disabled");
                }else{
                    $("#shareFee").removeAttr("disabled");
                }
            });

            $("#area").area();
            examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            var nodata = false;
            $("#new-card").click(function(){
                if(nodata){
                    $("#tbody").html("");
                }
                nodata = false;
                $tr = $('<tr>'+
                        '<td class="bank-select" input-name="bank_bankSn" input-val=""></td>'+
                        '<td><input id="bname" class="form-control" name="bname" type="text"/></td>'+
                        '<td><input id="bank_mobile" class="form-control" name="bank_mobile" type="text"/></td>'+
                        '<td><input id="bank_idCard" class="form-control" name="bank_idCard" type="text"/></td>'+
                        '<td><input id="bank_bankCard" class="form-control" name="bank_bankCard" type="text"/></td>'+
                        '<td><input id="bank_name" name="bank_name" type="hidden"/></td>'+
                        '<td><select id="bank_isDefault" name="bank_isDefault"><option value="1">是</option><option value="0">否</option></select></td>'+
                        '<td><input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
                        '</tr>');
                $("#tbody").append($tr);
                $tr.find(".bank-select").initBank();
                $tr.find(".del-card").click(function(){
                    $this = $(this);
                    li = layer.confirm('您确认删除吗?', {
                        btn: ['确定','取消'] //按钮
                    }, function(){
                        $this.parent().parent().remove();
                        layer.close(li);
                    }, function(){});
                });
                $tr.find("select").select();
            });
            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
                if(!$("#_f").validate()) return;
                var c_count=$("#tbody tr").length;
                //判断新增是否绑卡 绑卡信息是否完善
                var td=$("#tbody tr").children('td');
                var bksn=td.eq(0).find("select").val();
                var options=$("#see option:selected");
                $("#bank_name").val(options.text());
                var bank=td.eq(0).find("select").text();
                var nm=td.eq(1).find("input").val();
                var ph=td.eq(2).find("input").val();
                var i_c=td.eq(3).find("input").val();
                var bk_c=td.eq(4).find("input").val();
                if(c_count<1){
                    parent.messageModel("请添加绑卡信息");
                    return;
                }else{
                    if(nm == null || nm == "" || ph == null || ph == "" || i_c == null || i_c == ""
                            || bk_c == null || bk_c == ""){
                        parent.messageModel("绑卡信息未完善");
                        return;
                    }
                }
                var $this = $(this);
                $this.html("保 存 中");
                $this.attr("disabled", true);
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                    url:'${basePath!}/platfrom/fundend/fundInfoAddAjaxSubmit',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:$("#_f").serialize(),
                    success: function(data){//成功
                        if (data.code == 0)
                        {
                            layer.close(index);
                            var obj = data;
                            //消息对话框
                            parent.messageModel("保存成功!");
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }else{
                            parent.messageModel(data.msg);
                            $this.html("保 存");
                            $this.attr("disabled", false);
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>
