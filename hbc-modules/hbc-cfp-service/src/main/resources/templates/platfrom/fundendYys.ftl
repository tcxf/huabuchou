<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common2.ftl">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<form id="searchForm" method="post" class="form-horizontal">
					<div id="examine" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
                            <div class="form-group">
                                <input type="text" name="name" placeholder="请输入运营商名称" id="name" class="form-control">
                            </div>
                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                    	</div>
                        <div class="project-list" id="data"></div>
					</div>
				</form>
            </div>
		</div>
	</div>
	<script type="text/javascript">
        var context2 = "${basePath!}";
        $(document).ready(function () {
            var $pager = $("#data").pager({
                url:"${basePath!}/platfrom/fundend/loadfundendoidList?id=${id!}",
                formId:"searchForm",
                pagerPages: 3,
                type: 'POST',
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>运营商ID</th>'+
                    '<th>运营商名称</th>'+
                    '<th>运营商手机</th>'+
                    '<th>法人</th>'+
                    '<th>身份证号</th>'+
                    '<th>操作</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{id}</td>'+
                    '<td>{name}</td>'+
                    '<td>{mobile}</td>'+
                    '<td>{legalName}</td>'+
                    '<td>{idCard}</td>'+
                    '<td id="opera_{id}">'+
						'<a id="typeOne_{id}" href="javascript:void(0);" data="{id},{simpleName},{typeOne}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i>  </a> '+
                    '<a id="type_{id}" href="javascript:void(0);" data="{id},{simpleName},{type}" class="btn btn-success btn-sm binding"><i class="fa fa-file"></i>  </a> '+
                    '</td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    $("#yyh").html("运营商共计："+result.data.total+"家");
                    var list = result.data.records;
                    for(var i = 0 ; i < list.length ; i++){
                        if(list[i].type == 1){
                            $("#typeOne_"+list[i].id).html("编辑还款比率");
                            $("#type_"+list[i].id).html("已绑定");
						}
                        else{
                            $("#typeOne_"+list[i].id).hide();
                            $("#type_"+list[i].id).html("立即绑定");
						}
                    }
                    //编辑页面
                    $(".edit").click(function (){
                        var title = $(this).attr("data").split(",")[1];
                        var id = $(this).attr("data").split(",")[0];
                        parent.openLayer('编辑运营商-'+title,'${basePath!}/platfrom/fundend/redactOpera?id='+id+'&&fid=${id}','1000px','700px',$pager);
                    });

                    //绑定页面
                    $(".binding").click(function (){
                        var title = $(this).attr("data").split(",")[1];
                        var id = $(this).attr("data").split(",")[0];
                        var type = $(this).attr("data").split(",")[2];
                        if (type == 0)
						{
                            parent.openLayer('运营商绑定-'+title,'${basePath!}/platfrom/fundend/loadfundBinding?id='+id+'&&fid=${id}','1000px','700px',$pager);
                        }
                    });
                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        increaseArea: '20%' // optional
                    });

                    // check 全部选中
                    $('.checks').on('ifChecked', function(event){
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function(event){
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                }
            });
		});
	</script>
</body>

</html>
