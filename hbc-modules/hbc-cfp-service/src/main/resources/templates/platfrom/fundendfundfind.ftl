<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common2.ftl">
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 资金机构基本信息</a></li>
					<li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
				</ul>
				<form id="_f" method="post" class="form-horizontal">
					<div class="tab-content">
						<div id="base" class="ibox-content active tab-pane">
							<input type="hidden" name="id" value="${id!}"/>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">手机号码</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:请填写手机号码,mobile:请输入正确的手机号码" id="mobile" name="mobile" value="${fFundendDtos.mobile!}" maxlength="30" placeholder="请填写手机号码">
									</div>
								</div>
							</div>

							<div class="hr-line-dashed" style="clear:both"></div>
							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">资金机构名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:请填写资金机构名称" id="fname" name="fname" value="${fFundendDtos.fname!}" maxlength="30" placeholder="请填写运营商名称">
									</div>
								</div>
							</div>

							<div class="hr-line-dashed" style="clear:both;"></div>

							<div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">法人名称</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:请填写法人名称" id="name" name="name" value="${fFundendDtos.name!}" maxlength="30" placeholder="请填写法人名称">
									</div>
								</div>

								<div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">法人身份证</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:请填写法人证件号" id="bankcard" name="bankcard" value="${fFundendDtos.bankcard!}" maxlength="30" placeholder="请填写法人身份证">
									</div>
								</div>
							</div>
							<div>

								<div class="form-group">
									<label class="col-sm-2 control-label">所属地区</label>
									<div class="col-sm-10" name="areaId" id="area" data-name="areaId" data-value="${fFundendDtos.address!}">
										
									</div>
								</div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">详情地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:请填写详情地址" id="addressInfo" name="addressInfo" value="${fFundendDtos.addressInfo!}" maxlength="30" placeholder="请填写详情地址">
                                    </div>
                                </div>
							</div>
							<div class="hr-line-dashed" style="clear:both"></div>
							<div class="hr-line-dashed" style="clear:both"></div>
						</div>
						<div id="bind" class="ibox-content tab-pane">
							<input type="button" id="new-card" class="btn btn-success" value="新增" />
							<div class="fixed-table-container form-group">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>开户行</th>
											<th>开户人</th>
											<th>预留手机</th>
											<th>身份证号</th>
											<th>卡号</th>
											<th>是否主卡</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody id="tbody">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

        <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<script>
		$(document).ready(function () {
            var context2 = "${basePath!}";
		$("#area").area();
		examine = false;
		$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
		    var target =e.target.toString(); //$(e.target)[0].hash;ssss
			if( target.indexOf('examine')>0 && !examine){
			    $(".img").uploadImg();
				examine = true;
			}
		});
		var nodata = false;
		function initCard(){
			$("#tbody").empty();
			$.post("${basePath!}/platfrom/fundend/loadBindCard",{id:"${id}"},function(d){
				var list = d.data.records;
				for(var i = 0 ; i < list.length ; i++){
				    if(list[i].isDefault == '1'){
                        select = '<select id="bank-isdefault" selected name="bank-isdefault"><option  value="1">是</option><option  value="0">否</option></select>';
					}else{
                        select = '<select id="bank-isdefault" name="bank-isdefault"><option value="1">是</option><option selected  value="0">否</option></select>';
					}

					$tr = $('<tr>'+
							'<td class="bank-select" input-name="bank-bankSn" input-val="'+list[i].bankSn+'"></td>'+
							'<td><input class="form-control" id="name" name="name2" type="text" value="'+list[i].name+'"/></td>'+
							'<td><input class="form-control" id="bank-mobile" name="bank-mobile" type="text" value="'+list[i].mobile+'"/></td>'+
							'<td><input class="form-control" id="bank-idCard" name="bank-idCard" type="text" value="'+list[i].idCard+'"/></td>'+
							'<td><input class="form-control" id="bank-bankCard" name="bank-bankCard" type="text" value="'+list[i].bankCard+'"/></td>'+
							'<td>'+select+'</td>'+
							'<td><input type="button" data-id="'+list[i].id+'" class="btn btn-info update-card" value="更新" /> <input type="button" data-id="'+list[i].id+'" class="del-card btn btn-danger" value="删除" /></td>'+
						'</tr>');
					$("#tbody").append($tr);
					$tr.find("select").select();
					$tr.find(".bank-select").initBank();
					$tr.find(".del-card").click(function(){
						$this = $(this);
						li = layer.confirm('您确认删除吗?', {
							btn: ['确定','取消'] //按钮
						}, function(){
							$.post("${basePath!}/platfrom/fundend/delBindCard",{id:$this.attr("data-id")},function(d){
								layer.alert(d.msg);
								$this.parent().parent().remove();
								layer.close(li);
							},"json");
						}, function(){});
					});

					$tr.find(".update-card").click(function(){
						$this = $(this);
						$p = $this.parent().parent();
						$.post("${basePath!}/platfrom/fundend/updateBindCard",{
							id:$this.attr("data-id"),
							bankCard:$p.find("#bank-bankCard").val(),
							name:$p.find("#name2").val(),
							bankSn:$p.find("#bank-bankSn").val(),
							idCard:$p.find("#bank-idCard").val(),
							isDefault:$p.find("#bank-isdefault").val(),
							mobile:$p.find("#bank-mobile").val()
						},function(d){
							layer.alert(d.msg);
							initCard();
						},"json");
					});
				}
			},"json");
		}
		initCard();

		$("#new-card").click(function(){
			if(nodata) $("#tbody").html("");
			nodata = false;
			$tr = $('<tr>'+
					'<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
					'<td><input id="name" class="form-control" name="name" type="text"/></td>'+
					'<td><input id="bank-mobile" class="form-control" name="bank-mobile" type="text"/></td>'+
					'<td><input id="bank-idCard" class="form-control" name="bank-idCard" type="text"/></td>'+
					'<td><input id="bank-bankCard" class="form-control" name="bank-bankCard" type="text"/></td>'+
					'<td><select id="bank-isdefault" name="bank-isDefault"><option value="1">是</option><option value="0">否</option></select></td>'+
					'<td><input type="button" class="save-card btn btn-info" value="保存" /> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
				'</tr>');
			$("#tbody").append($tr);
			$tr.find(".bank-select").initBank();
			$tr.find(".save-card").click(function(){
				$this = $(this);
				$p = $this.parent().parent();
				$.post("${basePath!}/platfrom/fundend/newBindCard",{
					oid:'${id}',
					bankCard:$p.find("#bank-bankCard").val(),
					name:$p.find("#name").val(),
					bankSn:$p.find("#bank-bankSn").val(),
					idCard:$p.find("#bank-idCard").val(),
					isDefault:$p.find("#bank-isDefault").val(),
					mobile:$p.find("#bank-mobile").val()
				},function(d){
                    layer.alert(d.msg);
					initCard();
				},"json");
			});
				$tr.find(".del-card").click(function(){
					$this = $(this);
					li = layer.confirm('您确认删除吗?', {
						btn: ['确定','取消'] //按钮
					}, function(){
					    layer.alert(r.msg);
						$this.parent().parent().remove();
						layer.close(li);
					}, function(){});
				});
				$tr.find("select").select();
			});

		 	$("select").select();
			$(".cancel").click(function(){parent.closeLayer();});
			$("#submit-save").click(function (){
				if(!$("#_f").validate()) return;
				var $this = $(this);
				$this.html("保 存 中");
				$this.attr("disabled", true);
				var index = layer.load(2, {time: 10*1000});
				$.ajax({
					url:'${basePath!}/platfrom/fundend/fundInfoEditAjaxSubmit',
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:$("#_f").serialize(),
					success: function(data){ //成功
						layer.close(index);
						var obj = data;
						//消息对话框
						parent.messageModel(data.msg);
						parent.c.gotoPage(null);
						parent.closeLayer();
					}
				});
			});  
		});
	</script>
</body>

</html>
