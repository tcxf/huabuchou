<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">D</h1>
            </div>
            <h3>欢迎使用 平台授信管理系统</h3>
            <form class="m-t" role="form">
                <div class="form-group">
                    <input type="text" id="userName" class="form-control" placeholder="用户名">
                </div>
                <div class="form-group">
                    <input type="password" id="pwd" class="form-control" placeholder="密码">
                </div>
                <input type="button" id="loginButton" class="btn btn-primary block full-width m-b" value="登 录"></button>
            </form>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
	
</body>


<script type="text/javascript">
	
	$(document).keydown(function(event){ 
		if(event.keyCode == 13){ //绑定回车 
			loginAjaxSubmit();
		};
	});

	$(function() {
		$("#loginButton").click(function(){
			if(validLogin() == true)
			{
				loginAjaxSubmit();
			}
		});
	});

	function validLogin(){
		if($("#userName").val() == "")
		{
			layer.msg('请填写登录名');
			return false;
		}
		if($("#pwd").val() == "")
		{
			layer.msg('请填写密码');
			return false;
		}
		return true;
	}
	
	function loginAjaxSubmit(){
		$.ajax({
		    cache: false,
		    type: "POST",
		    url:"${basePath!}/platfrom/platfrom/login",
		    data: {userName:$("#userName").val(),pwd:$("#pwd").val()},
		    error: function(request) {
				layer.msg('登陆发生错误');
		    },
		    success: function(results) {
		    	var resultJson = results;
		    	var resultCode = resultJson.code;
		    	var resultMsg = resultJson.msg;
		    	if(resultCode == "1")
		    	{
		    		if(resultMsg != null && resultMsg != "1" && resultMsg != "2" && resultMsg != "5"){

		    		    if(resultJson.data!=null&&resultJson.data=="true"){
                            var pwd = $("#pwd").val()
                            window.location.href = "${basePath!}/platfrom/p_index/goToupdatepwd?type=1&pwd="+pwd;
						}else{
                            window.location.href = "${basePath!}/platfrom/page/index";
						}
		    		}else{
		    			window.location.href = "${basePath!}/userIndex.jsp";
	    			}
		    	}
		    	else
		    	{
					layer.msg(results.msg);
		    	}
		    }
		});
	}
	
	function validRestPwd(){
		if($("#userNameForRest").val() == "")
		{
			jNotify("请输入用户名!",{
				ShowOverlay : false,
				MinWidth : 100,
				VerticalPosition : "bottom",
				HorizontalPosition : "right"
			});
			return false;
		}

		if($("#regEmail").val() == "")
		{
			jNotify("请输入注册邮箱!",{
				ShowOverlay : false,
				MinWidth : 100,
				VerticalPosition : "bottom",
				HorizontalPosition : "right"
			});
			return false;
		}
		return true;
	}

	function loginRestPwdToggle(toogleFlag){
		if(toogleFlag == 1){
			$("#loginDiv").show();
			$("#resetPwdDiv").hide();
		}
		else{
			$("#resetPwdDiv").show();
			$("#loginDiv").hide();
		}
	}
</script>

</html>
