<!DOCTYPE html>
<html>

<head>

	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">


	 <title> - 基本表单</title>
	 <meta name="keywords" content="">
	 <meta name="description" content="">

	 <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	 <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	 <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	 <link href="${basePath!}/css/animate.css" rel="stylesheet">
	 <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>商家分类编辑</h5>
                    <div class="form-group" style="position: absolute;right:20px;top:5px;z-index: 99999;">
                        <div>
                            <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
                            <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="_f" method="post" class="form-horizontal">
                        <input type="hidden" name="id" />
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">名称</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" validate-rule="required,maxLength[20],ffstring" id="resourceName" name="name"  maxlength="30" placeholder="请填写行业名称">
                                </div>
                            </div>
                            <#--<div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">饼图颜色</label>
                                <div class="col-sm-8">
                                    <select name="charColor"id="charColor" class="form-control"'  style="margin-bottom: 20px; font-size: 10px;" >
                                    <option value='#FFCC00' selected <c:if test="${entity != null && entity.charColor=='#FFCC00'}">selected</c:if>>中黄色</option>
                                    <option value='#FF6600'<c:if test="${entity != null && entity.charColor=='#FF6600'}">selected</c:if>>橙色</option>
                                    <option value='#FF0033'<c:if test="${entity != null && entity.charColor=='#FF0033'}">selected</c:if>>红色</option>
                                    <option value='#99CC33'<c:if test="${entity != null && entity.charColor=='#99CC33'}">selected</c:if>>若绿色</option>
                                    <option value='#9999FF'<c:if test="${entity != null && entity.charColor=='#9999FF'}">selected</c:if>>青紫色</option>
                                    <option value='#66CCFF'<c:if test="${entity != null && entity.charColor=='#66CCFF'}">selected</c:if>>湖蓝色</option>
                                    <option value='#CCFF00'<c:if test="${entity != null && entity.charColor=='#CCFF00'}">selected</c:if>>若草色</option>
                                    <option value='#FF999'<c:if test="${entity != null && entity.charColor=='#FF999'}">selected</c:if>>珊瑚色</option>
                                    <option value='#FF66FF'<c:if test="${entity != null && entity.charColor=='#FF66FF'}">selected</c:if>>赤紫色</option>
                                    <option value='#0066FF'<c:if test="${entity != null && entity.charColor=='#0066FF'}">selected</c:if>>蓝色</option>
                                    <option value='#339933'<c:if test="${entity != null && entity.charColor=='#339933'}">selected</c:if>>绿色</option>
                                    <option value='#6600CC'<c:if test="${entity != null && entity.charColor=='#6600CC'}">selected</c:if>>紫色</option>
                                    <option value='#996633'<c:if test="${entity != null && entity.charColor=='#996633'}">selected</c:if>>褐色</option>
                                    <option value='#FFFF00'<c:if test="${entity != null && entity.charColor=='#FFFF00'}">selected</c:if>>柠檬黄</option>
                                    <select>
                                </div>
                            </div>-->
                        </div>
                        <div class="hr-line-dashed" style="clear:both;"></div>
                        <div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">图标</label>
                                <div class="col-sm-8">
                                    <div class="img" file-id="icon" file-value="" file-size="50*50"></div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label">排序号</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" validate-rule="required,number,ffstring" id="orderList" name="orderList" maxlength="30" placeholder="请填写排序号">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>
<script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
<!-- iCheck -->
<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
<script src="${basePath!}/js/plugins/webuploader/webuploader.js"></script>
<script>
    var context='${basePath!}';
    var context2='${basePath!}';
    $(document).ready(function () {

         $(".img").uploadImg();

        $("#cancel").click(function(){
            parent.closeLayer();
        });

        $("#submit-save").click(function (){
				if(!$("#_f").validate()) return;
				var $this = $(this);
				$this.html("保 存 中");
				$this.attr("disabled", true);
				var index = layer.load(2, {time: 10*1000});
				$.ajax({
					url:'${basePath!}/platfrom/p_merchant/insertOrUpdate',
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:$("#_f").serialize(),
					success: function(data){ //成功
						layer.close(index);
						console.log(data);
						/*if (obj.resultCode == "-1") {
							parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
							$this.html("保存内容");
							$this.attr("disabled", false);
						}*/
						if (data.code==0) {
							//消息对话框
                            parent.messageModel(data.msg);
							parent.c.gotoPage(null);
							parent.closeLayer();
						}else{
                            parent.messageModel(data.msg);
                            $this.attr("disabled", false);
                        }
					}
				});
        });

    });
</script>
</body>

</html>
