<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户行业管理</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商户行业</a>
						</div>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">商家商户行业</label>
								<input type="text" validate-rule="required,ffstring" name="name" placeholder="请输入商户行业名称" id="name" class="form-control">
							</div>
							<#--<div style="display: none;" class="form-group">   <!-- 注释的代码 &ndash;&gt;
								<label for="exampleInputEmail2" class="sr-only">所属运营商</label>
								<span id="oid-span" ele-id="oid" ele-name="oid"></span>
							</div> -->
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>



	<script type="text/javascript">
	$(document).ready(function(){
   		//$("#oid-span").operaSelect();
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/p_merchant/pageInfo",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							//'<th>所属运营商</th>'+
							'<th>商户行业名称</th>'+
							'<th>图标</th>'+
							'<th>排序号</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							//'<td>{operaName}</td>'+
							'<td>{name}</td>'+
							'<td><img src="{icon}" style="width:50px;height:50px;"/></td>'+
							'<td>{orderList}</td>'+
							'<td>'+
								'<a href="javascript:void(0);" data="{id}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data-id="{id}" class="btn btn-danger btn-sm remove"><i class="fa fa-remove"></i> 删除 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="6"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				/*var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].resourceType == 1)
						$("#resourceType_"+list[i].id).html("普通菜单");
					else
						$("#resourceType_"+list[i].id).html("模块");
				}*/
				
				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('${basePath!}/platfrom/p_merchant/deleteMerchantCategory',param,$pager);
				});
				
				//编辑页面
				$(".edit").click(function (){
					//var title = $(this).attr("data").split(",")[1];
					//var id = $(this).attr("data").split(",")[0];
					var id = $(this).attr("data");
					parent.openLayer('修改商家分类','${basePath!}/platfrom/p_merchant/jumpMerchantCategoryEdit?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${basePath!}/merchantCategory/delete.htm',param,$pager);
			}
		});
		
		//打开新增页面
		$("#new").click(function (){
			parent.openLayer('添加商家分类','${basePath!}/platfrom/p_merchant/jumpMerchantCategoryAdd','1000px','700px',$pager);
		});
	});
	
</script>

	</body>
</html>
