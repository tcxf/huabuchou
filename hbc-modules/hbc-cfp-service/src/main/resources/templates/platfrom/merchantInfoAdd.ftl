<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <#include "common/common.ftl">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
                    <li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
                    <li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                    <div class="tab-content">
                        <div id="base" class="ibox-content active tab-pane">
                            <input type="hidden" name="id" value="${id!}"/>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg=""  name="name">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户简称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="" name="simpleName">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">登陆密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" validate-rule="">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">重复密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" validate-rule="">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:法人名称不能为空" id="legalName" name="legalName" value="" maxlength="30" placeholder="请填写法人名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人证件号</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:法人证件号不能为空" id="idCard" name="idCard" value="" maxlength="30" placeholder="请填写法人证件号">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">手机号码</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:手机号码不能为空" id="mobile" name="mobile" value="" maxlength="30" placeholder="请填写手机号码">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">详细地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:详情地址不能为空" id="address" name="address" value="" maxlength="30" placeholder="请填写详细地址">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">所属地区</label>
                                <div class="col-sm-10" id="area" data-name="area" data-value="">

                                </div>
                            </div>
                            <div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">所属运营商</label>
                                    <div class="col-sm-8">
                                        <#--<select name="oid" id="oid">
                                            <option value="">请选择运营商</option>
                                        </select>-->
                                        <span id="oid-span" ele-id="oid" ele-name="oid"></span>
                                    </div>


                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">行业类型</label>
                                    <div class="col-sm-8">
                                        <#--<select name="micId" id="micId">
                                            <option value="">请选择分类</option>
                                        </select>-->
                                       <span id="micId-span" ele-id="micId" ele-name="micId"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">状态</label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status">
                                            <option value="1" >启用</option>
                                            <option value="0" >禁用</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>

                        <!-- 商户审核信息 -->
                        <div id="examine" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户性质</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:商户性质不能为空" id="nature" name="nature" value="" maxlength="30" placeholder="请填写商户性质">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">品牌经营年限</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:品牌经营年限不能为空" id="operYear" name="operYear" value="" maxlength="30" placeholder="请填写品牌经营年限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">经营面积</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:经营面积不能为空" id="operArea" name="operArea" value="" maxlength="30" placeholder="请填写经营面积">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">注册资金（万）</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:注册资金不能为空" id="regMoney" name="regMoney" value="" maxlength="30" placeholder="请填写注册资金">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户电话</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:经营面积不能为空" id="phoneNumber" name="phoneNumber" value="" maxlength="30" placeholder="请填写商户店铺电话">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商家介绍</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:注册资金不能为空" id="recommendGoods" name="recommendGoods" value="" maxlength="30" placeholder="请填写商家介绍">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">场地照片</label>
                                <div class="img" file-id="localPhoto" file-value="" file-size="400*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">营业执照</label>
                                <div class="img" file-id="licensePic" file-value="" file-size="250*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">法人身份证</label>
                                <div class="img" file-id="legalPhoto" file-value="" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">借记卡照片</label>
                                <div class="img" file-id="normalCard" file-value="" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">信用卡照片</label>
                                <div class="img" file-id="creditCard" file-value="" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                        </div>
                        <div id="settlement" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both;display:none;"></div>
                            <div style="display:none">
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">授信额度上限</label>
                                    <div class="col-sm-8">
                                        <select name="authMaxFlag" id="authMaxFlag">
                                            <option value="1" >开启</option>
                                            <option value="0" >关闭</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6" style="display:none">
                                    <label class="col-sm-4 control-label">额度上限</label>
                                    <div class="col-sm-8">
                                        <input type="text" validate-rule="required,floatss" validate-msg="required:额度上限不能为空" class="form-control" id="authMax" name="authMax" value="" maxlength="30" placeholder="请填额度上限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;display:none;"></div>
                            <div style="display:none">
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">提现手续费开关</label>
                                    <div class="col-sm-8">
                                        <select name="withdrawFeeFlag" id="withdrawFeeFlag">
                                            <option value="true" >开启</option>
                                            <option value="false" >关闭</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6" style="display:none">
                                    <label class="col-sm-4 control-label">提现手续费率（%）</label>
                                    <div class="col-sm-8">
                                        <input type="text" validate-rule="required,num" validate-msg="required:提现手续费率不能为空" class="form-control" id="withdrawFee" name="withdrawFee" value="" maxlength="30" placeholder="请填写手续费率">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div >
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">是否开启让利</label>
                                    <div class="col-sm-8">
                                        <select name="shareFlag" id="shareFlag">
                                            <option value="1" >开启</option>
                                            <option value="0" >关闭</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">让利利率（%）</label>
                                    <div class="col-sm-8">
                                        <input type="text"  validate-rule="required,num" validate-msg="required:让利利率不能为空" class="form-control" id="shareFee" name="shareFee" value="" maxlength="30" placeholder="请填写让利利率">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">结算周期</label>
                                <div class="col-sm-10">
                                    <select name="settlementLoop" id="settlementLoop">
                                        <option value="1" >日结</option>
                                        <option value="2" >周结</option>
                                        <option value="7" >半月</option>
                                        <option value="3" >月结</option>
                                        <option value="4" >季结</option>
                                        <option value="5" >半年</option>
                                        <option value="6" >一年</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="bind" class="ibox-content tab-pane">
                            <input type="button" id="new-card" class="btn btn-success" value="新增" />
                            <div class="fixed-table-container form-group">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:10%;min-width:150px;">开户行</th>
                                        <th style="width:10%;min-width:60px;">开户人</th>
                                        <th style="width:10%;min-width:80px;">预留手机</th>
                                        <th style="width:20%;min-width:80px;">身份证号</th>
                                        <th>卡号</th>
                                        <th style="width:5%;min-width:120px;">是否主卡</th>
                                        <th style="width:20%">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody" style="text-align:center;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <script>
        $(document).ready(function () {

            $("#authMaxFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#authMax").attr("disabled","disabled");
                }else{
                    $("#authMax").removeAttr("disabled");
                }
            });

            $("#withdrawFeeFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#withdrawFee").attr("disabled","disabled");
                }else{
                    $("#withdrawFee").removeAttr("disabled");
                }
            });

            $("#shareFlag").change(function(){
                var t = $(this).val();
                if(t == "false"){
                    $("#shareFee").attr("disabled","disabled");
                }else{
                    $("#shareFee").removeAttr("disabled");
                }
            });


            $("#area").area();//加载地区

            $("#oid-span").operaSelect();//加载商户

            $("#micId-span").loadIndustryType();//加载行业类型

            examine = false;

            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            var nodata = false;

            $("#new-card").click(function(){
                if(nodata) $("#tbody").html("");
                nodata = false;
                $tr = $('<tr>'+
                        '<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
                        '<td><input class="form-control" name="bank-name" type="text"/></td>'+
                        '<td><input class="form-control" name="bank-mobile" type="text"/></td>'+
                        '<td><input class="form-control" name="bank-idCard" type="text"/></td>'+
                        '<td><input class="form-control" name="bank-bankCard" type="text"/></td>'+
                        '<td>' +
                        '<select id="bank-isdefault" name="bank-isdefault">' +
                        '<option value="true">是</option>' +
                        '<option value="false">否</option>' +
                        '</select>' +
                        '</td>'+
                        '<td><input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
                        '</tr>');
                $("#tbody").append($tr);
                $tr.find("select").select();
                $tr.find(".bank-select").initBank();

                $tr.find(".del-card").click(function(){
                    $this = $(this);
                    li = layer.confirm('您确认删除吗?', {
                        btn: ['确定','取消'] //按钮
                    }, function(){
                        $this.parent().parent().remove();
                        layer.close(li);
                    }, function(){});
                });
            });

               /* function initCard(){
                    $("#tbody").empty();
                    $.post("${basePath!}/merchantInfo/loadBindCard.htm",{id:"${id!}"},function(d){
                        for(var i = 0 ; i < d.data.length ; i++){
                            select = '<select id="bank-isdefault" name="bank-isdefault"><option '+(d.data[i].isDefault?'selected':'')+' value="true">是</option><option '+(!d.data[i].isDefault?'selected':'')+' value="false">否</option></select>';
                            $tr = $('<tr>'+
                                    '<td class="bank-select" input-name="bank-bankSn" input-val="'+d.data[i].bankSn+'"></td>'+
                                    '<td><input class="form-control" id="bank-name" name="bank-name" type="text" value="'+d.data[i].name+'"/></td>'+
                                    '<td><input class="form-control" id="bank-mobile" name="bank-mobile" type="text" value="'+d.data[i].mobile+'"/></td>'+
                                    '<td><input class="form-control" id="bank-idCard" name="bank-idCard" type="text" value="'+d.data[i].idCard+'"/></td>'+
                                    '<td><input class="form-control" id="bank-bankCard" name="bank-bankCard" type="text" value="'+d.data[i].bankCard+'"/></td>'+
                                    '<td>'+select+'</td>'+
                                    '<td><input type="button" data-id="'+d.data[i].id+'" class="btn btn-info update-card" value="更新" /> <input type="button" data-id="'+d.data[i].id+'" class="del-card btn btn-danger" value="删除" /></td>'+
                                    '</tr>');
                            $("#tbody").append($tr);
                            $tr.find("select").select();
                            $tr.find(".bank-select").initBank();
                            $tr.find(".del-card").click(function(){
                                $this = $(this);
                                li = layer.confirm('您确认删除吗?', {
                                    btn: ['确定','取消'] //按钮
                                }, function(){
                                    $.post("${basePath!}/merchantInfo/delBindCard.htm",{id:$this.attr("data-id")},function(d){
                                        $this.parent().parent().remove();
                                        layer.close(li);
                                    },"json");
                                }, function(){});
                            });

                            $tr.find(".update-card").click(function(){
                                $this = $(this);
                                $p = $this.parent().parent();
                                $.post("${basePath!}/merchantInfo/updateBindCard.htm",{
                                    id:$this.attr("data-id"),
                                    bankCard:$p.find("#bank-bankCard").val(),
                                    bankName:$p.find("#bank-name").val(),
                                    bankSn:$p.find("#bank-bankSn").val(),
                                    idCard:$p.find("#bank-idCard").val(),
                                    isDefault:$p.find("#bank-isdefault").val(),
                                    mobile:$p.find("#bank-mobile").val()
                                },function(d){
                                    layer.alert(d.resultMsg);
                                    if(d.resultCode != -1){
                                        initCard();
                                    }
                                },"json");
                            });

                        }
                    },"json");
                }
                initCard();

                $("#new-card").click(function(){
                    if(nodata) $("#tbody").html("");
                    nodata = false;
                    $tr = $('<tr>'+
                            '<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
                            '<td><input id="bank-name" class="form-control" name="bank-name" type="text"/></td>'+
                            '<td><input id="bank-mobile" class="form-control" name="bank-mobile" type="text"/></td>'+
                            '<td><input id="bank-idCard" class="form-control" name="bank-idCard" type="text"/></td>'+
                            '<td><input id="bank-bankCard" class="form-control" name="bank-bankCard" type="text"/></td>'+
                            '<td><select id="bank-isdefault" name="bank-isdefault"><option value="true">是</option><option value="false">否</option></select></td>'+
                            '<td><input type="button" class="save-card btn btn-info" value="保存" /> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
                            '</tr>');
                    $("#tbody").append($tr);
                    $tr.find(".bank-select").initBank();
                    $tr.find(".save-card").click(function(){
                        $this = $(this);
                        $p = $this.parent().parent();
                        $.post("${basePath!}/merchantInfo/newBindCard.htm",{
                            id:'${id!}',
                            bankCard:$p.find("#bank-bankCard").val(),
                            bankName:$p.find("#bank-name").val(),
                            bankSn:$p.find("#bank-bankSn").val(),
                            idCard:$p.find("#bank-idCard").val(),
                            isDefault:$p.find("#bank-isdefault").val(),
                            mobile:$p.find("#bank-mobile").val()
                        },function(d){
                            layer.alert(d.resultMsg);
                            if(d.resultCode != -1){
                                initCard();
                            }
                        },"json");
                    });
                    $tr.find(".del-card").click(function(){
                        $this = $(this);
                        li = layer.confirm('您确认删除吗?', {
                            btn: ['确定','取消'] //按钮
                        }, function(){
                            $this.parent().parent().remove();
                            layer.close(li);
                        }, function(){});
                    });
                    $tr.find("select").select();
                });*/




            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});
            $(".submit-save").click(function (){
                if(!$("#_f").validate()) return;

                var totalData=document.getElementById("_f");
                var formData = new FormData(totalData);

                var arr = [];
                $('.bank-select').each(function (v) {
                    var obj = {};
                    obj.bankName = $("[name='bank-bankSn']").val();
                    obj.name = $("[name='bank-name']").val();
                    obj.mobile = $("[name='bank-mobile']").val();
                    obj.idCard = $("[name='bank-idCard']").val();
                    obj.bankCard = $("[name='bank-bankCard']").val();
                    obj.isDefault = $('#bank-isdefault option:selected').val();
                    arr.push(obj);
                })

                console.log(JSON.stringify(arr));
                formData.append("bind_card",JSON.stringify(arr));
                console.log(formData);

                var $this = $(this);

                $this.html("保 存 中");
               // $this.attr("disabled", true);
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                    url:'${basePath!}/platfrom/p_merchant/addMerchant',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:formData,
                    processData: false,                // jQuery不要去处理发送的数据
                    contentType: false,
                    success: function(data){ //成功
                        layer.close(index);
                        var obj = data;
                        /*if (obj.resultCode == "-1") {
                            parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
                            $this.html("保存内容");
                            $this.attr("disabled", false);
                        }*/

                        /*if (obj.resultCode == "1") {
                            //消息对话框
                            parent.messageModel("保存成功!");
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }*/
                    }
                });
            });

            var uploader = WebUploader.create({
                // 选完文件后，是否自动上传。
                auto: true,
                // swf文件路径
                swf: '${basePath!}/js/plugins/webuploader/Uploader.swf',
                // 文件接收服务端。
                server: '${basePath!}/fileUpload/upload.htm',
                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: '#img',
                // 只允许选择图片文件。
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }
            });
        });
    </script>
</body>

</html>
