<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 基本表单</title>
    <style>
		#bind th{
			font-size: 14px;
		}
	</style>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
		 <div>
			<input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
		</div>
	</div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
					<li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
					<li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
					<li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
					<#--<li><a data-toggle="tab" href="#jymx" aria-expanded="true"> 交易明细</a></li>
					<li><a data-toggle="tab" href="#jsmx" aria-expanded="true"> 结算明细</a></li>-->
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<input type="hidden" name="id" value="${mMerchantInfo.id!}"/>
						<div>
						  	<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户id</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
								${mMerchantInfo.id!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantInfo.name)!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户简称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.simpleName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">行业类型</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									<#--<#list  mlist as m>

									</#list>-->
									<#--<c:forEach items="${clist}" var="i">
										<c:if test="${id != null && i.id == entity.micId}">${i.name}</c:if>
									</c:forEach>-->
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人名称</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.legalName!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">法人证件号</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.idCard!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">手机号码</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.mobile!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group">
								<label class="col-sm-2 control-label">所属地区</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.address!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">详细地址</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${mMerchantInfo.address!}
								</label>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">状态</label>
								<label class="col-sm-8 control-label" style="text-align: left;"> 
								<#--	<c:if test="${id != null && entity.status == 1}">启用</c:if>
									<c:if test="${id != null && entity.status == 0}">禁用</c:if>-->
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					
					<!-- 商户审核信息 -->
					<div id="examine" class="ibox-content tab-pane">
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">商户性质</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantOtherInfo.nature)!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">品牌经营年限</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantOtherInfo.operYear)!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">经营面积</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantOtherInfo.operArea)!}
								</label>
							</div>
							
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">注册资金（万）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantOtherInfo.regMoney)!}
								</label>
							</div>
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">场地照片</label>
							<img src="${res!}${(mMerchantOtherInfo.localPhoto)!}" style="width:400px;height:400px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">营业执照</label>
							<img src="${res!}${(mMerchantOtherInfo.licensePic)!}" style="width:250px;height:400px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">法人身份证</label>
							<img src="${res!}${(mMerchantOtherInfo.legalPhoto)!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">借记卡照片</label>
							<img src="${res!}${(mMerchantOtherInfo.normalCard)!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">信用卡照片</label>
							<img src="${res!}${(mMerchantOtherInfo.creditCard)!}" style="width:400px;height:250px;" />
						</div>
						<div class="hr-line-dashed" style="clear:both"></div>
						
					</div>
					<div id="settlement" class="ibox-content tab-pane">
						<div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">让利利率（%）</label>
								<label class="col-sm-8 control-label" style="text-align: left;">
									${(mMerchantOtherInfo.shareFee)!}
								</label>
							</div>
						</div>
						
						<div class="hr-line-dashed" style="clear:both"></div>
                           	 <div class="form-group col-sm-6">
									<label class="col-sm-4 control-label">结算周期</label>
									<select name="settlementLoop" class="col-sm-8" id="settlementLoop">
										<option value="1"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="1">selected</#if> >日结</option>
										<option value="2"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="2">selected</#if> >周结</option>
                                        <option value="7"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="7">selected</#if> >半月</option>
										<option value="3"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="3">selected</#if> >月结</option>
										<option value="4" <#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="4">selected</#if> >季度结</option>
										<option value="5" <#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="5">selected</#if>  >半年结</option>
										<option value="6"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="6">selected</#if> >年结</option>
									</select>
                           		 </div>
						<div class="hr-line-dashed" style="clear:both"></div>
					</div>
					<div id="bind" class="ibox-content tab-pane">
						<div class="fixed-table-container form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width:10%;min-width:150px;">开户行</th>
										<th style="width:10%;min-width:60px;">开户人</th>
										<th style="width:10%;min-width:80px;">预留手机</th>
										<th style="width:20%;min-width:80px;">身份证号</th>
										<th>卡号</th>
										<th style="width:5%;min-width:120px;">是否主卡</th>
									</tr>
								</thead>
								<tbody id="tbody" style="text-align:center;">
								
								</tbody>
							</table>
						</div>
					</div>
					<div id="jymx" class="ibox-content tab-pane">
						<div class="ibox-content">
							<form id="jymx-form" class="form-horizontal" method="post">
								<#--<input type="hidden" id="mnum" name="mnum" value="${entity.mnum!}"/>
								<input type="hidden" name="oid" value="${entity.oid!}"/>-->
                                <input type="hidden" name="oid" value="${id!}"/>
								<div>
									<div class="form-group col-sm-12">
										<h3>交易总额：¥ <span id="totalJymxAmount">--</span> 元</h3>
									</div>
								</div> 
								<div>
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">交易时间</label>
										<div class="col-sm-9">
											<div class="col-sm-6" style="padding-left:0px;"> 
												<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
											</div>
											<div class="col-sm-6" style="padding-left:0px;">
												<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
											</div>
										</div>
									</div>
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">用户名称</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="realName" name="realName" maxlength="30" placeholder="请填写用户名称">
										</div>
									</div>
								</div> 
								<div>
									<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
										<input class="btn btn-success" id="jymx-search" type="button" value=" 查 询 " />
										<input class="btn btn-info" id="daochu" type="button" value=" 下 载 报 表 " />
									</div>
								</div>
							</form>
							<div class="project-list" id="jymx-list"></div>
						</div>
					</div>
					<div id="jsmx" class="ibox-content tab-pane">
						<div class="ibox-content">
							<form id="jsmx-form" class="form-horizontal">
								<#--<input type="hidden" name="mnum" value="${entity.mnum}"/>
								<input type="hidden" name="oid" value="${entity.oid}"/>-->
                                <input type="hidden" name="oid" value="${id!}"/>
								<div>
									<div class="form-group col-sm-6">
										<h3>结算总额：¥ <span id="s">--</span> 元</h3>
									</div>
									<div class="form-group col-sm-6">
										<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>
									</div>
								</div>
								<div>
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">交易时间</label>
										<div class="col-sm-9">
											<div class="col-sm-6" style="padding-left:0px;">
												<input type="text" class="form-control" name="startDate" id="startDateJs" style="width:100%" maxlength="30" placeholder="">
											</div>
											<div class="col-sm-6" style="padding-left:0px;">
												<input type="text" class="form-control" name="endDate" id="endDateJs" style="width:100%" maxlength="30" placeholder="">
											</div>
										</div>
									</div>
									
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">商户名称</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" name="manme" maxlength="30" placeholder="请填写商户名称">
										</div>
									</div>
									
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">商户id</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" name="mnum" maxlength="30" placeholder="请填写商户id">
										</div>
									</div>
									
									<div class="form-group col-sm-4">
										<label class="col-sm-3 control-label">结算状态</label>
										<div class="col-sm-9">
											<select id="status" name="status">
												<option value="">全部</option>
												<option value="0">待确认</option>
												<option value="1">待结算</option>
												<option value="2">已结算</option>
											</select>
										</div>
									</div>
								</div>
								<div>
									<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
										<input class="btn btn-success" id="jsmx-search" type="button" value=" 查 询 " />
									</div>
								</div>
							</form>
							<div class="project-list" id="jsmx-list">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script>
        var context= "${basePath!}";
        var context2= "${basePath!}";

		$(document).ready(function () {
			$("#area").area();
			examine = false;
			$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
				  var target =e.target.toString(); //$(e.target)[0].hash;
				  if( target.indexOf('examine')>0 && !examine){
					$(".img").uploadImg();
					examine = true;
				  }
		  	});
			var nodata = false;

            initCard();//加载卡信息

			function initCard(){
				$("#tbody").empty();
				$.post("${basePath!}/platfrom/p_merchant/loadBindCardInfo",{id:"${id!}"},function(d){
					for(var i = 0 ; i < d.data.length ; i++){
						select = d.data[i].isDefault?"是":"否";
						$tr = $('<tr style="text-align:left;">'+
								'<td class="bank-select" input-name="bank-bankSn" input-val="'+d.data[i].bankSn+'"></td>'+
								'<td>'+d.data[i].name+'</td>'+
								'<td>'+d.data[i].mobile+'</td>'+
								'<td>'+d.data[i].idCard+'</td>'+
								'<td>'+d.data[i].bankCard+'</td>'+
								'<td>'+select+'</td>'+
							'</tr>');
						$("#tbody").append($tr);
						$tr.find("select").select();
						$tr.find(".bank-select").initBank();
						/*	$tr.find(".del-card").click(function(){
							$this = $(this);
							li = layer.confirm('您确认删除吗?', {
								btn: ['确定','取消'] //按钮
							}, function(){
								$.post("${basePath!}/merchantInfo/delBindCard.htm",{id:$this.attr("data-id")},function(d){
									$this.parent().parent().remove();
									layer.close(li);
								},"json");
							}, function(){});
						});*/
						
						/*$tr.find(".update-card").click(function(){
							$this = $(this);
							$p = $this.parent().parent();
							$.post("${basePath!}/merchantInfo/updateBindCard.htm",{
								id:$this.attr("data-id"),
								bankCard:$p.find("#bank-bankCard").val(),
								bankName:$p.find("#bank-name").val(),
								bankSn:$p.find("#bank-bankSn").val(),
								idCard:$p.find("#bank-idCard").val(),
								isDefault:$p.find("#bank-isdefault").val(),
								mobile:$p.find("#bank-mobile").val()
							},function(d){
								layer.alert(d.resultMsg);
								if(d.resultCode != -1){
									initCard();
								}
							},"json");
						});*/
						
					}
				},"json");
			}

			
			/*$("#new-card").click(function(){
				if(nodata) $("#tbody").html("");
				nodata = false;
				$tr = $('<tr>'+
						'<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
						'<td><input id="bank-name" class="form-control" name="bank-name" type="text"/></td>'+
						'<td><input id="bank-mobile" class="form-control" name="bank-mobile" type="text"/></td>'+
						'<td><input id="bank-idCard" class="form-control" name="bank-idCard" type="text"/></td>'+
						'<td><input id="bank-bankCard" class="form-control" name="bank-bankCard" type="text"/></td>'+
						'<td><select id="bank-isdefault" name="bank-isdefault"><option value="true">是</option><option value="false">否</option></select></td>'+
						'<td><input type="button" class="save-card btn btn-info" value="保存" /> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
					'</tr>');
				$("#tbody").append($tr);
				$tr.find(".bank-select").initBank();
				$tr.find(".save-card").click(function(){
					$this = $(this);
					$p = $this.parent().parent();
					$.post("${basePath!}/merchantInfo/newBindCard.htm",{
						id:'${id!}',
						bankCard:$p.find("#bank-bankCard").val(),
						bankName:$p.find("#bank-name").val(),
						bankSn:$p.find("#bank-bankSn").val(),
						idCard:$p.find("#bank-idCard").val(),
						isDefault:$p.find("#bank-isdefault").val(),
						mobile:$p.find("#bank-mobile").val()
					},function(d){
						layer.alert(d.resultMsg);
						if(d.resultCode != -1){
							initCard();
						}
					},"json");
				});
				$tr.find(".del-card").click(function(){
					$this = $(this);
					li = layer.confirm('您确认删除吗?', {
						btn: ['确定','取消'] //按钮
					}, function(){
						$this.parent().parent().remove();
						layer.close(li);
					}, function(){});
				});
				$tr.find("select").select();
			});*/
			
			$("select").select();
			$(".cancel").click(function(){parent.closeLayer();});

			/*$(".submit-save").click(function (){
				if(!$("#_f").validate()) return;
				var $this = $(this);
				$this.html("保 存 中");
				$this.attr("disabled", true);
				var index = layer.load(2, {time: 10*1000});
				$.ajax({
					url:'${basePath!}/merchantInfo/<c:choose><c:when test="${id!}">merchantInfoEditAjaxSubmit</c:when><c:otherwise>merchantInfoAddAjaxSubmit</c:otherwise></c:choose>.htm',
					type:'post', //数据发送方式
					dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
					data:$("#_f").serialize(),
					success: function(data){ //成功
						layer.close(index);
						var obj = data;
						if (obj.resultCode == "-1") {
							parent.messageModel(obj.resultMsg==null?"系统异常":obj.resultMsg);
							$this.html("保存内容");
							$this.attr("disabled", false);
						}
						
						if (obj.resultCode == "1") {
							//消息对话框
							parent.messageModel("保存成功!");
							parent.c.gotoPage(null);
							parent.closeLayer();
						}
					}
				});
			});*/
			
			var uploader = WebUploader.create({
		  		 // 选完文件后，是否自动上传。
		  		 auto: true,
		  		 // swf文件路径
		  		 swf: '/grant-manager/js/plugins/webuploader/Uploader.swf',
		  		 // 文件接收服务端。
		  		 server: '/grant-manager/fileUpload/upload.htm',
		  		 // 选择文件的按钮。可选。
		  		 // 内部根据当前运行是创建，可能是input元素，也可能是flash.
		  		 pick: '#img',
		  		 // 只允许选择图片文件。
		  		 accept: {
		  			  title: 'Images',
		  			  extensions: 'gif,jpg,jpeg,bmp,png',
		  			  mimeTypes: 'image/*'
		  		 }
		  	});

			laydate({
				elem: '#startDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDate', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#startDateJs', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			laydate({
				elem: '#endDateJs', 
				event: 'click', //触发事件
			  	format: 'YYYY-MM-DD' //日期格式
			});
			$("#repayType").select();
			$("#tradeType").select();
			

			//交易明细里列表
			/*var $pager_jymx = $("#jymx-list").pager({
				url:"${basePath!}/p_merchant/tradingDetailManagerList?mid=${id!}",
				formId:"jymx-form",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>交易单号</th>'+
								'<th>商户id</th>'+
								'<th>商户名称</th>'+
								'<th>行业类型</th>'+
								'<th>用户id</th>'+
								'<th>用户名称</th>'+
								'<th>交易类型</th>'+
								'<th>交易时间</th>'+
								'<th>交易金额</th>'+
								'<th>结算金额</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{serialNo}</td>'+
								'<td>{mid}</td>'+
								'<td>{legalName}</td>'+
								'<td>{nature}</td>'+
								'<td>{uid}</td>'+
								'<td>{realName}</td>'+
								'<td>{tradingType}</td>'+
								'<td>{tradingDate}</td>'+
								'<td>{tradeAmount}</td>'+
								'<td>{actualAmount}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						/!*noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'*!/
				},
				callbak: function(result){
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					
					loadJymxAmount();//加载交易总金额
				}
			});*/
			
			$("#jymx-search").click(function(){
				$pager_jymx.gotoPage(1);
			})
			
			/*$("#daochu").click(function(){
				window.location.href = "${basePath!}/merchantInfo/toExcel.htm?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&uname="+$("#uname").val()+"&mnum="+$("#mnum").val();
				/!*
				$("#jymx-form").attr("action","${basePath!}/merchantInfo/toExcel.htm");
				$("#jymx-form").submit(); *!/
			});*/

            $("#daochu").click(function(){
                $("#jymx-form").attr("action","${basePath!}/p_merchant/tradingPoi");
                $("#jymx-form").submit();
            });

	
			function loadJymxAmount(){
				$.ajax({
					url:"${basePath!}/p_merchant/totalJymxAmount",
				    type:'POST', //GET
				    data:$("#jymx-form").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#totalJymxAmount").html(d.data);
				    }
				});
			}
			
			
			//结算明细里列表
			/*var $pager_jsmx = $("#jsmx-list").pager({
				url:"${basePath!}/p_merchant/clearDetail?mid=${id!}",
				formId:"jsmx-form",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group pager-content">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
								'<th>结算单号</th>'+
								'<th>商户id</th>'+
								'<th>商户名称</th>'+
								'<th>出账时间</th>'+
								'<th>结算时间</th>'+
								'<th>结算状态</th>'+
								'<th>结算金额</th>'+
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{serialNo}</td>'+
								'<td>{mid}</td>'+
								'<td>{legalName}</td>'+
								'<td>{outSettlementDate}</td>'+
								'<td>{settlementTime}</td>'+
								'<td>{status}</td>'+
								'<td>{amount}</td>'+
							'</tr>',
						footer:'</tbody></table>',
						noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					var binfo = null;
					$.ajax({
						url:context+"/js/bank/bank_info.json",
					    type:'POST', //GET
					    timeout:30000,    //超时时间
					    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
					    async:false,
					    success:function(data){
					    	binfo = data;
					    }
					});
					$('input').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
						increaseArea: '20%' // optional
					});
					// check 全部选中
					$('.checks').on('ifChecked', function(event){
						 $("input[id^='check-id']").iCheck('check');
					});
					
					// check 全部移除
					$('.checks').on('ifUnchecked', function(event){
						 $("input[id^='check-id']").iCheck('uncheck');
					});
					loadJsmxAmount();
				}
			});*/
			
			//jymx-search

			$("#jsmx-search").click(function(){
				console.log(11);
				$pager_jsmx.gotoPage(1);
			})
			
			function loadJsmxAmount(){
				$.ajax({
					url:"${basePath!}/p_merchant/clearMoney",
				    type:'POST', //GET
				    data:$("#jsmx-form").serialize(),
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(d){
				    	$("#uns").html(d.data.waitSettled);
				    	$("#s").html(d.data.alreadySettled);
				    }
				});
			}
		});
	</script>
</body>

</html>
