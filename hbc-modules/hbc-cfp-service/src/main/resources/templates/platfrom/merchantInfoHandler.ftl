
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
                    <li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
                    <#--<li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>-->
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                    <div class="tab-content">
                        <div id="base" class="ibox-content active tab-pane">
                            <input type="hidden" name="id" value="${mMerchantInfo.id!}"/>

                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:商户名称不能为空" id="name" name="name" value="${(mMerchantInfo.name)!}" maxlength="30" placeholder="请填写商户名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户简称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,maxLength[6],ffstring" validate-msg="required:商户简称不能为空,maxLength:商户简称最大只能输入6个字符" id="simpleName" name="simpleName" value="${(mMerchantInfo.simpleName)!}" maxlength="30" placeholder="请填写商户简称">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">登陆密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" id="pwd" name="pwd" class="form-control" validate-rule="pwdss" value="">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">重复密码</label>
                                    <div class="col-sm-8">
                                        <input type="password" id="pwd1" class="form-control" validate-rule="">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:法人名称不能为空" id="legalName" name="legalName" value="${(mMerchantInfo.legalName)!}" maxlength="30" placeholder="请填写法人名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人证件号</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:法人证件号不能为空" id="idCard" name="idCard" value="${(mMerchantInfo.idCard)!}" maxlength="30" placeholder="请填写法人证件号">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">手机号码</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:手机号码不能为空" id="mobile" name="mobile" value="${(mMerchantInfo.mobile)!}" maxlength="30" placeholder="请填写手机号码">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">详细地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:详情地址不能为空" id="address" name="address" value="${(mMerchantInfo.address)!}" maxlength="30" placeholder="请填写详细地址">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">所属地区</label>
                                <div class="col-sm-10" id="area" data-name="area" data-value="${(mMerchantInfo.area)!}">

                                </div>
                            </div>
                            <div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">所属运营商</label>
                                    <div class="col-sm-8">
                                        <select name="oid" id="oid">
                                            <option value="">请选择运营商</option>
											<#list olist as o>
												<#if mMerchantInfo.oid== o.id>
													<option value="${(o.id)!}" selected>${(o.name)!}</option>
												<#else>
                                           		 <option value="${(o.id)!}">${(o.name)!}</option>
												</#if>
											</#list>
                                        </select>
                                    </div>


                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">行业类型</label>
                                    <div class="col-sm-8">
                                        <select name="micId" id="micId">
                                            <option value="">请选择分类</option>
												<#list mlist as m>
													<#if (mMerchantInfo.micId)?? && mMerchantInfo.micId==m.id>
														<option value="${(m.id)!}" selected>${(m.name)!}</option>
													<#else>
														<option value="${(m.id)!}">${(m.name)!}</option>
													</#if>
												</#list>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">状态</label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status">
												<#if mMerchantInfo.status =="1">
                                                    <option value="1" selected>启用</option>
                                                    <option value="0"  >禁用</option>
												<#else >
														 <option value="1" >启用</option>
                                                    <option value="0" selected >禁用</option>
												</#if>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>

                        <!-- 商户审核信息 -->
                        <div id="examine" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户性质</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:商户性质不能为空" id="nature" name="nature" value="${(mMerchantOtherInfo.nature)!}" maxlength="30" placeholder="请填写商户性质">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">品牌经营年限</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:品牌经营年限不能为空" id="operYear" name="operYear" value="${(mMerchantOtherInfo.operYear)!}" maxlength="30" placeholder="请填写品牌经营年限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">经营面积</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:经营面积不能为空" id="operArea" name="operArea" value="${(mMerchantOtherInfo.operArea)!}" maxlength="30" placeholder="请填写经营面积">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">注册资金（万）</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:注册资金不能为空" id="regMoney" name="regMoney" value="${(mMerchantOtherInfo.regMoney)!}" maxlength="30" placeholder="请填写注册资金">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商户电话</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:商户电话不能为空" id="phoneNumber" name="phoneNumber" value="${(mMerchantOtherInfo.phoneNumber)!}" maxlength="30" placeholder="请填写商户店铺电话">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">商家介绍</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required" validate-msg="required:商家介绍不能为空" id="recommendGoods" name="recommendGoods" value="${(mMerchantOtherInfo.recommendGoods)!}" maxlength="30" placeholder="请填写商家介绍">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">场地照片</label>

                                <div class="img" file-id="localPhoto" file-value="${(mMerchantOtherInfo.localPhoto)!}"  file-size="400*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">营业执照</label>
                                <div class="img" file-id="licensePic" file-value="${(mMerchantOtherInfo.licensePic)!}" file-size="250*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">法人身份证</label>
                                <div class="img" file-id="legalPhoto" file-value="${(mMerchantOtherInfo.legalPhoto)!}" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">借记卡照片</label>
                                <div class="img" file-id="normalCard" file-value="${(mMerchantOtherInfo.normalCard)!}" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">信用卡照片</label>
                                <div class="img" file-id="creditCard" file-value="${(mMerchantOtherInfo.creditCard)!}" file-size="400*250">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                        </div>
                        <div id="settlement" class="ibox-content tab-pane">

                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div >
                                <#--<div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">是否开启让利</label>
                                    <div class="col-sm-8">
                                        <select name="shareFlag" id="shareFlag">
                                            <option value="1" <#if mMerchantOtherInfo.shareFlag?? && mMerchantOtherInfo.shareFlag==0> selected</#if >>开启</option>
                                            <option value="0"<#if mMerchantOtherInfo.shareFlag?? && mMerchantOtherInfo.shareFlag==1> selected</#if >>关闭</option>
                                        </select>
                                    </div>
                                </div>-->

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">让利利率（%）</label>
                                    <div class="col-sm-8">
                                        <input type="text"  validate-rule="required,num" validate-msg="required:让利利率不能为空" class="form-control" id="shareFee" name="shareFee" value="${(mMerchantOtherInfo.shareFee)!}" maxlength="30" placeholder="请填写让利利率">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">结算周期</label>
                                <div class="col-sm-10">
                                    <select name="settlementLoop" id="settlementLoop">
                                        <option value="1"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="1">selected</#if> >日结</option>
                                        <option value="2"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="2">selected</#if> >周结</option>
                                        <option value="7"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="7">selected</#if> >半月</option>
                                        <option value="3"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="3">selected</#if> >月结</option>
                                        <option value="4" <#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="4">selected</#if> >季度结</option>
                                        <option value="5" <#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="5">selected</#if>  >半年结</option>
                                        <option value="6"<#if mMerchantOtherInfo.settlementLoop?? && mMerchantOtherInfo.settlementLoop=="6">selected</#if> >年结</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="bind" class="ibox-content tab-pane">
                            <input type="button" id="new-card" class="btn btn-success" value="新增" />
                            <div class="fixed-table-container form-group">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:10%;min-width:150px;">开户行</th>
                                        <th style="width:10%;min-width:60px;">开户人</th>
                                        <th style="width:10%;min-width:80px;">预留手机</th>
                                        <th style="width:20%;min-width:80px;">身份证号</th>
                                        <th>卡号</th>
                                        <th style="width:5%;min-width:120px;">是否主卡</th>
                                        <th style="width:15%">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody" style="text-align:center;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
    <script type="text/javascript" src="${basePath}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${basePath!}/js/plugins/webuploader/webuploader.js"></script>
    <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>


    <script>
        var context= "${basePath!}";
        var context2= "${basePath!}";

        $(document).ready(function () {
           //  $(".img").uploadImg();//加载图片

            $("#oid").select();//加载运营商
            $("#micId").select();//加载行业类型
            $("#status").select();//加载装填
            $("#area").area();//渲染地区

          examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });

           // initCard();

            function initCard(){
                $("#tbody").empty();
                $.post("${basePath!}/platfrom/p_merchant/loadBindCardInfo",{id:"${mMerchantInfo.id!}"},function(d){
                    for(var i = 0 ; i < d.data.length ; i++){
                        if(d.data[i].isDefault=="1"){
                            select = '<select  name="bank-isdefault"><option selected value="1">是</option></select>';
                        }else{
                            select = '<select  name="bank-isdefault"><option selected value="0">否</option></select>';
                        }
                        $tr = $('<tr>'+
                                '<td class="bank-select" input-name="bank-bankSn" input-val="'+d.data[i].bankSn+'"></td>'+
                                '<td><input class="form-control"  name="bank-name" type="text" value="'+d.data[i].name+'"/></td>'+
                                '<td><input class="form-control"  name="bank-mobile" type="text" value="'+d.data[i].mobile+'"/></td>'+
                                '<td><input class="form-control"  name="bank-idCard" type="text" value="'+d.data[i].idCard+'"/></td>'+
                                '<td><input class="form-control"  name="bank-bankCard" type="text" value="'+d.data[i].bankCard+'"/></td>'+
                                '<td>'+select+'</td>'+
                                '<td><input type="button" data-id="'+d.data[i].id+'" class="del-card btn btn-danger" value="删除" /></td>'+
                                '</tr>');
                        $("#tbody").append($tr);
                        $tr.find("select").select();
                        $tr.find(".bank-select").initBank();

                    }
                },"json");
            }


           $("#new-card").click(function(){

                nodata = false;

                $tr = $('<tr>'+
                        '<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
                        '<td><input  class="form-control" name="bank-name" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-mobile" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-idCard" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-bankCard" type="text"/></td>'+
                        '<td><select  name="bank-isdefault"><option value="1">是</option><option value="0">否</option></select></td>'+
                        '<td> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
                        '</tr>');
                $("#tbody").append($tr);

                $tr.find(".bank-select").initBank();

                $tr.find(".del-card").click(function(){
                    $this = $(this);
                    li = layer.confirm('您确认删除吗?', {
                        btn: ['确定','取消'] //按钮
                    }, function(){
                        $this.parent().parent().remove();
                        layer.close(li);
                    }, function(){});
                });
                $tr.find("select").select();
            });

            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});

            $("#submit-save").click(function (){

                var totalData=document.getElementById("_f");
                var formData = new FormData(totalData);
                var arr = [];

                var pwd=$("#pwd").val();
                var pwd1=$("#pwd1").val();

                if(pwd!=pwd1){
                        layer.msg("密码不一致！")
                         return;
                };

              /*  $("#tbody").find("tr").each(function(){
                    var obj = {};
                    var tdArr = $(this).children();
                    //var bankSn=  tdArr.eq(0).find('option:selected').val();
                    obj.bankSn = tdArr.eq(0).find('option:selected').val();
                    obj.name =tdArr.eq(1).find('input').val();
                    obj.mobile =tdArr.eq(2).find('input').val();
                    obj.idCard = tdArr.eq(3).find('input').val();
                    obj.bankCard = tdArr.eq(4).find('input').val();
                    obj.isDefault = tdArr.eq(5).find('option:selected').val();
                    arr.push(obj);
                });
                formData.append("bind_card",JSON.stringify(arr));*/
                var $this = $(this);
                $this.html("保 存 中");

                // $this.attr("disabled", true);
                var index = layer.load(2, {time: 10*1000});

               $.ajax({
                    url:'${basePath!}/platfrom/p_merchant/updateMerchantInfo',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:formData,
                    processData: false,                // jQuery不要去处理发送的数据
                    contentType: false,
                    success: function(data){ //成功
                        layer.close(index);
                        //消息对话框
                        if(data.code==0){
                            parent.messageModel(data.msg);
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }else{
                            layer.msg(data.msg)
                        }
                    }
                });

            });
        });
    </script>
</body>

</html>
