<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商户管理</h5>
                        <div class="ibox-tools">
                           <#-- <a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建商户</a>-->
                        </div>
						<div class="ibox-tools">
						</div>
					</div>
					<div class="ibox-content">
					<div id="sh">
					</div>
						<div id="sh1">
						
				<span id="todayAdd"></span>
					</div>
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 form-inline">
									<div class="form-group">
									   <label for="exampleInputName2">商户名称：</label>
										<input type="text" name="merchantName" placeholder="请输入商户名称" id="merchantName" class="form-control">
									</div>
								</div>
								 <div class="col-lg-4 col-md-4 col-sm-4 form-inline">
									<label for="exampleInputEmail2" class="sr-only">所属运营商</label>
                                     <label class="control-label">运营商:</label>
                                     <span id="oid-span" ele-id="oid" ele-name="oid" style="padding-left: 14px"></span>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 form-group form-inline">
									<label for="exampleInputName2">申请时间:</label>
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:30%" maxlength="30" placeholder="">
											-
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:30%" maxlength="30" placeholder="">
								</div>
							</div>
							<div class="row" style="margin-top:20px;padding-bottom:50px">
								 <div class="col-lg-4 col-md-4 col-sm-4 form-inline">
                                   <div class="form-group control-label">
                                       <label for="exampleInputName2">商户状态：</label>
                                       <select id="status" name="status">
                                         <option value="">全部</option>
                                         <option value="0">禁用</option>
                                         <option value="1">启用</option>
                                        <!--  <option value="2">待审核</option> -->
                                       </select>
                                   </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-4 form-inline">
                                 	<div class="form-group"> 
								       <label class="control-label">行业类型:</label>

                                        <span id="micId-span" ele-id="micId" ele-name="micId"></span>

								    </div>
                                 </div>
                                <div class="form-group col-lg-1 col-md-1 col-sm-1" style="text-align:center">
                                    <input type="button" id="search" class="btn btn-warning" value="搜索" />
                                </div>
							</div>
						</form>
						<div class="project-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){

        $("#oid-span").operaSelect();//加载运营商
        $("#micId-span").loadIndustryType();//加载行业类型
        $("select").select();


   		laydate({
			elem: '#startDate',
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate',
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
   		
   		
		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/p_merchant/MerchantManagerPageInfo",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>所属运营商</th>'+
							'<th>法人名称</th>'+
							'<th>商户手机</th>'+
							'<th>商户名称</th>'+
							'<th>行业类型</th>'+
							'<th>申请日期</th>'+
							'<th>审核日期</th>'+
							'<th>商户状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{oname}</td>'+
							'<td>{legalName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{name}</td>'+
							'<td>{merchantCate}</td>'+
							'<td>{createDate}</td>'+
							'<td>{examineDate}</td>'+
							'<td id="status_{id}"></td>'+
							'<td>'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){

				var list = result.data.records;
				for(var i = 0 ; i < result.data.records.length ; i++){
					console.log(list[i].status);
					if(list[i].status == 1)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
					else if(list[i].status == 0)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
					else
						$("#status_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>待审核</span>");
						
				}
				
				//删除事件
				/*$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
					var bo = parent.deleteData('${basePath!}/merchantInfo/jumpMerchantInfoDel.htm',param,$pager);
				});*/
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('修改商户-'+title,'${basePath!}/platfrom/p_merchant/update?id='+id,'1000px','700px',$pager);
				});
				//详情页面
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
					parent.openLayer('商户详情-'+title,'${basePath!}/platfrom/p_merchant/merchantDetail?id='+id,'1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});

                loadAmount();//加载顶部商户数量
			}
		});

        function loadAmount(){
            $.ajax({
                url:"${basePath!}/platfrom/p_merchant/todayAddMerchant",
                type:'POST', //GET
                data:{},
                timeout:30000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(d){
                    console.log(d);
                    $("#sh").html("商户共计："+d.data.totalNum+" 家");
					$("#todayAdd").html("今日新增："+d.data.merchantNum+" 家")
                }
            });
        }
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		/*	$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
				parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
				var bo = parent.deleteData('${basePath!}/merchantInfo/delete.htm',param,$pager);
			}
		});*/
		
		//打开新增页面
		/*$("#new").click(function (){
            //parent.openLayer('添加商户','${basePath!}/p_merchant/addMerchant','1000px','700px',$pager);
            parent.openLayer('添加商户','${basePath!}/platfrom/p_merchant/jumpMerchantInfoAdd','1000px','700px',$pager);
		});*/
	});
	
</script>

	</body>
</html>
