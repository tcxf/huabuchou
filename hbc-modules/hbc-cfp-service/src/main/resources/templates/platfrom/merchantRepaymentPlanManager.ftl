<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>还款记录</h5>
					</div>
					<div class="ibox-content">
						<form method ="post" id="searchForm" class="form-horizontal">
							<input id = "status" type="hidden" name="status" value="2"/>
							<div>
								<div class="form-group col-sm-12">
									<h3>待还款金额：¥ <span id="totalAmount">--</span> 元  = ¥ <span id="currentCorpus">--</span> 元 (本金)+ ¥ <span id="currentFee">--</span> 元(利息) + ¥ <span id="lateFee">--</span> 元(滞纳金)</h3>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">还款日</label>
									<div class="col-sm-9">
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
										</div>
										<div class="col-sm-6" style="padding-left:0px;">
											<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
										</div>
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">流水单号</label>
									<div class="col-sm-9">
										<input id="ls" type="text" class="form-control" name="repaymentSn" maxlength="30" placeholder="请填写流水单号">
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">用户姓名</label>
									<div class="col-sm-9">
										<input id="yhname" type="text" class="form-control" name="uname" maxlength="30" placeholder="请填写用户姓名">
									</div>
								</div>
								
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">手机号码</label>
									<div class="col-sm-9">
										<input id="phone" type="text" class="form-control" name="mobile" maxlength="30" placeholder="请填写用户手机号码">
									</div>
								</div>
								
<!-- 								<div class="form-group col-sm-6"> -->
<!-- 									<label class="col-sm-3 control-label">出账状态</label> -->
<!-- 									<div class="col-sm-9"> -->
<!-- 										<select id="isOut" name="isOut"> -->
<!-- 											<option value="">全部</option> -->
<!-- 											<option value="true">已出帐</option> -->
<!-- 											<option value="false">未出账</option> -->
<!-- 										</select> -->
<!-- 									</div> -->
<!-- 								</div> -->
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
									<input id="huankuan" class="btn btn-info" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#huankuan").click(function(){
			$("#searchForm").attr("action","${hsj}/repaymentPlan/rmhuankuan.htm");
			$("#searchForm").submit();
		});
	
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		$("#isOut").select();
   		$("#tradeType").select();
		var $pager = $("#data").pager({
			url:"${hsj}/repaymentPlan/rmRepaymentPlanManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>流水单号</th>'+
							
							'<th>用户手机</th>'+
							'<th>用户姓名</th>'+
							'<th>应还本金</th>'+
							'<th>滞纳金</th>'+
							'<th>分期利息</th>'+
							'<th>本期应还</th>'+
							'<th>期数</th>'+
							'<th>还款日</th>'+
							'<th>操作</th>'+
							'<th></th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{repaymentSn}</td>'+
							
							'<td>{mobile}</td>'+
							'<td>{uname}</td>'+
							'<td>¥ {currentCorpus} 元</td>'+
							'<td>¥ {lateFeeAct} 元</td>'+
							'<td>¥ {currentFee} 元</td>'+
							'<td id="yh_{id}"></td>'+
							'<td>{currentTime}/{totalTime}</td>'+
							'<td>{repaymentDateStr}</td>'+
							'<td><a href="javascript:void(0);" data="{rdId}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 查询明细 </a></td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				
				for(var i = 0 ; i < result.list.length ; i++){
					var item = result.list[i];
					$("#yh_"+item.id).html("¥ " + (parseFloat(item.currentCorpus)+parseFloat(item.lateFeeAct)+parseFloat(item.currentFee)).toFixed(2) + " 元");
				}
				
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				loadAmount();
				
				$(".hk").click(function(){
					var id =$(this).attr("data");
					parent.confirm("是否确认标记状态为已还？",function(){
						$.post("<%=path%>/repaymentPlan/doRmRepayment.htm",{id:id},function(d){
							parent.messageModel(d.resultMsg);
							$pager.gotoPage($pager.pageNumber);
						},"json");
					});
				});
				
				$(".mx").click(function(){
					window.location.href ="<%=path%>/repaymentPlan/merchantRepaymentPlans.htm?id="+$(this).attr("data");		
				});
			}
		});
		

		function loadAmount(){
			$.ajax({
				url:"<%=path%>/repaymentPlan/rmTotalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data.formatTotalAmount);
			    	$("#currentCorpus").html(d.data.formatCurrentCorpus);
			    	$("#currentFee").html(d.data.formatCurrentFee);
			    	$("#lateFee").html(d.data.formatLateFee);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
</script>

	</body>
</html>
