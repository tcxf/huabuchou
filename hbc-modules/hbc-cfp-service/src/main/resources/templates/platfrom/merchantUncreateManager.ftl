<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>未出账单</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">商户ID</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="mnum" maxlength="30" placeholder="请填写用户id">
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">商户名称</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="mname" maxlength="30" placeholder="请填写用户姓名">
									</div>
								</div>
								<div class="form-group col-sm-6 col-lg-4">
									<label class="col-sm-3 control-label">商户手机</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="mobile" maxlength="30" placeholder="请填写用户手机">
									</div>
								</div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		var $pager = $("#data").pager({
			url:"${hsj}/uncreate/rmUncreateManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>商户ID</th>'+
							'<th>商户手机</th>'+
							'<th>商户名称</th>'+
							'<th>账单总额</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{unum}</td>'+
							'<td>{mobile}</td>'+
							'<td>{realName}</td>'+
							'<td>{amount}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="4"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				

				$(".tlog").click(function(){
					window.location="<%=path%>/uncreate/uncreateLog.htm?id="+$(this).attr("data");
				});

				$(".send").click(function(){
					$.post("<%=path%>/uncreate/send.htm",{id:$(this).attr("data")},function(d){
						parent.messageModel(d.resultMsg);
						if(d.resultCode == "1"){
							$pager.gotoPage($pager.pageNumber);
						}
					},"json");
				});
				
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
