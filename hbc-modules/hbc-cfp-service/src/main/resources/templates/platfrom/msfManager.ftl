<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
    <style>
		#searchForm h3{
			font-size: 14px;
		}
        select{
            height: 25px!important;
            width: 100px!important;
        }
	</style>
	<meta name="keywords" content="">
	<meta name="description" content="">

<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>商家让利</h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <a href="${basePath!}/platfrom/tradingDetail/Qumsf">   <button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
                                </div>
                                <div class="col-sm-8">

                                </div>
                            </div>

							<div>
								<div class="form-group col-sm-12">
									<h3>商户让利总额：¥ <span id="totalAmount">--</span> 元</h3>
								</div>
							</div>
								<div class="row">
                                    <div class="form-group col-sm-6 col-lg-4">
                                        <label class="col-sm-4 control-label">交易时间</label>
                                        <div class="col-sm-8">
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                            <div class="col-sm-6" style="padding-left:0px;">
                                                <input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-sm-6 col-lg-4">
                                        <label class="col-sm-4 control-label">下单用户</label>
                                        <div class="col-sm-8">
                                            <input id="manme" type="text" class="form-control" name="realName" maxlength="30" placeholder="请填写商户名称">
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 col-lg-4">
                                        <label class="col-sm-4 control-label">商户名称</label>
                                        <div class="col-sm-8">
                                            <input id="uname" type="text" class="form-control" name="name" maxlength="30" placeholder="请填写用户名称">
                                        </div>
                                    </div>
								</div>

								<div class="row">
                                    <div class="form-group col-sm-6 col-lg-4">
                                        <label class="col-sm-4 control-label">所属运营商</label>
                                        <div class="col-sm-8">
                                            <select name="opid" id="opid">
                                                <option value="">请选择运营商</option>
											<#list list as list >
                                                    <option id="id" value="${list.id!}">${list.name!}</option>
											</#list>

                                            </select>
                                        </div>
                                    </div>

                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-4 control-label">是否使用优惠卷</label>
                                            <div class="col-sm-8">
                                                <select id="isUseDiscout" name="isUseDiscout">
                                                    <option value="">全部</option>
                                                    <option value="0">已使用</option>
                                                    <option value="1">未使用</option>
                                                </select>
                                            </div>
                                        </div>
							</div>
							<div>
								<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
									<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
                                    <input class="btn btn-info" id="xj" type="button" value=" 下 载 报 表 " />
								</div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
        //下载报表
        $("#xj").click(function(){
            $("#searchForm").attr("action","${basePath!}/platfrom/tradingDetail/SJtradingPoi");
            $("#searchForm").submit();
        });

		$("#oid-span").operaSelect();
        $("#opid").select();
        $("#isUseDiscout").select();
		$("#sjrl").click(function(){
		var tradeType = $("#tradeType option:selected").val();
		var operaName =$("#oid-span option:selected").val();
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD 00:00:00' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD 23:59:59' //日期格式
		});
   		$("#tradeType").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/tradingDetail/tradingDetailManagerList",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>下单用户</th>'+
							'<th>用户手机</th>'+
							'<th>商户名称</th>'+
                			'<th>商户手机</th>'+
                			'<th>所属运营商</th>'+
							'<th>交易状态</th>'+
							'<th>交易时间</th>'+
							'<th>交易总额</th>'+
							'<th>优惠金额</th>'+
							'<th>实付金额</th>'+
							'<th>商户让利金额</th>'+
                			'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{realName}</td>'+
							'<td>{mobile}</td>'+
							'<td>{name}</td>'+
				            '<td>{mmobile}</td>'+
                			'<td>{opname}</td>'+
							'<td>{paymentStatus}</td>'+
							'<td>{tradingDate}</td>'+
							'<td>{tradeAmount}</td>'+
							'<td>{discountAmount}</td>'+
							'<td>{actualAmount}</td>'+
							'<td>{operatorShareFee}</td>'+
				            '<td><a href="javascript:void(0);"  data="{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> </td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});

                $(".detail").click(function () {
                    var id=$(this).attr("data");
                    window.location.href="${basePath!}/platfrom/tradingDetail/QueryOneMsf?tid="+id;
                })
				
				loadAmount();
			}
		});
		
		function loadAmount(){
			$.ajax({
				url:"${basePath}/platfrom/tradingDetail/SUM",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#totalAmount").html(d.data.sumoperatorsharefee);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
