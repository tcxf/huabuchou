
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">
</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
    <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 基本信息</a></li>
                    <li><a data-toggle="tab" href="#examine" aria-expanded="true"> 其他信息</a></li>
                    <li><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
                    <li><a data-toggle="tab" href="#bind" aria-expanded="true"> 绑卡信息</a></li>
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                    <div class="tab-content">
                        <div id="base" class="ibox-content active tab-pane">
                            <input type="hidden" name="id" value="${id!}"/>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">运营商域名</label>
                                    <label class="col-sm-8 control-label" style="text-align: left;">
                                        <input type="text" class="form-control" id="osn" name="osn" readonly  maxlength="30" value="${entity.osn!}" placeholder="请填写运营商编码，即二级域名">
                                    </label>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">手机号码</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,mobile" validate-msg="required:请填写手机号码,mobile:请输入正确的手机号码" id="mobile" name="ymobile"   value="${entity.mobile!}" maxlength="30" placeholder="请填写手机号码">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">运营商名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" validate-msg="required:请填写运营商名称" id="name" name="yname" value="${entity.oname!}"  maxlength="30" placeholder="请填写运营商名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">运营商简称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,maxLength[6]" validate-msg="required:请填写运营商简称,maxLength:运营商简称最大只能输入6个字符" id="simpleName" name="simpleName"  value="${entity.simpleName!}" maxlength="30" placeholder="请填写运营商简称">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both;"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人名称</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,CHS" validate-msg="required:请填写法人名称" id="legalName" name="legalName" value="${entity.legalName!}"   maxlength="30" placeholder="请填写法人名称">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">法人身份证</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,idcard" validate-msg="required:请填写法人证件号" id="idCard" name="idCard" value="${entity.idCard!}"   maxlength="30" placeholder="请填写法人身份证">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">所属地区</label>
                                    <div class="col-sm-10" id="area" data-name="area" data-value="${entity.area!}">

                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">详细地址</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring"  id="address" name="address" value="${entity.address!}"   maxlength="30" placeholder="请填写详细地址">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">状态</label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status" value="${entity.status!}" >
                                            <option value="0" <#if (((entity.status)!'') == '0')>selected="selected"</#if> >禁用</option>
                                            <option value="1" <#if (((entity.status)!'') == '1')>selected="selected"</#if> >开通</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>

                        <!-- 运营商审核信息 -->
                        <div id="examine" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">运营商性质</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" id="nature" name="nature" value="${entity.nature!}"   maxlength="30" placeholder="请填写运营商性质">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">品牌经营年限</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number,ffstring"  validate-msg="required:品牌经营年限,number:品牌经营年限必须为数字"  id="operYear" name="operYear"  value="${entity.operYear!?c}" maxlength="30" placeholder="请填写品牌经营年限">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">经营面积</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:请填写经营面积,number:品牌经营面积必须为数字" id="operArea" name="operArea"  value="${entity.operArea!?c}" maxlength="30" placeholder="请填写经营面积">
                                    </div>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">注册资金（万）</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,number" validate-msg="required:请填写注册资金,number:注册资金必须为数字"  id="regMoney" name="regMoney" value="${entity.regMoney!}"   maxlength="30" placeholder="请填写注册资金">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">服务号appid</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" id="appid" name="appid" value="${entity.appid!}"  maxlength="30" placeholder="请填写微信服务号的appid">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">服务号secret</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,ffstring" id="appsecret" value="${entity.appsecret!}"   name="appsecret"  maxlength="40" placeholder="请填写微信服务号的appsecret">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">场地照片</label>
                                <div class="img" file-id="localPhoto" file-value="${entity.localPhoto!}" file-size="400*400">

                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">营业执照</label>
                                <div class="img" file-id="licensePic" file-value="${entity.licensePic!}" file-size="400*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">法人身份证</label>
                                <div class="img" file-id="legalPhoto" file-value="${entity.legalPhoto!}" file-size="400*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">借记卡照片</label>
                                <div class="img" file-id="normalCard" file-value="${entity.normalCard!}" file-size="400*400">
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">信用卡照片</label>
                                <div class="img" file-id="creditCard" file-value="${entity.creditCard!}" file-size="400*400">

                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>

                        </div>
                        <div id="settlement" class="ibox-content tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>

                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">积分比例%</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,integer,max[10000]" validate-msg="required:请填写积分比例,integer:积分比例必须为正整数,max:积分比例不能大于10000" id="scorePercent" name="scorePercent"  value="${entity.scorePercent!}"  maxlength="30" placeholder="请填写积分比例">
                                    </div>
                                </div>

                                <#--<div class="form-group col-sm-6">-->
                                    <#--<label class="col-sm-4 control-label">短信费用（每条）</label>-->
                                    <#--<div class="col-sm-8">-->
                                        <#--<input type="text" class="form-control" validate-rule="required,num" validate-msg="required:请填写短信费用,number:短信费用只能填写2位小数的数字" id="messageFee" name="messageFee"  value="${entity.messageFee!}" maxlength="30" placeholder="请填写短信费用（每条）">-->
                                    <#--</div>-->
                                <#--</div>-->
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">是否开通确认结算单审核</label>
                                    <div class="col-sm-8">
                                        <select name="settlementStatus" id="settlementStatus" value="${entity.settlementStatus!}">
                                            <option value="1" <#if (((entity.settlementStatus)!'') == '1')>selected="selected"</#if> >开通</option>
                                            <option value="0" <#if (((entity.settlementStatus)!'') == '0')>selected="selected"</#if> >禁用</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed" style="clear:both"></div>
                                <#--<div class="form-group col-sm-6">-->
                                    <#--<label class="col-sm-4 control-label">结算周期</label>-->
                                    <#--<div class="col-sm-8">-->
                                        <#--<select name="settlementLoop" id="settlementLoop" value="${entity.settlementLoop!} ">-->
                                            <#--<option value="1" <#if (((entity.settlementLoop)!'') == '1')>selected="selected"</#if> >日结</option>-->
                                            <#--<option value="2" <#if (((entity.settlementLoop)!'') == '2')>selected="selected"</#if> >周结</option>-->
                                            <#--<option value="7" <#if (((entity.settlementLoop)!'') == '7')>selected="selected"</#if> >半月</option>-->
                                            <#--<option value="3" <#if (((entity.settlementLoop)!'') == '3')>selected="selected"</#if> >月结</option>-->
                                            <#--<option value="4" <#if (((entity.settlementLoop)!'') == '4')>selected="selected"</#if> >季结</option>-->
                                            <#--<option value="5" <#if (((entity.settlementLoop)!'') == '5')>selected="selected"</#if> >半年</option>-->
                                            <#--<option value="6" <#if (((entity.settlementLoop)!'') == '6')>selected="selected"</#if> >一年</option>-->
                                        <#--</select>-->

                                    <#--</div>-->

                                <#--</div>-->
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">平台服务费%</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  validate-rule="required,num" validate-msg="required:请填写让利利率,num:让利利率只能填写2位小数的数字，不填默认为0,则不开启让利"  id="reBate" name="platfromShareRatio" value="${entity.platfromShareRatio!}"   maxlength="30" placeholder="请填写返佣利率">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>
                        <div id="bind" class="ibox-content tab-pane">
                            <input type="button" id="new-card" class="btn btn-success" value="新增" />
                            <div class="fixed-table-container form-group">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:10%;min-width:150px;">开户行</th>
                                        <th style="width:10%;min-width:60px;">开户人</th>
                                        <th style="width:10%;min-width:80px;">预留手机</th>
                                        <th style="width:20%;min-width:80px;">身份证号</th>
                                        <th>卡号</th>
                                        <th style="width:5%;min-width:120px;">是否主卡</th>
                                        <th style="width:20%">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody" style="text-align:center;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
    <script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${basePath!}/js/plugins/webuploader/webuploader.js"></script>
    <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>

    <script>
        var context= "${basePath!}";
        var context2= "${basePath!}";
        $(document).ready(function () {
            $("select").select();
            $("#area").area();
            examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;ssss
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            var nodata = false;

            initCard();
            function initCard(){
                $("#tbody").empty();
                $.post("${basePath!}/platfrom/otherInfo/loadBindCardInfo?id=${entity.id!}",function(d){
                    for(var i = 0 ; i < d.data.length ; i++){
                        if(d.data[i].isDefault=="1"){
                            select = '<select  name="bank-isdefault" id="bank-isdefault"><option selected value="1">是</option></select>';
                        }else{
                            select = '<select  name="bank-isdefault" id="bank-isdefault"><option selected value="0">否</option></select>';
                        }
                        $tr = $('<tr>'+
                                '<td class="bank-select" input-name="bank-bankSn" id="bank-bankSn" input-val="'+d.data[i].bankSn+'"></td>'+
                                '<td><input class="form-control"  name="bank-name" id="bank-name" type="text" value="'+d.data[i].name+'"/></td>'+
                                '<td><input class="form-control"  name="bank-mobile" id="bank-mobile" type="text" value="'+d.data[i].mobile+'"/></td>'+
                                '<td><input class="form-control"  name="bank-idCard" id="bank-idCard" type="text" value="'+d.data[i].idCard+'"/></td>'+
                                '<td><input class="form-control"  name="bank-bankCard" id="bank-bankCard" type="text" value="'+d.data[i].bankCard+'"/></td>'+
                                '<td>'+select+'</td>'+
                                '<td><input type="button" data-id="'+d.data[i].id+'" class="del-card btn btn-danger" value="删除" /></td>'+
                                '</tr>');
                        $("#tbody").append($tr);
                        $tr.find("select").select();
                        $tr.find(".bank-select").initBank();
                        $tr.find(".del-card").click(function(){
                            $this = $(this);
                            li = layer.confirm('您确认删除吗?', {
                                btn: ['确定','取消'] //按钮
                            }, function(){
                                $.post("${basePath!}/platfrom/fundend/delBindCard",{id:$this.attr("data-id")},function(d){
                                    layer.alert(d.msg);
                                    $this.parent().parent().remove();
                                    layer.close(li);
                                },"json");
                            }, function(){});
                        });
                        $tr.find(".update-card").click(function(){
                            $this = $(this);
                            $p = $this.parent().parent();
                            $.post("${basePath!}/platfrom/otherInfo/updateBindCard",{
                                id:$this.attr("data-id"),
                                bankCard:$p.find("#bank-bankCard").val(),
                                bankName:$p.find("#bank-name").val(),
                                bankSn:$p.find("#bank-bankSn").val(),
                                idCard:$p.find("#bank-idCard").val(),
                                isDefault:$p.find("#bank-isdefault").val(),
                                mobile:$p.find("#bank-mobile").val()
                            },function(d){
                                layer.alert(d.msg);
                                if(d.code ==0){
                                    initCard();
                                }
                            },"json");
                        });
                    }
                },"json");
            }
            $("#new-card").click(function(){
                nodata = false;
                $tr = $('<tr>'+
                        '<td class="bank-select" input-name="bank-bankSn" input-val=""></td>'+
                        '<td><input  class="form-control" name="bank-name" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-mobile" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-idCard" type="text"/></td>'+
                        '<td><input  class="form-control" name="bank-bankCard" type="text"/></td>'+
                        '<td><select  name="bank-isdefault"><option value="1">是</option><option value="0">否</option></select></td>'+
                        '<td> <input type="button" class="del-card btn btn-danger" value="删除" /></td>'+
                        '</tr>');
                $("#tbody").append($tr);
                $tr.find(".bank-select").initBank();
                $tr.find(".del-card").click(function(){
                    $this = $(this);
                    li = layer.confirm('您确认删除吗?', {
                        btn: ['确定','取消'] //按钮
                    }, function(){
                        $this.parent().parent().remove();
                        layer.close(li);
                    }, function(){});
                });
                $tr.find("select").select();
            });
            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
                var totalData=document.getElementById("_f");
                var formData = new FormData(totalData);
                var arr = [];
                // var pwd=$("#pwd").val();
                // var pwd1=$("#pwd1").val();
                // if(pwd!=pwd1){
                //     layer.msg("密码不一致！")
                //     return;
                // };

                var osn =$("#osn").val();
                if(osn==null || osn == ""){
                    layer.msg("请输入正确的二级域名，二级域名是否正确");
                    return;
                }
                var mobile = $("#mobile").val();
                if(mobile== null || mobile ==""){
                    layer.msg("手机号码不能为空");
                    return;
                }
                var nature= $("#nature").val();
                if(nature==null  || nature ==""){
                    layer.msg("运营商性质请输入内容");
                    return;
                }
                var operYear =$("#operYear").val();
                if(operYear==null || operYear==""){
                    layer.msg("请输入品牌年限");
                    return;
                }
                var operArea=$("#operArea").val();
                if(operArea==null || operArea==""){
                    layer.msg("请输入经营面积");
                    return;
                }

                var regMoney=$("#regMoney").val();
                if(regMoney==null || regMoney==""){
                    layer.msg("请输入注册资金");
                    return;
                }

                var appid=$("#appid").val();
                if(appid==null || appid==""){
                    layer.msg("请输入Appid");
                    return;
                }

                var appsecret=$("#appsecret").val();
                if(appsecret==null || appsecret==""){
                    layer.msg("请输入微信公众号appsecret");
                    return;
                }

                var scorePercent =$("#scorePercent").val();
                if(scorePercent == null || scorePercent==""){
                    layer.msg("请输入积分比率");
                    return;
                }

                var name =$("#name").val();
                if(name==null || name ==""){
                    layer.msg("请输入运营商名称");
                    return;
                }
                var simpleName=$("#simpleName").val();
                if(simpleName ==null || simpleName== ""){
                    layer.msg("请输入运营商简称");
                    return;
                }

                var  legalName=$("#legalName").val();
                if( legalName ==null ||  legalName== ""){
                    layer.msg("请输入法人名称");
                    return;
                }
                var  idCard=$("#idCard").val();
                if( idCard ==null || idCard== ""){
                    layer.msg("请输入法人省份证");
                    return;
                }

                $("#tbody").find("tr").each(function(){
                    var obj = {};
                    var tdArr = $(this).children();
                    obj.bankSn = tdArr.eq(0).find('option:selected').val();
                    // obj.bankName = tdArr.eq(1).find('input').val();
                    obj.name =tdArr.eq(1).find('input').val();
                    obj.mobile =tdArr.eq(2).find('input').val();
                    obj.idCard = tdArr.eq(3).find('input').val();
                    obj.bankCard = tdArr.eq(4).find('input').val();
                    obj.isDefault = tdArr.eq(5).find('option:selected').val();
                    arr.push(obj);
                });
                console.log(JSON.stringify(arr))
                formData.append("bind_card",JSON.stringify(arr));
                var $this = $(this);
                $this.html("保 存 中");
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                    url:'${basePath!}/platfrom/otherInfo/updateOperainfo',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    data:formData,
                    processData: false,                // jQuery不要去处理发送的数据
                    contentType: false,
                    success: function(data){ //成功
                        layer.close(index);
                        //消息对话框
                        if(data.code==0){
                            parent.messageModel(data.msg);
                            parent.c.gotoPage(null);
                            parent.closeLayer();
                        }else {
                            parent.messageModel(data.msg);
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>
