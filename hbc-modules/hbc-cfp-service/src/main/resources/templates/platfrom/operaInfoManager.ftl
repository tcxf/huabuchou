<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
<#include "common/common.ftl">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>运营商管理</h5>
						<div class="ibox-tools">
                            <a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建运营商</a>
						</div>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
						
				<span>今日新增：${number}人</span>
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<a href="${basePath!}/platfrom/otherInfo/CreateQueryAllDate"><button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button></a>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">运营商名称</label>
								<input type="text" name="name" placeholder="请输入运营商名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
   		$("#parentId").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/otherInfo/queryall",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>运营商id</th>'+
							'<th>运营商名称</th>'+
							'<th>手机</th>'+
							'<th>简称</th>'+
							'<th>规模（万）</th>'+
							'<th>商户数</th>'+
							'<th>用户数</th>'+
							'<th>注册日期</th>'+
							'<th>所属地域</th>'+
							'<th>状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{id}</td>'+
							'<td>{name}</td>'+
							'<td>{mobile}</td>'+
							'<td>{simpleName}</td>'+
							'<td>{regMoney}</td>'+
							'<td>{merchantNum}</td>'+
							'<td>{userNum}</td>'+
							'<td>{createDate}</td>'+
							'<td>{address}</td>'+
							'<td id="status_{id}"></td>'+
							'<td id="opera_{id}">'+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-info btn-sm edit"><i class="fa fa-edit"></i> 编辑 </a> '+
								'<a href="javascript:void(0);" data="{id},{simpleName}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 查看 </a> '+
								'<a href="javascript:void(0);" data="{osn}" class="btn btn-danger btn-sm home"><i class="fa fa-home"></i> 站点信息 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$("#yyh").html("运营商共计："+result.data.total+"家");
			
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].status == 1)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用中</span>");
					else if(list[i].status == 0){
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>未开通</span>");
						$("#opera_"+list[i].id).find(".detail").hide();
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" datas="'+list[i].id+'" class="btn btn-success btn-sm open"><i class="fa fa-key"></i> <span>立即开通</span> </a>');
					}
					else if(list[i].status == 2)
						$("#status_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>已禁用</span>");
					else
						$("#status_"+list[i].id).html("<span style='color:#000000;background-color:yellow;'>未知</span>");
						
				}
				
				$(".open").click(function(){
					if($(this).hasClass("disabled")) return;
					var oid = $(this).attr("datas");
					$(this).addClass("disabled");
					$(this).find("i").removeClass("fa-key");
					$(this).find("i").addClass("fa-spinner");
					$(this).find("i").addClass("fa-spin");
					$(this).find("span").html("正在处理...");
					setTimeout(function(){
						$(this).find("span").html("权限初始化...");
						$.post('${basePath}/platfrom/otherInfo/initData',{id:oid},function(d){
							parent.messageModel(d.msg);
							if(d.code == 0){
								$pager.gotoPage($pager.pageNumber);
							}else{
								$(this).removeClass("disabled");
								$(this).find("i").addClass("fa-key");
								$(this).find("i").removeClass("fa-spinner");
								$(this).find("i").removeClass("fa-spin");
								$(this).find("span").html("立即开通");
							}
						},"json");
					},1500);
				});
				
				//删除事件
				$(".remove").click(function (){
					//删除数据请求
					var param='id='+$(this).attr("data-id");
                    var bo = parent.deleteData('${basePath!}/platfrom/otherInfo/forwardOperaInfoDel',param,$pager);
				});
				
				//编辑页面
				$(".edit").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
                    parent.openLayer('修改运营商-'+title,'${basePath!}/platfrom/otherInfo/forwardOperaInfoEdit?id='+id,'1000px','700px',$pager);
				});
				//编辑页面
				$(".detail").click(function (){
					var title = $(this).attr("data").split(",")[1];
					var id = $(this).attr("data").split(",")[0];
                   parent.openLayer('运营商详情-'+title,'${basePath!}/platfrom/otherInfo/operaInfoDetail?id='+id,'1000px','700px',$pager);

                    //parent.openLayer('运营商详情-'+title,'$!{basePath!}/operaInfo/forwardOperaInfoDetail 1000px','700px',$pager);
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				
				$(".home").click(function(){
					var osn = $(this).attr('data');
					osn=osn.substr(0,osn.indexOf('.'));
                    parent.modelContent('<div class="wrapper wrapper-content animated fadeInUp"><b>'+
											'<div class="row" style="line-height:25px;font-size:12px;">'+
											'<div class="col-sm-12">运营商后台地址：</div>'+
											'<div class="col-sm-12">http://'+'platform.huabuchou.com'+'/cfp/opera/opera/gologin</div>'+
											'<div class="col-sm-12">商家PC后台地址：</div>'+
											'<div class="col-sm-12">http://'+'merchant.huabuchou.com'+'/merchant/login/gologin</div>'+
											'<div class="col-sm-12">商城地址：</div>'+
											'<div class="col-sm-12">http://'+osn+'.huabuchou.com'+'/consumer/cuser/gologin</div>'+
											'<div class="col-sm-12">资金端后台地址：</div>'+
											'<div class="col-sm-12">http://'+'platform.huabuchou.com'+'/cfp/fund/flogin/gologin</div>'+
											'<div class="col-sm-12" style="color:red">*注意：点击空白区域关闭显示层</div>'+
											'</div>'+
										'</b></div>');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
            $pager.gotoPage($pager.pageNumber);
		});
		
		//删除多个内容
		$(".deleteCheck").click(function (){
			 var ids="";
			 $("input[name='checkbox-name']").each(function(){
				 if(this.checked==true && $(this).val() != "-1"){
				 	ids += $(this).val()+",";
				 }
			});
			if (ids.length<=0) {
                parent.messageModel("请选择您要删的内容");
			}
			
			if (ids.length>0) {
				//删除数据请求
				var param='id='+ids;
                var bo = parent.deleteData('${basePath!}/platfrom/operaInfo/delete',param,$pager);
			}
		});
		
		//打开新增页面
		$("#new").click(function (){
            parent.openLayer('添加运营商','${basePath!}/platfrom/otherInfo/insert','1000px','700px',$pager);
		});
	});

</script>

	</body>
</html>
