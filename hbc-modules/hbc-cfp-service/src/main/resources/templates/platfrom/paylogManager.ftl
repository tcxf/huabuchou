<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
  	<style>
  		table{
			  table-layout:fixed;
			}
			.a-ids{
			overflow:hidden;
			text-overflow:ellipsis;
			white-space:nowrap;
			}
  	
  	</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
			
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>代付记录</h5>
							</div>
					
						</div>
					</div>
					
				</div>
				<div id="bind" class="ibox-content tab-pane">
						<div class="fixed-table-container form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width:15%;min-width:150px;">流水号</th>
										<th style="width:10%;min-width:60px;">账户类型</th>
										<th style="width:10%;min-width:80px;">结算对象</th>
										<th style="width:5%;min-width:80px;">姓名</th>
										<th style="width:13%;min-width:80px;">代付交易状态</th>
										<th>结算金额</th>
										<th>ids</th>
										<th style="width:7%;min-width:120px;">操作</th>
									</tr>
								</thead>
								<tbody id="tbody" style="text-align:center;">
								
								</tbody>
							</table>
						</div>
					</div>
				
			</div>
			
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
	
			function initCard(){
				$("#tbody").empty();
				$.post("<%=path%>/paylog/paylogManagerfindlist.htm",{},function(d){
					for(var i = 0 ; i < d.data.length ; i++){
						select = d.data[i].isDefault?"是":"否";
						var transStatusStr ='';
						if(d.data[i].trans_status == 0) {
								transStatusStr  = '打款成功';
							} else if(d.data[i].trans_status == null) {
								transStatusStr  = '处理中';
							}else if(d.data[i].trans_status ==2 ) {
								transStatusStr  = '付款失败';
							}else if(d.data[i].trans_status == 3) {
								transStatusStr  = '银行退票成功';
							}else {
								transStatusStr  = '未知';
							}
						var acttype ='';
						if(d.data[i].act_type == 0) acttype  = '对公';
						else if(d.data[i].act_type == 1) acttype  = '对私';
						var settype ='';
						if(d.data[i].set_type == 2) settype  = '运营商';
						else if(d.data[i].set_type == 1) settype  = '商户';
						$tr = $('<tr style="text-align:left;">'+
								'<td>'+d.data[i].serial_no+'</td>'+
								'<td>'+acttype+'</td>'+
								'<td>'+settype+'</td>'+
								'<td>'+d.data[i].real_name+'</td>'+
								'<td>'+transStatusStr+'</td>'+
								'<td>'+d.data[i].amount+'</td>'+
								'<td class="a-ids">'+d.data[i].sids+'</td>'+
								'<td><a href="javascript:void(0);" data="'+d.data[i].sids+'" class="btn btn-primary btn-sm tlog"><i class="fa fa-list-ul"></i> 结算账单 </a> </td>'+
								'<td><a href="javascript:void(0);" data="'+d.data[i].sids+'" class="btn btn-primary btn-sm jiesuan"><i class="fa fa-list-ul"></i> 结算账单明细 </a> </td>'+
								'</tr>');
						$("#tbody").append($tr);
						$tr.find("select").select();
						$tr.find(".bank-select").initBank();
						$tr.find(".tlog").click(function(){
							window.location.href="<%=path %>/paylog/paylogfind.htm?sids="+$(this).attr("data");
						});
						$tr.find(".jiesuan").click(function(){
							window.location.href="<%=path %>/paylog/payjiesuanfind.htm?sids="+$(this).attr("data");
						});
					}
						},"json");
				}
			
			initCard();
	});
	
</script>

	</body>
</html>
