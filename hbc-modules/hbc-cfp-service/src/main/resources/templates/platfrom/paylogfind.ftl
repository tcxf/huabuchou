<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
			
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>代付明细记录</h5>
							</div>
					
						</div>
					</div>
					
				</div>
				<div id="bind" class="ibox-content tab-pane">
						<div class="fixed-table-container form-group">
							<table class="table table-hover">
								<thead>
									<tr>
										<th style="width:10%;min-width:150px;">流水号</th>
										<th style="width:10%;min-width:60px;">银行卡</th>
										<th style="width:10%;min-width:80px;">结算对象</th>
										<th style="width:10%;min-width:80px;">姓名</th>
										<th style="width:20%;min-width:80px;">交易时间</th>
										<th>结算金额</th>
									
									</tr>
								</thead>
								<tbody id="tbody" style="text-align:center;">
								
								</tbody>
							</table>
						</div>
					</div>
				
			</div>
			
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			initCard();
			function initCard(){
				$("#tbody").empty();
				$.post("<%=path%>/paylog/paylogManagerlist.htm",{sids:'${sids}'},function(d){
					
					for(var i = 0 ; i < d.data.length ; i++){
						var settype ='';
						if(d.data[i].settlementType == 1) settype  = '运营商';
						else if(d.data[i].settlementType == 0) settype  = '商户';
						$tr = $('<tr style="text-align:left;">'+
								'<td>'+d.data[i].serialNo+'</td>'+
								'<td>'+d.data[i].bankCard+'</td>'+
								'<td>'+settype+'</td>'+
								'<td>'+d.data[i].name+'</td>'+
								'<td>'+d.data[i].settlementTimeStr+'</td>'+
								'<td>'+d.data[i].amount+'</td>'+
							'</tr>');
						$("#tbody").append($tr);
					}
						},"json");
				}
			
			
	});
	
</script>

	</body>
</html>
