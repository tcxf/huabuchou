<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path %>/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>充值明细</h5>
					</div>
					<div class="form-group" style="display:none;position: absolute;right:20px;top:10px;z-index: 99999;">
						 <div>
							<input class="btn btn-primary in" id="submit-save" type="button" value="充值记录" />
						</div>
					</div> 
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<%-- <div>
								<div class="form-group col-sm-12">
									<h3>余额：<b>${inp.balance}</b></h3>
								</div>
							</div>	 --%>
						</form>
						<!-- <div class="form-group col-sm-12" style="text-align:left;margin-top:10px;">
							<button class="btn btn-info" type="button"  id='txsq'>充值</button>
						</div>  --> 
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		
		var $pager;
		
		$(".in").click(function(){ 
			$("#data").pager({
				url:"${hsj}/interfacePurse/rechargelist.htm", 
				formId:"searchForm",
				pagerPages: 3,
				template:{
					header:'<div class="fixed-table-container form-group">'+
					'<table class="table table-hover">'+
						'<thead>'+
							'<tr>'+
							 '<th>订单号</th>'+
	                          '<th>充值金额</th>'+
	                          '<th>接口余额</th>'+
	                          '<th>所属运营商</th>'+
	                          '<th>支付方式</th>'+
	                          '<th>订单状态</th>'+
	                          '<th>充值时间</th>'+ 
							'</tr>'+
						'</thead><tbody>',
					body:'<tr>'+
								'<td>{id}</td>'+
								'<td>{rechargeBalance}</td>'+
								'<td>{balance}</td>'+
								'<td>{oname}</td>'+
								'<td id="pay_{id}"></td>'+
								'<td id="order_{id}"></td>'+
								'<td >{createDate}</td>'+
							'</tr>', 
						footer:'</tbody></table>',
						noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
				},
				callbak: function(result){
					for(var i = 0 ; i < result.list.length ; i++){
						var item = result.list[i];
						var pt=item.payType;
						var os=item.orderStatus;  
						if(pt == 1){
							pt="快捷支付";
						}
						$("#pay_"+item.id).html(pt);
						if(os == "1"){
							os="已完成";
						}
						$("#order_"+item.id).html(os); 
					}
				}
			});
		});
		
		$(".in").trigger('click'); 
		$("#txsq").click(function (){
			/* parent.openLayer('钱包提现','${hsj}/wallet/wallettxManager.htm','300px','50px',$pager);  */
		});
	});
	
</script>

	</body>
</html>
