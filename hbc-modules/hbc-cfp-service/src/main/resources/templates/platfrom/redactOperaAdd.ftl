<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
<div class="form-group" style="position: absolute;right:20px;top:20px;z-index: 99999;">
    <div>
        <input class="btn btn-primary submit-save" id="submit-save" type="button" value="保存内容" />
        <input class="btn btn-white cancel" id="cancel" type="button" value="取消" />
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#settlement" aria-expanded="true"> 费用及结算信息</a></li>
                </ul>
                <form id="_f" method="post" class="form-horizontal">
                        <div id="settlement" class="ibox-content active tab-pane">
                            <div class="hr-line-dashed" style="clear:both"></div>
                            <div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">分期利率</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  validate-rule="required,num" validate-msg="required:请填写分期利率,num:分期利率只能填写2位小数的数字" id="serviceFee" name="serviceFee" maxlength="30" placeholder="请填写分期利率">
                                        <input type="hidden" id="oid" name="oid" value="${id}" />
                                        <input type="hidden" id="fid" name="fid" value="${fid}" />
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label">逾期还款费率</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" validate-rule="required,num" validate-msg="required:请填写逾期还款费率,num:逾期还款费率只能填写2位小数的数字"  id="yqRate" name="yqRate" maxlength="30" placeholder="请填写逾期还款费率">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed" style="clear:both"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
    <script type="text/javascript" src="${basePath!}/js/common.js"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>
    <script src="${basePath!}/js/plugins/layer/layer.min.js"></script>
    <!-- iCheck -->
    <script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
    <script src="${basePath!}/js/plugins/webuploader/webuploader.js"></script>
    <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
    <script>
        $(document).ready(function () {
            $("#area").area();
            examine = false;
            $('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
                var target =e.target.toString(); //$(e.target)[0].hash;ssss
                if( target.indexOf('examine')>0 && !examine){
                    $(".img").uploadImg();
                    examine = true;
                }
            });
            var nodata = false;
            $("select").select();
            $(".cancel").click(function(){parent.closeLayer();});
            $("#submit-save").click(function (){
                var data = {};
                data.fid = $("#fid").val();
                data.oid = $("#oid").val();
                data.serviceFee = $("#serviceFee").val();
                data.yqRate = $("#yqRate").val();
                if(!$("#_f").validate()) return;
                var $this = $(this);
                $this.html("保 存 中");
                $this.attr("disabled", true);
                var index = layer.load(2, {time: 10*1000});
                $.ajax({
                    url:'${basePath!}/platfrom/fundend/redactOperaAddAjaxSubmit',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    contentType : 'application/json',
                    data:JSON.stringify(data),
                    success: function(data){ //成功
                        layer.close(index);
                        var obj = data;
                        //消息对话框
                        parent.messageModel(data.msg);
                        parent.c.gotoPage(null);
                        parent.closeLayer();
                    }
                });
            });
        });
    </script>
</body>

</html>
