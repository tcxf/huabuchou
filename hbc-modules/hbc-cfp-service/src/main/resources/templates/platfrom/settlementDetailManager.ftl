<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
					<li><a data-toggle="tab" href="#uncreate" aria-expanded="true" id="unOut_Bill"> 未生成结算单</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div class="row">
                                        <div class="form-group col-sm-6 col-lg-4">
                                            <h3>待结算总额：¥ <span id="DAmount">--</span> 元</h3>
                                        </div>
                                        <div class="form-group col-sm-6 col-lg-4"">
                                        	<h3>已结算总额：¥ <span id="YAmount">--</span> 元</h3>
                                    	</div>
									</div>
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">出账时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户名称</label>
											<div class="col-sm-9">
												<input id="mname" type="text" class="form-control" name="mname" maxlength="30" placeholder="请填写商户名称">
											</div>
										</div>

										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">结算状态</label>
											<div class="col-sm-9">
												<select id="platStatus" name="platStatus">
													<option value="">全部</option>
													<option value="0">待确认</option>
													<option value="1">待结算</option>
													<option value="2">已结算</option>
												</select>
											</div>
										</div>

                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">结算时间</label>
                                            <div class="col-sm-9">
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="startDate2" id="createDate" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                                <div class="col-sm-6" style="padding-left:0px;">
                                                    <input type="text" class="form-control" name="endDate2" id="exstDate" style="width:100%" maxlength="30" placeholder="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6 col-lg-4">
                                            <label class="col-sm-3 control-label">选择运营商</label>
                                            <div class="col-sm-9">
                                                <select name="ooid" id="ooid">
                                                    <option value="">请选择运营商</option>
                                                    <#list list as list >
                                                        <option id="id" value="${list.id!}">${list.name!}</option>
													</#list>
                                                </select>
                                            </div>
                                        </div>
									</div>

									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
											<input id="jsmx" class="btn btn-info" type="button" value=" 下 载 报 表 " />
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data"></div>
							</div>
						</div>
					</div>
					<div id="uncreate" class="ibox-content tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm2" class="form-horizontal">
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data-uncreate"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
 	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		var context='${basePath!}'
		$(document).ready(function(){
            $("select").select();
		$("#oid-span").operaSelect();

		//下载报表
		$("#jsmx").click(function(){
			$("#searchForm").attr("action","${basePath!}/platfrom/pSettlement/AlreadyOrderPoi");
			$("#searchForm").submit();
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
            laydate({
                elem: '#createDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });
            laydate({
                elem: '#exstDate',
                event: 'click', //触发事件
                format: 'YYYY-MM-DD' //日期格式
            });

		$("#platStatus").select();
		var $pager = $("#data").pager({
			url:"${basePath!}/platfrom/pSettlement/pSettlementPageInfo",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>商户名称</th>'+
							'<th>所属运营商</th>'+
							'<th>出账时间</th>'+
							'<th>结算时间</th>'+
							'<th>结算状态</th>'+
							'<th>结算状态(平台)</th>'+
							'<th>结算状态(运营商)</th>'+
							'<th>商户结算</th>'+
							'<th>运营商结算</th>'+
							'<th>平台结算</th>'+
							'<th>结算金额</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{mname}</td>'+
							'<td>{oname}</td>'+
							'<td>{outSettlementDate}</td>'+
							'<td>{settlementTime}</td>'+
							'<td>{status}</td>'+
							'<td>{platStatus}</td>'+
							'<td>{operStatus}</td>'+
							'<td>{oamount}</td>'+
							'<td>{operatorShareFee} </td>'+
							'<td>{platfromShareFee}</td>'+
							'<td>{sumamount}</td>'+
							'<td id="opera_{id}"><a href="javascript:void(0);"  data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-file"></i> 交易明细 </a> </td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="13"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
			    var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                    if(list[i].platStatus == '待确认' && list[i].operStatus == '待结算')
                    {
                        $("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm surely"><i class="fa fa-check"></i> 确认结算 </a>');
                    }
                    if (list[i].status == '待结算' && list[i].isPay == '0')
					{
                        $("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm ok"><i class="fa fa-check"></i> 确认付款 </a>');
					}
                }
				$(".ok").click(function () {
                    var itemid = $(this).attr("data");
                    parent.confirm('是否确认打款？',function(d){
                        $.post('${basePath!}/platfrom/pSettlement/ok',{
                            id:itemid
                        },function(d){
                            parent.messageModel(d.msg);
                            if (d.code==0) {
                                //消息对话框
                                $pager.gotoPage($pager.pageNumber);
                            }
                            parent.closeLayer();
                        },"json");
                    });
                });
				$(".surely").click(function(){
				    var itemid = $(this).attr("data");
					parent.confirm('是否确认账单？',function(d){
						$.post('${basePath!}/platfrom/pSettlement/updateplatStatus',{
							id:itemid
						},function(d){
                            parent.messageModel(d.msg);
                            if (d.code==0) {
                                //消息对话框
                                $pager.gotoPage($pager.pageNumber);
                            }
                            parent.closeLayer();
						},"json");
					});
				});
                loadAmount2();//加载头部信息
                loadAmount3();
				//loadFanYongAlreadySet();//加载平台返佣已结算

                //loadPFanYongWaitSet();//加载平台返佣待结算

				//交易明细
                $(".tlog").click(function(){
                    window.location="${basePath!}/platfrom/pSettlement/jumpSettlementDetail?id="+$(this).attr("data");
                });
			}
		});

            $("#unOut_Bill").click(function(){
                var $pager_uncreate = $("#data-uncreate").pager({
                    url:"${basePath!}/platfrom/pSettlement/pNoSettlementPageInfo",
                    formId:"searchForm2",
                    pagerPages: 3,
                    template:{
                        header:'<div class="fixed-table-container form-group pager-content">'+
                        '<table class="table table-hover">'+
                        '<thead>'+
                        '<tr>'+
                        '<th>商户ID</th>'+
                        '<th>商户名称</th>'+
                        '<th>商户手机</th>'+
                        '<th>结算周期</th>'+
                        '<th>平台结算佣金</th>'+
                        '<th>运营商佣金</th>'+
                        '<th>结算金额</th>'+
                        '<th>操作</th>'+
                        '</tr>'+
                        '</thead><tbody>',
                        body:'<tr>'+
                        '<td>{mid}</td>'+
                        '<td>{legalName}</td>'+
                        '<td>{legalTel}</td>'+
                        '<td>{clearDay}</td>'+
                        '<td>{pShareAmount}</td>'+
                        '<td>{opaShareAmount}</td>'+
                        '<td>{actualAmount}</td>'+
                        '<td><a href="javascript:void(0);" data="{mid}" class="btn btn-success btn-sm mx"><i class="fa fa-file"></i> 交易明细 </a></td>'+
                        '</tr>',
                        footer:'</tbody></table>',
                        noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
                    },
                    callbak: function(result){
                        $('input').iCheck({
                            checkboxClass: 'icheckbox_square-green',
                            radioClass: 'iradio_square-green',
                            increaseArea: '20%' // optional
                        });
                        // check 全部选中
                        $('.checks').on('ifChecked', function(event){
                            $("input[id^='check-id']").iCheck('check');
                        });

                        // check 全部移除
                        $('.checks').on('ifUnchecked', function(event){
                            $("input[id^='check-id']").iCheck('uncheck');
                        });
                        //交易明细
                        $(".mx").click(function(){
                            window.location="${basePath!}/platfrom/pSettlement/jumpMerchantTradeDetail?mid="+$(this).attr("data");
                        });
                    }
                });
            });

		function loadAmount2() {
			$.ajax({
				url: "${basePath!}/platfrom/pSettlement/DAmount",
				type: 'POST', //GET
				data: $("#searchForm").serialize(),
				timeout: 30000,    //超时时间
				dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				success: function (d) {
					$("#DAmount").html(d.data.dsumamount);
					console.log(d);
				}
			});
		}

		function loadAmount3() {
			$.ajax({
				url: "${basePath!}/platfrom/pSettlement/YAmount",
				type: 'POST', //GET
				data: $("#searchForm").serialize(),
				timeout: 30000,    //超时时间
				dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				success: function (d) {
					$("#YAmount").html(d.data.ysumamount);
					console.log(d);
				}
			});
		}

		$("#loading-example-btn").click(function(){
			//$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
