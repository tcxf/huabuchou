<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#base" aria-expanded="true"> 已生成结算单</a></li>
					<li><a data-toggle="tab" href="#uncreate" aria-expanded="true"> 未生成结算单</a></li>
				</ul>
				<div class="tab-content">
					<div id="base" class="ibox-content active tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6">
											<h3>结算总额：¥ <span id="s">--</span> 元</h3>
										</div>
										<div class="form-group col-sm-6">
											<h3>待结算总额：¥ <span id="uns">--</span> 元</h3>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">出账时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="startDate" id="startDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="endDate" id="endDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户名称</label>
											<div class="col-sm-9">
												<input id="manme" type="text" class="form-control" name="manme" maxlength="30" placeholder="请填写商户名称">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">商户id</label>
											<div class="col-sm-9">
												<input id="mnum"  type="text" class="form-control" name="mnum" maxlength="30" placeholder="请填写商户id">
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">结算状态</label>
											<div class="col-sm-9">
												<select id="status" name="status">
													<option value="">全部</option>
													<option value="3">未发送</option>
													<option value="0">待确认</option>
													<option value="1">待结算</option>
													<option value="2">已结算</option>
												</select>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">运营商</label>
											<div class="col-sm-9">
												<span id="oid-span" ele-id="oid" ele-name="oid"></span>
											</div>
										</div>
										
										<div class="form-group col-sm-6 col-lg-4">
											<label class="col-sm-3 control-label">结算时间</label>
											<div class="col-sm-9">
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="createDate" id="createDate" style="width:100%" maxlength="30" placeholder="">
												</div>
												<div class="col-sm-6" style="padding-left:0px;">
													<input type="text" class="form-control" name="exstDate" id="exstDate" style="width:100%" maxlength="30" placeholder="">
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="form-group col-sm-12" style="text-align:center;margin-top:10px;">
											<input class="btn btn-success" id="search" type="button" value=" 查 询 " />
											<input id="jsmx" class="btn btn-info" type="button" value=" 下 载 报 表 " />
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data"></div>
							</div>
						</div>
					</div>
					<div id="uncreate" class="ibox-content tab-pane">
						<div class="ibox">
							<div class="ibox-title">
								<h5>结算明细</h5>
							</div>
							<div class="ibox-content">
								<form id="searchForm" class="form-horizontal">
									<div>
										<div class="form-group col-sm-6">
											<h3>待结算总额：¥ <span id="ua">--</span> 元</h3>
										</div>
									</div>
								</form>
								<div style="clear:both;"></div>
								<div class="project-list pager-list" id="data-uncreate"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>
	<script src="<%=path %>/js/plugins/iCheck/icheck.min.js"></script>
	<script src="<%=path %>/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
            $("select").select();
		$("#oid-span").operaSelect();
		$("#jsmx").click(function(){
		 var status = $("#status option:selected").val();
			window.location.href = "${hsj}/settlementDetail/jsmx.htm?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&createDate="+$("#createDate").val()+"&exstDate="+$("#exstDate").val()+"&status="+status+"&manme="+$("#manme").val()+"&mnum="+$("#mnum").val();
			//window.location.href = "${hsj}/yq/aaa.htm";
		}); 
		
		laydate({
			elem: '#createDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#exstDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#startDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		laydate({
			elem: '#endDate', 
			event: 'click', //触发事件
		  	format: 'YYYY-MM-DD' //日期格式
		});
		$("#status").select();
		var $pager = $("#data").pager({
			url:"${hsj}/settlementDetail/settlementDetailManagerList.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>结算单号</th>'+
							'<th>运营商编码</th>'+
							'<th>运营商名称</th>'+
							'<th>出账时间</th>'+
							'<th>结算时间</th>'+
							'<th>结算状态</th>'+
							'<th>结算卡号</th>'+
							'<th>开户行</th>'+
							'<th>结算金额</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{osn}</td>'+
							'<td>{operaName}</td>'+
							'<td>{outSettlementDateStr}</td>'+
							'<td>{settlementTimeStr}</td>'+
							'<td id="status_{id}"></td>'+
							'<td>{bankCard}</td>'+
							'<td id="bankname_{id}"></td>'+
							'<td class="edit" data="{id}" data-val="{amount}">{amount}</td>'+
							'<td id="opera_{id}"><a href="javascript:void(0);" data="{id}" class="btn btn-primary btn-sm tlog"><i class="fa fa-list-ul"></i> 账单明细 </a> </td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$(".edit").click(function(){
					if($(this).hasClass("ee")) return;
					var id = randomString(32);
					$(this).addClass("ee");
					$input  = $('<div class="form-group"><input id="'+id+'" type="text" class="form-control" value="'+$(this).attr("data-val")+'"  placeholder="请填写变更后的结算金额" /></div>');
					$div = $('<div class="form-group"></div>');
					$sure  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-info btn-sm "><i class="fa fa-edit"></i> <span>保存</span> </a></div>');
					$cancel  = $('<div class="form-group col-sm-6" style="margin-bottom:0px;"><a href="javascript:void(0);" class="btn btn-danger btn-sm "><i class="fa fa-remove"></i> 取消 </a></div>');
					$(this).empty()
					$(this).append($input);
					$div.append($sure);
					$div.append($cancel);
					$(this).append($div);
					
					$cancel.click(function(){
						var topdiv = $(this).parent().parent();
						$(this).parent().parent().html($(this).parent().parent().attr("data-val"));
						setTimeout(function(){
							topdiv.removeClass("ee");
						},100);
					});
					$sure.click(function(){
						var money = $(this).parent().parent().find("input").val();
						var id = $(this).parent().parent().attr("data");
						$.ajax({
							url:"${hsj}/settlementDetail/updateMoney.htm",
							type:"post",
							data:"money="+money+"&id="+id,
							success:function(d){
								
								if(d.resultCode ==1){
									parent.messageModel("修改成功");
								$pager.gotoPage($pager.pageNumber);
								}else{
									parent.messageModel(d.resultMsg);
								}		
							},dataType:"json"
						});
					});
				});
				
			
				
				
				
				var binfo = null;
				$.ajax({
					url:context+"/js/bank/bank_info.json",
				    type:'POST', //GET
				    timeout:30000,    //超时时间
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    async:false,
				    success:function(data){
				    	binfo = data;
				    }
				});
				
				
				
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					if(list[i].status == 0){
						$("#status_"+list[i].id).html("待确认");
						$("#opera_"+list[i].id).append('<a href="javascript:void(0);" data="'+list[i].id+'" class="btn btn-success btn-sm sure"><i class="fa fa-check"></i> 确认付款 </a>');
					}else if(list[i].status == 1){
						$("#status_"+list[i].id).html("待结算");
					}else if(list[i].status == 3){
						$("#status_"+list[i].id).html("未发送");
					}else{
						$("#status_"+list[i].id).html("已结算");
					}
					if(binfo != null){
						for(var b = 0 ; b < binfo.length ; b++){
							if(binfo[b].sn == list[i].bankSn) {
								$("#bankname_"+list[i].id).html(binfo[b].name);
								break;
							}
						}
					}else{
						$("#bankname_"+list[i].id).html("未知");
					}
				}
				
				$(".sure").click(function(){
					id = $(this).attr("data");
					parent.confirm('是否确认账单？',function(d){
						$.post('<%=path%>/settlementDetail/sureSettlement.htm',{
							id:id
						},function(d){
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}
							parent.messageModel(d.resultMsg);
						},"json");
					});
				});
				
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				

				$(".tlog").click(function(){
					window.location="<%=path%>/settlementDetail/settlementDetailLog.htm?id="+$(this).attr("data");
				});

				$(".send").click(function(){
					$.post("<%=path%>/settlementDetail/send.htm",{id:$(this).attr("data")},function(d){
						parent.messageModel(d.resultMsg);
						if(d.resultCode == "1"){
							$pager.gotoPage($pager.pageNumber);
						}
					},"json");
				});
				
				$(".offline").click(function(){
					var id = $(this).attr("data");
					parent.confirm("是否确认线下结算？",function(){
						$.post('<%=path%>/settlementDetail/offline.htm',{id:id},function(d){
							if(d.resultCode == 1){
								$pager.gotoPage($pager.pageNumber);
							}
							parent.messageModel(d.resultMsg);
						},"json");
					});
				});
				
				loadAmount();
			}
		});
		
		
		
		
		var $pager_uncreate = $("#data-uncreate").pager({
			url:"${hsj}/settlementDetail/uncreateSettlementDetail.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>运营商编码</th>'+
							'<th>运营商名称</th>'+
							'<th>结算周期</th>'+
							'<th>结算金额</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td id="osn_{id}">{osn}</td>'+
							'<td id="operaName_{id}">{operaName}</td>'+
							'<td id="range_{id}"></td>'+
							'<td>{amount}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var d = result.list;
				for(var i = 0 ; i < d.length ; i++){
					console.log(d[i].mnum == null || d[i].mnum == "");
					$("#range_"+d[i].id).html(d[i].loopStartStr.split(" ")[0] + " - " + d[i].loopEndStr.split(" ")[0]);
					if(d[i].mnum == null || d[i].mnum == ""){
						console.log(d[i]);
						$("#mnum_"+d[i].id).html("运营商已删除");
						$("#manme_"+d[i].id).html("运营商已删除");
					}
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
				uncreateAmount();
			}
		});
		
		function uncreateAmount(){
			$.ajax({
				url:"<%=path%>/settlementDetail/uncreateSettlementAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#ua").html(d.data);
			    }
			});
			
		}

		function loadAmount(){
			$.ajax({
				url:"<%=path%>/settlementDetail/totalAmount.htm",
			    type:'POST', //GET
			    data:$("#searchForm").serialize(),
			    timeout:30000,    //超时时间
			    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			    success:function(d){
			    	$("#uns").html(d.data.uns);
			    	$("#s").html(d.data.s);
			    }
			});
		}
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
