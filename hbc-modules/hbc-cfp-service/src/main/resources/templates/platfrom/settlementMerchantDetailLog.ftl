<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${basePath!}/js/plugins/layer/laydate/need/laydate.css">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>交易明细  <a  href="javascript:history.back(-1)" >返回上一级</a></h5>
					</div>
					<div class="ibox-content">
						<form id="searchForm" class="form-horizontal">
							<div>
							</div>
						</form>
						<div style="clear:both;"></div>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		var $pager = $("#data").pager({
	
			url:"${basePath!}/platfrom/pSettlement/MerchantSettlementDetail?mid=${mid!}",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group pager-content">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>交易单号</th>'+
							'<th>支付编号</th>'+
							'<th>下单用户</th>'+
							'<th>用户手机</th>'+
							'<th>商户名称</th>'+
							'<th>交易总额</th>'+
							'<th>优惠金额</th>'+
							'<th>实付金额</th>'+
							'<th>运营商抽成</th>'+
							'<th>直接上级返佣</th>'+
							'<th>间接上级返佣</th>'+
							'<th>支付时间</th>'+
							'<th>支付方式</th>'+
							'<th>支付状态</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{serialNo}</td>'+
							'<td>{paymentSn}</td>'+
							'<td>{realName}</td>'+
							'<td>{telPhone}</td>'+
							'<td>{unum}</td>'+
							'<td>{tradeAmount}</td>'+
							'<td>{discountAmount}</td>'+
							'<td>{actualAmount}</td>'+
							'<td>{opaShareAmount}</td>'+
							'<td>{redirectShareAmount}</td>'+
							'<td>{inderectShareAmount}</td>'+
							'<td>{tradingDate}</td>'+
							'<td>授信支付</td>'+
							'<td>{paymentStatus}</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});

				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});
				
				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});
		
		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});
	});
	
</script>

	</body>
</html>
