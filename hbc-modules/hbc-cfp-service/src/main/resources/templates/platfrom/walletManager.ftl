<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${basePath!}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${basePath!}/css/animate.css" rel="stylesheet">
	<link href="${basePath!}/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>钱包详情</h5>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">钱包名称</label>
								<input type="text" name="name" placeholder="请输入钱包名称" id="name" class="form-control">
							</div>
                            <div class="form-group control-label">
                                <label for="exampleInputName2">用户类型：</label>
                                <select id="utype" name="utype">
                                    <option value="">全部</option>
                                    <option value="1">用户</option>
                                    <option value="2">商户</option>
                                    <option value="3">运营商</option>
                                    <option value="4">资金端</option>
                                </select>
                            </div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
	<script type="text/javascript" src="${basePath!}/js/common.js"></script>

	
	
	<script type="text/javascript">
	$(document).ready(function(){
		var $pager =  $("#data").pager({
			url:"${basePath!}/platfrom/wallet/findwallet",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
						    '<th>钱包名称</th>'+
							'<th>用户名称</th>'+
							'<th>手机号</th>'+
							'<th>用户类型</th>'+
							'<th>钱包总金额</th>'+
							'<th>钱包状态</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
				            '<td>{wname}</td>'+
							'<td>{uname}</td>'+
							'<td>{umobil}</td>'+
							'<td id="utype_{id}"></td>'+
							'<td >{totalAmount}</td>'+
							'<td id="rzStatus_{id}"></td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id}" class="btn btn-danger btn-sm find"><i class="fa fa-file"></i> 查看 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
					var $btn;
					if (list[i].utype == 1) {
                        $("#utype_"+list[i].id).html("<span>用户</span>");
					}else if (list[i].utype == 2){
                        $("#utype_"+list[i].id).html("<span>商户</span>");
					} else if (list[i].utype == 3){
                        $("#utype_"+list[i].id).html("<span>运营商</span>");
                    }else if (list[i].utype == 4){
                        $("#utype_"+list[i].id).html("<span>资金端</span>");
                    }

                    if(list[i].stuta == 1){
						$("#rzStatus_"+list[i].id).html("<span style='color:#ffffff;background-color:green;'>启用</span>");
						$btn = $('<a href="javascript:void(0);" data-id="'+list[i].id+'" class="btn btn-primary btn-sm lk"><i class="fa fa-unlock"></i> <span>冻结</span> </a>');
					}else{
						$("#rzStatus_"+list[i].id).html("<span style='color:#ffffff;background-color:red;'>冻结</span>" );
						$btn = $('<a href="javascript:void(0);" data-id="'+list[i].id+'" class="btn btn-primary btn-sm lk"><i class="fa fa-lock"></i> <span>启用</span> </a>');
					}
					$btn.bind("click", function(){
						var isFreeze = "";
						if($(this).find("i").hasClass("fa-lock")){
							$(this).find("i").removeClass("fa-lock");
							$(this).find("i").addClass("fa-unlock");
							$(this).find("i").parent().find("span").html("启用");
							isFreeze = "true";
						}else{
							$(this).find("i").removeClass("fa-unlock");
							$(this).find("i").addClass("fa-lock");
							$(this).find("i").parent().find("span").html("冻结");
							isFreeze = "false";
						}
						var id =$(this).attr("data-id");
						$.post('${basePath!}/platfrom/wallet/upWalletST',{isFreeze:isFreeze,id:id},function(r){
							if(r.code==0){
                                parent.messageModel("修改成功");
								$pager.gotoPage($pager.pageNumber);

                            }
						},"json");
					});
						
					$("#oper_"+list[i].id).append($btn);
				
					
					
				}
				$("#loading-example-btn").click(function(){
					$pager.gotoPage($pager.pageNumber);
				});
				
				$(".find").click(function (){
					//删除数据请求
					var id=$(this).attr("data");
					parent.openLayer('钱包详情','${basePath!}/platfrom/wallet/findwallets?id='+id,'1000px','700px',$pager);
				});
			}
		
		});
		
	

	
	});
	
</script>

	</body>
</html>
