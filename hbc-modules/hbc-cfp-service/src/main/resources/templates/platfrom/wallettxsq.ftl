<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title> - 项目</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico"> <link href="<%=path %>/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="<%=path %>/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="<%=path %>/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="<%=path %>/css/animate.css" rel="stylesheet">
	<link href="<%=path %>/css/auth.css?v=4.1.0" rel="stylesheet">
	<link href="<%=path %>/css/style.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>提现申请</h5>
						<div class="ibox-tools">
							<a href="javascript:void(0);" id="new" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> 新建运营商</a>
						</div>
					</div>
					<div class="ibox-content">
					<div id="yyh"></div>
						<div id="yyh1">
					</div>
						
						<form id="searchForm" class="form-inline">
							<div class="form-group">
								<div class="row m-b-sm m-t-sm">
									<div class="col-md-1">
										<button type="button" id="loading-example-btn" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> 刷新</button>
									</div>
									<div class="col-sm-8">
										 
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2" class="sr-only">钱包名称</label>
								<input type="text" name="name" placeholder="请输入钱包名称" id="name" class="form-control">
							</div>
							<input type="button" id="search" class="btn btn-warning" value="搜索" />
						</form>
						<div class="project-list pager-list" id="data"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<!-- 全局js -->
	<script src="<%=path %>/js/jquery.min.js?v=2.1.4"></script>
	<script src="<%=path %>/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="<%=path %>/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="<%=path%>/js/pager.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common.js"></script>

	
	
	<script type="text/javascript">
	$(document).ready(function(){
		
   	
		var $pager =  $("#data").pager({
			url:"${hsj}/platfrom/wallet/wallettxsqlist.htm",
			formId:"searchForm",
			pagerPages: 3,
			template:{
				header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover">'+
					'<thead>'+
						'<tr>'+
							'<th>用户名称</th>'+
							'<th>手机号</th>'+
							'<th>银行卡号</th>'+
							'<th>提现金额</th>'+
							'<th>到账金额</th>'+
							'<th>申请时间</th>'+
							'<th>操作</th>'+
						'</tr>'+
					'</thead><tbody>',
				body:'<tr>'+
							'<td>{uname}</td>'+
							'<td>{umobil}</td>'+
							'<td>{bank}</td>'+
							'<td >{txje}</td>'+
							'<td>{dzje}</td>'+
							'<td >{dzdate}</td>'+
							'<td id="oper_{id}">'+
								'<a href="javascript:void(0);" data="{id}" class="btn btn-danger btn-sm edit"><i class="fa fa-file"></i> 打款 </a> '+
								'<a href="javascript:void(0);" data="{id}" class="btn btn-success btn-sm detail"><i class="fa fa-file"></i> 驳回 </a> '+
							'</td>'+
						'</tr>',
					footer:'</tbody></table>',
					noData:'<tr><td colspan="9"><center>暂无数据</center></tr>'
			},
			callbak: function(result){
				var stuta = 0;
				var list = result.list;
				for(var i = 0 ; i < list.length ; i++){
					
						$(".detail").click(function () {
							stuta = 3;
							var id =$(this).attr("data");
							$.post('<%=path%>/wallet/wallettxupdata.htm',{stuta:stuta,id:id},function(r){
								parent.messageModel(r.resultMsg);

							},"json");
						});
					
						$(".edit").click(function () {
							stuta = 2;
							var id =$(this).attr("data");
							$.post('<%=path%>/wallet/wallettxupdata.htm',{stuta:stuta,id:id},function(r){
								parent.messageModel(r.resultMsg);
							},"json");
						});
				 
					

					
				
		<%-- 			var authExamine = list[i].authExamine;
					
					$btn.bind("click", function(){
						var isFreeze = "";
						if($(this).find("i").hasClass("fa-lock")){
							$(this).find("i").removeClass("fa-lock");
							$(this).find("i").addClass("fa-unlock");
							$(this).find("i").parent().find("span").html("启用");
							isFreeze = "true";
						}else{
							$(this).find("i").removeClass("fa-unlock");
							$(this).find("i").addClass("fa-lock");
							$(this).find("i").parent().find("span").html("冻结");
							isFreeze = "false";
						}
						var id =$(this).attr("data-id");
						$.post('<%=path%>/userInfo/updateFreezeStatus.htm',{isFreeze:isFreeze,id:id},function(r){
							parent.messageModel(r.resultMsg);
							if(r.resultCode=="1"){
								$pager.gotoPage($pager.pageNumber);
							}
						},"json");
					});
					if(authExamine == 3 || (list[i].authStatus || list[i].authStatus == "true")){
						$("#oper_"+list[i].id).append($btn); 
					}  --%>
				/* 	if(authExamine == 1){
						$examine = $('<a href="javascript:void(0);" style="margin-left:4px;" data-id="'+list[i].id+'" class="btn btn-primary btn-sm examine"><i class="fa fa-unlock"></i> <span>审核</span> </a>');
						$examine.click(function(){
							var id=$(this).attr("data-id");
							parent.openLayer('授信审核','${hsj}/userInfo/showApply.htm?id='+id,'1000px','700px',$pager);
						});
						$("#oper_"+list[i].id).append($examine);
					} */
				}
			
			
			}
		
		});
		
	

	
	});
	
</script>

	</body>
</html>
