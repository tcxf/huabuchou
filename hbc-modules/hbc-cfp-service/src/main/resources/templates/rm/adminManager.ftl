<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/plugins/iCheck/custom1.css" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
	<title> - 项目</title>
    <style>
		table{
			font-size: 14px;
		}
		input[type='radio']{
			background: #0e9aef!important;
		}
		li{
			list-style: none!important;
		}
		.ibox-tools a{
			padding:5px;
			background: #f49110;
			border-radius: 8px;
			float: right;
			color: #fff!important;
		}
        .left_mume .xuanzhong>a{
            background: #f49110;
            color: #fff;
            display: block;
        }
        .left_mume .xuanzhong>a:hover{
            background: #f49110;
            color: #fff;
            display: block;
        }
	</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInUp">
        <div class="all_content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 left_mume">
                        <ul>
                            <li class="xuanzhong">
                                <a href="#">拒绝授信</a>
                            </li>
                            <li>
                                <a href="${basePath!}/fund/InitialCredit/goChushiSx?id=${id!}&type=${type!}">初始授信</a>
                            </li>
                            <li>
                                <a href="${basePath!}/fund/creditCustom/goCreditCustomViews?id=${id!}&type=${type!}">提额授信</a>
                            </li>
                            <li>
                                <a href="${basePath!}/fund/creditCustom/goCreditCustomView?id=${id!}&type=${type!}">授信提额综合分值</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>拒绝授信</h5>
                                        <#if type != 'hidden'>
                                            <div  class="ibox-tools">
                                                <a href="javascript:void(0);" id="update" > 编  辑</a>
                                                <a href="javascript:void(0);" style="display: none" id="new" > 保  存</a>
                                                <a href="javascript:void(0);" style="display: none" id="qx" > 取  消</a>
                                            </div>
                                        </#if>
                                    </div>
                                    <div class="ibox-content">
                                        <form id="searchForm" class="form-inline">
                                            <div class="form-group">
                                                <div class="row m-b-sm m-t-sm">
                                                    <input type="hidden" id="relationshipId" value="${id!}" name="relationshipId">
                                                </div>
                                            </div>
                                        </form>
                                        <div class="project-list" style="overflow: auto" id="">
                                            <li   id="data">
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	</div>

	<!-- 全局js -->
	<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>


	<!-- 自定义js -->
	<script src="${basePath!}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${basePath!}/js/pager1.js"></script>
	<script src="${basePath!}/js/plugins/iCheck/icheck.min.js"></script>




    <script type="text/javascript" src="${basePath!}/js/common.js"></script>


	<style>
	/*定义滚动条高宽及背景 高宽分别对应横竖滚动条的尺寸*/  
::-webkit-scrollbar  
{  
    width: 5px;  
    height: 5px;  
    background-color: #F5F5F5;  
}  
  
/*定义滚动条轨道 内阴影+圆角*/  
::-webkit-scrollbar-track  
{  
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);  
    border-radius: 2px;  
    background-color: #F5F5F5;  
}  
  
/*定义滑块 内阴影+圆角*/  
::-webkit-scrollbar-thumb  
{  
    border-radius: 2px;  
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);  
    background-color: #555;  
}  
	</style>
	<script type="text/javascript">



	$(document).ready(function(){

	  var $pager = $("#data").pager({
			url:"${basePath!}/fund/creditCustom/selectRefusingCreditByfId",
			formId:"searchForm",
			pagerPages: 3,
			template:{
                body:'<tr class="kk" >'+
                '<td> <input readonly="readonly" class="name"  style="outline: none;border:none" name="name"  value="{name}" type="text" placeholder=""/>'+
                ' <input style="display: none" name="code" value="{code}" type="text" placeholder=""/>'+
                ' <input style="display: none" name="status"  value="{status}" type="text" placeholder=""/>'+
                ' <input style="display: none" name="versionId"  value="{versionId}" type="text" placeholder=""/>'+
                ' <input style="display: none" name="id" value="{id}" type="text" placeholder=""/>'+
                ' <input style="display: none" name="content"  value="{content}" type="text" placeholder=""/>'+
                '</td>'+
                '<td> <input readonly="readonly" onkeyup="value=value.replace(/[^\\d]/g,\'\')"  class="number" style="outline: none;border:none"   name="number"  id="number" type="text" value="{number}" placeholder=""/></td>'+
                '<td>'+
                '' +
                '<input class="dx" disabled="disabled"   value="1" type="radio" name="zt_{id}">启用&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                '<input class="dx" disabled="disabled" type="radio" value="0" name="zt_{id}">禁用' +

                '<td>'+
                '' +
                '<input class="dx" disabled="disabled" type="radio" value="1" name="sg_{id}" >是&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                '<input class="dx"  disabled="disabled"  value="0" type="radio" name="sg_{id}">否' +
                '</td>'+
                '</tr>',
                header:'<div class="fixed-table-container form-group">'+
				'<table class="table table-hover table-bordered">'+
					'<thead>'+
						'<tr>'+
							'<th>类别</th>'+
							'<th>条件</th>'+
							'<th>状态</th>'+
               				 '<th>是否同意二次申请(三个月后可再次申请)</th>'+
						'</tr>'+
					'</thead><tbody>',
					footer:'</tbody></table>',
			},
			callbak: function(result){
				var list = result.data.records;
				for(var i = 0 ; i < list.length ; i++){
                        $("input[type=radio][name=zt_"+list[i].id+"][value="+list[i].status+"]").attr("checked",'checked');
                        $("input[type=radio][name=sg_"+list[i].id+"][value="+list[i].twoapplications+"]").attr("checked",'checked');

				}




				$('input').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
					increaseArea: '20%' // optional
				});
				// check 全部选中
				$('.checks').on('ifChecked', function(event){
					 $("input[id^='check-id']").iCheck('check');
				});

				// check 全部移除
				$('.checks').on('ifUnchecked', function(event){
					 $("input[id^='check-id']").iCheck('uncheck');
				});
			}
		});

        $("select").select();

		$("#loading-example-btn").click(function(){
			$pager.gotoPage($pager.pageNumber);
		});

		$("#qx").click(function () {
            location.reload();
            <#--window.location = '${basePath!}/fund/creditCustom/test';-->
        })
		//编辑
		$("#update").click(function () {
		    $(".number").css("border","1px solid #dadada");
            // $(".name").css("border","1px solid #dadada");
            $(".number").removeAttr("readonly");
            $('.dx').removeAttr("disabled");
            document.getElementById("qx").style.display = "block";
            document.getElementById("new").style.display = "block";
            document.getElementById("update").style.display = "none";
        })
		$("#new").click(function (){
		    var type =0;
            $(".kk").each(function(){
                var ss ={};
                var name = $(this).find("input[name='name']").val();
                var number = $(this).find("input[name='number']").val();
                if(number==0){
                    type=1;
                    parent.messageModel("条件个数必须大于零");
                    return;

                }
                var code = $(this).find("input[name='code']").val();
                var versionId = $(this).find("input[name='versionId']").val();
                var content = $(this).find("input[name='content']").val();
                var id = $(this).find("input[name='id']").val();
                var status =$("input[name=zt_"+id+"]:checked").val();
                var twoapplications = $("input[name=sg_"+id+"]:checked").val();
            });
            if(type==0){
                var array = new Array();
                $(".kk").each(function(){
                    var ss ={};
                    var name = $(this).find("input[name='name']").val();
                    var number = $(this).find("input[name='number']").val();
                    if(number==0){
                        type=1;
                        return;

                    }
                    var code = $(this).find("input[name='code']").val();
                    var versionId = $(this).find("input[name='versionId']").val();
                    var content = $(this).find("input[name='content']").val();
                    var id = $(this).find("input[name='id']").val();
                    var status =$("input[name=zt_"+id+"]:checked").val();
                    var twoapplications = $("input[name=sg_"+id+"]:checked").val();
                    ss.name=name;
                    ss.number=number;
                    ss.code=code;
                    ss.status=status;
                    ss.versionId=versionId;
                    ss.content=content;
                    ss.twoapplications=twoapplications;
                    ss.relationshipId = '${id!}';
                    array.push(ss);
                });
                $.ajax({
                    url:'${basePath!}/fund/creditCustom/createRefusingCredit',
                    type:'post', //数据发送方式
                    dataType:'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
                    contentType:"application/json",
                    data:JSON.stringify(array),
                    success: function(data){ //成功
                        //消息对话框
                        parent.messageModel(data.msg);
                        if (data.code == 0){
                            location.reload();
                        }
                    }
                });
			}else{
                parent.messageModel("条件个数必须大于零");
                return;
			}

		});
	});
	
</script>

	</body>
</html>
