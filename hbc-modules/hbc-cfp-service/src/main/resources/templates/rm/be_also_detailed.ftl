 <!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .month_pay{
            margin-top: 10px;
            margin-left: 20px;
        }
        .month_pay .col-sm-2>select{
            width: 160px;
            height: 30px;
        }
        .month_pay .col-sm-3{
            padding-left: 0;
            text-align: left;
        }
        .month_pay .col-sm-3 input{
            width: 100%;
            height: 30px;
            padding-left: 10px;
        }
        .month_pay .col-sm-6{
            text-align: right;
        }
        .month_pay .col-sm-6>a{
            text-decoration: none;
            color: #f49110;
            padding: 8px 10px;
            border: 1px solid #f49110;
            border-radius: 3px;
        }
        .month_pay .col-sm-6>a>img{
            width: 16px;
        }
        .month_form{
            margin-top: 20px;
            margin-left: 10px;
        }
        .repayment_state{
            position: relative;
        }
        .a1{
            position: absolute;
            width: 30px;
            height: 30px;
            line-height: 30px;
            background:rgb(102, 153, 0);
            top:0;
            right: 0;
            color: #fff;
            text-align: center;
        }
        .records{
            position: relative;
        }
        .add_div{
            width: 220px;
            height: 160px;
            border: 1px solid #ededed;
            position: absolute;
            top:35px;
            left: 0;
            display: none;
            border-radius: 8px;
        }
        .select_div{
            margin-top: 10px;
            width: 100%;
            text-align: center;
            height: 60px;
            line-height: 30px;
            border-bottom: 1px solid #ededed;
        }
        .select_div select{
            width: 140px;
            height: 30px;
        }
        .btn-info{
            margin-top: -5px;
        }
        .add_content{
            width: 100%;
            height: 130px;
            background: #f5f5f5;
            overflow: auto;
        }
        .add_content>p{
            padding: 10px;
        }
        .month_titie{
            position: fixed;
            bottom:60px
        }
        .month_titie .col-sm-10>ul{
            padding: 0;
            margin: 0;
        }
        .a2{
            width: 30px;
            height: 30px;
            line-height: 30px;
            background:rgb(102, 153, 0);
            color: #fff;
            text-align: center;
        }
    </style>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="top_text">
            <div class="top_cont">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4 no_sx">
                            接口使用记录
                        </div>
                    </div>
                </div>
            </div>
            <div class="month_pay">
                <form id="searchForm" class="form-inline">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-2">
                                <select name="" id="">
                                    <option value="">所有还款状态</option>
                                    <option value="">已还款</option>
                                    <option value="">未还款</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" placeholder="搜索姓名/手机号">
                            </div>
                            <div class="col-sm-1">
                                <input type="button" id="search" class="btn btn-warning" value="搜索" />
                            </div>
                            <div class="col-sm-1 col-sm-offset-5">
                                <a href="#">
                                    <img src="img/u4205.png" alt="">
                                    <span>导出</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="month_form">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>姓名</th>
                            <th>手机号</th>
                            <th>使用金额 (元)</th>
                            <th>还款金额 (元)</th>
                            <th class="repayment_state">
                                还款状态
                                <div class="a1">A1</div>
                            </th>
                            <th>前3日电话提醒</th>
                            <th>前1日电话提醒</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>张三</td>
                            <td>151 1234 5678</td>
                            <td>100.00</td>
                            <td>20.00</td>
                            <td>未还款</td>
                            <td class="records">
                                <a href="#">记录▼</a>
                                <div class="add_div">
                                    <div class="select_div">
                                        <select name="" id="">
                                            <option value="">请选择联系状态</option>
                                            <option value="">已通知</option>
                                            <option value="">未接通</option>
                                            <option value="">占线</option>
                                            <option value="">空号</option>
                                            <option value="">停机</option>
                                        </select>
                                        <a href="#" class="btn btn-info">确定</a>
                                    </div>
                                    <div class="add_content">
                                        <p>2018-09-19 16:20:22 &nbsp&nbsp<span>未接通</span></p>
                                    </div>
                                </div>
                            </td>
                            <td class="records">
                                <a href="#">记录▼</a>
                                <div class="add_div">
                                    <div class="select_div">
                                        <select name="" id="">
                                            <option value="">请选择联系状态</option>
                                            <option value="">已通知</option>
                                            <option value="">未接通</option>
                                            <option value="">占线</option>
                                            <option value="">空号</option>
                                            <option value="">停机</option>
                                        </select>
                                        <a href="#" class="btn btn-info">确定</a>
                                    </div>
                                    <div class="add_content">
                                        <p>2018-09-19 16:20:22&nbsp&nbsp<span>未接通</span></p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <div class="month_titie">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-1">
                                <div class="a2">A1</div>
                            </div>
                            <div class="col-sm-10">
                                <ul>
                                    <li>只展示本月应还账单</li>
                                    <li>状态分为：已还款、未还款，已还款的账单无需标记</li>
                                    <li>已通知的账单不能重复标记，只有未接通、占线的账单可重复标记，需记录每次的标记及标记时间</li>
                                    <li>标记为空号、停机的账单，将直接转入后期管理</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- 全局js -->
    <script src="js/jquery.min.js?v=2.1.4"></script>
    <script src="js/bootstrap.min.js?v=3.3.6"></script>
    <script>
        $(document).ready(function () {
            $(".records>a").click(function () {
                $(this).nextAll(".add_div").eq(0).slideToggle();

            });

            $(".btn-info").click(function () {
              var s=  $(".select_div").find("option:selected").text();
                $(".add_div").css("display","none");
            })
        });



    </script>
</body>
</html>
