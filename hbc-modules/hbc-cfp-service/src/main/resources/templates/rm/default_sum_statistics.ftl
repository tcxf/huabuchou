<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .sx_date_form{
            margin-top: 10px;
            margin-left: 20px;
        }
        .date_form{
            margin-top: 10px;
            margin-left: 16px;
        }
        .sx_date_form .col-sm-3 input{
            width: 120px;
            height: 30px;
        }

        .sx_date_form .col-sm-8{
            text-align: right;
        }
        .sx_date_form .col-sm-8>a{
            text-decoration: none;
            color: #f49110;
            padding: 8px 10px;
            border: 1px solid #f49110;
            border-radius: 3px;
        }
        .sx_date_form .col-sm-8>a>img{
            width: 16px;
        }

    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="sx_date_form">
        <form id="searchForm" class="form-inline">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <input placeholder="开始日期" class="form-control layer-date" id="start">
                        &nbsp
                        ～
                        &nbsp
                        <input placeholder="结束日期" class="form-control layer-date" id="end">
                    </div>
                    <div class="col-sm-1">
                        <input type="button" id="search" class="btn btn-warning" value="搜索" />
                    </div>
                    <div class="col-sm-8">
                        <a href="#">
                            <img src="img/u4205.png" alt="">
                            <span>导出</span>
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <div class="date_form">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>日期</th>
                    <th>违约人数</th>
                    <th>还款人数</th>
                    <th>违约回款率</th>
                    <th>违约金额 (元)</th>
                    <th>回款金额 (元)</th>
                    <th>滞纳金收入 (元)</th>
                    <th>违约客户流转率</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>2018-09-12</td>
                    <td>100</td>
                    <td>15</td>
                    <td>20%</td>
                    <td>10,000.00</td>
                    <td>8,000.00</td>
                    <td>200.00</td>
                    <td>5%</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="js/jquery.min.js?v=2.1.4"></script>
<script src="js/bootstrap.min.js?v=3.3.6"></script>
<script src="js/plugins/layer/laydate/laydate.js"></script>

<script>
    $(document).ready(function () {
        //日期范围限制
        var start = {
            elem: '#start',
            format: 'YYYY/MM/DD',
            max: '2099-06-16 23:59:59', //最大日期
            istime: true,
            istoday: false,
            choose: function (datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: '#end',
            format: 'YYYY/MM/DD',
            max: '2099-06-16 23:59:59',
            istime: true,
            istoday: false,
            choose: function (datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };
        laydate(start);
        laydate(end);

    });

</script>
</body>

</html>
