<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .sx_date_form{
            margin-top: 10px;
            margin-left: 20px;
        }
        .sx_date_form .col-sm-3 input{
            width: 120px;
            height: 30px;
        }
        .sx_date_form .col-sm-2>select{
            width: 160px;
            height: 30px;
        }
        .sx_date_form .col-sm-5 input[type="text"]{
            width: 320px;
            height: 30px;
            padding-left: 10px;
        }
        .sx_date_form .col-sm-5 input[type="button"]{
            margin-top: -5px;
        }
        .date_form{
            margin-top: 10px;
            margin-left: 16px;
        }
        .date_form img{
            width: 16px;
            margin-top: -5px;
            margin-left: 20px;
        }
        .appointment_time{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            height: 700px;
        }
       .appointment_time>.modal_content{
            width: 50%;
            height: 260px;
            background: #fff;
            margin-left: 25%;
            margin-top: 5%;
            border-radius: 8px;
            position: relative;
        }
        .modal_head{
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #f5f5f5;
            padding-left: 10px;
            border-top-right-radius: 8px;
            border-top-left-radius: 8px;
        }
        .modal_head p{
            float: left;
        }
        .modal_head b{
            float: right;
            padding-right: 20px;
            contain: paint;
            font-size: 20px;
        }
        .appointment_time>.modal_content .modal_body{
            clear: both;
            width: 100%;
            height: 160px;
            overflow: auto;
            padding: 20px 0;
        }
        .appointment_time>.modal_content .modal_body>ul{
            padding: 0;
            margin: 0;
        }
        .appointment_time>.modal_content .modal_body>ul li{
            list-style: none;
        }
        .appointment_time>.modal_content .modal_foots{
            position: absolute;
            bottom:20px;
            right: 20px;
        }
        .appointment_time>.modal_content .modal_foots>a{
            padding: 10px 30px;
            background: #f49110;
            border-radius: 8px;
            color: #fff;
        }
        .collection{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            height: 700px;
        }
        .collection>.modal_content{
            width: 60%;
            height: 500px;
            background: #fff;
            margin-left: 20%;
            margin-top: 2%;
            border-radius: 8px;
            position: relative;
        }
        .modal_foot{
            position: absolute;
            bottom: 20px;
            width: 100%;
        }
        .modal_foot>.sx_zf{
            width: 100%;
        }
        .modal_foot>.sx_zf>ul{
            padding: 0;
            margin: 0;
        }
        .modal_foot>.sx_zf li{
            list-style: none;
            height: 40px;
            line-height: 40px;
            float: right;
            width: 110px;
            text-align: center;
            border-radius: 8px;
        }
        .modal_foot>.sx_zf .sx_sore{
            background: #f5f5f5;
            margin-right: 10px;
        }
        .modal_foot>.sx_zf .sx_ok{
            background: #f49110;
            color: #fff;
            margin-right: 10px;
        }
        .modal_foot>.sx_zf .sx_ok>a{
            color: #fff;
        }
        .modal_menu{
            width: 100%;
            height: 60px;
        }
        .modal_menu>ul li{
            list-style: none;
            width: 20%;
            height: 40px;
            line-height: 40px;
            border: 1px solid #ededed;
            float: left;
            text-align: center;
        }
        .modal_menu>ul .active{
            background: #f49110;

        }
        .modal_menu>ul li>a{
            color: #000;
        }
        .modal_menu>ul .active>a{
            color: #fff;
        }
        .tab-content{
            clear: both;
        }
        .basic_information{
            margin-left: 40px;
            height: 300px;
            overflow: auto;
        }
        .basic_information td{
           width: 200px;
            height: 40px;
       }
        .authorization_message{
            margin-top: 20px;
            margin-left: 20px;
        }
        .authorization_ok{
            width: 40px;
            height: 16px;
            border-radius: 30px;
            background: #f49110;
            display: inline-block;
        }
        .this_ok{
            color: #f49110;
        }
        .authorization_no{
            width: 40px;
            height: 16px;
            border-radius: 30px;
            background: #999;
            display: inline-block;
        }
        .this_no{
            color: #999;
        }
        .authorization_list{
            margin-top: 40px;
            margin-left: 10px;
            width: 100%;
        }
        .authorization_list>ul{
            padding: 0;
            margin: 0;
        }
        .authorization_list li{
            list-style: none;
            float: left;
            width: 20%;
            background: #999;
            margin-right: 10px;
            height: 40px;
            line-height: 40px;
            text-align: center;
            border-radius: 30px;
            color: #fff;
            margin-bottom: 20px;
        }
        .yisq{
            background: #f49110 !important;
        }
        .cuishou{
            width: 100%;
        }
        .cuishou>ul{
            padding: 0;
            margin: 0;
        }
        .cuishou>ul>li{
            list-style: none;
            width: 40%;
            height: 320px;
            border: 1px solid #ededed;
            float: left;
            margin-left: 4%;
            overflow: auto;
            padding: 20px 5px;
        }
        .cuishou li .row{
            height: 40px;

        }
        .cuishou li .row input{
            height: 30px;
            padding-left: 10px;
            border: 1px solid #ededed;
        }
        .cuishou li .row select{
            width: 100%;
            height: 30px;
        }
        .cuishou li .row textarea{
            width: 100%;
        }
        .change_text{
            padding-top: 20px;
            clear: both;
        }
        .change_text li{
            list-style: none;
            float: right;
            width: 30%;
            height: 30px;
            line-height: 30px;
            text-align: center;
        }
        .change_text li>a{
            padding:8px 15px;
            color: #fff;
        }
        .change_text li>.ok_text{
            border: 1px solid #f49110;
            color: #f49110;
        }
        .change_text li>.clear_text{
            border: 1px solid #ededed;
            color: #333;
        }
        .conllection_content{
            padding: 0!important;
        }
        .collection_text{
            width: 100%;
            height: 30px;
            line-height: 30px;
            background: #f5f5f5;
            padding-left: 10px;
        }
        .conllection_list{
            width: 100%;
            padding-top: 10px;
            color: #000;
        }
        .conllection_list>ul{
            padding: 0;
            margin: 0;
        }
        .conllection_list li{
            list-style: none;
            padding: 5px;
            border-bottom: 1px solid #ededed;
        }
        .conllection_list .row{
            height: 30px!important;
        }
        .cuishou_date{
            color: #999;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="sx_date_form">
        <form id="searchForm" class="form-inline">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <input placeholder="开始日期" class="form-control layer-date" id="start">
                        &nbsp
                        ～
                        &nbsp
                        <input placeholder="结束日期" class="form-control layer-date" id="end">
                    </div>
                    <div class="col-sm-2">
                        <select name="" id="">
                            <option value="">所有授信状态</option>
                            <option value="">已授信</option>
                            <option value="">被拒绝</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="" id="">
                            <option value="">所有还款状态</option>
                            <option value="">已还款</option>
                            <option value="">未出账</option>
                            <option value="">逾期中</option>
                            <option value="">/</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" placeholder="搜索姓名/手机号">
                        <input type="button" id="search" class="btn btn-warning" value="搜索" />
                    </div>
                </div>
            </div>
        </form>
        <div class="date_form">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>流水号</th>
                    <th>姓名</th>
                    <th>手机号</th>
                    <th>预约还款时间</th>
                    <th>违约天数</th>
                    <th>应还金额 (元)</th>
                    <th>催记操作</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>2018062101</td>
                    <td>张三</td>
                    <td>151 1234 5678</td>
                    <td>2018-09-13  10:00
                        <a href="#"><img src="img/u5711.png" alt=""></a>
                    </td>
                    <td>10</td>
                    <td>1,500.00</td>
                    <td>
                        <a href="#" class="collection_msg">催收详情</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--预约还款情况-->
            <div class="appointment_time">
                <div class="modal_content">
                    <div class="modal_head">
                        <p>预约还款情况</p>
                        <b>×</b>
                    </div>
                    <div class="modal_body">
                       <ul>
                           <li>
                               <div class="container-fluid">
                                   <div class="row">
                                       <div class="col-sm-2">
                                           姓名: <span>张三</span>
                                       </div>
                                       <div class="col-sm-3">
                                           手机: <span>13912345678</span>
                                       </div>
                                       <div class="col-sm-4">
                                           预约还款时间: <span>2018-09-13  10:00</span>
                                       </div>
                                       <div class="col-sm-3">
                                           已过时间，未还
                                       </div>
                                   </div>
                               </div>
                           </li>
                       </ul>
                    </div>
                    <div class="modal_foots">
                        <a href="#" class="sx_ok">确定</a>
                    </div>
                </div>
            </div>
            <!--催收详情-->
            <div class="collection">
                <div class="modal_content">
                    <div class="modal_head">
                        <p>催收详情</p>
                        <b>×</b>
                    </div>
                    <div class="modal_body">
                        <div class="tab-container">
                            <div class="modal_menu">
                                <ul>
                                    <li class="active"><a data-toggle="tab" href="#tab-a">基本信息</a>
                                    </li>
                                    <li class=""><a data-toggle="tab" href="#tab-b">授权信息</a>
                                    </li>
                                    <li class=""><a data-toggle="tab" href="#tab-c">催收记录</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="tab-content">
                                <div id="tab-a" class="tab-pane active">
                                   <div class="basic_information">
                                        <table>
                                            <tr>
                                                <td>姓名：</td>
                                                <td>张三</td>
                                            </tr>
                                            <tr>
                                                <td>手机号码：</td>
                                                <td>151 1234 5678</td>
                                            </tr>
                                            <tr>
                                                <td>授信额度 (元)：</td>
                                                <td>1,500.00</td>
                                            </tr>
                                            <tr>
                                                <td>使用额度 (元)：</td>
                                                <td>1,000.00</td>
                                            </tr>
                                            <tr>
                                                <td>本期应还金额 (元)：</td>
                                                <td>300.00 <span style="color: #ff0000">（本金+手续费）</span></td>
                                            </tr>
                                            <tr>
                                                <td>本期应还日期：</td>
                                                <td>2018-09-10</td>
                                            </tr>
                                            <tr>
                                                <td>违约天数：</td>
                                                <td>3天</td>
                                            </tr>
                                            <tr>
                                                <td>截止今日应还款金额 (元)：</td>
                                                <td>320.00 <span style="color: #ff0000">（本金+手续费+滞纳金）</span></td>
                                            </tr>
                                            <tr>
                                                <td>微信号/QQ</td>
                                                <td>123456789</td>
                                            </tr>
                                            <tr>
                                                <td>紧急联系人及联系方式</td>
                                                <td>李四  156 1234 5678</td>
                                            </tr>
                                            <tr>
                                                <td>预约还款时间</td>
                                                <td>
                                                    <input class="form-control layer-date" placeholder="YYYY-MM-DD hh:mm:ss" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                </td>
                                            </tr>
                                        </table>
                                   </div>
                                </div>
                                <div id="tab-b" class="tab-pane">
                                   <div class="authorization_message">
                                        <div class="one_top">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="authorization_ok"></div>
                                                        <span class="this_ok">已授权</span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="authorization_no"></div>
                                                        <span class="this_no">未授权</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       <div class="authorization_list">
                                           <ul>
                                               <li class="yisq">手机运营商详单
                                                   <a href="#" class="updates" data-toggle="tooltip" data-placement="top" title="下载运营商详单">
                                                       <img src="img/update_img.png" alt="">
                                                   </a>
                                               </li>
                                               <li>京东授权</li>
                                               <li>淘宝授权</li>
                                               <li>支付宝授权</li>
                                               <li>学信网授权</li>
                                               <li>社保授权</li>
                                               <li>公积金授权</li>
                                               <li>滴滴授权</li>
                                           </ul>
                                       </div>
                                   </div>
                                </div>
                                <div id="tab-c" class="tab-pane">
                                    <div class="cuishou">
                                        <ul>
                                            <li>
                                               <div class="container-fluid">
                                                   <div class="row">
                                                       <div class="col-sm-4">
                                                           催收时间
                                                       </div>
                                                       <div class="col-sm-8 cs_time">
                                                           2018-9-14 17:34:01
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-sm-4">
                                                           联系人姓名
                                                       </div>
                                                       <div class="col-sm-8">
                                                           <input type="text" id="names" placeholder="请输入联系人姓名">
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-sm-4">
                                                           与客户关系
                                                       </div>
                                                       <div class="col-sm-8 guanxis">
                                                           <select name="" id="">
                                                               <option value="">本人</option>
                                                               <option value="">父母</option>
                                                               <option value="">兄弟</option>
                                                               <option value="">姊妹</option>
                                                               <option value="">夫妻</option>
                                                           </select>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-sm-4">
                                                           备注
                                                       </div>
                                                       <div class="col-sm-8">
                                                          <textarea name="textareaname" rows="5" cols="20"></textarea>
                                                       </div>
                                                   </div>
                                                   <div class="change_text">
                                                        <ul>
                                                            <li>
                                                                <a href="#" class="clear_text">清除</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="ok_text">确定</a>
                                                            </li>
                                                        </ul>
                                                   </div>
                                               </div>
                                            </li>
                                            <li class="conllection_content">
                                                <div class="collection_text">
                                                    <p>催收记录</p>
                                                </div>
                                                <div class="conllection_list">
                                                    <ul>
                                                        <li>
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        联系人姓名：<span class="names">张三</span>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        与客户关系：<span class="guanxi">本人</span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        备注：<span class="beizhu">无</span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12 cuishou_date">
                                                                        2018-09-10 09:21:21
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal_foot">
                        <div class="sx_zf">
                            <ul>
                                <li class="sx_sore">
                                    取消
                                </li>
                                <li class="sx_ok">
                                    <a href="#">确定</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="js/jquery.min.js?v=2.1.4"></script>
<script src="js/bootstrap.min.js?v=3.3.6"></script>
<script src="js/plugins/layer/laydate/laydate.js"></script>

<script>
    $(document).ready(function () {
        //日期范围限制
        var start = {
            elem: '#start',
            format: 'YYYY/MM/DD',
            max: '2099-06-16 23:59:59', //最大日期
            istime: true,
            istoday: false,
            choose: function (datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: '#end',
            format: 'YYYY/MM/DD',
            max: '2099-06-16 23:59:59',
            istime: true,
            istoday: false,
            choose: function (datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };
        laydate(start);
        laydate(end);
        $(".date_form tr img").click(function () {
            $(".appointment_time").css("display","block")
        });
        $(".collection_msg").click(function () {
            $(".collection").css("display","block")
        });

        $(".modal_head b").click(function (){
            $(".appointment_time").css("display","none");
            $(".collection").css("display","none");
        });
        $(".sx_ok").click(function (){
            $(".appointment_time").css("display","none");
            $(".collection").css("display","none");
        });
        $(".sx_sore").click(function (){
            $(".collection").css("display","none");
        });

        $(".ok_text").click(function () {
            var times=$(".cs_time").html();
            var names=$("#names").val();
            var guanxi= $(".guanxis").find("option:selected").text();
            var beizhu=$("textarea").val();
            if(names==""){
                $(".names").html("无");
                return
            }else{
                $(".names").html(names);
            }
            if(beizhu==""){
                $(".beizhu").html("无");
                return
            }else{
                $(".beizhu").html(beizhu);
            }
            $(".cuishou_date").html(times);
            $(".guanxi").html(guanxi);

        });
        $(".clear_text").click(function (){
            $("#names").val("");
            $("textarea").val("");
        })
    });

</script>
</body>

</html>
