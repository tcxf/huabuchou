<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">

    <title> 风控主页</title>

    <meta name="keywords" content="">
    <meta name="description" content="">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

     <link href="css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?v=4.1.0" rel="stylesheet">
    <style>

    </style>
</head>

<body class="fixed-sidebar full-height-layout gray-bg">
  <div class="fk_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2">
                    <img src="img/u7.png" alt="">
                    <b>风控管理系统</b>
                </div>
                <div class="col-sm-10">
                    <ul>
                        <li>
                            <a href="#">注销</a>
                        </li>
                        <li>
                            admin
                        </li>
                    </ul>
                </div>
            </div>
        </div>
  </div>
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a class="J_menuItem" href="index_v1.html">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">首页</span>
                        </a>
                    </li>
                    <li>
                        <a class="J_menuItem" href="data_analysis.html">
                            <i class="glyphicon glyphicon-folder-open"></i>
                            <span class="nav-label">数据分析</span>
                        </a>
                    </li>
                    <li>
                        <a href="#"><span class="nav-label">前期审核</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="user_detail.html">
                                <i class="glyphicon glyphicon-credit-card"></i>
                                <span>授信用户明细</span>
                            </a>
                            </li>
                            <li><a class="J_menuItem" href="score_customization.html">
                                <i class="glyphicon glyphicon-th-large"></i>
                                <span>评分自定义</span>
                            </a>
                            </li>
                            <li><a class="J_menuItem" href="daily_credit_report.html">
                                <i class="glyphicon glyphicon-file"></i>
                                <span>授信日报表</span>
                            </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><span class="nav-label">中期管理</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="be_also_detailed.html">
                                <i class="fa fa-money"></i>
                                <span>本月应还明细</span>
                            </a>
                            </li>
                            <li><a class="J_menuItem" href="repayment_statistics.html">
                                <i class="fa fa-user-secret"></i>
                                <span>已还款统计</span>
                            </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><span class="nav-label">后期管理</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="default_user_details.html">
                                <i class="fa fa-exclamation-circle"></i>
                                <span>违约用户明细</span>
                            </a>
                            </li>
                            <li><a class="J_menuItem" href="personal_default_statistics.html">
                                <i class="fa fa-user-times"></i>
                                <span>个人违约统计</span>
                            </a>
                            </li>
                            <li><a class="J_menuItem" href="default_sum_statistics.html">
                                <i class="fa fa-pie-chart"></i>
                                <span>违约总和统计</span>
                            </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="title_text">
                    首页
                </div>
            <div class="row J_mainContent" id="content-main">
                <iframe id="J_iframe" width="100%" height="100%" src="index_v1.html?v=4.0" frameborder="0" data-id="index_v1.html" seamless></iframe>
            </div>
        </div>
        <!--右侧部分结束-->
    </div>

    <!-- 全局js -->
    <script src="js/jquery.min.js?v=2.1.4"></script>
    <script src="js/bootstrap.min.js?v=3.3.6"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
  <script src="js/hAdmin.js?v=4.1.0"></script>
    <script type="text/javascript" src="js/index.js"></script>

  <script>
      $("#side-menu li>a").click(function () {
          var t=$(this).text();
         $(".title_text").html(t);
      });

  </script>
</body>

</html>
