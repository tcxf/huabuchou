<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--360浏览器优先以webkit内核解析-->
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">

    <title> - 主页</title>
    <#include "common/common.ftl">
    <style>
        ul {
            padding: 0;
            margin: 0;
        }

        .top_title {
            width: 98%;
            margin-left: 2%;
            font-size: 14px;
            font-weight: 600;
        }

        .user_data {
            width: 98%;
            margin-left: 2%;
            margin-top: 20px;
            clear: both;
        }

        .user_data li {
            width: 20%;
            height: 100px;
            list-style: none;
            border: 1px solid #fff7ee;
            box-shadow: 1px 1px 1px 2px #fff7ee;
            float: left;
            margin-right: 2%;
            text-align: center;
            padding-top: 2px;
        }

        .user_data li h3 {
            font-size: 34px;
            color: #f49110;
        }

        .sx_tj {
            width: 98%;
            margin-left: 2%;
            font-size: 14px;
            font-weight: 600;
            clear: both;
            margin-top: 180px;
        }

        .xs_table {
            width: 100%;
            margin-left: 2%;
            margin-top: 20px;
            height: 300px;
            overflow: auto;
        }

        .xs_tables {
            width: 100%;
            margin-left: 2%;
            margin-top: 20px;
            height: 300px;
            overflow: auto;
        }

        .table tr th {
            text-align: center;
            border: 2px solid #fff7ee !important;
            font-weight: 600;
            background: #fff7ee;
        }

        .table tr td {
            text-align: center;
            border: 2px solid #fff7ee !important;
        }

        .bottom_title {
            width: 90%;
            margin-left: 2%;
            text-align: right;
        }

        .se_menu {
            padding-left: 20px;
            padding-top: 10px;
        }

        .se_menu select {
            width: 120px;
            height: 30px;
            text-align: center;
        }

        .table_content {
            width: 100%;
        }

        .text-muted {
            color: transparent;
            font-size: 16px;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        授信用户统计
                    </div>
                </div>
            </div>
        </div>
        <div class="user_data">
            <ul>
                <li>
                    <p>昨日新增用户</p>
                    <h3>${countpople!}</h3>
                </li>
                <li>
                    <p>昨日新增授信额度（元）</p>
                    <h3>${sunmoeny!}</h3>
                </li>
                <li>
                    <p>总授信用户（人）</p>
                    <h3>${countpoples!}</h3>
                </li>
                <li>
                    <p>总授信额度（元）</p>
                    <h3>${sunmoenys!}</h3>
                </li>
            </ul>
        </div>
        <div class="sx_tj">
            授信月份统计
        </div>
        <div class="se_menu">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2">
                        <select name="fid" id="fid">
                            <option value="">请选择资金方</option>
							        <#list list as list >
                                        <option id="id" value="${list.fid!}" selected> ${list.fname!}</option>
                                    </#list>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="oid" id="oid">
                            <option value="">请选择运营商</option>
                                <#list list as list >
                                     <option id="id" value="${list.oid!}" selected> ${list.oname!}</option>
                                </#list>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="table_content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="xs_table">
                            <table class="table table-bordered">
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="xs_tables">
                            <table class="table table-bordered">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




<#--<div class="bottom_title">-->
<#--&lt;#&ndash;合计记录: <span>7条</span>&ndash;&gt;-->
<#--</div>-->
</div>
<!-- 全局js -->
<script type="text/javascript">
    $(document).ready(function () {
        $("select").select();
        f();
        f1();

        function f() {
            var fid = $("#fid").val();
            var $pager = $(".xs_table").pager({
                url: "${basePath!}/fund/Credituserstatistics/QueryCredituserstatistics?fid=" + fid,
                formId: "searchForm",
                pagerPages: 3,
                template: {
                    header: '<div class="fixed-table-container form-group">' +
                    '<table class="table table-hover">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>月份</th>' +
                    '<th>授信人数（人）</th>' +
                    '<th>授信额度（元）</th>' +
                    '<th>平均授信额度（元）</th>' +
                    '<th>通过率</th>' +
                    '<th>额度使用率</th>' +
                    '</tr>' +
                    '</thead><tbody>',
                    body: '<tr>' +
                    '<td>{month}</td>' +
                    '<td>{sxPeople}</td>' +
                    '<td>{sxMoney}</td>' +
                    '<td>{avgSxMoney}</td>' +
                    '<td>{passRate}%</td>' +
                    '<td>{usingMoneyRate}%</td>' +
                    '</tr>',
                    footer: '</tbody></table>',
                    noData: '<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function (result) {
                    var list = result.data.records;
                    for (var i = 0; i < list.length; i++) {
                    }
                    // check 全部选中
                    $('.checks').on('ifChecked', function (event) {
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function (event) {
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                }
            });
        }

        function f1() {
            var fid = $("#fid").val();
            var $pager = $(".xs_tables").pager({
                url: "${basePath!}/fund/Credituserstatistics/QueryCredituserstatisticss?fid=" + fid,
                formId: "searchForm",
                pagerPages: 3,
                template: {
                    header: '<div class="fixed-table-container form-group">' +
                    '<table class="table table-hover">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>还款周期</th>' +
                    '<th>违约率</th>' +
                    '<th>不良率</th>' +
                    '<th>坏账率</th>' +
                    '</tr>' +
                    '</thead><tbody>',
                    body: '<tr>' +
                    '<td>{repaymentTime}</td>' +
                    '<td>{breakContractRate}%</td>' +
                    '<td>{unhealthyRate}%</td>' +
                    '<td>{badDebtRate}%</td>' +
                    '</tr>',
                    footer: '</tbody></table>',
                    noData: '<tr><td colspan="10"><center>暂无数据</center></tr>'
                },
                callbak: function (result) {
                    var list = result.data.records;
                    for (var i = 0; i < list.length; i++) {
                    }
                    // check 全部选中
                    $('.checks').on('ifChecked', function (event) {
                        $("input[id^='check-id']").iCheck('check');
                    });

                    // check 全部移除
                    $('.checks').on('ifUnchecked', function (event) {
                        $("input[id^='check-id']").iCheck('uncheck');
                    });
                }
            });

            $("#fid").change(function () {
                var fid = $("#fid").val();
                if(fid==null||fid==""){
                    return;
                }
                f();
                f1();
            })
        }
    });
</script>
</body>

</html>
