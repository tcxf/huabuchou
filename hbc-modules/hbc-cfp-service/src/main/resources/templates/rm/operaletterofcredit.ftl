<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <title>提额授信</title>
    <style>
        .left_mume .xuanzhong>a{
            background: #f49110;
            color: #fff;
            display: block;
        }
        .left_mume .xuanzhong>a:hover{
            background: #f49110;
            color: #fff;
            display: block;
        }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="all_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 left_mume">
                    <ul>
                        <li>
                            <a href="">拒绝授信</a>
                        </li>
                        <li>
                            <a href="">初始授信</a>
                        </li>
                        <li class="xuanzhong">
                            <a href="">提额授信</a>
                        </li>
                        <li>
                            <a href="">授信提额综合分值</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-10">
                    <div class="top_text">
                        <div class="top_cont">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-4 no_sx">
                                        提额授信
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tow_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>条件</th>
                                    <th>权重</th>
                                    <th>状态</th>
                                    <th class="operation">操作</th>
                                </tr>
                                </thead>
                                <tbody name="table_tt" id="table_tt">
                                <tr>
                                    <td rowspan="4" style="border-bottom: 1px solid #ededed" id="countPayScore_name"></td>
                                    <td id="sencondOverdueWeight_name"></td>
                                    <td id="sencondOverdueWeight_max"></td>
                                    <td id="sencondOverdueWeight"></td>

                                    <td id="sencondOverdueWeight_code" hidden></td>
                                    <td id="sencondOverdueWeight_typeIds" hidden></td>

                                    <td>
                                        <a href="#" class="fq_repayment edit_btn" id="sencondOverdueWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="firstOverdueWeight_name"></td>
                                    <td id="firstOverdueWeight_max"></td>
                                    <td id="firstOverdueWeight"></td>

                                    <td id="firstOverdueWeight_code" hidden></td>
                                    <td id="firstOverdueWeight_typeIds" hidden></td>

                                </tr>

                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="monthPayWeight_name"></td>
                                    <td id="monthPayWeight_max"></td>
                                    <td id="monthPayWeight"></td>

                                    <td id="monthPayWeight_code" hidden></td>
                                    <td id="monthPayWeight_typeIds" hidden></td>
                                </tr>


                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="firstPayWeight_name"></td>
                                    <td id="firstPayWeight_max"></td>
                                    <td id="firstPayWeight"></td>

                                    <td id="firstPayWeight_code" hidden ></td>
                                    <td id="firstPayWeight_typeIds" hidden></td>
                                </tr>


                                <tr style="border-bottom: 1px solid #ededed">
                                    <td id="allPayScore_name"></td>
                                    <td id="oneAllPayScore_name"></td>
                                    <td id="oneAllPayScore_max"></td>
                                    <td id="oneAllPayScore"></td>

                                    <td id="oneAllPayScore_code" hidden ></td>
                                    <td id="oneAllPayScore_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="one_repayment edit_btn" id="oneAllPayScore_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed" id="minPayScore_name"></td>
                                    <td id="secondPayWeight_name"></td>
                                    <td id="secondPayWeight_max"></td>
                                    <td id="secondPayWeight"></td>

                                    <td id="secondPayWeight_code" hidden></td>
                                    <td id="secondPayWeight_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="zd_repayment edit_btn"  id="secondPayWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="threePayWeight_name"></td>
                                    <td id="threePayWeight_max"></td>
                                    <td id="threePayWeight"></td>


                                    <td id="threePayWeight_code" hidden></td>
                                    <td id="threePayWeight_typeIds" hidden></td>

                                </tr>

                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="fourPayWeight_name"></td>
                                    <td id="fourPayWeight_max"></td>
                                    <td id="fourPayWeight"></td>

                                    <td id="fourPayWeight_code" hidden></td>
                                    <td id="fourPayWeight_typeIds" hidden></td>
                                </tr>

                                <tr>
                                    <td rowspan="5" style="border-bottom: 1px solid #ededed" id="sspfScore_name"></td>
                                    <td id="firstSspfWeight_name"></td>
                                    <td id="firstSspfWeight_max"></td>
                                    <td id="firstSspfWeight"></td>

                                    <td id="firstSspfWeight_code" hidden></td>
                                    <td id="firstSspfWeight_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="social_security edit_btn" id="firstSspfWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="secondSspfWeight_name"></td>
                                    <td id="secondSspfWeight_max"></td>
                                    <td id="secondSspfWeight"></td>

                                    <td id="secondSspfWeight_code" hidden></td>
                                    <td id="secondSspfWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="threeSspfWeight_name"></td>
                                    <td id="threeSspfWeight_max"></td>
                                    <td id="threeSspfWeight"></td>

                                    <td id="threeSspfWeight_code" hidden></td>
                                    <td id="threeSspfWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="fourSspfWeight_name"></td>
                                    <td id="fourSspfWeight_max"></td>
                                    <td id="fourSspfWeight"></td>

                                    <td id="fourSspfWeight_code" hidden></td>
                                    <td id="fourSspfWeight_typeIds" hidden></td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="fiveSspfWeight_name"></td>
                                    <td id="fiveSspfWeight_max"></td>
                                    <td id="fiveSspfWeight"></td>

                                    <td id="fiveSspfWeight_code" hidden></td>
                                    <td id="fiveSspfWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed" id="tbAddressScore_name"></td>
                                    <td id="threeAddressWeight_name"></td>
                                    <td id="threeAddressWeight_max"></td>
                                    <td id="threeAddressWeight"></td>

                                    <td id="threeAddressWeight_code" hidden></td>
                                    <td id="threeAddressWeight_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="taobao edit_btn"  id="threeAddressWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="firstAddressWeight_name"></td>
                                    <td id="firstAddressWeight_max"></td>
                                    <td id="firstAddressWeight"></td>

                                    <td id="firstAddressWeight_code" hidden></td>
                                    <td id="firstAddressWeight_typeIds" hidden></td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="secondAddressWeight_name"></td>
                                    <td id="secondAddressWeight_max"></td>
                                    <td id="secondAddressWeight"></td>


                                    <td id="secondAddressWeight_code" hidden></td>
                                    <td id="secondAddressWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed" id="educationScore_name"></td>
                                    <td id="firstEduWeight_name"></td>
                                    <td id="firstEduWeight_max"></td>
                                    <td id="firstEduWeight"></td>

                                    <td id="firstEduWeight_code" hidden></td>
                                    <td id="firstEduWeight_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="xuexing edit_btn" id="firstEduWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="secondEduWeight_name"></td>
                                    <td id="secondEduWeight_max"></td>
                                    <td id="secondEduWeight"></td>

                                    <td id="secondEduWeight_code" hidden></td>
                                    <td id="secondEduWeight_typeIds" hidden></td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td style="display: none;width: 0"></td>
                                    <td id="otherEduWeight_name"></td>
                                    <td id="otherEduWeight_max"></td>
                                    <td id="otherEduWeight"></td>

                                    <td id="otherEduWeight_code" hidden></td>
                                    <td id="otherEduWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td rowspan="7" id="zhimaScore_name"></td>
                                    <td id="sevenZhimaWeight_name"></td>
                                    <td id="sevenZhimaWeight_max"></td>
                                    <td id="sevenZhimaWeight"></td>


                                    <td id="sevenZhimaWeight_code" hidden></td>
                                    <td id="sevenZhimaWeight_typeIds" hidden></td>
                                    <td>
                                        <a href="#" class="didi edit_btn" id="sevenZhimaWeight_typeId" typeid="">
                                            <img src="${basePath!}/img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="firstZhimaWeight_name"></td>
                                    <td id="firstZhimaWeight_max"></td>
                                    <td id="firstZhimaWeight"></td>

                                    <td id="firstZhimaWeight_code" hidden></td>
                                    <td id="firstZhimaWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="secondZhimaWeight_name"></td>
                                    <td id="secondZhimaWeight_max"></td>
                                    <td id="secondZhimaWeight"></td>

                                    <td id="secondZhimaWeight_code" hidden></td>
                                    <td id="secondZhimaWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="threeZhimaWeight_name"></td>
                                    <td id="threeZhimaWeight_max"></td>
                                    <td id="threeZhimaWeight"></td>

                                    <td id="threeZhimaWeight_code" hidden></td>
                                    <td id="threeZhimaWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="fourZhimaWeight_name"></td>
                                    <td id="fourZhimaWeight_max"></td>
                                    <td id="fourZhimaWeight"></td>

                                    <td id="fourZhimaWeight_code" hidden></td>
                                    <td id="fourZhimaWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="fiveZhimaWeight_name"></td>
                                    <td id="fiveZhimaWeight_max"></td>
                                    <td id="fiveZhimaWeight"></td>

                                    <td id="fiveZhimaWeight_code" hidden></td>
                                    <td id="fiveZhimaWeight_typeIds" hidden></td>
                                </tr>
                                <tr>
                                    <td style="display: none;width: 0"></td>
                                    <td id="sixZhimaWeight_name"></td>
                                    <td id="sixZhimaWeight_max"></td>
                                    <td id="sixZhimaWeight"></td>

                                    <td id="sixZhimaWeight_code" hidden></td>
                                    <td id="sixZhimaWeight_typeIds" hidden></td>
                                </tr>
                                </tbody>
                            </table>
                            <!--编辑分期还款-->
                            <div class="fq_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p id="countPayScore_names"></p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="sencondOverdueWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="hidden" id="varOne" value="">
                                                                <input type="hidden" id="varTwo" value="">
                                                                <input type="hidden" id="varThree" value="">

                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="sencondOverdueWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="sencondOverdueWeight_radio" value="1" id="sencondOverdueWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="sencondOverdueWeight_radio" value="0" id="sencondOverdueWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="firstOverdueWeight_names">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstOverdueWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstOverdueWeight_radio"  value="1" id="firstOverdueWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstOverdueWeight_radio" value="0" id="firstOverdueWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="monthPayWeight_names">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="monthPayWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="monthPayWeight_radio"  value="1" id="monthPayWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="monthPayWeight_radio" value="0" id="monthPayWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="firstPayWeight_names">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstPayWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstPayWeight_radio"  value="1" id="firstPayWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstPayWeight_radio" value="0" id="firstPayWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑一次性还款-->
                            <div class="one_pay_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑一次性还款</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    条件
                                                </div>
                                                <div class="col-sm-4" id="oneAllPayScore_names">

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    评分权重
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="va" id="oneAllPayScore_va">
                                                        <option value="0">0</option>
                                                        <option value="0.1">0.1</option>
                                                        <option value="0.2">0.2</option>
                                                        <option value="0.3">0.3</option>
                                                        <option value="0.4">0.4</option>
                                                        <option value="0.5">0.5</option>
                                                        <option value="0.6">0.6</option>
                                                        <option value="0.7">0.7</option>
                                                        <option value="0.8">0.8</option>
                                                        <option value="0.9">0.9</option>
                                                        <option value="1">1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    状态
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="radio" name="oneAllPayScore_radio"  value="1" id="oneAllPayScore_status">
                                                    &nbsp
                                                    <label for="">启用</label>
                                                    &nbsp
                                                    <input type="radio" name="oneAllPayScore_radio" value="0" id="oneAllPayScore_statuss">
                                                    &nbsp
                                                    <label for="">禁用</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑最低额度还款-->
                            <div class="zd_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑最低额度还款</p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="secondPayWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="secondPayWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="secondPayWeight_radio"  value="1" id="secondPayWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="secondPayWeight_radio" value="0" id="secondPayWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="threePayWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="threePayWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="threePayWeight_radio"  value="1" id="threePayWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="threePayWeight_radio" value="0" id="threePayWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="fourPayWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="fourPayWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fourPayWeight_radio"  value="1" id="fourPayWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fourPayWeight_radio" value="0" id="fourPayWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑社保授权-->
                            <div class="shebao_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p id="sspfScore_names"></p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="firstSspfWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstSspfWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstSspfWeigh_radio"  value="1" id="firstSspfWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstSspfWeigh_radio" value="0" id="firstSspfWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="secondSspfWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="secondSspfWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="secondSspfWeight_radio"  value="1" id="secondSspfWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="secondSspfWeight_radio" value="0" id="secondSspfWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="threeSspfWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="threeSspfWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="threeSspfWeight_radio"  value="1" id="threeSspfWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="threeSspfWeight_radio" value="0" id="threeSspfWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="fourSspfWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="fourSspfWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fourSspfWeight_radio"  value="1" id="fourSspfWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fourSspfWeight_radio" value="0" id="fourSspfWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="fiveSspfWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="fiveSspfWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fiveSspfWeight_radio"  value="1" id="fiveSspfWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fiveSspfWeight_radio" value="0" id="fiveSspfWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--编辑淘宝+支付宝授权-->
                            <div class="taobao_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p id="tbAddressScore_names"></p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="threeAddressWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="threeAddressWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="threeAddressWeight_radio"  value="1" id="threeAddressWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="threeAddressWeight_radio" value="0" id="threeAddressWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="firstAddressWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstAddressWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstAddressWeight_radio"  value="1" id="firstAddressWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstAddressWeight_radio" value="0" id="firstAddressWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="secondAddressWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="secondAddressWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="secondAddressWeight_radio"  value="1" id="secondAddressWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="secondAddressWeight_radio" value="0" id="secondAddressWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑学信网授权-->
                            <div class="xuexing_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p id="educationScore_names"></p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="firstEduWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstEduWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstEduWeight_radio"  value="1" id="firstEduWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstEduWeight_radio" value="0" id="firstEduWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="secondEduWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="secondEduWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="secondEduWeight_radio"  value="1" id="secondEduWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="secondEduWeight_radio" value="0" id="secondEduWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3" id="otherEduWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="otherEduWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="otherEduWeight_radio"  value="1" id="otherEduWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="otherEduWeight_radio" value="0" id="otherEduWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑滴滴授权-->
                            <div class="didi_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p id="zhimaScore_names"></p>
                                        <b> <a href="#">×</a></b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="sevenZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="sevenZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="sevenZhimaWeight_radio"  value="1" id="sevenZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="sevenZhimaWeight_radio" value="0" id="sevenZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="firstZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="firstZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="firstZhimaWeight_radio"  value="1" id="firstZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="firstZhimaWeight_radio" value="0" id="firstZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="secondZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="secondZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="secondZhimaWeight_radio"  value="1" id="secondZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="secondZhimaWeight_radio" value="0" id="secondZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="threeZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="threeZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="threeZhimaWeight_radio"  value="1" id="threeZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="threeZhimaWeight_radio" value="0" id="threeZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="fourZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="fourZhimaWeight" id="fourZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fourZhimaWeight_radio"  value="1" id="fourZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fourZhimaWeight_radio" value="0" id="fourZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="fiveZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="fiveZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fiveZhimaWeight_radio"  value="1" id="fiveZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fiveZhimaWeight_radio" value="0" id="fiveZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4" id="sixZhimaWeight_names">

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="va" id="sixZhimaWeight_va">
                                                                    <option value="0">0</option>
                                                                    <option value="0.1">0.1</option>
                                                                    <option value="0.2">0.2</option>
                                                                    <option value="0.3">0.3</option>
                                                                    <option value="0.4">0.4</option>
                                                                    <option value="0.5">0.5</option>
                                                                    <option value="0.6">0.6</option>
                                                                    <option value="0.7">0.7</option>
                                                                    <option value="0.8">0.8</option>
                                                                    <option value="0.9">0.9</option>
                                                                    <option value="1">1</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="sixZhimaWeight_radio"  value="1" id="sixZhimaWeight_status">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="sixZhimaWeight_radio" value="0" id="sixZhimaWeight_statuss">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script>
    $(".fq_repayment").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }
                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });
                }
            }
        });
        $(".fq_modal").css("display","block")
    });
    $(".one_repayment").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }

                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });

                }
            }
        });
        $(".one_pay_modal").css("display","block")
    });
    $(".zd_repayment").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }

                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });

                }
            }
        });
        $(".zd_modal").css("display","block")
    });
    $(".social_security").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }
                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });
                }
            }
        });
        $(".shebao_modal").css("display","block")
    });

    $(".taobao").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }
                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });
                }
            }
        });
        $(".taobao_modal").css("display","block")
    });

    $(".xuexing").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }
                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }
                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });
                }
            }
        });
        $(".xuexing_modal").css("display","block")
    });
    $(".didi").click(function () {
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyListById?typeId="+($(this).attr("typeid")),
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d) {
                console.log(d);
                for ( var i=0; i < d.data.length; i++) {
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_va").val(d.data[i].value);

                    var tempVa = $("#" + d.data[i].code + "_max").text();
                    if($("#" + d.data[i].code + "_max").attr("dir")==2)
                    {
                        $("#" + d.data[i].code + "_va").val(tempVa);
                    }
                    else
                    {
                        $("#" + d.data[i].code + "_va").val(d.data[i].value);
                    }

                    var dData = d.data[i].code;
                    $("#"+ dData+"_va").change(function(e)
                    {
                        var tempVV = $(this).attr("id");
                        var aaa= tempVV.substring(0,tempVV.length-3);
                        $("#"+ aaa+"_max").html($("#"+tempVV).val());
                        $("#"+ aaa+"_max").attr("dir",2);
                    });

                    if(d.data[i].status=="1"){
                        $("#" + d.data[i].code + "_status").attr("checked","checked");
                    }else {
                        $("#" + d.data[i].code + "_statuss").attr("checked","checked");
                    }

                    var ttt = dData+"_radio";
                    $("input[name='"+ttt+"']").change(function(e)
                    {
                        var tempStr = $(this).attr("id").substring(0,$(this).attr("name").length-6);
                        if($(this).val()==1)
                        {
                            $("#"+tempStr).html("启用");
                        }
                        else
                        {
                            $("#"+tempStr).html("关闭");
                        }

                    });
                }
            }
        });
        $(".didi_modal").css("display","block")
    });

    f();
    //授信提额查询
    function f(){
        $.ajax({
            url:"${basePath!}/opera/creditCustom/getRmBaseCreditUpmoneyList",
            type:'POST', //GET
            data:$("#searchForm").serialize(),
            cache:false,
            timeout:30000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(d){
                console.log(d)
                for ( var i=0; i < d.data.length; i++) {

                    if(d.data[i].status==1){
                        $("#" + d.data[i].code).html("启用");
                    }else {
                        $("#" + d.data[i].code).html("关闭");
                    }

                    $("#" + d.data[i].code + "_max").html(d.data[i].value);
                    $("#" + d.data[i].code + "_name").html(d.data[i].name);
                    $("#" + d.data[i].code + "_names").html(d.data[i].name);
                    $("#" + d.data[i].code + "_typeId").attr("typeid",d.data[i].typeId);
                    $("#" + d.data[i].code + "_max").attr("dir",1);
                    $("#" + d.data[i].code + "_code").html(d.data[i].code);
                    $("#" + d.data[i].code + "_typeIds").html(d.data[i].typeId);
                }
            }
        });
    }
    $("#new_oks").click(function () {
        var tempTr = $("#table_tt").children("tr");
        var array = new Array();
        for (var i = 0; i <tempTr.length ; i++) {
            var data = {};
            var tempTd = tempTr.eq(i).find("td");
            var td1= tempTd.eq(1);
            var td2= tempTd.eq(2);
            var td3= tempTd.eq(3);
            var td4= tempTd.eq(4);
            var td5= tempTd.eq(5);
            if (td1.attr("id") == undefined ) {

            }else {
                td1=eval(td1);
                var name =td1.text();
                data.name = name;

                td2=eval(td2);
                var value = td2.text();
                data.value = value;

                td4=eval(td4);
                var code = td4.text();
                data.code = code;

                td5=eval(td5);
                var typeId = td5.text();
                data.typeId = typeId;

                td3=eval(td3);
                var status = td3.text();
                if (status=="启用"){
                    status = "1";
                } else {
                    status = "0";
                }
                data.status = status;
                array.push(data);
            }
        }
        $.ajax({
            url:'${basePath!}/opera/creditCustom/saveRmBaseCreditUpmoney',
            type : 'post', //数据发送方式
            dataType : 'json', //接受数据格式 (这里有很多,常用的有html,xml,js,json)
            contentType:"application/json",
            data : JSON.stringify(array),
            error : function() { //失败

            },
            success : function(d) { //成功
                if(d.code ==0){
                    parent.messageModel(d.msg);
                    f();
                }else {
                    parent.messageModel(d.msg);
                    f();
                }
            }
        });
    });
    $("#no_oks").click(function () {
        window.location.reload();
    })

    $(".edit").click(function () {
        $(".confirms").css("display","inline");
        $(".cancel").css("display","inline");
        $(".edit").css("display","none");
        $(".edit_btn").css("display","block");
        $('.operation').css("color","#676a6c");
    });
    $(".confirms").click(function () {
        $(".edit").css("display","inline");
        $(".confirms").css("display","none");
        $(".cancel").css("display","none");
        $(".edit_btn").css("display","none");
        $('.operation').css("color","transparent");
    });
    $(".cancel").click(function () {
        $(".edit").css("display","inline");
        $(".confirms").css("display","none");
        $(".cancel").css("display","none");
        $(".edit_btn").css("display","none");
        $('.operation').css("color","transparent");
    });


    $(".modal_head b").click(function () {
        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");
    });
    $(".sx_ok").click(function () {

        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");
        $("#varOne").val();

    });
    $(".sx_sore").click(function () {
        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");

        window.location.reload();
    })

</script>
</body>
</html>