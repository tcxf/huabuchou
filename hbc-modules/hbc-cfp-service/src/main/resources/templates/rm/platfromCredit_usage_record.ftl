<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <title>授信使用记录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <#include "common/common.ftl">
    <style>
        .sx_date_form{
            margin-top: 10px;
            margin-left: 20px;
        }
        .sx_date_form .col-sm-4{
            height: 60px;
            width: 30%;
        }
        .sx_date_form .col-sm-4 input{
            width: 120px;
            height: 30px;
        }
        .jiekou input{
            width: 120px;
            padding-left: 10px;
            height: 30px;
        }
        .sx_date_form .col-sm-2{
            text-align: right;
        }
        .form-inline .col-sm-3{
            width: 20%;
        }
        .daochu>.col-sm-12{
            text-align: right;
            padding-right: 40px;
        }
        .daochu .col-sm-12>a{
            text-decoration: none;
            color: #f49110;
            padding: 8px 10px;
            border: 1px solid #f49110;
            border-radius: 3px;
        }
        .daochu .col-sm-12>a>img{
            width: 16px;
        }
        .bottom_title{
            width: 90%;
            margin-left: 2%;
            text-align: right;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        授信使用记录
                    </div>
                </div>
            </div>
        </div>
        <div class="sx_date_form">
            <form id="searchForm" class="form-inline">
                <div class="row daochu">
                    <div class="col-sm-12">
                        <a href="#" id="xz">
                            <img src="${basePath}/img/u4205.png" alt="">
                            <span>导出</span>
                        </a>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="">授信时间</label>
                            &nbsp&nbsp
                            <input placeholder="开始日期" class="form-control layer-date" name="startDate" id="startDate">
                            &nbsp
                            ～
                            &nbsp
                            <input placeholder="结束日期" class="form-control layer-date" name="endDate" id="endDate">
                        </div>
                        <div class="col-sm-2 jiekou">
                            <label for="">用户名称</label>
                            &nbsp&nbsp
                            <input type="text" name="realName" placeholder="请输入用户名称">
                        </div>
                        <div class="col-sm-2 jiekou">
                            <label for="">手机号码</label>
                            &nbsp&nbsp
                            <input type="text" name="mobile" placeholder="请输入手机号码">
                        </div>
                        <div class="col-sm-3">
                            <label for="">用户类型</label>
                            &nbsp&nbsp
                            <select name="userAttribute" id="userAttribute">
                                <option value="">所有用户类型</option>
                                <option value="1">会员</option>
                                <option value="2">商户</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                        </div>
                    </div>
                </div>
            </form>
            <div class="date_form">
                <table class="table table-bordered">
            </div>
        </div>
    </div>

<script>
    $(document).ready(function () {
        $("select").select();
        laydate({
            elem: '#startDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 00:00:00' //日期格式
        });
        laydate({
            elem: '#endDate',
            event: 'click', //触发事件
            format: 'YYYY-MM-DD 23:59:59' //日期格式
        })
        $("#xz").click(function(){
            if(confirm("该下载报表只支持本月下载！如需要下载其它数据请使用时间搜索!")){
                $("#searchForm").attr("action","${basePath!}/platfrom/FKCreditmanagementController/tradingPoi");
                $("#searchForm").submit();
            }
        });

        var $pager = $(".date_form").pager({
            url:"${basePath!}/platfrom/FKCreditmanagementController/queryCreditRecord",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover table-bordered">'+
                '<thead>'+
                '<tr>'+
                '<th>用户名称</th>'+
                '<th>手机号码</th>'+
                '<th>用户类型</th>'+
                '<th>授信时间</th>'+
                '<th>授信总额</th>'+
                '<th>已使用额度</th>'+
                '<th>剩余授信额度</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{realName}</td>'+
                '<td>{mobile}</td>'+
                '<td>{userAttribute}</td>'+
                '<td>{refCreateDate}</td>'+
                '<td>{maxMoney}元</td>'+
                '<td>{sumMoney}元</td>'+
                '<td>{balance}元</td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                }
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });
                // check 全部选中
                $('.checks').on('ifChecked', function(event){
                    $("input[id^='check-id']").iCheck('check');
                });

                // check 全部移除
                $('.checks').on('ifUnchecked', function(event){
                    $("input[id^='check-id']").iCheck('uncheck');
                });
            }
        });

    });

</script>
</body>

</html>
