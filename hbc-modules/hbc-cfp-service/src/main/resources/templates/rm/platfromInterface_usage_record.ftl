<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <title>接口使用记录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
     <#include "common/common.ftl">
    <style>
        .sx_date_form{
            margin-top: 10px;
            margin-left: 20px;
        }
        .sx_date_form .col-sm-4{
            height: 60px;
        }
        .sx_date_form .col-sm-4 input{
            width: 120px;
            height: 30px;
        }
        .jiekou input{
            width: 200px;
            padding-left: 10px;
            height: 30px;
        }
        .sx_date_form .col-sm-12{
            text-align: right;
            margin-bottom: 20px;
        }
        .sx_date_form .col-sm-12>a{
            text-decoration: none;
            color: #f49110;
            padding: 8px 10px;
            border: 1px solid #f49110;
            border-radius: 3px;
        }
        .sx_date_form .col-sm-12>a>img{
            width: 16px;
        }
        .bottom_title{
            width: 90%;
            margin-left: 2%;
            text-align: right;
        }
        .text-muted {
            font-size: 16px;
            color: transparent!important;
        }
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        接口使用记录
                    </div>
                </div>
            </div>
        </div>
        <div class="sx_date_form">
            <form id="searchForm" class="form-inline">
                <div class="container-fluid">
                    <div class="row">

                    <#--<div class="col-sm-3 jiekou">-->
                    <#--<label for="">接口名称</label>-->
                    <#--&nbsp&nbsp-->
                    <#--<input type="text" name="typeName" placeholder="请输入接口名称">-->
                    <#--</div>-->
                    <#--<div class="col-sm-1">-->
                    <#--<input type="button" id="search" class="btn btn-warning" value="搜索" />-->
                    <#--</div>-->
                        <div class="col-sm-12">
                            <a href="#" id="xz">
                                <img src="${basePath}/img/u4205.png" alt="">
                                <span>导出</span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="date_form">
                <table class="table table-bordered">
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {

        $("#xz").click(function(){
            $("#searchForm").attr("action","${basePath!}/platfrom/interface/tradingPoi");
            $("#searchForm").submit();
        });

        var $pager = $(".date_form").pager({
            url:"${basePath!}/platfrom/interface/Queryinterface",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover table-bordered">'+
                '<thead>'+
                '<tr>'+
                '<th>公司名称</th>'+
                '<th>接口名称</th>'+
                '<th>运营商名称</th>'+
                '<th>使用次数</th>'+
                '<th>使用总额</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{corporatename}</td>'+
                '<td>{typeName}</td>'+
                '<td>{operatorType}</td>'+
                '<td>{counts}次</td>'+
                '<td>{money}元</td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="10"><center>暂无数据</center></tr>'
            },
            callbak: function(result){
                var list = result.data.records;
                for(var i = 0 ; i < list.length ; i++){
                }
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%' // optional
                });
                // check 全部选中
                $('.checks').on('ifChecked', function(event){
                    $("input[id^='check-id']").iCheck('check');
                });

                // check 全部移除
                $('.checks').on('ifUnchecked', function(event){
                    $("input[id^='check-id']").iCheck('uncheck');
                });
            }
        });

    });

</script>
</body>

</html>