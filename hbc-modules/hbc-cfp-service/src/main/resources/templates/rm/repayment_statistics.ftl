<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <style>
        .sx_date_form{
            margin-top: 10px;
            margin-left: 10px;
        }
       .date_form{
           margin-top: 10px;
           margin-left: 10px;
       }
        .sx_date_form .col-sm-3 input{
            width: 120px;
            height: 30px;
        }

        .sx_date_form .col-sm-1{
            text-align: right;
        }
        .sx_date_form .col-sm-1>div{
            text-decoration: none;
            color: #f49110;
            width: 60px;
            height: 30px;
            line-height: 30px;
            border: 1px solid #f49110;
            border-radius: 3px;
            text-align: center;
            display: inline-block;
        }
        .sx_date_form .col-sm-1 img{
            width: 16px;
        }

    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="top_text">
        <div class="top_cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 no_sx">
                        已还款统计
                    </div>
                </div>
            </div>
        </div>
        <div class="sx_date_form">
            <form id="searchForm" class="form-inline">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <input placeholder="开始日期" class="form-control layer-date" id="startTime" name="startTime">
                            &nbsp
                            ～
                            &nbsp
                            <input placeholder="结束日期" class="form-control layer-date" id="endTime" name="endTime">
                        </div>
                        <div class="col-sm-1">
                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                        </div>
                        <div class="col-sm-1 col-sm-offset-7">
                            <div id="exportExcle">
                                <img src="${basePath!}/img/u4205.png" alt="">
                                <span>导出</span>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
            <div class="date_form" id="data">

            </div>
        </div>
    </div>

</div>

<!-- 全局js -->
<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>

<!-- 自定义js -->
<script src="${basePath!}/js/content.js?v=1.0.0"></script>
<script type="text/javascript" src="${basePath!}/js/pager.js"></script>
<script type="text/javascript" src="${basePath!}/js/common.js"></script>


<script>
    $(document).ready(function () {

        //下载报表
        $("#exportExcle").click(function(){
            $("#searchForm").attr("action","${basePath!}/platfrom/riskControl/ExportExcle");
            $("#searchForm").submit();
        });

        //日期范围限制
        var start = {
            elem: '#startTime',
            format: 'YYYY-MM-DD 00:00:00',
            max: '2099-06-16 23:59:59', //最大日期
            istime: true,
            istoday: false,
            choose: function (datas) {
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };
        var end = {
            elem: '#endTime',
            format: 'YYYY-MM-DD 23:59:59',
            max: '2099-06-16 23:59:59',
            istime: true,
            istoday: false,
            choose: function (datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };
        laydate(start);
        laydate(end);


        var $pager = $("#data").pager({
            url:"${basePath!}/platfrom/riskControl/prepayStatistics",
            formId:"searchForm",
            pagerPages: 3,
            template:{
                header:'<div class="fixed-table-container form-group pager-content">'+
                '<table class="table table-hover table-bordered">'+
                '<thead>'+
                '<tr>'+
                '<th>还款日期</th>'+
                '<th>应还人数</th>'+
                '<th>实还人数</th>'+
                '<th>按时还款率</th>'+
                '<th>应还金额 (元)</th>' +
                '<th>实际回款金额 (元)</th>'+
                '<th>金额回款率</th>'+
                '<th>中期异常人数</th>'+
                '</tr>'+
                '</thead><tbody>',
                body:'<tr>'+
                '<td>{createTime}</td>'+
                '<td>{shouldPayPeople}</td>'+
                '<td>{alreadyPayPeople}</td>'+
                '<td>{payRate}</td>'+
                '<td>{shouldPayMoney}</td>'+
                '<td>{actualRepayMoney}</td>'+
                '<td>{repayRate}</td>'+
                '<td>{unusualNumber}</td>'+
                '</tr>',
                footer:'</tbody></table>',
                noData:'<tr><td colspan="12"><center>暂无数据</center></tr>'
            },
            callbak: function(result){

            }
        });

  /*  $("#search").click(function(){
        goToPage(1);
    });*/

    });

</script>
</body>

</html>
