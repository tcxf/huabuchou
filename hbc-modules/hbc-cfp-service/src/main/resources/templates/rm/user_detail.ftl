<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="${basePath!}/favicon.ico">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="${basePath!}/css/animate.css" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">

    <style>
        .search_sx{
            width: 98%;
            height: 60px;
            line-height: 60px;
            margin-left: 2%;
        }
        .form-inline{
            height: 60px;
            line-height: 60px;
        }
        #searchForm  .col-sm-2 input{
            width: 100%;
        }
        #searchForm .col-sm-7 select{
            width: 140px;
            height: 30px;
        }
        #searchForm .col-sm-3>input{
            width: 130px;
            height: 30px;
        }
        .sx_table{
            margin-left: 20px;
        }
       .sx_table .table th{

            background: #f5f5f5;
        }

        .table td img{
            width: 20px;
            height: 20px;
        }
        .table td a{
            color: #1296db;
        }
        .sx_modal{
            background: rgba(0,0,0,.5);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            display: none;
            -webkit-overflow-scrolling: touch;
            outline: 0;
        }
        .modal_content{
            width: 40%;
            height: 500px;
            background: #fff;
            margin-left: 30%;
            margin-top: 5%;
            border-radius: 8px;
            position: relative;
        }
        .modal_head{
            width: 100%;
            height: 40px;
            line-height: 40px;
            background: #f5f5f5;
            padding-left: 10px;
            border-top-right-radius: 8px;
            border-top-left-radius: 8px;
        }
        .modal_head p{
            float: left;
        }
        .modal_head b{
            float: right;
            padding-right: 20px;
            contain: paint;
            font-size: 20px;
        }
        .modal_body{
            margin-top: 20px;
            margin-left: 10px;
        }
        .modal_body ul{
            padding: 0;
            margin: 0;
        }
        .modal_body>.tab-container>ul li{
            list-style: none;
            width: 25%;
            height: 40px;
            line-height: 40px;
            float: left;
            border: 1px solid #ededed;
            text-align: center;
        }
        .modal_body>.tab-container>ul .active{
            background: #f49110;

        }
        .modal_body>.tab-container>ul .active>a{
            color: #fff;
        }
        .modal_body li>a{
            color: #000;
            display: block;
        }
        .tab-content{
            clear: both;
        }
        .tow_tow{
            clear: both;
        }
        .tow_tow ul{
            padding: 0;
            margin: 0;
        }
        .tow_tow ul li{
            list-style: none;
            width: 25%;
            height: 60px;
            line-height: 60px;
            float: left;
        }
        .tow_tow ul li>a{
            font-size: 14px;
            font-weight: 600;
        }
        .tow_tow ul .active>a{
            color: #f49110;
        }
        .name_time>.container-fluid{
            padding-left: 0;
        }
        .name_time .col-sm-6:last-child{
            text-align: right;
        }
        .sx_messge{
            margin-top: 20px;
            width: 100%;
            height: 260px;
            overflow: auto;
        }
        .sx_messge>.table th{
            text-align: left;!important;
            width: 30%;
        }
        .sx_messge>.table td{
            text-align: left;!important;
            height: 20px;
            line-height: 20px;
        }
        .modal_foot{
            position: absolute;
            bottom: 0;
            width: 100%;
        }
        .modal_foot>.sx_zf{
            width: 100%;
        }
        .modal_foot>.sx_zf>ul{
            padding: 0;
            margin: 0;
        }
        .modal_foot>.sx_zf li{
            list-style: none;
            height: 40px;
            line-height: 40px;
            float: left;
        }
        .modal_foot>.sx_zf .sx_sore{
            width: 70%!important;
            margin-right: 0!important;
            border-radius: 0!important;
        }
        .modal_foot>.sx_zf .sx_ok{
            width: 30%!important;
            margin-right: 0!important;
            border-radius: 0!important;
        }
        .modal_foot>.sx_zf .sx_ok>a{
            color: #fff;
        }
        .name_times{
            height: 60px;
            line-height: 60px;
        }
        .sx_zt .row{
            height: 40px;
            line-height: 40px;
        }
        .jujue{
            color: #f49110;
        }
        .no_pass{
            color: #000;
        }
        .hkzt{
            width: 100%;
            height: 200px;
            overflow: auto;
        }
        .wh{
            color: #f49110;
        }
    </style>
</head>

<body class="fixed-sidebar full-height-layout gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="top_text">
            <div class="top_cont">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4 no_sx">
                            授信用户明细
                        </div>
                    </div>
                </div>
            </div>
            <div class="sx_table">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-e-margins">

                            <form id="searchForm" class="form-inline">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input placeholder="开始日期" class="form-control layer-date" id="startTime" name="startTime">
                                            ～
                                            <input placeholder="结束日期" class="form-control layer-date" id="endTime" name="endTime">
                                        </div>

                                        <div class="col-sm-2" style="text-align: center">
                                            <input id="mobile" type="text" class="form-control" name="mobile" maxlength="30" placeholder="请输入手机号">
                                        </div>

                                        <div class="col-sm-2" style="text-align: center">
                                            <input id="realName" type="text" class="form-control" name="realName" maxlength="30" placeholder="请输入姓名">
                                        </div>

                                        <div class="col-sm-2" style="text-align: center">
                                            <select name="authStatus" id="authStatus">
                                                <option value="">所有授信状态</option>
                                                <option value="1">已授信</option>
                                                <option value="3">被拒绝</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-2" style="text-align: left">
                                            <select name="userAttribute" id="userAttribute">
                                                <option value="">用户类型</option>
                                                <option value="">所有</option>
                                                <option value="1">会员用户</option>
                                                <option value="2">商户用户</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-1">
                                            <input type="button" id="search" class="btn btn-warning" value="搜索" />
                                        </div>

                                    </div>
                                </div>
                            </form>

                            <div style="clear:both;"></div>
                            <div class="date_form" id="data">

                            </div>

                            <div class="sx_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>详情</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="tab-container">
                                            <ul>
                                                <li class="active"><a data-toggle="tab" href="#tab-1"> 授信评分</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-2">被拒绝情况</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-3">还款状态</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab-1" class="tab-pane active">
                                                    <div class="tab-container">
                                                        <div class="tow_tow">
                                                            <ul>
                                                                <li class="active"><a data-toggle="tab" href="#tab-4">初始授信评分</a>
                                                                </li>
                                                                <li class=""><a data-toggle="tab" href="#tab-5">授信提额评分</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div class="name_time">
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                姓名: <span id="realName1">张三</span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                授信时间: <span id="sxTime1"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <#--初始授信情况-->
                                                                <div id="tab-4" class="tab-pane active">
                                                                    <div class="sx_messge">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>信息</th>
                                                                                <th>条件</th>
                                                                                <th>评分</th>
                                                                            </tr>
                                                                            </thead>

                                                                            <tbody id="init_credit">
                                                                            <#-- <tr>
                                                                      <td>性别</td>
                                                                      <td>男性</td>
                                                                      <td>2</td>
                                                                  </tr>-->
                                                                            </tbody>

                                                                            <tfoot>
                                                                            <tr>
                                                                                <td>初始授信得分</td>
                                                                                <td></td>
                                                                                <td id="init_grade"></td>
                                                                            </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            <#--授信提额情况-->
                                                                <div id="tab-5" class="tab-pane">
                                                                    <div class="sx_messge">
                                                                        <table class="table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>信息</th>
                                                                                <th></th>
                                                                                <th>评分</th>
                                                                            </tr>
                                                                            </thead>

                                                                            <tbody id="again_credit">
                                                                            <#--<tr>
                                                                  <td>还款情况</td>
                                                                  <td>按时还款</td>
                                                                  <td>5</td>
                                                              </tr>-->
                                                                            </tbody>
                                                                            <tfoot>
                                                                            <tr>
                                                                                <td>提额授信得分</td>
                                                                                <td></td>
                                                                                <td id="upCredit_grade"></td>
                                                                            </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="tab-2" class="tab-pane">
                                                    <div class="name_times">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    姓名: <span id="realName2">张三</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    授信时间: <span id="sxTime2"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <#--拒绝情况-->
                                                    <div class="sx_zt" id="refuse_credit">
                                                        <div class="container-fluid" id="shouxinbutton">
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    授信状态
                                                                </div>
                                                                <div class="col-sm-5 jujue"  id="isrefuse">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    被拒绝原因
                                                                </div>
                                                                <div class="col-sm-5 no_pass" id="refuse_reason">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    再次申请授信时间
                                                                </div>
                                                                <div class="col-sm-5 no_pass" id="isAgainCredit">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab-3" class="tab-pane">
                                                    <div class="name_times">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    姓名: <span id="realName3">张三</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hkzt">
                                                        <table class="table">
                                                            <thead>
                                                            <tr>
                                                            <#--   <th>账单月</th>-->
                                                                <th>还款方式</th>
                                                                <th>还款日期</th>
                                                                <th>还款金额（元）</th>
                                                                <th>违约罚息</th>
                                                                <th>还款状态</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="repay_List">
                                                            <#--<tr>
                                                                <td>分期还款</td>
                                                                <td>2018-07-20</td>
                                                                <td>300.00</td>
                                                                <td>0</td>
                                                                <td>已还款</td>
                                                            </tr>-->
                                                            <#-- <tr class="wh">
                                                                 <td>2018-05</td>
                                                                 <td>一次性还款</td>
                                                                 <td>逾期中</td>
                                                                 <td>2000.00</td>
                                                                 <td>3</td>
                                                                 <td>未还款</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>2018-04</td>
                                                                 <td>最低金额还款</td>
                                                                 <td>2018-05-20</td>
                                                                 <td>300.00</td>
                                                                 <td>0</td>
                                                                 <td>已还款</td>
                                                             </tr>-->
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    授信总得分: <span id="total_grade"></span>
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- 全局js -->
 <#--   <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>

    <script src="${basePath!}/js/plugins/jeditable/jquery.jeditable.js"></script>

    <!-- Data Tables &ndash;&gt;
    <script src="${basePath!}/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${basePath!}/js/plugins/dataTables/dataTables.bootstrap.js"></script>-->

    <!-- 全局js -->
    <script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
    <script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${basePath!}/js/plugins/layer/laydate/laydate.js"></script>
    <!-- 自定义js -->
    <script src="${basePath!}/js/content.js?v=1.0.0"></script>
    <script type="text/javascript" src="${basePath!}/js/pager.js"></script>
    <script type="text/javascript" src="${basePath!}/js/common.js"></script>

    <script>
        $(document).ready(function () {
            $("select").select()
            //日期范围限制
            var start = {
                elem: '#startTime',
                format: 'YYYY-MM-DD 00:00:00',
                max: '2099-06-16 23:59:59', //最大日期
                istime: true,
                istoday: false,
                choose: function (datas) {
                    end.min = datas; //开始日选好后，重置结束日的最小日期
                    end.start = datas //将结束日的初始值设定为开始日
                }
            };
            var end = {
                elem: '#endTime',
                format: 'YYYY-MM-DD 23:59:59',
                max: '2099-06-16 23:59:59',
                istime: true,
                istoday: false,
                choose: function (datas) {
                    start.max = datas; //结束日选好后，重置开始日的最大日期
                }
            };
            laydate(start);
            laydate(end);

            var $pager = $("#data").pager({
                url:"${basePath!}/platfrom/riskControl/PageInfo",
                formId:"searchForm",
                pagerPages: 3,
                template:{
                    header:'<div class="fixed-table-container form-group pager-content">'+
                    '<table class="table table-hover table-bordered">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>流水号</th>'+
                    '<th>姓名</th>'+
                    '<th>用户类型</th>'+
                    '<th>手机号</th>'+
                    '<th>授信状态</th>'+
                    '<th>授信评分</th>'+
                    '<th>授信额度（元）</th>'+
                    '<th>剩余额度（元）</th>'+
                    '<th>授信时间</th>'+
                    '<th>操作</th>'+
                    '</tr>'+
                    '</thead><tbody>',
                    body:'<tr>'+
                    '<td>{id}</td>'+
                    '<td>{realName}</td>'+
                    '<td>{userAttribute}</td>'+
                    '<td>{mobile}</td>'+
                    '<td>{authStatus}</td>'+
                    '<td>{grade}</td>'+
                    '<td>{maxMoney}</td>'+
                    '<td>{balance}</td>'+
                    '<td>{creditTime}</td>'+
                    '<td><a href="javascript:void(0);" data="{id}" data_name="{realName}" data_Time="{creditTime}" class="mx"> 详情 </a></td>'+
                    '</tr>',
                    footer:'</tbody></table>',
                    noData:'<tr><td colspan="12"><center>暂无数据</center></tr>'
                },
                callbak: function(result){
                    $(".mx").click(function(){
                        loadDetailInfo($(this).attr("data"),$(this).attr("data_name"),$(this).attr("data_Time"));//加载详情信息
                        $(".sx_modal").css("display","block")
                    });
                }
            });

          /*  $("#search").click(function(){
            });*/

            function loadDetailInfo(userId,realName,sxTime){
                /*授信用户姓名*/
                $('#realName1').html(realName);
                $('#realName2').html(realName);
                $('#realName3').html(realName);

                /*授信时间*/
                $('#sxTime1').html(sxTime);
                $('#sxTime2').html(sxTime);

                $.ajax({
                    url:"${basePath!}/riskControl/CreditInfo",
                    type:'GET', //GET
                    data:{uid:userId},
                    timeout:30000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(d){
                        /*初始授信信息*/
                        var initcreditData='';
                        for(var i = 0 ; i < d.data.creditGradeList.length ; i++){
                            initcreditData+='<tr>' +
                                    '<td>'+d.data.creditGradeList[i].name+'</td>' +
                                    '<td>'+d.data.creditGradeList[i].details+'</td>' +
                                    '<td>'+d.data.creditGradeList[i].value+'</td>' +
                                    '</tr>'
                        }
                        $('#init_credit').html(initcreditData);
                        $('#init_grade').html(d.data.initGrade);//加载初始授信分数


                        /*授信提额信息*/
                        if(d.data.tbUpMoneyList!=null && d.data.tbUpMoneyList.length>0) {
                            var upCreditData = '';
                            for (var i = 0; i < d.data.tbUpMoneyList.length; i++) {
                                upCreditData += '<tr>' +
                                        '<td>' + d.data.tbUpMoneyList[i].name + '</td>' +
                                        '<td></td>' +
                                        '<td>' + d.data.tbUpMoneyList[i].value + '</td>' +
                                        '</tr>'
                            }
                            $('#again_credit').html(upCreditData);
                        }else{
                            $("#again_credit").html('<tr><td colspan="12"><center>暂无数据</center></tr>');
                        }

                        //加载授信提额分数
                        $('#upCredit_grade').html(d.data.upmoneyGrade);

                        //加载总授信得分
                        $('#total_grade').html(d.data.totalGrade);

                        /*拒绝授信信息*/
                        var isrefuse = d.data.userRefCon.examineFailReason;//是否存在被拒绝
                        var isAgainCreditTime = d.data.userRefCon.nextauth_date;//是否存在下次授信时间
                        if(!isrefuse) {
                            $("#shouxinbutton").css("display","none")
                           //$("#refuse_credit").html('<div class="main-date" style="margin-top: 2px;text-align:center;">暂无数据</div>');
                        }else{

                            if(d.data.userRefCon.authStatus=='1'){
                                $("#shouxinbutton").css("display","none")
                            }else{
                                $("#refuse_reason").html(isrefuse);
                            }

                            if(isAgainCreditTime){
                               $("#isAgainCredit").html(d.data.userRefCon.nextauth_date);
                           }else{
                               $("#isAgainCredit").html("不可再次授信");
                           }
                        }
                        /*还款状态信息*/

                        if(d.data.tbRepaymentPlanList!=null && d.data.tbRepaymentPlanList.length>0){
                            var repayData='';
                            for(var i = 0 ; i < d.data.tbRepaymentPlanList.length ; i++){
                                repayData+='<tr>' +
                                        '<td>'+d.data.tbRepaymentPlanList[i].payType +'</td>' +
                                        '<td>'+d.data.tbRepaymentPlanList[i].repaymentDate +'</td>' +
                                        '<td>'+d.data.tbRepaymentPlanList[i].totalMoney +'</td>' +
                                        '<td>'+d.data.tbRepaymentPlanList[i].lateFee +'</td>' +
                                        '<td>'+d.data.tbRepaymentPlanList[i].waitpay +'</td>' +
                                        '</tr>'
                            }
                            $('#repay_List').html(repayData);
                        }else{
                            $("#repay_List").html('<tr><td colspan="12"><center>暂无数据</center></tr>');
                        }
                    }
                });
            }
        });

        $(".table a").click(function () {
            $(".sx_modal").css("display","block")
        });
        $(".modal_head b").click(function () {
            $(".sx_modal").css("display","none")
        });
        $(".sx_ok").click(function () {
            $(".sx_modal").css("display","none")
        });
    </script>
</body>

</html>
