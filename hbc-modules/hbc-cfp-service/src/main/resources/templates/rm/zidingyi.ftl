<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="${basePath!}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${basePath!}/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath!}/css/zidingyi.css">
    <title>评分自定义</title>

</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="pf_menu">
        <ul>
            <li class="left_menu">
                <div class="tab-container">
                    <ul class="menu-list">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> 拒绝授信</a>
                        </li>
                        <li><a data-toggle="tab" href="#tab-2">初始授信</a>
                        </li>
                        <li><a data-toggle="tab" href="#tab-3">提额授信</a>
                        </li>
                        <li><a data-toggle="tab" href="#tab-4">授信提额综合分值</a>
                        </li>
                        <li><a data-toggle="tab" href="#tab-5">外地授信管理</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="right_content">
                <div class="top_text">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4 no_sx">
                                拒绝授信
                            </div>
                            <div class="col-sm-8">
                                <ul>
                                    <li>
                                        <a href="#" class="edit">
                                            <img src="img/edit.png" alt="">
                                            编辑</a>
                                    </li>
                                    <li>
                                        <a href="#" class="confirms">确定</a>
                                    </li>
                                    <li>
                                        <a href="#" class="cancel">取消</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <!--拒绝授信-->
                    <div id="tab-1" class="tab-pane active">
                        <div class="one_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>条件</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>逾期平台</td>
                                    <td>2个及以上</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr class="yqcs">
                                    <td>单平台逾期次数</td>
                                    <td>2个及以上</td>
                                    <td>禁用</td>
                                    <td>
                                        <a href="#" class="edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>欺诈风险</td>
                                    <td>多头负债高于16家</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="sx_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑逾期平台</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="yq">
                                            <form>
                                                <ul>
                                                    <li>
                                                        <label>逾期平台命中数量</label>
                                                        <input type="text" placeholder="请输入命中数量">
                                                    </li>
                                                    <li>
                                                        <label>是否同意二次申请</label>
                                                        <select name="" id="">
                                                            <option value="">不同意</option>
                                                            <option value="">同意</option>
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <label>状态</label>
                                                        <input type="radio" name="radio" checked value="1">
                                                        <label for="">启用</label>
                                                        <input type="radio" name="radio" value="2">
                                                        <label for="">禁用</label>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--初始授信-->
                    <div id="tab-2" class="tab-pane">
                        <div class="tow_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>条件</th>
                                    <th>权重</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="5" style="border-bottom: 1px solid #ededed">户籍</td>
                                    <td>一线城市</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="city edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>

                                    <td>二线城市</td>
                                    <td>0.8</td>
                                    <td>启用</td>

                                </tr>
                                <tr>
                                    <td>三线城市</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>四线城市</td>
                                    <td>0.5</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>其他城市</td>
                                    <td>0.4</td>
                                    <td>禁用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="2" style="border-bottom: 1px solid #ededed">性别</td>
                                    <td>男</td>
                                    <td>0.7</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="sex edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>女</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="6" style="border-bottom: 1px solid #ededed">年龄</td>
                                    <td>18~22</td>
                                    <td>0.7</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="age edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>23~27</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>28~33</td>
                                    <td>0.7</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>34~45</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>46~50</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>51~80</td>
                                    <td>0.3</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="5">多头负债个数</td>
                                    <td>1~2</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="fuzai edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3~6</td>
                                    <td>0.8</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>7~10</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>11~13</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>14~16</td>
                                    <td>0.2</td>
                                    <td>启用</td>
                                </tr>
                                </tbody>
                            </table>
                            <!--编辑户籍城市-->
                            <div class="sore_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑户籍城市</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="tab-container">
                                            <ul>
                                                <li class="active"><a data-toggle="tab" href="#tab-a">一线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-b">二线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-c">三线城市</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-d">四线城市</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab-a" class="tab-pane active">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="" id="">
                                                                        <option value="">0</option>
                                                                        <option value="">0.1</option>
                                                                        <option value="">0.2</option>
                                                                        <option value="">0.3</option>
                                                                        <option value="">0.4</option>
                                                                        <option value="">0.5</option>
                                                                        <option value="">0.6</option>
                                                                        <option value="">0.7</option>
                                                                        <option value="">0.8</option>
                                                                        <option value="">0.9</option>
                                                                        <option value="">1.0</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="radio" checked value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="radio" value="2">
                                                                    <label for="">禁用</label>
                                                                </li>

                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-b" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="" id="">
                                                                        <option value="">0</option>
                                                                        <option value="">0.1</option>
                                                                        <option value="">0.2</option>
                                                                        <option value="">0.3</option>
                                                                        <option value="">0.4</option>
                                                                        <option value="">0.5</option>
                                                                        <option value="">0.6</option>
                                                                        <option value="">0.7</option>
                                                                        <option value="">0.8</option>
                                                                        <option value="">0.9</option>
                                                                        <option value="">1.0</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="radio" checked value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="radio" value="2">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-c" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="" id="">
                                                                        <option value="">0</option>
                                                                        <option value="">0.1</option>
                                                                        <option value="">0.2</option>
                                                                        <option value="">0.3</option>
                                                                        <option value="">0.4</option>
                                                                        <option value="">0.5</option>
                                                                        <option value="">0.6</option>
                                                                        <option value="">0.7</option>
                                                                        <option value="">0.8</option>
                                                                        <option value="">0.9</option>
                                                                        <option value="">1.0</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="radio" checked value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="radio" value="2">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div id="tab-d" class="tab-pane">
                                                    <div class="yq">
                                                        <form>
                                                            <ul>
                                                                <li>
                                                                    <label>评分权重</label>
                                                                    <select name="" id="">
                                                                        <option value="">0</option>
                                                                        <option value="">0.1</option>
                                                                        <option value="">0.2</option>
                                                                        <option value="">0.3</option>
                                                                        <option value="">0.4</option>
                                                                        <option value="">0.5</option>
                                                                        <option value="">0.6</option>
                                                                        <option value="">0.7</option>
                                                                        <option value="">0.8</option>
                                                                        <option value="">0.9</option>
                                                                        <option value="">1.0</option>
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <label style="margin-right: 30px">状态</label>
                                                                    <input type="radio" name="radio" checked value="1">
                                                                    <label for="">启用</label>
                                                                    <input type="radio" name="radio" value="2">
                                                                    <label for="">禁用</label>
                                                                </li>
                                                            </ul>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑性别-->
                            <div class="sex_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑性别</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="tab-container" style="margin-left: 10px">
                                            <ul>
                                                <li class="active"><a data-toggle="tab" href="#tab-e">男</a>
                                                </li>
                                                <li class=""><a data-toggle="tab" href="#tab-f">女</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <div id="tab-e" class="tab-pane active">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                性别
                                                            </div>
                                                            <div class="col-sm-4">
                                                                男
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                评分权重
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                状态
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="radio" name="sex" checked value="1">
                                                                <label for="">启用</label>
                                                                <input type="radio" name="sex" value="2">
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab-f" class="tab-pane">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                性别
                                                            </div>
                                                            <div class="col-sm-4">
                                                                女
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                评分权重
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                状态
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="radio" name="sexa" checked value="1">
                                                                <label for="">启用</label>
                                                                <input type="radio" name="sexa" value="2">
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑年龄权重-->
                            <div class="age_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑年龄权重</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="18">
                                                                ～
                                                                <input type="text" placeholder="22">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="23">
                                                                ～
                                                                <input type="text" placeholder="27">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="28">
                                                                ～
                                                                <input type="text" placeholder="33">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="34">
                                                                ～
                                                                <input type="text" placeholder="45">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age3" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age3" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="46">
                                                                ～
                                                                <input type="text" placeholder="50">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age5" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age5" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                年龄&nbsp
                                                                <input type="text" placeholder="51">
                                                                ～
                                                                <input type="text" placeholder="80">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age6" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age6" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑多头负债-->
                            <div class="fuzai_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑多头负债</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input type="text" placeholder="1">
                                                                ～
                                                                <input type="text" placeholder="2">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input type="text" placeholder="3">
                                                                ～
                                                                <input type="text" placeholder="6">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input type="text" placeholder="7">
                                                                ～
                                                                <input type="text" placeholder="10">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input type="text" placeholder="11">
                                                                ～
                                                                <input type="text" placeholder="13">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai3" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai3" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                负债个数&nbsp
                                                                <input type="text" placeholder="14">
                                                                ～
                                                                <input type="text" placeholder="16">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai5" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="fuzai5" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--提额授信-->
                    <div id="tab-3" class="tab-pane">
                        <div class="tow_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>条件</th>
                                    <th>权重</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed">分期还款</td>
                                    <td>逾期4天以上还款</td>
                                    <td>0</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="fq_repayment edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>

                                    <td>逾期1-3天还款</td>
                                    <td>0.4</td>
                                    <td>启用</td>

                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>按时还款</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>一次性还款</td>
                                    <td>按时还款、提前还款</td>
                                    <td>0.5</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="one_repayment edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" style="border-bottom: 1px solid #ededed">最低额度还款</td>
                                    <td>第一次按时还</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="zd_repayment edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>第二次按时还</td>
                                    <td>0..7</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="border-bottom: 1px solid #ededed">社保授权</td>
                                    <td>近1个月无社保信息，但以前 <br>有缴纳信息</td>
                                    <td>0.2</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="social_security edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1个月社保信息</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>2~3个月社保信息</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>4~6个月社保信息</td>
                                    <td>0.8</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>7个月以上社保信息</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="border-bottom: 1px solid #ededed">公积金授权</td>
                                    <td>近1个月无公积金信息，但以 <br>前有缴纳信息</td>
                                    <td>0.2</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="accumulation_fund edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1个月公积金信息</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>2~3个月公积金信息</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>4~6个月公积金信息</td>
                                    <td>0.8</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>7个月以上公积金信息</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed">淘宝+支付宝</td>
                                    <td>未获取有效地址</td>
                                    <td>0</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="taobao edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>获取1个有效地址</td>
                                    <td>0.5</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>获取2个有效地址</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed">京东</td>
                                    <td>未获取有效地址</td>
                                    <td>0</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="jindong edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>获取1个有效地址</td>
                                    <td>0.5</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>获取2个有效地址</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-bottom: 1px solid #ededed">学信网</td>
                                    <td>大专</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="xuexing edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>本科</td>
                                    <td>0.8</td>
                                    <td>启用</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ededed">
                                    <td>硕士及以上</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td rowspan="7">滴滴</td>
                                    <td>未获取授权</td>
                                    <td>0.2</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="didi edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分550分以下</td>
                                    <td>0.1</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分550分~580分</td>
                                    <td>0.2</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分581分~600分</td>
                                    <td>0.4</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分601分~620分</td>
                                    <td>0.6</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分621分~650分</td>
                                    <td>0.8</td>
                                    <td>启用</td>
                                </tr>
                                <tr>
                                    <td>芝麻信用分650分以上</td>
                                    <td>1.0</td>
                                    <td>启用</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                            <!--编辑分期还款-->
                            <div class="fq_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑分期还款</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                逾期4天以上还款
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                逾期1-3天以上还款
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                按时还款
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="yuqi2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑一次性还款-->
                            <div class="one_pay_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑一次性还款</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    条件
                                                </div>
                                                <div class="col-sm-4">
                                                    按时还款、提前还款
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    评分权重
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="" id="">
                                                        <option value="">0</option>
                                                        <option value="">0.1</option>
                                                        <option value="">0.2</option>
                                                        <option value="">0.3</option>
                                                        <option value="">0.4</option>
                                                        <option value="">0.5</option>
                                                        <option value="">0.6</option>
                                                        <option value="">0.7</option>
                                                        <option value="">0.8</option>
                                                        <option value="">0.9</option>
                                                        <option value="">1.0</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    状态
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="radio" name="one_hk" checked value="1">
                                                    &nbsp
                                                    <label for="">启用</label>
                                                    &nbsp
                                                    <input type="radio" name="one_hk" value="2">
                                                    &nbsp
                                                    <label for="">禁用</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑最低额度还款-->
                            <div class="zd_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑最低额度还款</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                第一次按时还
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="zd_pay" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="zd_pay" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                第二次按时还
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="age1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="age1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑社保授权-->
                            <div class="shebao_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑社保授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                近1个月无社保信息，<br>但以前有缴纳信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                1个月社保信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                2-3个月社保信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                4-6个月社保信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao3" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao3" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                7个月以上社保信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao4" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="shebao4" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑公积金授权-->
                            <div class="gongjijin_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑公积金授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                近1个月无公积金信息,<br>但以前有缴纳信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                1个月公积金信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                2-3个月公积金信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                4-6个月公积金信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin3" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin3" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                7个月以上公积金信息
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin4" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="gongjijin4" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑淘宝+支付宝授权-->
                            <div class="taobao_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑淘宝+支付宝授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                未获取有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                获取1个有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                获取2个有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑京东授权-->
                            <div class="jindong_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑京东授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                未获取有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                获取1个有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                获取2个有效地址
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="taobao2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑学信网授权-->
                            <div class="xuexing_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑学信网授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                大专
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                本科
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                硕士及以上
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="xuexing2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--编辑滴滴授权-->
                            <div class="didi_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <p>编辑滴滴授权</p>
                                        <b>×</b>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                未获取授权
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分550分以下
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi1" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi1" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分550分~580分
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi2" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi2" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分581分~600分
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi3" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi3" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分601分~620分
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi4" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi4" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分621分~650分
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi5" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi5" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                芝麻信用分650分以上
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">评分权重</label>
                                                                &nbsp
                                                                <select name="" id="">
                                                                    <option value="">0</option>
                                                                    <option value="">0.1</option>
                                                                    <option value="">0.2</option>
                                                                    <option value="">0.3</option>
                                                                    <option value="">0.4</option>
                                                                    <option value="">0.5</option>
                                                                    <option value="">0.6</option>
                                                                    <option value="">0.7</option>
                                                                    <option value="">0.8</option>
                                                                    <option value="">0.9</option>
                                                                    <option value="">1.0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">状态</label>
                                                                &nbsp
                                                                <input type="radio" name="didi6" checked value="1">
                                                                &nbsp
                                                                <label for="">启用</label>
                                                                &nbsp
                                                                <input type="radio" name="didi6" value="2">
                                                                &nbsp
                                                                <label for="">禁用</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--授信提额综合分值-->
                    <div id="tab-4" class="tab-pane">
                        <div class="tow_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>授信评分</th>
                                    <th>分值</th>
                                    <th>额度标准（元）</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="10">授信评分</td>
                                    <td>0分-25分</td>
                                    <td>1,500.00</td>
                                    <td>
                                        <a href="#" class="sx_pingfen edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>26分-30分</td>
                                    <td>2,000.00</td>
                                </tr>
                                <tr>
                                    <td>31分-35分</td>
                                    <td>2,500.00</td>
                                </tr>
                                <tr>
                                    <td>36分-40分</td>
                                    <td>3,000.00</td>
                                </tr>
                                <tr>
                                    <td>41分-45分</td>
                                    <td>3,500.00</td>
                                </tr>
                                <tr>
                                    <td>46分-50分</td>
                                    <td>3,800.00</td>
                                </tr>
                                <tr>
                                    <td>51分-60分</td>
                                    <td>4,000.00</td>
                                </tr>
                                <tr>
                                    <td>61分-65分</td>
                                    <td>4,300.00</td>
                                </tr>
                                <tr>
                                    <td>66分-70分</td>
                                    <td>4,600.00</td>
                                </tr>
                                <tr>
                                    <td>71分-75分</td>
                                    <td>4,800.00</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="sx_pingfen_modal">
                                <div class="modal_content">
                                    <div class="modal_head">
                                        <div class="modal_head">
                                            <p>编辑授信提额综合分值</p>
                                            <b>×</b>
                                        </div>
                                    </div>
                                    <div class="modal_body">
                                        <div class="age_list">
                                            <ul>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="0">
                                                                ～
                                                                <input type="text" placeholder="25">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="1500">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="26">
                                                                ～
                                                                <input type="text" placeholder="30">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="2000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="31">
                                                                ～
                                                                <input type="text" placeholder="35">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="2500">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="36">
                                                                ～
                                                                <input type="text" placeholder="40">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="3000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="41">
                                                                ～
                                                                <input type="text" placeholder="45">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="3500">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="46">
                                                                ～
                                                                <input type="text" placeholder="50">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="3800">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="51">
                                                                ～
                                                                <input type="text" placeholder="60">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="4000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="61">
                                                                ～
                                                                <input type="text" placeholder="65">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="4300">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="66">
                                                                ～
                                                                <input type="text" placeholder="70">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="4600">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                分值
                                                                &nbsp
                                                                <input type="text" placeholder="71">
                                                                ～
                                                                <input type="text" placeholder="75">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                额度（元）
                                                                &nbsp
                                                                <input type="text" placeholder="4800">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal_foot">
                                        <div class="sx_zf">
                                            <ul>
                                                <li class="sx_sore">
                                                    取消
                                                </li>
                                                <li class="sx_ok">
                                                    <a href="#">确定</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--外地授信管理-->
                    <div id="tab-5" class="tab-pane">
                        <div class="tow_table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>类别</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>外地手机号不可授信</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="wd_phone edit_btn ">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>外地IP不可授信</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="wd_ip edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>外地户籍不可授信</td>
                                    <td>启用</td>
                                    <td>
                                        <a href="#" class="wd_hj edit_btn">
                                            <img src="img/u1733.png" alt="">
                                            编辑
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--编辑外地手机号不可授信-->
                        <div class="phone_modal">
                            <div class="modal_content">
                                <div class="modal_head">
                                    <p>
                                        编辑外地手机号不可授信
                                    </p>
                                    <b>×</b>
                                </div>
                                <div class="modal_body">
                                    <div class="qd_content">
                                        <ul>
                                            <li>
                                                外地手机号不可授信
                                            </li>
                                            <li>
                                                <label for="">状态</label>
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <input type="radio" name="phone" checked value="1">
                                                &nbsp
                                                <label for="">启用</label>
                                                &nbsp
                                                <input type="radio" name="phone" value="1">
                                                &nbsp
                                                <label for="">禁用</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="modal_foot">
                                    <div class="sx_zf">
                                        <ul>
                                            <li class="sx_sore">
                                                取消
                                            </li>
                                            <li class="sx_ok">
                                                <a href="#">确定</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--编辑外地IP不可授信-->
                        <div class="ip_modal">
                            <div class="modal_content">
                                <div class="modal_head">
                                    <p>
                                        编辑外地IP不可授信
                                    </p>
                                    <b>×</b>
                                </div>
                                <div class="modal_body">
                                    <div class="qd_content">
                                        <ul>
                                            <li>
                                                外地IP不可授信
                                            </li>
                                            <li>
                                                <label for="">状态</label>
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <input type="radio" name="ip" checked value="1">
                                                &nbsp
                                                <label for="">启用</label>
                                                &nbsp
                                                <input type="radio" name="ip" value="1">
                                                &nbsp
                                                <label for="">禁用</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="modal_foot">
                                    <div class="sx_zf">
                                        <ul>
                                            <li class="sx_sore">
                                                取消
                                            </li>
                                            <li class="sx_ok">
                                                <a href="#">确定</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--编辑外地户籍不可授信-->
                        <div class="hj_modal">
                            <div class="modal_content">
                                <div class="modal_head">
                                    <p>
                                        编辑外地户籍不可授信
                                    </p>
                                    <b>×</b>
                                </div>
                                <div class="modal_body">
                                    <div class="qd_content">
                                        <ul>
                                            <li>
                                                外地户籍不可授信
                                            </li>
                                            <li>
                                                <label for="">状态</label>
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <input type="radio" name="hj" checked value="1">
                                                &nbsp
                                                <label for="">启用</label>
                                                &nbsp
                                                <input type="radio" name="hj" value="1">
                                                &nbsp
                                                <label for="">禁用</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="modal_foot">
                                    <div class="sx_zf">
                                        <ul>
                                            <li class="sx_sore">
                                                取消
                                            </li>
                                            <li class="sx_ok">
                                                <a href="#">确定</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<script src="${basePath!}/js/jquery.min.js?v=2.1.4"></script>
<script src="${basePath!}/js/bootstrap.min.js?v=3.3.6"></script>
<script>
    $(".menu-list>li>a").click(function () {
        var t=$(this).text();
        $(".no_sx").html(t);
        $(".edit").css("display","inline");
        $(".confirms").css("display","none");
        $(".cancel").css("display","none");
        $(".edit_btn").css("display","none");
    });
    $(".edit").click(function () {
        $(".confirms").css("display","inline");
        $(".cancel").css("display","inline");
        $(".edit").css("display","none");
        $(".edit_btn").css("display","block");
    });
    $(".confirms").click(function () {
        $(".edit").css("display","inline");
        $(".confirms").css("display","none");
        $(".cancel").css("display","none");
        $(".edit_btn").css("display","none");
    });
    $(".cancel").click(function () {
        $(".edit").css("display","inline");
        $(".confirms").css("display","none");
        $(".cancel").css("display","none");
        $(".edit_btn").css("display","none");
    });
    $("#tab-1 a").click(function () {
        $(".sx_modal").css("display","block")
    });
    $("#tab-2 .city").click(function () {
        $(".sore_modal").css("display","block")
    });
    $("#tab-2 .sex").click(function () {
        $(".sex_modal").css("display","block")
    });
    $("#tab-2 .age").click(function () {
        $(".age_modal").css("display","block")
    });
    $("#tab-2 .fuzai").click(function () {
        $(".fuzai_modal").css("display","block")
    });
    $("#tab-3 .fq_repayment").click(function () {
        $(".fq_modal").css("display","block")
    });
    $("#tab-3 .one_repayment").click(function () {
        $(".one_pay_modal").css("display","block")
    });
    $("#tab-3 .zd_repayment").click(function () {
        $(".zd_modal").css("display","block")
    });
    $("#tab-3 .social_security").click(function () {
        $(".shebao_modal").css("display","block")
    });
    $("#tab-3 .accumulation_fund").click(function () {
        $(".gongjijin_modal").css("display","block")
    });
    $("#tab-3 .taobao").click(function () {
        $(".taobao_modal").css("display","block")
    });
    $("#tab-3 .jindong").click(function () {
        $(".jindong_modal").css("display","block")
    });
    $("#tab-3 .xuexing").click(function () {
        $(".xuexing_modal").css("display","block")
    });
    $("#tab-3 .didi").click(function () {
        $(".didi_modal").css("display","block")
    });
    $("#tab-4 .sx_pingfen").click(function () {
        $(".sx_pingfen_modal").css("display","block")
    });
    $("#tab-5 .wd_phone").click(function () {
        $(".phone_modal").css("display","block")
    });
    $("#tab-5 .wd_ip").click(function () {
        $(".ip_modal").css("display","block")
    });
    $("#tab-5 .wd_hj").click(function () {
        $(".hj_modal").css("display","block")
    });
    $(".modal_head b").click(function () {
        $(".sx_modal").css("display","none");
        $(".sore_modal").css("display","none");
        $(".sex_modal").css("display","none");
        $(".age_modal").css("display","none");
        $(".fuzai_modal").css("display","none");
        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");
        $(".sx_pingfen_modal").css("display","none");
        $(".phone_modal").css("display","none");
        $(".ip_modal").css("display","none");
        $(".hj_modal").css("display","none")
    });
    $(".sx_ok").click(function () {
        $(".sx_modal").css("display","none");
        $(".sore_modal").css("display","none");
        $(".sex_modal").css("display","none");
        $(".age_modal").css("display","none");
        $(".fuzai_modal").css("display","none");
        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");
        $(".sx_pingfen_modal").css("display","none");
        $(".phone_modal").css("display","none");
        $(".ip_modal").css("display","none");
        $(".hj_modal").css("display","none")
    });
    $(".sx_sore").click(function () {
        $(".sx_modal").css("display","none");
        $(".sore_modal").css("display","none");
        $(".sex_modal").css("display","none");
        $(".age_modal").css("display","none");
        $(".fuzai_modal").css("display","none");
        $(".fq_modal").css("display","none");
        $(".one_pay_modal").css("display","none");
        $(".zd_modal").css("display","none");
        $(".shebao_modal").css("display","none");
        $(".gongjijin_modal").css("display","none");
        $(".taobao_modal").css("display","none");
        $(".jindong_modal").css("display","none");
        $(".xuexing_modal").css("display","none");
        $(".didi_modal").css("display","none");
        $(".sx_pingfen_modal").css("display","none");
        $(".phone_modal").css("display","none");
        $(".ip_modal").css("display","none");
        $(".hj_modal").css("display","none")
    });
</script>
</body>
</html>