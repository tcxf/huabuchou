package com.tcxf.hbc.ctu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhouyj
 * @date 2018/3/23
 */
@EnableAsync
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.tcxf.hbc.ctu", "com.tcxf.hbc.common.bean"})
public class HbcCtuApplication {
    public static void main(String[] args) {
        SpringApplication.run(HbcCtuApplication.class, args);
    }
}