package com.tcxf.hbc.ctu.common.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @program: Workspace2
 * @description: 公信宝配置
 * @author: JinPeng
 * @create: 2018-07-24 17:48
 **/
@Configuration
@ConditionalOnProperty(prefix = "gxb", name = "appId")
@ConfigurationProperties(prefix = "gxb")
public class GxbPropertiesConfig {

    private String appId;

    private String appSecurity;

    private String gxbApiFactory;

    private String gxbApiDataFactory;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecurity() {
        return appSecurity;
    }

    public void setAppSecurity(String appSecurity) {
        this.appSecurity = appSecurity;
    }

    public String getGxbApiFactory() {
        return gxbApiFactory;
    }

    public void setGxbApiFactory(String gxbApiFactory) {
        this.gxbApiFactory = gxbApiFactory;
    }

    public String getGxbApiDataFactory() {
        return gxbApiDataFactory;
    }

    public void setGxbApiDataFactory(String gxbApiDataFactory) {
        this.gxbApiDataFactory = gxbApiDataFactory;
    }
}
