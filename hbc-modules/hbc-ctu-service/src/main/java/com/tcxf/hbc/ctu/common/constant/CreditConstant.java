package com.tcxf.hbc.ctu.common.constant;

public interface CreditConstant {
    String CARD_INFO = "CARD_INFO";//银行三要素
    String CARD_SAME = "SAME";//匹配
    String CARD_DIFFERENT = "DIFFERENT";//不匹配
    String CARD_ACCOUNTNO_UNABLE_VERIFY = "ACCOUNTNO_UNABLE_VERIFY";//信息不匹配,不能核实
    String ACCOUNT_NO = "ACCOUNT_NO";//银行卡
    String POLICE_INFO = "POLICE_INFO";//犯罪信息
    String MOBILE_INFO = "MOBILE_INFO";//运营商
    String MULTIPLE_INFO = "MULTIPLE_INFO";//多头负债
    String CONTACTCNT_INFO = "CONTACTCNT_INFO";//非标签号码
    String DEBTCAll_INFO = "DEBTCAll_INFO";//催收类号码
    String CITY_LEVEL_FIRST ="1";//一线城市
    String CITY_LEVEL_SECOND ="2";//二线城市
    String CITY_LEVEL_THREE ="3";//三线城市
    String CITY_LEVEL_FOUR ="4";//四线城市
    String THREELEMENT = "threelement";//银行卡三要素
    String OPERATORS = "operators";//运营商三要素
    String MULTIPLENEW = "multiplenew";//多重负债



    /*****************************拒绝授信条件常量*************************/
    String AGECHECK = "agecheck";//年龄条件
    String OVERDUECHECK = "overduecheck";//逾期平台条件
    String OVERDUENUMCHECK = "overduenumcheck";//逾期次数条件
    String LIABILITIENUMCHECK = "liabilitienumcheck";//多头负债平台注册个数条件
    String COLLECTIONNUMCHECK = "collectionnumcheck";//催收类别电话个数条件
    String CONTACTSNUMCHECK = "contactsnumcheck";//6个月内联系人电话个数条件





    String MOBILE = "1";//中国移动
    String UNICOM = "2";//中国联通
    String TELECOM = "3";//中国电信

    String SEXSCORE = "sexScore";//性别评分
    String AGESCORE = "ageScore";//年龄评分
    String CITYSCORE = "cityScore";//户籍评分
    String MUTIPLESCORE = "mutipleScore";//多头负债评分
    String TAGMOBILESCORE = "tagMobileScore";//标签类电话评分
    String DEBTCALLSCORE = "debtCallScore";//催收类电话评分
    String PAYMENTSCREDITPARTSCORE = "paymentsCreditPartScore";//分期还款评分
    String SINGLECREDITPARTSCORE = "singleCreditPartScore";//一次性还款评分

    String THREELEMENTTYPE = "1";//三要素接口
    String OPERATORSTYPE = "2";//运营商接口
    String MULTIPLENEWTYPE = "3";//多头负债接口
    String DEBTCALLCNTTYPE = "4";//手机电话记录接口
    String OPERATORINITCONFTYPE = "5";//获取登录方式接口
    String SUBMITLOGINSTATUSTYPE = "6";//运营商登录接口
    String SHEBAOTYPE = "7";//社保返回参数接口
    String HOUSEFUNDTYPE = "8";//公积金返回参数接口
    String CHSITYPE = "9";//学信网返回参数接口
    String ECOMMERCETYPE = "10";//淘宝返回参数接口
    String JDTYPE = "11";//京东返回参数接口
    String SESAMESCORETYPE = "12";//滴滴芝麻分返回参数接口

}
