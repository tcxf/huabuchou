package com.tcxf.hbc.ctu.common.constant;

public interface IncreaseCreditConstant {
    String EMERGENCY_CONTACT = "emergencyContact";//紧急联系人
    String QQORWECHAT = "qqOrWeChat";//qq或者微信
    String CONTACTNAME = "contactName";//联系人姓名
    String CONTACTMOBILE = "contactMobile";//联系人手机号码
    String RELATION = "relation";//与本人关系
    String SOCIALINSURANCE = "socialInsurance";//社保认证
    String ACCUMULATIONFUND = "accumulationFund";//公积金认证
    String EDUCATIONAPPROVE = "educationApprove";//学历认证
    String TAOBAOAPPROVE = "taobaoApprove";//淘宝认证
    String JDCOMAPPROVE = "jDcomApprove";//京东认证
    String DIDIDACHEAPPROVE = "didiDacheApprove";//滴滴认证


    public static final String SHEBAOURL = "https://prod.gxb.io/v2/auth/shebao?returnUrl=https://prod.gxb.io&token=";//社保H5地址
    public static final String HOUSEFUNDURL = "https://prod.gxb.io/v2/auth/housefund?returnUrl=https://prod.gxb.io&token=";//公积金H5地址
    public static final String CHSIURL = "https://prod.gxb.io/v2/auth/chsi?returnUrl=https://prod.gxb.io&token=";//学信网H5地址
    public static final String ECOMMERCEURL = "https://prod.gxb.io/v2/auth/ecommerce?returnUrl=https://prod.gxb.io&token=";//淘宝/支付宝H5地址
    public static final String JDURL = "https://prod.gxb.io/v2/auth/jd?returnUrl=https://prod.gxb.io&token=";//京东H5地址
    public static final String DIDIZMURL = "https://prod.gxb.io/v2/auth/didizm?returnUrl=https://prod.gxb.io&token=";//滴滴H5地址

}
