package com.tcxf.hbc.ctu.common.util;

import com.tcxf.hbc.ctu.common.util.gxb.OperatorTaskTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class GXBInterfaceUtil {
    Logger logger = LoggerFactory.getLogger(GXBInterfaceUtil.class);
    public static HashMap<String,Object> doOperator()
    {
        HashMap<String,Object> map = new HashMap<String,Object>();
        try
        {
            OperatorTaskTest ott = new OperatorTaskTest();
            map= ott.doOperatorTask();

        }
        catch(Exception e)
        {
//            logger.error(e.getMessage());
        }
        return map;
    }

    public static void main(String[] args)
    {
        GXBInterfaceUtil.doOperator();
    }
}
