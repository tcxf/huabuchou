package com.tcxf.hbc.ctu.common.util;

import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.ctu.model.dto.MobileTest;
import net.sf.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class OperatorUtil{

    public static String calcMobileCity(String mobileNumber) throws MalformedURLException {

        //获取拍拍网的API地址
        //        String urlString = "http://virtual.paipai.com/extinfo/GetMobileProductInfo?mobile="
        //                + mobileNumber + "&amount=10000&callname=getPhoneNumInfoExtCallback";
        //淘宝网的API地址
        String urlString = "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel="
                + mobileNumber;

        StringBuffer sb = new StringBuffer();
        BufferedReader buffer;
        URL url = new URL(urlString);
        String province = "";
        try {
            //获取URL地址中的页面内容
            InputStream in = url.openStream();
            // 解决乱码问题
            buffer = new BufferedReader(new InputStreamReader(in, "gb2312"));
            String line = null;
            //一行一行的读取数据
            while ((line = buffer.readLine()) != null) {
                sb.append(line);
            }
            in.close();
            buffer.close();
            System.out.println(sb.toString());
            //定义两种不同格式的字符串
            //   __GetZoneResult_ = {    mts:'1594578',    province:'黑龙江',    catName:'中国移动',    telString:'15945782060',    areaVid:'30496',    ispVid:'3236139',   carrier:'黑龙江移动'}
//            String objectStr = "{\"mts\":\"1594578\",\"province\":\"黑龙江\",\"catName\":\"中国移动\",\"telString\":\"15945782060\",\"areaVid\":\"30496\",\"ispVid\":\"3236139\",\"carrier\":\"黑龙江移动\"}";

            //1、使用JSONObject
            JSONObject jsonObject2 = JSONObject.fromObject(sb.toString().substring(18,sb.length()));
            String pro1 = jsonObject2.getString("province");
            System.out.println(pro1);
            MobileTest stu = (MobileTest) JSONObject.toBean(jsonObject2, MobileTest.class);
            province = stu.getProvince();
            System.out.println(province);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //从JSONObject对象中读取城市名称
        return province/*jsonObject.getString("cityname")*/;
    }



    //得归属地，运营商。如：西双版纳,中国电信
    public static String getResult(String tel) {
        try{
            //获取返回结果
            String json = httpRequest(tel).toString();
            //拆分xml页面代码
            String[] a = json.split("att");
            String[] b = a[1].split(",");
            //归属地
            String city = "";
            if (b.length == 3){
                city = b[2].replace(">", "").replace("</", "");
            }else if (b.length == 2){
                city = b[1].replace(">", "").replace("</", "");
            }
            String[] c = a[2].split("operators");
            //运营商
            String carrier = c[1].replace(">", "").replace("</", "");
            String cityAndCarrier = city+","+carrier;
            return cityAndCarrier;
        }catch(Exception e){
            throw new CheckedException("手机号码归属地异常，请稍后再试");
        }
    }

    //得归属地，运营商。如：西双版纳,中国电信
    public static String getCarrier(String tel) {
        try{
            //获取返回结果
            String json = httpRequest(tel).toString();
            //拆分xml页面代码
            String[] a = json.split("att");
            //归属地
            String[] c = a[2].split("operators");
            //运营商
            String carrier = c[1].replace(">", "").replace("</", "");
            return carrier;
        }catch(Exception e){
            throw new CheckedException("手机号码归属地异常，请稍后再试");
        }
    }

    /**
     * 发起http请求获取返回结果
     * @param tel 待查询手机号
     * @return String 结果字符串
     */
    public static String httpRequest(String tel) {

        //组装查询地址(requestUrl 请求地址)
        String requestUrl = "http://api.k780.com:88/?app=phone.get&phone="+tel+"&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=xml";

        //String requestUrl = "http://api.k780.com:88/?app=idcard.get&idcard=""433025199110106625&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=xml%22"
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoOutput(false);
            httpUrlConn.setDoInput(true);
            httpUrlConn.setUseCaches(false);
            httpUrlConn.setRequestMethod("GET");
            httpUrlConn.connect();
            //将返回的输入流转换成字符串
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            //释放资源
            inputStream.close();
            inputStream = null;
            httpUrlConn.disconnect();
        }
        catch (Exception e) {
            return "发起http请求后，获取返回结果失败！";
        }
        return buffer.toString();
    }





    // 测试
    public static void main(String[] args) throws Exception{
//        System.out.println(OperatorUtil.calcMobileCity("13647300951"));
//        String str="湖南省长沙市";
//        System.out.println(str.indexOf("江西"));

//        System.out.println(OperatorUtil.getCarrier("13918410871"));

//        System.out.println(OperatorUtil.getResult("13918410871"));

        System.out.println(OperatorUtil.getResult("15802653507"));
    }

}