package com.tcxf.hbc.ctu.common.util.api;

import com.tcxf.hbc.ctu.common.util.parm.AuthRequest;
import com.tcxf.hbc.ctu.common.util.parm.AuthToken;
import com.tcxf.hbc.ctu.common.util.parm.GxbResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * 统一认证授权接口
 */
public interface AuthApi {
    /**
     * 获取授权token
     *
     * @param req
     * @return
     */
    @POST("v2/get_auth_token")
    Call<GxbResponse<AuthToken>> auth(@Body AuthRequest req);

}
