package com.tcxf.hbc.ctu.common.util.api;

import com.tcxf.hbc.ctu.common.util.parm.GxbResponse;
import com.tcxf.hbc.ctu.common.util.parm.Status;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginConfig;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.Map;

/**
 * 人行征信API
 */
public interface CreditApi {
    /**
     * 获取登录配置
     *
     * @param token 用户的授权token
     * @return
     */
    @GET("minterm/v3/init_config/credit/{token}")
    Call<GxbResponse<LoginConfig>> getCreditInitConf(@Path("token") String token);

    /**
     * 获取任务的当前状态
     * 
     * @param token
     * @return
     */
    @GET("minterm/v3/get_status/{token}")
    Call<GxbResponse<Status>> getStatus(@Path("token") String token);

    /**
     * 登录提交接口，提交后就要开始轮训后台状态，直到登录成功或失败
     * 
     * @param token
     * @return
     */
    @POST("minterm/v3/login_submit/{token}")
    Call<GxbResponse<Status>> submitLogin(@Path("token") String token, @Body Map<String, Object> request);

}
