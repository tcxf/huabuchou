package com.tcxf.hbc.ctu.common.util.api;

import com.tcxf.hbc.ctu.common.util.data.shebao.ShebaoChannelGroupDTO;
import com.tcxf.hbc.ctu.common.util.parm.GxbResponse;
import com.tcxf.hbc.ctu.common.util.parm.Status;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginConfig;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

/**
 * @author Shayne
 * @description
 * @create 2018-05-04 下午3:15
 **/
public interface ShebaoApi {
    /**
     * 获取社保城市列表
     *
     * @return
     * @Param token
     */
    @GET(value = "shebao/v4/channelList/{token}")
    Call<GxbResponse<List<ShebaoChannelGroupDTO>>> queryChannelListConfig(@Path("token") String token);

    /**
     * 获取社保登录方式列表 LoginConfig
     *
     * @param token
     * @param channelCode
     * @return
     */
    @GET(value = "shebao/v4/init_config/{token}/{channelCode}")
    Call<GxbResponse<LoginConfig>> queryAuthDetailConfig(@Path("token") String token, @Path("channelCode") String channelCode);

    /**
     * 提交用户信息认证
     *
     * @param token
     * @param
     * @return
     */

    @POST(value = "shebao/v4/login_submit/{token}")
    Call<GxbResponse<Status>> sLoginSubmit(@Path("token") String token, @Body RequestBody requestBody);

    /**
     * 查询任务状态
     *
     * @param token
     * @return
     */
    @GET(value = "shebao/v3/get_status/{token}")
    Call<GxbResponse<Status>> getStatus(@Path("token") String token);

    /**
     * 刷新验证码
     *
     * @param token
     * @return
     */
    @GET(value = "shebao/v3/refresh_verify_code/{token}/")
    Call<GxbResponse<Status>> refreshVerifyCode(@Path("token") String token);

    /**
     * 刷新验证码
     *
     * @param token
     * @return
     */
    @GET(value = "shebao/v3/refresh_sms_code/{token}/")
    Call<GxbResponse<Status>> refreshSmsVerifyCode(@Path("token") String token, @Body RequestBody requestBody);

    /**
     * 提交验证码
     *
     * @param token
     * @return
     */
    @POST(value = "shebao/v3/code_submit/{token}/{code}")
    Call<GxbResponse<Status>> codeSubmit(@Path("token") String token, @Path("code") String code);


}
