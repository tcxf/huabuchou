package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:34
 */
public class CreditLoanRecord implements Serializable{

    private static final long serialVersionUID = -3130937310041258415L;

    /**
     * 贷款类型(1，信用卡；2，购房贷款；3，其他贷款；)
     */
    private Byte loanType;

    /**
     * 银行名称/贷款公司名称
     */
    private String orgName;

    /**
     * 信用卡类型(0其他 1贷记卡 2准贷记卡 ) 信用卡独有
     */
    private Byte cardType;

    /**
     * 账户类型
     */
    private Integer currencyType;

    /**
     * 是否发生逾期
     */
    private Boolean isOverdue;

    /**
     * 五年内逾期月数
     */
    private Integer overdueMonths;

    /**
     * 发放日期
     */
    private Date grantDate;

    /**
     * 截止日期
     */
    private Date endDate;

    /**
     * 额度(折合人命币)
     */
    private BigDecimal creditQuota;

    /**
     * 已使用额度（折合人民币）
     */
    private BigDecimal usedQuota;

    /**
     * 逾期金额
     */
    private BigDecimal overdueQuota;

    /**
     * 账户状态("1，未激活；2，正常；3，冻结；4，销户；5结清)
     */
    private Byte accountStatus;

    /**
     * 详情
     */
    private String detail;


    public CreditLoanRecord() {
    }

    public Byte getLoanType() {
        return loanType;
    }

    public void setLoanType(Byte loanType) {
        this.loanType = loanType;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Byte getCardType() {
        return cardType;
    }

    public void setCardType(Byte cardType) {
        this.cardType = cardType;
    }

    public Integer getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Integer currencyType) {
        this.currencyType = currencyType;
    }

    public Boolean getOverdue() {
        return isOverdue;
    }

    public void setOverdue(Boolean overdue) {
        isOverdue = overdue;
    }

    public Integer getOverdueMonths() {
        return overdueMonths;
    }

    public void setOverdueMonths(Integer overdueMonths) {
        this.overdueMonths = overdueMonths;
    }

    public Date getGrantDate() {
        return grantDate;
    }

    public void setGrantDate(Date grantDate) {
        this.grantDate = grantDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getCreditQuota() {
        return creditQuota;
    }

    public void setCreditQuota(BigDecimal creditQuota) {
        this.creditQuota = creditQuota;
    }

    public BigDecimal getUsedQuota() {
        return usedQuota;
    }

    public void setUsedQuota(BigDecimal usedQuota) {
        this.usedQuota = usedQuota;
    }

    public BigDecimal getOverdueQuota() {
        return overdueQuota;
    }

    public void setOverdueQuota(BigDecimal overdueQuota) {
        this.overdueQuota = overdueQuota;
    }

    public Byte getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Byte accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
