package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:34
 */
public class CreditStatis implements Serializable{

    private static final long serialVersionUID = -6752302612123538625L;

    /**
     * 贷款类型（1，信用卡；2，购房贷款；3，其他贷款；）
     */
    private Byte loanType;

    /**
     * 账户数
     */
    private Integer accountCount;

    /**
     * 未结清/未销户账户数
     */
    private Integer unclearedCount;

    /**
     * 发生过逾期的账户数
     */
    private Integer overdueCount;

    /**
     * 发生过90天以上逾期的账户数
     */
    private Integer longOverdueCount;

    /**
     * 为他人担保笔数
     */
    private Integer ensureCount;

    public CreditStatis() {
    }

    public Byte getLoanType() {
        return loanType;
    }

    public void setLoanType(Byte loanType) {
        this.loanType = loanType;
    }

    public Integer getAccountCount() {
        return accountCount;
    }

    public void setAccountCount(Integer accountCount) {
        this.accountCount = accountCount;
    }

    public Integer getUnclearedCount() {
        return unclearedCount;
    }

    public void setUnclearedCount(Integer unclearedCount) {
        this.unclearedCount = unclearedCount;
    }

    public Integer getOverdueCount() {
        return overdueCount;
    }

    public void setOverdueCount(Integer overdueCount) {
        this.overdueCount = overdueCount;
    }

    public Integer getLongOverdueCount() {
        return longOverdueCount;
    }

    public void setLongOverdueCount(Integer longOverdueCount) {
        this.longOverdueCount = longOverdueCount;
    }

    public Integer getEnsureCount() {
        return ensureCount;
    }

    public void setEnsureCount(Integer ensureCount) {
        this.ensureCount = ensureCount;
    }
}
