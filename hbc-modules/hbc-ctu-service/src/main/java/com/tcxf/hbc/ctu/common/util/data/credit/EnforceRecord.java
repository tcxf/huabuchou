package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/5/23 下午8:37
 */
public class EnforceRecord implements Serializable {

    private static final long serialVersionUID = -8812512665074613197L;

    private String detail;
    private String orgName;
    private String caseNo;
    private String reason;
    private String closeWay;
    private String caseStatus;
    private String requireSubject;
    private String enforceSubject;
    private BigDecimal requireQuota;
    private BigDecimal enforceQuota;
    private Date startDate;
    private Date endDate;

    public EnforceRecord() {
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCloseWay() {
        return closeWay;
    }

    public void setCloseWay(String closeWay) {
        this.closeWay = closeWay;
    }

    public String getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }

    public String getRequireSubject() {
        return requireSubject;
    }

    public void setRequireSubject(String requireSubject) {
        this.requireSubject = requireSubject;
    }

    public String getEnforceSubject() {
        return enforceSubject;
    }

    public void setEnforceSubject(String enforceSubject) {
        this.enforceSubject = enforceSubject;
    }

    public BigDecimal getRequireQuota() {
        return requireQuota;
    }

    public void setRequireQuota(BigDecimal requireQuota) {
        this.requireQuota = requireQuota;
    }

    public BigDecimal getEnforceQuota() {
        return enforceQuota;
    }

    public void setEnforceQuota(BigDecimal enforceQuota) {
        this.enforceQuota = enforceQuota;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EnforceRecord{");
        sb.append("detail='").append(detail).append('\'');
        sb.append(", orgName='").append(orgName).append('\'');
        sb.append(", caseNo='").append(caseNo).append('\'');
        sb.append(", reason='").append(reason).append('\'');
        sb.append(", closeWay='").append(closeWay).append('\'');
        sb.append(", caseStatus='").append(caseStatus).append('\'');
        sb.append(", requireSubject='").append(requireSubject).append('\'');
        sb.append(", enforceSubject='").append(enforceSubject).append('\'');
        sb.append(", requireQuota=").append(requireQuota);
        sb.append(", enforceQuota=").append(enforceQuota);
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append('}');
        return sb.toString();
    }
}
