package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/5/18 下午6:55
 */
public class EnsureLoanRecord implements Serializable{

    private static final long serialVersionUID = 6972792487977939558L;
    /**
     * 被担保人姓名
     */
    private String beEnsureName;
    /**
     * 被担保人证件类型
     */
    private String cardType;
    /**
     * 被担保人证件号码
     */
    private String cardNumber;
    /**
     * 贷款机构
     */
    private String orgName;
    /**
     * 担保剩余金额
     */
    private BigDecimal ensureQuota;
    /**
     * 贷款金额(折合人命币)
     */
    private BigDecimal creditQuota;
    /**
     * 担保总金额
     */
    private BigDecimal totalEnsureQuota;
    /**
     * 详情
     */
    private String detail;
    /**
     * 截止时间
     */
    private Date endDate;
    /**
     * 担保时间
     */
    private Date ensureDate;
    /**
     * 账户状态
     */
    private Integer accountStatus;

    public EnsureLoanRecord() {
    }


    public String getBeEnsureName() {
        return beEnsureName;
    }

    public void setBeEnsureName(String beEnsureName) {
        this.beEnsureName = beEnsureName;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public BigDecimal getEnsureQuota() {
        return ensureQuota;
    }

    public void setEnsureQuota(BigDecimal ensureQuota) {
        this.ensureQuota = ensureQuota;
    }

    public BigDecimal getCreditQuota() {
        return creditQuota;
    }

    public void setCreditQuota(BigDecimal creditQuota) {
        this.creditQuota = creditQuota;
    }

    public BigDecimal getTotalEnsureQuota() {
        return totalEnsureQuota;
    }

    public void setTotalEnsureQuota(BigDecimal totalEnsureQuota) {
        this.totalEnsureQuota = totalEnsureQuota;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEnsureDate() {
        return ensureDate;
    }

    public void setEnsureDate(Date ensureDate) {
        this.ensureDate = ensureDate;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EnsureLoanRecord{");
        sb.append("beEnsureName='").append(beEnsureName).append('\'');
        sb.append(", cardType='").append(cardType).append('\'');
        sb.append(", cardNumber='").append(cardNumber).append('\'');
        sb.append(", orgName='").append(orgName).append('\'');
        sb.append(", ensureQuota=").append(ensureQuota);
        sb.append(", creditQuota=").append(creditQuota);
        sb.append(", totalEnsureQuota=").append(totalEnsureQuota);
        sb.append(", detail='").append(detail).append('\'');
        sb.append(", endDate=").append(endDate);
        sb.append(", ensureDate=").append(ensureDate);
        sb.append(", accountStatus=").append(accountStatus);
        sb.append('}');
        return sb.toString();
    }
}
