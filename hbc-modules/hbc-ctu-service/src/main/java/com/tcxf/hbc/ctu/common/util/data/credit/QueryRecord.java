package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:35
 */
public class QueryRecord implements Serializable{

    private static final long serialVersionUID = -8664731462559975329L;

    /**
     * 查询类型(机构查询还是个人查询)
     */
    private Byte queryType;

    /**
     * 查询日期
     */
    private Date queryDate;

    /**
     * 查询操作员
     */
    private String operator;

    /**
     * 查询原因
     */
    private String reason;

    public Byte getQueryType() {
        return queryType;
    }

    public void setQueryType(Byte queryType) {
        this.queryType = queryType;
    }

    public Date getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(Date queryDate) {
        this.queryDate = queryDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
