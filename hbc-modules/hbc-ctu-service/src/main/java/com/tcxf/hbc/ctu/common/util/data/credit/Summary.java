package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:35
 */
public class Summary implements Serializable{


    private static final long serialVersionUID = 1241909744460555146L;
    /**
     * 信贷记录概要
     */
    private String loanSummary;

    /**
     * 公共记录概要
     */
    private String publicSummary;

    /**
     * 查询记录概要
     */
    private String querySummary;

    /**
     * 征信报告编号
     */
    private String reportNo;

    /**
     * 查询时间
     */
    private Date queryTime;

    /**
     * 报告时间
     */
    private Date reportTime;

    public Summary() {
    }

    public String getLoanSummary() {
        return loanSummary;
    }

    public void setLoanSummary(String loanSummary) {
        this.loanSummary = loanSummary;
    }

    public String getPublicSummary() {
        return publicSummary;
    }

    public void setPublicSummary(String publicSummary) {
        this.publicSummary = publicSummary;
    }

    public String getQuerySummary() {
        return querySummary;
    }

    public void setQuerySummary(String querySummary) {
        this.querySummary = querySummary;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public Date getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(Date queryTime) {
        this.queryTime = queryTime;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }
}
