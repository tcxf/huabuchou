package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:28
 */

public class UserCreditData implements Serializable{


    private static final long serialVersionUID = 3781092209401647479L;

    private UserInfo userInfo;
    private List<CreditLoanRecord>  creditLoanRecords;

    private List<LoanRecord>  houseLoanRecords;

    private List<LoanRecord>  otherLoanRecords;

    private Summary summary;

    private List<CreditStatis> creditStatis;

    private List<QueryRecord> queryRecords;

    private List<EnsureLoanRecord> ensureLaneRecords;

    private List<EnforceRecord> enforceRecords;

    public UserCreditData() {
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<CreditLoanRecord> getCreditLoanRecords() {
        return creditLoanRecords;
    }

    public void setCreditLoanRecords(List<CreditLoanRecord> creditLoanRecords) {
        this.creditLoanRecords = creditLoanRecords;
    }

    public List<LoanRecord> getHouseLoanRecords() {
        return houseLoanRecords;
    }

    public void setHouseLoanRecords(List<LoanRecord> houseLoanRecords) {
        this.houseLoanRecords = houseLoanRecords;
    }

    public List<LoanRecord> getOtherLoanRecords() {
        return otherLoanRecords;
    }

    public void setOtherLoanRecords(List<LoanRecord> otherLoanRecords) {
        this.otherLoanRecords = otherLoanRecords;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public List<CreditStatis> getCreditStatis() {
        return creditStatis;
    }

    public void setCreditStatis(List<CreditStatis> creditStatis) {
        this.creditStatis = creditStatis;
    }

    public List<QueryRecord> getQueryRecords() {
        return queryRecords;
    }

    public void setQueryRecords(List<QueryRecord> queryRecords) {
        this.queryRecords = queryRecords;
    }

    public List<EnsureLoanRecord> getEnsureLaneRecords() {
        return ensureLaneRecords;
    }

    public void setEnsureLaneRecords(List<EnsureLoanRecord> ensureLaneRecords) {
        this.ensureLaneRecords = ensureLaneRecords;
    }

    public List<EnforceRecord> getEnforceRecords() {
        return enforceRecords;
    }

    public void setEnforceRecords(List<EnforceRecord> enforceRecords) {
        this.enforceRecords = enforceRecords;
    }
}
