package com.tcxf.hbc.ctu.common.util.data.credit;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenzhihui
 * @date 2018/4/10 上午11:28
 */

public class UserInfo implements Serializable{

    private static final long serialVersionUID = -5507025915384267235L;
    /**
     * 姓名
     */
    private String name;

    /**
     * 证件类型
     */
    private String cardType;

    /**
     * 证件号码
     */
    private String cardNumber;

    /**
     * 婚姻状况（1,未婚；2,已婚；3,离异；）
     */
    private Integer marriage;

    /**
     * 征信报告编号
     */
    private String reportNo;

    /**
     * 查询时间
     */
    private Date queryTime;

    /**
     * 报告时间
     */
    private Date reportTime;

    /**
     * 是否属于本人 1是 0否
     */
    private Integer status;

    public UserInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getMarriage() {
        return marriage;
    }

    public void setMarriage(Integer marriage) {
        this.marriage = marriage;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public Date getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(Date queryTime) {
        this.queryTime = queryTime;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
