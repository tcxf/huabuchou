package com.tcxf.hbc.ctu.common.util.data.housefund;

import com.alibaba.fastjson.serializer.ValueFilter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class DefaultValueFilter implements ValueFilter{

    private List<PlaceField> fieldList;

    private String placeValue;

    public DefaultValueFilter(List<PlaceField> fieldList,String placeValue) {
        this.fieldList = fieldList;
        this.placeValue = placeValue;
    }

    @Override
    public Object process(Object object, String name, Object value) {
        if(fieldList != null && fieldList.size() > 0){
            for (PlaceField placeField : fieldList) {
                if(StringUtils.equals(name,placeField.getName())&&value!=null){             //todo check
                    return placeField.getPlaceValue(value,placeValue);
                }
            }
        }
        return value;
    }


}
