package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HouseFundBaseInfoDTO implements Serializable{
    private static final long serialVersionUID = -7878188569839947839L;

    /** 姓名*/
    private String name;

    /** 手机号 --加密*/
    private String mobile;

    /** 邮箱*/
    private String email;

    /** 家庭住址 --加密*/
    private String homeAddress;

    /** 证件号(身份证号，护照号)  --加密*/
    private String cardNumber;

    /** 证件类型(idCard, passport) */
    private String cardType;

    /** 城市编号*/
    private String areaCode;

    /** 城市名称*/
    private String city;

    /** 客户号 --加密*/
    private String personnalNumber;

    /** 公积金账号  --加密*/
    private String houseFundNumber;

    /** 公积金查询密码 --加密*/
    private String houseFundPassword;

    /** 包含公积金余额跟补贴余额*/
    private BigDecimal balance;

    /** 公积金余额*/
    private BigDecimal fundBalance;

    /** 补贴公积金余额*/
    private BigDecimal subsidyBalance;

    /** 企业账户号码*/
    private String corporationNumber;

    /** 当前缴存企业名称*/
    private String corporationName;

    /** 企业月度缴存*/
    private BigDecimal monthlyCopraationIncome;

    /** 个人月度缴存*/
    private BigDecimal monthlyPersonalIncome;

    /** 补贴月缴存*/
    private BigDecimal subsidyIncome;

    /** 月度总缴存*/
    private BigDecimal monthlyTotalIncome;

    /** 企业缴存比例*/
    private BigDecimal corporationRatio;

    /** 个人缴存比例*/
    private BigDecimal personnalRatio;

    /** 补贴公积金公司缴存比例*/
    private BigDecimal subsidyCorporationRatio;

    /** 补贴公积金个人缴存比例*/
    private BigDecimal subsidyPersonalRatio;

    /** 缴存基数*/
    private BigDecimal baseNumber;

    /** 开户日期*/
    private Date beginDate;

    /** 是否有效，1有效，0无效*/
    private Short isValid;

    /** 是否本人，1本人，0非本人*/
    private Short status;

    /** */
    private Date createdAt;

    /** */
    private Date updatedAt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPersonnalNumber() {
        return personnalNumber;
    }

    public void setPersonnalNumber(String personnalNumber) {
        this.personnalNumber = personnalNumber;
    }

    public String getHouseFundNumber() {
        return houseFundNumber;
    }

    public void setHouseFundNumber(String houseFundNumber) {
        this.houseFundNumber = houseFundNumber;
    }

    public String getHouseFundPassword() {
        return houseFundPassword;
    }

    public void setHouseFundPassword(String houseFundPassword) {
        this.houseFundPassword = houseFundPassword;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getFundBalance() {
        return fundBalance;
    }

    public void setFundBalance(BigDecimal fundBalance) {
        this.fundBalance = fundBalance;
    }

    public BigDecimal getSubsidyBalance() {
        return subsidyBalance;
    }

    public void setSubsidyBalance(BigDecimal subsidyBalance) {
        this.subsidyBalance = subsidyBalance;
    }

    public String getCorporationNumber() {
        return corporationNumber;
    }

    public void setCorporationNumber(String corporationNumber) {
        this.corporationNumber = corporationNumber;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public BigDecimal getMonthlyCopraationIncome() {
        return monthlyCopraationIncome;
    }

    public void setMonthlyCopraationIncome(BigDecimal monthlyCopraationIncome) {
        this.monthlyCopraationIncome = monthlyCopraationIncome;
    }

    public BigDecimal getMonthlyPersonalIncome() {
        return monthlyPersonalIncome;
    }

    public void setMonthlyPersonalIncome(BigDecimal monthlyPersonalIncome) {
        this.monthlyPersonalIncome = monthlyPersonalIncome;
    }

    public BigDecimal getSubsidyIncome() {
        return subsidyIncome;
    }

    public void setSubsidyIncome(BigDecimal subsidyIncome) {
        this.subsidyIncome = subsidyIncome;
    }

    public BigDecimal getMonthlyTotalIncome() {
        return monthlyTotalIncome;
    }

    public void setMonthlyTotalIncome(BigDecimal monthlyTotalIncome) {
        this.monthlyTotalIncome = monthlyTotalIncome;
    }

    public BigDecimal getCorporationRatio() {
        return corporationRatio;
    }

    public void setCorporationRatio(BigDecimal corporationRatio) {
        this.corporationRatio = corporationRatio;
    }

    public BigDecimal getPersonnalRatio() {
        return personnalRatio;
    }

    public void setPersonnalRatio(BigDecimal personnalRatio) {
        this.personnalRatio = personnalRatio;
    }

    public BigDecimal getSubsidyCorporationRatio() {
        return subsidyCorporationRatio;
    }

    public void setSubsidyCorporationRatio(BigDecimal subsidyCorporationRatio) {
        this.subsidyCorporationRatio = subsidyCorporationRatio;
    }

    public BigDecimal getSubsidyPersonalRatio() {
        return subsidyPersonalRatio;
    }

    public void setSubsidyPersonalRatio(BigDecimal subsidyPersonalRatio) {
        this.subsidyPersonalRatio = subsidyPersonalRatio;
    }

    public BigDecimal getBaseNumber() {
        return baseNumber;
    }

    public void setBaseNumber(BigDecimal baseNumber) {
        this.baseNumber = baseNumber;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Short getValid() {
        return isValid;
    }

    public void setValid(Short valid) {
        isValid = valid;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
