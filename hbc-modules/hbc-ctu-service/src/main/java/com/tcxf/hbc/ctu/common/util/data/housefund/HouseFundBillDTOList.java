package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HouseFundBillDTOList implements Serializable{
    private static final long serialVersionUID = 7267473876926684956L;

    /** 出账*/
    private BigDecimal outcome;

    /** 入账*/
    private BigDecimal income;

    /** 补贴入账*/
    private BigDecimal subsidyIncome;

    /** 补贴出账*/
    private BigDecimal subsidyOutcome;

    /** 补缴*/
    private BigDecimal additionalIncome;

    /** 余额*/
    private BigDecimal balance;

    /** 缴存时间*/
    private Date billDate;

    /** 当前缴存企业名称*/
    private String corporationName;

    /** 企业缴存*/
    private BigDecimal monthlyCopraationIncome;

    /** 个人缴存*/
    private BigDecimal monthlyPersonalIncome;

    /** 总缴存*/
    private BigDecimal monthlyTotalIncome;

    /** 企业缴存比例*/
    private BigDecimal corporationRatio;

    /** 个人缴存比例*/
    private BigDecimal personnalRatio;

    /** 缴存描述信息(贷款发放，银行还款...)*/
    private String description;

    /** */
    private Date createdAt;

    /** */
    private Date updatedAt;

    public BigDecimal getOutcome() {
        return outcome;
    }

    public void setOutcome(BigDecimal outcome) {
        this.outcome = outcome;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getSubsidyIncome() {
        return subsidyIncome;
    }

    public void setSubsidyIncome(BigDecimal subsidyIncome) {
        this.subsidyIncome = subsidyIncome;
    }

    public BigDecimal getSubsidyOutcome() {
        return subsidyOutcome;
    }

    public void setSubsidyOutcome(BigDecimal subsidyOutcome) {
        this.subsidyOutcome = subsidyOutcome;
    }

    public BigDecimal getAdditionalIncome() {
        return additionalIncome;
    }

    public void setAdditionalIncome(BigDecimal additionalIncome) {
        this.additionalIncome = additionalIncome;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public BigDecimal getMonthlyCopraationIncome() {
        return monthlyCopraationIncome;
    }

    public void setMonthlyCopraationIncome(BigDecimal monthlyCopraationIncome) {
        this.monthlyCopraationIncome = monthlyCopraationIncome;
    }

    public BigDecimal getMonthlyPersonalIncome() {
        return monthlyPersonalIncome;
    }

    public void setMonthlyPersonalIncome(BigDecimal monthlyPersonalIncome) {
        this.monthlyPersonalIncome = monthlyPersonalIncome;
    }

    public BigDecimal getMonthlyTotalIncome() {
        return monthlyTotalIncome;
    }

    public void setMonthlyTotalIncome(BigDecimal monthlyTotalIncome) {
        this.monthlyTotalIncome = monthlyTotalIncome;
    }

    public BigDecimal getCorporationRatio() {
        return corporationRatio;
    }

    public void setCorporationRatio(BigDecimal corporationRatio) {
        this.corporationRatio = corporationRatio;
    }

    public BigDecimal getPersonnalRatio() {
        return personnalRatio;
    }

    public void setPersonnalRatio(BigDecimal personnalRatio) {
        this.personnalRatio = personnalRatio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
