package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;

public class HouseFundChannelDTO implements Serializable {
    private static final long serialVersionUID = 1266418713906926868L;

    private String channelCode;
    private String channelName;
    private String cityCode;
    private String channelDesc;
    private String groupName;
    private String status;

    public HouseFundChannelDTO() {
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getChannelDesc() {
        return channelDesc;
    }

    public void setChannelDesc(String channelDesc) {
        this.channelDesc = channelDesc;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
