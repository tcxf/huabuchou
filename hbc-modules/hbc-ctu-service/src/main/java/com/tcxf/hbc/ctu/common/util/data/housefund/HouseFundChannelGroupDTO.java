package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.util.List;

public class HouseFundChannelGroupDTO implements Serializable {
    private static final long serialVersionUID = -372603960161304307L;

    private String groupName;
    private List<HouseFundChannelDTO> channelDTOS;

    public HouseFundChannelGroupDTO() {
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<HouseFundChannelDTO> getChannelDTOS() {
        return channelDTOS;
    }

    public void setChannelDTOS(List<HouseFundChannelDTO> channelDTOS) {
        this.channelDTOS = channelDTOS;
    }
}
