package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HouseFundLoanInfoDTOList implements Serializable{
    private static final long serialVersionUID = 1266418713906926868L;

    /** 贷款合同号*/
    private String contractNumber;

    /** 客户号 --加密*/
    private String personnalNumber;

    /** 公积金账号  --加密*/
    private String houseFundNumber;

    /** 贷款人姓名 --加密*/
    private String name;

    /** 贷款人手机号 --加密*/
    private String mobile;

    /** 贷款状态*/
    private String status;

    /** 承办银行*/
    private String bank;

    /** 贷款类型(公积金贷款、商业贷款、组合贷款、其他..)*/
    private Boolean loanType;

    /** 贷款人身份证号 ---加密*/
    private String idCard;

    /** 贷款买房地址 ---加密*/
    private String houseAddress;

    /** 通讯地址 ---加密*/
    private String mailingAddress;

    /** 贷款期数*/
    private Integer periods;

    /** 贷款金额*/
    private BigDecimal loanAmount;

    /** 月度还款金额*/
    private BigDecimal monthlyRepayAmount;

    /** 贷款开始时间*/
    private Date startDate;

    /** 贷款结束时间*/
    private Date endDate;

    /** 还款类型 1 等额本金 2等额本息*/
    private String repayType;

    /** 每月还款日期*/
    private Integer deductDay;

    /** 扣款账号*/
    private String bankAccount;

    /** 扣款账号姓名 --加密*/
    private String bankAccountName;

    /** 贷款利率*/
    private BigDecimal loanInterestPercent;

    /** 罚息利率*/
    private BigDecimal penaltyInterestPercent;

    /** 商业贷款合同号*/
    private String commercialContractNumber;

    /** 商业贷款银行*/
    private String commercialBank;

    /** 商业贷款金额*/
    private BigDecimal commercialAmount;

    /** 第二还款人银行账号*/
    private String secondBankAccount;

    /** 第二还款人银行账号姓名*/
    private String secondBankAccountName;

    /** 第二还款人身份证号  --加密*/
    private String secondIdCard;

    /** 第二还款人手机号 --加密*/
    private String secondMobile;

    /** 第二还款人工作单位*/
    private String secondCorporationName;

    /** 贷款余额*/
    private BigDecimal remainAmount;

    /** 剩余期数*/
    private Boolean remainPeriods;

    /** 最后还款日期*/
    private Date lastRepayDate;

    /** 逾期本金*/
    private BigDecimal overdueCapital;

    /** 逾期利息*/
    private BigDecimal overdueInterest;

    /** 逾期罚息*/
    private BigDecimal overduePenalty;

    /** 逾期天数*/
    private Long overdueDays;

    /** 以往逾期次数*/
    private Integer overdueTimes;

    /** */
    private Date createdAt;

    /** */
    private Date updatedAt;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getPersonnalNumber() {
        return personnalNumber;
    }

    public void setPersonnalNumber(String personnalNumber) {
        this.personnalNumber = personnalNumber;
    }

    public String getHouseFundNumber() {
        return houseFundNumber;
    }

    public void setHouseFundNumber(String houseFundNumber) {
        this.houseFundNumber = houseFundNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Boolean getLoanType() {
        return loanType;
    }

    public void setLoanType(Boolean loanType) {
        this.loanType = loanType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public Integer getPeriods() {
        return periods;
    }

    public void setPeriods(Integer periods) {
        this.periods = periods;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public BigDecimal getMonthlyRepayAmount() {
        return monthlyRepayAmount;
    }

    public void setMonthlyRepayAmount(BigDecimal monthlyRepayAmount) {
        this.monthlyRepayAmount = monthlyRepayAmount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public Integer getDeductDay() {
        return deductDay;
    }

    public void setDeductDay(Integer deductDay) {
        this.deductDay = deductDay;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public BigDecimal getLoanInterestPercent() {
        return loanInterestPercent;
    }

    public void setLoanInterestPercent(BigDecimal loanInterestPercent) {
        this.loanInterestPercent = loanInterestPercent;
    }

    public BigDecimal getPenaltyInterestPercent() {
        return penaltyInterestPercent;
    }

    public void setPenaltyInterestPercent(BigDecimal penaltyInterestPercent) {
        this.penaltyInterestPercent = penaltyInterestPercent;
    }

    public String getCommercialContractNumber() {
        return commercialContractNumber;
    }

    public void setCommercialContractNumber(String commercialContractNumber) {
        this.commercialContractNumber = commercialContractNumber;
    }

    public String getCommercialBank() {
        return commercialBank;
    }

    public void setCommercialBank(String commercialBank) {
        this.commercialBank = commercialBank;
    }

    public BigDecimal getCommercialAmount() {
        return commercialAmount;
    }

    public void setCommercialAmount(BigDecimal commercialAmount) {
        this.commercialAmount = commercialAmount;
    }

    public String getSecondBankAccount() {
        return secondBankAccount;
    }

    public void setSecondBankAccount(String secondBankAccount) {
        this.secondBankAccount = secondBankAccount;
    }

    public String getSecondBankAccountName() {
        return secondBankAccountName;
    }

    public void setSecondBankAccountName(String secondBankAccountName) {
        this.secondBankAccountName = secondBankAccountName;
    }

    public String getSecondIdCard() {
        return secondIdCard;
    }

    public void setSecondIdCard(String secondIdCard) {
        this.secondIdCard = secondIdCard;
    }

    public String getSecondMobile() {
        return secondMobile;
    }

    public void setSecondMobile(String secondMobile) {
        this.secondMobile = secondMobile;
    }

    public String getSecondCorporationName() {
        return secondCorporationName;
    }

    public void setSecondCorporationName(String secondCorporationName) {
        this.secondCorporationName = secondCorporationName;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public Boolean getRemainPeriods() {
        return remainPeriods;
    }

    public void setRemainPeriods(Boolean remainPeriods) {
        this.remainPeriods = remainPeriods;
    }

    public Date getLastRepayDate() {
        return lastRepayDate;
    }

    public void setLastRepayDate(Date lastRepayDate) {
        this.lastRepayDate = lastRepayDate;
    }

    public BigDecimal getOverdueCapital() {
        return overdueCapital;
    }

    public void setOverdueCapital(BigDecimal overdueCapital) {
        this.overdueCapital = overdueCapital;
    }

    public BigDecimal getOverdueInterest() {
        return overdueInterest;
    }

    public void setOverdueInterest(BigDecimal overdueInterest) {
        this.overdueInterest = overdueInterest;
    }

    public BigDecimal getOverduePenalty() {
        return overduePenalty;
    }

    public void setOverduePenalty(BigDecimal overduePenalty) {
        this.overduePenalty = overduePenalty;
    }

    public Long getOverdueDays() {
        return overdueDays;
    }

    public void setOverdueDays(Long overdueDays) {
        this.overdueDays = overdueDays;
    }

    public Integer getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(Integer overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
