package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HouseFundLoanRepaymentDTOList implements Serializable{
    private static final long serialVersionUID = 3450900753694855515L;
    
    /** 自增ID*/
    private Integer id;

    /** 用户ID*/
    private Integer userId;

    /** t_housefund_base_info ID*/
    private Integer baseinfoId;

    /** t_housefund_loan_info ID*/
    private Integer loaninfoId;

    /** 每月还款金额*/
    private BigDecimal repaymentAmount;

    /** 还款日期*/
    private Date repayDate;

    /** 记账日期*/
    private Date accountingDate;

    /** 还款本金*/
    private BigDecimal repayCapital;

    /** 还款利息*/
    private BigDecimal repayInterest;

    /** 还款罚息*/
    private BigDecimal repayPenalty;

    /** 本金余额*/
    private BigDecimal loanLeftAmount;

    /** 摘要*/
    private String msg;

    /** */
    private Date createdAt;

    /** */
    private Date updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBaseinfoId() {
        return baseinfoId;
    }

    public void setBaseinfoId(Integer baseinfoId) {
        this.baseinfoId = baseinfoId;
    }

    public Integer getLoaninfoId() {
        return loaninfoId;
    }

    public void setLoaninfoId(Integer loaninfoId) {
        this.loaninfoId = loaninfoId;
    }

    public BigDecimal getRepaymentAmount() {
        return repaymentAmount;
    }

    public void setRepaymentAmount(BigDecimal repaymentAmount) {
        this.repaymentAmount = repaymentAmount;
    }

    public Date getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(Date repayDate) {
        this.repayDate = repayDate;
    }

    public Date getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Date accountingDate) {
        this.accountingDate = accountingDate;
    }

    public BigDecimal getRepayCapital() {
        return repayCapital;
    }

    public void setRepayCapital(BigDecimal repayCapital) {
        this.repayCapital = repayCapital;
    }

    public BigDecimal getRepayInterest() {
        return repayInterest;
    }

    public void setRepayInterest(BigDecimal repayInterest) {
        this.repayInterest = repayInterest;
    }

    public BigDecimal getRepayPenalty() {
        return repayPenalty;
    }

    public void setRepayPenalty(BigDecimal repayPenalty) {
        this.repayPenalty = repayPenalty;
    }

    public BigDecimal getLoanLeftAmount() {
        return loanLeftAmount;
    }

    public void setLoanLeftAmount(BigDecimal loanLeftAmount) {
        this.loanLeftAmount = loanLeftAmount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
