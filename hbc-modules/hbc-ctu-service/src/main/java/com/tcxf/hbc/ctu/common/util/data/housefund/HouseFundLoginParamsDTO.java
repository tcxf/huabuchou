package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.util.HashMap;

public class HouseFundLoginParamsDTO implements Serializable {
    private static final long serialVersionUID = 7267473876926684956L;

    private String channelCode;
    private HashMap<String, String> formParams;
    private HashMap<String, String> extParams;

    public HouseFundLoginParamsDTO() {
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public HashMap<String, String> getFormParams() {
        return formParams;
    }

    public void setFormParams(HashMap<String, String> formParams) {
        this.formParams = formParams;
    }

    public HashMap<String, String> getExtParams() {
        return extParams;
    }

    public void setExtParams(HashMap<String, String> extParams) {
        this.extParams = extParams;
    }
}
