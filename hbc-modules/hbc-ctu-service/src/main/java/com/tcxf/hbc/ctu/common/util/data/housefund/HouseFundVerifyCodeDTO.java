package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Shayne
 * @description
 * @create 2018-05-30 下午6:53
 **/
public class HouseFundVerifyCodeDTO implements Serializable{
    private static final long serialVersionUID = 7172095700516817623L;

    private String type;
    private String code;
    private HashMap<String, String> formParams;

    public HouseFundVerifyCodeDTO(String type, String code, HashMap<String, String> formParams) {
        this.type = type;
        this.code = code;
        this.formParams = formParams;
    }

    public HouseFundVerifyCodeDTO() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HashMap<String, String> getFormParams() {
        return formParams;
    }

    public void setFormParams(HashMap<String, String> formParams) {
        this.formParams = formParams;
    }
}
