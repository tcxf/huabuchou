package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JsonPlaceHolder {

    boolean need() default true;

    int type() default 1;

}