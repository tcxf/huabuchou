package com.tcxf.hbc.ctu.common.util.data.housefund;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class JsonUtil {

    public static final String toJSONString(Object object,String placeValue) {
        List<PlaceField> fields = getFields(object.getClass());
        return JSON.toJSONString(object,new DefaultValueFilter(fields,placeValue));
    }

    private static List<PlaceField> getFields(Class<?> type){
        List<PlaceField> fields = new ArrayList<>();
        Field[] allMethods = type.getDeclaredFields();
        for (Field field : allMethods) {
            JsonPlaceHolder holder = field.getAnnotation(JsonPlaceHolder.class);
            if(null != holder){
                if(holder.need()){
                    PlaceField pf = new PlaceField(field.getName(),holder.type());
                    fields.add(pf);
                }
                if (field.getGenericType() != null
                        && field.getGenericType() instanceof ParameterizedType) {
                    ParameterizedType pt = ((ParameterizedType) field.getGenericType());
                    Class clz = (Class) pt.getActualTypeArguments()[0];
                    fields.addAll(getFields(clz));
                }else{
                    fields.addAll(getFields(field.getType()));

                }
            }
        }
        return fields;
    }

}
