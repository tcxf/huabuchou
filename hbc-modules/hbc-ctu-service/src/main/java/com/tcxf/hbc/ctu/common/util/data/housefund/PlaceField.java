package com.tcxf.hbc.ctu.common.util.data.housefund;

public class PlaceField {
    private String name;
    /**
     * 1在前面加，2在后面加，3覆盖
     */
    private int type;

    public PlaceField(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getPlaceValue(Object oriValue,String placeValue){
        if(type == 1){
            return placeValue + oriValue;
        }else if(type == 2){
            return oriValue + placeValue;
        }else if(type == 3){
            return placeValue;
        }else{
            return oriValue + "";
        }
    }
}
