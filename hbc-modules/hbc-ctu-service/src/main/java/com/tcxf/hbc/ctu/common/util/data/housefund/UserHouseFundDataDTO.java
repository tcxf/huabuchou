package com.tcxf.hbc.ctu.common.util.data.housefund;

import java.io.Serializable;
import java.util.List;

public class UserHouseFundDataDTO implements Serializable{
    private static final long serialVersionUID = 7285459614450770076L;

    private HouseFundBaseInfoDTO houseFundBaseInfoDTO;

    @JsonPlaceHolder(need = false) private List<HouseFundBillDTOList> houseFundBillDTOList;
    @JsonPlaceHolder(need = false) private List<HouseFundLoanInfoDTOList> houseFundLoanInfoDTOList;
    @JsonPlaceHolder(need = false) private List<HouseFundLoanRepaymentDTOList> houseFundLoanRepaymentDTOList;

    public HouseFundBaseInfoDTO getHouseFundBaseInfoDTO() {
        return houseFundBaseInfoDTO;
    }

    public void setHouseFundBaseInfoDTO(HouseFundBaseInfoDTO houseFundBaseInfoDTO) {
        this.houseFundBaseInfoDTO = houseFundBaseInfoDTO;
    }

    public List<HouseFundBillDTOList> getHouseFundBillDTOList() {
        return houseFundBillDTOList;
    }

    public void setHouseFundBillDTOList(List<HouseFundBillDTOList> houseFundBillDTOListList) {
        this.houseFundBillDTOList = houseFundBillDTOListList;
    }

    public List<HouseFundLoanInfoDTOList> getHouseFundLoanInfoDTOList() {
        return houseFundLoanInfoDTOList;
    }

    public void setHouseFundLoanInfoDTOList(List<HouseFundLoanInfoDTOList> houseFundLoanInfoDTOList) {
        this.houseFundLoanInfoDTOList = houseFundLoanInfoDTOList;
    }

    public List<HouseFundLoanRepaymentDTOList> getHouseFundLoanRepaymentDTOList() {
        return houseFundLoanRepaymentDTOList;
    }

    public void setHouseFundLoanRepaymentDTOList(List<HouseFundLoanRepaymentDTOList> houseFundLoanRepaymentDTOList) {
        this.houseFundLoanRepaymentDTOList = houseFundLoanRepaymentDTOList;
    }
}
