package com.tcxf.hbc.ctu.common.util.data.operatorReport;

import java.io.Serializable;

public class CreditPlatformRegistrationDetails implements Serializable {
    private String code;
    private String time;

    public String getCode() {
        return code;
    }

    public String getTime() {
        return time;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
