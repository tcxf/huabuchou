package com.tcxf.hbc.ctu.common.util.data.operatorReport;

import java.io.Serializable;

public class OverduePlatformDetails implements Serializable {

    private String code ;
    private String counts ;
    private String money ;
    private String time ;

    public void setCode(String code) {
        this.code = code;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCode() {
        return code;
    }

    public String getCounts() {
        return counts;
    }

    public String getMoney() {
        return money;
    }

    public String getTime() {
        return time;
    }
}
