package com.tcxf.hbc.ctu.common.util.data.shebao;

import com.alibaba.fastjson.annotation.JSONType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenhzihui
 */
@JSONType(ignores = {"id", "userId"})
public class BaseInfo implements Serializable {

    private static final long serialVersionUID = 1682549786025456217L;
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户Id 关联商户下的授权人员
     */
    private Long userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 登录账号，对于同一个人不变
     */
    private String loginAccount;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 个人编号(社保编号)
     */
    private String personalNo;

    /**
     * 性别，0未知 1男 2女
     */
    private Integer gender;

    /**
     * 民族
     */
    private String nation;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 手机号/电话
     */
    private String phone;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 文化水平 0其他 1小学 2初中 3高中 4专科 5本科 6硕士 7博士 8博士后
     */
    private Integer educationLevel;

    /**
     * 通讯地址
     */
    private String address;

    /**
     * 户口性质 0其他 1省内居民户口 2省内农村户口 3省外居民户口 4省外农村户口
     */
    private Integer householdType;

    /**
     * 户口所在地地址
     */
    private String householdAddress;

    /**
     * 工作日期/参加工作日期
     */
    private Date workDate;

    /**
     * 工作状态 0未知 1在职 2离职 3退休
     */
    private Integer workStatus;

    /**
     * 退休日期
     */
    private Date retireDate;

    /**
     * 当前参保单位
     */
    private String company;

    /**
     * 参保单位编号
     */
    private String companyNo;

    /**
     * 单位类型 0未知 1国企 2私企 3事业单位
     */
    private Integer companyType;

    /**
     * 社保基数（养老）
     */
    private BigDecimal baseAmount;

    /**
     * 首次缴费时间
     */
    private Date firstPayDate;

    /**
     * 最近一次缴费时间
     */
    private Date lastPayDate;

    /**
     * 社保状态 0未知 1正常参保 2停止参保
     */
    private Integer status;

    private Integer isSelf;

    private Date lastUpdateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPersonalNo() {
        return personalNo;
    }

    public void setPersonalNo(String personalNo) {
        this.personalNo = personalNo;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(Integer educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getHouseholdType() {
        return householdType;
    }

    public void setHouseholdType(Integer householdType) {
        this.householdType = householdType;
    }

    public String getHouseholdAddress() {
        return householdAddress;
    }

    public void setHouseholdAddress(String householdAddress) {
        this.householdAddress = householdAddress;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Integer getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(Integer workStatus) {
        this.workStatus = workStatus;
    }

    public Date getRetireDate() {
        return retireDate;
    }

    public void setRetireDate(Date retireDate) {
        this.retireDate = retireDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public Integer getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public Date getFirstPayDate() {
        return firstPayDate;
    }

    public void setFirstPayDate(Date firstPayDate) {
        this.firstPayDate = firstPayDate;
    }

    public Date getLastPayDate() {
        return lastPayDate;
    }

    public void setLastPayDate(Date lastPayDate) {
        this.lastPayDate = lastPayDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Integer getIsSelf() {
        return isSelf;
    }

    public void setIsSelf(Integer isSelf) {
        this.isSelf = isSelf;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BaseInfoDTO{");
        sb.append("id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", loginAccount='").append(loginAccount).append('\'');
        sb.append(", idCard='").append(idCard).append('\'');
        sb.append(", personalNo='").append(personalNo).append('\'');
        sb.append(", gender=").append(gender);
        sb.append(", nation='").append(nation).append('\'');
        sb.append(", birthday=").append(birthday);
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", educationLevel=").append(educationLevel);
        sb.append(", address='").append(address).append('\'');
        sb.append(", householdType=").append(householdType);
        sb.append(", householdAddress='").append(householdAddress).append('\'');
        sb.append(", workDate=").append(workDate);
        sb.append(", workStatus=").append(workStatus);
        sb.append(", retireDate=").append(retireDate);
        sb.append(", company='").append(company).append('\'');
        sb.append(", companyNo='").append(companyNo).append('\'');
        sb.append(", companyType=").append(companyType);
        sb.append(", baseAmount=").append(baseAmount);
        sb.append(", firstPayDate=").append(firstPayDate);
        sb.append(", lastPayDate=").append(lastPayDate);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
