package com.tcxf.hbc.ctu.common.util.data.shebao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Shayne
 * @description
 * @create 2018-05-04 下午12:11
 **/
public class BillRecordDTOS implements Serializable{
    private static final long serialVersionUID = 8423684934786752863L;

    /**
     * 保险类型
     */
    private Integer insuranceType;

    /**
     * 费用名称
     */
    private String paymentName;

    /**
     * 单位名称
     */
    private String company;

    /**
     * 缴费基数
     */
    private BigDecimal baseAmount;

    /**
     * 单位缴费
     */
    private BigDecimal companyPay;

    /**
     * 个人缴费
     */
    private BigDecimal personalPay;

    /**
     * 单位缴费比例
     */
    private Float companyPercent;

    /**
     * 个人缴费比例
     */
    private Float personalPercent;

    /**
     * 缴费周期 如201705
     */
    private Date period;

    /**
     * 缴费日期
     */
    private Date payDate;

    /**
     * 缴费状态 0未知 1已实缴 2未交费 3未到账
     */
    private Integer payStatus;

    /**
     * 缴费类型 0未知 1正常缴费 2补缴
     */
    private Integer payType;

    private String remark;

    public Integer getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(Integer insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getCompanyPay() {
        return companyPay;
    }

    public void setCompanyPay(BigDecimal companyPay) {
        this.companyPay = companyPay;
    }

    public BigDecimal getPersonalPay() {
        return personalPay;
    }

    public void setPersonalPay(BigDecimal personalPay) {
        this.personalPay = personalPay;
    }

    public Float getCompanyPercent() {
        return companyPercent;
    }

    public void setCompanyPercent(Float companyPercent) {
        this.companyPercent = companyPercent;
    }

    public Float getPersonalPercent() {
        return personalPercent;
    }

    public void setPersonalPercent(Float personalPercent) {
        this.personalPercent = personalPercent;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
