package com.tcxf.hbc.ctu.common.util.data.shebao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Shayne
 * @description
 * @create 2018-05-04 下午12:13
 **/
public class InsuranceDTOS implements Serializable{
    private static final long serialVersionUID = -2219274517731112638L;

    /**
     * 保险类型
     * 1工伤保险 2生育保险 3医疗保险 4养老保险 5失业保险 6大病保险 0其他保险
     */
    private Integer insuranceType;

    /**
     * 当前参保公司名称
     */
    private String company;

    /**
     * 上年缴存金额
     */
    private BigDecimal lastyearBalanceAmount;

    /**
     * 当年支付金额
     */
    private BigDecimal thisyearPayAmount;

    /**
     * 累计缴存金额
     */
    private BigDecimal totalDepositAmount;
    /**
     * 累计支付金额
     */
    private BigDecimal totalPayAmount;

    /**
     * 当年缴存月份数
     */
    private Integer thisyearDepositMonth;
    /**
     * 连续缴存月份数
     */
    private Integer continuousDepositMonth;

    /**
     * 累计缴存月份数
     */
    private Integer paymentMonth;
    /**
     * 首次参保日期
     */
    private Date firstInsureDate;
    /**
     * 本次参保日期
     */
    private Date lastInsureDate;
    /**
     * 当前参保基数
     */
    private BigDecimal baseAmount;
    /**
     * 当前公司缴费
     */
    private BigDecimal companyPay;
    /**
     * 当前个人缴费
     */
    private BigDecimal personalPay;
    /**
     * 当前公司缴费比例 百分比如12.0
     */
    private float companyPercent;
    /**
     * 当前公司缴费比例 百分比如12.0
     */
    private float personalPercent;
    /**
     * 当前参保状态，0未知 1正常参保 2暂停参保 3注销
     */
    private Integer status;


    public Integer getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(Integer insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public BigDecimal getLastyearBalanceAmount() {
        return lastyearBalanceAmount;
    }

    public void setLastyearBalanceAmount(BigDecimal lastyearBalanceAmount) {
        this.lastyearBalanceAmount = lastyearBalanceAmount;
    }

    public BigDecimal getThisyearPayAmount() {
        return thisyearPayAmount;
    }

    public void setThisyearPayAmount(BigDecimal thisyearPayAmount) {
        this.thisyearPayAmount = thisyearPayAmount;
    }

    public BigDecimal getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public BigDecimal getTotalPayAmount() {
        return totalPayAmount;
    }

    public void setTotalPayAmount(BigDecimal totalPayAmount) {
        this.totalPayAmount = totalPayAmount;
    }

    public Integer getThisyearDepositMonth() {
        return thisyearDepositMonth;
    }

    public void setThisyearDepositMonth(Integer thisyearDepositMonth) {
        this.thisyearDepositMonth = thisyearDepositMonth;
    }

    public Integer getContinuousDepositMonth() {
        return continuousDepositMonth;
    }

    public void setContinuousDepositMonth(Integer continuousDepositMonth) {
        this.continuousDepositMonth = continuousDepositMonth;
    }

    public Integer getPaymentMonth() {
        return paymentMonth;
    }

    public void setPaymentMonth(Integer paymentMonth) {
        this.paymentMonth = paymentMonth;
    }

    public Date getFirstInsureDate() {
        return firstInsureDate;
    }

    public void setFirstInsureDate(Date firstInsureDate) {
        this.firstInsureDate = firstInsureDate;
    }

    public Date getLastInsureDate() {
        return lastInsureDate;
    }

    public void setLastInsureDate(Date lastInsureDate) {
        this.lastInsureDate = lastInsureDate;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getCompanyPay() {
        return companyPay;
    }

    public void setCompanyPay(BigDecimal companyPay) {
        this.companyPay = companyPay;
    }

    public BigDecimal getPersonalPay() {
        return personalPay;
    }

    public void setPersonalPay(BigDecimal personalPay) {
        this.personalPay = personalPay;
    }

    public float getCompanyPercent() {
        return companyPercent;
    }

    public void setCompanyPercent(float companyPercent) {
        this.companyPercent = companyPercent;
    }

    public float getPersonalPercent() {
        return personalPercent;
    }

    public void setPersonalPercent(float personalPercent) {
        this.personalPercent = personalPercent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
