package com.tcxf.hbc.ctu.common.util.data.shebao;

import com.alibaba.fastjson.annotation.JSONType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenzhihui
 */
@JSONType(ignores = {"id", "userId", "baseInfoId"})
public class MedicalConsumeRecords implements Serializable {

    private static final long serialVersionUID = -1863514615869455926L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * t_security_base_info对应主键
     */
    private Long baseInfoId;

    /**
     * 费用名称
     */
    private String paymentName;

    /**
     * 医疗类别 0其他 1门诊 2药店 3住院
     */
    private Integer type;

    /**
     * 医疗机构名称
     */
    private String orgName;

    /**
     * 总费用
     */
    private BigDecimal totalCost;

    /**
     * 医保支付
     */
    private BigDecimal medicalPay;

    /**
     * 个人支付
     */
    private BigDecimal personalPay;

    /**
     * 个人支付
     */
    private String remark;

    /**
     * 结算时间
     */
    private Date payDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBaseInfoId() {
        return baseInfoId;
    }

    public void setBaseInfoId(Long baseInfoId) {
        this.baseInfoId = baseInfoId;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getMedicalPay() {
        return medicalPay;
    }

    public void setMedicalPay(BigDecimal medicalPay) {
        this.medicalPay = medicalPay;
    }

    public BigDecimal getPersonalPay() {
        return personalPay;
    }

    public void setPersonalPay(BigDecimal personalPay) {
        this.personalPay = personalPay;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MedicalConsumeRecordDTO{");
        sb.append("id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", baseInfoId=").append(baseInfoId);
        sb.append(", paymentName='").append(paymentName).append('\'');
        sb.append(", type=").append(type);
        sb.append(", orgName='").append(orgName).append('\'');
        sb.append(", totalCost=").append(totalCost);
        sb.append(", medicalPay=").append(medicalPay);
        sb.append(", personalPay=").append(personalPay);
        sb.append(", payDate=").append(payDate);
        sb.append(", remark=").append(remark);
        sb.append('}');
        return sb.toString();
    }
}
