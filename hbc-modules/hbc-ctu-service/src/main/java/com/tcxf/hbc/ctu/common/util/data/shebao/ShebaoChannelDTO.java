package com.tcxf.hbc.ctu.common.util.data.shebao;

import java.io.Serializable;

/**
 * @author Shayne
 * @description
 * @create 2018-04-27 下午9:52
 **/
public class ShebaoChannelDTO implements Serializable{
    private static final long serialVersionUID = -2696441430271753486L;

    private String channelCode;
    private String channelName;
    private String cityCode;
    private String channelDesc;
    private String groupName;
    private String status;

    public ShebaoChannelDTO() {
    }

    public ShebaoChannelDTO(String channelCode, String channelName, String cityCode, String channelDesc, String groupName, String status) {
        this.channelCode = channelCode;
        this.channelName = channelName;
        this.cityCode = cityCode;
        this.channelDesc = channelDesc;
        this.groupName = groupName;
        this.status = status;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getChannelDesc() {
        return channelDesc;
    }

    public void setChannelDesc(String channelDesc) {
        this.channelDesc = channelDesc;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
