package com.tcxf.hbc.ctu.common.util.data.shebao;

import java.io.Serializable;
import java.util.List;

/**
 * @author Shayne
 * @description
 * @create 2018-04-28 下午2:05
 **/
public class ShebaoChannelGroupDTO implements Serializable{
    private static final long serialVersionUID = -372603960161304307L;

    private String groupName;
    private List<ShebaoChannelDTO> channelDTOS;

    public ShebaoChannelGroupDTO() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<ShebaoChannelDTO> getChannelDTOS() {
        return channelDTOS;
    }

    public void setChannelDTOS(List<ShebaoChannelDTO> channelDTOS) {
        this.channelDTOS = channelDTOS;
    }
}
