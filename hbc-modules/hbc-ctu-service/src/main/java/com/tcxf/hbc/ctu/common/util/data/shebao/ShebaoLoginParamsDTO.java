package com.tcxf.hbc.ctu.common.util.data.shebao;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Shayne
 * @description
 * @create 2018-04-27 下午9:54
 **/
public class ShebaoLoginParamsDTO implements Serializable{
    private static final long serialVersionUID = 2563395349778690618L;

    private String channelCode;
    private HashMap<String, String> formParams;
    private HashMap<String, String> extParams;

    public ShebaoLoginParamsDTO() {
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public HashMap<String, String> getFormParams() {
        return formParams;
    }

    public void setFormParams(HashMap<String, String> formParams) {
        this.formParams = formParams;
    }

    public HashMap<String, String> getExtParams() {
        return extParams;
    }

    public void setExtParams(HashMap<String, String> extParams) {
        this.extParams = extParams;
    }
}
