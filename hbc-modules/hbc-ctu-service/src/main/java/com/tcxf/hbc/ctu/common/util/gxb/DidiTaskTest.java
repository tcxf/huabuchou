package com.tcxf.hbc.ctu.common.util.gxb;

import com.tcxf.hbc.ctu.common.util.api.OperatorApi;
import com.tcxf.hbc.ctu.common.util.parm.AuthParm;
import com.tcxf.hbc.ctu.common.util.parm.CodeSubmitRequest;
import com.tcxf.hbc.ctu.common.util.parm.LoginRequest;
import com.tcxf.hbc.ctu.common.util.parm.Status;

import java.io.IOException;
import java.util.UUID;

/**
 * @program: Workspace2
 * @description: 滴滴打车
 * @author: JinPeng
 * @create: 2018-08-07 11:56
 **/
public class DidiTaskTest extends AbstractGxbTest {

    OperatorApi operatorApi = gxbApiFactory.newApi(OperatorApi.class);


    //姓名
    public String name;

    //手机号
    public String phone;

    //身份证
    public String idcard;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    @Override
    protected AuthParm getAuthParm() {
        String sequenceNo = UUID.randomUUID().toString().replace("-", "");
        return new AuthParm(sequenceNo, "diditaxi", System.currentTimeMillis(), name, phone, idcard);
    }

    @Override
    protected Status refreshLoginQrCode(String token) throws IOException {
        return null;
    }

    @Override
    protected Status refreshLoginPicCode(String token) throws IOException {
        return operatorApi.refreshLoginPicCode(token).execute().body().getData();
    }

    @Override
    protected Status refreshLoginSmsCode(String token) throws IOException {
        return operatorApi.refreshLoginSmsCode(token).execute().body().getData();
    }

    @Override
    protected Status submitLogin(String token, LoginRequest loginRequest) throws IOException {
        return operatorApi.submitLogin(token, loginRequest).execute().body().getData();
    }

    @Override
    protected Status getStatus(String token) throws IOException {
        return operatorApi.getStatus(token).execute().body().getData();
    }

    @Override
    protected Status submitVarifyCode(String token, String code) throws IOException {
        return operatorApi.submitCode(token, new CodeSubmitRequest(code)).execute().body().getData();
    }
}
