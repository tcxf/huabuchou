/**
 * This document and its contents are protected by copyright 2017 and owned by gxb.io Inc. The
 * copying and reproduction of this document and/or its content (whether wholly or partly) or any
 * incorporation of the same into any other material in any media or format of any kind is strictly
 * prohibited. All rights are reserved.
 *
 * Copyright (c) gxb.io Inc. 2017
 */
package com.tcxf.hbc.ctu.common.util.gxb;

import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.ctu.common.constant.CreditConstant;
import com.tcxf.hbc.ctu.common.util.api.AuthApi;
import com.tcxf.hbc.ctu.common.util.api.OperatorApi;
import com.tcxf.hbc.ctu.common.util.parm.*;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginConfig;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginField;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginForm;
import com.tcxf.hbc.ctu.common.util.util.GsonUtil;
import com.tcxf.hbc.ctu.service.IBaseCreditConfigService;
import com.tcxf.hbc.ctu.service.IBaseInterfaceInfoService;
import com.tcxf.hbc.ctu.service.impl.BaseInterfaceInfoServiceImpl;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import retrofit2.Response;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Created by yaojun on 2017/7/5.
 */
public class OperatorTaskTest extends AbstractGxbTest {
    private static final Logger logger = LoggerFactory.getLogger(OperatorTaskTest.class);

    @Autowired
    private IBaseCreditConfigService baseCreditConfigService;

    @Autowired
    private IBaseInterfaceInfoService iBaseInterfaceInfoService;

    //姓名
    public String name;

    //手机号
    public String phone;

    //身份证
    public String idcard;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }


    OperatorApi operatorApi = gxbApiFactory.newApi(OperatorApi.class);

    OperatorApi operatorApiData = gxbApiDataFactory.newApi(OperatorApi.class);
    @Override
    protected AuthParm getAuthParm() {
        String sequenceNo = UUID.randomUUID().toString().replace("-", "");
        return new AuthParm(sequenceNo, "operator_pro", System.currentTimeMillis(), name, phone, idcard);
    }




    public HashMap<String,Object> doOperatorTask() throws Exception {
        HashMap<String,Object> map = new HashMap<String,Object>();
        // step1:生成token
        final AuthToken authToken = getAuthToken();
        // step2:获取运营商授权初始化配置，需要手机号
        LoginConfig loginConfig = operatorApi.getOperatorInitConf(authToken.getToken(), "18073615537").execute().body().getData();
        // step3：前端页面更加loginConfig动态的渲染页面，理论上本页面配置不会经常变更
        logger.info("已成功获取授权项{}-{}的登录初始化配置，最近更新时间{}", loginConfig.getAuthItem(), loginConfig.getAuthName(), loginConfig.getLastUpdatedAt());
        logger.info("当前网站支持{}种授权登录方式", loginConfig.getLoginForms().size());
        // 此处默认选择第一种方式作为测试用例
        LoginForm testLoginForm = loginConfig.getLoginForms().get(0);
        logger.info("本用例采用{}登录模式作为测试", testLoginForm.getFormName());
        // step4:loginForm 状态监测,验证当前的login模式是否处于正常服务状态
        map.put("token",authToken);
        map.put("loginForm",testLoginForm);
        return map;
    }

    public boolean loginStatusCheck(LoginForm loginForm) {
        boolean isCheck = false;
        try
        {
            switch (loginForm.getStatus()) {
                case NORMAL:
                    logger.info("form状态验证成功，{}登录能正常使用,登录模式为{}", loginForm.getFormName(), loginForm.getLoginFormType());
                    isCheck= true;
                    return isCheck;
                case MAINTAINED:
                    logger.warn("form状态验证失败，{}登录正在维护", loginForm.getFormName());
                    isCheck= false;
                    return isCheck;
                case OFFLINE:
                    logger.warn("form状态验证失败，{}登录已下线", loginForm.getFormName());
                    isCheck= false;
                    return isCheck;
                default:
                    logger.warn("form状态验证失败，{}未知form状态", loginForm.getFormName());
                    isCheck= false;
                    return isCheck;
            }
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
        }
        return isCheck;
    }
    public HashMap<String,Object> secondMethods(HashMap<String,Object> map)throws Exception
    {

        AuthToken authToken = (AuthToken)map.get("token");
        LoginForm testLoginForm = (LoginForm)map.get("loginForm");
        if (loginStatusCheck(testLoginForm)) {
            // step5:testLoginForm 初始化
            return loginFormInitByMobile(testLoginForm, authToken);
        }
        else
        {
            return null;
        }
    }

    /**
     * 服务密码登录
     * @param
     */
    public Map<String, Object> serviceLogin_v2(CreditVo creditVo)
    {
        Map<String, Object> pparm = new HashMap<>();
        try
        {
            AuthToken authToken = new AuthToken();

            authToken.setToken(creditVo.getToken());

            //验证码
            String randomPassword = creditVo.getRandomPassword();

            String code = creditVo.getCode();

            String text = randomPassword == null || "".equals(randomPassword) ? randomPassword : code;

            final List<Status.PhaseStatus> taskEndPhaseStatus = Arrays.asList(Status.PhaseStatus.SUCCESS, Status.PhaseStatus.FAILED);
            // step8:登录成功抓取持续轮训状态，支持终止（SUCCESS/FAIL）。登录轮询最多5分钟。每次任务必然会返回终止状态，gxb接口会保证这点，5分钟只是做业务的逻辑退避，理论上不需要设置
            Status taskEndStatus = processing(new Callable<Status>() {
                @Override
                public Status call() throws Exception {
                    Status status = getStatus(authToken.getToken());
                    // step9: 处理登录过程中可能存在的交互，包括短信验证，图片验证，抓取失败等。直到达到的任务终止
                    if (interactiveStatusHandler(status, authToken, text) && taskEndPhaseStatus.contains(status.getPhaseStatus())) {
                        return status;
                    } else {
                        return null;
                    }
                }
            }, TimeUnit.MINUTES.toMillis(5));
            if (taskEndStatus != null && Status.PhaseStatus.SUCCESS.equals(taskEndStatus.getPhaseStatus())) {
                logger.info("抓取任务执行成功");
                pparm.put("loginResultStatus",taskEndStatus);
            } else if (taskEndStatus != null && Status.PhaseStatus.FAILED.equals(taskEndStatus.getPhaseStatus())) {
                logger.info("抓取任务执行失败，失败原因：{}", taskEndStatus.getExtra().getRemark());
            } else {
                logger.warn("任务超时，请重试");
            }

        }catch (Exception e){
            logger.error("登录手机运营商出错:"+e.getMessage());
            e.printStackTrace();
        }

        return pparm;
    }

    /**
     * 服务密码登录
     * @param
     */
    public Map<String, Object> serviceLogin(CreditVo creditVo)
    {
        Map<String, Object> pparm = new HashMap<>();
        try
        {

            // step6:用swing mock提交执行登录
            AuthToken authToken = new AuthToken();

            authToken.setToken(creditVo.getToken());


//            String serviceType = creditVo.getServiceType();
//            if("move".equals(serviceType)){
//                LoginConfig loginConfig = operatorApi.getOperatorInitConf(authToken.getToken(), creditVo.getMobile()).execute().body().getData();
//
//                CCreditUserInfo threelementUserInfo = new CCreditUserInfo();
//
//                threelementUserInfo.setCreateTime(new Date());
//                threelementUserInfo.setJsonStr(loginConfig.toString());
//                threelementUserInfo.setType(CreditConstant.OPERATORINITCONFTYPE);
//                threelementUserInfo.setTypeName("获取登录方式接口");
//                threelementUserInfo.setUid(creditVo.getUid());
//
//                pparm.put("operatorInitConf", threelementUserInfo);
//
//
//                //接口返回结果收集
////                baseCreditConfigService.addJASONStr(threelementUserInfo);
//
//                LoginForm testLoginForm = loginConfig.getLoginForms().get(0);
//
//                logger.info("本用例采用{}登录模式作为测试", testLoginForm.getFormName());
//
//            }
//            LoginForm testLoginForm = (LoginForm)map.get("loginForm");
            String mobile = creditVo.getMobile();
            //服务密码
            String serviceCode = creditVo.getServiceCode();
            //验证码
            String randomPassword = creditVo.getRandomPassword();

            String code = creditVo.getCode();

            String[] options = {"OK"};
            Map<String, String> parm = new HashMap<String, String>();
            parm.put("password",serviceCode);
            parm.put("randomPassword",randomPassword);
            parm.put("code",code);
            parm.put("username",mobile);

            String text = randomPassword == null || "".equals(randomPassword) ? randomPassword : code;

            logger.info("**************** 登录必要参数:" + authToken.getToken() + "****" +parm + "****************");

            String lrls = creditVo.getLoginResultStatus();

            Status status = null;
            if ("loginResultStatus".equals(lrls)){
                status = submitVarifyCode(authToken.getToken(), randomPassword);
            }else {
                //登录运营商
                status = submitLogin(authToken.getToken(), GsonUtil.fromJson(GsonUtil.toJson(parm), LoginRequest.class));
            }

            CCreditUserInfo threelementUserInfo = new CCreditUserInfo();

            threelementUserInfo.setCreateTime(new Date());
            threelementUserInfo.setJsonStr(status.toString());
            threelementUserInfo.setType(CreditConstant.SUBMITLOGINSTATUSTYPE);
            threelementUserInfo.setTypeName("运营商登录接口");
            threelementUserInfo.setUid(creditVo.getUid());

            pparm.put("submitLogin", threelementUserInfo);


//            baseCreditConfigService.addJASONStr(threelementUserInfo);

            // step7:对登录结果开始发起轮询。登录轮询最多3分钟。每次任务必然会返回终止状态，gxb接口会保证这点，3分钟只是做业务的逻辑退避，理论上不需要设置
            final List<Status.PhaseStatus> loginEndPhaseStatus =
                    Arrays.asList(Status.PhaseStatus.LOGIN_SUCCESS, Status.PhaseStatus.LOGIN_FAILED, Status.PhaseStatus.FAILED, Status.PhaseStatus.WAITING, Status.PhaseStatus.IMAGE_VERIFY_NEW, Status.PhaseStatus.SMS_VERIFY_NEW, Status.PhaseStatus.SUCCESS);
            logger.info("**************** 1:" + loginEndPhaseStatus + "****************");
            Status loginResultStatus = processing(new Callable<Status>() {
                @Override
                public Status call() throws Exception {
                    Status status = getStatus(authToken.getToken());
                    logger.info("**************** 2:" + status + "****************");

                    Boolean boole = interactiveStatusHandler(status, authToken, text);

                    logger.info("***** 3:" + boole + "****"+ status.getPhaseStatus() + "********" + authToken.getToken() + "****" + status.getStage());

                    // step7: 处理登录过程中可能存在的交互，包括短信验证，图片验证，登录失败等。直到达到的登录终止状态或者stage变为LOGINED
                    if ((Status.Stage.LOGINED.equals(status.getStage())
                            || boole) && (loginEndPhaseStatus.contains(status.getPhaseStatus()))) {
                        return status;
                    } else {
                        return null;
                    }
                }
            }, TimeUnit.MINUTES.toMillis(3));


            if (loginResultStatus != null &&(Status.PhaseStatus.SUCCESS.equals(loginResultStatus.getPhaseStatus()) || Status.PhaseStatus.WAITING.equals(loginResultStatus.getPhaseStatus()) || Status.PhaseStatus.LOGIN_WAITING.equals(loginResultStatus.getPhaseStatus()))) {
                logger.info("登录任务执行成功");
                pparm.put("loginResultStatus",loginResultStatus);
            }else if (Status.PhaseStatus.SMS_VERIFY_NEW.equals(loginResultStatus.getPhaseStatus()) || Status.PhaseStatus.LOGIN_SUCCESS.equals(loginResultStatus.getPhaseStatus())){
                logger.info("需要进行短信验证码二次认证，请输入最新验证码");

                pparm.put("loginResultStatus",loginResultStatus);
            }else if (Status.PhaseStatus.IMAGE_VERIFY_NEW.equals(loginResultStatus.getPhaseStatus()) || Status.PhaseStatus.LOGIN_SUCCESS.equals(loginResultStatus.getPhaseStatus())){
                logger.info("需要进行图片验证码二次认证，请输入最新验证码");

                pparm.put("loginResultStatus",loginResultStatus);
            }else if (Status.PhaseStatus.LOGIN_FAILED.equals(loginResultStatus.getPhaseStatus()) || Status.PhaseStatus.FAILED.equals(loginResultStatus.getPhaseStatus())){
                pparm.put("loginResultStatus",loginResultStatus);
            }
        }
        catch(Exception e)
        {
            logger.error("登录手机运营商出错:"+e.getMessage());
            e.printStackTrace();
        }
        return pparm;
    }

    public HashMap<String,Object> getDebtCallCnt(HashMap<String,Object> map,int count){

        HashMap<String,Object> hmap = new HashMap<>();

        try{
            AuthToken authToken = (AuthToken) map.get("authToken");
            logger.info("*****************authToken：" + authToken);
            Response<GxbResponse<ReportResponse>> response = operatorApiData.getReportdata(authToken.getToken()).execute();

            hmap.put("debtCallCnt", 0);
//            hmap.put("contactCnt", (int)(1+Math.random()*(200-1+100)));
            hmap.put("contactCnt", 0);
            hmap.put("jsonStr", JsonTools.getJsonFromObject(response.body().getData()));

            if ( !ValidateUtil.isEmpty(response) && response.body().getRetCode() == 1 && !ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary())){
                //催收类电话次数
                Integer debtCallCnt = 0;
                //近6个月联系人次数
//                Integer contactCnt = (int)(1+Math.random()*(200-1+100));
                Integer contactCnt = 0;
                if ( !ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary())){

                    if (!ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary()) &&
                            !ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary().getRiskSummary())){
                        logger.info("************************执行获取催收类电话次数************************");
                        debtCallCnt = response.body().getData().getAuthResult().getReportSummary().getRiskSummary().getDebtCallCnt();
                    }
                    if (!ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary()) &&
                            !ValidateUtil.isEmpty(response.body().getData().getAuthResult().getReportSummary().getCallSummary())){
                        logger.info("************************执行获取近6个月联系人次数************************");
                        contactCnt = response.body().getData().getAuthResult().getReportSummary().getCallSummary().get(0).getContactCnt();
                    }
                }

                logger.info("******************debtCallCnt:{}", debtCallCnt);
                logger.info("******************contactCnt:{}", contactCnt);

                hmap.put("debtCallCnt", debtCallCnt);
                hmap.put("contactCnt", contactCnt);
                hmap.put("jsonStr", JsonTools.getJsonFromObject(response.body().getData()));
            }else {
                Thread.sleep(500);
                count ++;
                if (count < 4)
                    getDebtCallCnt(map,count);
            }


        }catch (Exception e){
            logger.error("获取催收类电话次数接口报错:"+e.getMessage());
            e.printStackTrace();
        }
        return hmap;
    }

    //获取公积金授权返回结果
    public HashMap<String,Object> getSocialInsuranceDate(HashMap<String,Object> map){

        HashMap<String,Object> hmap = new HashMap<>();

        try{
            AuthToken authToken = (AuthToken) map.get("authToken");

            Response response = operatorApiData.getReportdata(authToken.getToken()).execute();
            logger.info("返回结果："+response);

        }catch (Exception e){
            logger.error("获取催收类电话次数接口报错:"+e.getMessage());
            e.printStackTrace();
        }
        return hmap;
    }

    public HashMap<String,Object> sendVerificationCode(HashMap<String,Object> map, int count){

        HashMap<String,Object> hmap = new HashMap<>();
        try {
            // step6:用swing mock提交执行登录
            AuthToken authToken = (AuthToken) map.get("authToken");

            logger.info("AuthToken信息：" + authToken.getToken());
            LoginConfig loginConfig = operatorApi.getOperatorInitConf(authToken.getToken(), map.get("mobile").toString()).execute().body().getData();

            logger.info("***********拉取登录弹出框信息***********");
            LoginForm testLoginForm = loginConfig.getLoginForms().get(0);

            String imageBase64 = null;

            Boolean logintag = false;
            String pictag = "false";
            String smstag = "false";

            for (LoginField loginField : testLoginForm.getFields()) {
                // 此处demo不对输入做具体的检测和校验，商户具体实现时，可使用以下配置
                logger.info("授权需要字段名：{}，字段中文名：{}，字段描述：{}，字段校验正则：{}，字段校验提示：{}", loginField.getField(), loginField.getFieldName(),
                        loginField.getFieldDesc(), loginField.getCheckRegex(), loginField.getFieldTips());
                if (loginField.getFieldExtraConfig() != null) {
                    logintag = true;
                    switch (loginField.getFieldExtraConfig().getFieldExtraType()) {
                        case SMS:
                            //这里是为了区分图片验证码还是手机短信验证码发送
                            if ("sendVerificationCode".equals(map.get("type"))){
                                logger.info("{}需要发送短信验证码", loginField.getField());
                                Status smsResult = refreshLoginSmsCode(authToken.getToken());
                                if (smsResult == null || Status.PhaseStatus.REFRESH_SMS_SUCCESS != smsResult.getPhaseStatus()) {
                                    hmap.put("status",false);
                                    Thread.sleep(500);
                                    count ++;
                                    if (count < 4){
                                        sendVerificationCode(map,count);
                                    }
                                    return hmap;
                                } else {
                                    hmap.put("status",true);
                                    smstag = "true";
                                    logger.info("{}随机短信验证码发送成功", loginField.getField());
                                }
                            }
                            break;
                        case PIC:
                            logger.info("{}需要刷新图片验证码", loginField.getField());
                            Status picResult = refreshLoginPicCode(authToken.getToken());
                            if (picResult == null || Status.PhaseStatus.REFRESH_IMAGE_SUCCESS != picResult.getPhaseStatus()) {
                                hmap.put("status",false);
                                Thread.sleep(500);
                                count ++;
                                if (count < 4)
                                    sendVerificationCode(map,count);
                                return hmap;
                            } else {
                                pictag = "true";
                                imageBase64 = picResult.getExtra().getRemark();
                                // 在线图片验证码base64转换地址：http://www.vgot.net/test/image2base64.php?
                                logger.info("{}图片验证码刷新成功，图片验证码(base64): {}", loginField.getField(), imageBase64);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (imageBase64 != null) {
                byte[] imageBytes = Base64.decodeBase64(imageBase64);
//                ImageIcon icon = new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageBytes)));
//                System.out.println("*******" + icon);
//                request.getSession().setAttribute("icon", imageBase64);
                hmap.put("icon",imageBase64);

            }

            hmap.put("pictag", pictag);
            hmap.put("smstag", smstag);
            hmap.put("logintag", logintag);
        }catch(Exception e)
        {
            logger.error("发送手机验证码出错:"+e.getMessage());
            e.printStackTrace();
        }
        return hmap;
    }

    @Override
    protected Status refreshLoginQrCode(String token) throws IOException {
        return null;
    }

    @Override
    protected Status refreshLoginPicCode(String token) throws IOException {
        return operatorApi.refreshLoginPicCode(token).execute().body().getData();
    }

    @Override
    protected Status refreshLoginSmsCode(String token) throws IOException {
        return operatorApi.refreshLoginSmsCode(token).execute().body().getData();
    }

    @Override
    protected Status submitLogin(String token, LoginRequest loginRequest) throws IOException {
        return operatorApi.submitLogin(token, loginRequest).execute().body().getData();
    }

    @Override
    protected Status getStatus(String token) throws IOException {
        return operatorApi.getStatus(token).execute().body().getData();
    }

    @Override
    protected Status submitVarifyCode(String token, String code) throws IOException {
        return operatorApi.submitCode(token, new CodeSubmitRequest(code)).execute().body().getData();
    }
}
