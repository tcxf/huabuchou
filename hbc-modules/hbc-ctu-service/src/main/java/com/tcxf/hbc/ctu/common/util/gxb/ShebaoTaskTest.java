package com.tcxf.hbc.ctu.common.util.gxb;

import com.google.gson.Gson;
import com.tcxf.hbc.ctu.common.util.api.ShebaoApi;
import com.tcxf.hbc.ctu.common.util.data.housefund.HouseFundLoginParamsDTO;
import com.tcxf.hbc.ctu.common.util.data.shebao.ShebaoChannelGroupDTO;
import com.tcxf.hbc.ctu.common.util.parm.AuthParm;
import com.tcxf.hbc.ctu.common.util.parm.AuthToken;
import com.tcxf.hbc.ctu.common.util.parm.LoginRequest;
import com.tcxf.hbc.ctu.common.util.parm.Status;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginConfig;
import com.tcxf.hbc.ctu.common.util.parm.config.LoginForm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @author Shayne
 * @description
 * @create 2018-04-20 下午4:08
 **/
public class ShebaoTaskTest extends AbstractGxbTest {
    private static final Logger logger = LoggerFactory.getLogger(ShebaoTaskTest.class);

    ShebaoApi shebaoApi = gxbApiFactory.newApi(ShebaoApi.class);

    //姓名
    public String name;

    //手机号
    public String phone;

    //身份证
    public String idcard;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }



    @Override
    protected AuthParm getAuthParm() {
        String sequenceNo = UUID.randomUUID().toString().replace("-", "");
        return new AuthParm(sequenceNo, "shebao", System.currentTimeMillis(), name, phone, idcard);
    }

    public void doShebaoTask() throws Exception {
        // step1:生成token
        final AuthToken authToken = getAuthToken();
        // step2:获取社保渠道列表
        List<ShebaoChannelGroupDTO> channelDTOS = shebaoApi.queryChannelListConfig(authToken.getToken()).execute().body().getData();
        // step3：根据渠道获取该渠道认证方式
        LoginConfig loginConfig = shebaoApi.queryAuthDetailConfig(authToken.getToken(), "SOCIAL_SECURITY_CHANGSHA").execute().body().getData();
        // step4：前端页面根据loginConfig动态的渲染页面，此页面是动态变更的，根据城市而定
        logger.info("已成功获取授权项{}-{}的登录初始化配置，最近更新时间{}", loginConfig.getAuthItem(), loginConfig.getAuthName(), loginConfig.getLastUpdatedAt());
        logger.info("当前网站支持{}种授权登录方式", loginConfig.getLoginForms().size());
        // 此处默认选择第一种方式作为测试用例
        LoginForm testLoginForm = loginConfig.getLoginForms().get(0);
        logger.info("本用例采用{}登录模式作为测试", testLoginForm.getFormName());
        // step5:loginForm 状态监测,验证当前的login模式是否处于正常服务状态

        if (loginFormStatusCheck(testLoginForm)) {
            // step6: testLoginForm 初始化
            if (loginFormInit(testLoginForm, authToken)) {
                // step6:用swing mock提交执行登录
                mockJPaneLogin(testLoginForm, authToken);
                Status loginResultStatus = this.getLoginStatus(authToken);

                // 理论上step7&step8做的事情是一样的，只是所处的stage不同，如果对于stage阶段不关心，可合并处理
                if (loginResultStatus != null && (Status.PhaseStatus.LOGIN_SUCCESS.equals(loginResultStatus.getPhaseStatus())
                        || Status.Stage.LOGINED.equals(loginResultStatus.getStage()))) {
                    final List<Status.PhaseStatus> taskEndPhaseStatus = Arrays.asList(Status.PhaseStatus.SUCCESS, Status.PhaseStatus.FAILED);
                    // step8:登录成功抓取持续轮训状态，支持终止（SUCCESS/FAIL）。登录轮询最多5分钟。每次任务必然会返回终止状态，gxb接口会保证这点，5分钟只是做业务的逻辑退避，理论上不需要设置
                    Status taskEndStatus = processing(new Callable<Status>() {
                        @Override
                        public Status call() throws Exception {
                            Status status = getStatus(authToken.getToken());
                            // step9: 处理登录过程中可能存在的交互，包括短信验证，图片验证，抓取失败等。直到达到的任务终止
                            if (interactiveStatusHandler(status, authToken, null) && taskEndPhaseStatus.contains(status.getPhaseStatus())) {
                                return status;
                            } else {
                                return null;
                            }
                        }
                    }, TimeUnit.MINUTES.toMillis(5));
                    if (taskEndStatus != null && Status.PhaseStatus.SUCCESS.equals(taskEndStatus.getPhaseStatus())) {
                        logger.info("抓取任务执行成功，数据已成功推送至您的回调接口");
                    } else if (taskEndStatus != null && Status.PhaseStatus.FAILED.equals(taskEndStatus.getPhaseStatus())) {
                        logger.info("抓取任务执行失败，失败原因：{}", taskEndStatus.getExtra().getRemark());
                    } else {
                        logger.warn("任务超时，请重试");
                    }
                } else {
                    logger.warn("登录失败，请重试");
                }
            }
        }
    }

    private Status getLoginStatus(AuthToken authToken) throws Exception {
        // step7:对登录结果开始发起轮询。登录轮询最多3分钟。每次任务必然会返回终止状态，gxb接口会保证这点，3分钟只是做业务的逻辑退避，理论上不需要设置
        final List<Status.PhaseStatus> loginEndPhaseStatus =
                Arrays.asList(Status.PhaseStatus.LOGIN_SUCCESS, Status.PhaseStatus.LOGIN_FAILED, Status.PhaseStatus.FAILED);
        Status loginResultStatus = processing(new Callable<Status>() {
            @Override
            public Status call() throws Exception {
                Status status = getStatus(authToken.getToken());
                // step7: 处理登录过程中可能存在的交互，包括短信验证，图片验证，登录失败等。直到达到的登录终止状态或者stage变为LOGINED
                if (Status.Stage.LOGINED.equals(status.getStage())
                        || interactiveStatusHandler(status, authToken, null) && (loginEndPhaseStatus.contains(status.getPhaseStatus()))) {
                    return status;
                } else {
                    return null;
                }
            }
        }, TimeUnit.MINUTES.toMillis(3));
        return loginResultStatus;
    }

    @Override
    protected Status refreshLoginQrCode(String token) throws IOException {
        return null;
    }

    @Override
    protected Status submitLogin(String token, LoginRequest loginInfo) throws IOException {
        return null;
    }

    @Override
    protected Status refreshLoginPicCode(String token) throws IOException {
        return shebaoApi.refreshVerifyCode(token).execute().body().getData();
    }

    @Override
    protected Status refreshLoginSmsCode(String token) throws IOException {
        Gson root = new Gson();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), root.toJson(null));
        return shebaoApi.refreshSmsVerifyCode(token, requestBody).execute().body().getData();
    }

    @Override
    protected Status submitVarifyCode(String token, String code) throws IOException {
        return this.submitVerifyCode(token, code);
    }

    @Override
    protected Status getStatus(String token) throws IOException {
        return shebaoApi.getStatus(token).execute().body().getData();
    }

    private Status submitLoginHouseFund(String token, HouseFundLoginParamsDTO houseFundLoginParamsDTO) throws IOException {
        Gson root = new Gson();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), root.toJson(houseFundLoginParamsDTO));
        return shebaoApi.sLoginSubmit(token, requestBody).execute().body().getData();
    }

    protected Status submitVerifyCode(String token, String code) throws IOException {
        return shebaoApi.codeSubmit(token, code).execute().body().getData();
    }

    protected void mockJPaneLogin(LoginForm loginForm, AuthToken token) throws Exception {
        String[] options = {"OK"};
        int selectedOption = JOptionPane.showOptionDialog(view.getViewFrame(), view.getPanel(), view.getTitle(), JOptionPane.NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, view.getIcon(), options, options[0]);
        if (selectedOption == 0) {
            if (LoginForm.LoginFormType.NORMAL.equals(loginForm.getLoginFormType())) {
                HashMap<String, String> parm = new HashMap<String, String>();
                for (String name : fieldTextMap.keySet()) {
                    String value = fieldTextMap.get(name).getText();
                    logger.info("获取输入参数名：{}，输入内容：{}", name, value);
                    parm.put(name, value);
                }
                parm.put("loginTypeCode", "ID_CARD");
                HouseFundLoginParamsDTO houseFundLoginParamsDTO = new HouseFundLoginParamsDTO();
                houseFundLoginParamsDTO.setChannelCode("SOCIAL_SECURITY_CHANGSHA");
                houseFundLoginParamsDTO.setFormParams(parm);
                submitLoginHouseFund(token.getToken(), houseFundLoginParamsDTO);
            }
        }
    }
}
