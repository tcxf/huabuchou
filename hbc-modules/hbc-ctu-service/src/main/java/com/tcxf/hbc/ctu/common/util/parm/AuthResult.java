package com.tcxf.hbc.ctu.common.util.parm;


import com.tcxf.hbc.ctu.common.util.data.operatorReport.CallDetailReport;
import com.tcxf.hbc.ctu.common.util.data.operatorReport.ReportSummary;
import com.tcxf.hbc.ctu.common.util.data.operatorReport.SmsDetailReport;

public class AuthResult {

    private ReportSummary reportSummary;
    private CallDetailReport callDetailReport;
    private SmsDetailReport smsDetailReport;

    public CallDetailReport getCallDetailReport() {
        return callDetailReport;
    }

    public SmsDetailReport getSmsDetailReport() {
        return smsDetailReport;
    }

    public void setCallDetailReport(CallDetailReport callDetailReport) {
        this.callDetailReport = callDetailReport;
    }

    public void setSmsDetailReport(SmsDetailReport smsDetailReport) {
        this.smsDetailReport = smsDetailReport;
    }

    public ReportSummary getReportSummary() {
        return reportSummary;
    }

    public void setReportSummary(ReportSummary reportSummary) {
        this.reportSummary = reportSummary;
    }
}
