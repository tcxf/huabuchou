package com.tcxf.hbc.ctu.common.util.parm;

public class ReportResponse {

    private AuthResult authResult;

    public AuthResult getAuthResult() {
        return authResult;
    }

    public void setAuthResult(AuthResult authResult) {
        this.authResult = authResult;
    }
}
