package com.tcxf.hbc.ctu.common.util.secure;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 * 3DES加密解密工具类
 */
public class DES {

    private static final String KEY = "uatspdbcccgame2014061800";


    /**
     * 3des加密  key必须是长度大于等于 3*8 = 24 位
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static String encryptThreeDESECB(final String src, final String key) throws Exception
    {
        final DESedeKeySpec dks = new DESedeKeySpec(key.getBytes("UTF-8"));
        final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        final SecretKey securekey = keyFactory.generateSecret(dks);

        final Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, securekey);
        final byte[] b = cipher.doFinal(src.getBytes());

        final BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(b).replaceAll("\r", "").replaceAll("\n", "");
    }

    /**
     * 默认秘钥3des加密
     * @param src
     * @return
     * @throws Exception
     */
    public static String encryptdf(final String src) throws Exception
    {
        return encryptThreeDESECB(src, KEY);
    }

    /**
     * 3DESECB解密,
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static String decryptThreeDESECB(final String src, final String key) throws Exception
    {
        // --通过base64,将字符串转成byte数组
        final BASE64Decoder decoder = new BASE64Decoder();
        final byte[] bytesrc = decoder.decodeBuffer(src);
        // --解密的key
        final DESedeKeySpec dks = new DESedeKeySpec(key.getBytes("UTF-8"));
        final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        final SecretKey securekey = keyFactory.generateSecret(dks);

        // --Chipher对象解密
        final Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, securekey);
        final byte[] retByte = cipher.doFinal(bytesrc);

        return new String(retByte);
    }

    /**
     * 默认秘钥3des解密
     * @param src
     * @return
     * @throws Exception
     */
    public static String decryptdf(final String src) throws Exception
    {
        return decryptThreeDESECB(src,KEY);
    }

    public static void main(String[] args) throws Exception {
        final String key = KEY ;
        // 加密流程
        String telePhone = "15629551180";
        String telePhone_encrypt = "";
        telePhone_encrypt = DES.encryptdf(telePhone);
        System.out.println(telePhone_encrypt);

        // 解密流程
        String tele_decrypt = DES.decryptdf(telePhone_encrypt);
        System.out.println("模拟代码解密:" + tele_decrypt);
    }
}
