package com.tcxf.hbc.ctu.common.util.txapi;

public class ApiUrlConfig {

    private static ApiUrlConfig apiUrlConfig = new ApiUrlConfig();

    public static ApiUrlConfig getInstance(){

        return apiUrlConfig;

    }

    //天行Token地址
    private static String tokenurl="http://test.tianxingshuke.com/api/rest/common/organization/auth?mockType=SAME&";

    //银行卡三要素地址
    private static String threelementurl = "http://test.tianxingshuke.com/api/rest/unionpay/auth/3element?mockType=SAME&";

    //运营商三要素地址
    private static String operatorsurl = "http://test.tianxingshuke.com/api/rest/operators/auth/3element?mockType=SAME&";

    //多重负债地址
    private static String multipleNewurl = "http://test.tianxingshuke.com/api/rest/riskTip/lending/multipleNew?mockType=SAME&";

    private static final String account = "test_f5w0f";
    private static final String signature="6ddb372eee674182aad89e67860c99f4";

    public String openUrl(String parameter, String url){

        url = url + parameter;

        return url;
    }

    public static String getTokenurl() {
        return tokenurl;
    }

    public static String getThreelementurl() {
        return threelementurl;
    }

    public static String getOperatorsurl() {
        return operatorsurl;
    }

    public static String getMultipleNewurl() {
        return multipleNewurl;
    }

    public static String getAccount() {
        return account;
    }

    public static String getSignature() {
        return signature;
    }
}
