package com.tcxf.hbc.ctu.controller.cuser;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.ctu.common.constant.CreditConstant;
import com.tcxf.hbc.ctu.common.util.AddressUtils;
import com.tcxf.hbc.ctu.common.util.GXBInterfaceUtil;
import com.tcxf.hbc.ctu.common.util.OperatorUtil;
import com.tcxf.hbc.ctu.common.util.gxb.OperatorTaskTest;
import com.tcxf.hbc.ctu.common.util.parm.AuthToken;
import com.tcxf.hbc.ctu.common.util.parm.Status;
import com.tcxf.hbc.ctu.common.util.secure.DES;
import com.tcxf.hbc.ctu.model.dto.MycreditDetailsDto;
import com.tcxf.hbc.ctu.service.*;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
@RestController
@RequestMapping("/baseCreditConfig")
public class BaseCreditConfigController extends BaseController {

    @Autowired private IFMoneyOtherInfoService fMoneyOtherInfoService;

    @Autowired private IFMoneyInfoService fMoneyInfoService;

    @Autowired
    private IBaseCreditConfigService baseCreditConfigService;

    @Autowired
    private ITbAccountInfoService iTbAccountInfoService;

    @Autowired
    private ICUserRefConService iCUserRefConService;

    @Autowired
    private IBaseInterfaceInfoService iBaseInterfaceInfoService;

    @Autowired
    private IFFundendBindService iFFundendBindService;

    @Autowired
    private ICUserInfoService iCUserInfoService;

    @Autowired
    private ITbBusinessCreditService iTbBusinessCreditService;

    @Autowired
    private ITbCreditGradeService iTbCreditGradeService;

    @Autowired
    private IBaseCreditInfoService iBaseCreditInfoService;

    @Autowired
    protected ICOperaSwitchService iCOperaSwitchService;

    @Autowired
    protected ITbAreaService iTbAreaService;

    @Autowired
    protected IOOperaInfoService iOOperaInfoService;

    @Autowired
    protected ISysAreaService iSysAreaService;

    @Autowired
    protected RefusingCreditService refusingCreditService;

    @Autowired
    private IRmSxtemplateRelationshipService rmSxtemplateRelationshipService;

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> template;

    @RequestMapping("secondMethod")
    public R secondMethod()
    {
        R r = new R();
        try
        {
            HashMap<String,Object> mapResult = GXBInterfaceUtil.doOperator();
            OperatorTaskTest ott = new OperatorTaskTest();
            HashMap<String,Object> mapType= ott.secondMethods(mapResult);
            if(mapType!=null && mapType.get("type").toString().equals("image"))
            {
                r.setMsg(mapType.get("type").toString());
                r.setData(mapType.get("imageData"));
            }
            else
            {
                r.setMsg("sms");
            }


        }
        catch(Exception e)
        {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return r;
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getMargin("2018-09-25","2018-09-01"));
    }

    /**
     * @Description: 检查授信入参的合法
     * @Param: [creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void validateSx(CreditVo creditVo) {

        if(ValidateUtil.isEmpty(creditVo.getIdCard())||
                ValidateUtil.isEmpty(creditVo.getMobile())||
                ValidateUtil.isEmpty(creditVo.getUserName())||
                ValidateUtil.isEmpty(creditVo.getAccountNO())){
            throw new CheckedException("授信信息不完整，请核实");
        }


        //验证该身份证号码是否已经授信过
        BaseCreditInfo baseCreditInfo = iBaseCreditInfoService.selectOne(new EntityWrapper<BaseCreditInfo>().eq("idCard",creditVo.getIdCard()));
        if (!ValidateUtil.isEmpty(baseCreditInfo)){
            throw new CheckedException("该身份证已授信，请核实");
        }

        //查询授信相关信息
        CUserRefCon cUserRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid",creditVo.getUid()).eq("oid",creditVo.getOid()));
        if (!ValidateUtil.isEmpty(cUserRefCon)&&cUserRefCon.getAuthStatus().equals(CUserRefCon.AUTHSTATUS_Y)) {
            throw new CheckedException("您已经授信成功，请勿重复授信");
        }

        //判断是否有拒绝授信历史
        if(CUserRefCon.AUTHSTATUS_JJ.equals(cUserRefCon.getAuthStatus())&&
                CUserRefCon.TWOAPPLICATIONS_N.equals(cUserRefCon.getTwoapplications())){
            throw new CheckedException("您已被永久拒绝授信");
        }
        if(cUserRefCon.getAuthStatus().equals(CUserRefCon.AUTHSTATUS_JJ)&&
                CUserRefCon.TWOAPPLICATIONS_Y.equals(cUserRefCon.getTwoapplications())){
            //获取拒绝时间
            int day = DateUtil.getMargin(DateUtil.format(new Date(),DateUtil.FORMAT_YYYY_MM_DD),
                    DateUtil.format(cUserRefCon.getAuthDate(),DateUtil.FORMAT_YYYY_MM_DD));
            //是否满足三个月时间
            if(day<=90){
                throw new CheckedException("暂时无法授信，请在"+(90-day)+"天后再进行授信");
            }
        }
        if(cUserRefCon.getAuthStatus().equals(CUserRefCon.AUTHSTATUS_JJ)&&
                ValidateUtil.isEmpty(cUserRefCon.getTwoapplications())&&
                !ValidateUtil.isEmpty(cUserRefCon.getCreditCount()) &&
                Integer.parseInt(cUserRefCon.getCreditCount()) == 2){
            //获取拒绝时间
            int day = DateUtil.getMargin(DateUtil.format(new Date(),DateUtil.FORMAT_YYYY_MM_DD),
                    DateUtil.format(cUserRefCon.getAuthDate(),DateUtil.FORMAT_YYYY_MM_DD));
            //是否满足三个月时间
            if(day<=90){
                throw new CheckedException("暂时无法授信，请在"+(90-day)+"天后再进行授信");
            }
        }

        //查询最新拒绝条件模板
        List<RefusingCredit> list = refusingCreditService.selectRefusingCreditByfId(creditVo.getFid());

        Map<String,RefusingCredit> map = new HashMap<>();
        for(RefusingCredit r : list){
            map.put(r.getCode(),r);
        }
        String now = DateUtil.format(new Date(),DateUtil.YEAR_FORMAT);
        String age = creditVo.getIdCard().substring(6,10);
        Integer ageInt = Integer.parseInt(now)-Integer.parseInt(age);
        if (map.get("agecheck").getStatus().equals(RefusingCredit.status_1)
                && ageInt < map.get("agecheck").getNumber()){
            baseCreditConfigService.updateCUserRefConByUId("","agecheck",creditVo,"您还未满"+map.get("agecheck").getNumber()+"岁周岁，不允许授信");
            throw new CheckedException("您还未满"+map.get("agecheck").getNumber()+"岁周岁，不允许授信");
        }

    }


    /**
     * 1.快速授信界面提交检测
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/checkUser" , method = RequestMethod.POST)
    @ApiOperation(value = "1.快速授信界面提交检测", notes = "1.快速授信界面提交检测")
    public R page(@RequestBody CreditVo creditVo) {
        //检查数据合法性
        try {
            //快速授信必须要有前提条件，运营商必须要绑定资金商，否则不能做快速授信
            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());

            if (!ValidateUtil.isEmpty(fFundendBind)){
                creditVo.setFid(fFundendBind.getFid());
            }
            validateSx(creditVo);
            //授信第一步 获取权重分值
            String  totalScore = baseCreditConfigService.onSx(creditVo);
            return R.newOK(totalScore);

        }catch (Exception ex){
            if(ex instanceof  CheckedException){
                //根据异常信息
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newErrorMsg("授信失败");
        }
    }

    public R checkRule(CreditVo creditVo) throws Exception{

        HashMap<String, Object> pagemap = new HashMap<>();

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        //这里是做了三个月后再次授信日期处理
        c.add(Calendar.MONTH, 3);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("authStatus", "0");
        hashMap.put("authMax", null);
        hashMap.put("authDate", new Date());
        hashMap.put("authExamine", "2");
        hashMap.put("uid", creditVo.getUid());
        hashMap.put("oid", creditVo.getOid());
        hashMap.put("examinefailreason", "");
        hashMap.put("nextauthDate", sf.format(c.getTime()));

        Integer area = Integer.parseInt(creditVo.getIdCard().substring(0, 6));


        //获取授信控制开关
        COperaSwitch cOperaSwitch = iCOperaSwitchService.selectOne(new EntityWrapper<COperaSwitch>().eq("oid", creditVo.getOid()));

        //获取运营商信息
        OOperaInfo oOperaInfo = iOOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("id", creditVo.getOid()));


        if (!ValidateUtil.isEmpty(cOperaSwitch)){
            //获取运营商详细地址信息
            TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id", oOperaInfo.getArea()));
            //根据用户的身份证信息获取详细地址
            SysArea sysArea = iSysAreaService.selectOne(new EntityWrapper<SysArea>().eq("area_code", area));

            if ("1".equals(cOperaSwitch.getIdcardState())){             //地区授信控制

                if (!sysArea.getAreaName().equals(tbArea.getName())){
                    pagemap.put("msg","您的籍贯不在该运营商范围内，不允许授信！");
                    hashMap.put("examinefailreason", "您的籍贯不在该运营商范围内，不允许授信！");
                    iCUserRefConService.updateCUserRefConByUid(hashMap);
                    return R.newError(pagemap);
                }
            }

            if ("1".equals(cOperaSwitch.getPhoneState())){              //运营商授信控制
                String province = OperatorUtil.calcMobileCity(creditVo.getMobile());

                if (!ValidateUtil.isEmpty(province) && tbArea.getDisplayName().indexOf(province) == -1){
                    pagemap.put("msg","您的手机号归属地不在该运营商范围内，不允许授信！");
                    hashMap.put("examinefailreason", "您的手机号归属地不在该运营商范围内，不允许授信！");
                    iCUserRefConService.updateCUserRefConByUid(hashMap);
                    return R.newError(pagemap);
                }
            }

            if ("1".equals(cOperaSwitch.getIpState())) {              //设备当前位置授信控制

                if (!ValidateUtil.isEmpty(creditVo.getIp())){
                    String json_result = null;

                    json_result = AddressUtils.getAddresses(creditVo.getIp());


                    JSONObject json = JSONObject.fromObject(json_result);

                    String country = JSONObject.fromObject(json.get("data")).get("country").toString();
                    String region = JSONObject.fromObject(json.get("data")).get("region").toString();
                    String city = JSONObject.fromObject(json.get("data")).get("city").toString();
                    String county = JSONObject.fromObject(json.get("data")).get("county").toString();
                    String isp = JSONObject.fromObject(json.get("data")).get("isp").toString();
                    String areas = JSONObject.fromObject(json.get("data")).get("area").toString();
                    System.out.println("国家： " + country);
                    System.out.println("地区： " + areas);
                    System.out.println("省份: " + region);
                    System.out.println("城市： " + city);
                    System.out.println("区/县： " + county);
                    System.out.println("互联网服务提供商： " + isp);

                    if (!ValidateUtil.isEmpty(areas) && tbArea.getName().indexOf(areas) == -1){
                        pagemap.put("msg","您的移动设备不在该运营商范围内，不允许授信！");
                        hashMap.put("examinefailreason", "您的移动设备不在该运营商范围内，不允许授信！");
                        iCUserRefConService.updateCUserRefConByUid(hashMap);
                        return R.newError(pagemap);
                    }


                }
            }

        }

        return R.newOK();
    }


    /**
     * 2.进入运营商授信界面
     * @param
     * @return
     */
    @RequestMapping(value = "/toOperatorView" , method = RequestMethod.POST)
    @ApiOperation(value = "2.进入运营商授信界面", notes = "2.进入运营商授信界面")
    public R<HashMap<String,Object>> toOperatorView(@RequestBody CreditVo creditVo){

        R r = new R();
        HashMap<String, Object> hmap = new HashMap<>();
        try {

            OperatorTaskTest ott = new OperatorTaskTest();

            ott.setIdcard(creditVo.getIdCard());
            ott.setName(creditVo.getUserName());
//            ott.setIdcard("433025199110106625");
//            ott.setName("田芳");
            ott.setPhone(creditVo.getMobile());

            if (ValidateUtil.isEmpty(creditVo.getMobile()) || "null".equals(creditVo.getMobile())){
                BaseCreditInfo baseCreditInfo = iBaseCreditInfoService.selectOne(new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()));
                if (!ValidateUtil.isEmpty(baseCreditInfo)){
                    ott.setIdcard(baseCreditInfo.getIdcard());
                    ott.setName(baseCreditInfo.getUsername());
                    ott.setPhone(baseCreditInfo.getMobile());
                    creditVo.setMobile(baseCreditInfo.getMobile());
                    creditVo.setIdCard(baseCreditInfo.getIdcard());
                    creditVo.setUserName(baseCreditInfo.getUsername());
                }else {
                    return R.newError(hmap);
                }
            }

            HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();

            ott.setAppId(ibfismap.get("appId").toString());
            ott.setAppSecurity(ibfismap.get("appSecurity").toString());

            AuthToken authToken = new AuthToken();

            //这里是做token校验，如果token为空则是第一次进来，否则是调用刷新拿验证码的进来
            if (ValidateUtil.isEmpty(creditVo.getToken())){
                authToken = ott.getAuthToken();
            }else {
                authToken.setToken(creditVo.getToken());
            }

            HashMap<String, Object> params = new HashMap<>();

            params.put("mobile", creditVo.getMobile());
            if (ValidateUtil.isEmpty(creditVo.getMobile())){
                BaseCreditInfo baseCreditInfo = iBaseCreditInfoService.selectOne(new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()));
                if (!ValidateUtil.isEmpty(baseCreditInfo)){
                    params.put("mobile", baseCreditInfo.getMobile());
                    creditVo.setMobile(baseCreditInfo.getMobile());
                }else {
                    return R.newError(hmap);
                }
            }

            params.put("authToken", authToken);
            params.put("type", "sendVerificationCode");

//        request.getSession().setAttribute("authToken", authToken);

            //发送图片验证码
            HashMap<String, Object> hashMap = ott.sendVerificationCode(params, 1);

            String icon = hashMap.get("icon") == null ? "" : hashMap.get("icon").toString();

            BaseCreditInfo baseCreditInfo = iBaseCreditInfoService.selectOne(new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()));
            creditVo.setTotalScore(baseCreditInfo.getGrade());

            hmap.put("icon", icon);
            hmap.put("token", authToken.getToken());
            hmap.put("logintag", hashMap.get("logintag"));
            hmap.put("pictag", hashMap.get("pictag"));
            hmap.put("smstag", hashMap.get("smstag"));
            hmap.put("mobile", creditVo.getMobile());
            hmap.put("idCard", creditVo.getIdCard());
            hmap.put("userName", creditVo.getUserName());
            hmap.put("totalScore", creditVo.getTotalScore());

            r.setData(hmap);
            r.setCode(0);
            r.setMsg("success");

        }catch (Exception e){
            logger.error(e.getMessage());
            hmap.put("msg",e.getMessage());

            return R.newError(hmap);
        }

        return r;

    }



    /**
     * 3.检验授信是否通过
     * @param
     * @return
     */
    @RequestMapping(value = "verifyCredit", method = RequestMethod.POST)
    @ApiOperation(value = "3.检验授信是否通过", notes = "3.检验授信是否通过")
    public R verifyCredit(@RequestBody CreditVo creditVo) {
        R r = new R();

        try {

            AuthToken authToken = new AuthToken();
            authToken.setToken(creditVo.getToken());
            HashMap<String, Object> params = new HashMap<>();

            params.put("token", authToken.getToken());

        }catch (Exception e){
            logger.error(e.getMessage());
            r.setCode(1);
            r.setMsg(e.getMessage());
        }
        return r;
    }

    /**
     * 4.运营商验证入口-发送手机验证码
     * @param
     * @return
     */
    @RequestMapping(value = "sendVerificationCode", method = RequestMethod.POST)
    @ApiOperation(value = "4.运营商验证入口-发送手机验证码", notes = "4.运营商验证入口-发送手机验证码")
    public R sendVerificationCode(@RequestBody CreditVo creditVo) {
        R r = new R();
        try{

            OperatorTaskTest ott = new OperatorTaskTest();
            //        AuthToken authToken = ott.getAuthToken();
            HashMap<String,Object> params = new HashMap<>();

            AuthToken authToken = new AuthToken();
            authToken.setToken(creditVo.getToken());
            params.put("authToken", authToken);
            params.put("type","sendVerificationCode");
            params.put("mobile",creditVo.getMobile());
            //        params.put("token", authToken);

            //        request.getSession().setAttribute("token", authToken.getToken());
            //发送验证码
            HashMap<String,Object> hashMap = ott.sendVerificationCode(params,1);

            if(Boolean.parseBoolean(hashMap.get("status").toString())){
                r.setCode(0);
                r.setMsg("短信验证码发送成功");
            }else {
                r.setCode(1);
                r.setMsg("短信验证码发送失败");
            }
        }catch (Exception e){
            logger.error(e.getMessage());
            r.setCode(1);
            r.setMsg(e.getMessage());
        }
        return r;
    }



    /**
     * 5.运营商验证入口
     * @param
     * @return
     */
    @RequestMapping(value = "threeMethod", method = RequestMethod.POST)
    @ApiOperation(value = "5.运营商验证入口", notes = "5.运营商验证入口")
    @Transactional
    public R threeMethod(@RequestBody CreditVo creditVo) {
        R r = new R();

        try{

            OperatorTaskTest ott = new OperatorTaskTest();
            AuthToken authToken = new AuthToken();
            authToken.setToken(creditVo.getToken());



            HashMap<String, Object> hMap = new HashMap<>();

            hMap.put("uid",creditVo.getUid());
            hMap.put("oid",creditVo.getOid());

            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());

            if (!ValidateUtil.isEmpty(fFundendBind)){
                creditVo.setFid(fFundendBind.getFid());
            }

            //查询这个人的额度详情
            CUserRefCon cUserRefCon = iCUserRefConService.getCUserRefConByUid(hMap);

            if (!ValidateUtil.isEmpty(cUserRefCon) && "1".equals(cUserRefCon.getAuthStatus())){
                logger.info("您已授信");
                r.setCode(3);
                r.setMsg("您已授信");
                return r;
            }

            //查询授信分值
            Double d = getTbCreditGradeByUid(creditVo);
            creditVo.setTotalScore(d.toString());

            //拉取电话详单
            Map<String, Object> pmap = ott.serviceLogin(creditVo);

            if (!ValidateUtil.isEmpty(cUserRefCon) && "0".equals(cUserRefCon.getAuthStatus())) {


                if (pmap.get("operatorInitConf") != null) {
                    baseCreditConfigService.addJASONStr((CCreditUserInfo) pmap.get("operatorInitConf"));
                }
                if (pmap.get("submitLogin") != null) {
                    baseCreditConfigService.addJASONStr((CCreditUserInfo) pmap.get("submitLogin"));
                }

                if (pmap.get("loginResultStatus") != null && (Status.PhaseStatus.SUCCESS.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus()) || Status.PhaseStatus.WAITING.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus()))) {

                    //一次验证
                    r = toSaveSXSuccess(authToken,creditVo,ott,fFundendBind,pmap);

                } else if (!ValidateUtil.isEmpty(pmap.get("loginResultStatus")) && (Status.PhaseStatus.LOGIN_FAILED.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus()) ||
                        Status.PhaseStatus.FAILED.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus())
                )) {
                    logger.info("登录任务执行失败");
                    r.setCode(2);
                    r.setMsg(((Status) pmap.get("loginResultStatus")).getExtra().getRemark());
                    return r;
                } else {

                    //二次验证
                    r = toSaveSXFail(authToken, creditVo, ott, fFundendBind, pmap);
                }

            }

        }catch (Exception e){
            logger.error(e.getMessage());
            r.setCode(2);
            r.setMsg(e.getMessage());
        }

        return r;
    }

    /**
     * @Description: 获取用户评分
     * @Param: [creditVo]
     * @return: java.lang.Double
     * @Author: JinPeng
     * @Date: 2018/10/30
    */
    public Double getTbCreditGradeByUid(CreditVo creditVo){

        Double grade = 0.0;

        //获取用户评分项
        List<TbCreditGrade> tbCreditGrade = iTbCreditGradeService.selectList(new EntityWrapper<TbCreditGrade>().eq("uid", creditVo.getUid()));

        if (ValidateUtil.isEmpty(tbCreditGrade)){
            return grade;
        }

        for (TbCreditGrade tcg: tbCreditGrade
             ) {
            if ("debtCallScore".equals(tcg.getCode())){
                grade = grade - Double.parseDouble(tcg.getValue());
            }else {
                grade = grade + Double.parseDouble(tcg.getValue());
            }
        }

        return grade;
    }



    /** 
     * @Description: 授信一次验证通过 
     * @Param: [authToken, creditVo, ott, totalScore, tbAccountInfo, fFundendBind, pmap] 
     * @return: com.tcxf.hbc.common.util.R 
     * @Author: JinPeng
     * @Date: 2018/10/17 
    */ 
    public R toSaveSXSuccess(AuthToken authToken, CreditVo creditVo, OperatorTaskTest ott, FFundendBind fFundendBind, Map<String, Object> pmap) throws Exception{

        R r = new R();



        TbAccountInfo tbAccountInfo = new TbAccountInfo();
        tbAccountInfo.setOid(creditVo.getUid());
        tbAccountInfo.setUtype(creditVo.getUtype());
        tbAccountInfo.setAuthType(creditVo.getAuthType());
        logger.info("*************执行转换额度*************");

        BigDecimal ts = baseCreditConfigService.getTotalScore(creditVo.getTotalScore(),fFundendBind.getFid());

        Double totalScore = Double.parseDouble(ts.toString());
        logger.info("*************额度:" + totalScore + "*************");
        String balance = "";

        //这里是授信成功给固定额度1500
        balance = DES.encryptdf("1500.00");

        tbAccountInfo.setBalance(balance);
        tbAccountInfo.setMaxMoney(balance);
        tbAccountInfo.setIsFreeze("0");
        logger.info("*************评分：" + creditVo.getTotalScore());
        tbAccountInfo.setGrade(creditVo.getTotalScore());
        tbAccountInfo.setCreateDate(new Date());
        tbAccountInfo.setRapidMoney(balance);
        logger.info("*************执行插入授信额度表*************");

        //这里是做防止重复插入授信
        iTbAccountInfoService.delete(new EntityWrapper<TbAccountInfo>().eq("oid", creditVo.getUid()));
        //执行插入授信额度表
        iTbAccountInfoService.insert(tbAccountInfo);

        logger.info("************tbAccountInfo.getId():" + tbAccountInfo.getId());
        //快速授信必须要有前提条件，运营商必须要绑定资金商，否则不能做快速授信
//                    FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());


        if (!ValidateUtil.isEmpty(fFundendBind)) {


            HashMap<String, Object> params = new HashMap<>();

            params.put("authToken", authToken);
            params.put("uid", creditVo.getUid());


            //获取催收类电话次数
            HashMap<String, Object> hmap = ott.getDebtCallCnt(params, 1);

            //接口返回结果收集
            this.toAddJASONStr(hmap.get("jsonStr").toString(), CreditConstant.DEBTCALLCNTTYPE, "手机电话记录接口", creditVo.getUid());

            Integer debtCallCnt = Integer.parseInt(hmap.get("debtCallCnt").toString());
            Integer contactCnt = Integer.parseInt(hmap.get("contactCnt").toString());

            if (contactCnt > 0) {

                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar c = Calendar.getInstance();
                System.out.println("当前日期： " + sf.format(c.getTime()));
                c.add(Calendar.MONTH, 3);
                System.out.println("增加三个月后日期 ： " + sf.format(c.getTime()));

                HashMap<String, Object> hhMap = new HashMap<>();
                hhMap.put("authStatus", "0");
                hhMap.put("authMax", null);
                hhMap.put("authDate", new Date());
                hhMap.put("authExamine", "2");
                hhMap.put("uid", creditVo.getUid());
                hhMap.put("oid", creditVo.getOid());
                hhMap.put("nextauthDate", sf.format(c.getTime()));


                Map<String, Object> map = new HashMap<>();

                //查询最新拒绝条件模板
                List<RefusingCredit> list = refusingCreditService.selectRefusingCreditByfId(creditVo.getFid());

                Map<String,RefusingCredit> rfmap = new HashMap<>();
                for(RefusingCredit rf : list){
                    rfmap.put(rf.getCode(),rf);
                }


                if (rfmap.get("collectionnumcheck").getStatus().equals(RefusingCredit.status_1) &&
                        debtCallCnt > rfmap.get("collectionnumcheck").getNumber()){
                    //执行修改用户，运营商，资金端，商户关系表
                    hhMap.put("examinefailreason", "催收类电话大于5次或者6个月内联系人个人不足15人！");
                    baseCreditConfigService.updateCUserRefConByUId("","examinefailreason",creditVo,"催收类电话大于"+rfmap.get("collectionnumcheck").getNumber()+"次！");

                    r.setCode(2);
                    r.setMsg("请三个月后重新申请");
                    return r;
                }

                logger.info("******************RefusingCredit.status:{}*****************", rfmap.get("contactsnumcheck").getStatus());
                logger.info("******************contactsnumcheck:{}*****************", rfmap.get("contactsnumcheck").getNumber());
                logger.info("******************contactCnt:{}*****************", contactCnt);

                if (rfmap.get("contactsnumcheck").getStatus().equals(RefusingCredit.status_1) &&
                        contactCnt < rfmap.get("contactsnumcheck").getNumber()){
                    //执行修改用户，运营商，资金端，商户关系表
                    hhMap.put("examinefailreason", "催收类电话大于5次或者6个月内联系人个人不足15人！");
                    baseCreditConfigService.updateCUserRefConByUId("","examinefailreason",creditVo,"6个月内联系人个人不足"+rfmap.get("contactsnumcheck").getNumber()+"人！");
                    r.setCode(2);
                    r.setMsg("请三个月后重新申请");
                    return r;
                }


                //6个月内联系人不足15人则退出快速授信
                map = getCalculateQuotaByCallCnt(contactCnt, debtCallCnt, totalScore, creditVo);

                if (!ValidateUtil.isEmpty(map)) {
                    totalScore = totalScore + Double.parseDouble(map.get("totalScore").toString());
                }

                r.setCode(0);
                r.setMsg("success");

                BaseCreditInfo baseCreditInfo = new BaseCreditInfo();
                baseCreditInfo.setStatus("2");

                iBaseCreditInfoService.update(baseCreditInfo, new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()));


                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("authStatus", "1");
                hashMap.put("authMax", new BigDecimal("1500"));
                hashMap.put("authDate", new Date());
                hashMap.put("authExamine", "1");
                hashMap.put("uid", creditVo.getUid());
                hashMap.put("oid", creditVo.getOid());
                logger.info("*************fFundendBind:" + fFundendBind);
                hashMap.put("fid", fFundendBind.getFid());
                hashMap.put("accid", tbAccountInfo.getId());
                logger.info("*************执行修改用户，运营商，资金端，商户关系表*************");
                //执行修改用户，运营商，资金端，商户关系表
                iCUserRefConService.updateCUserRefConByUid(hashMap);

                FMoneyInfo fMoneyInfo = fMoneyInfoService.selectOne(new EntityWrapper<FMoneyInfo>().eq("fid", fFundendBind.getFid()));


                if (!ValidateUtil.isEmpty(fMoneyInfo) && "1".equals(fMoneyInfo.getStatus())) {
                    FMoneyOtherInfo fMoneyOtherInfo = new FMoneyOtherInfo();
                    fMoneyOtherInfo.setCreateDate(new Date());
                    fMoneyOtherInfo.setFid(fFundendBind.getFid());
                    fMoneyOtherInfo.setFmid(fMoneyInfo.getId());
                    fMoneyOtherInfo.setMoney(new BigDecimal("1500"));
                    fMoneyOtherInfo.setType("0");
                    fMoneyOtherInfo.setUid(creditVo.getUid());

                    //记录授信总金额出入明细表
                    fMoneyOtherInfoService.insert(fMoneyOtherInfo);
                }


                logger.info("登录任务执行成功");
                r.setCode(0);
                r.setMsg("success");
            } else {
                logger.info("登录任务执行失败,请重试！");
                r.setCode(2);
                r.setMsg(((Status) pmap.get("loginResultStatus")).getExtra().getRemark());
                return r;
            }

        } else {
            r.setCode(2);
            r.setMsg("该运营商未绑定资金商，请先绑定资金商再进行授信");
        }

        return r;
    }


    /** 
     * @Description: 授信两次验证通过 
     * @Param: [authToken, creditVo, ott, totalScore, tbAccountInfo, fFundendBind, pmap] 
     * @return: com.tcxf.hbc.common.util.R 
     * @Author: JinPeng
     * @Date: 2018/10/17 
    */
    public R toSaveSXFail(AuthToken authToken, CreditVo creditVo, OperatorTaskTest ott, FFundendBind fFundendBind, Map<String, Object> pmap) throws Exception{
        R r = new R();

        HashMap<String, Object> params = new HashMap<>();

        params.put("authToken", authToken);
        params.put("uid", creditVo.getUid());


        //获取催收类电话次数
        HashMap<String, Object> hmap = ott.getDebtCallCnt(params, 1);

        //接口返回结果收集
        this.toAddJASONStr(hmap.get("jsonStr").toString(), CreditConstant.DEBTCALLCNTTYPE, "手机电话记录接口", creditVo.getUid());

        Integer debtCallCnt = Integer.parseInt(hmap.get("debtCallCnt").toString());
        Integer contactCnt = Integer.parseInt(hmap.get("contactCnt").toString());

        if (contactCnt > 0) {

            logger.info("**********************************************************************************");

            TbAccountInfo tbAccountInfo = new TbAccountInfo();
            tbAccountInfo.setOid(creditVo.getUid());
            tbAccountInfo.setUtype(creditVo.getUtype());
            tbAccountInfo.setAuthType(creditVo.getAuthType());
            logger.info("*************执行转换额度*************");

            BigDecimal ts = baseCreditConfigService.getTotalScore(creditVo.getTotalScore(),fFundendBind.getFid());

            Double totalScore = Double.parseDouble(ts.toString());
            logger.info("*************额度:" + totalScore + "*************");
            String balance = "";

            //这里是授信成功给固定额度1500
            balance = DES.encryptdf("1500.00");

            tbAccountInfo.setBalance(balance);
            tbAccountInfo.setMaxMoney(balance);
            tbAccountInfo.setIsFreeze("0");
            logger.info("*************评分：" + creditVo.getTotalScore());
            tbAccountInfo.setGrade(creditVo.getTotalScore());
            tbAccountInfo.setCreateDate(new Date());
            tbAccountInfo.setRapidMoney(balance);
            logger.info("*************执行插入授信额度表*************");
            //这里是做防止重复插入授信
            iTbAccountInfoService.delete(new EntityWrapper<TbAccountInfo>().eq("oid", creditVo.getUid()));
            //执行插入授信额度表
            iTbAccountInfoService.insert(tbAccountInfo);

            logger.info("************tbAccountInfo.getId():" + tbAccountInfo.getId());
//                        //快速授信必须要有前提条件，运营商必须要绑定资金商，否则不能做快速授信
//                        FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());


            if (!ValidateUtil.isEmpty(fFundendBind)) {

                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar c = Calendar.getInstance();
                System.out.println("当前日期： " + sf.format(c.getTime()));
                c.add(Calendar.MONTH, 3);
                System.out.println("增加三个月后日期 ： " + sf.format(c.getTime()));

                HashMap<String, Object> hhMap = new HashMap<>();
                hhMap.put("authStatus", "0");
                hhMap.put("authMax", null);
                hhMap.put("authDate", new Date());
                hhMap.put("authExamine", "2");
                hhMap.put("uid", creditVo.getUid());
                hhMap.put("oid", creditVo.getOid());
                hhMap.put("nextauthDate", sf.format(c.getTime()));


                Map<String, Object> map = new HashMap<>();

                logger.info("****************debtCallCnt:{}", debtCallCnt);
                logger.info("****************contactCnt:{}", contactCnt);

                //查询最新拒绝条件模板
                List<RefusingCredit> list = refusingCreditService.selectRefusingCreditByfId(creditVo.getFid());

                Map<String,RefusingCredit> rfmap = new HashMap<>();
                for(RefusingCredit rf : list){
                    rfmap.put(rf.getCode(),rf);
                }

                if (rfmap.get("collectionnumcheck").getStatus().equals(RefusingCredit.status_1) &&
                        debtCallCnt > rfmap.get("collectionnumcheck").getNumber()){
                    //执行修改用户，运营商，资金端，商户关系表
                    hhMap.put("examinefailreason", "催收类电话大于5次或者6个月内联系人个人不足15人！");
                    iCUserRefConService.updateCUserRefConByUid(hhMap);
                    r.setCode(2);
                    r.setMsg("请三个月后重新申请");
                    return r;
                }

                logger.info("******************RefusingCredit.status:{}*****************", rfmap.get("contactsnumcheck").getStatus());
                logger.info("******************contactsnumcheck:{}*****************", rfmap.get("contactsnumcheck").getNumber());
                logger.info("******************contactCnt:{}*****************", contactCnt);

                if (rfmap.get("contactsnumcheck").getStatus().equals(RefusingCredit.status_1) &&
                        contactCnt < rfmap.get("contactsnumcheck").getNumber()){
                    //执行修改用户，运营商，资金端，商户关系表
                    hhMap.put("examinefailreason", "催收类电话大于5次或者6个月内联系人个人不足15人！");
                    iCUserRefConService.updateCUserRefConByUid(hhMap);
                    r.setCode(2);
                    r.setMsg("请三个月后重新申请");
                    return r;
                }

                //6个月内联系人不足15人则退出快速授信
                getCalculateQuotaByCallCnt(contactCnt, debtCallCnt, totalScore, creditVo);

                r.setCode(0);
                r.setMsg("success");


                BaseCreditInfo baseCreditInfo = new BaseCreditInfo();
                baseCreditInfo.setStatus("2");

                iBaseCreditInfoService.update(baseCreditInfo, new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()));


                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("authStatus", "1");
                hashMap.put("authMax", new BigDecimal("1500"));
                hashMap.put("authDate", new Date());
                hashMap.put("authExamine", "1");
                hashMap.put("uid", creditVo.getUid());
                hashMap.put("oid", creditVo.getOid());
                logger.info("*************fFundendBind:" + fFundendBind);
                hashMap.put("fid", fFundendBind.getFid());
                hashMap.put("accid", tbAccountInfo.getId());
                logger.info("*************执行修改用户，运营商，资金端，商户关系表*************");
                //执行修改用户，运营商，资金端，商户关系表
                iCUserRefConService.updateCUserRefConByUid(hashMap);


                FMoneyInfo fMoneyInfo = fMoneyInfoService.selectOne(new EntityWrapper<FMoneyInfo>().eq("fid", fFundendBind.getFid()));


                if (!ValidateUtil.isEmpty(fMoneyInfo) && "1".equals(fMoneyInfo.getStatus())) {
                    FMoneyOtherInfo fMoneyOtherInfo = new FMoneyOtherInfo();
                    fMoneyOtherInfo.setCreateDate(new Date());
                    fMoneyOtherInfo.setFid(fFundendBind.getFid());
                    fMoneyOtherInfo.setFmid(fMoneyInfo.getId());
                    fMoneyOtherInfo.setMoney(new BigDecimal("1500"));
                    fMoneyOtherInfo.setType("0");
                    fMoneyOtherInfo.setUid(creditVo.getUid());

                    //记录授信总金额出入明细表
                    fMoneyOtherInfoService.insert(fMoneyOtherInfo);
                }


                logger.info("登录任务执行成功");
                r.setCode(0);
                r.setMsg("success");
                return r;
            } else {
                r.setCode(2);
                r.setMsg("该运营商未绑定资金商，请先绑定资金商再进行授信");
            }

            logger.info("**********************************************************************************");

            return r;
        } else {
            if (Status.PhaseStatus.IMAGE_VERIFY_NEW.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus()) ||
                    Status.PhaseStatus.LOGIN_SUCCESS.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus())) {
                r.setData(((Status) pmap.get("loginResultStatus")).getExtra().getRemark());
                r.setCode(1);
                r.setMsg("需要进行图片验证码二次认证，请输入最新验证码");
                return r;
            } else if (Status.PhaseStatus.SMS_VERIFY_NEW.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus()) ||
                    Status.PhaseStatus.LOGIN_SUCCESS.equals(((Status) pmap.get("loginResultStatus")).getPhaseStatus())) {
                r.setCode(1);
                r.setData(((Status) pmap.get("loginResultStatus")).getExtra().getRemark());
                r.setMsg("需要进行短信验证码二次认证，请输入最新验证码");
                return r;
            } else {
                logger.info("登录任务执行失败,请重试！");
                r.setCode(2);
                r.setMsg(((Status) pmap.get("loginResultStatus")).getExtra().getRemark());
                return r;
            }
        }
        
    }





        //根据联系电话和催收类电话个数计算额度
    public Map<String,Object> getCalculateQuotaByCallCnt(Integer contactCnt,Integer debtCallCnt,Double totalScore, CreditVo creditVo){

        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        try {

            //查询当前版本

            RmSxtemplateRelationship selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(creditVo.getFid());

            String contactCntStr = contactCnt == null ? "0" : contactCnt.toString();
            String debtCallCntStr = debtCallCnt == null ? "0" : debtCallCnt.toString();

            //查询初始授信模板条件
            Map<String,RmBaseCreditType> map = baseCreditConfigService.getRmBaseCreditInitialList(creditVo);

            TbCreditGrade tbCreditGrade = new TbCreditGrade();

            tbCreditGrade.setOid(creditVo.getOid());
            tbCreditGrade.setStatus(TbCreditGrade.STATUS_Y);
            tbCreditGrade.setUid(creditVo.getUid());

            BigDecimal ts = new BigDecimal(totalScore);

            //根据电话记录数算额度
            BigDecimal tagMobileScore = baseCreditConfigService.tagMobileConvert(map,CreditConstant.CONTACTCNT_INFO,Double.parseDouble(contactCntStr));

            logger.info("***************标签类电话分值：{}", tagMobileScore);
            ts = ts.add(tagMobileScore);

            tbCreditGrade.setDetails(contactCntStr);
            tbCreditGrade.setCode(CreditConstant.TAGMOBILESCORE);
            tbCreditGrade.setName("标签类电话");
            tbCreditGrade.setValue(tagMobileScore + "");
            tbCreditGrade.setVersionId(selectRmSxtemplateRelationship.getId());

            iTbCreditGradeService.insert(tbCreditGrade);


            //根据催收电话记录数算额度
            BigDecimal debtCallScore = baseCreditConfigService.debtCallConvert(map,CreditConstant.DEBTCAll_INFO,Double.parseDouble(debtCallCntStr));
            logger.info("***************催收类电话：{}", debtCallScore);
            ts = ts.subtract(debtCallScore);

            TbCreditGrade tg = new TbCreditGrade();

            tg.setOid(creditVo.getOid());
            tg.setStatus(TbCreditGrade.STATUS_Y);
            tg.setUid(creditVo.getUid());
            tg.setDetails(debtCallCntStr);
            tg.setCode(CreditConstant.DEBTCALLSCORE);
            tg.setName("催收类电话");
            tg.setValue(debtCallScore + "");
            tg.setVersionId(selectRmSxtemplateRelationship.getId());

            iTbCreditGradeService.insert(tg);

            resultMap.put("totalScore",ts);
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return resultMap;
    }


    /**
     * 查询该用户基本信息
     * @param
     * @return
     */
    @RequestMapping(value = "getUserInfo", method = RequestMethod.POST)
    @ApiOperation(value = "查询该用户基本信息", notes = "查询该用户基本信息")
    public R<CUserInfo> getUserInfo(@RequestBody CreditVo creditVo) {

        try{

            CUserInfo tbi = iCUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("id", creditVo.getUid()));
            return R.<CUserInfo>newOK(tbi);

        }catch (Exception e){
            logger.error(e.getMessage());
            return R.newErrorMsg(e.getMessage());
        }
    }

    /**
     * 查询该用户快速授信基本信息
     * @param
     * @return
     */
    @RequestMapping(value = "getBaseCreditInfo", method = RequestMethod.POST)
    @ApiOperation(value = "查询该用户基本信息", notes = "查询该用户基本信息")
    public R<BaseCreditInfo> getBaseCreditInfo(@RequestBody CreditVo creditVo) {

        try{

            BaseCreditInfo bci = iBaseCreditInfoService.selectOne(new EntityWrapper<BaseCreditInfo>().eq("uid", creditVo.getUid()).eq("status", "1"));
            return R.<BaseCreditInfo>newOK(bci);

        }catch (Exception e){
            logger.error(e.getMessage());
            return R.newErrorMsg(e.getMessage());
        }
    }


    /**
     * 查询该用户授信基本信息
     * @param
     * @return
     */
    @RequestMapping(value = "getMycreditDetails", method = RequestMethod.POST)
    @ApiOperation(value = "查询该用户授信基本信息", notes = "查询该用户授信基本信息")
    public R<MycreditDetailsDto> getMycreditDetails(@RequestBody CreditVo creditVo) {

        try {
            HashMap<String, Object> hashMap = new HashMap<>();

            hashMap.put("uid",creditVo.getUid());
            hashMap.put("oid",creditVo.getOid());

            //查询这个人的额度详情
            MycreditDetailsDto mycreditDetailsDto = iCUserRefConService.getMycreditDetails(hashMap);

            //可用额度
            String balance = ValidateUtil.isEmpty(mycreditDetailsDto.getBalance()) ? "0" : DES.decryptdf(mycreditDetailsDto.getBalance());
            //快速授信额度
            String rapidMoney = ValidateUtil.isEmpty(mycreditDetailsDto.getRapidMoney()) ? "0" : DES.decryptdf(mycreditDetailsDto.getRapidMoney());

            BigDecimal bl = new Calculate(balance).format(2, RoundingMode.DOWN);
            balance = bl.toString();

            mycreditDetailsDto.setBalance(balance);
            mycreditDetailsDto.setRapidMoney(rapidMoney);

            return R.newOK(mycreditDetailsDto);

        }catch (Exception e){
            logger.info(e.getMessage());
            R.newErrorMsg(e.getMessage());
        }

        return R.newOK();
    }



    /**
     * 获取用户的授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "getTbAccountInfo", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户的授信信息", notes = "获取用户的授信信息")
    public R<CUserRefCon> getTbAccountInfo(@RequestBody CreditVo creditVo) throws Exception{
        R r = new R();

        HashMap<String, Object> hashMap = new HashMap<>();

        hashMap.put("uid",creditVo.getUid());
        hashMap.put("oid",creditVo.getOid());

        //查询这个人的额度详情
        CUserRefCon cUserRefCon = iCUserRefConService.getCUserRefConByUid(hashMap);

        r.setData(cUserRefCon);
        r.setCode(0);
        r.setMsg("success");
        return r;
    }


    /**
     * @Description:  接口返回结果收集
     * @Param: [jsonStr, type, typeName, uid]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/7/27
     */
    public void toAddJASONStr(String jsonStr, String type, String typeName, String uid)
    {
        CCreditUserInfo multipleNewUserInfo = new CCreditUserInfo();
        multipleNewUserInfo.setCreateTime(new Date());
        multipleNewUserInfo.setJsonStr(jsonStr);
        multipleNewUserInfo.setType(type);
        multipleNewUserInfo.setTypeName(typeName);
        multipleNewUserInfo.setUid(uid);
        //接口返回结果收集
        baseCreditConfigService.addJASONStr(multipleNewUserInfo);
    }



    public static String getAddressByIP()
    {
        try
        {
            String strIP = "222.240.156.19";
            URL url = new URL( "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=" + strIP);
            URLConnection conn = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "GBK"));
            String line = null;
            StringBuffer result = new StringBuffer();
            while((line = reader.readLine()) != null)
            {
                result.append(line);
            }
            reader.close();
            strIP = result.substring(result.indexOf( "该IP所在地为：" ));
            strIP = strIP.substring(strIP.indexOf( "：") + 1);
            String province = strIP.substring(6, strIP.indexOf("省"));
            String city = strIP.substring(strIP.indexOf("省") + 1, strIP.indexOf("市"));

            return "省：" + province + "市：" + city;
        }
        catch( IOException e)
        {
            return "读取失败";
        }
    }

    //根据身份证获取基本信息
//    public Map<String,Object> getCalculateQuota(String idCard, ArrayList<CreditPlatformRegistrationDetails> result, String type, String uid, String oid){
//
//        HashMap<String,Object> resultMap = new HashMap<String,Object>();
//        try {
//
//            Double totalScore = 0.0;
//            Map<String,Object> score = new HashMap<>();
//            Map<String,Object> credit = new HashMap<>();
//
//
//            List<BaseCreditConfig> list = baseCreditConfigService.getListByType("1");
//
//            if (!ValidateUtil.isEmpty(list)) {
//                for (int i = 0; i < list.size(); i++) {
//                    score.put(list.get(i).getName(), list.get(i).getValue());
//                    if (list.get(i).getSubList() != null) {
//                        for (int j = 0; j < list.get(i).getSubList().size(); j++) {
//                            credit.put(list.get(i).getSubList().get(j).getName(), list.get(i).getSubList().get(j).getValue());
//                        }
//                    }
//                }
//            }
//
//            logger.info("***************idCard:" + idCard + "******************");
//            logger.info("***************score:" + score + "******************");
//            logger.info("***************credit:" + credit + "******************");
//
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
//            Date date = new Date();
//            String age = idCard.substring(6,10);
//            String now = sdf.format(date);
//            Integer ageInt = Integer.parseInt(now)-Integer.parseInt(age);
//
//
//
//            //根据年龄获取对应额度
//            Double ageScore = baseCreditConfigService.ageConvert(idCard,score,credit);
//            logger.info("***************ageScore:" + ageScore + "******************");
//
//            TbCreditGrade tbCreditGrade = new TbCreditGrade();
//            tbCreditGrade.setOid(oid);
//            tbCreditGrade.setStatus("1");
//            tbCreditGrade.setUid(uid);
//            tbCreditGrade.setDetails(ageInt.toString());
//            tbCreditGrade.setCode(CreditConstant.AGESCORE);
//            tbCreditGrade.setName("年龄");
//            tbCreditGrade.setValue(ageScore.toString());
//            iTbCreditGradeService.insert(tbCreditGrade);
//
//            totalScore = totalScore+ageScore;
//            //根据性别获取对应额度
//            Map<String,Object> sexmap = baseCreditConfigService.sexConvert(idCard,null, score, credit);
//            logger.info("***************sex:" + sexmap.get("sexScore") + "******************");
//            totalScore = totalScore + Double.parseDouble(sexmap.get("sexScore").toString());
//            String sexTemp = idCard.substring(idCard.length()-2,idCard.length()-1);
//            TbCreditGrade tg = new TbCreditGrade();
//            tg.setOid(oid);
//            tg.setStatus("1");
//            tg.setUid(uid);
//
//            if(Integer.parseInt(sexTemp)%2==0)
//            {
//                tg.setDetails("女");
//            }else {
//                tg.setDetails("男");
//            }
//
//            tg.setCode(CreditConstant.SEXSCORE);
//            tg.setName("性别");
//            tg.setValue(sexmap.get("sexScore").toString());
//            iTbCreditGradeService.insert(tg);
//
//            //根据户籍获取对应额度
//            Double cityScore = baseCreditConfigService.cityConvert(idCard,score,credit,uid,oid);
//            logger.info("***************cityScore:" + cityScore + "******************");
//
//            totalScore = totalScore+cityScore;
//            //根据多头负债获取对应额度
//            Double mutipleScore = baseCreditConfigService.MutipleConvert(result,CreditConstant.MULTIPLE_INFO,score,credit,uid,oid);
//            logger.info("***************mutipleScore:" + mutipleScore + "******************");
//
//            totalScore = totalScore+mutipleScore;
//
//            //基本信息算完后的总额度
//            resultMap.put("totalScore",totalScore);
//            logger.info("***************totalScore:" + totalScore);
//        }catch (Exception e){
//            logger.error(e.getMessage());
//        }
//        return resultMap;
//    }




    /**
     * 计算总分
     * @param count 调用接口次数
     * @return
     * @throws InterruptedException
     */
    public Map<String,Object> getTotalScore(int count,String idCard,Map<String,Object> score,Map<String,Object> credit,Long totalScore) throws InterruptedException {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();

        try {

            String jsonStr ="";
            String threeElement="";
            threeElement = baseCreditConfigService.dataConvertScore(jsonStr,CreditConstant.CARD_INFO);
            if(threeElement.equals("false"))
            {
                resultMap.put("status","threeElement");
                return resultMap;
            }
            else if(threeElement.equals("error"))
            {
                count++;
                Thread.sleep(500);
                if(count<4)
                {
                    this.getTotalScore(count,idCard,score,credit,totalScore);
                }
            }
            resultMap.put("totalScore",totalScore);
            resultMap.put("status","success");
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return resultMap;

    }

    /**
     * 保存商业授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "saveBusinessCredit", method = RequestMethod.POST)
    @ApiOperation(value = "保存商业授信信息", notes = "保存商业授信信息")
    public R saveBusinessCredit(@RequestBody CreditVo creditVo){
        try{
            TbBusinessCredit tbBusinessCredit = new TbBusinessCredit();

            tbBusinessCredit.setAuthMax(creditVo.getAuthMax());
            tbBusinessCredit.setTotalTimes(creditVo.getTotaltimes());
            tbBusinessCredit.setCreateDate(new Date());
            tbBusinessCredit.setOid(creditVo.getOid());
            tbBusinessCredit.setUid(creditVo.getUid());
            tbBusinessCredit.setMid(creditVo.getMid());
            tbBusinessCredit.setStatus("2");

            //执行插入商业授信表
            iTbBusinessCreditService.insert(tbBusinessCredit);

        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newError();
        }
        return R.newOK();
    }

    /**
     * 保存商业授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "getTbBusinessCreditByUid", method = RequestMethod.POST)
    @ApiOperation(value = "保存商业授信信息", notes = "保存商业授信信息")
    public R<TbBusinessCredit> getTbBusinessCreditByUid(@RequestBody CreditVo creditVo){

        try{
            HashMap<String, Object> hashMap = new HashMap<>();

            hashMap.put("uid",creditVo.getUid());
            hashMap.put("oid",creditVo.getOid());
            //执行插入商业授信表
            TbBusinessCredit tbBusinessCredit = iTbBusinessCreditService.getTbBusinessCreditByUid(hashMap);
            logger.info("************tbBusinessCredit:" + tbBusinessCredit);
            return R.newOK(tbBusinessCredit);
        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newError();
        }
    }

    /**
     * 获取商业授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "getTbBusinessCreditDetailsByUid", method = RequestMethod.POST)
    @ApiOperation(value = "获取商业授信信息", notes = "获取商业授信信息")
    public R<HashMap<String, Object>> getTbBusinessCreditDetailsByUid(@RequestBody CreditVo creditVo){

        try{
            HashMap<String, Object> hashMap = new HashMap<>();

            hashMap.put("uid",creditVo.getUid());
            hashMap.put("oid",creditVo.getOid());
            //执行插入商业授信表
            HashMap<String, Object> map = iTbBusinessCreditService.getTbBusinessCreditDetailsByUid(hashMap);
            logger.info("************getTbBusinessCreditDetailsByUid:" + map);
            return R.newOK(map);
        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newError();
        }
    }


}
