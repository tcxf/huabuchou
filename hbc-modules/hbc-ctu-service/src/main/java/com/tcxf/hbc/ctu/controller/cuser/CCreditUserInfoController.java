package com.tcxf.hbc.ctu.controller.cuser;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.ctu.service.ICCreditUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
@RestController
@RequestMapping("/cCreditUserInfo")
public class CCreditUserInfoController extends BaseController {
    @Autowired private ICCreditUserInfoService cCreditUserInfoService;

    /**
    * 通过ID查询
    *
    * @param id ID
    * @return CCreditUserInfo
    */
    @GetMapping("/{id}")
    public CCreditUserInfo get(@PathVariable Integer id) {
        return cCreditUserInfoService.selectById(id);
    }


    /**
    * 分页查询信息
    *
    * @param params 分页对象
    * @return 分页对象
    */
    @RequestMapping("/page")
    public Page page(@RequestParam Map<String, Object> params) {
        params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return cCreditUserInfoService.selectPage(new Query<>(params), new EntityWrapper<>());
    }

    /**
     * 添加
     * @param  cCreditUserInfo  实体
     * @return success/false
     */
    @PostMapping
    public Boolean add(@RequestBody CCreditUserInfo cCreditUserInfo) {
        return cCreditUserInfoService.insert(cCreditUserInfo);
    }

    /**
     * 删除
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Integer id) {
        CCreditUserInfo cCreditUserInfo = new CCreditUserInfo();
//        cCreditUserInfo.setId(id);
//        cCreditUserInfo.setUpdateTime(new Date());
//        cCreditUserInfo.setDelFlag(CommonConstant.STATUS_DEL);
        return cCreditUserInfoService.updateById(cCreditUserInfo);
    }

    /**
     * 编辑
     * @param  cCreditUserInfo  实体
     * @return success/false
     */
    @PutMapping
    public Boolean edit(@RequestBody CCreditUserInfo cCreditUserInfo) {
        return cCreditUserInfoService.updateById(cCreditUserInfo);
    }
}
