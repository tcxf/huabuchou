package com.tcxf.hbc.ctu.controller.cuser;
import java.io.*;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.Assert;
import com.tcxf.hbc.common.util.R;
import com.xiaoleilu.hutool.io.FileUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.ctu.service.ICUserInfoService;
import com.tcxf.hbc.common.web.BaseController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 消费者用户表 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-26
 */
@RestController
@RequestMapping("/cUserInfo")
public class CUserInfoController extends BaseController {

    @Autowired private ICUserInfoService cUserInfoService;

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return CUserInfo
     */
    @GetMapping("/{id}")
    public CUserInfo get(@PathVariable Integer id)
    {
        return cUserInfoService.selectById(id);
    }

    @GetMapping("/getUserById/{id}")
    @ApiOperation(value = "根据userId查询用户信息", notes = "根据userId查询用户信息")
    @ResponseBody
    public CUserInfo getUserInfo(@PathVariable String id)
    {
        return cUserInfoService.getUserInfo(id);
    }

    @GetMapping("/delUserInfo/{id}")
    @ApiOperation(value = "根据userId删除用户信息", notes = "根据userId删除用户信息")
    @ResponseBody
    public R<Boolean> delUserInfo(@PathVariable String id)
    {
        cUserInfoService.delUserInfo(id);
        MessageFormat.format("sss{0},sdfsd{1}", new Object[]{1,"sss"});
        return new R<>(true);
    }

    public  void sendSms(String mobile,String buzzType,String smsTempateId, Object args)
    {
        String tempStr =  "" ;
         String content = MessageFormat.format(tempStr, args);
        this.sendSms(mobile,buzzType,content);
    }

    public  void sendSms(String mobile,String buzzType,String content)
    {

    }

    @GetMapping("/selectUserInfoPage/{status}")
    @ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息")
    @ApiImplicitParam(name = "status", value = "用户状态", required = true, dataType = "String", paramType = "path")
    @ResponseBody
    public Page selectUserInfoPage(@PathVariable String status)
    {
        Page page = cUserInfoService.queryList(status);
        return page;
    }


    /**
     * 分页查询信息
     *
     * @param params 分页对象
     * @return 分页对象
     */
    @RequestMapping(value="/page",method = RequestMethod.POST)
    @ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息",httpMethod ="POST")
    public Page page(@RequestParam Map<String, Object> params)
    {
        // params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return cUserInfoService.selectPage(new Query<>(params), new EntityWrapper<>());
    }

    /**
     * 添加用户信息
     * @param  cUserInfo  实体
     * @return success/false
     */
    @RequestMapping(value="/add",method = RequestMethod.POST)
    @ApiOperation(value = "新增用户信息", notes = "新增用户信息",httpMethod ="POST")
    public R<Boolean> add(@RequestBody CUserInfo cUserInfo)
    {
        Assert.validateEntity(cUserInfo);

       // cUserInfo.setId(IdWorker.getId());
        boolean result = cUserInfoService.insert(cUserInfo);
        return new R<>(result);
    }


    /**
     * 编辑
     * @param  cUserInfo  实体
     * @return success/false
     */
    @PutMapping
    public Boolean edit(@RequestBody CUserInfo cUserInfo)
    {
        cUserInfo.setModifyDate(new Date());
        return cUserInfoService.updateById(cUserInfo);
    }

}
