package com.tcxf.hbc.ctu.controller.cuser;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.TbBusinessCredit;
import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.ctu.service.ITbCreditGradeService;
import com.tcxf.hbc.ctu.service.ITbUpMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 授信分值额度
 * jiangyong
 */
@RestController
@RequestMapping("/CreditScoreController")
public class CreditScoreController {
    @Autowired
    private ITbUpMoneyService tbUpMoneyService;

    @Autowired
    private ITbCreditGradeService iTbCreditGradeService;

    @RequestMapping(value = "/tbUpMoney/{uid}",method = {RequestMethod.POST})
    public R  CreditScore(@PathVariable("uid")String uid){
        R r=new R();
        List <TbUpMoney> uid1 = tbUpMoneyService.selectList(new EntityWrapper <TbUpMoney>().eq("uid", uid));
        r.setData(uid1);
        return  r;
    }


    @RequestMapping(value = "/tbCreditGrade/{uid}",method = {RequestMethod.POST})
    public R  CreditScores(@PathVariable("uid")String uid){
        R r=new R();
        List <TbCreditGrade> uid2 = iTbCreditGradeService.selectList(new EntityWrapper <TbCreditGrade>().eq("uid", uid));
        r.setData(uid2);
        return  r;
    }

}
