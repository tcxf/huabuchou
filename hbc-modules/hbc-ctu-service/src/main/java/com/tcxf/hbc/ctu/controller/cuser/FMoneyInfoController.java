package com.tcxf.hbc.ctu.controller.cuser;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.BaseCreditInfo;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.ctu.service.IFFundendBindService;
import com.tcxf.hbc.ctu.service.IFMoneyInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.web.BaseController;

/**
 * <p>
 * 资金端授信上限表 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@RestController
@RequestMapping("/fMoneyInfo")
public class FMoneyInfoController extends BaseController {

    @Autowired private IFMoneyInfoService fMoneyInfoService;

    @Autowired
    private IFFundendBindService iFFundendBindService;


    /**
     * 获取商业授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "getFMoneyInfoByFid", method = RequestMethod.POST)
    @ApiOperation(value = "获取资金端剩余授信额度", notes = "获取资金端剩余授信额度")
    public R<FMoneyInfo> getFMoneyInfoByFid(@RequestBody CreditVo creditVo){

        try{
            logger.error("***************creditVo.getOid():" + creditVo.getOid());
            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());
            logger.error("***************fFundendBind.getFid():" + fFundendBind.getFid());
            if (!ValidateUtil.isEmpty(fFundendBind)){
                FMoneyInfo fMoneyInfo = fMoneyInfoService.selectOne(new EntityWrapper<FMoneyInfo>().eq("fid", fFundendBind.getFid()));
                logger.error("***************fFundendBind.getFid():" + fFundendBind.getFid());
                return R.newOK(fMoneyInfo);
            }else {
                return R.newError("没有资金端");
            }

        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newError();
        }
    }

}
