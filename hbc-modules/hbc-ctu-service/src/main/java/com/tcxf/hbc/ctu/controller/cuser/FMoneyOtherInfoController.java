package com.tcxf.hbc.ctu.controller.cuser;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.ctu.service.IFFundendBindService;
import com.tcxf.hbc.ctu.service.IFMoneyInfoService;
import com.tcxf.hbc.ctu.service.IFMoneyOtherInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.web.BaseController;

/**
 * <p>
 * 授信总金额出入明细表 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@RestController
@RequestMapping("/fMoneyOtherInfo")
public class FMoneyOtherInfoController extends BaseController {
    @Autowired private IFMoneyOtherInfoService fMoneyOtherInfoService;

    @Autowired
    private IFFundendBindService iFFundendBindService;

    @Autowired private IFMoneyInfoService fMoneyInfoService;

    /**
     * 获取商业授信信息
     * @param
     * @return
     */
    @RequestMapping(value = "getFMoneyOtherInfoByFid", method = RequestMethod.POST)
    @ApiOperation(value = "获取资金端已使用授信额度", notes = "获取资金端已使用授信额度")
    public R<BigDecimal> getFMoneyOtherInfoByFid(@RequestBody CreditVo creditVo){

        try{

            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(creditVo.getOid());
            if (!ValidateUtil.isEmpty(fFundendBind)){
                FMoneyInfo fMoneyInfo = fMoneyInfoService.selectOne(new EntityWrapper<FMoneyInfo>().eq("fid", fFundendBind.getFid()));

                Date starttimes = new Date();

                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

                HashMap<String, Object> hmap = new HashMap<>();
                hmap.put("fmid", fMoneyInfo.getId());
                String starttime = sf.format(starttimes) + " 00:00:00";
                String endtime = sf.format(starttimes) + " 23:59:59";
                hmap.put("starttime", starttime);
                hmap.put("endtime", endtime);


                BigDecimal sum = fMoneyOtherInfoService.getFMoneyOtherInfoSumByFid(hmap);

                if (ValidateUtil.isEmpty(sum)){
                    sum = new BigDecimal("0");
                }

                return R.newOK(sum);
            }else {
                return R.newError("没有资金端");
            }

        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newError();
        }
    }


    public static void main(String[] args) {

        Date d = new Date();

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        System.out.println(sf.format(d));
    }


}
