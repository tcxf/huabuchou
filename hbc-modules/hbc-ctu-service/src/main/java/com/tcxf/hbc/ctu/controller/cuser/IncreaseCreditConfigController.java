package com.tcxf.hbc.ctu.controller.cuser;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.gson.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.BalanceVo;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.vo.IncreaseCreditVo;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.ctu.common.constant.CreditConstant;
import com.tcxf.hbc.ctu.common.constant.IncreaseCreditConstant;
import com.tcxf.hbc.ctu.common.util.data.chsi.EducationInfoList;
import com.tcxf.hbc.ctu.common.util.data.ecommerce.EcommerceAddressDTO;
import com.tcxf.hbc.ctu.common.util.data.ecommerceReport.TaobaoAddressReportDTO;
import com.tcxf.hbc.ctu.common.util.data.housefund.HouseFundBillDTOList;
import com.tcxf.hbc.ctu.common.util.data.jd.AddressList;
import com.tcxf.hbc.ctu.common.util.data.shebao.BillRecordDTOS;
import com.tcxf.hbc.ctu.common.util.gxb.*;
import com.tcxf.hbc.ctu.common.util.parm.AuthToken;
import com.tcxf.hbc.ctu.common.util.secure.DES;
import com.tcxf.hbc.ctu.model.dto.CallBackParam;
import com.tcxf.hbc.ctu.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pegnjin
 * @since 2018-07-10
 */
@RestController
@RequestMapping("/increaseCreditConfig")
public class IncreaseCreditConfigController extends BaseController {

    @Autowired private FMoneyOtherInfoController fMoneyOtherInfoController;

    @Autowired private FMoneyInfoController fMoneyInfoController;

    @Autowired private IFMoneyOtherInfoService fMoneyOtherInfoService;

    @Autowired private IFMoneyInfoService fMoneyInfoService;

    @Autowired
    private IBaseCreditConfigService baseCreditConfigService;

    @Autowired
    ITbUpMoneyService iTbUpMoneyService;

    @Autowired
    private BaseCreditConfigController baseCreditConfigController;

    @Autowired
    ITbAccountInfoService iTbAccountInfoService;

    @Autowired
    private IBaseInterfaceInfoService iBaseInterfaceInfoService;

    @Autowired
    private IFFundendBindService iFFundendBindService;

    @Autowired
    private ICUserRefConService iCUserRefConService;


    /**
     * 提额回调
     * @return
     */
    @RequestMapping(value = "/callBack")
    @ApiOperation(value = "提额回调", notes = "提额回调")
    public R callBack(String authJson, String token, String OId){

        logger.info("*****************token:" + token);

        JsonObject jsonObject = new JsonParser().parse(authJson).getAsJsonObject();

        returnResultProcessing(jsonObject,token,OId);

        return R.newOK();

    }


    /**
     * 返回结果处理方法
     * @param jsonObject
     */
    public void returnResultProcessing(JsonObject jsonObject, String token, String OId){

        try {

            Gson googleJson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

            //快速授信必须要有前提条件，运营商必须要绑定资金商，否则不能做快速授信
            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(OId);

            //查询初始授信模板条件
            Map<String,RmBaseCreditType> map = iTbUpMoneyService.getRmBaseCreditUpmoneyList(fFundendBind.getFid());

            /** 社保返回参数 start **/

            if (jsonObject != null && jsonObject.get("billRecordDTOS") != null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("billRecordDTOS");

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.SOCIALINSURANCE));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    //接口返回结果收集
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.SHEBAOTYPE, "社保返回参数接口", tbUpMoney.getUid());
                }


                logger.info("******************社保jsonArray:" + jsonArray);
                List<BillRecordDTOS> brdl = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    brdl.add(googleJson.fromJson(jsonElement, BillRecordDTOS.class));
                }
                Boolean flag = true;

                for (int i = 0; i < brdl.size(); i++) {

                    if (i == 0) {
                        try {
                            //获取最近缴费的日期
                            Date payDate = brdl.get(i).getPayDate();
                            Date currentDate = new Date();
                            Long between = (currentDate.getTime() - payDate.getTime()) / 86400000;//上一次缴纳社保距离今天的天数
                            if (between > 30) {
                                flag = false;
                                break;
                            }

                            logger.info("缴费社保最近月份离今天的时间：" + between);

                        } catch (Exception e) {
                            logger.error("缴费社保异常：" + e.getMessage());
                        }
                    }
                }

                //缴纳社保次数
                int count = 0;
                if (flag) {
                    count = brdl.size() / 5;
                }

                iTbUpMoneyService.getSocialInsuranceScore(map, count, token);

            }
            /** 社保返回参数 end **/

            /** 公积金返回参数 start **/
            if (jsonObject != null && jsonObject.get("houseFundBillDTOList") != null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("houseFundBillDTOList");

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.ACCUMULATIONFUND));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.HOUSEFUNDTYPE, "公积金返回参数接口", tbUpMoney.getUid());
                }

                logger.info("******************公积金jsonArray:" + jsonArray);
                List<HouseFundBillDTOList> brdl = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    brdl.add(googleJson.fromJson(jsonElement, HouseFundBillDTOList.class));
                }
                Boolean flag = true;

                for (int i = 0; i < brdl.size(); i++) {

                    if (i == 0) {
                        try {
                            //获取最近缴费的日期
                            Date payDate = brdl.get(i).getBillDate();
                            Date currentDate = new Date();
                            Long between = (currentDate.getTime() - payDate.getTime()) / 86400000;//上一次缴纳公积金距离今天的天数
                            if (between > 30) {
                                flag = false;
                                break;
                            }

                            logger.info("缴费公积金最近月份离今天的时间：" + between);

                        } catch (Exception e) {
                            logger.error("缴费公积金异常：" + e.getMessage());
                        }
                    }
                }

                //缴纳公积金次数
                int count = 0;
                if (flag) {
                    count = brdl.size();
                }

                iTbUpMoneyService.getSocialInsuranceScore(map, count, token);

            }
            /** 公积金返回参数 end **/

            /** 学信网返回参数 start **/
            if (jsonObject != null && jsonObject.get("educationInfoList") != null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("educationInfoList");

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.EDUCATIONAPPROVE));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.CHSITYPE, "学信网返回参数接口", tbUpMoney.getUid());
                }

                logger.info("******************学信网jsonArray:" + jsonArray);
                List<EducationInfoList> brdl = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    brdl.add(googleJson.fromJson(jsonElement, EducationInfoList.class));
                }
                //最近学历
                String degree = null;

                for (int i = 0; i < brdl.size(); i++) {

                    if (i == 0) {
                        try {
                            //获取最近学历
                            degree = brdl.get(i).getDegree();
                            logger.info("最终学历为：" + degree);
                            break;

                        } catch (Exception e) {
                            logger.error("获取学历异常：" + e.getMessage());
                        }
                    }
                }

                iTbUpMoneyService.getEducationInfoScore(map, degree, token);

            }
            /** 学信网返回参数 end **/

            /** 淘宝返回参数 start **/
            if (jsonObject != null && jsonObject.get("ecommerceConsigneeAddresses") != null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("ecommerceConsigneeAddresses");

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.TAOBAOAPPROVE));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.ECOMMERCETYPE, "淘宝返回参数接口", tbUpMoney.getUid());
                }

                logger.info("******************淘宝jsonArray:" + jsonArray);
                List<EcommerceAddressDTO> brdl = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    brdl.add(googleJson.fromJson(jsonElement, EcommerceAddressDTO.class));
                }

                int addresscount = 0;
                int mobilecount = 0;
                Set<String> set = new HashSet<>();
                for (int i = 0; i < brdl.size(); i++) {

                    try {
                        //获取收货号码
                        String telNumber = brdl.get(i).getTelNumber();
                        set.add(telNumber);

                        logger.info("收货号码为：" + telNumber);
                        break;

                    } catch (Exception e) {
                        logger.error("获取收货号码异常：" + e.getMessage());
                    }
                }
                //抓取地址数量
                addresscount = brdl.size();
                //联系人号码数量
                mobilecount = set.size();

                iTbUpMoneyService.getTaobaoApprove(map, addresscount, mobilecount, token);
            }
            /** 淘宝返回参数 end **/

            /** 京东返回参数 start **/
            if (jsonObject != null && jsonObject.get("addressList") != null) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("addressList");

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.JDCOMAPPROVE));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.JDTYPE, "京东返回参数接口", tbUpMoney.getUid());
                }

                logger.info("******************京东jsonArray:" + jsonArray);
                List<AddressList> brdl = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    brdl.add(googleJson.fromJson(jsonElement, AddressList.class));
                }

                int addresscount = 0;
                int mobilecount = 0;
                Set<String> set = new HashSet<>();
                for (int i = 0; i < brdl.size(); i++) {

                    try {
                        //获取收货号码
                        String mobilePhone = brdl.get(i).getMobilePhone();
                        set.add(mobilePhone);

                        logger.info("收货号码为：" + mobilePhone);
                        break;

                    } catch (Exception e) {
                        logger.error("获取收货号码异常：" + e.getMessage());
                    }
                }
                //抓取地址数量
                addresscount = brdl.size();
                //联系人号码数量
                mobilecount = set.size();

                iTbUpMoneyService.getTaobaoApprove(map, addresscount, mobilecount, token);
            }
            /** 京东返回参数 end **/

            /** 滴滴芝麻分返回参数 start **/
            if (jsonObject != null && jsonObject.get("sesameScore") != null) {
                String sesameScore = jsonObject.get("sesameScore").toString();

                TbUpMoney tbUpMoney = iTbUpMoneyService.selectOne(new EntityWrapper<TbUpMoney>().eq("token", token).eq("code", IncreaseCreditConstant.DIDIDACHEAPPROVE));

                if (!ValidateUtil.isEmpty(tbUpMoney)){
                    baseCreditConfigController.toAddJASONStr(jsonObject.toString(), CreditConstant.SESAMESCORETYPE, "滴滴芝麻分返回参数接口", tbUpMoney.getUid());
                }

                logger.info("******************滴滴sesameScore:" + sesameScore);
                iTbUpMoneyService.getDidiDacheApprove(map, sesameScore, token);
            }
            /** 滴滴芝麻分返回参数 end **/

        }catch (Exception e){
            logger.error("**************提额异常：{}", e.getMessage());
        }

    }

    public Map<String,Object> getCreditConfig(){
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        Map<String,Object> score = new HashMap<>();
        Map<String,Object> credit = new HashMap<>();

        List<BaseCreditConfig> list = baseCreditConfigService.getListByType("2");

        if (!ValidateUtil.isEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                score.put(list.get(i).getName(), list.get(i).getValue());
                if (list.get(i).getSubList() != null) {
                    for (int j = 0; j < list.get(i).getSubList().size(); j++) {
                        credit.put(list.get(i).getSubList().get(j).getName(), list.get(i).getSubList().get(j).getValue());
                    }
                }
            }
        }

        return resultMap;
    }

    /**
     * 进入提额界面-获取需输入项
     * @return
     */
    @RequestMapping(value = "/toIncreaseCredit" , method = RequestMethod.POST)
    @ApiOperation(value = "进入提额界面-获取需输入项", notes = "进入提额界面-获取需输入项")
    public R<List<TbUpMoney>> toIncreaseCredit(@RequestBody TbUpMoney tbUpMoney){

        HashMap<String,Object> hmap = new HashMap<>();
        hmap.put("uid",tbUpMoney.getUid());
        hmap.put("oid",tbUpMoney.getOid());

        List<TbUpMoney> list = iTbUpMoneyService.getOneIncreaseCreditByUid(hmap);

        return R.newOK(list);
    }


    /**
     * 进入紧急联系人界面
     * @return
     */
    @RequestMapping(value = "/toEmergencyContact" , method = RequestMethod.POST)
    @ApiOperation(value = "进入紧急联系人界面", notes = "进入紧急联系人界面")
    public R<List<TbUpMoney>> toEmergencyContact(){

        R r = new R();
//        if(tbUpMoney != null){
//            HashMap<String,Object> hmap = new HashMap<>();
//            hmap.put("uid",tbUpMoney.getUid());
//            hmap.put("parentid",tbUpMoney.getParentId());
//            hmap.put("oid",tbUpMoney.getOid());
//
//            List<TbUpMoney> list = iTbUpMoneyService.getTwoIncreaseCreditByUid(hmap);
//            r.setData(list);
//        }

        r.setCode(0);
        r.setMsg("success");
        return r;
    }

    /**
     * 保存紧急联系人
     * @return
     */
    @RequestMapping(value = "/saveEmergencyContact" , method = RequestMethod.POST)
    @ApiOperation(value = "保存紧急联系人", notes = "保存紧急联系人")
    public R saveEmergencyContact(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
//        if (increaseCreditVo.getParentId() != null && !"".equals(increaseCreditVo.getParentId())){
//            List<TbUpMoney> tbUpMoneylist = increaseCreditVo.getIncreaseCreditlist();
//            for (TbUpMoney tbUpMoney:tbUpMoneylist) {
//                HashMap<String,Object> hmap = new HashMap<>();
//                hmap.put("id",tbUpMoney.getId());
//                hmap.put("value",tbUpMoney.getValue());
//                iTbUpMoneyService.updateTwoIncreaseCreditByid(hmap);
//            }
//
//        }else {
        TbUpMoney tm = new TbUpMoney();
        if ("emergencyContact".equals(increaseCreditVo.getType())){
            tm.setCode(IncreaseCreditConstant.EMERGENCY_CONTACT);
            tm.setName("紧急联系人");
        }else if ("qqOrWeChat".equals(increaseCreditVo.getType())){
            tm.setCode(IncreaseCreditConstant.QQORWECHAT);
            tm.setName("QQ/微信");
        }
        tm.setGrade("1");
        tm.setStatus("1");
        tm.setUid(increaseCreditVo.getUid());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);
        List<TbUpMoney> tbUpMoneylist = increaseCreditVo.getIncreaseCreditlist();
        for (TbUpMoney tbUpMoney:tbUpMoneylist) {
            tbUpMoney.setUid(increaseCreditVo.getUid());
//                tbUpMoney.setName("联系人姓名");
            tbUpMoney.setGrade("2");
            tbUpMoney.setParentId(tm.getId());
            tbUpMoney.setOid(increaseCreditVo.getOid());
//                tbUpMoney.setCode(IncreaseCreditConstant.CONTACTNAME);
            iTbUpMoneyService.insertOneIncreaseCredit(tbUpMoney);
        }

//        }
        r.setCode(0);
        r.setMsg("success");

        return r;
    }


    /**
     * 进入社保认证界面
     * @return
     */
    @RequestMapping(value = "/toSocialInsurance" , method = RequestMethod.POST)
    @ApiOperation(value = "进入社保认证界面", notes = "进入社保认证界面")
    public R<String> toSocialInsurance(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        ShebaoTaskTest ott = new ShebaoTaskTest();

        ott.setIdcard(increaseCreditVo.getIdcard());
        ott.setName(increaseCreditVo.getName());
        ott.setPhone(increaseCreditVo.getPhone());


        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();

        ott.setAppId(ibfismap.get("appId").toString());
        ott.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = ott.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String sheBaoUrl = ibfismap.get("shebaoUrl").toString() + token;
//                String sheBaoUrl = IncreaseCreditConstant.SHEBAOURL + authToken.getToken();
        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.SOCIALINSURANCE);
        tm.setName("社保认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(sheBaoUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }

    /**
     * 拉取社保授权返回结果
     * @return
     */
    @RequestMapping(value = "/getSocialInsuranceDate" , method = RequestMethod.POST)
    @ApiOperation(value = "拉取社保授权返回结果", notes = "拉取社保授权返回结果")
    public R getSocialInsuranceDate(HttpServletRequest request){
        R r = new R();
        String token = request.getSession().getAttribute("token").toString();
        OperatorTaskTest ott = new OperatorTaskTest();

        AuthToken authToken = new AuthToken();
        authToken.setToken(token);

        HashMap<String,Object> params  = new HashMap<>();

        params.put("authToken", authToken);

        ott.getSocialInsuranceDate(params);

        return r;
    }

    /**
     * 保存社保认证信息
     * @return
     */
    public R saveSocialInsurance(){

        R r = new R();
        OperatorTaskTest ott = new OperatorTaskTest();
        AuthToken authToken = ott.getAuthToken();


        return r;
    }



    /**
     * 进入公积金认证界面
     * @return
     */
    @RequestMapping(value = "/toAccumulationFund" , method = RequestMethod.POST)
    @ApiOperation(value = "进入公积金认证界面", notes = "进入公积金认证界面")
    public R<String> toAccumulationFund(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        HouseFundTaskTest ott = new HouseFundTaskTest();
        ott.setIdcard(increaseCreditVo.getIdcard());
        ott.setName(increaseCreditVo.getName());
        ott.setPhone(increaseCreditVo.getPhone());

        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        ott.setAppId(ibfismap.get("appId").toString());
        ott.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = ott.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String housefundUrl = ibfismap.get("housefundUrl").toString() + token;

//        String housefundUrl = IncreaseCreditConstant.HOUSEFUNDURL + authToken.getToken();

        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.ACCUMULATIONFUND);
        tm.setName("公积金认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(housefundUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }

    /**
     * 保存公积金认证信息
     * @return
     */
    public R saveAccumulationFund(){

        R r = new R();

        return r;
    }

    /**
     * 进入学历认证界面
     * @return
     */
    @RequestMapping(value = "/toEducationApprove" , method = RequestMethod.POST)
    @ApiOperation(value = "进入学历认证界面", notes = "进入学历认证界面")
    public R<String> toEducationApprove(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        ChsiTaskTest ott = new ChsiTaskTest();
        ott.setIdcard(increaseCreditVo.getIdcard());
        ott.setName(increaseCreditVo.getName());
        ott.setPhone(increaseCreditVo.getPhone());

        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        ott.setAppId(ibfismap.get("appId").toString());
        ott.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = ott.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String chsiUrl = ibfismap.get("chsiUrl").toString() + token;

//        String chsiUrl = IncreaseCreditConstant.CHSIURL + authToken.getToken();

        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.EDUCATIONAPPROVE);
        tm.setName("学历认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(chsiUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }

    /**
     * 保存学历认证信息
     * @return
     */
    public R saveEducationApprove(){

        R r = new R();

        return r;
    }

    /**
     * 进入淘宝认证界面
     * @return
     */
    @RequestMapping(value = "/toTaobaoApprove" , method = RequestMethod.POST)
    @ApiOperation(value = "进入淘宝认证界面", notes = "进入淘宝认证界面")
    public R<String> toTaobaoApprove(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        TaobaoTaskTest ott = new TaobaoTaskTest();
        ott.setIdcard(increaseCreditVo.getIdcard());
        ott.setName(increaseCreditVo.getName());
        ott.setPhone(increaseCreditVo.getPhone());

        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        ott.setAppId(ibfismap.get("appId").toString());
        ott.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = ott.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String ecommerceUrl = ibfismap.get("ecommerceUrl").toString() + token;

//        String ecommerceUrl = IncreaseCreditConstant.ECOMMERCEURL + authToken.getToken();

        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.TAOBAOAPPROVE);
        tm.setName("淘宝认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(ecommerceUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }

    /**
     * 保存淘宝认证信息
     * @return
     */
    public R saveTaobaoApprove(){

        R r = new R();

        return r;
    }

    /**
     * 进入支付宝认证界面
     * @return
     */
    @RequestMapping(value = "/toAlipayApprove" , method = RequestMethod.POST)
    @ApiOperation(value = "进入支付宝认证界面", notes = "进入支付宝认证界面")
    public R<String> toAlipayApprove(){

        R r = new R();
        OperatorTaskTest ott = new OperatorTaskTest();
        AuthToken authToken = ott.getAuthToken();

        String sheBaoUrl = "https://prod.gxb.io/v2/auth/ecommerce?returnUrl=https://prod.gxb.io&token=" + authToken.getToken();

        r.setData(sheBaoUrl);
        r.setCode(0);
        r.setMsg("success");


        return r;
    }

    /**
     * 保存支付宝认证信息
     * @return
     */
    public R saveAlipayApprove(){

        R r = new R();

        return r;
    }

    /**
     * 进入京东认证界面
     * @return
     */
    @RequestMapping(value = "/toJDcomApprove" , method = RequestMethod.POST)
    @ApiOperation(value = "进入京东认证界面", notes = "进入京东认证界面")
    public R<String> toJDcomApprove(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        JDTaskTest ott = new JDTaskTest();
        ott.setIdcard(increaseCreditVo.getIdcard());
        ott.setName(increaseCreditVo.getName());
        ott.setPhone(increaseCreditVo.getPhone());

        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        ott.setAppId(ibfismap.get("appId").toString());
        ott.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = ott.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String jdUrl = ibfismap.get("jdUrl").toString() + token;

//        String jdUrl = IncreaseCreditConstant.JDURL + authToken.getToken();

        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.JDCOMAPPROVE);
        tm.setName("京东认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(jdUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }

    /**
     * 保存京东认证信息
     * @return
     */
    public R saveJDcomApprove(){

        R r = new R();

        return r;
    }

    /**
     * 进入滴滴认证界面
     * @return
     */
    @RequestMapping(value = "/toDidiDacheApprove" , method = RequestMethod.POST)
    @ApiOperation(value = "进入滴滴认证界面", notes = "进入滴滴认证界面")
    public R<String> toDidiDacheApprove(@RequestBody IncreaseCreditVo increaseCreditVo){

        R r = new R();
        DidiTaskTest dtt = new DidiTaskTest();
        dtt.setIdcard(increaseCreditVo.getIdcard());
        dtt.setName(increaseCreditVo.getName());
        dtt.setPhone(increaseCreditVo.getPhone());


        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        dtt.setAppId(ibfismap.get("appId").toString());
        dtt.setAppSecurity(ibfismap.get("appSecurity").toString());

        AuthToken authToken = dtt.getAuthToken();
        String token = "";
        if (!ValidateUtil.isEmpty(authToken.getToken())){
            token = authToken.getToken();
        }
        String didiZmUrl = ibfismap.get("didizmUrl").toString() + token;

//        String didiZmUrl = IncreaseCreditConstant.DIDIZMURL + authToken.getToken();

        TbUpMoney tm = new TbUpMoney();
        tm.setUid(increaseCreditVo.getUid());
        tm.setGrade("1");
        tm.setCode(IncreaseCreditConstant.DIDIDACHEAPPROVE);
        tm.setName("滴滴认证");
        tm.setToken(authToken.getToken());
        tm.setOid(increaseCreditVo.getOid());
        iTbUpMoneyService.insertOneIncreaseCredit(tm);

        r.setData(didiZmUrl);
        r.setCode(0);
        r.setMsg("success");

        return r;
    }


    /**
     * 保存滴滴认证信息
     * @return
     */
    public R saveDidiDacheApprove(){

        R r = new R();

        return r;
    }

    /**
     * 保存滴滴认证信息
     * @return
     */
    public R saveIncreaseCredit(){

        R r = new R();

        return r;
    }

    /**
     * 执行提额程序
     * @return
     */
    @RequestMapping(value = "/toSaveIncreaseCredit" , method = RequestMethod.POST)
    @ApiOperation(value = "执行提额程序", notes = "执行提额程序")
    @Transactional
    public R<BalanceVo> toSaveIncreaseCredit(@RequestBody IncreaseCreditVo increaseCreditVo) {
        R r = new R();

        try {

            HashMap<String, Object> map = new HashMap<>();
            map.put("uid", increaseCreditVo.getUid());
            map.put("oid", increaseCreditVo.getOid());

            Map<String,Object> score = new HashMap<>();
            Map<String,Object> credit = new HashMap<>();

            //拿到Fid
            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(increaseCreditVo.getOid());

            Integer onecount = iTbUpMoneyService.getNotTokenIncreaseCreditByUid(map);

            logger.info("******************提额必填完成项：" + onecount);

            if (onecount <= 1){
                r.setCode(1);
                r.setMsg("紧急联系人和QQ/微信必填");
                return r;
            }

            Integer count = iTbUpMoneyService.getTokenIncreaseCreditByUid(map);

            logger.info("******************提额完成项：" + count);

            if (count <= 0){
                r.setCode(1);
                r.setMsg("至少完成一项方可进行提额，完善越多提额越高");
                return r;
            }

            Integer permitcount = iTbUpMoneyService.getTokenIncreaseCreditPermitByUid(map);

            logger.info("******************待提额项：" + permitcount);

            if (permitcount <= 0){
                r.setCode(1);
                r.setMsg("无待提额项，请完善更多信息");
                return r;
            }

            List<BaseCreditConfig> list = baseCreditConfigService.getListByType("2");

            if (!ValidateUtil.isEmpty(list)) {
                for (int i = 0; i < list.size(); i++) {
                    score.put(list.get(i).getName(), list.get(i).getValue());
                    if (list.get(i).getSubList() != null) {
                        for (int j = 0; j < list.get(i).getSubList().size(); j++) {
                            credit.put(list.get(i).getSubList().get(j).getName(), list.get(i).getSubList().get(j).getValue());
                        }
                    }
                }
            }


            //查询初始授信模板条件
            Map<String,RmBaseCreditType> rbctmap = iTbUpMoneyService.getRmBaseCreditUpmoneyList(fFundendBind.getFid());


            logger.info("********执行还款提额额度********");
            //还款提额额度
            BigDecimal pmCredit = iTbUpMoneyService.getPaymentsCredit(score,credit,increaseCreditVo.getUid(),increaseCreditVo.getOid(),rbctmap);

            Double paymentsCredit = 0.0;
            if (!ValidateUtil.isEmpty(pmCredit)){
                paymentsCredit = Double.parseDouble(pmCredit.toString());
            }

            logger.info("********执行授权项提额额度********");
            //授权项提额额度
            List<TbUpMoney> tokenlist = iTbUpMoneyService.getOneTokenIncreaseCreditByUid(map);
            Double tokencredit = 0.0;

            for (TbUpMoney tbUpMoney:tokenlist
                    ) {
                Double value = tbUpMoney.getValue() == null || "".equals(tbUpMoney.getValue()) ? 0 : Double.parseDouble(tbUpMoney.getValue());
                tokencredit = tokencredit + value;
            }

            paymentsCredit = paymentsCredit != null ? paymentsCredit : 0;

            //此次提额总额度
            Double sumcredit = paymentsCredit + tokencredit;
            logger.info("**************此次提额总额度:" + sumcredit);

            String balance = "";
            String maxMoney = "";
            Double sumbalance = 0.00;
            Double summaxMoney = 0.00;
            Double sumGrade = 0.00;
            Double subGrade = 0.00;

            logger.info("********执行获取原始额度********");
            //获取原始额度
            TbAccountInfo tbi = iTbAccountInfoService.getTbAccountInfoByUid(map);
            if (tbi != null && tbi.getBalance() != null && tbi.getMaxMoney() != null){
                logger.info("***********tbi.getBalance():" + tbi.getBalance());
                logger.info("***********tbi.getMaxMoney():" + tbi.getMaxMoney());
                balance = DES.decryptdf(tbi.getBalance());
                maxMoney = DES.decryptdf(tbi.getMaxMoney());
            }
            if (sumcredit != null){
                sumGrade = Double.parseDouble(tbi.getGrade()) + sumcredit;
            }

            logger.info("********执行此次提额总额度********");
            //此次提额后总额度
            BigDecimal totalScore = baseCreditConfigService.getTotalScore(sumGrade.toString(), fFundendBind.getFid());
            if (!ValidateUtil.isEmpty(totalScore)){
                summaxMoney = Double.parseDouble(totalScore.toString());
            }

            //此次提升的额度
            subGrade = summaxMoney - Double.parseDouble(maxMoney);


            CreditVo creditVo = new CreditVo();
            creditVo.setOid(increaseCreditVo.getOid());

            //获取资金端的授信余额详情
            R<FMoneyInfo> r2 = fMoneyInfoController.getFMoneyInfoByFid(creditVo);

            if (!ValidateUtil.isEmpty(r2.getData()) && "1".equals(r2.getData().getStatus())) {         //资金端采用限额授信
                R<BigDecimal> r3 = fMoneyOtherInfoController.getFMoneyOtherInfoByFid(creditVo);

                //当日授信额度减去已授信额度
                BigDecimal b1 = r2.getData().getDayMoney().subtract(r3.getData());

                int i1 = b1.compareTo(new BigDecimal("0"));
                int i2 = b1.compareTo(new BigDecimal(subGrade));


                //判断当日授信剩余额度是否满足授信条件
                if ((i1 == 1 || i1 == 0) && i2 == -1) {          //当日剩余授信额度无法满足授信
                    r.setCode(1);
                    r.setMsg("今日授信总额已达上限，请明日再进行提额");
                    return r;
                }
            }



            //此次提额后的剩余额度
            sumbalance = Double.parseDouble(balance) + subGrade;

            Double sum = summaxMoney;

            BigDecimal bl = new Calculate(sum.toString()).format(2, RoundingMode.DOWN);
            String slance = bl.toString();

            HashMap<String, Object> hmap = new HashMap<>();
            hmap.put("balance", DES.encryptdf(sumbalance.toString()));
            hmap.put("maxMoney", DES.encryptdf(summaxMoney.toString()));
            hmap.put("grade",sumGrade);
            hmap.put("uid", increaseCreditVo.getUid());

            logger.info("********执行修改原始额度********");
            //修改原始额度
            iTbAccountInfoService.updateTbAccountInfoByUid(hmap);

            logger.info("********执行修改提额输入项状态********");
            //修改提额输入项状态
            iTbUpMoneyService.updateOneStatusIncreaseCreditByid(map);

//            //快速授信必须要有前提条件，运营商必须要绑定资金商，否则不能做快速授信
//            FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(increaseCreditVo.getOid());


            FMoneyInfo fMoneyInfo = fMoneyInfoService.selectOne(new EntityWrapper<FMoneyInfo>().eq("fid", fFundendBind.getFid()));


            if (!ValidateUtil.isEmpty(fMoneyInfo) && "1".equals(fMoneyInfo.getStatus())){
                FMoneyOtherInfo fMoneyOtherInfo = new FMoneyOtherInfo();
                fMoneyOtherInfo.setCreateDate(new Date());
                fMoneyOtherInfo.setFid(fFundendBind.getFid());
                fMoneyOtherInfo.setFmid(fMoneyInfo.getId());
                fMoneyOtherInfo.setMoney(new BigDecimal(subGrade));
                fMoneyOtherInfo.setType("0");
                fMoneyOtherInfo.setUid(increaseCreditVo.getUid());

                //记录授信总金额出入明细表
                fMoneyOtherInfoService.insert(fMoneyOtherInfo);
            }

            CUserRefCon cUserRefCon = new CUserRefCon();
            cUserRefCon.setUpmoneyDate(new Date());

            //修改授信提额时间
            iCUserRefConService.update(cUserRefCon, new EntityWrapper<CUserRefCon>().eq("uid", increaseCreditVo.getUid()).eq("oid", increaseCreditVo.getOid()));

            BalanceVo balanceVo = new BalanceVo();

            balanceVo.setSubGrade(subGrade.toString());
            balanceVo.setSumbalance(slance);

            r.setData(balanceVo);
            r.setCode(0);
            r.setMsg("success");
        }catch (Exception e){
            logger.info(e.getMessage());
            r.setCode(1);
            r.setMsg("执行提额失败");
        }

        return r;
    }



}
