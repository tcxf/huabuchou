package com.tcxf.hbc.ctu.controller.cuser;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.SysArea;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.ctu.service.ISysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 行政区划 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-28
 */
@RestController
@RequestMapping("/sysArea")
public class SysAreaController extends BaseController {
    @Autowired private ISysAreaService sysAreaService;

    /**
    * 通过ID查询
    *
    * @param id ID
    * @return SysArea
    */
    @GetMapping("/{id}")
    public SysArea get(@PathVariable Integer id) {
        return sysAreaService.selectById(id);
    }


    /**
    * 分页查询信息
    *
    * @param params 分页对象
    * @return 分页对象
    */
    @RequestMapping("/page")
    public Page page(@RequestParam Map<String, Object> params) {
        params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return sysAreaService.selectPage(new Query<>(params), new EntityWrapper<>());
    }

    /**
     * 添加
     * @param  sysArea  实体
     * @return success/false
     */
    @PostMapping
    public Boolean add(@RequestBody SysArea sysArea) {
        return sysAreaService.insert(sysArea);
    }

    /**
     * 删除
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable String id) {
        SysArea sysArea = new SysArea();
//        sysArea.setId(id);
//        return sysAreaService.updateById(sysArea);
        return false;
    }

    /**
     * 编辑
     * @param  sysArea  实体
     * @return success/false
     */
    @PutMapping
    public Boolean edit(@RequestBody SysArea sysArea) {
//        sysArea.setUpdateTime(new Date());
//        return sysAreaService.updateById(sysArea);
        return false;
    }
}
