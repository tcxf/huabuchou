package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.BaseCreditConfig;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
public interface BaseCreditConfigMapper extends BaseMapper<BaseCreditConfig> {

    public List<BaseCreditConfig> getListByType(String type);
}
