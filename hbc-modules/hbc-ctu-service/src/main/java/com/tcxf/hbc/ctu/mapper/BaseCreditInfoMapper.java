package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.BaseCreditInfo;

/**
 * <p>
 * 授信四要素表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-16
 */
public interface BaseCreditInfoMapper extends BaseMapper<BaseCreditInfo> {

}
