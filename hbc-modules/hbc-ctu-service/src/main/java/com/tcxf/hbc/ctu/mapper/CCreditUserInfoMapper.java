package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.CCreditUserInfo;

import java.util.HashMap;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
public interface CCreditUserInfoMapper extends BaseMapper<CCreditUserInfo> {


    /**
     * 查询历史记录并验证
     * @param map
     * @return
     */
    public CCreditUserInfo checkUserCreditStatus(HashMap<String, Object> map);
}
