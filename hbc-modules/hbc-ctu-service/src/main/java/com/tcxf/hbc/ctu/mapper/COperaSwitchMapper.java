package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.COperaSwitch;

/**
 * <p>
 * 运营商开关表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
public interface COperaSwitchMapper extends BaseMapper<COperaSwitch> {

}
