package com.tcxf.hbc.ctu.mapper;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface CUserInfoMapper extends BaseMapper<CUserInfo> {

    public List<CUserInfo> queryUserInfoForList(Query<CUserInfo> query, Map<String, Object> condition);

}
