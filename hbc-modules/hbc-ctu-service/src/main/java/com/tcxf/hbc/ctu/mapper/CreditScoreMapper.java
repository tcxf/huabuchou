package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.CreditScore;


/**
 * 授信提额综合分值
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface CreditScoreMapper extends BaseMapper<CreditScore> {

}
