package com.tcxf.hbc.ctu.mapper;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 资金端、运营商绑定关系表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface FFundendBindMapper extends BaseMapper<FFundendBind> {

    public FFundendBind getFFundendBindByOid(String oid);

}
