package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * <p>
 * 授信总金额出入明细表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
public interface FMoneyOtherInfoMapper extends BaseMapper<FMoneyOtherInfo> {

    public BigDecimal getFMoneyOtherInfoSumByFid(HashMap<String, Object> map);

}
