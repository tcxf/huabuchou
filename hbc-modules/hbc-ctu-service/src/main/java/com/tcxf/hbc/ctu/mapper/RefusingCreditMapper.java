package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RefusingCredit;

import java.util.List;

/**
 * 拒绝授信
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface RefusingCreditMapper extends BaseMapper<RefusingCredit> {

    List<RefusingCredit> selectRefusingCreditByvId(String vid);
}
