package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RefusingVersion;

/**
 * 授信版本
 * @Auther: liuxu
 * @Date: 2018/9/19 16:20
 * @Description:
 */
public interface RefusingVersionMapper extends BaseMapper<RefusingVersion> {
}
