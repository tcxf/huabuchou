package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;

import java.util.List;

/**
 * <p>
 * 初始授信条件模板表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
public interface RmBaseCreditInitialMapper extends BaseMapper<RmBaseCreditInitial> {

    //根据类型，版本号获得初始模板
    public List<RmBaseCreditInitial> getListByType(RmBaseCreditInitial rmBaseCreditInitial);

}
