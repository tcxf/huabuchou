package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;

/**
 * <p>
 * 提额授信条件模板表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface RmBaseCreditUpmoneyMapper extends BaseMapper<RmBaseCreditUpmoney> {

}
