package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;

/**
 * <p>
 * 授信模板关系表 Mapper 接口
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
public interface RmSxtemplateRelationshipMapper extends BaseMapper<RmSxtemplateRelationship> {

}
