package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.SysArea;

/**
 * <p>
 * 行政区划 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-28
 */
public interface SysAreaMapper extends BaseMapper<SysArea> {

    public SysArea getAreaByCode(String areaCode);
}
