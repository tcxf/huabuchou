package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbArea;

/**
 * <p>
 * 地区表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
public interface TbAreaMapper extends BaseMapper<TbArea> {

}
