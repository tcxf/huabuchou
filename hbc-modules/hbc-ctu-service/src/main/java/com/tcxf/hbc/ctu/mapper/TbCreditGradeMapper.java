package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbCreditGrade;

/**
 * <p>
 * 快速授信评分项 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
public interface TbCreditGradeMapper extends BaseMapper<TbCreditGrade> {

}
