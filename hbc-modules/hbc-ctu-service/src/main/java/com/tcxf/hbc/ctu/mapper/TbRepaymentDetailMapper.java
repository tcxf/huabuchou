package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;

/**
 * <p>
 * 还款表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface TbRepaymentDetailMapper extends BaseMapper<TbRepaymentDetail> {


}
