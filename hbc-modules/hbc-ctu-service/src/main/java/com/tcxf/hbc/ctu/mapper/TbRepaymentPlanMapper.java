package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 还款计划表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface TbRepaymentPlanMapper extends BaseMapper<TbRepaymentPlan> {

    public List<TbRepaymentPlan> getTbRepaymentPlanlistbyUid(HashMap<String, Object> hashMap);

}
