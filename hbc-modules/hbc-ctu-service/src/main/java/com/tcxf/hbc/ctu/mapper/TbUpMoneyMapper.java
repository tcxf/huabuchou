package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbUpMoney;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 授信提额表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
public interface TbUpMoneyMapper extends BaseMapper<TbUpMoney> {

    public List<TbUpMoney> getOneIncreaseCreditByUid(HashMap<String, Object> map);


    public List<TbUpMoney> getTwoIncreaseCreditByUid(HashMap<String, Object> map);


    public void updateTwoIncreaseCreditByid(HashMap<String, Object> map);

    public void updateTwoIncreaseCreditBytoken(HashMap<String, Object> map);

    public List<TbUpMoney> getOneTokenIncreaseCreditByUid(HashMap<String, Object> map);

    public void delTbUpMoneyByUid(HashMap<String, Object> map);

    //获取认证项数目
    public Integer getTokenIncreaseCreditByUid(HashMap<String, Object> map);

    //获取认证可提额项数目
    public Integer getTokenIncreaseCreditPermitByUid(HashMap<String, Object> map);

    public Integer getNotTokenIncreaseCreditByUid(HashMap<String, Object> map);

    public void updateOneStatusIncreaseCreditByid(HashMap<String, Object> map);



}
