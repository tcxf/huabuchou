package com.tcxf.hbc.ctu.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWallet;

/**
 * <p>
 * 资金钱包表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
public interface TbWalletMapper extends BaseMapper<TbWallet> {

}
