package com.tcxf.hbc.ctu.model.dto;

/**
 * @program: hbc
 * @description: 回调参数
 * @author: JinPeng
 * @create: 2018-07-20 17:17
 **/
public class CallBackParam {

    public int retCode;
    public String retMsg;

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }
}
