package com.tcxf.hbc.ctu.model.dto;

/**
 * @program: Workspace2
 * @description: 授信详情Dto
 * @author: JinPeng
 * @create: 2018-08-01 13:46
 **/
public class MycreditDetailsDto {

    //授信可用余额
    private String balance;

    //授信额度
    private String rapidMoney;

    //出账日期
    private String ooaDate;

    //还款时间
    private String repayDate;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRapidMoney() {
        return rapidMoney;
    }

    public void setRapidMoney(String rapidMoney) {
        this.rapidMoney = rapidMoney;
    }

    public String getOoaDate() {
        return ooaDate;
    }

    public void setOoaDate(String ooaDate) {
        this.ooaDate = ooaDate;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }
}
