package com.tcxf.hbc.ctu.model.dto;

/**
 * @program: Workspace2
 * @description: 天行入参DTO
 * @author: JinPeng
 * @create: 2018-08-06 16:18
 **/
public class TianXingParmDto {

    //天行account
    private String account;

    //银行卡
    private String accountNO;

    //用户名
    private String userName;

    //身份证号
    private String idCard;

    //手机号码
    private String mobile;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountNO() {
        return accountNO;
    }

    public void setAccountNO(String accountNO) {
        this.accountNO = accountNO;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
