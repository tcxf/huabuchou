package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CreditScore;

/**
 * 授信提额综合分值
 * @Auther: liuxu
 * @Date: 2018/9/19 16:14
 * @Description:
 */
public interface CreditScoreService extends IService<CreditScore> {

}
