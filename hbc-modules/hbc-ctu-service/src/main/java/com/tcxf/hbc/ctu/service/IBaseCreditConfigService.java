package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.BaseCreditConfig;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.tcxf.hbc.common.entity.RmBaseCreditType;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.ctu.common.util.data.operatorReport.CreditPlatformRegistrationDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
public interface IBaseCreditConfigService extends IService<BaseCreditConfig> {

    public void addJASONStr(CCreditUserInfo cCreditUserInfo);
    public List<BaseCreditConfig> getListByType(String type);
    public BigDecimal tagMobileConvert(Map<String,RmBaseCreditType> map, String type, Double contactCnt) throws Exception;
    public BigDecimal debtCallConvert(Map<String,RmBaseCreditType> map, String type,Double debtCallCnt) throws Exception;
    public String dataConvertScore(String jasonStr, String type) throws Exception;
    public BigDecimal getTotalScore(String totalScore, String fid) throws Exception;
    public String onSx(CreditVo creditVo);
    public void updateCUserRefConByUId(String twoapplications, String refusingCode, CreditVo creditVo,String examineFailReason);
    public Map<String,RmBaseCreditType> getRmBaseCreditInitialList(CreditVo creditVo);
}
