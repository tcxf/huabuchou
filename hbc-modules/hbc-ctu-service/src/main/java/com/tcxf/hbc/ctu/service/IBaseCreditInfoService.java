package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.BaseCreditInfo;

/**
 * <p>
 * 授信四要素表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-16
 */
public interface IBaseCreditInfoService extends IService<BaseCreditInfo> {

}
