package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.BaseInterfaceInfo;

import java.util.HashMap;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
public interface IBaseInterfaceInfoService extends IService<BaseInterfaceInfo> {

    public HashMap<String, Object> getBaseInterfaceInfoList();
}
