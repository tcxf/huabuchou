package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CCreditUserInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
public interface ICCreditUserInfoService extends IService<CCreditUserInfo> {

    public List<CCreditUserInfo> getListByType(String type);
    public Boolean getJsonByUid(String uId, String type);
    public void saveJsonByUid(String json, String uId, String type);
}
