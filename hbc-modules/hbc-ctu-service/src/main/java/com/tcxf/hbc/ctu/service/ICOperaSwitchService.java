package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.COperaSwitch;

/**
 * <p>
 * 运营商开关表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
public interface ICOperaSwitchService extends IService<COperaSwitch> {

}
