package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;

import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserInfoService extends IService<CUserInfo> {

    public CUserInfo getUserInfo(String userId);

    public void delUserInfo(String userId);

    public void modifyUserInfo(CUserInfo cUserInfo);

    public void addUserInfo(CUserInfo cUserInfo);

    public Page selectUserInfoPage(Map<String, Object> params);



    /**
     * 分页查询用户信息
     * @param status
     * @return
     */
    public Page queryList(String status);

    /**
     * 分页查询用户信息
     * @param query
     * @return
     */
    public Page queryList( Query<CUserInfo> query);
}
