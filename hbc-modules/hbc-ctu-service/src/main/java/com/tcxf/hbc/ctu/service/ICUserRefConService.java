package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.ctu.model.dto.MycreditDetailsDto;

import java.util.HashMap;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface ICUserRefConService extends IService<CUserRefCon> {

    public void updateCUserRefConByUid(HashMap<String, Object> map);

    public void updateCUserRefConCountByUid(HashMap<String, Object> map);

    public CUserRefCon getCUserRefConByUid(HashMap<String, Object> map);

    public MycreditDetailsDto getMycreditDetails(HashMap<String, Object> map);

}
