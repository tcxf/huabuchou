package com.tcxf.hbc.ctu.service;

import com.tcxf.hbc.common.entity.BaseCreditConfig;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface IFFundendBindService extends IService<FFundendBind> {

    public FFundendBind getFFundendBindByOid(String oid);

}
