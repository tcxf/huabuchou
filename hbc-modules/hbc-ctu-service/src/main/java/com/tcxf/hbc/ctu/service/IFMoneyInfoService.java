package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.FMoneyInfo;

/**
 * <p>
 * 资金端授信上限表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
public interface IFMoneyInfoService extends IService<FMoneyInfo> {

}
