package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * <p>
 * 授信总金额出入明细表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
public interface IFMoneyOtherInfoService extends IService<FMoneyOtherInfo> {

    public BigDecimal getFMoneyOtherInfoSumByFid(HashMap<String, Object> map);

}
