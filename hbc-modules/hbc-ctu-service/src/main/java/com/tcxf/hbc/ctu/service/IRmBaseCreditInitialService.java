package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;

import java.util.List;

/**
 * <p>
 * 初始授信条件模板表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
public interface IRmBaseCreditInitialService extends IService<RmBaseCreditInitial> {


    public List<RmBaseCreditInitial> getRmBaseCreditInitialList(RmBaseCreditInitial rmBaseCreditInitial);

    public void createRmBaseCreditInitial(List<RmBaseCreditInitial> list, String fid);

}
