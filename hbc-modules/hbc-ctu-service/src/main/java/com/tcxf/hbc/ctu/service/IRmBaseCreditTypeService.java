package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RmBaseCreditType;

/**
 * <p>
 * 授信条件类别表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface IRmBaseCreditTypeService extends IService<RmBaseCreditType> {

}
