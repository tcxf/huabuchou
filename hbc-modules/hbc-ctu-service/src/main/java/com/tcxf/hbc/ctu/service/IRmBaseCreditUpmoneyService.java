package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;

import java.util.List;

/**
 * <p>
 * 提额授信条件模板表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
public interface IRmBaseCreditUpmoneyService extends IService<RmBaseCreditUpmoney> {

    public void createRmBaseCreditUpmoney(List<RmBaseCreditUpmoney> list, String fid);

}
