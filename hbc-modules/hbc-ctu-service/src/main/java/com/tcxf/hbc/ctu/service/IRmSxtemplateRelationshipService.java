package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;

/**
 * <p>
 * 授信模板关系表 服务类
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
public interface IRmSxtemplateRelationshipService extends IService<RmSxtemplateRelationship> {

    public RmSxtemplateRelationship selectNewVersionByfid(String fid);

}
