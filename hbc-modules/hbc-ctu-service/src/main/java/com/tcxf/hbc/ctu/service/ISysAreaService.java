package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.SysArea;

/**
 * <p>
 * 行政区划 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-28
 */
public interface ISysAreaService extends IService<SysArea> {

    public SysArea getAreaByCode(String areaCode);
}
