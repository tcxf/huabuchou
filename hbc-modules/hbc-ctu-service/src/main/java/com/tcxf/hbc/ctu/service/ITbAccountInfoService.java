package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbAccountInfo;

import java.util.HashMap;

/**
 * <p>
 * 用户授信资金额度表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface ITbAccountInfoService extends IService<TbAccountInfo> {

    public String insertTbAccountInfo(TbAccountInfo tbAccountInfo);

    public void updateTbAccountInfoByUid(HashMap<String, Object> hashMap);

    public TbAccountInfo getTbAccountInfoByUid(HashMap<String, Object> hashMap);

}
