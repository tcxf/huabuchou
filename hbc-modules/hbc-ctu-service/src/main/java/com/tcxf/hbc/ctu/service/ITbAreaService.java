package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbArea;

/**
 * <p>
 * 地区表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
public interface ITbAreaService extends IService<TbArea> {

}
