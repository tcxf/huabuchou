package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbCreditGrade;

/**
 * <p>
 * 快速授信评分项 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
public interface ITbCreditGradeService extends IService<TbCreditGrade> {

}
