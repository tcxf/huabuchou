package com.tcxf.hbc.ctu.service;

import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 还款计划表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
public interface ITbRepaymentPlanService extends IService<TbRepaymentPlan> {

}
