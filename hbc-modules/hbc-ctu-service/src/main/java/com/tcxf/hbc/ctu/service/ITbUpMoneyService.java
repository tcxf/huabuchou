package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.RmBaseCreditType;
import com.tcxf.hbc.common.entity.TbUpMoney;
import com.tcxf.hbc.common.vo.CreditVo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授信提额表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
public interface ITbUpMoneyService extends IService<TbUpMoney> {

    public List<TbUpMoney> getOneIncreaseCreditByUid(HashMap<String, Object> map);

    public List<TbUpMoney> getOneTokenIncreaseCreditByUid(HashMap<String, Object> map);

    public List<TbUpMoney> getTwoIncreaseCreditByUid(HashMap<String, Object> map);

    public void updateTwoIncreaseCreditByid(HashMap<String, Object> map);

    public void updateOneStatusIncreaseCreditByid(HashMap<String, Object> map);

    public void insertOneIncreaseCredit (TbUpMoney tbUpMoney);

    public Map<String,RmBaseCreditType> getRmBaseCreditUpmoneyList(String Fid);

    public void getSocialInsuranceScore(Map<String,RmBaseCreditType> map, int count, String token);

    public void getEducationInfoScore(Map<String,RmBaseCreditType> map, String degree, String token);

    public void getTaobaoApprove(Map<String,RmBaseCreditType> map, int addresscount, int mobilecount, String token);

    public void getDidiDacheApprove(Map<String,RmBaseCreditType> map, String sesameScore, String token);

    public BigDecimal getPaymentsCredit(Map<String, Object> score, Map<String, Object> credit, String uid, String oid, Map<String,RmBaseCreditType> map);

    public Integer getTokenIncreaseCreditByUid(HashMap<String, Object> map);

    public Integer getTokenIncreaseCreditPermitByUid(HashMap<String, Object> map);

    public Integer getNotTokenIncreaseCreditByUid(HashMap<String, Object> map);
}
