package com.tcxf.hbc.ctu.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbWallet;

/**
 * <p>
 * 资金钱包表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
public interface ITbWalletService extends IService<TbWallet> {

}
