package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.gson.*;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.ctu.common.constant.CreditConstant;
import com.tcxf.hbc.ctu.common.util.AddressUtils;
import com.tcxf.hbc.ctu.common.util.OperatorUtil;
import com.tcxf.hbc.ctu.common.util.TXInterfaceUtil;
import com.tcxf.hbc.ctu.common.util.data.operatorReport.CreditPlatformRegistrationDetails;
import com.tcxf.hbc.ctu.common.util.data.operatorReport.OverduePlatformDetails;
import com.tcxf.hbc.ctu.mapper.BaseCreditConfigMapper;
import com.tcxf.hbc.ctu.mapper.CCreditUserInfoMapper;
import com.tcxf.hbc.ctu.mapper.SysAreaMapper;
import com.tcxf.hbc.ctu.mapper.TbCreditGradeMapper;
import com.tcxf.hbc.ctu.model.dto.TianXingParmDto;
import com.tcxf.hbc.ctu.service.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author zhouyinjun
 * @since 2018-06-26
 * 授信服务service
 */
@Service
public class BaseCreditConfigServiceImpl extends ServiceImpl<BaseCreditConfigMapper, BaseCreditConfig> implements IBaseCreditConfigService {
    Logger logger = LoggerFactory.getLogger(BaseCreditConfigServiceImpl.class);
    @Autowired
    SysAreaMapper sysAreaMapper;
    @Autowired
    private BaseCreditConfigMapper baseCreditConfigMapper;
    @Autowired
    private CCreditUserInfoMapper cCreditUserInfoMapper;
    @Autowired
    private TbCreditGradeMapper tbCreditGradeMapper;
    @Autowired
    private ITbCreditGradeService iTbCreditGradeService;
    @Autowired
    private IBaseInterfaceInfoService iBaseInterfaceInfoService;
    @Autowired
    protected ICOperaSwitchService iCOperaSwitchService;

    @Autowired
    protected IOOperaInfoService iOOperaInfoService;

    @Autowired
    protected ITbAreaService iTbAreaService;

    @Autowired
    protected ISysAreaService iSysAreaService;

    @Autowired
    private IBaseCreditInfoService iBaseCreditInfoService;

    @Autowired
    private ICUserInfoService iCUserInfoService;

    @Autowired
    private ITbWalletService iTbWalletService;

    @Autowired
    private ICUserRefConService iCUserRefConService;
    @Autowired
    private IBaseCreditConfigService baseCreditConfigService;
    @Autowired
    protected RefusingCreditService refusingCreditService;

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IRmBaseCreditTypeService iRmBaseCreditTypeService;

    @Autowired
    private IRmBaseCreditInitialService iRmBaseCreditInitialService;

    @Autowired
    private CreditScoreService creditScoreService;

    @Autowired
    private IRmSxtemplateRelationshipService rmSxtemplateRelationshipService;




    /**
     * @Description: 根据身份证号码获取对应的地区城市信息
     * @Param: [sysArea]
     * @return: java.lang.String
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private String getIdCardCity(SysArea sysArea) {
        if(ValidateUtil.isEmpty(sysArea)){
            throw new CheckedException("身份证信息异常,请核实");
        }
        if(sysArea.getTreeLevel().toString().equals(SysArea.TreeLevel_1)) {             //市级籍贯
            return sysArea.getAreaName();
        }
        if(sysArea.getTreeLevel().toString().equals(SysArea.TreeLevel_2)) {             //区县级籍贯
            //根据用户的身份证信息获取详细地址
            SysArea city = iSysAreaService.selectOne(new EntityWrapper<SysArea>().eq("area_code", sysArea.getParentCode()));
            return city.getAreaName();
        }
        throw new CheckedException("身份证信息异常,请核实");
    }

    /**
     * @Description: 根据IP地址获取对应的地区城市
     * @Param: []
     * @return: java.lang.String
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private String getIpCity(CreditVo creditVo){

        String json_result = AddressUtils.getAddresses(creditVo.getIp());

        if(ValidateUtil.isEmpty(json_result)){
            throw new CheckedException("登录IP地址异常，请稍后再试");
        }
        Map<String,String> map =JsonTools.parseJSON2Map(JsonTools.parseJSON2Map(json_result).get("data").toString());

        if(ValidateUtil.isEmpty(map.get("city"))){
            throw new CheckedException("登录IP地址异常，请稍后再试");
        }
        return map.get("city");
    }

    /**
     * @Description: 匹配身份证地区
     * @Param: [creditVo, cOperaSwitch, tbArea]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void validateIdcardCity(CreditVo creditVo, COperaSwitch cOperaSwitch, TbArea tbArea){
        if(!COperaSwitch.idcard_state_YES.equals(cOperaSwitch.getIdcardState())){
            return ;
        }
        //根据用户的身份证信息获取详细地址(包含县级 或者 市级)
        SysArea sysArea = iSysAreaService.selectOne(new EntityWrapper<SysArea>().eq("area_code", creditVo.getIdCard().substring(0, 6)));
        //获取身份证对应的城市地区信息
        String city = getIdCardCity(sysArea);
        if(tbArea.getDisplayName().indexOf(city) == -1){
            throw new CheckedException("您的籍贯不在该运营商范围内，不允许授信！");
        }
    }

    /**
     * @Description: 匹配手机归属地
     * @Param: [creditVo, cOperaSwitch, tbArea]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void validateProvinceCity(CreditVo creditVo, COperaSwitch cOperaSwitch, TbArea tbArea){
        if(!COperaSwitch.phoneState_YES.equals(cOperaSwitch.getPhoneState())){
            return ;
        }
        //查询手机归属地
        String province = OperatorUtil.getResult(creditVo.getMobile());

        if (!ValidateUtil.isEmpty(province) &&
                tbArea.getDisplayName().indexOf(province) == -1){
            throw new CheckedException("您的手机号归属地不在该运营商范围内，不允许授信！");
        }
    }

    /**
     * @Description: ip地址对应地区匹配
     * @Param: [creditVo, cOperaSwitch, tbArea]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void validateipCity(CreditVo creditVo, COperaSwitch cOperaSwitch, TbArea tbArea){
        if(!COperaSwitch.ipState_YES.equals(cOperaSwitch.getIpState())){
            return ;
        }
        //查询ip归属地状态
        String  ipCity = getIpCity(creditVo);
        if (!ValidateUtil.isEmpty(ipCity) && tbArea.getName().indexOf(ipCity) == -1){
            throw new CheckedException("您的移动设备不在该运营商范围内，不允许授信！");
        }
    }
    /**
     * @Description: 授信配置匹配
     * @Param: [creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void validateCOperaSwitch(CreditVo creditVo){
        //获取运营商信息
        OOperaInfo oOperaInfo = iOOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("id", creditVo.getOid()));
        //获取运营商详细地址信息
        TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id", oOperaInfo.getArea()));
        //查询授信限制配置信息
        COperaSwitch cOperaSwitch = iCOperaSwitchService.selectOne(new EntityWrapper<COperaSwitch>().eq("oid", creditVo.getOid()));
        //验证身份证地区
        validateIdcardCity(creditVo,cOperaSwitch,tbArea);
        //验证手机号码归属地地区
        validateProvinceCity(creditVo,cOperaSwitch,tbArea);
        //验证登录IP地区
        validateipCity(creditVo,cOperaSwitch,tbArea);
        //查询这个人的额度详情
        CUserRefCon cUserRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid",creditVo.getUid()).eq("oid",creditVo.getOid()));
        if (CUserRefCon.AUTHSTATUS_Y.equals(cUserRefCon.getAuthStatus())) {
            throw new CheckedException("您已经授信成功，请勿重复授信");
        }

    }

    /**
     * @Description: 银行卡三要素
     * @Param: [ibfismap, txpd, creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void postThreelement(HashMap<String, Object> ibfismap,TianXingParmDto txpd,CreditVo creditVo,CUserRefCon cUserRefCon) {

        //组装入参参数
        HashMap<String, Object> threeMap = new HashMap<String, Object>();
        threeMap.put("account", ibfismap.get("account").toString());
        threeMap.put("signature", ibfismap.get("signature").toString());
        threeMap.put("url", this.jointParm(CreditConstant.THREELEMENT,txpd,ibfismap));
        threeMap.put("tokenurl", ibfismap.get("tianXingTokenUrl").toString());
        //调用接口
        String threelementresult = TXInterfaceUtil.getResultByOkHttp(threeMap, 1);

        toAddJASONStr(threelementresult,CreditConstant.THREELEMENTTYPE,"银行卡三要素接口", creditVo);

        logger.info("银行卡三要素返回结果:" + threelementresult);
        if(ValidateUtil.isEmpty(threelementresult)){
            throw new CheckedException("快速授信失败");
        }
        JsonObject threelementresuljson = new JsonParser().parse(threelementresult).getAsJsonObject();
        if("false".equals(threelementresuljson.get("success").getAsString())) {
            //记录授信次数
            updateCUserRefConByUId("","",creditVo,"银行卡三要素认证不通过");
            throw new CheckedException("手机号及银行卡必须在本人自己名下");
        }
        if(!"SAME".equals(threelementresuljson.get("data").getAsJsonObject().get("checkStatus").getAsString())){
            //记录授信次数
            updateCUserRefConByUId("","",creditVo,"银行卡三要素认证不通过");
            throw new CheckedException("手机号及银行卡必须在本人自己名下");
        }

    }

    /**
     * @Description: 运营商三要素
     * @Param: [ibfismap, txpd, creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    private void postOperators(HashMap<String, Object> ibfismap,TianXingParmDto txpd,CreditVo creditVo,CUserRefCon cUserRefCon){

        //组装入参参数
        HashMap<String, Object> threeMap = new HashMap<String, Object>();
        threeMap.put("account", ibfismap.get("account").toString());
        threeMap.put("signature", ibfismap.get("signature").toString());
        threeMap.put("tokenurl", ibfismap.get("tianXingTokenUrl").toString());
        threeMap.put("url", this.jointParm(CreditConstant.OPERATORS,txpd,ibfismap));


        //运营商三要素返回结果
        String operatorsresult = TXInterfaceUtil.getResultByOkHttp(threeMap, 1);

        toAddJASONStr(operatorsresult,CreditConstant.OPERATORSTYPE,"运营商三要素接口", creditVo);

        logger.info("运营商三要素返回结果:" + operatorsresult);

        if(ValidateUtil.isEmpty(operatorsresult)){
            throw new CheckedException("快速授信失败");
        }
        JsonObject operatorsresuljson = new JsonParser().parse(operatorsresult).getAsJsonObject();
        if("false".equals(operatorsresuljson.get("success").getAsString())){
            //记录授信次数
            updateCUserRefConByUId("","",creditVo,"运营商三要素认证不通过");
            throw new CheckedException("手机号及银行卡必须在本人自己名下");
        }
        if(!"SAME".equals(operatorsresuljson.get("data").getAsJsonObject().get("compareStatus").getAsString())){
            //记录授信次数
            updateCUserRefConByUId("","",creditVo,"运营商三要素认证不通过");
            throw new CheckedException("手机号及银行卡必须在本人自己名下");
        }
    }

    /**
     * @Description: 多重负载接口
     * @Param: [ibfismap, txpd, creditVo, cUserRefCon]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/14
     */
    private ArrayList<CreditPlatformRegistrationDetails> postMultiplenew(HashMap<String, Object> ibfismap,TianXingParmDto txpd,CreditVo creditVo){

        //组装入参参数
        HashMap<String, Object> threeMap = new HashMap<String, Object>();
        threeMap.put("account", ibfismap.get("account").toString());
        threeMap.put("signature", ibfismap.get("signature").toString());
        threeMap.put("tokenurl", ibfismap.get("tianXingTokenUrl").toString());
        threeMap.put("url", this.jointParm(CreditConstant.MULTIPLENEW,txpd,ibfismap));
        //多重负债返回结果
        String multipleNewresult = TXInterfaceUtil.getResultByOkHttp(threeMap, 1);

        toAddJASONStr(multipleNewresult,CreditConstant.MULTIPLENEWTYPE,"多头负债接口", creditVo);

        logger.info("多重负债返回结果:" + multipleNewresult);
        if (ValidateUtil.isEmpty(multipleNewresult)) {
            throw new CheckedException("快速授信失败");
        }
        JsonObject jsonObject = new JsonParser().parse(multipleNewresult).getAsJsonObject();

        //查询最新拒绝条件模板
        List<RefusingCredit> list = refusingCreditService.selectRefusingCreditByfId(creditVo.getFid());

        Map<String,RefusingCredit> map = new HashMap<>();
        for(RefusingCredit r : list){
            map.put(r.getCode(),r);
        }
        if ("false".equals(jsonObject.get("success").getAsString())) {
            throw new CheckedException("快速授信失败");
        }
        //逾期平台结果
        JsonArray multipleNewdata = jsonObject.get("data").getAsJsonObject().get("overduePlatformDetails").getAsJsonArray();
        //多头负债平台注册结果
        JsonArray creditPlatformRegistrationDetails = jsonObject.get("data").getAsJsonObject().get("creditPlatformRegistrationDetails").getAsJsonArray();
        Gson googleJson = new Gson();
        List<OverduePlatformDetails> jsonObjList = new ArrayList<>();
        for (JsonElement jsonElement : multipleNewdata) {
            jsonObjList.add(googleJson.fromJson(jsonElement,OverduePlatformDetails.class));
        }

        ArrayList<CreditPlatformRegistrationDetails> creditPlatformRegistrationjsonObjList = new ArrayList<>();
        for (JsonElement jsonElement : creditPlatformRegistrationDetails) {
            creditPlatformRegistrationjsonObjList.add(googleJson.fromJson(jsonElement,CreditPlatformRegistrationDetails.class));
        }

        //判断逾期平台个数 超过两个逾期平台则拒绝授信
        if (map.get("overduecheck").getStatus().equals(RefusingCredit.status_1) &&
                jsonObjList.size() >= map.get("overduecheck").getNumber()) {

            String twoapplications = map.get("overduecheck").getTwoapplications();
            updateCUserRefConByUId(twoapplications,"overduecheck",creditVo,"逾期平台个数超过"+map.get("overduecheck").getNumber()+"个，拒绝授信");
            throw new CheckedException(ValidateUtil.isEmpty(twoapplications)?"授信失败":
                    twoapplications.equals(RefusingCredit.TWOAPPLICATIONS_Y)?"授信失败，请三个月之后再进行授信":
                            "您已被永久拒绝授信");
        }
        //判断单平台逾期次数 超过2两次则拒绝授信
        for (OverduePlatformDetails oo : jsonObjList) {
            Integer counts = !ValidateUtil.isEmpty(oo.getCounts()) ? Integer.parseInt(oo.getCounts()) : 0;
            if (map.get("overduenumcheck").getStatus().equals(RefusingCredit.status_1)&&
                    counts >= map.get("overduenumcheck").getNumber()) {
                String twoapplications = map.get("overduenumcheck").getTwoapplications();
                updateCUserRefConByUId(twoapplications,"overduenumcheck",creditVo,"单平台逾期次数超过"+map.get("overduenumcheck").getNumber()+"个，拒绝授信");
                throw new CheckedException(ValidateUtil.isEmpty(twoapplications)?"授信失败":
                        twoapplications.equals(RefusingCredit.TWOAPPLICATIONS_Y)?"授信失败，请三个月之后再进行授信":
                                "您已被永久拒绝授信");
            }
        }
        //判断多头负债是否超过16 超过则拒绝授信
        if (map.get("liabilitienumcheck").getStatus().equals(RefusingCredit.status_1)&&
                creditPlatformRegistrationjsonObjList.size() > map.get("liabilitienumcheck").getNumber()) {

            String twoapplications = map.get("liabilitienumcheck").getTwoapplications();
            updateCUserRefConByUId(twoapplications,"liabilitienumcheck",creditVo,"多头负债超过"+map.get("liabilitienumcheck").getNumber()+"个，拒绝授信");
            throw new CheckedException(ValidateUtil.isEmpty(twoapplications)?"授信失败":
                    twoapplications.equals(RefusingCredit.TWOAPPLICATIONS_Y)?"授信失败，请三个月之后再进行授信":
                            "您已被永久拒绝授信");
        }
        return creditPlatformRegistrationjsonObjList;

    }

    /**
     * @Description: 进行授信操作
     * @Param: [creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    @Override
    public String onSx(CreditVo creditVo) {
        //授信配置匹配验证
        validateCOperaSwitch(creditVo);
        //查询接口信息
        HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();
        //用户关系信息
        CUserRefCon cUserRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid",creditVo.getUid()).eq("oid",creditVo.getOid()));
        //天行接口入参
        TianXingParmDto txpd = new TianXingParmDto();
        txpd.setAccount(ibfismap.get("account").toString());
        txpd.setAccountNO(creditVo.getAccountNO());
        txpd.setIdCard(creditVo.getIdCard());
        txpd.setMobile(creditVo.getMobile());
        txpd.setUserName(creditVo.getUserName());

        //银行卡三要素接口调用
        postThreelement(ibfismap,txpd,creditVo,cUserRefCon);
        //运营商三要素接口调用
        postOperators(ibfismap,txpd,creditVo,cUserRefCon);
        //多重负债接口调用
         ArrayList<CreditPlatformRegistrationDetails> creditPlatformRegistrationjsonObjList = postMultiplenew(ibfismap,txpd,creditVo);
        //计算基本信息的权重分值
        String totalScore = getCalculateQuota(creditPlatformRegistrationjsonObjList,creditVo);
        //记录授信步骤
        creditVo.setTotalScore(totalScore);
        insertBaseCreditInfo(creditVo);
        updateTbWallet(creditVo);
        updateCUserInfo(creditVo);
        return totalScore;

    }
    //修改用户钱包姓名
    private void updateTbWallet(CreditVo creditVo){
        TbWallet tbWallet = new TbWallet();
        tbWallet.setUname(creditVo.getUserName());
        tbWallet.setWname(creditVo.getUserName());
        //修改tb_wallet用户名字，钱包名字
        iTbWalletService.update(tbWallet,new EntityWrapper<TbWallet>().eq("uid",
                creditVo.getAccountid()));

    }


    /**
     * @Description: 获取初始授信评分项
     * @Param: [creditVo]
     * @return: java.util.Map
     * @Author: JinPeng
     * @Date: 2018/9/26
    */
    @Override
    public Map<String,RmBaseCreditType> getRmBaseCreditInitialList(CreditVo creditVo){

        //查询当前版本
        RmSxtemplateRelationship rmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(creditVo.getFid());

        //查询初始化授信记录列表
        List<RmBaseCreditInitial> rbcilist = iRmBaseCreditInitialService.selectList(new EntityWrapper<RmBaseCreditInitial>().eq("versionId", rmSxtemplateRelationship.getSxInitId()).eq("status", RmBaseCreditInitial.STATUS_YES));

        //查询授信类别列表
        List<RmBaseCreditType> rbctlist = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().eq("status","0"));


        //封装数据
        Map<String,RmBaseCreditType> map = new HashMap<>();
        for (RmBaseCreditType rbct: rbctlist) {
            map.put(rbct.getId(),rbct);
        }

        for(RmBaseCreditInitial rbcit: rbcilist) {
            map.get(rbcit.getTypeId()).getRbcilist().add(rbcit);
        }

        return map;
    }

    //修改用户姓名
    private void updateCUserInfo(CreditVo creditVo){
        CUserInfo cUserInfo = new CUserInfo();
        cUserInfo.setId(creditVo.getUid());
        cUserInfo.setRealName(creditVo.getUserName());
        iCUserInfoService.updateById(cUserInfo);
    }

    /**
     * @Description: 记录授信步骤 以便重新进来授信可以定位
     * @Param: [creditVo]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/14
     */
    private void insertBaseCreditInfo(CreditVo creditVo){
        BaseCreditInfo bci = new BaseCreditInfo();
        bci.setIdcard(creditVo.getIdCard());
        bci.setAccountno(creditVo.getAccountNO());
        bci.setMobile(creditVo.getMobile());
        bci.setCreateDate(new Date());
        bci.setUsername(creditVo.getUserName());
        bci.setStatus("1");
        bci.setUid(creditVo.getUid());
        bci.setGrade(creditVo.getTotalScore());
        iBaseCreditInfoService.insert(bci);
    }


    /** 
     * @Description: 记录拒绝授信条件
     * @Param: [twoapplications, refusingCode, creditVo] 
     * @return: void 
     * @Author: JinPeng
     * @Date: 2018/9/25 
    */
    @Override
    public void updateCUserRefConByUId(String twoapplications, String refusingCode, CreditVo creditVo,String examineFailReason){

        RmSxtemplateRelationship selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(creditVo.getFid());
        //用户关系信息
        CUserRefCon cUserRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid",creditVo.getUid()).eq("oid",creditVo.getOid()));
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        //这里是做了三个月后再次授信日期处理
        c.add(Calendar.MONTH, 3);
        cUserRefCon.setTwoapplications(twoapplications);
        cUserRefCon.setRefusingCode(refusingCode);
        cUserRefCon.setAuthExamine(cUserRefCon.AUTHEXAMINE_2);
        cUserRefCon.setNextauthDate(c.getTime());
        cUserRefCon.setAuthDate(new Date());
        cUserRefCon.setExamineFailReason(examineFailReason);
        cUserRefCon.setAuthStatus(cUserRefCon.AUTHSTATUS_JJ);
        cUserRefCon.setVersionId(selectRmSxtemplateRelationship.getId());
        if(ValidateUtil.isEmpty(twoapplications)&&cUserRefCon.getCreditCount().equals("2")){
            cUserRefCon.setCreditCount("1");
        }else{
            Integer count = Integer.parseInt(cUserRefCon.getCreditCount())+1;
            cUserRefCon.setCreditCount(count.toString());
        }
        iCUserRefConService.update(cUserRefCon, new EntityWrapper<CUserRefCon>().eq("uid",cUserRefCon.getUid()));
    }



    //计算基本信息的权重分值
    public String getCalculateQuota(ArrayList<CreditPlatformRegistrationDetails> result,CreditVo creditVo){

        HashMap<String,Object> resultMap = new HashMap<>();
        String idCard = creditVo.getIdCard();
        String uid = creditVo.getUid();
        String oid = creditVo.getOid();
        try {

            BigDecimal totalScore = new BigDecimal("0");
            //计算年龄
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            Date date = new Date();
            String age = idCard.substring(6,10);
            String now = sdf.format(date);
            Integer ageInt = Integer.parseInt(now)-Integer.parseInt(age);
            //查询初始授信模板条件
            Map<String,RmBaseCreditType> map = getRmBaseCreditInitialList(creditVo);

            //查询当前版本
            RmSxtemplateRelationship rmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(creditVo.getFid());




            //根据年龄获取对应额度
            BigDecimal ageScore = ageConvert(ageInt,map);
            logger.info("***************ageScore:" + ageScore + "******************");


            TbCreditGrade tbCreditGrade = new TbCreditGrade();
            tbCreditGrade.setOid(oid);
            tbCreditGrade.setStatus("1");
            tbCreditGrade.setUid(uid);
            tbCreditGrade.setDetails(ageInt.toString());
            tbCreditGrade.setCode(CreditConstant.AGESCORE);
            tbCreditGrade.setName("年龄");
            tbCreditGrade.setValue(ageScore.toString());
            tbCreditGrade.setVersionId(rmSxtemplateRelationship.getId());
            iTbCreditGradeService.insert(tbCreditGrade);


            //根据性别获取对应额度
            BigDecimal sexScore= sexConvert(idCard,map);
            logger.info("***************sex:" + sexScore + "******************");
            totalScore = totalScore.add(sexScore);
            String sexTemp = idCard.substring(idCard.length()-2,idCard.length()-1);
            TbCreditGrade tg = new TbCreditGrade();
            tg.setOid(oid);
            tg.setStatus("1");
            tg.setUid(uid);

            if(Integer.parseInt(sexTemp)%2==0)
            {
                tg.setDetails("女");
            }else {
                tg.setDetails("男");
            }

            tg.setCode(CreditConstant.SEXSCORE);
            tg.setName("性别");
            tg.setValue(sexScore.toString());
            tg.setVersionId(rmSxtemplateRelationship.getId());
            iTbCreditGradeService.insert(tg);

            //根据户籍获取对应额度
            BigDecimal cityScore = cityConvert(idCard,map,uid,oid,rmSxtemplateRelationship.getId());
            logger.info("***************cityScore:" + cityScore + "******************");

            totalScore = totalScore.add(cityScore);
            //根据多头负债获取对应额度
            BigDecimal mutipleScore = MutipleConvert(result,CreditConstant.MULTIPLE_INFO,map,uid,oid,rmSxtemplateRelationship.getId());
            logger.info("***************mutipleScore:" + mutipleScore + "******************");

            totalScore = totalScore.add(mutipleScore);

            //基本信息算完后的总额度
            resultMap.put("totalScore",totalScore);
            logger.info("***************totalScore:" + totalScore);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return resultMap.get("totalScore").toString();
    }

    /**
     * @Description:添加接口调用记录
     * @Param: [jsonStr, type, typeName, uid]
     * @return: void
     * @Author: JinPeng
     * @Date: 2018/9/13
     */
    public void toAddJASONStr(String jsonStr, String type, String typeName, CreditVo creditVo)
    {


        CCreditUserInfo multipleNewUserInfo = new CCreditUserInfo();
        multipleNewUserInfo.setCreateTime(new Date());
        multipleNewUserInfo.setJsonStr(jsonStr);
        multipleNewUserInfo.setType(type);
        multipleNewUserInfo.setTypeName(typeName);
        multipleNewUserInfo.setUid(creditVo.getUid());

        String carrier = OperatorUtil.getCarrier(creditVo.getMobile());

        if (!ValidateUtil.isEmpty(carrier) && "中国移动".equals(carrier)){

            multipleNewUserInfo.setOperatorType("0");

        }else if (!ValidateUtil.isEmpty(carrier) && "中国联通".equals(carrier)){

            multipleNewUserInfo.setOperatorType("1");

        }else if (!ValidateUtil.isEmpty(carrier) && "中国电信".equals(carrier)){

            multipleNewUserInfo.setOperatorType("2");

        }else {
            multipleNewUserInfo.setOperatorType(null);
        }
        //接口返回结果收集
        baseCreditConfigService.addJASONStr(multipleNewUserInfo);
    }

    /**
     * @Description: 初始化参数
     * @Param: [type, txpd, ibfismap]
     * @return: java.lang.String
     * @Author: JinPeng
     * @Date: 2018/8/6
     */
    public String jointParm(String type, TianXingParmDto txpd, HashMap<String, Object> ibfismap){

        String url = "";

        if (CreditConstant.THREELEMENT.equals(type)){        //银行卡三要素参数
            url = ibfismap.get("threelementUrl").toString() + "account=" + txpd.getAccount() + "&accountNO=" + txpd.getAccountNO() + "&name=" + txpd.getUserName() + "&idCard=" + txpd.getIdCard();
        }else if (CreditConstant.OPERATORS.equals(type)){    //运营商三要素参数
            url = ibfismap.get("operatorsUrl").toString() + "account=" + txpd.getAccount() + "&mobile=" + txpd.getMobile() + "&name=" + txpd.getUserName() + "&idCard=" + txpd.getIdCard();
        }else if (CreditConstant.MULTIPLENEW.equals(type)){    //多重负债参数
            url = ibfismap.get("multipleNewUrl").toString() + "account=" + txpd.getAccount() + "&mobile=" + txpd.getMobile();
        }
        return url;
    }



    /**
     * json添加到数据库
     * @param cCreditUserInfo
     */
    public void addJASONStr(CCreditUserInfo cCreditUserInfo)
    {
        cCreditUserInfoMapper.insert(cCreditUserInfo);
    }

    /**
     * 验证历史数据
     * @param map
     * @return
     */
    public CCreditUserInfo checkUserCreditStatus(HashMap<String,Object> map)
    {
        CCreditUserInfo c =  cCreditUserInfoMapper.checkUserCreditStatus(map);
        return null;
    }


    /**
     * 根据类型获取授信基础信息
     * @param type
     * @return
     */
    @Override
    public List<BaseCreditConfig> getListByType(String type) {
        return baseCreditConfigMapper.getListByType(type);
    }


    /**
     * 获取性别评分
     * @param idCard
     * @return
     * @throws Exception
     */
    public BigDecimal sexConvert(String idCard, Map<String,RmBaseCreditType> map) throws Exception
    {

        //性别区间权重
        BigDecimal sexCredit = new BigDecimal("0");
        //性别权重
        BigDecimal sexLong = new BigDecimal("0");

        String sexTemp = idCard.substring(idCard.length()-2,idCard.length()-1);

        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("sexScore")){
                rbcilist = ent.getValue().getRbcilist();
                sexLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        for(RmBaseCreditInitial rm : rbcilist){
            if(Integer.parseInt(sexTemp)%2==0 && "firstSexWeight".equals(rm.getCode())){
                sexCredit = new BigDecimal(rm.getValue());
                break;
            }else if (Integer.parseInt(sexTemp)%2 !=0 && "secondSexWeight".equals(rm.getCode())){
                sexCredit = new BigDecimal(rm.getValue());
                break;
            }
        }


        return sexLong.multiply(sexCredit);
    }


    /**
     * 获取年龄评分
     * @return
     */
    private BigDecimal ageConvert(Integer ageInt,Map<String,RmBaseCreditType> map)
    {

        //年龄区间权重
        BigDecimal ageCredit = new BigDecimal("0");
        //年龄权重
        BigDecimal ageLong = new BigDecimal("0");
        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("ageScore")){
                rbcilist = ent.getValue().getRbcilist();
                ageLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }

        for(RmBaseCreditInitial rm : rbcilist){
            if (ValidateUtil.isEmpty(rm.getMaxScore())){
                rm.setMaxScore(new BigDecimal(ageInt));
            }
            if(Integer.parseInt(rm.getMaxScore().toString()) >= ageInt && ageInt >= Integer.parseInt(rm.getMinScore().toString())){
                ageCredit = new BigDecimal(rm.getValue());
                break;
            }
        }
        return ageCredit.multiply(ageLong);
    }

    /**
     * 获取城市评分
     * @param idCard
     * @return
     */
    public BigDecimal cityConvert(String idCard,Map<String,RmBaseCreditType> map,String uid,String oid,String versionId) throws Exception
    {

        //城市区间权重
        BigDecimal cityCredit = new BigDecimal("0");
        //城市权重
        BigDecimal cityLong = new BigDecimal("0");
        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("addressScore")){
                rbcilist = ent.getValue().getRbcilist();
                cityLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }

        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditInitial rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }




        String cityArea = idCard.substring(0,6);
        SysArea sa = sysAreaMapper.getAreaByCode(cityArea);
        TbCreditGrade tbCreditGrade = new TbCreditGrade();

        logger.info("****************城市***************");
        if (ValidateUtil.isEmpty(sa)){
            cityCredit = creditmap.get("otherCityWeight");
            tbCreditGrade.setDetails("其他城市");
        }else {
            String cityLevel = sa.getCityLevel();

            if(cityLevel==null || "".equals(cityLevel) || cityLevel.equals(CreditConstant.CITY_LEVEL_FOUR))//其他城市
            {
                cityCredit = creditmap.get("otherCityWeight");
                tbCreditGrade.setDetails("其他城市");
            }
            else if(CreditConstant.CITY_LEVEL_FIRST.equals(cityLevel))
            {
                cityCredit = creditmap.get("firstCityWeight");//一线城市
                tbCreditGrade.setDetails("一线城市");
            }
            else if(CreditConstant.CITY_LEVEL_SECOND.equals(cityLevel))//二线城市
            {
                cityCredit = creditmap.get("secondCityWeight");
                tbCreditGrade.setDetails("二线城市");
            }
            else if(CreditConstant.CITY_LEVEL_THREE.equals(cityLevel))//三线城市
            {
                cityCredit = creditmap.get("threeCityWeight");
                tbCreditGrade.setDetails("三线城市");
            }
            else if(CreditConstant.CITY_LEVEL_FOUR.equals(cityLevel))//四线城市
            {
                cityCredit = creditmap.get("fourCityWeight");
                tbCreditGrade.setDetails("四线城市");
            }
        }

        cityCredit = ValidateUtil.isEmpty(cityCredit) ? new BigDecimal("0") : cityCredit;

        tbCreditGrade.setOid(oid);
        tbCreditGrade.setStatus("1");
        tbCreditGrade.setUid(uid);
        tbCreditGrade.setCode(CreditConstant.CITYSCORE);
        tbCreditGrade.setName("户籍");
        tbCreditGrade.setValue(cityLong.multiply(cityCredit).toString());
        tbCreditGrade.setVersionId(versionId);
        tbCreditGradeMapper.insert(tbCreditGrade);

        return cityLong.multiply(cityCredit);


    }

    /** 
     * @Description: 根据电话记录数算额度 
     * @Param: [map, type, contactCnt] 
     * @return: java.lang.Double 
     * @Author: JinPeng
     * @Date: 2018/9/26 
    */ 
    public BigDecimal tagMobileConvert(Map<String,RmBaseCreditType> map, String type, Double contactCnt) throws Exception{
        
        //城市区间权重
        BigDecimal tagMobilecredit = new BigDecimal("0");
        //城市权重
        BigDecimal scoreDouble = new BigDecimal("0");
        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("titleScore")){
                rbcilist = ent.getValue().getRbcilist();
                scoreDouble = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }

        try
        {
            if(type.equals(CreditConstant.CONTACTCNT_INFO))//非标签号码
            {


                for (RmBaseCreditInitial rm : rbcilist){
                    if (ValidateUtil.isEmpty(rm.getMaxScore())){
                        rm.setMaxScore(new BigDecimal(contactCnt));
                    }
                    if(Integer.parseInt(rm.getMaxScore().toString()) >= contactCnt && contactCnt >= Integer.parseInt(rm.getMinScore().toString())){
                        tagMobilecredit = new BigDecimal(rm.getValue());
                        break;
                    }
                }

            }

        }catch (Exception e)
        {
            logger.error("计算详单非标签类号码数量评分出错:"+e.getMessage());
        }

        return tagMobilecredit.multiply(scoreDouble);
    }

    /**
     * 获取催收类电话评分
     * @param type
     * @param debtCallCnt
     * @return
     */
    public BigDecimal debtCallConvert(Map<String,RmBaseCreditType> map, String type, Double debtCallCnt) throws Exception{

        //城市区间权重
        BigDecimal debtCallcredit = new BigDecimal("0");
        //城市权重
        BigDecimal scoreDouble = new BigDecimal("0");
        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("mobileScore")){
                rbcilist = ent.getValue().getRbcilist();
                scoreDouble = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }

        try
        {
            if(type.equals(CreditConstant.DEBTCAll_INFO))//非标签号码
            {
                for (RmBaseCreditInitial rm : rbcilist){
                    if(debtCallCnt.equals(rm.getNumber())){
                        debtCallcredit = new BigDecimal(rm.getValue());
                        break;
                    }
                }
            }

        }catch (Exception e)
        {
            logger.error("计算催收类号码数量评分出错:"+e.getMessage());
        }

        return debtCallcredit.multiply(scoreDouble);
    }

    /**
     * 计算多头负债评分
     * @param
     * @param type 类型
     * @return
     */
    public BigDecimal MutipleConvert(ArrayList<CreditPlatformRegistrationDetails> result, String type, Map<String,RmBaseCreditType> map,String uid,String oid,String versionId) throws Exception
    {

        //城市区间权重
        BigDecimal creditLong = new BigDecimal("0");
        //城市权重
        BigDecimal scoreLong = new BigDecimal("0");
        List<RmBaseCreditInitial> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("moduleScore")){
                rbcilist = ent.getValue().getRbcilist();
                scoreLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }

        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditInitial rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }





        try
        {
            if(type.equals(CreditConstant.MULTIPLE_INFO))//多头负债
            {
                TbCreditGrade tbCreditGrade = new TbCreditGrade();

                for (RmBaseCreditInitial rm : rbcilist){
                    if(Integer.parseInt(rm.getMaxScore().toString()) >= result.size() && result.size() >= Integer.parseInt(rm.getMinScore().toString())){
                        creditLong = new BigDecimal(rm.getValue());
                        break;
                    }
                }

                tbCreditGrade.setDetails(result.size() + "");
                tbCreditGrade.setOid(oid);
                tbCreditGrade.setStatus("1");
                tbCreditGrade.setUid(uid);
                tbCreditGrade.setCode(CreditConstant.MUTIPLESCORE);
                tbCreditGrade.setName("多头负债");
                tbCreditGrade.setValue(scoreLong.multiply(creditLong)+ "");
                tbCreditGrade.setVersionId(versionId);
                tbCreditGradeMapper.insert(tbCreditGrade);

            }



        }
        catch (Exception e)
        {
            logger.error("计算多头负债评分出错:"+e.getMessage());
        }
        return scoreLong.multiply(creditLong);
    }

    @Override
    public BigDecimal getTotalScore(String totalScore, String fid) throws Exception{

        logger.info("*******************totalScore:{}*****************", totalScore);

        //查询当前版本
        RmSxtemplateRelationship  selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(fid);

        List<CreditScore> cslist = creditScoreService.selectList(new EntityWrapper<CreditScore>().eq("version_id", selectRmSxtemplateRelationship.getSxUpmoneyFocusId()));

        BigDecimal t = new BigDecimal("0");

        Double tscore = Double.parseDouble(totalScore);

        for (CreditScore cs : cslist){
            if(Integer.parseInt(cs.getMaxScore().toString()) >= tscore && tscore >= Integer.parseInt(cs.getMinScore().toString())){
                t = cs.getAmount();
                logger.info("*******************t:{}*****************", t);
                break;
            }
        }

        return t;
    }

    /**
     * 银行卡三要素
     * @param jasonStr
     * @param type
     */
    public String dataConvertScore(String jasonStr,String type) throws Exception
    {
        String returnStr="false";
        try
        {
            JSONObject json = new JSONObject(jasonStr);
            if(CreditConstant.CARD_INFO.equals(type))//三要素
            {
                CreditAuthInfo cai = (CreditAuthInfo)json.get("data");
                if(CreditConstant.CARD_SAME.equals(cai.getCheckStatus()))
                {
                    returnStr =  "true";
                }
                else if(CreditConstant.CARD_DIFFERENT.equals(cai.getCheckStatus()))
                {
                    returnStr =  "false";
                }
                else
                {
                    returnStr = "error";
                }
            }
            else if(type.equals(CreditConstant.POLICE_INFO))//犯罪信息
            {
                CreditPoliceInfo cpi =(CreditPoliceInfo)json.get("data");
                if(CreditConstant.POLICE_INFO.equals(type))
                {
                    if(cpi.getEscapeCompared().equals("不一致"))
                    {
                        returnStr =  "false";
                    }
                    if(cpi.getDrugCompared().equals("不一致"))
                    {
                        returnStr =  "false";
                    }
                    if(cpi.getDrugRelated().equals("不一致"))
                    {
                        returnStr =  "false";
                    }
                }
            }
            else if(type.equals(CreditConstant.MOBILE_INFO))//运营商
            {
                CreditMobileInfo cmi = (CreditMobileInfo)json.get("data");
                if(cmi.getCompareStatus().equals("SAME"))
                {
                    returnStr = "true";
                }
                else if(cmi.getCompareStatus().equals("DIFFERENT"))
                {
                    returnStr = "true";
                }
                else if(cmi.getCompareStatus().equals("NO_DATA"))
                {
                    returnStr = "error";
                }

                if(cmi.getCompareStatusDesc().equals("一致"))
                {
                    returnStr = "true";
                }
                else if(cmi.getCompareStatusDesc().equals("不一致"))
                {
                    returnStr = "false";
                }
                else if(cmi.getCompareStatusDesc().equals("NO_DATA"))
                {
                    returnStr = "error";
                }
            }
        }
        catch(Exception e)
        {
            logger.error("银行卡三要素出错:"+e.getMessage());
            returnStr="error";
        }

        return returnStr;
    }

}
