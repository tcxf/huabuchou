package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.BaseCreditInfo;
import com.tcxf.hbc.ctu.mapper.BaseCreditInfoMapper;
import com.tcxf.hbc.ctu.service.IBaseCreditInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信四要素表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-16
 */
@Service
public class BaseCreditInfoServiceImpl extends ServiceImpl<BaseCreditInfoMapper, BaseCreditInfo> implements IBaseCreditInfoService {

}
