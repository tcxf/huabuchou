package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.CCreditUserInfo;
import com.tcxf.hbc.ctu.mapper.CCreditUserInfoMapper;
import com.tcxf.hbc.ctu.service.ICCreditUserInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-26
 */
@Service
public class CCreditUserInfoServiceImpl extends ServiceImpl<CCreditUserInfoMapper, CCreditUserInfo> implements ICCreditUserInfoService {


    @Override
    public List<CCreditUserInfo> getListByType(String type) {
        return null;
    }

    public Boolean getJsonByUid(String uId, String type)
    {
        return false;
    }

    public  void saveJsonByUid(String jsonStr,String uId,String type)
    {

    }



}
