package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.COperaSwitch;
import com.tcxf.hbc.ctu.mapper.COperaSwitchMapper;
import com.tcxf.hbc.ctu.service.ICOperaSwitchService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商开关表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-09-10
 */
@Service
public class COperaSwitchServiceImpl extends ServiceImpl<COperaSwitchMapper, COperaSwitch> implements ICOperaSwitchService {

}
