package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.ctu.mapper.CUserInfoMapper;
import com.tcxf.hbc.ctu.service.ICUserInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
@Slf4j
public class CUserInfoServiceImpl extends ServiceImpl<CUserInfoMapper, CUserInfo> implements ICUserInfoService {

    @Autowired
    private  CUserInfoMapper cUserInfoMapper;

    /**
     * 根据用户id查询用户信息
     * @param userId
     * @return
     */
    @Override
    @Cacheable(value = "user_info", key = "#userId  + '_user_info'")
    public CUserInfo getUserInfo(String userId) {
        return this.selectById(userId);
    }

    /**
     * 根据用户id删除用户信息
     * @param userId
     */
    @Override
    @CacheEvict(value = "user_info", key = "#userId + '_user_info'")
    public void delUserInfo(String userId) {
        this.deleteById(userId);
    }

    /**
     * 根据用户id更新用户信息
     * @param cUserInfo
     */
    @Override
    @CachePut(value = "user_info", key = "#userId + '_user_info'")
    public void modifyUserInfo(CUserInfo cUserInfo)
    {
        this.updateById(cUserInfo);
    }

    /**
     * 新增用户
     * @param cUserInfo
     */
    public void addUserInfo(CUserInfo cUserInfo)
    {
        this.insert(cUserInfo);
    }

    /**
     * 分页查询用户列表
     * @param params
     * @return
     */
    @Override
    public Page selectUserInfoPage( Map<String, Object> params)
    {
        params.put("status",CommonConstant.STATUS_NORMAL);
       return this.selectPage(new Query<CUserInfo>(params),new EntityWrapper<CUserInfo>());
    }

    /**
     * 分页查询用户信息
     * @param status
     * @return
     */
    @Override
    public Page queryList(String status)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query<CUserInfo> query = new  Query(params);
        return  this.queryList(query);
    }

    /**
     * 分页查询用户信息
     * @param query
     * @return
     */
    @Override
    public Page queryList( Query<CUserInfo> query)
    {
        List<CUserInfo> list = cUserInfoMapper.queryUserInfoForList(query, query.getCondition());
        query.setRecords(list);
        return query;
    }

}
