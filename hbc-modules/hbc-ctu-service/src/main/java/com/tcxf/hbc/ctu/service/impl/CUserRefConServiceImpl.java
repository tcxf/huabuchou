package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.ctu.mapper.CUserRefConMapper;
import com.tcxf.hbc.ctu.mapper.TbUpMoneyMapper;
import com.tcxf.hbc.ctu.model.dto.MycreditDetailsDto;
import com.tcxf.hbc.ctu.service.IBaseCreditConfigService;
import com.tcxf.hbc.ctu.service.ICUserRefConService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class CUserRefConServiceImpl extends ServiceImpl<CUserRefConMapper, CUserRefCon> implements ICUserRefConService {

    Logger logger = LoggerFactory.getLogger(IBaseCreditConfigService.class);

    @Autowired
    CUserRefConMapper cUserRefConMapper;

    @Override
    public void updateCUserRefConByUid(HashMap<String, Object> map){
        cUserRefConMapper.updateCUserRefConByUid(map);
    }

    @Override
    public void updateCUserRefConCountByUid(HashMap<String, Object> map){
        cUserRefConMapper.updateCUserRefConCountByUid(map);
    }

    public CUserRefCon getCUserRefConByUid(HashMap<String, Object> map){
        return cUserRefConMapper.getCUserRefConByUid(map);
    }

    @Override
    public MycreditDetailsDto getMycreditDetails(HashMap<String, Object> map){
        return cUserRefConMapper.getMycreditDetails(map);
    }

}

