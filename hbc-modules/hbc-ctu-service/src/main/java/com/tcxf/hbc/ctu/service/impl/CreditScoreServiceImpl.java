package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.CreditScore;
import com.tcxf.hbc.ctu.mapper.CreditScoreMapper;
import com.tcxf.hbc.ctu.service.CreditScoreService;
import org.springframework.stereotype.Service;


/**
 * 授信提额综合分值service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class CreditScoreServiceImpl extends ServiceImpl<CreditScoreMapper, CreditScore> implements CreditScoreService {

}
