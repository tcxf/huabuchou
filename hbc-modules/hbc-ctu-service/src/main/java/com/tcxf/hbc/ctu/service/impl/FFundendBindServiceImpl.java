package com.tcxf.hbc.ctu.service.impl;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.ctu.mapper.FFundendBindMapper;
import com.tcxf.hbc.ctu.service.IFFundendBindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class FFundendBindServiceImpl extends ServiceImpl<FFundendBindMapper, FFundendBind> implements IFFundendBindService {

    @Autowired
    FFundendBindMapper fFundendBindMapper;

    public FFundendBind getFFundendBindByOid(String oid){

        return fFundendBindMapper.getFFundendBindByOid(oid);
    }
}
