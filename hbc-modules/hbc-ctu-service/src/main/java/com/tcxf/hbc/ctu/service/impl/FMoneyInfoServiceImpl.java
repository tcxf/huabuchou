package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.FMoneyInfo;
import com.tcxf.hbc.ctu.mapper.FMoneyInfoMapper;
import com.tcxf.hbc.ctu.service.IFMoneyInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金端授信上限表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@Service
public class FMoneyInfoServiceImpl extends ServiceImpl<FMoneyInfoMapper, FMoneyInfo> implements IFMoneyInfoService {

}
