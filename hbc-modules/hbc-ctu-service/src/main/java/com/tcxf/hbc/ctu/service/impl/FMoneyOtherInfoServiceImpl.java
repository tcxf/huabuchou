package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.FMoneyOtherInfo;
import com.tcxf.hbc.ctu.mapper.FMoneyOtherInfoMapper;
import com.tcxf.hbc.ctu.service.IFMoneyOtherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * <p>
 * 授信总金额出入明细表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-09-05
 */
@Service
public class FMoneyOtherInfoServiceImpl extends ServiceImpl<FMoneyOtherInfoMapper, FMoneyOtherInfo> implements IFMoneyOtherInfoService {

    @Autowired
    FMoneyOtherInfoMapper fMoneyOtherInfoMapper;

    public BigDecimal getFMoneyOtherInfoSumByFid(HashMap<String, Object> map){
        return fMoneyOtherInfoMapper.getFMoneyOtherInfoSumByFid(map);
    }

}
