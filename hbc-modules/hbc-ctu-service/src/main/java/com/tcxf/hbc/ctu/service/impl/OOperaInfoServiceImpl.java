package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.ctu.mapper.OOperaInfoMapper;
import com.tcxf.hbc.ctu.service.IOOperaInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
@Service
public class OOperaInfoServiceImpl extends ServiceImpl<OOperaInfoMapper, OOperaInfo> implements IOOperaInfoService {

}
