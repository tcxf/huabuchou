package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RefusingCredit;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.ctu.mapper.RefusingCreditMapper;
import com.tcxf.hbc.ctu.service.IRmSxtemplateRelationshipService;
import com.tcxf.hbc.ctu.service.RefusingCreditService;
import com.tcxf.hbc.ctu.service.RefusingVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拒绝授信条件service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class RefusingCreditServiceImpl extends ServiceImpl<RefusingCreditMapper, RefusingCredit>  implements RefusingCreditService {

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Autowired
    private IRmSxtemplateRelationshipService rmSxtemplateRelationshipService;

    @Autowired
    private RefusingCreditMapper refusingCreditMapper;
    /**
     * 创建拒绝授信条件
     * @param list
     * @param fid
     */
    @Override
    public void createRefusingCredit(List<RefusingCredit> list,String fid) {
        //1、数据验证
        validatecreateRefusingCredit(list,fid);
        //3、创建新版本
        String refusingVersionId = insertRefusingVersion(fid);
        //4、创建新的拒绝授信条件
        for(RefusingCredit r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRefusingVersion(String fid){
        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_1);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_1));
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }

    /**
     * 数据验证
     */
    private void validatecreateRefusingCredit(List<RefusingCredit> list,String fid){
        for(RefusingCredit r : list){
            if(ValidateUtil.isEmpty(r.getNumber())||ValidateUtil.isEmpty(r.getTwoapplications())){
                throw new CheckedException(r.getName()+"的条件个数不能为空");
            }
        }
        List<RefusingCredit> selectList = selectRefusingCreditByfId(fid);
        if(selectList.size()!=list.size()){
            return;
        }
        //分组
        Map<String ,RefusingCredit> map = new HashMap<String,RefusingCredit>();
        for(RefusingCredit r : list){
            map.put(r.getCode(),r);
        }
        for(RefusingCredit r : list) {
            if(ValidateUtil.isEmpty(map.get(r.getCode()))){
                return;
            }
            if(r.getNumber()!=map.get(r.getCode()).getNumber()||
                    r.getTwoapplications()!=map.get(r.getCode()).getTwoapplications()){
                return;
            }
        }
        throw new CheckedException("未修改条件，无需更新");
    }

    /**
     * 根据资金端ID查询最新版本的条件
     * @return
     */
    @Override
    public List<RefusingCredit> selectRefusingCreditByfId(String fid){
        //查询最新大版本
        RmSxtemplateRelationship selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(fid);
        List<RefusingCredit> list = refusingCreditMapper.selectRefusingCreditByvId(selectRmSxtemplateRelationship.getSxRefusingId());
        return list;
    }


    /**
     * 根据拒绝授信条件ID查询
     * @return
     */
    @Override
    public RefusingCredit selectRefusingCreditById(String id){
        return null;
    }


}
