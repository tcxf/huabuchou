package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.ctu.mapper.RefusingVersionMapper;
import com.tcxf.hbc.ctu.service.RefusingVersionService;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 拒绝授信条件service
 * @Auther: liuxu
 * @Date: 2018/9/19 16:15
 * @Description:
 */
@Service
public class RefusingVersionServiceImpl extends ServiceImpl<RefusingVersionMapper, RefusingVersion>  implements RefusingVersionService {

    @Override
    public String createVersion(String fid,String type) {
        RefusingVersion selectRefusingVersion = selectOne(new EntityWrapper<RefusingVersion>().eq("status",RefusingVersion.STATUS_YES).eq("fid",fid).eq("type",type));
        if(ValidateUtil.isEmpty(selectRefusingVersion)){
            return DateUtil.format(new Date(),DateUtil.FORMAT_YYYYMMDD)+"001";
        }
        return String.valueOf(Integer.parseInt(selectRefusingVersion.getVersion())+1);
    }
}
