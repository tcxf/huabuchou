package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmBaseCreditInitial;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.ctu.mapper.RmBaseCreditInitialMapper;
import com.tcxf.hbc.ctu.service.IRmBaseCreditInitialService;
import com.tcxf.hbc.ctu.service.RefusingVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 初始授信条件模板表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-19
 */
@Service
public class RmBaseCreditInitialServiceImpl extends ServiceImpl<RmBaseCreditInitialMapper, RmBaseCreditInitial> implements IRmBaseCreditInitialService {

    @Autowired
    RmBaseCreditInitialMapper rmBaseCreditInitialMapper;

    @Autowired
    private RefusingVersionService refusingVersionService;

    @Override
    public List<RmBaseCreditInitial> getRmBaseCreditInitialList(RmBaseCreditInitial rmBaseCreditInitial) {
        return rmBaseCreditInitialMapper.getListByType(rmBaseCreditInitial);
    }


    /**
     * 创建初始授信条件
     * @param list
     * @param fid
     */
    @Override
    public void createRmBaseCreditInitial(List<RmBaseCreditInitial> list, String fid) {
        //3、创建新版本
        String refusingVersionId = insertRmBaseCreditInitial(fid);
        //4、创建新的拒绝授信条件
        for(RmBaseCreditInitial r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRmBaseCreditInitial(String fid){


        RefusingVersion rfVersion = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("type",RefusingVersion.TYPE_2).eq("status",RefusingVersion.STATUS_YES).eq("fId","1"));


        //将现在的有效版本修改为失效版本
        if (!ValidateUtil.isEmpty(rfVersion)){

            RefusingVersion rfv = new RefusingVersion();
            rfv.setId(rfVersion.getId());

            refusingVersionService.update(rfv,new EntityWrapper<>());
        }

        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_2);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_2));
        //新增新版本
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }

}
