package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RefusingVersion;
import com.tcxf.hbc.common.entity.RmBaseCreditUpmoney;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.ctu.mapper.RmBaseCreditUpmoneyMapper;
import com.tcxf.hbc.ctu.service.IRmBaseCreditUpmoneyService;
import com.tcxf.hbc.ctu.service.RefusingVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 提额授信条件模板表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-09-20
 */
@Service
public class RmBaseCreditUpmoneyServiceImpl extends ServiceImpl<RmBaseCreditUpmoneyMapper, RmBaseCreditUpmoney> implements IRmBaseCreditUpmoneyService {

    @Autowired
    private RefusingVersionService refusingVersionService;

    /**
     * 创建初始授信条件
     * @param list
     * @param fid
     */
    @Override
    public void createRmBaseCreditUpmoney(List<RmBaseCreditUpmoney> list, String fid) {
        //3、创建新版本
        String refusingVersionId = insertRmBaseCreditUpmoney(fid);
        //4、创建新的拒绝授信条件
        for(RmBaseCreditUpmoney r : list){
            r.setVersionId(refusingVersionId);
        }
        insertBatch(list);
    }

    /**
     * 新增版本
     * @param fid
     * @return
     */
    private String insertRmBaseCreditUpmoney(String fid){


        RefusingVersion rfVersion = refusingVersionService.selectOne(new EntityWrapper<RefusingVersion>().eq("type",RefusingVersion.TYPE_3).eq("status",RefusingVersion.STATUS_YES).eq("fId","1"));


        //将现在的有效版本修改为失效版本
        if (!ValidateUtil.isEmpty(rfVersion)){

            RefusingVersion rfv = new RefusingVersion();
            rfv.setId(rfVersion.getId());

            refusingVersionService.update(rfv,new EntityWrapper<>());
        }

        RefusingVersion refusingVersion = new RefusingVersion();
        refusingVersion.setCreateDate(new Date());
        refusingVersion.setFid(fid);
        refusingVersion.setType(RefusingVersion.TYPE_3);
        refusingVersion.setVersion(refusingVersionService.createVersion(fid,RefusingVersion.TYPE_3));
        //新增新版本
        refusingVersionService.insert(refusingVersion);
        return refusingVersion.getId();
    }
}
