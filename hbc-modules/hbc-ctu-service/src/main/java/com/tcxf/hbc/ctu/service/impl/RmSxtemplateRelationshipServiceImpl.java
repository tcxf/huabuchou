package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.RmSxtemplateRelationship;
import com.tcxf.hbc.ctu.mapper.RmSxtemplateRelationshipMapper;
import com.tcxf.hbc.ctu.service.IRmSxtemplateRelationshipService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信模板关系表 服务实现类
 * </p>
 *
 * @author pengjin
 * @since 2018-10-22
 */
@Service
public class RmSxtemplateRelationshipServiceImpl extends ServiceImpl<RmSxtemplateRelationshipMapper, RmSxtemplateRelationship> implements IRmSxtemplateRelationshipService {


    @Override
    public RmSxtemplateRelationship selectNewVersionByfid(String fid) {
        RmSxtemplateRelationship selectRmSxtemplateRelationship = selectOne(new EntityWrapper<RmSxtemplateRelationship>().
                eq("status",RmSxtemplateRelationship.STATUS_YES).eq("fid",fid));
        return selectRmSxtemplateRelationship;
    }
}
