package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.SysArea;
import com.tcxf.hbc.ctu.mapper.SysAreaMapper;
import com.tcxf.hbc.ctu.service.ISysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 行政区划 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-06-28
 */
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysArea> implements ISysAreaService {
    @Autowired
    private SysAreaMapper sysAreaMapper;
    @Override
    public SysArea getAreaByCode(String areaCode) {
        return sysAreaMapper.getAreaByCode(areaCode);
    }
}
