package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.ctu.mapper.TbAccountInfoMapper;
import com.tcxf.hbc.ctu.service.ITbAccountInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 用户授信资金额度表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class TbAccountInfoServiceImpl extends ServiceImpl<TbAccountInfoMapper, TbAccountInfo> implements ITbAccountInfoService {

    Logger logger = LoggerFactory.getLogger(TbAccountInfoServiceImpl.class);

    @Autowired
    TbAccountInfoMapper tbAccountInfoMapper;

    @Override
    public String insertTbAccountInfo(TbAccountInfo tbAccountInfo){
        tbAccountInfoMapper.insert(tbAccountInfo);
        return tbAccountInfo.getId();
    }

    @Override
    public void updateTbAccountInfoByUid(HashMap<String, Object> hashMap){
        tbAccountInfoMapper.updateTbAccountInfoByUid(hashMap);
    }

    @Override
    public TbAccountInfo getTbAccountInfoByUid(HashMap<String, Object> hashMap){
        return tbAccountInfoMapper.getTbAccountInfoByUid(hashMap);
    }

}
