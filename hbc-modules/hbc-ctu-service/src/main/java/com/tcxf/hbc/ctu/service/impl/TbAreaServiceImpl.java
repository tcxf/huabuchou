package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.ctu.mapper.TbAreaMapper;
import com.tcxf.hbc.ctu.service.ITbAreaService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
@Service
public class TbAreaServiceImpl extends ServiceImpl<TbAreaMapper, TbArea> implements ITbAreaService {

}
