package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbCreditGrade;
import com.tcxf.hbc.ctu.mapper.TbCreditGradeMapper;
import com.tcxf.hbc.ctu.service.ITbCreditGradeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快速授信评分项 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
@Service
public class TbCreditGradeServiceImpl extends ServiceImpl<TbCreditGradeMapper, TbCreditGrade> implements ITbCreditGradeService {

}
