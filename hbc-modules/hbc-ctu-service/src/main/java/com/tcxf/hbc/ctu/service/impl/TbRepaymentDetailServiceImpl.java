package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.ctu.mapper.TbRepaymentDetailMapper;
import com.tcxf.hbc.ctu.service.ITbRepaymentDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class TbRepaymentDetailServiceImpl extends ServiceImpl<TbRepaymentDetailMapper, TbRepaymentDetail> implements ITbRepaymentDetailService {

}
