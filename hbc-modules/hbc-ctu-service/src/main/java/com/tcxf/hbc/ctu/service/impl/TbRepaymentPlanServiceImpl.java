package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.ctu.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.ctu.service.ITbRepaymentPlanService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款计划表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-11
 */
@Service
public class TbRepaymentPlanServiceImpl extends ServiceImpl<TbRepaymentPlanMapper, TbRepaymentPlan> implements ITbRepaymentPlanService {

}
