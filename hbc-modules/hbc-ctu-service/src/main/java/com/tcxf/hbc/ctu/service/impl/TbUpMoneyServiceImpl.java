package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.ctu.common.constant.CreditConstant;
import com.tcxf.hbc.ctu.mapper.TbCreditGradeMapper;
import com.tcxf.hbc.ctu.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.ctu.mapper.TbUpMoneyMapper;
import com.tcxf.hbc.ctu.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 授信提额表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-07-11
 */
@Service
public class TbUpMoneyServiceImpl extends ServiceImpl<TbUpMoneyMapper, TbUpMoney> implements ITbUpMoneyService {

    Logger logger = LoggerFactory.getLogger(IBaseCreditConfigService.class);

    @Autowired
    private TbUpMoneyMapper tbUpMoneyMapper;

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;

    @Autowired
    private TbCreditGradeMapper tbCreditGradeMapper;
    
    @Autowired
    private IRmBaseCreditTypeService iRmBaseCreditTypeService;

    @Autowired
    private IRmBaseCreditUpmoneyService iRmBaseCreditUpmoneyService;

    @Autowired
    private IFFundendBindService iFFundendBindService;

    @Autowired
    private IRmSxtemplateRelationshipService rmSxtemplateRelationshipService;


    @Override
    public Integer getTokenIncreaseCreditByUid(HashMap<String, Object> map){
        return tbUpMoneyMapper.getTokenIncreaseCreditByUid(map);
    }

    public void updateOneStatusIncreaseCreditByid(HashMap<String, Object> map){
        tbUpMoneyMapper.updateOneStatusIncreaseCreditByid(map);
    }

    @Override
    public Integer getTokenIncreaseCreditPermitByUid(HashMap<String, Object> map){
        return tbUpMoneyMapper.getTokenIncreaseCreditPermitByUid(map);
    }

    @Override
    public Integer getNotTokenIncreaseCreditByUid(HashMap<String, Object> map){
        return tbUpMoneyMapper.getNotTokenIncreaseCreditByUid(map);
    }

    public List<TbUpMoney> getOneIncreaseCreditByUid(HashMap<String, Object> map){
            tbUpMoneyMapper.delTbUpMoneyByUid(map);
        return tbUpMoneyMapper.getOneIncreaseCreditByUid(map);
    }

    public List<TbUpMoney> getTwoIncreaseCreditByUid(HashMap<String, Object> map){
        return tbUpMoneyMapper.getTwoIncreaseCreditByUid(map);
    }

    public void updateTwoIncreaseCreditByid(HashMap<String, Object> map){
        tbUpMoneyMapper.updateTwoIncreaseCreditByid(map);
    }

    public void insertOneIncreaseCredit(TbUpMoney tbUpMoney){
        tbUpMoneyMapper.insert(tbUpMoney);
    }

    public List<TbUpMoney> getOneTokenIncreaseCreditByUid(HashMap<String, Object> map){
        return tbUpMoneyMapper.getOneTokenIncreaseCreditByUid(map);
    }


    /**
     * @Description: 获取初始授信评分项
     * @Param: [creditVo]
     * @return: java.util.Map
     * @Author: JinPeng
     * @Date: 2018/9/26
     */
    @Override
    public Map<String,RmBaseCreditType> getRmBaseCreditUpmoneyList(String Fid){

        //查询当前版本
        RmSxtemplateRelationship selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(Fid);


        List<RmBaseCreditUpmoney> rbcilist = iRmBaseCreditUpmoneyService.selectList(new EntityWrapper<RmBaseCreditUpmoney>().eq("versionId", selectRmSxtemplateRelationship.getSxUpmoneyId()).eq("status", RmBaseCreditUpmoney.STATUS_YES));

        //查询授信类别列表
        List<RmBaseCreditType> rbctlist = iRmBaseCreditTypeService.selectList(new EntityWrapper<RmBaseCreditType>().eq("status",RmBaseCreditType.TYPE_1));


        //封装数据
        Map<String,RmBaseCreditType> map = new HashMap<>();

        for (RmBaseCreditType rbct: rbctlist) {
            map.put(rbct.getId(),rbct);
        }

        for (RmBaseCreditUpmoney rbcit: rbcilist) {
            map.get(rbcit.getTypeId()).getRbculist().add(rbcit);
        }

        return map;
    }


    /**
     * 持久化社保提额额度（公积金和社保提额规则一样）
     * @param count
     * @param token
     */
    public void getSocialInsuranceScore(Map<String,RmBaseCreditType> map, int count, String token){


        //社保公积金区间权重
        BigDecimal socialCredit = new BigDecimal("0");
        //社保公积金权重
        BigDecimal socialLong = new BigDecimal("0");

        List<RmBaseCreditUpmoney> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("sspfScore")){
                rbcilist = ent.getValue().getRbculist();
                socialLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditUpmoney rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }



        if (count == 0){//近1个月无社保公积金信息，但以前有缴纳信息
            socialCredit = creditmap.get("firstSspfWeight");
        }else if (count == 1){//1个月社保公积金信息
            socialCredit = creditmap.get("secondSspfWeight");
        }else if (count >= 2 && count <= 3){//2-3个月社保公积金信息
            socialCredit = creditmap.get("threeSspfWeight");
        }else if (count >= 4 && count <= 6){//4-6个月社保公积金信息
            socialCredit = creditmap.get("fourSspfWeight");
        }else if (count >= 7){//7个月以上社保公积金信息
            socialCredit = creditmap.get("fiveSspfWeight");
        }

        socialCredit = ValidateUtil.isEmpty(socialCredit) ? new BigDecimal("0") : socialCredit;

        HashMap<String, Object> hmap = new HashMap<>();

        hmap.put("value", socialCredit.multiply(socialLong));
        hmap.put("token", token);

        //执行持久化社保提额额度
        tbUpMoneyMapper.updateTwoIncreaseCreditBytoken(hmap);

    }

    /**
     * 持久化学历提额额度
     * @param degree
     * @param token
     */
    public void getEducationInfoScore(Map<String,RmBaseCreditType> map, String degree, String token){

        //学历区间权重
        BigDecimal educationInfoCredit = new BigDecimal("0");
        //学历权重
        BigDecimal educationLong = new BigDecimal("0");

        List<RmBaseCreditUpmoney> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("educationScore")){
                rbcilist = ent.getValue().getRbculist();
                educationLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditUpmoney rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }

        if (degree != null){
            if ("专科".equals(degree)){//专科
                educationInfoCredit = creditmap.get("firstEduWeight");
            }else if ("本科".equals(degree)){//本科
                educationInfoCredit = creditmap.get("secondEduWeight");
            }else if ("硕士研究生".equals(degree) || "博士研究生".equals(degree)){//硕士及以上
                educationInfoCredit = creditmap.get("otherEduWeight");
            }

        }

        educationInfoCredit = ValidateUtil.isEmpty(educationInfoCredit) ? new BigDecimal("0") : educationInfoCredit;

        HashMap<String, Object> hmap = new HashMap<>();

        hmap.put("value", educationInfoCredit.multiply(educationLong));
        hmap.put("token", token);

        //执行持久化学历提额额度
        tbUpMoneyMapper.updateTwoIncreaseCreditBytoken(hmap);
    }

    /**
     * 持久化淘宝提额额度（京东和淘宝提额规则一样）
     * @param addresscount
     * @param mobilecount
     * @param token
     */
    public void getTaobaoApprove(Map<String,RmBaseCreditType> map, int addresscount, int mobilecount, String token){

        //淘宝抓取地址区间权重
        BigDecimal taobaoAddressCredit = new BigDecimal("0");

        //淘宝抓取电话区间权重
        BigDecimal taobaoMobileCredit = new BigDecimal("0");

        //淘宝抓取地址权重
        BigDecimal tbAddressLong = new BigDecimal("0");

        //淘宝抓取电话权重
        BigDecimal tbMobileLong = new BigDecimal("0");

        List<RmBaseCreditUpmoney> tbAddresslist = new ArrayList<>();
        List<RmBaseCreditUpmoney> tbMobilelist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("tbAddressScore")){
                tbAddresslist = ent.getValue().getRbculist();
                tbAddressLong = new BigDecimal(ent.getValue().getValue());
            }

            if(code.equals("tbMobileScore")){
                tbMobilelist = ent.getValue().getRbculist();
                tbMobileLong = new BigDecimal(ent.getValue().getValue());
            }
        }


        Map<String, BigDecimal> tbAddressmap = new HashMap();

        for(RmBaseCreditUpmoney rm : tbAddresslist){
            tbAddressmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }

        Map<String, BigDecimal> tbMobilemap = new HashMap();

        for(RmBaseCreditUpmoney rm : tbMobilelist){
            tbMobilemap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }


        if (addresscount == 1){//抓取1个地址
            taobaoAddressCredit = tbAddressmap.get("firstAddressWeight");
        }else if (addresscount >= 2){//抓取2个地址
            taobaoAddressCredit = tbAddressmap.get("secondAddressWeight");
        }else if (addresscount == 0){//未抓取地址
            taobaoAddressCredit = tbAddressmap.get("threeAddressWeight");
        }

        taobaoAddressCredit = ValidateUtil.isEmpty(taobaoAddressCredit) ? new BigDecimal("0") : taobaoAddressCredit;


        if (mobilecount == 0){//未抓取其他联系人
            taobaoMobileCredit = tbMobilemap.get("secondContactsWeight");
        }else if (mobilecount >= 1){//抓取其他联系人
            taobaoMobileCredit = tbMobilemap.get("firstContactsWeight");
        }

        taobaoMobileCredit = ValidateUtil.isEmpty(taobaoMobileCredit) ? new BigDecimal("0") : taobaoMobileCredit;


        HashMap<String, Object> hmap = new HashMap<>();

        hmap.put("value", taobaoAddressCredit.multiply(tbAddressLong).add(taobaoMobileCredit.multiply(tbMobileLong)));
        hmap.put("token", token);

        //执行持久化淘宝提额额度
        tbUpMoneyMapper.updateTwoIncreaseCreditBytoken(hmap);

    }

    /**
     * 持久化滴滴提额额度
     * @param sesame
     * @param token
     */
    public void getDidiDacheApprove(Map<String,RmBaseCreditType> map, String sesame, String token){

        //滴滴区间权重
        BigDecimal didiDacheCredit = new BigDecimal("0");
        //滴滴权重
        BigDecimal didiDacheLong = new BigDecimal("0");

        List<RmBaseCreditUpmoney> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("zhimaScore")){
                rbcilist = ent.getValue().getRbculist();
                didiDacheLong = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditUpmoney rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }


        if (sesame != null && !"".equals(sesame))  {
            Integer sesameScore = Integer.parseInt(sesame);

            if (sesameScore <= 550){//近1个月无社保公积金信息，但以前有缴纳信息
                didiDacheCredit = creditmap.get("firstZhimaWeight");
            }else if (sesameScore >= 551 && sesameScore <= 580){//1个月社保公积金信息
                didiDacheCredit = creditmap.get("secondZhimaWeight");
            }else if (sesameScore >= 581 && sesameScore <= 600){//2-3个月社保公积金信息
                didiDacheCredit = creditmap.get("threeZhimaWeight");
            }else if (sesameScore >= 601 && sesameScore <= 620){//4-6个月社保公积金信息
                didiDacheCredit = creditmap.get("fourZhimaWeight");
            }else if (sesameScore >= 621 && sesameScore < 650){//4-6个月社保公积金信息
                didiDacheCredit = creditmap.get("fiveZhimaWeight");
            }else if (sesameScore >= 651){//7个月以上社保公积金信息
                didiDacheCredit = creditmap.get("sixZhimaWeight");
            }
        }else {
            didiDacheCredit = creditmap.get("sevenZhimaWeight");
        }

        didiDacheCredit = ValidateUtil.isEmpty(didiDacheCredit) ? new BigDecimal("0") : didiDacheCredit;


        HashMap<String, Object> hmap = new HashMap<>();

        hmap.put("value", didiDacheCredit.multiply(didiDacheLong));
        hmap.put("token", token);

        //执行持久化滴滴提额额度
        tbUpMoneyMapper.updateTwoIncreaseCreditBytoken(hmap);
    }

    /**
     * 还款额度计算
     * @param score
     * @param credit
     * @param uid
     * @return
     */
    public BigDecimal getPaymentsCredit(Map<String, Object> score, Map<String, Object> credit, String uid, String oid, Map<String,RmBaseCreditType> map){
//        Double paymentsCredit = 0.0;
        HashMap<String, Object> paymentsMap = new HashMap<>();
        paymentsMap.put("uid",uid);
        paymentsMap.put("isSplit", "1");

        FFundendBind fFundendBind = iFFundendBindService.getFFundendBindByOid(oid);


        BigDecimal paymentsCredit = new BigDecimal("0");
        BigDecimal paymentsPayCredit = new BigDecimal("0");

        //查询当前版本
        RmSxtemplateRelationship selectRmSxtemplateRelationship = rmSxtemplateRelationshipService.selectNewVersionByfid(fFundendBind.getFid());


        List<RmBaseCreditUpmoney> rbcilist = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("countPayScore")){
                rbcilist = ent.getValue().getRbculist();
                paymentsCredit = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        Map<String, BigDecimal> creditmap = new HashMap();

        for(RmBaseCreditUpmoney rm : rbcilist){
            creditmap.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }


        List<RmBaseCreditUpmoney> rbcilistpay = new ArrayList<>();
        for(Map.Entry<String,RmBaseCreditType> ent  : map.entrySet()){
            String code = ent.getValue().getCode();
            if(code.equals("allPayScore")){
                rbcilistpay = ent.getValue().getRbculist();
                paymentsPayCredit = new BigDecimal(ent.getValue().getValue());
                break;
            }
        }


        Map<String, BigDecimal> creditmappay = new HashMap();

        for(RmBaseCreditUpmoney rm : rbcilistpay){
            creditmappay.put(rm.getCode(), new BigDecimal(rm.getValue()));
        }


        //分期
        List<TbRepaymentPlan> paymentslist = tbRepaymentPlanMapper.getTbRepaymentPlanlistbyUid(paymentsMap);

        if (paymentslist != null){
            for (TbRepaymentPlan tbRepaymentPlan:paymentslist
                    ) {
                //还款日期
                Date repaymentDate = tbRepaymentPlan.getRepaymentDate();
                //用户实际进行还款的日期
                Date processDate = tbRepaymentPlan.getProcessDate();
                Long between = (processDate.getTime() - repaymentDate.getTime())/86400000;

                BigDecimal paymentsCreditPart = this.getPaymentsCreditPart(score,credit,between,paymentsCredit,creditmap);

                paymentsCredit = paymentsCredit.add(paymentsCreditPart);

            }

            TbCreditGrade tbCreditGrade = new TbCreditGrade();

            tbCreditGrade.setOid(oid);
            tbCreditGrade.setStatus("1");
            tbCreditGrade.setUid(uid);
            tbCreditGrade.setCode(CreditConstant.PAYMENTSCREDITPARTSCORE);
            tbCreditGrade.setName("分期还款");
            tbCreditGrade.setValue(paymentsCredit + "");
            tbCreditGrade.setVersionId(selectRmSxtemplateRelationship.getId());

            tbCreditGradeMapper.insert(tbCreditGrade);
        }

        HashMap<String, Object> singleMap = new HashMap<>();
        singleMap.put("uid",uid);
        singleMap.put("isSplit", "0");

        //不分期
        List<TbRepaymentPlan> singlelist = tbRepaymentPlanMapper.getTbRepaymentPlanlistbyUid(paymentsMap);

        if (singlelist != null){
            Double dd = 0.0;
            for (TbRepaymentPlan tbRepaymentPlan:singlelist
                 ) {
                //还款日期
                Date repaymentDate = tbRepaymentPlan.getRepaymentDate();
                //用户实际进行还款的日期
                Date processDate = tbRepaymentPlan.getProcessDate();
                Long between = (processDate.getTime() - repaymentDate.getTime())/86400000;

                BigDecimal singleCreditPart = new BigDecimal("0");
                if (between == 0){
                    singleCreditPart = creditmappay.get("firstPayWeight")
                    .multiply(paymentsPayCredit);
                }

                paymentsCredit = paymentsCredit .add(singleCreditPart);
                dd = dd + Double.parseDouble(singleCreditPart.toString());

            }

            TbCreditGrade tbCreditGrade = new TbCreditGrade();

            tbCreditGrade.setOid(oid);
            tbCreditGrade.setStatus("1");
            tbCreditGrade.setUid(uid);
            tbCreditGrade.setCode(CreditConstant.SINGLECREDITPARTSCORE);
            tbCreditGrade.setName("一次性还款");
            tbCreditGrade.setValue(dd + "");
            tbCreditGrade.setVersionId(selectRmSxtemplateRelationship.getId());

            tbCreditGradeMapper.insert(tbCreditGrade);

        }



        return paymentsCredit;
    }

    /**
     * 分期提额计算
     * @param score
     * @param credit
     * @param between
     * @return
     */
    public BigDecimal getPaymentsCreditPart(Map<String, Object> score, Map<String, Object> credit, Long between, BigDecimal paymentsScore, Map<String, BigDecimal> creditmap){
//        Double paymentsCredit = 0.0;
        BigDecimal paymentsCredit = new BigDecimal("0");

            if (between == 0){//按时还款/月
                paymentsCredit = creditmap.get("monthPayWeight");
            }else if (between >= 1 && between <= 3){//逾期1-3天还款/月
                paymentsCredit = creditmap.get("firstOverdueWeight");
            }else if (between >= 4 && between <= 7){//逾期4-7天还款/月
                paymentsCredit = creditmap.get("sencondOverdueWeight");
            }

        paymentsCredit = ValidateUtil.isEmpty(paymentsCredit) ? new BigDecimal("0") : paymentsCredit;

        paymentsScore = paymentsCredit.multiply(paymentsScore);

        return paymentsScore;
    }


}
