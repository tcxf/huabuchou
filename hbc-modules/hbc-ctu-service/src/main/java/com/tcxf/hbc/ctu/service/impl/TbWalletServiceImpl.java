package com.tcxf.hbc.ctu.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.ctu.mapper.TbWalletMapper;
import com.tcxf.hbc.ctu.service.ITbWalletService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金钱包表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-08-07
 */
@Service
public class TbWalletServiceImpl extends ServiceImpl<TbWalletMapper, TbWallet> implements ITbWalletService {

}
