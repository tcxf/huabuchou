<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~    Copyright (c) 2018-2025, lengleng All rights reserved.
  ~
  ~ Redistribution and use in source and binary forms, with or without
  ~ modification, are permitted provided that the following conditions are met:
  ~
  ~ Redistributions of source code must retain the above copyright notice,
  ~ this list of conditions and the following disclaimer.
  ~ Redistributions in binary form must reproduce the above copyright
  ~ notice, this list of conditions and the following disclaimer in the
  ~ documentation and/or other materials provided with the distribution.
  ~ Neither the name of the pig4cloud.com developer nor the names of its
  ~ contributors may be used to endorse or promote products derived from
  ~ this software without specific prior written permission.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.tcxf.hbc</groupId>
    <artifactId>hbc-daemon-service</artifactId>
    <version>1.1</version>
    <packaging>jar</packaging>

    <name>hbc-daemon-service</name>
    <description>后台跑批定时任务模块</description>

	<parent>
		<groupId>com.tcxf.hbc</groupId>
		<artifactId>hbc-modules</artifactId>
		<version>1.0-ALPHA</version>
	</parent>
    <properties>
        <elastic-job.version>2.1.5</elastic-job.version>
        <curator.version>2.10.0</curator.version>
    </properties>

    <dependencies>
	   <dependency>
			<groupId>com.tcxf.hbc</groupId>
			<artifactId>hbc-common</artifactId>
			<version>1.1</version>
		</dependency>
        <dependency>
            <groupId>com.github.xjzrc.spring.boot</groupId>
            <artifactId>elastic-job-lite-spring-boot-starter</artifactId>
            <version>2.0.0</version>
            <exclusions>
                <exclusion>
                    <groupId>org.apache.curator</groupId>
                    <artifactId>curator-framework</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.curator</groupId>
                    <artifactId>curator-recipes</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>${curator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>${curator.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.apache.curator</groupId>
                    <artifactId>curator-framework</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
            <exclusions>
                <!--去掉默认的tomcat-jdbc的依赖-->
                <exclusion>
                    <groupId>org.apache.tomcat</groupId>
                    <artifactId>tomcat-jdbc</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <!--HikariCP连接池-->
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>

    </dependencies>

    <build>
        <!--添加利用maven插件构建docker镜像的插件依赖-->
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>0.4.13</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <!-- tag::plugin[] -->
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <executions>
                    <!--设置在执行maven 的install时构建镜像-->
                    <execution>
                        <id>build-image</id>
                        <phase>install</phase>
                        <goals>
                            <goal>build</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <!--安装了docker的主机，并且打开了api remote接口设置-->
                    <dockerHost>http://172.31.11.181:5678</dockerHost>
                    <pushImage>true</pushImage><!--设置上传镜像到私有仓库，需要docker设置指定私有仓库地址-->
                    <registryUrl>172.31.11.181:5000</registryUrl>
                    <imageName>172.31.11.181:5000/${docker.image.prefix}/${project.artifactId}:${project.version}</imageName>
                    <!--镜像的基础版本-->
                    <baseImage>java:openjdk-8-jdk-alpine</baseImage>
                    <!--镜像启动参数-->
                    <entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
                    <resources>
                        <resource>
                            <targetPath>/</targetPath>
                            <directory>${project.build.directory}</directory>
                            <include>${project.build.finalName}.jar</include>
                        </resource>
                    </resources>
                </configuration>
            </plugin>
            <!-- end::plugin[] -->
        </plugins>
    </build>

</project>
