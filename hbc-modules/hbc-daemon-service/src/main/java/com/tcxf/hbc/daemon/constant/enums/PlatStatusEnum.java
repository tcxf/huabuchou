package com.tcxf.hbc.daemon.constant.enums;

/**
 * 平台结算状态 （0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）
 * @author tanyongfang
 */
public enum PlatStatusEnum implements IDictEnum<String> {
    /**0-待确认 */
    UNCONFIRMED("0","待确认"),
    /**1-待结算 */
    FOR_THE_ACCOUNT("1","待结算"),
    /**2-已结算 */
    SETTLED_ACCOUNT("2","已结算"),
    /**3-未发送 */
    UNSENT("3","未发送"),
    /**4-未生成 */
    NOT_GENERATE("4","未生成"),
    /**5-遗漏交易 */
    MISSING_TRANSACTION("5","遗漏交易"),;
    
    private String key;
    private String value;
    PlatStatusEnum(String key, String value){
        this.key = key;
        this.value = value;
    }
    
    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

}
