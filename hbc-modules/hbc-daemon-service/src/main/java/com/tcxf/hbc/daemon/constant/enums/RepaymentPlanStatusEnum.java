package com.tcxf.hbc.daemon.constant.enums;
/**
 * 状态 1-已还 2-未还
 * @author tanyongfang
 *
 */
public enum RepaymentPlanStatusEnum implements IDictEnum<String> {
    PAID("1","已还"),
    UN_PAID("2","未还");
    
    private String key;
    private String value;
    RepaymentPlanStatusEnum(String key, String value){
        this.key = key;
        this.value = value;
    }
    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

}
