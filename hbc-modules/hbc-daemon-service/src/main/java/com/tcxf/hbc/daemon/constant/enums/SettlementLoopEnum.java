package com.tcxf.hbc.daemon.constant.enums;

import java.util.Calendar;
import java.util.Date;

/**
 * 结算周期（1-日 2-周 3-月 4-季 5-半年 6-一年）
 * 
 * @author tanyongfang
 */
public enum SettlementLoopEnum implements IDictEnum<Integer> {
    DAY(1, "日"), WEEK(2, "周"), MONTH(3, "月"), QUARTER(4, "季"), HALF_A_YEAR(5, "半年"), YEAR(6, "一年"),HALF_A_MONTH(7,"半月");

    private Integer key;
    private String value;
    
    SettlementLoopEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public Integer getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return this.value;
    }
    
    public SettlementDate getSettlementDate(Calendar c) {
        Date endDate = null;
        Date startDate = null;
        Date rdate = null;
        switch(this) {
            case DAY:
                // 获取结算的时间点
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.add(Calendar.MILLISECOND, 1);
                c.add(Calendar.DAY_OF_MONTH, -1);
                startDate = c.getTime();
                break;
            case WEEK:
                /** 第二部分，创建周结算单，每周星期1，生成结算周期为周结的商户结算单 **/
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.add(Calendar.MILLISECOND, 1);
                c.add(Calendar.DAY_OF_MONTH, -7);
                startDate = c.getTime();
                break;
            case HALF_A_MONTH:
                /** 生成结算周期为半月结的商户结算单 **/
                if(c.get(Calendar.DAY_OF_MONTH)==15)
                {
                    c.set(Calendar.DAY_OF_MONTH, 1);
                }
                else if(c.get(Calendar.DAY_OF_MONTH)==c.getActualMaximum(Calendar.DAY_OF_MONTH))
                {
                    c.set(Calendar.DAY_OF_MONTH, 16);
                }
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                startDate = c.getTime();
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 59);
                c.set(Calendar.SECOND, 59);
                c.set(Calendar.MILLISECOND, 59);
                Calendar c2 = Calendar.getInstance();

                if(c2.get(Calendar.DAY_OF_MONTH)==15)
                {
                    c2.set(Calendar.DAY_OF_MONTH, 15);
                }
                else if(c2.get(Calendar.DAY_OF_MONTH)==c.getActualMaximum(Calendar.DAY_OF_MONTH))
                {
                    c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
                }

                endDate = c2.getTime();

                break;
            case MONTH:
                /** 第三部分，创建月结算单，每月1号，生成结算周期为月结的商户结算单 **/
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.DAY_OF_MONTH, 1);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                startDate = c.getTime();
                break;
            case QUARTER:
                /** 第四部分，创建季度结算单，每4/7/10/1 月的1号，生成结算周期为季度结算的商户结算单 **/
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.DAY_OF_MONTH, 1);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.add(Calendar.MONTH, -2);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                startDate = c.getTime();
                break;
            case HALF_A_YEAR:
                /** 第五部分，创建半年结算单，每7/1 月的1号，生成结算周期半年度结算的商户结算单 **/
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.DAY_OF_MONTH, 1);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.add(Calendar.MONTH, -5);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                startDate = c.getTime();
                break;
            case YEAR:
                /** 第六部分，创建年度结算单，每年1 月的1号，生成结算周期年度结算的商户结算单 **/
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.DAY_OF_MONTH, 1);
                rdate = c.getTime();
                c.add(Calendar.MILLISECOND, -1);
                endDate = c.getTime();
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.add(Calendar.MONTH, -11);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                startDate = c.getTime();
                break;
            default:
                break;
        }
        return new SettlementDate(startDate, endDate, rdate);
    }
    
    public static class SettlementDate{
        private Date startDate = null;
        private Date endDate = null;
        private Date rdate = null;
        public SettlementDate(Date startDate, Date endDate, Date rdate) {
            super();
            this.startDate = startDate;
            this.endDate = endDate;
            this.rdate = rdate;
        }
        public Date getEndDate() {
            return endDate;
        }
        public Date getStartDate() {
            return startDate;
        }
        public Date getRdate() {
            return rdate;
        }
    }
    
}
