package com.tcxf.hbc.daemon.constant.enums;

/**
 * 结算状态 （0-待确认 1-待结算 2-已结算）
 * @author tanyongfang
 */
public enum SettlementStatusEnum implements IDictEnum<String> {
    /**0-待确认 */
    UNCONFIRMED("0","待确认"),
    /**1-待结算 */
    FOR_THE_ACCOUNT("1","待结算"),
    /**2-已结算 */
    SETTLED_ACCOUNT("2","已结算");
    
    private String key;
    private String value;
    SettlementStatusEnum(String key, String value){
        this.key = key;
        this.value = value;
    }
    
    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

}
