package com.tcxf.hbc.daemon.constant.enums;

/**
 * 交易类型（0-授信消费 1-非授信消费）
 * 
 * @author tanyongfang
 *
 */
public enum TradingTypeEnum implements IDictEnum<String> {
    /**0 授信消费*/
    CREDIT("0", "授信消费"),
    /**1 非授信消费*/
    NON_CREDIT("1", "非授信消费");

    private String key;
    private String value;

    TradingTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

}
