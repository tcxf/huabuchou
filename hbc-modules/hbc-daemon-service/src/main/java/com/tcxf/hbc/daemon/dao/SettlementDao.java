package com.tcxf.hbc.daemon.dao;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.google.common.collect.Maps;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.daemon.entity.SettlementInfo;
import com.tcxf.hbc.daemon.model.dto.RepaymentDto;
import com.tcxf.hbc.daemon.model.dto.RepaymentSplitDto;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import com.tcxf.hbc.daemon.model.dto.SettlementTestDto;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 测试结算dao
 */
@Repository
public class SettlementDao {
    private static final Logger logger = LoggerFactory.getLogger(SettlementDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询运营商
     * @return
     */
    public OOperaInfo queryAllStatistiacal()
    {
        String sql ="select * from o_opera_info o where o.id=?";
        return jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<>(OOperaInfo.class));
    }

    /**
     * 查询运营商相关数据
     * @param oid
     * @return
     */
    public List<RmCreditmonthStatistical> queryAllCreditMonthStatistical(String oid)
    {
        String sql = "select * from rm_creditmonth_statistical where oid=?";
        return jdbcTemplate.query(sql,new Object[]{oid},new BeanPropertyRowMapper<>(RmCreditmonthStatistical.class));
    }

    /**
     * 查询授信通过率
     * @return
     */
    public List<RmCreditmonthStatistical> queryCreditStatistical()
    {
        List<RmCreditmonthStatistical> list = new ArrayList<RmCreditmonthStatistical>();
        String queryOid = "select w.repay_date as status,f.oid,f.fid from o_opera_info a,o_opera_other_info w,f_fundend_bind f where a.id = w.oid and a.status=1 and f.oid = w.oid";
        List<FFundendBind> olist=jdbcTemplate.query(queryOid,new Object[]{},new BeanPropertyRowMapper<>(FFundendBind.class));
        for (int i = 0; i <olist.size() ; i++) {
            String queryCurrentData = "SELECT * from rm_creditmonth_statistical r where oid=? and date_format(r.create_time, '%Y-%m') = date_format(now(), '%Y-%m') limit 1";
            List<RmCreditmonthStatistical> listRM= jdbcTemplate.query(queryCurrentData,new Object[]{olist.get(i).getOid()},new BeanPropertyRowMapper<>(RmCreditmonthStatistical.class));
            if(listRM!=null && listRM.size()>0)
            {
                continue;
            }
            String sql = "SELECT" +
                    " c.oid," +
                    " c.fid," +
                    " SUM(c.auth_max) as sx_money," +
                    " count(*) AS sx_people," +
                    " (SUM(c.auth_max) / count(*)) AS avg_sx_money," +
                    " if(sum(c.auth_status)=0,0,sum(if(c.auth_status=1,1,0))/sum(c.auth_status)*100)  as pass_rate," +
                    " sum(if(c.residue_Money!=c.auth_max,1,0)) as using_money_rate," +
                    "date_format(c.auth_date, '%Y-%m') as month" +
//                    " IF(count(*)=0,0,SUM(if(c.auth_status=2,1,0))/count(*)*100) as pass_rate" +
                    " FROM" +
                    " c_user_ref_con c" +
                    " where c.auth_status in (1,2,3)" +
                    " and " +
                    " date_format(c.auth_date, '%Y %m') = date_format(DATE_SUB(curdate(), INTERVAL 1 MONTH),'%Y %m')" +
                    " and oid = ?" +
                    " GROUP BY c.oid limit 1";
             List<RmCreditmonthStatistical> rm =jdbcTemplate.query(sql,new Object[]{olist.get(i).getOid()},new BeanPropertyRowMapper<>(RmCreditmonthStatistical.class));
            list.addAll(rm);
        }
        return list;

    }
    /**
     * 查询违约率、不良率、坏账率
     * @param a
     * @param oid
     * @param date
     * @param queryAll
     * @return
     */
    public TbRepaymentDetail queryMoneyRepay(int a,String oid,Date date,boolean queryAll)
    {
        BigDecimal bd= null;
        StringBuffer str = new StringBuffer();
        TbRepaymentDetail td = null;
        String queryParam = "select count(s.id) as id from (" +
                " SELECT d.id from tb_repayment_detail d,tb_repayment_plan p where " +
                " p.rd_id=d.id and d.oid=? " +
                " and p.process_date IS NULL ";
        str.append(queryParam);
        if(queryAll)
        {
            str.append(" and p.status=1");
        }
        else
        {
            str.append(" and p.status=2");
        }

        if(a==1)
        {
            str.append(" and DATEDIFF(NOW(), ?) <=30  GROUP BY d.id ) s");
            td =jdbcTemplate.queryForObject(str.toString(),new Object[]{oid,date},new BeanPropertyRowMapper<>(TbRepaymentDetail.class));
        }
        else if(a==2)
        {
            str.append(" and DATEDIFF(NOW(), ?)>30 and DATEDIFF(repayment_date, ?)<=90  GROUP BY d.id ) s");
            td =jdbcTemplate.queryForObject(str.toString(),new Object[]{oid,date,date},new BeanPropertyRowMapper<>(TbRepaymentDetail.class));
        }
        else
        {
            str.append(" and DATEDIFF(NOW(), ?) >90  GROUP BY d.id ) s");
            td =jdbcTemplate.queryForObject(str.toString(),new Object[]{oid,date},new BeanPropertyRowMapper<>(TbRepaymentDetail.class));
        }
        return td;
    }

    /**
     * 获得违约率、不良率、坏账率
     * @param a
     * @param oid
     * @param date
     * @return
     */
    public BigDecimal queryAllRepayCount(int a,String oid,Date date)
    {
         TbRepaymentDetail tdT = this.queryMoneyRepay(a,oid,date,true);
         TbRepaymentDetail tdF = this.queryMoneyRepay(a,oid,date,false);

         BigDecimal bdt = new BigDecimal(tdT.getId());
         BigDecimal bdf = new BigDecimal(tdF.getId());
         BigDecimal total = Calculate.add(bdt,bdf).getAmount();
         if(Calculate.equalsZero(total))
         {
             return total;
         }
         else
         {
             return Calculate.division(bdt,total).getAmount();
         }


    }

    /**
     * 查询不良率统计
     * @return
     */
    public List<RmRepaymentSituation> queryMonthStatistical()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        List<RmRepaymentSituation> list = new ArrayList<RmRepaymentSituation>();
        String queryOid = "select w.repay_date as status,f.oid,f.fid from o_opera_info a,o_opera_other_info w,f_fundend_bind f where a.id = w.oid and a.status=1 and f.oid = w.oid";
        List<FFundendBind> olist=jdbcTemplate.query(queryOid,new Object[]{},new BeanPropertyRowMapper<>(FFundendBind.class));
        for (int i = 0; i <olist.size() ; i++) {
            String queryMonthData="SELECT * from rm_repayment_situation r where  date_format(r.create_time, '%Y-%m') = date_format(now(), '%Y-%m') and oid=? limit 1";
            List<RmRepaymentSituation> listR= jdbcTemplate.query(queryMonthData,new Object[]{olist.get(i).getOid()},new BeanPropertyRowMapper<>(RmRepaymentSituation.class));
            if(listR!=null && listR.size()>0)
            {
                continue;
            }
            Calendar c= Calendar.getInstance();
            int day = c.get(Calendar.DATE);
            if (day!=Integer.parseInt(olist.get(i).getStatus()))
            {
                continue;
            }
            c.add(Calendar.MONTH, -1);
            c.add(Calendar.DAY_OF_MONTH,Integer.parseInt(olist.get(i).getStatus()));
            BigDecimal break_contract_rate =  this.queryAllRepayCount(1,olist.get(i).getOid(), c.getTime());
            BigDecimal unhealthy_rate = this.queryAllRepayCount(2,olist.get(i).getOid(), c.getTime());
            BigDecimal bad_debt_rate = this.queryAllRepayCount(3,olist.get(i).getOid(), c.getTime());
            RmRepaymentSituation rm = new RmRepaymentSituation();
            rm.setFid(olist.get(i).getFid());
            rm.setOid(olist.get(i).getOid());
            rm.setRepaymentTime(c.getTime());
            rm.setBreakContractRate(break_contract_rate+"");
            rm.setUnhealthyRate(unhealthy_rate+"");
            rm.setBadDebtRate(bad_debt_rate+"");
            list.add(rm);
        }
        return list;
    }
    /**
     * 新增通过率
     * @param list
     * @return
     */
    public int[] addRmCreditmonthStatistical(List<RmCreditmonthStatistical> list)
    {
        List<Object[]> params = new ArrayList<>();
        String sql ="insert into rm_creditmonth_statistical(id,oid,fid,month,sx_people,sx_money,avg_sx_money,pass_rate,using_money_rate,create_time)" +
                "values(?,?,?,?,?,?,?,?,?,?)";
        for (int i = 0; i <list.size() ; i++) {
            params.add(new Object[]{IdWorker.getIdStr(),list.get(i).getOid(),list.get(i).getFid(),list.get(i).getMonth(),
            list.get(i).getSxPeople(),list.get(i).getSxMoney(),list.get(i).getAvgSxMoney(),list.get(i).getPassRate(),list.get(i).getUsingMoneyRate(),new Date()});
        }
       return jdbcTemplate.batchUpdate(sql,params);
    }


    /**
     * 新增违约率
     * @param list
     * @return
     */
    public int[] addRmRepaymentSituation(List<RmRepaymentSituation> list)
    {
        List<Object[]> params = new ArrayList<>();
        String sql ="insert into rm_repayment_situation(id,oid,fid,break_contract_rate,unhealthy_rate,bad_debt_rate,repayment_time,create_time)" +
                "values(?,?,?,?,?,?,?,?) ";
        for (int i = 0; i <list.size() ; i++) {
            params.add(new Object[]{IdWorker.getIdStr(),list.get(i).getOid(),list.get(i).getFid(),
            list.get(i).getBreakContractRate(),list.get(i).getUnhealthyRate(),list.get(i).getBadDebtRate(),
            list.get(i).getRepaymentTime(),new Date()});
        }
        return jdbcTemplate.batchUpdate(sql,params);
    }

    /**
     * 查询还款统计
     * @return
     */
    public List<TbRepayStatistical> queryRepayStatistical()
    {
        List<TbRepayStatistical> resultList = new ArrayList<TbRepayStatistical>();
        String queryAllFund = "select * from f_fundend_bind ";
        List<FFundendBind> fundList = jdbcTemplate.query(queryAllFund,new Object[]{},new BeanPropertyRowMapper<>(FFundendBind.class));

        String sql = "SELECT" +
                " ? as oid, count(a.id) AS should_pay_people," +
                " count(a.process_date) AS already_pay_people," +
                " DATE(a.repayment_date) as create_time," +
                " IFNULL(" +
                " SUM(" +
                " a.current_corpus + a.current_fee + a.late_fee" +
                " )," +
                " 0" +
                " ) AS should_pay_money," +
                " (SELECT IFNULL(sum(current_corpus+current_fee+late_fee),0) from " +
                " (" +
                " SELECT " +
                " SUM(p.current_corpus)as current_corpus," +
                " SUM(p.current_fee) as current_fee," +
                " SUM(p.late_fee) as late_fee," +
                " DATE(p.process_date) as process_date," +
                " p.repayment_date," +
                " c.oid" +
                " from tb_repayment_plan p ,tb_repayment_detail c where p.status='1' and" +
                "  DATEDIFF(repayment_date, NOW()) = -1 and p.rd_id=c.id and c.oid=?" +
                " ) t )as amount, " +
                "(" +
                " SELECT t2.unusual_number from " +
                "                (" +
                " SELECT " +
                " count(*) as unusual_number" +
                " from tb_repayment_plan p ,tb_repayment_detail c,p_notice pn  where p.status='1' and pn.plan_id=p.id and " +
                " DATEDIFF(repayment_date, NOW()) = -1 and p.rd_id=c.id  and (pn.one_day in(3,4) or pn.three_day in(3,4)) and c.oid=?" +
                "                )" +
                " t2 " +
                "" +
                " ) as unusual_number" +
                " FROM" +
                " tb_repayment_plan a," +
                " tb_repayment_detail c" +
                " WHERE" +
                " DATEDIFF(a.repayment_date, NOW()) = -1  and a.rd_id=c.id and c.oid=?";

        for (int i = 0; i <fundList.size() ; i++) {

            String queryCurrentData = "SELECT" +
                    " f.*" +
                    " FROM" +
                    " f_fundend_bind f," +
                    " tb_repay_statistical t" +
                    " WHERE" +
                    " f. STATUS = 1" +
                    " AND f.oid = t.oid" +
                    " AND date_format(t.create_time, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d') and f.oid=?";
            List<FFundendBind> list = jdbcTemplate.query(queryCurrentData,new Object[]{fundList.get(i).getOid()},new BeanPropertyRowMapper<>(FFundendBind.class));
            if(list==null || list.size()==0)
            {
                List<TbRepayStatistical> listRepay= jdbcTemplate.query(sql,new Object[]{fundList.get(i).getOid(),fundList.get(i).getOid(),fundList.get(i).getOid(),fundList.get(i).getOid()},new BeanPropertyRowMapper<>(TbRepayStatistical.class));
                resultList.addAll(listRepay);
            }

            String queryFData = "SELECT" +
                    " f.*" +
                    " FROM" +
                    " f_fundend_bind f," +
                    " tb_repay_statistical t" +
                    " WHERE" +
                    " f. STATUS = 1" +
                    " AND f.fid = t.fid" +
                    " AND date_format(t.create_time, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d') and f.fid=?";
            List<FFundendBind> listFund = jdbcTemplate.query(queryFData,new Object[]{fundList.get(i).getFid()},new BeanPropertyRowMapper<>(FFundendBind.class));
            if(listFund==null || listFund.size()==0)
            {
                String queryFundData = "SELECT" +
                        " ? as fid, count(a.id) AS should_pay_people," +
                        " count(a.process_date) AS already_pay_people," +
                        " DATE(a.repayment_date) as create_time," +
                        " IFNULL(" +
                        " SUM(" +
                        " a.current_corpus + a.current_fee + a.late_fee" +
                        " )," +
                        " 0" +
                        " ) AS should_pay_money," +
                        " (SELECT IFNULL(sum(current_corpus+current_fee+late_fee),0) from " +
                        " (" +
                        " SELECT " +
                        " SUM(p.current_corpus)as current_corpus," +
                        " SUM(p.current_fee) as current_fee," +
                        " SUM(p.late_fee) as late_fee," +
                        " DATE(p.process_date) as process_date," +
                        " p.repayment_date," +
                        " c.oid" +
                        " from tb_repayment_plan p ,tb_repayment_detail c where p.status='1' and" +
                        "  DATEDIFF(repayment_date, NOW()) = -1 and p.rd_id=c.id and c.fid=?" +
                        " ) t )as amount, " +
                        "(" +
                        " SELECT t2.unusual_number from " +
                        "                (" +
                        " SELECT " +
                        " count(*) as unusual_number" +
                        " from tb_repayment_plan p ,tb_repayment_detail c,p_notice pn  where p.status='1' and pn.plan_id=p.id and " +
                        " DATEDIFF(repayment_date, NOW()) = -1 and p.rd_id=c.id  and (pn.one_day in(3,4) or pn.three_day in(3,4)) and c.fid=?" +
                        "                )" +
                        " t2 " +
                        "" +
                        " ) as unusual_number" +
                        " FROM" +
                        " tb_repayment_plan a," +
                        " tb_repayment_detail c" +
                        " WHERE" +
                        " DATEDIFF(a.repayment_date, NOW()) = -1  and a.rd_id=c.id and c.fid=?";
                List<TbRepayStatistical> listRepay= jdbcTemplate.query(queryFundData,new Object[]{fundList.get(i).getFid(),fundList.get(i).getFid(),fundList.get(i).getFid(),fundList.get(i).getFid()},new BeanPropertyRowMapper<>(TbRepayStatistical.class));
                resultList.addAll(listRepay);
            }



        }

        String sqlList = "select * from tb_repay_statistical t where date_format(t.create_time, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d') and oid is null and fid is null limit 1";
        List listS= jdbcTemplate.query(sqlList,new Object[]{},new BeanPropertyRowMapper<>(TbRepayStatistical.class));

        List<TbRepayStatistical> listRs= new ArrayList<TbRepayStatistical>();
        if(listS==null || listS.size()==0)
        {
            String sqlTwo = "SELECT" +
                    " count(a.id) AS should_pay_people," +
                    " count(a.process_date) AS already_pay_people," +
                    " DATE(a.repayment_date) as create_time," +
                    " IFNULL(" +
                    " SUM(" +
                    " a.current_corpus + a.current_fee + a.late_fee" +
                    " )," +
                    " 0" +
                    " ) AS should_pay_money," +
                    " (SELECT IFNULL(sum(current_corpus+current_fee+late_fee),0) from " +
                    " (" +
                    " SELECT " +
                    " SUM(p.current_corpus)as current_corpus," +
                    " SUM(p.current_fee) as current_fee," +
                    " SUM(p.late_fee) as late_fee," +
                    " DATE(p.process_date) as process_date," +
                    " p.repayment_date," +
                    " c.oid" +
                    " from tb_repayment_plan p ,tb_repayment_detail c where p.status='1' and" +
                    "  DATEDIFF(repayment_date, NOW()) = -1 and p.rd_id=c.id " +
                    " ) t )as amount," +
                    "(" +
                    " SELECT t2.unusual_number from " +
                    "                (" +
                    " SELECT " +
                    " count(*) as unusual_number" +
                    " from tb_repayment_plan p ,tb_repayment_detail c,p_notice pn  where p.status='1' and pn.plan_id=p.id and " +
                    " DATEDIFF(repayment_date, NOW()) > -1 and p.rd_id=c.id  and (pn.one_day in(3,4) or pn.three_day in(3,4)) " +
                    "                )" +
                    " t2 " +
                    "" +
                    " ) as unusual_number" +
                    " FROM" +
                    " tb_repayment_plan a," +
                    " tb_repayment_detail c" +
                    " WHERE" +
                    " DATEDIFF(a.repayment_date, NOW()) = -1  and a.rd_id=c.id ;";


            listRs = jdbcTemplate.query(sqlTwo,new Object[]{},new BeanPropertyRowMapper<>(TbRepayStatistical.class));
        }

        resultList.addAll(listRs);
        return resultList;
    }

    /**
     * 新增还款统计
     * @param list
     * @return
     */
    public int[] addRepayStatistical(List<TbRepayStatistical> list)
    {
        String sql = "insert into tb_repay_statistical(id,oid,fid,should_pay_people,already_pay_people,pay_rate,repay_rate,should_pay_money,actual_repay_money,unusual_number,create_time)" +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i <list.size() ; i++) {
            BigDecimal shouldPayMoney = new BigDecimal(0);
            BigDecimal actualRepayMoney = new BigDecimal(0);
            Float resultPeople = 0f;
            if(list.get(i).getShouldPayMoney()!=null)
            {
                shouldPayMoney=list.get(i).getShouldPayMoney();
            }
            if(list.get(i).getActualRepayMoney()!=null)
            {
                actualRepayMoney=list.get(i).getActualRepayMoney();
            }
            BigDecimal resultMoney = new BigDecimal(0);
            if(!Calculate.equalsZero(actualRepayMoney))
            {
                resultMoney =Calculate.division(shouldPayMoney,actualRepayMoney).getAmount();
            }
            if(list.get(i).getAlreadyPayPeople()!=null  && !list.get(i).getAlreadyPayPeople().equals("0"))
            {
                resultPeople = Float.parseFloat(list.get(i).getShouldPayPeople())*100/Float.parseFloat(list.get(i).getAlreadyPayPeople());
            }

            params.add(new Object[]{IdWorker.getIdStr(),list.get(i).getOid(),list.get(i).getFid(),list.get(i).getShouldPayPeople(),list.get(i).getAlreadyPayPeople()
                    ,resultPeople ,
                    resultMoney,
                    shouldPayMoney,actualRepayMoney,list.get(i).getUnusualNumber(),new Date()});
        }
       return  jdbcTemplate.batchUpdate(sql,params);
    }

    /**
     * 运营商_查询昨天授信记录
     *
     * @return
     */
    public List<CUserRefCon> queryUserAuthMax() {
//        String sql = "select * from c_user_ref_con c where DATEDIFF(auth_date,NOW()) = -1 and c.auth_examine=?";
        String sql = "SELECT  DATE(auth_date) as auth_date,count(up_Money) as uid,SUM(auth_max) AS auth_max,count(*) as id,fid,SUM(auth_examine=1)" +
                " as auth_examine,oid FROM c_user_ref_con  where DATEDIFF(auth_date,NOW()) = -1 GROUP BY DATE(auth_date),oid ORDER BY DATE(auth_date) desc";
        List<CUserRefCon> list = jdbcTemplate.query(sql, new Object[]{}, new BeanPropertyRowMapper<>(CUserRefCon.class));

        String sqlTwo = "SELECT 1 as oid, 1 as fid ,DATE(auth_date) as auth_date,count(up_Money) as uid,SUM(auth_max) AS auth_max,count(*) as id,SUM(auth_examine=1)" +
                " as auth_examine FROM c_user_ref_con  where DATEDIFF(auth_date,NOW()) = -1 GROUP BY DATE(auth_date) ORDER BY DATE(auth_date) desc";
        List<CUserRefCon> listTwo = jdbcTemplate.query(sqlTwo, new Object[]{}, new BeanPropertyRowMapper<>(CUserRefCon.class));

        if (list != null && listTwo != null) {
            list.addAll(listTwo);
        }
        return list;
    }

    /**
     * 平台_查询昨天授信记录
     *
     * @return
     */
    public List<CUserRefCon> queryAuthMax() {
//        String sql = "select * from c_user_ref_con c where DATEDIFF(auth_date,NOW()) = -1 and c.auth_examine=?";
        String queryAlreadyData = "SELECT * from credit_daily where date_format(create_time, '%Y-%m-%d')=date_format(now(),'%Y-%m-%d')";
        List<CUserRefCon> list= null;
        List<CreditDaily> cdList = jdbcTemplate.query(queryAlreadyData,new Object[]{},new BeanPropertyRowMapper<>(CreditDaily.class));
        if(cdList!=null && cdList.size()>0)
        {
            list = null;
        }
        else
        {
            String sql = "SELECT  DATE(auth_date) as auth_date,count(up_Money) as uid,SUM(auth_max) AS auth_max,count(*) as id,fid,SUM(auth_examine=1)" +
                    " as auth_examine,oid FROM c_user_ref_con  where DATEDIFF(auth_date,NOW()) = -1 GROUP BY DATE(auth_date) ORDER BY DATE(auth_date) desc";
            list = jdbcTemplate.query(sql, new Object[]{}, new BeanPropertyRowMapper<>(CUserRefCon.class));
        }
        return list;
    }


    /**
     * 授信日统计
     *
     * @param list
     * @return
     */
    public int[] addCreditDaily(List<CUserRefCon> list) {
        String sql = "insert into credit_daily(id,oid,fid,sx_number_people,upmoney_number_people,percapita_up_money,percapita_sx_money,credit_line,passing_rate,create_time)" +
                " values(?,?,?,?,?,?,?,?,?,?)";
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAuthDate() != null) {
                BigDecimal upMoney = new BigDecimal(0);
                BigDecimal uCount = new BigDecimal(0);
                if(!list.get(i).getUid().equals("0"))
                {
                    uCount=new BigDecimal(list.get(i).getUid());
                }

                BigDecimal resultUpMoney = new BigDecimal(0);

                if(!Calculate.equalsZero(uCount))
                {
                    resultUpMoney=Calculate.division(list.get(i).getUpMoney(), new BigDecimal(Integer.parseInt(list.get(i).getUid()) * 100)).getAmount();
                }
                BigDecimal authMax = new BigDecimal(0);
                BigDecimal authExamine = new BigDecimal(0);
                if(!list.get(i).getId().equals("0"))
                {
                    authMax = Calculate.division(list.get(i).getAuthMax(), new BigDecimal(list.get(i).getAuthExamine())).getAmount();
                    authExamine = Calculate.division(new BigDecimal(list.get(i).getAuthExamine()), new BigDecimal(list.get(i).getId())).getAmount();
                }
                params.add(new Object[]{IdWorker.getIdStr(), list.get(i).getOid(), list.get(i).getFid(), list.get(i).getAuthExamine(), list.get(i).getUid(),resultUpMoney
                        ,authMax,list.get(i).getAuthMax(),authExamine, new Date()
                });
            }
        }
        return jdbcTemplate.batchUpdate(sql, params);
    }

    /**
     * 更新账单状态
     *
     * @param list
     */
    public void updateById(List<TbRepay> list) {
        List<Object[]> uList = new ArrayList<>();

        String updateLog = "update tb_repay_log set status= '2' where repay_id=?";
        String sql = "update tb_repay set status= '2' where id=?";
        for (int i = 0; i < list.size(); i++) {
            uList.add(new Object[]{list.get(i).getId()});
        }
        jdbcTemplate.batchUpdate(sql, uList);
        jdbcTemplate.batchUpdate(updateLog, uList);
    }

    /**
     * 更新失效优惠券状态
     *
     * @param list
     */
    public void updateInvalidPacket(List<MRedPacketSetting> list) {
        List<Object[]> uList = new ArrayList<>();
        String sql = "update m_red_packet_setting set status=3 where id=?";
        for (int i = 0; i < list.size(); i++) {
            uList.add(new Object[]{list.get(i).getId()});
        }
        jdbcTemplate.batchUpdate(sql, uList);

    }

    /**
     * 查询账单状态
     *
     * @return
     */
    public List<TbRepay> findOverFifteenMsRepayBill() {
        String sql = "SELECT * from tb_repay u1 where  u1.create_date < date_format(date_sub(now(),interval '00:15:00' day_second),'%Y-%m-%d %H:%i:%S')" +
                "    AND u1.`status` = '0'";
        List<TbRepay> list = jdbcTemplate.query(sql, new Object[]{}, new BeanPropertyRowMapper(TbRepay.class));
        return list;
    }


    /**
     * 查询已失效的优惠券
     *
     * @return
     */
    public List<MRedPacketSetting> findInvalidPacket(Integer cJob, Integer mJob, Integer startLimit, Integer endLimit) {
        String sql = "select id,t.time_type,date_add(t.create_date, interval t.days day)," +
                "t.end_date from m_red_packet_setting t where t.mi_id mod ? = ? GROUP BY t.mi_id limit ?,?";
        return jdbcTemplate.query(sql, new Object[]{cJob, mJob, startLimit, endLimit}, new BeanPropertyRowMapper(MRedPacketSetting.class));
    }

    public SettlementTestDto findSettlementByTest(Integer settlementLoop) {
        SettlementTestDto std = null;
        String sql = "select * from tb_settlement_init_test t where t.isCheck = 1 and t.settlementLoop = ?";
        Object o = jdbcTemplate.queryForObject(sql, new Object[]{settlementLoop}, new BeanPropertyRowMapper(SettlementTestDto.class));
        if (o != null) {
            std = (SettlementTestDto) o;
        }

        return std;
    }

    /**
     * 查询逾期账单
     *
     * @param cJob
     * @param mJob
     * @return
     */
    public List<TbRepaymentPlan> findRepaymentSplitList(Integer cJob, Integer mJob, Integer startLimit, Integer endLimit) {
        String sql = "SELECT p.uid,((datediff(now(),repayment_date))*p.yq_rate*0.01*p.total_corpus) as late_fee,p.rd_id,p.uid from" +
                " tb_repayment_plan p  where p.uid mod ? = ? and p.status='2' and p.repayment_date<now() and (p.process_date is null)  group by p.rd_id limit ?,?";
        return jdbcTemplate.query(sql, new Object[]{mJob, cJob, startLimit, endLimit}, new BeanPropertyRowMapper(TbRepaymentPlan.class));
    }

    /**
     * 查询所有还款记录产生的滞纳金
     *
     * @return
     */
    public List<RepaymentSplitDto> findRepaymentSplitSum(String id) {
        String sql = "select (sum((datediff(now(),repayment_date))*t.yq_rate*0.01*t.total_corpus)) as totalLateFee" +
                ",t.uid as uid,t.rd_id as rdId,d.oid as oid from tb_repayment_plan t,tb_repayment_detail d" +
                " where t.rd_id=d.id and  t.status='2' and t.rd_id=? and t.repayment_date<now() group by t.rd_id";

        return jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(RepaymentSplitDto.class));
    }

    /**
     * 查询未还款产生的滞纳金
     *
     * @param list
     * @return
     */
    public int[] updateRepaymentSlitSum(List<RepaymentSplitDto> list) {

        List<Object[]> uList = new ArrayList<>();
        List<Object[]> oList = new ArrayList<>();
        List<Object[]> params = new ArrayList<>();
        String sql = "update tb_repayment_detail t set t.total_late_fee = ? where t.id = ? and t.uid = ?";
        for (int i = 0; i < list.size(); i++) {
            params.add(new Object[]{list.get(i).getTotalLateFee(), list.get(i).getRdId(), list.get(i).getUid()});
            oList.add(new Object[]{list.get(i).getUid(), list.get(i).getOid()});
            uList.add(new Object[]{list.get(i).getUid()});
        }

        logger.info("===========授信状态==params==========" + JSON.toJSONString(params));
        logger.info("===========授信状态==oList==========" + JSON.toJSONString(oList));
        logger.info("===========授信状态====uList========" + JSON.toJSONString(uList));
        //3：CUserRefCon 修改授信状态
        CUserRefCon cUserRefCon = new CUserRefCon();
        cUserRefCon.setAuthStatus("2");
        String sqlInvalid = "update c_user_ref_con set auth_status= '2' where uid = ? and oid=?";
        //授信状态（0-未授信 1-已授信 2-已冻结）

        jdbcTemplate.batchUpdate(sqlInvalid, oList);

        //4：tb_account_info 修改授信状态
        TbAccountInfo tbAccountInfo = new TbAccountInfo();
        tbAccountInfo.setIsFreeze("1");
        //授信是否冻结 0:未冻结 1:冻结
        String sqlOk = "update tb_account_info set is_freeze='1' where oid = ?";
        jdbcTemplate.batchUpdate(sqlOk, uList);

        return jdbcTemplate.batchUpdate(sql, params);
    }

    /**
     * 更新逾期账单
     *
     * @param list
     * @return
     */
    public int[] updateRepaymentSlitList(List<TbRepaymentPlan> list) {
        List<Object[]> params = new ArrayList<>();

        String sql = "UPDATE tb_repayment_plan p set p.late_fee = ? where p.uid = ? and p.rd_id=? ";
        for (int i = 0; i < list.size(); i++) {
            params.add(new Object[]{list.get(i).getLateFee(), list.get(i).getUid(), list.get(i).getRdId()});
        }
        return jdbcTemplate.batchUpdate(sql, params);
    }

    /**
     * 逾期冻结授信额度
     *
     * @param list
     */
    public void updateAmountInvalid(List<Object[]> list) {
        //3：CUserRefCon 修改授信状态
        CUserRefCon cUserRefCon = new CUserRefCon();
        cUserRefCon.setAuthStatus("1");
        String sql = "update c_user_ref_con set auth_status= 1 where uid = ? and oid=?";
        //授信状态（0-未授信 1-已授信 2-已冻结）
        jdbcTemplate.batchUpdate(sql, list);

        //4：tb_account_info 修改授信状态
        TbAccountInfo tbAccountInfo = new TbAccountInfo();
        tbAccountInfo.setIsFreeze("0");
        //授信是否冻结 0:未冻结 1:冻结
        String sqlOk = "update tb_account_info set is_freeze=0 where uid = ?";
        jdbcTemplate.batchUpdate(sqlOk, list);
    }

    /**
     * 查询绑定资金端id
     *
     * @param oid
     * @return
     */
    public RepaymentDto queryFidByOid(String oid) {
        String sql = "SELECT fid FROM f_fundend_bind a where a.oid=? and status=1";
        return (RepaymentDto) jdbcTemplate.queryForObject(sql, new Object[]{oid}, new BeanPropertyRowMapper(RepaymentDto.class));
    }

    /**
     * 查询时间段内人员授信支付结算
     *
     * @param beginTime
     * @param endTime
     * @param totalPartNum
     * @param currentPart
     * @return
     */
    public List<SettlementInfo> queryUserSettlementBypartId(String beginTime, String endTime, Integer totalPartNum, Integer currentPart) {
        String sql = "SELECT t2.id,t2.uid, sum(t2.actual_amount) total_amount FROM tb_trading_detail t2 WHERE "
                + "t2.trading_type = '0' AND t2.payment_status = '1' AND t2.trading_date > ? AND t2.trading_date <  ? "
                + " AND t2.uid % ? = ?  GROUP BY t2.uid";

        List<SettlementInfo> list = jdbcTemplate.query(sql,
                new Object[]{beginTime, endTime, totalPartNum, currentPart}, new BeanPropertyRowMapper(SettlementInfo.class));

        if (list != null && list.size() > 0) {
            return list;
        } else {
            return null;
        }
    }

    /**
     * 新增结算记录
     *
     * @param settlementDetail
     * @return
     */
    public int add(TbSettlementDetail settlementDetail) {
        return jdbcTemplate.update("insert into tb_settlement_detail values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                IdWorker.getIdStr(),
                settlementDetail.getMid(),
                settlementDetail.getOid(),
                settlementDetail.getFid(),
                settlementDetail.getSettlementTime(),
                settlementDetail.getStatus(),
                settlementDetail.getAmount(),
                settlementDetail.getShareFee(),
                settlementDetail.getSerialNo(),
                settlementDetail.getOutSettlementDate(),
                settlementDetail.getLoopStart(),
                settlementDetail.getLoopEnd(),
                settlementDetail.getRoleStime(),
                settlementDetail.getPlatformShareFee(),
                settlementDetail.getDiscountFee(),
                settlementDetail.getOperStatus(),
                settlementDetail.getPlatStatus(),
                new Date(),
                settlementDetail.getModifyDate()
        );
    }

    /**
     * 根据商户id生成所有用户分润总金额
     *
     * @param mid
     * @return
     */
    public List<TbUserSharefeeSettlement> createUserSettlement(String mid, String sid, Date startDate, Date endDate) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SUM(ff.total_money) as total_money,ff.uid as uid," + sid + " as sid,ff.uType as uType from (" +
                "SELECT s.inderect_user_id as uid,s.redirect_user_type as uType,SUM(s.inderect_share_amount) as total_money from tb_trading_detail d,tb_trade_profit_share t,tb_three_sale_profit_share s " +
                "where " +
                "d.mid= ? " +
                "and d.utype='1' " +
                "and d.trading_type='0' " +
                "and d.payment_status='1' " +
                "and t.mid= ? " +
                "and d.trading_date>= ? and d.trading_date<= ? " +
                "and d.id = t.trade_id " +
                "and t.three_sale_share_id = s.id GROUP BY s.inderect_user_id " +
                "UNION " +
                "SELECT s.redirect_user_id as uid,s.redirect_user_type as uType,SUM(s.redirect_share_amount) as total_money from tb_trading_detail d,tb_trade_profit_share t,tb_three_sale_profit_share s " +
                "where " +
                "d.mid= ? " +
                "and d.utype='1' " +
                "and d.trading_type='0' " +
                "and d.payment_status='1' " +
                "and t.mid= ? " +
                "and d.trading_date>= ? and d.trading_date<= ? " +
                "and d.id = t.trade_id " +
                "and t.three_sale_share_id = s.id GROUP BY s.redirect_user_id " +
                ") ff GROUP BY ff.uid "
        );
        if (logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        List<TbUserSharefeeSettlement> list = jdbcTemplate.query(sql.toString(), new Object[]{mid, mid, startDate, endDate, mid, mid, startDate, endDate}, new BeanPropertyRowMapper(TbUserSharefeeSettlement.class));
        return list;
    }

    /**
     * 查询总数
     *
     * @param startDate
     * @param endDate
     * @param tradingType
     * @param settlementLoop
     * @param cJob
     * @param mJob
     * @return
     */
    public HashMap<String, Object> queryTradingSettlementCount(Date startDate, Date endDate, String tradingType, Integer settlementLoop, Integer cJob, Integer mJob) {
        Integer totalCount = 0;
        HashMap<String, Object> map = new HashMap<String, Object>();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT count(t.*) as T.mid,T.id as tid");
        sql.append(" FROM m_merchant_info M LEFT JOIN m_merchant_other_info MO ON MO.mid=M.id");
        sql.append(" LEFT JOIN o_opera_info O ON O.id=M.oid LEFT JOIN tb_trading_detail T ON T.mid = M.id");
        sql.append(" LEFT JOIN tb_trade_profit_share TPS ON TPS.trade_id=T.id");
        sql.append(" WHERE T.payment_status='1' AND t.is_settlement=0 AND T.trading_type = ?");
        sql.append(" AND T.trading_date >= ? AND T.trading_date <= ? AND MO.settlement_loop = ? ");
        sql.append("AND mod(M.ID,?)=? ");
        sql.append("AND (T.sid is null or T.sid='') ");
        sql.append(" GROUP BY T.mid");
        sql.append(" ORDER BY T.mid");
        if (logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        logger.info(sql.toString());
        @SuppressWarnings("unchecked")

        SettlementDto sd = (SettlementDto) jdbcTemplate.queryForObject(sql.toString(), new Object[]{tradingType, startDate, endDate, settlementLoop, mJob, cJob}, new BeanPropertyRowMapper(SettlementDto.class));
        if (sd != null) {
            Integer mCount = Integer.parseInt(sd.getMid()) % 100;
            map.put("totalCount", Integer.parseInt(sd.getMid()));
            map.put("totalPage", Math.floor(totalCount / 100) + mCount);
            map.put("lastPageSize", mCount);
        }
        return map;
    }


    /**
     * 查询交易表
     *
     * @param startDate
     * @param endDate
     * @param tradingType
     * @param settlementLoop
     * @return
     */
    public List<SettlementDto> queryTradingSettlement(Date startDate, Date endDate, String tradingType, Integer settlementLoop, Integer cJob, Integer mJob) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT IFNULL(SUM(T.actual_amount),0) AS amount,T.mid,T.oid,T.id as tid");
        sql.append(" ,IFNULL(SUM(TPS.operator_discount_fee+TPS.operator_share_fee),0) AS shareFee,");
        sql.append(" IFNULL(SUM(TPS.operator_discount_fee),0) as operator_discount_fee, ");
        sql.append(" IFNULL(SUM(TPS.operator_share_fee) ,0) as operator_share_fee ,");
        sql.append(" IFNULL(SUM(TPS.opera_share_fee) ,0) as opera_share_fee,");
        sql.append(" IFNULL(SUM(TPS.platfrom_share_fee),0) AS platformShareFee,M.name,MO.settlement_loop AS settlementLoop,O.osn");
        sql.append(" FROM m_merchant_info M LEFT JOIN m_merchant_other_info MO ON MO.mid=M.id");
        sql.append(" LEFT JOIN o_opera_info O ON O.id=M.oid LEFT JOIN tb_trading_detail T ON T.mid = M.id");
        sql.append(" LEFT JOIN tb_trade_profit_share TPS ON TPS.trade_id=T.id");
        sql.append(" WHERE T.payment_status='1' AND t.is_settlement=0 AND T.trading_type = ?");
        sql.append(" AND T.trading_date >= ? AND T.trading_date <= ? AND MO.settlement_loop = ? ");
        sql.append(" AND mod(M.ID,?)=? ");
        sql.append(" AND (T.sid is null or T.sid='') ");
        sql.append(" GROUP BY T.mid");
        sql.append(" ORDER BY T.mid");
        if (logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        logger.info(sql.toString());
        @SuppressWarnings("unchecked")
        List<SettlementDto> list = jdbcTemplate.query(sql.toString(), new Object[]{tradingType, startDate, endDate, settlementLoop, mJob, cJob}, new BeanPropertyRowMapper(SettlementDto.class));
        return list;
    }

    /**
     * 额度结算查询
     * @param tradingType
     * @param settlementLoop
     * @param cJob
     * @param mJob
     * @param startLimit
     * @param endLimit
     * @param rdate
     * @return
     */
    public List<SettlementDto> queryTradingCreditQuotaSettlementPage(Date startDate, Date endDate,String tradingType,Integer settlementLoop,Integer cJob,Integer mJob,Integer startLimit,Integer endLimit,Date rdate){
        StringBuilder sql = new StringBuilder();
        sql.append("select cpo.*,? as rdate,? as startDate, ? as endDate, ? as settlementLoop,? as cJob,? as mJob from ( SELECT settlementStatus," +
                " IFNULL(sum(tt.amount),0) as amount, IFNULL(SUM(operator_discount_fee),0) as operatorDiscountFee, " +
                " tt.mid,tt.oid,GROUP_CONCAT(tt.tid) as tid, IFNULL(sum(tt.shareFee),0) as shareFee ,IFNULL(SUM(opera_share_fee),0) as operaShareFee," +
                " IFNULL(SUM(tt.operator_share_fee),0) as operatorShareFee,IFNULL(SUM(tt.platformShareFee),0) as platformShareFee,tt.name as name," +
                " tt.settlementLoop as settlementLoop,tt.osn  as osn" +
                " from " +
                " (SELECT" +
                " T.actual_amount AS amount,O.settlement_status as settlementStatus," +
                " T.mid," +
                " T.oid," +
                " T.id AS tid," +
                " TPS.operator_discount_fee " +
                " AS shareFee," +
                " TPS.operator_discount_fee" +
                " AS operator_discount_fee," +
                " TPS.operator_share_fee" +
                " AS operator_share_fee," +
                " TPS.opera_share_fee AS opera_share_fee," +
                " TPS.platfrom_share_fee" +
                " AS platformShareFee," +
                " M. NAME," +
                " MO.settlement_loop AS settlementLoop," +
                " O.osn     " +
                " FROM" +
                " m_merchant_info M" +
                " LEFT JOIN m_merchant_other_info MO ON MO.mid = M.id        " +
                " LEFT JOIN o_opera_info O ON O.id = M.oid" +
                " LEFT JOIN tb_trading_detail T ON T.mid = M.id        " +
                " LEFT JOIN tb_trade_profit_share TPS ON TPS.trade_id = T.id      " +
                " WHERE" +
                " T.payment_status = '1'" +
                " AND t.is_settlement = 0" +
                " AND T.trading_type = ?     " +
//                " AND T.trading_date >= ? " +
//                " AND T.trading_date <= ? " +
                " AND MO.settlement_loop = ? " +
                " AND mod(M.ID,?)=? "+
                " AND (T.sid IS NULL OR T.sid = '') " +
                " ORDER BY" +
                " T.mid" +
                " ) tt  GROUP BY " +
                " tt.mid");
        sql.append(" limit ?,? ) as cpo,m_merchant_other_info as mco where cpo.mid=mco.mid and  cpo.amount >= mco.minmoney");
        if(logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        logger.error(sql.toString());
        logger.error("settlementParams:"+rdate+ +settlementLoop+" "+cJob+" "+mJob+" "+startLimit+" "+endLimit);
        @SuppressWarnings("unchecked")
        List<SettlementDto> list = jdbcTemplate.query(sql.toString(),new Object[]{
                rdate,startDate,endDate,settlementLoop,cJob,mJob,
                tradingType,settlementLoop,mJob,cJob,startLimit,endLimit
        }, new BeanPropertyRowMapper(SettlementDto.class));
        return list;
    }

    public List<SettlementDto> queryTradingSettlementParamPage(String latestday,Date startDate, Date endDate,String tradingType,Integer settlementLoop,Integer cJob,Integer mJob,Integer startLimit,Integer endLimit,Date rdate){
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ? as rdate,? as startDate, ? as endDate, ? as settlementLoop,? as cJob,? as mJob,settlementStatus," +
                " IFNULL(sum(tt.amount),0) as amount, IFNULL(SUM(operator_discount_fee),0) as operatorDiscountFee, " +
                " tt.mid,tt.oid,GROUP_CONCAT(tt.tid) as tid, IFNULL(sum(tt.shareFee),0) as shareFee ,IFNULL(SUM(opera_share_fee),0) as operaShareFee," +
                " IFNULL(SUM(tt.operator_share_fee),0) as operatorShareFee,IFNULL(SUM(tt.platformShareFee),0) as platformShareFee,tt.name as name," +
                " tt.settlementLoop as settlementLoop,tt.osn  as osn" +
                " from " +
                " (SELECT" +
                " T.actual_amount AS amount,O.settlement_status as settlementStatus," +
                " T.mid," +
                " T.oid," +
                " T.id AS tid," +
                " TPS.operator_discount_fee " +
                " AS shareFee," +
                " TPS.operator_discount_fee" +
                " AS operator_discount_fee," +
                " TPS.operator_share_fee" +
                " AS operator_share_fee," +
                " TPS.opera_share_fee AS opera_share_fee," +
                " TPS.platfrom_share_fee" +
                " AS platformShareFee," +
                " M. NAME," +
                " MO.settlement_loop AS settlementLoop," +
                " O.osn     " +
                " FROM" +
                " m_merchant_info M" +
                " LEFT JOIN m_merchant_other_info MO ON MO.mid = M.id        " +
                " LEFT JOIN o_opera_info O ON O.id = M.oid" +
                " LEFT JOIN tb_trading_detail T ON T.mid = M.id        " +
                " LEFT JOIN tb_trade_profit_share TPS ON TPS.trade_id = T.id      " +
                " WHERE" +
                " T.payment_status = '1'" +
                " AND t.is_settlement = 0" +
                " AND T.trading_type = ?     " +
                " AND T.trading_date >= ? " +
                " AND T.trading_date <= ? " +
                " AND MO.settlement_loop = ? " +
                " AND mod(M.ID,?)=? AND MO.latestday=?"+
                " AND (T.sid IS NULL OR T.sid = '') " +
                " ORDER BY" +
                " T.mid" +
                ") tt  GROUP BY " +
                " tt.mid");
        sql.append(" limit ?,? ");
        if(logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        logger.error(sql.toString());
        logger.error("settlementParams:"+rdate+" "+startDate+" "+endDate+" "+settlementLoop+" "+cJob+" "+mJob+" "+latestday+" "+startLimit+" "+endLimit);
        @SuppressWarnings("unchecked")
        List<SettlementDto> list = jdbcTemplate.query(sql.toString(),new Object[]{rdate,startDate,endDate,settlementLoop,cJob,mJob,tradingType,startDate,endDate,settlementLoop,mJob,cJob,latestday,startLimit,endLimit}, new BeanPropertyRowMapper(SettlementDto.class));
        return list;
    }

    public List<SettlementDto> queryTradingSettlementPage(Date startDate, Date endDate, String tradingType, Integer settlementLoop, Integer cJob, Integer mJob, Integer startLimit, Integer endLimit, Date rdate) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ? as rdate,? as startDate, ? as endDate, ? as settlementLoop,? as cJob,? as mJob,settlementStatus," +
                " IFNULL(sum(tt.amount),0) as amount, IFNULL(SUM(operator_discount_fee),0) as operatorDiscountFee, " +
                " tt.mid,tt.oid,GROUP_CONCAT(tt.tid) as tid, IFNULL(sum(tt.shareFee),0) as shareFee ,IFNULL(SUM(opera_share_fee),0) as operaShareFee," +
                " IFNULL(SUM(tt.operator_share_fee),0) as operatorShareFee,IFNULL(SUM(tt.platformShareFee),0) as platformShareFee,tt.name as name," +
                " tt.settlementLoop as settlementLoop,tt.osn  as osn" +
                " from " +
                " (SELECT" +
                " T.actual_amount AS amount,O.settlement_status as settlementStatus," +
                " T.mid," +
                " T.oid," +
                " T.id AS tid," +
                " TPS.operator_discount_fee " +
                " AS shareFee," +
                " TPS.operator_discount_fee" +
                " AS operator_discount_fee," +
                " TPS.operator_share_fee" +
                " AS operator_share_fee," +
                " TPS.opera_share_fee AS opera_share_fee," +
                " TPS.platfrom_share_fee" +
                " AS platformShareFee," +
                " M. NAME," +
                " MO.settlement_loop AS settlementLoop," +
                " O.osn     " +
                " FROM" +
                " m_merchant_info M" +
                " LEFT JOIN m_merchant_other_info MO ON MO.mid = M.id        " +
                " LEFT JOIN o_opera_info O ON O.id = M.oid" +
                " LEFT JOIN tb_trading_detail T ON T.mid = M.id        " +
                " LEFT JOIN tb_trade_profit_share TPS ON TPS.trade_id = T.id      " +
                " WHERE" +
                " T.payment_status = '1'" +
                " AND t.is_settlement = 0" +
                " AND T.trading_type = ?     " +
                " AND T.trading_date >= ? " +
                " AND T.trading_date <= ? " +
                " AND MO.settlement_loop = ? " +
                " AND mod(M.ID,?)=? " +
                " AND (T.sid IS NULL OR T.sid = '') " +
                " ORDER BY" +
                " T.mid" +
                ") tt  GROUP BY " +
                " tt.mid");
        sql.append(" limit ?,? ");
        if (logger.isDebugEnabled()) {
            logger.debug(sql.toString());
        }
        logger.error(sql.toString());
        logger.error("settlementParams:" + rdate + " " + startDate + " " + endDate + " " + settlementLoop + " " + cJob + " " + mJob + " " + startLimit + " " + endLimit);
        @SuppressWarnings("unchecked")
        List<SettlementDto> list = jdbcTemplate.query(sql.toString(), new Object[]{rdate, startDate, endDate, settlementLoop, cJob, mJob, tradingType, startDate, endDate, settlementLoop, mJob, cJob, startLimit, endLimit}, new BeanPropertyRowMapper(SettlementDto.class));
        return list;
    }

    /**
     * 根据条件 批量更新交易记录表
     *
     * @param sid
     * @param tid
     * @return
     */
    public int[] updateSidByDateAndMid(String sid, String tid) {
        String tempId[] = tid.split(",");
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i < tempId.length; i++) {
            params.add(new Object[]{sid, tempId[i]});
        }
        String sql = "UPDATE tb_trading_detail SET sid=?,is_settlement=0 WHERE id=?";
        return jdbcTemplate.batchUpdate(sql, params);
    }

    /**
     * 批量新增用户结算记录
     *
     * @param list
     * @return
     */
    public int[] batchSaveUserSharefeeDetail(List<TbUserSharefeeSettlement> list, String settlementId) {
        StringBuffer sql = new StringBuffer();
        sql.append("insert into tb_user_sharefee_settlement(id,uid,settlement_id,total_money,is_settlement)values(");
        sql.append("?,?,?,?,?)");
        List<Object[]> params = new ArrayList<>();
        for (TbUserSharefeeSettlement s : list) {
            if (s.getuType().equals("1")) {
                String getUserSql = "SELECT * from tb_user_account_type_con where user_type=1 and  " +
                        "acct_id =( select acct_id from tb_user_account_type_con where  con_id=?)";
                TbUserAccountTypeCon tat = (TbUserAccountTypeCon) jdbcTemplate.queryForObject(getUserSql, new Object[]{s.getUid()}, new BeanPropertyRowMapper(TbUserAccountTypeCon.class));
                params.add(new Object[]{IdWorker.getIdStr(), tat.getConId(), settlementId, s.getTotalMoney(), 0});
            } else {
                params.add(new Object[]{IdWorker.getIdStr(), s.getUid(), settlementId, s.getTotalMoney(), 0});
            }

        }
        return jdbcTemplate.batchUpdate(sql.toString(), params);
    }

    /**
     * 更新钱包记录表
     */
    public void batchUpdateWalletIncome(List<TbSettlementDetail> dlist) {
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i < dlist.size(); i++) {
            String tempIdList[] = dlist.get(i).getTid().split(",");
            for (int j = 0; j < tempIdList.length; j++) {
                StringBuffer sql = new StringBuffer();
                sql.append("update tb_wallet_income t set t.settlement_id = ? , t.settlement_status = ? where tid = ? ");
                params.add(new Object[]{dlist.get(i).getId(), 0, tempIdList[j]});
                jdbcTemplate.batchUpdate(sql.toString(), params);
            }
        }
    }

    /**
     * 批量新增结算记录
     *
     * @param settlementDetailList
     * @return
     */
    public int[] batchSaveTbSettlementDetail(List<TbSettlementDetail> settlementDetailList) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into tb_settlement_detail(id,mid,oid,fid,settlement_time,status,amount,share_fee,serial_no"
                + ",out_settlement_date,loop_start,loop_end,role_stime,platform_share_fee"
                + ",discount_fee,oper_status,plat_status,create_date,modify_date,opera_share_fee) values");
        sql.append(" (?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?)");
        List<Object[]> params = new ArrayList<>();
        for (TbSettlementDetail sd : settlementDetailList) {
            params.add(new Object[]{sd.getId(), sd.getMid(), sd.getOid(), sd.getFid(), sd.getSettlementTime()
                    , sd.getStatus(), sd.getAmount(), sd.getShareFee(), sd.getSerialNo()
                    , sd.getOutSettlementDate(), sd.getLoopStart(), sd.getLoopEnd()
                    , sd.getRoleStime(), sd.getPlatformShareFee(), sd.getDiscountFee()
                    , sd.getOperStatus(), sd.getPlatStatus(), sd.getCreateDate(), sd.getModifyDate(), sd.getOperaShareFee()});
        }
        return jdbcTemplate.batchUpdate(sql.toString(), params);
    }

    /**
     * 查询商户对应的资金端ID
     *
     * @param mid 商户ID列表
     * @return mid, fid
     */
    public String queryMerchantFidList(String mid) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();


        Map<String, String> map = new HashMap<String, String>();
        StringBuilder sql = new StringBuilder();
        sql.append("select * from m_merchant_info where id=?");
        MMerchantInfo m = jdbcTemplate.queryForObject(sql.toString(), new Object[]{mid}, new BeanPropertyRowMapper<>(MMerchantInfo.class));
        String queryFid = "select * from f_fundend_bind where oid = ?";
        FFundendBind ffb = jdbcTemplate.queryForObject(queryFid, new Object[]{m.getOid()}, new BeanPropertyRowMapper<>(FFundendBind.class));
        return ffb.getFid();

//        StringBuilder sql = new StringBuilder();
//        sql.append("SELECT mid,fid FROM c_user_ref_con WHERE mid IN(");
//        for (String mid : mids) {
//            sql.append("?,");
//        }4
//        sql.append(")");
//        sql = sql.deleteCharAt(sql.length() - 2);
//
//        Object[] array = mids.toArray(new Object[] {});
//        return jdbcTemplate.query(sql.toString(), array, new RowMapper<Map<String,String>>() {
//            @Override
//            public Map<String,String> mapRow(ResultSet rs, int rowNum) throws SQLException {
//                Map<String,String> map = Maps.newHashMap();
//                map.put("mid",rs.getString("mid"));
//                map.put("fid",rs.getString("fid"));
//                return map;
//            }
//        });
    }

    /**
     * 查询商户上个月交易记录
     *
     * @param startDate
     * @param endDate
     * @param day
     * @param cJob
     * @return
     */
    public List<RepaymentDto> findOperaRepayment(Date startDate, Date endDate, Integer day, Integer cJob) {
        // 上月消费总额统计
        String t_sql = " SELECT t.mid, t.oid, " +
                " SUM( t.actual_amount ) AS amount,m.ooa_date as ooaDate," +
                "m.repay_date as repayDate" +
                " FROM " +
                " tb_trading_detail AS t " +
                " ,m_merchant_info AS urc," +
                " m_merchant_other_info m " +
                " WHERE " +
                " urc.id = m.mid " +
                " AND urc.id=t.mid " +
                " AND urc.oid=t.oid " +
                " AND (t.rdid is null or t.rdid='' ) " +
                " AND t.mid mod 10 = ?                  " +
                " AND t.utype= '2' AND m.ooa_date = ? " +
                " AND t.trading_date>= ? " +
                " AND t.trading_date<=? " +
                " GROUP BY t.mid, t.oid ORDER BY t.mid, t.oid";
        List<RepaymentDto> list = jdbcTemplate.query(t_sql, new Object[]{cJob, day, startDate, endDate}, new BeanPropertyRowMapper(RepaymentDto.class));
        return list;
    }

    public List<RepaymentDto> findUnRepaymentPage(Date startDate, Date endDate,
                                                  Integer day, Integer cJob, Integer mJob) {
        // 上月消费总额统计
        String t_sql = "select tt.startDate,tt.endDate,tt.cJob,tt.mJob,tt.uid,tt.oid, IFNULL(sum(tt.amount),0) as amount,tt.fid  from ( " +
                " SELECT ? as startDate,? as endDate ,? as cJob,? as mJob,t.uid as uid, t.oid as oid, t.actual_amount AS amount,urc.fid as fid FROM tb_trading_detail AS t ,c_user_ref_con AS urc " +
                " WHERE (t.rdid is null or t.rdid='' ) AND (t.rid is null or t.rid='') AND ((urc.mid is not  null or urc.mid!='' )) AND t.uid mod ? = ? "
                + " AND urc.oid = t.oid  AND t.uid = urc.uid AND urc.ooa_date =? AND t.payment_status='1' "
                + " AND t.trading_date>=? AND t.trading_date<=? AND t.trading_type=0 AND t.actual_amount!=0 AND t.oid is not null AND t.oid!='' "
                + "   ) as tt where  tt.oid is not null and tt.oid!='' and tt.amount!=0.000000  GROUP BY tt.uid, tt.oid";
        logger.info(t_sql);
        logger.info("params:" + startDate + " " + endDate + " " + day + " " + " " + cJob + " " + "" + mJob);
        List<RepaymentDto> list = jdbcTemplate.query(t_sql, new Object[]{startDate, endDate, cJob, mJob, mJob, cJob, day, startDate, endDate}, new BeanPropertyRowMapper(RepaymentDto.class));
        return list;
    }

    /**
     * 获取用户还款计划信息
     * 出账
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @param day       一个月的第几天
     * @return
     */
    public List<RepaymentDto> findUnRepayment(Date startDate, Date endDate,
                                              Integer day, Integer cJob, Integer mJob) {
        // 上月消费总额统计
        String t_sql = "SELECT t.uid, t.oid, SUM( t.actual_amount ) AS amount,urc.fid FROM tb_trading_detail AS t ,c_user_ref_con AS urc " +
                " WHERE (t.rdid is null or t.rdid='' ) AND t.uid mod ? = ? "
                + " AND urc.oid = t.oid  AND t.uid = urc.uid AND urc.ooa_date =?  "
                + " AND t.trading_date>=? AND t.trading_date<=? "
                + " GROUP BY t.uid, t.oid ORDER BY t.uid, t.oid ";
        List<RepaymentDto> list = jdbcTemplate.query(t_sql, new Object[]{mJob, cJob, day, startDate, endDate}, new BeanPropertyRowMapper(RepaymentDto.class));
        return list;
    }

    /**
     * 查询用户关联的资金端ID
     *
     * @param uid 用户ID
     * @return
     */
    public CUserRefCon getCUserRefCon(String uid, String oid, Integer cJob) {
        String sql = " SELECT uid,fid,mid,oid,ooa_date,repay_date FROM c_user_ref_con WHERE uid=? AND oid=? AND uid mod 10 = ? LIMIT 1";
        return (CUserRefCon) jdbcTemplate.queryForObject(sql, new Object[]{uid, oid, cJob}, new BeanPropertyRowMapper(CUserRefCon.class));
    }

    /**
     * 获取资金端信息
     *
     * @param fid 资金端ID
     * @return 资金端信息
     */
    public FFundend getFFundend(String fid) {
        String sql = " SELECT id, fname, name, mobile, address, account, pwd, cardid, bankcard, status"
                + ", create_date AS createDate, modify_date AS modifyDate FROM　f_fundend WHERE fid = ? LIMIT 1";
        return (FFundend) jdbcTemplate.queryForObject(sql, new Object[]{fid}, new BeanPropertyRowMapper(FFundend.class));
    }

    public int[] batchSaveTbRepaymentDetail(List<TbRepaymentDetail> repaymentDetailList) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into tb_repayment_detail(id, mid, oid, uid, utype, fid, total_times"
                + ", current_times, total_corpus, total_fee, total_late_fee, repayment_sn, status"
                + ", repay_corpus, repay_fee, repay_late_fee, out_date, repay_date, loop_start, loop_end"
                + ", rate, is_split, service_fee_rate, service_fee, ip, split_date, show_date, create_date, modify_date) values");
        sql.append(" (?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?,?, ?, ?, ?, ?, now(), now())");
        List<Object[]> params = new ArrayList<>();
        for (TbRepaymentDetail sd : repaymentDetailList) {
            params.add(new Object[]{sd.getId(), sd.getMid(), sd.getOid(), sd.getUid(), sd.getUtype()
                    , sd.getFid(), sd.getTotalTimes(), sd.getCurrentTimes(), sd.getTotalCorpus()
                    , sd.getTotalFee(), sd.getTotalLateFee(), sd.getRepaymentSn(), sd.getStatus()
                    , sd.getRepayCorpus(), sd.getRepayFee(), sd.getRepayLateFee()
                    , sd.getOutDate(), sd.getRepayDate(), sd.getLoopStart(), sd.getLoopEnd()
                    , sd.getRate(), sd.getIsSplit(), sd.getServiceFeeRate(), sd.getServiceFee()
                    , sd.getIp(), sd.getSplitDate(), sd.getShowDate()});
        }
        return jdbcTemplate.batchUpdate(sql.toString(), params);
    }

    public int[] batchSaveTbRepaymentPlan(List<TbRepaymentPlan> repaymentPlanlList) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into tb_repayment_plan(id, uid, utype, total_times, current_times"
                + ", total_corpus, total_fee, rest_corpus, rest_fee, current_corpus"
                + ", current_fee, late_fee, repayment_date, process_date, rd_id, serial_no"
                + ", status, repayment_type, yq_rate, loop_start, loop_end, show_date"
                + ", call_repay_status, create_date, modify_date) values");
        sql.append(" (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?,?, now(), now())");
        List<Object[]> params = new ArrayList<>();
        for (TbRepaymentPlan sd : repaymentPlanlList) {
            params.add(new Object[]{IdWorker.getIdStr(), sd.getUid(), sd.getUtype(), sd.getTotalTimes(), sd.getCurrentTimes()
                    , sd.getTotalCorpus(), sd.getTotalFee(), sd.getRestCorpus(), sd.getRestFee()
                    , sd.getCurrentCorpus(), sd.getCurrentFee(), sd.getLateFee(), sd.getRepaymentDate()
                    , sd.getProcessDate(), sd.getRdId(), sd.getSerialNo(), sd.getStatus()
                    , sd.getRepaymentType(), sd.getYqRate(), sd.getLoopStart(), sd.getLoopEnd()
                    , sd.getShowDate(), sd.getCallRepayStatus()});
        }
        return jdbcTemplate.batchUpdate(sql.toString(), params);
    }

    public int updateRdByDateAndUid(Date startDate, Date endDate, String uid, String rdId, String tradeType) {
        String sql = "UPDATE tb_trading_detail SET rdid=?  WHERE uid = ? AND payment_status='1' AND (  rid IS NULL OR rid ='')  AND (  rdid IS NULL OR rdid ='')" +
                "  AND trading_type = ? AND ( trading_date >? AND trading_date <?)";
        logger.info("updateRdByDateAndUid:" + sql);
        logger.info("updateRdByDateAndUid params:" + startDate + " " + endDate + " " + uid + "" + rdId + "" + tradeType);
        return jdbcTemplate.update(sql, rdId, uid, tradeType, startDate, endDate);
    }

    /**
     * 获取资金端信息
     *
     * @param
     * @return 资金端信息
     */
    public List<OOperaInfo> queryOOperaInfoList(List<String> oids) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT id,osn FROM o_opera_info WHERE id in(");
        for (String oid : oids) {
            sql.append("?,");
        }
        sql.append(")");
        sql = sql.deleteCharAt(sql.length() - 2);
        return jdbcTemplate.query(sql.toString(), oids.toArray(), new BeanPropertyRowMapper(OOperaInfo.class));
    }

    public TbRepaymentRatioDict getTbRepaymentRatioDictByOidAndFid(String oid, String fid) {
        String sql = " SELECT oid,fid,yq_rate as yqRate FROM tb_repayment_ratio_dict WHERE oid=? AND fid = ? LIMIT 1";
        List<TbRepaymentRatioDict> list = jdbcTemplate.query(sql, new Object[]{oid, fid}, new BeanPropertyRowMapper(TbRepaymentRatioDict.class));
        return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }

    public OOperaInfo getOOperaInfo(String oid) {
        String sql = " SELECT id,osn FROM o_opera_info WHERE id=? ";
        List<OOperaInfo> list = jdbcTemplate.query(sql, new Object[]{oid}, new BeanPropertyRowMapper(OOperaInfo.class));
        return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }

    public OOperaOtherInfo getOOperaOtherInfo(String oid) {
        String sql = "SELECT repay_date,ooa_date FROM o_opera_other_info a where a.oid=? ";
        return (OOperaOtherInfo) jdbcTemplate.queryForObject(sql, new Object[]{oid}, new BeanPropertyRowMapper(OOperaOtherInfo.class));
    }
}
