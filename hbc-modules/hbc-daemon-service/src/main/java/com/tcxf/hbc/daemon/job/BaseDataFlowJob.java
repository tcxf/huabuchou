package com.tcxf.hbc.daemon.job;

import com.tcxf.hbc.daemon.constant.enums.SettlementLoopEnum;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

public class BaseDataFlowJob {

    @Autowired
    private ISettlementService settlementService;

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> template;
    /**
     * 获取分片redis中结算记录的当前页
     * @param jobKey
     * @return
     */
    public String  getShardingContextFromRedis(String jobKey)
    {
        return template.opsForValue().get(jobKey);
    }

    /**
     * 设置分片redis中结算记录的当前页
     * @param jobKey
     * @param jobValue
     */
    public void setShardingContextToRedis(String jobKey,String jobValue)
    {
        template.opsForValue().set(jobKey,jobValue);
    }

    /**
     * 删除分片redis中结算记录的当前页
     * @param jobKey
     */
    public void deleteShardingContextFromRedis(String jobKey)
    {
        template.delete(jobKey);
    }
    /**
     * 获取结算数据
     * @param sll
     * @param c
     * @param item
     * @param itemTotalCount
     * @param startLimit
     * @param endLimit
     * @return
     */
    public List<SettlementDto> getList(SettlementLoopEnum sll, Calendar c,Integer item,Integer itemTotalCount,Integer startLimit,Integer endLimit)
    {
      return  settlementService.findSettlementList(
              sll,c,item,itemTotalCount,startLimit,endLimit);
    }

    /**
     * 结算处理
     * @param list
     */
    public void processList(List<SettlementDto> list)
    {
        settlementService.processSettlementList(list);
    }
}
