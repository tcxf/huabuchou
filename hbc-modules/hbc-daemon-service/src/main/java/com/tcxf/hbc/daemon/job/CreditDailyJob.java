package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 生成授信统计
 */
public class CreditDailyJob extends BaseDataFlowJob implements DataflowJob<CUserRefCon> {
    @Autowired
    private ISettlementService settlementService;

    @Override
    public void processData(ShardingContext shardingContext, List<CUserRefCon> list) {
        settlementService.addCreditDaily(list);
    }

    @Override
    public List<CUserRefCon> fetchData(ShardingContext shardingContext) {
        List<CUserRefCon> list =  settlementService.queryAuthMax();
        if(list.size()>0 )
        {
            if(list.get(0).getAuthMax()==null)
            {
                list.remove(0);
            }
        }
        return list;
    }
}
