package com.tcxf.hbc.daemon.job;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.daemon.model.dto.RepaymentSplitDto;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 滞纳金结算
 */
public class McPaymentSplitJob extends BaseDataFlowJob implements DataflowJob<TbRepaymentPlan> {
    private static final Logger logger = LoggerFactory.getLogger(McPaymentSplitJob.class);
    @Autowired
    ISettlementService iSettlementService;

    @Override
    public List<TbRepaymentPlan> fetchData(ShardingContext s) {
        String redisStart = super.getShardingContextFromRedis(s.getJobName()+s.getShardingItem());
        List<TbRepaymentPlan> list = null;
        if(redisStart!=null && !"".equals(redisStart))
        {
            Integer tempCount = Integer.parseInt(redisStart);
            list = iSettlementService.findRepaymentSplitList(s.getShardingItem(),s.getShardingTotalCount(),tempCount,tempCount+100);
            if(list!=null && list.size()>0)
            {
                super.setShardingContextToRedis(s.getJobName()+s.getShardingItem(),((tempCount+101)+""));
            }
            else
            {
                super.deleteShardingContextFromRedis(s.getJobName()+s.getShardingItem());
            }
        }
        else
        {
            list = iSettlementService.findRepaymentSplitList(s.getShardingItem(),s.getShardingTotalCount(),0,100);
            if(list!=null && list.size()>0)
            {
                super.setShardingContextToRedis(s.getJobName()+s.getShardingItem(),"101");
            }
        }

        if(list.size()==0)
        {
            list = null;
        }
        return list;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<TbRepaymentPlan> list) {

        logger.info("=============查询所有未还款产生的滞纳金==================="+ JSON.toJSONString(list));
        /**
         * 更新还款记录滞纳金
         */
        int listCount[] = iSettlementService.updateRepaymentSlitList(list);
        for (int i = 0; i <list.size() ; i++) {
            /**
             * 查询所有未还款产生的滞纳金
             */
            List<RepaymentSplitDto>  listDetail= iSettlementService.findRepaymentSplitSum(list.get(i).getRdId());

            /**
             * 更新所有未还款产生的滞纳金总额
             */
            int splitCount[] = iSettlementService.updateRepaymentSlitSum(listDetail);
            logger.info("更新滞纳金总额记录数:" + splitCount.length);
        }

        logger.info("更新还款记录用户数:" + listCount.length);

    }
}
