package com.tcxf.hbc.daemon.job;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.daemon.constant.enums.SettlementLoopEnum;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.List;

/**
 *  商户结算单按日结算-定时任务
 * @author tanyongfang
 *
 */
public class McSettlementForDaySimpleJob extends BaseDataFlowJob implements DataflowJob<SettlementDto> {
    private static final Logger logger = LoggerFactory.getLogger(McSettlementForDaySimpleJob.class);

    private Integer startLimit = 0;
    private Integer endLimit = 100;
    private Integer tempCount = 1;
    @Override
    public List<SettlementDto> fetchData(ShardingContext shardingContext) {
        Calendar c = Calendar.getInstance();
        String redisStart = super.getShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
        List<SettlementDto> listDto = null;
            if(redisStart!=null && !"".equals(redisStart))
            {
                Integer tempCount = Integer.parseInt(redisStart);
                listDto = super.getList(SettlementLoopEnum.DAY,c,shardingContext.getShardingItem(),
                        shardingContext.getShardingTotalCount(),tempCount,tempCount+endLimit);
                for (int i = 0; i < listDto.size(); i++) {
                    if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                    {
                        listDto.remove(i);
                    }
                }
                if(listDto!=null && listDto.size()>0)
                {
                    super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),(tempCount+endLimit+tempCount)+"");
                }
                else
                {
                    super.deleteShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
                }

            }
            else
            {
                listDto = super.getList(SettlementLoopEnum.DAY,c,shardingContext.getShardingItem(),
                        shardingContext.getShardingTotalCount(),startLimit,endLimit);
                for (int i = 0; i < listDto.size(); i++) {
                    if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                    {
                        listDto.remove(i);
                    }
                }
                if(listDto!=null && listDto.size()>0)
                {
                    super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),(endLimit+tempCount)+"");
                }
            }

            if(listDto.size()==0)
            {
                listDto = null;
            }
        return listDto;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<SettlementDto> list) {

        super.processList(list);
    }

}
