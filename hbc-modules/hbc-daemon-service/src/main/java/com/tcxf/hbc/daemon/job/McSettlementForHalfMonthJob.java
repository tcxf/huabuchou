package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.daemon.constant.enums.SettlementLoopEnum;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class McSettlementForHalfMonthJob extends BaseDataFlowJob implements DataflowJob<SettlementDto> {

    private Integer startLimit = 0;
    private Integer endLimit = 100;
    private Integer tempCount = 1;
    private static final Logger logger = LoggerFactory.getLogger(McSettlementForQuarterSimpleJob.class);

    @Override
    public List<SettlementDto> fetchData(ShardingContext shardingContext) {
        Calendar c = Calendar.getInstance();
        List<SettlementDto> listDto = null;
        if(c.get(Calendar.DAY_OF_MONTH)==15 || c.get(Calendar.DAY_OF_MONTH)==c.getActualMaximum(Calendar.DAY_OF_MONTH))
        {
            String redisStart = super.getShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());

            if(redisStart!=null && !"".equals(redisStart))
            {
                Integer tempCount = Integer.parseInt(redisStart);
                listDto = super.getList(SettlementLoopEnum.HALF_A_MONTH,c,shardingContext.getShardingItem(),
                        shardingContext.getShardingTotalCount(),tempCount,tempCount+endLimit);
                for (int i = 0; i < listDto.size(); i++) {
                    if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                    {
                        listDto.remove(i);
                    }
                }
                if(listDto!=null && listDto.size()>0)
                {
                    super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),(tempCount+endLimit+tempCount)+"");
                }
                else
                {
                    super.deleteShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
                }

            }
            else
            {
                listDto = super.getList(SettlementLoopEnum.HALF_A_MONTH,c,shardingContext.getShardingItem(),
                        shardingContext.getShardingTotalCount(),startLimit,endLimit);
                for (int i = 0; i < listDto.size(); i++) {
                    if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                    {
                        listDto.remove(i);
                    }
                }
                if(listDto!=null && listDto.size()>0)
                {
                    super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),(endLimit+tempCount)+"");
                }
            }
        }


        if(listDto==null || listDto.size()==0)
        {
            listDto = null;
        }
        return listDto;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<SettlementDto> list) {

        super.processList(list);
    }
}
