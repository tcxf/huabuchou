package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.daemon.constant.enums.SettlementLoopEnum;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 *  商户结算单按周结算-定时任务
 * @author tanyongfang
 * 
 */
public class McSettlementForWeekSimpleJob extends BaseDataFlowJob implements DataflowJob<SettlementDto> {
    private static final Logger logger = LoggerFactory.getLogger(McSettlementForWeekSimpleJob.class);
    @Override
    public List<SettlementDto> fetchData(ShardingContext shardingContext) {
        Calendar c = Calendar.getInstance();
        String redisStart = super.getShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
        List<SettlementDto> listDto = null;
        if(redisStart!=null && !"".equals(redisStart))
        {
            Integer tempCount = Integer.parseInt(redisStart);
            listDto = super.getList(SettlementLoopEnum.WEEK,c,shardingContext.getShardingItem(),
                    shardingContext.getShardingTotalCount(),tempCount,tempCount+100);
            for (int i = 0; i < listDto.size(); i++) {
                if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                {
                    listDto.remove(i);
                }
            }
            if(listDto!=null && listDto.size()>0)
            {
                super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),((tempCount+101)+""));
            }
            else
            {
                super.deleteShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
            }

        }
        else
        {
            listDto = super.getList(SettlementLoopEnum.WEEK,c,shardingContext.getShardingItem(),
                    shardingContext.getShardingTotalCount(),0,100);
            for (int i = 0; i < listDto.size(); i++) {
                if(listDto.get(i).getAmount()==null || Calculate.equalsZero(listDto.get(i).getAmount()))
                {
                    listDto.remove(i);
                }
            }
            if(listDto!=null && listDto.size()>0)
            {
                super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),"101");
            }
        }

        if(listDto.size()==0)
        {
            listDto = null;
        }
        return listDto;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<SettlementDto> list) {
        super.processList(list);
    }
}
