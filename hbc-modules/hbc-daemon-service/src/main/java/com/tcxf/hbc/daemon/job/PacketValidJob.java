package com.tcxf.hbc.daemon.job;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
public class PacketValidJob extends BaseDataFlowJob implements DataflowJob<MRedPacketSetting> {
    private static final Logger logger = LoggerFactory.getLogger(PacketValidJob.class);
    @Autowired
    ISettlementService settlementService;
    @Override
    public List<MRedPacketSetting> fetchData(ShardingContext shardingContext) {

        List<MRedPacketSetting> list= null;
        String redisStart = super.getShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
        if(redisStart!=null && !"".equals(redisStart))
        {
            Integer tempCount = Integer.parseInt(redisStart);
            list =settlementService.findInvalidPacket(shardingContext.getShardingItem(),shardingContext.getShardingTotalCount(),tempCount,tempCount+100);
            if(list!=null && list.size()>0)
            {
                super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),((tempCount+101)+""));
            }
            else
            {
                super.deleteShardingContextFromRedis(shardingContext.getJobName()+shardingContext.getShardingItem());
            }
        }
        else
        {
            list =settlementService.findInvalidPacket(shardingContext.getShardingItem(),shardingContext.getShardingTotalCount(),0,100);
            if(list!=null && list.size()>0)
            {
                super.setShardingContextToRedis(shardingContext.getJobName()+shardingContext.getShardingItem(),"101");
            }
        }
        if(list.size()==0)
        {
            list = null;
        }
        return list;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<MRedPacketSetting> list) {
            settlementService.updateInvalidPacket(list);
    }
}
