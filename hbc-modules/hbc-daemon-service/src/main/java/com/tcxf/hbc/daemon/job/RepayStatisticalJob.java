package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.TbRepayStatistical;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 生成已还款统计
 */
public class RepayStatisticalJob extends BaseDataFlowJob implements DataflowJob<TbRepayStatistical> {
    @Autowired
    private ISettlementService settlementService;
    @Override
    public List<TbRepayStatistical> fetchData(ShardingContext shardingContext) {
        return settlementService.queryRepayStatistical();
    }

    @Override
    public void processData(ShardingContext shardingContext, List<TbRepayStatistical> list) {
        settlementService.addRepayStatistical(list);
    }
}
