package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.RmCreditmonthStatistical;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 授信通过率统计
 */
public class RmCreditmonthStatisticalJob extends BaseDataFlowJob implements DataflowJob<RmCreditmonthStatistical> {
    @Autowired
    private ISettlementService settlementService;
    @Override
    public List<RmCreditmonthStatistical> fetchData(ShardingContext shardingContext) {
        return settlementService.queryCreditStatistical();
    }

    @Override
    public void processData(ShardingContext shardingContext, List<RmCreditmonthStatistical> list) {
        settlementService.addRmCreditmonthStatistical(list);
    }
}
