package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.RmRepaymentSituation;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 不良率统计
 */
public class RmRepaymentSituationJob extends BaseDataFlowJob implements DataflowJob<RmRepaymentSituation> {
    @Autowired
    private ISettlementService settlementService;
    @Override
    public List<RmRepaymentSituation> fetchData(ShardingContext shardingContext) {
        return settlementService.queryMonthStatistical();
    }

    @Override
    public void processData(ShardingContext shardingContext, List<RmRepaymentSituation> list) {
        settlementService.addRmRepaymentSituation(list);
    }
}
