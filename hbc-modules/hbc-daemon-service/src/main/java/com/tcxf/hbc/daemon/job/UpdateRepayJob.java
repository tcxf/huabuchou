package com.tcxf.hbc.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.entity.TbRepay;
import com.tcxf.hbc.daemon.service.ITbRepayService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author YWT_tai
 * @Date :Created in 9:13 2018/8/27
 */
public class UpdateRepayJob extends BaseDataFlowJob implements DataflowJob<TbRepay> {

    @Autowired
    private ITbRepayService iTbRepayService;

    @Override
    public List<TbRepay> fetchData(ShardingContext shardingContext) {

        List<TbRepay> list=iTbRepayService.findOverFifteenMsRepayBill();
        if(list!=null && list.size()==1 && (list.get(0).getUid()==null || list.get(0).getUid()==""))
        {
            list.remove(0);
        }
        return list;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<TbRepay> list) {
        iTbRepayService.updateById(list);
    }
}
