package com.tcxf.hbc.daemon.job;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.daemon.model.dto.RepaymentDto;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import sun.util.logging.resources.logging;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UserCreateRepaymentDetailSimpleJob extends BaseDataFlowJob implements DataflowJob<RepaymentDto> {

    private static final Logger logger = LoggerFactory.getLogger(UserCreateRepaymentDetailSimpleJob.class);

    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        /** 首先查询所有出账日期为今天的消费用户 **/
        int day = c.get(Calendar.DAY_OF_MONTH);
        /** 获取上个月消费总额，以及当月需要还款的分期总额 **/
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.DAY_OF_MONTH,1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MILLISECOND, 0);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);
        Date startDate = c.getTime();
        c.add(Calendar.MILLISECOND, 1);
        c.set(Calendar.DAY_OF_MONTH,31);
        Date endDate = c.getTime();
        System.out.println(startDate);
        System.out.println(endDate);
    }

    @Autowired
    private ISettlementService settlementService;
    @Override
    public List<RepaymentDto> fetchData(ShardingContext shardingContext) {
        Calendar c = Calendar.getInstance();
        /** 首先查询所有出账日期为今天的消费用户 **/
        int day = c.get(Calendar.DAY_OF_MONTH);
        /** 获取上个月消费总额，以及当月需要还款的分期总额 **/
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.DAY_OF_MONTH,1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MILLISECOND, 0);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);
        Date startDate = c.getTime();
        c.add(Calendar.MILLISECOND, 1);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        c.set(Calendar.DAY_OF_MONTH,31);
        Date endDate = c.getTime();
        List<RepaymentDto> list = null;
        list = settlementService.findUnRepaymentPage(startDate,endDate,day, shardingContext.getShardingItem(), shardingContext.getShardingTotalCount());
        for (int i = 0; i < list.size(); i++) {
            if(StringUtil.isEmpty(list.get(i).getOid()) || StringUtil.isEmpty(list.get(i).getFid()) )
            {
                list.remove(i);
            }
        }
        if(list.size()==0)
        {
            list = null;
        }
        else
        {
            logger.info("===============list size :"+list.size());
            logger.info("JSONOBJECT"+JSON.toJSONString(list.get(0)));
            logger.info("JSONLIST"+JSON.toJSONString(list));
        }
        return list;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<RepaymentDto> list) {
        logger.info("----------------------生成用户还款单和还款计划---"+shardingContext.getShardingItem()+"job-----------------");
        settlementService.processRepayment(list);
    }
}
