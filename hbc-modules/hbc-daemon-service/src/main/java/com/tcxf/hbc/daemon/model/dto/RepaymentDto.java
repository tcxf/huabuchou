package com.tcxf.hbc.daemon.model.dto;

import com.tcxf.hbc.common.util.DateUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RepaymentDto implements Serializable {

	private static final long serialVersionUID = -7166434233799366353L;
	private String oid; // 运营商id
	private String operaName; // 所属运营商
	private String uid; // 用户id
	private BigDecimal amount; // 账单总额
	private String osn; // 运营商编码
	private Date loopStart; // 账单周期开始
	private Date loopEnd; // 账单周期结束
	private String repayDate; // 账单还款日
	private String mid; //商户id
	private String ooaDate;//出账日期
	public String getOoaDate() {
		return ooaDate;
	}

	public void setOoaDate(String ooaDate) {
		this.ooaDate = ooaDate;
	}

	private Date startDate;
	private Date endDate;
	private Integer cJob;
	private Integer mJob;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getcJob() {
		return cJob;
	}

	public void setcJob(Integer cJob) {
		this.cJob = cJob;
	}

	public Integer getmJob() {
		return mJob;
	}

	public void setmJob(Integer mJob) {
		this.mJob = mJob;
	}

	private String id; // 用户id
	private String unum; // 用户id
	private String realName; // 用户姓名
	private String idCard; // 身份照
	private String mobile; // 用户手机号码

	private String oidS;
	private String fid;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getOidS() {
		return oidS;
	}

	public void setOidS(String oidS) {
		this.oidS = oidS;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUnum() {
		return unum;
	}

	public void setUnum(String unum) {
		this.unum = unum;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOperaName() {
		return operaName;
	}

	public void setOperaName(String operaName) {
		this.operaName = operaName;
	}

	public String getLoopStartEnd() {
		return DateUtil.format(loopStart, "yyyy-MM-dd") + " - "
				+ DateUtil.format(loopEnd, "yyyy-MM-dd");
	}

	public Date getLoopStart() {
		return loopStart;
	}

	public void setLoopStart(Date loopStart) {
		this.loopStart = loopStart;
	}

	public Date getLoopEnd() {
		return loopEnd;
	}

	public void setLoopEnd(Date loopEnd) {
		this.loopEnd = loopEnd;
	}

	public String getRepayDate() {
		return repayDate;
	}

	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}

	public String getOsn() {
		return osn;
	}

	public void setOsn(String osn) {
		this.osn = osn;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
