package com.tcxf.hbc.daemon.model.dto;

import java.math.BigDecimal;

public class RepaymentSplitDto {
    private String uid;
    private String oid;
    private String rdId;
    private BigDecimal totalLateFee;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getRdId() {
        return rdId;
    }

    public void setRdId(String rdId) {
        this.rdId = rdId;
    }

    public BigDecimal getTotalLateFee() {
        return totalLateFee;
    }

    public void setTotalLateFee(BigDecimal totalLateFee) {
        this.totalLateFee = totalLateFee;
    }
}
