package com.tcxf.hbc.daemon.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SettlementDto implements Serializable {

    private static final long serialVersionUID = -2618977393866899148L;

    private BigDecimal amount;                                  // 结算单结算总额
    private BigDecimal        shareFee;                                // 结算单商家让利金额
    private BigDecimal        platformShareFee;                        // 结算单平台让利金额
    private String            mid;
    private String            oid;
    private String            name;
    private Integer           settlementLoop;
    private String            osn;
    private BigDecimal operatorShareFee;                                //运营商交易抽成费
    private BigDecimal operaShareFee;                                   //运营商收取商户通道费用
    private String tid;                                           //交易id
    private Date startDate;
    private Date endDate;
    private Integer cJob;
    private String settlementStatus;
    private Integer mJob;
    private Date rdate;
    private BigDecimal operatorDiscountFee;

    public BigDecimal getOperatorDiscountFee() {
        return operatorDiscountFee;
    }

    public void setOperatorDiscountFee(BigDecimal operatorDiscountFee) {
        this.operatorDiscountFee = operatorDiscountFee;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }


    public Integer getmJob() {
        return mJob;
    }

    public void setmJob(Integer mJob) {
        this.mJob = mJob;
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getcJob() {
        return cJob;
    }

    public void setcJob(Integer cJob) {
        this.cJob = cJob;
    }

    public Date getRdate() {
        return rdate;
    }

    public void setRdate(Date rdate) {
        this.rdate = rdate;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getOsn() {
        return osn;
    }

    public void setOsn(String osn) {
        this.osn = osn;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(Integer settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public BigDecimal getPlatformShareFee() {
        return platformShareFee;
    }

    public void setPlatformShareFee(BigDecimal platformShareFee) {
        this.platformShareFee = platformShareFee;
    }

    public BigDecimal getOperatorShareFee() {
        return operatorShareFee;
    }

    public void setOperatorShareFee(BigDecimal operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public BigDecimal getOperaShareFee() {
        return operaShareFee;
    }

    public void setOperaShareFee(BigDecimal operaShareFee) {
        this.operaShareFee = operaShareFee;
    }
}
