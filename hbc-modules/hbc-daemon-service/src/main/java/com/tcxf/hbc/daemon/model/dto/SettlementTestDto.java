package com.tcxf.hbc.daemon.model.dto;

import java.io.Serializable;
import java.util.Date;

public class SettlementTestDto implements Serializable {

    private String id;
    private Date startDate;
    private Date endDate;
    private String jobType;
    private String settlementLoop;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(String settlementLoop) {
        this.settlementLoop = settlementLoop;
    }
}
