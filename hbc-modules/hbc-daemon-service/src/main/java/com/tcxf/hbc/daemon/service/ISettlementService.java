package com.tcxf.hbc.daemon.service;

import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.daemon.constant.enums.SettlementLoopEnum;
import com.tcxf.hbc.daemon.model.dto.RepaymentDto;
import com.tcxf.hbc.daemon.model.dto.RepaymentSplitDto;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 结算表
 * </p>
 *
 * @author tanyongfang
 * @since 2018-06-29
 */
public interface ISettlementService {

    public List<SettlementDto> queryTradingSettlementParamPage(String latestday, int cJob, int mJob, Integer startLimit, Integer endLimit);
    /**
     * 查询额度结算
     * @param cJob
     * @param mJob
     * @param startLimit
     * @param endLimit
     * @return
     */
    public List<SettlementDto> findCreditQuotaSettlementList(SettlementLoopEnum settlementLoopEnum, Calendar c,int cJob,int mJob,Integer startLimit,Integer endLimit);

    /**
     * 查询授信通过率
     * @return
     */
    public List<RmCreditmonthStatistical> queryCreditStatistical();
    /**
     * 查询不良率统计
     * @return
     */
    public List<RmRepaymentSituation> queryMonthStatistical();
    /**
     * 新增违约率
     * @param list
     * @return
     */
    public int[] addRmRepaymentSituation(List<RmRepaymentSituation> list);
    /**
     * 新增通过率
     * @param list
     * @return
     */
    public int[] addRmCreditmonthStatistical(List<RmCreditmonthStatistical> list);


    public List<TbRepayStatistical> queryRepayStatistical();

    public int[] addRepayStatistical(List<TbRepayStatistical> list);
    /**
     * 查询授信日报表数据
     * @return
     */
    public List<CUserRefCon> queryAuthMax();

    /**
     * 新增授信日报表数据
     * @return
     */
    public int[] addCreditDaily(List<CUserRefCon> list);

    /**
     * 更新失效优惠券
     * @param list
     */
    public void updateInvalidPacket(List<MRedPacketSetting> list);

    /**
     * 查询过期优惠券
     * @param cJob
     * @param mJob
     * @param startLimit
     * @param endLimit
     * @return
     */
    public List<MRedPacketSetting> findInvalidPacket(Integer cJob,Integer mJob,Integer startLimit,Integer endLimit);
    /**
     * 查询账单分页
     * @param startDate
     * @param endDate
     * @param day
     * @param cJob
     * @param mJob
     * @return
     */
    public List<RepaymentDto> findUnRepaymentPage(Date startDate, Date endDate,Integer day, Integer cJob, Integer mJob);

    /**
     * 创建账单
     * @param list
     */
    public void processRepayment(List<RepaymentDto> list);

    /**
     * 查询结算总数
     * @param settlementLoopEnum
     * @param c
     * @param cJob
     * @param mJob
     * @return
     */
    public HashMap<String,Object> findSettlementListCount(SettlementLoopEnum settlementLoopEnum, Calendar c,int cJob,int mJob);

    /**
     * 分页查询结算
     * @param cJob
     * @param mJob
     * @param startLimit
     * @param endLimit
     * @return
     */
    public List<SettlementDto> findSettlementList(SettlementLoopEnum settlementLoopEnum, Calendar c,int cJob,int mJob,Integer startLimit,Integer endLimit);

    /**
     * 结算处理
     * @param settlementDetailList
     */
    public void processSettlementList(List<SettlementDto> settlementDetailList);

    /**
     * 更新未还款产生的滞纳金总额
     * @param list
     * @return
     */
    public int[] updateRepaymentSlitSum(List<RepaymentSplitDto> list);

    /**
     * 查询总滞纳金
     * @return
     */
    public List<RepaymentSplitDto> findRepaymentSplitSum(String id);

    /**
     * 更新逾期账单
     * @param list
     * @return
     */
    public int[] updateRepaymentSlitList(List<TbRepaymentPlan> list);

    /**
     * 查询逾期账单
     * @param cJob
     * @param mJob
     * @return
     */
    public List<TbRepaymentPlan>  findRepaymentSplitList(Integer cJob, Integer mJob,Integer startLimit,Integer endLimit);

    /**
     * 创建用户结算单
     * @param mid
     * @param c
     */
    public void createUserSettlement(String mid,SettlementLoopEnum settlementLoopEnum,Calendar c);

    public List<HashMap<String,Object>> createOperaRepaymentDetail(Calendar c,int cJob);


    /**
     * 生成用户还款计划
     */
    public List<HashMap<String, Object>> createRepaymentDetail(Calendar c, Integer cJob,String userType,Integer mJob);

    /**
     * 生成用户逾期账单
     * @param c
     * @param cJob
     * @return
     */
    public List<HashMap<String,Object>> updateRepaymentSplit(Calendar c, Integer cJob);

}
