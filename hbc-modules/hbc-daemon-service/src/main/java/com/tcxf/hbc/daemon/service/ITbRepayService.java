package com.tcxf.hbc.daemon.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepay;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
public interface ITbRepayService{

    List<TbRepay> findOverFifteenMsRepayBill();

    public void updateById(List<TbRepay> list);
}
