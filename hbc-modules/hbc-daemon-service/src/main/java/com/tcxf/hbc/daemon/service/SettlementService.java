package com.tcxf.hbc.daemon.service;

import com.tcxf.hbc.daemon.dao.SettlementDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 测试结算服务
 */
@Service
public class SettlementService {
    @Autowired
    private SettlementDao settlementDao;

    private final static int TOTAL_PART_NUM = 3;

    /**
     * 获取用户授信支付日结交易
     *
     * @param currentPart
     */
    public void fetchDayUserSettlementByPart(Integer currentPart) {
        String beginTime = null;
        String endTime = null;
        settlementDao.queryUserSettlementBypartId(beginTime, endTime, TOTAL_PART_NUM, currentPart);
    }
}
