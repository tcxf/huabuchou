package com.tcxf.hbc.daemon.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.daemon.constant.enums.*;
import com.tcxf.hbc.daemon.dao.SettlementDao;
import com.tcxf.hbc.daemon.model.dto.RepaymentDto;
import com.tcxf.hbc.daemon.model.dto.RepaymentSplitDto;
import com.tcxf.hbc.daemon.model.dto.SettlementDto;
import com.tcxf.hbc.daemon.model.dto.SettlementTestDto;
import com.tcxf.hbc.daemon.service.ISettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 结算表 服务实现类
 * </p>
 *
 * @author tanyongfang
 * @since 2018-06-29
 */
@Service
public class SettlementServiceImpl implements ISettlementService {

    private static final Logger logger = LoggerFactory.getLogger(SettlementServiceImpl.class);

    @Override
    public List<SettlementDto> queryTradingSettlementParamPage(String latestday, int cJob, int mJob, Integer startLimit, Integer endLimit) {
        Date startDate = null;
        Date endDate = null;
        if(latestday.equals("30"))
        {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -31);
            startDate = c.getTime();
            Calendar ca = Calendar.getInstance();
            ca.set(Calendar.HOUR_OF_DAY, 23);
            ca.set(Calendar.MINUTE, 59);
            ca.set(Calendar.SECOND, 59);
            ca.set(Calendar.MILLISECOND, 59);
            ca.add(Calendar.DATE,-1);
            endDate =ca.getTime();
        }
        else if(latestday.equals("60"))
        {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -61);
            startDate = c.getTime();
            Calendar ca = Calendar.getInstance();
            ca.set(Calendar.HOUR_OF_DAY, 23);
            ca.set(Calendar.MINUTE, 59);
            ca.set(Calendar.SECOND, 59);
            ca.set(Calendar.MILLISECOND, 59);
            ca.add(Calendar.DATE,-1);

            endDate =ca.getTime();
        }
        else if(latestday.equals("90"))
        {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -91);
            startDate = c.getTime();
            Calendar ca = Calendar.getInstance();
            ca.set(Calendar.HOUR_OF_DAY, 23);
            ca.set(Calendar.MINUTE, 59);
            ca.set(Calendar.SECOND, 59);
            ca.set(Calendar.MILLISECOND, 59);
            ca.add(Calendar.DATE,-1);
            endDate =ca.getTime();
        }
        else if(latestday.equals("15"))
        {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -16);
            startDate = c.getTime();
            Calendar ca = Calendar.getInstance();
            ca.set(Calendar.HOUR_OF_DAY, 23);
            ca.set(Calendar.MINUTE, 59);
            ca.set(Calendar.SECOND, 59);
            ca.set(Calendar.MILLISECOND, 59);
            ca.add(Calendar.DATE,-1);
            endDate =ca.getTime();
        }
        else if(latestday.equals("45"))
        {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.add(Calendar.DAY_OF_MONTH, -46);
            startDate = c.getTime();
            Calendar ca = Calendar.getInstance();
            ca.set(Calendar.HOUR_OF_DAY, 23);
            ca.set(Calendar.MINUTE, 59);
            ca.set(Calendar.SECOND, 59);
            ca.set(Calendar.MILLISECOND, 59);
            ca.add(Calendar.DATE,-1);
            endDate =ca.getTime();
        }
        Date rdate = new Date();
        // 查询商户结算
        Integer settlementLoop = 8;
        String credit = TradingTypeEnum.CREDIT.getKey();
        List<SettlementDto> settlementDetailList = settlementDao.queryTradingSettlementParamPage(latestday,startDate, endDate, credit,
                settlementLoop,cJob,mJob,startLimit,endLimit,rdate);
        System.out.println(settlementDetailList.size()+":"+cJob);
        return settlementDetailList;
    }

    @Override
    public List<SettlementDto> findCreditQuotaSettlementList(SettlementLoopEnum settlementLoopEnum, Calendar c, int cJob, int mJob, Integer startLimit, Integer endLimit) {
        Date startDate = new Date();
        Date endDate = new Date();
        Date rdate = new Date();
        // 查询商户结算
        String credit = TradingTypeEnum.CREDIT.getKey();
        List<SettlementDto> settlementDetailList = settlementDao.queryTradingCreditQuotaSettlementPage(startDate, endDate, credit,
                8,cJob,mJob,startLimit,endLimit,rdate);
        System.out.println(settlementDetailList.size()+":"+cJob);
        return settlementDetailList;
    }

    @Override
    public List<RmCreditmonthStatistical> queryCreditStatistical() {
        return settlementDao.queryCreditStatistical();
    }

    @Override
    public List<RmRepaymentSituation> queryMonthStatistical() {
        return settlementDao.queryMonthStatistical();
    }

    @Override
    public int[] addRmRepaymentSituation(List<RmRepaymentSituation> list) {
        return settlementDao.addRmRepaymentSituation(list);
    }

    @Override
    public int[] addRmCreditmonthStatistical(List<RmCreditmonthStatistical> list) {
        return settlementDao.addRmCreditmonthStatistical(list);
    }

    @Override
    public int[] addCreditDaily(List<CUserRefCon> list) {
        return settlementDao.addCreditDaily(list);
    }

    @Override
    public List<TbRepayStatistical> queryRepayStatistical() {
        return settlementDao.queryRepayStatistical();
    }

    @Override
    public int[] addRepayStatistical(List<TbRepayStatistical> list) {
        return settlementDao.addRepayStatistical(list);
    }

    @Override
    public List<CUserRefCon> queryAuthMax() {
        return settlementDao.queryAuthMax();
    }

    @Override
    public void updateInvalidPacket(List<MRedPacketSetting> list)
    {
        settlementDao.updateInvalidPacket(list);
    }

    @Override
    public void processRepayment(List<RepaymentDto> list) {
        List<TbRepaymentDetail> dlist = new ArrayList<TbRepaymentDetail>();
        List<String> oids = new ArrayList<String>(); // 保存所有的运营商id
        int temp =1;
        Date startDate = list.get(0).getStartDate();
        Date endDate = list.get(0).getEndDate();
        for (RepaymentDto r : list) { // 循环所有还款数据
            logger.error("job任务:"+r.getcJob()+" 账单信息：" + JSON.toJSONString(r));
            if (r.getAmount()==null)
            {
                continue;
            }
            if (Calculate.equalsZero(r.getAmount())) {

                continue;
            }

            CUserRefCon info = settlementDao.getCUserRefCon(r.getUid(),r.getOid(),r.getcJob());
            if (info == null) {
                logger.error("CUserRefCon is null");
                continue;
            }
//            FFundend f = settlementDao.getFFundend(info.getFid());
            TbRepaymentRatioDict rrd = settlementDao.getTbRepaymentRatioDictByOidAndFid(r.getOid(),r.getFid());
            if (rrd == null) {
                logger.error("TbRepaymentRatioDict is null");
                continue;
            }
            OOperaInfo oInfo = settlementDao.getOOperaInfo(r.getOid());
            if (oInfo == null) {
                logger.error("OOperaInfo is null");
                continue;
            }

            TbRepaymentDetail rd = new TbRepaymentDetail();
            rd.setId(IdWorker.getIdStr());
            rd.setUid(r.getUid());
            rd.setRate(rrd == null ? BigDecimal.valueOf(0l) : rrd.getYqRate());
            rd.setOid(r.getOid());
            rd.setFid(r.getFid());
            rd.setTotalCorpus(r.getAmount());
            rd.setTotalFee(BigDecimal.valueOf(0));
            rd.setTotalLateFee(BigDecimal.valueOf(0));
            rd.setTotalTimes(1);
            rd.setCurrentTimes(1);
            rd.setStatus("0");
            rd.setIsSplit("0");
            rd.setRepayCorpus(BigDecimal.valueOf(0));
            rd.setRepayFee(BigDecimal.valueOf(0));
            rd.setRepayLateFee(BigDecimal.valueOf(0));
            rd.setLoopStart(r.getStartDate());
            rd.setLoopEnd(r.getEndDate());
            rd.setUtype("1");
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.DAY_OF_MONTH, info.getRepayDate() ==null? 1 : Integer.parseInt(info.getRepayDate()));
            rd.setRepayDate(c.getTime());
            rd.setOutDate(info.getOoaDate());
            String serialNo = "R" + DateUtil.format(new Date(), "yyyyMMddHHmmss")
                    + StringUtil.RadomCode(4, "other").toUpperCase() + "00"+temp;
            temp = temp+1;
            rd.setRepaymentSn(serialNo);
            dlist.add(rd);
            if (!oids.contains(r.getOid())) {
                oids.add(r.getOid());
            }
        }
        if (dlist.size() > 0){
            // 批量保存还款数据
            settlementDao.batchSaveTbRepaymentDetail(dlist);
            List<TbRepaymentPlan> rplist = new ArrayList<TbRepaymentPlan>();
            List<OOperaInfo> oinfos = settlementDao.queryOOperaInfoList(oids);
            Map<String, OOperaInfo> omap = new HashMap<>();
            for (OOperaInfo operaInfo : oinfos) {
                omap.put(operaInfo.getId(), operaInfo);
            }


            for (TbRepaymentDetail s : dlist) { // 批量生成还款计划
                TbRepaymentPlan rp = new TbRepaymentPlan();
                rp.setUtype("1");
                rp.setCurrentCorpus(s.getTotalCorpus());
                rp.setCurrentFee(s.getTotalFee());
                rp.setCurrentTimes(1);
                rp.setLateFee(BigDecimal.valueOf(0));
                rp.setRdId(s.getId());
                rp.setShowDate(s.getLoopEnd());
                rp.setRepaymentDate(s.getRepayDate());
                rp.setRestCorpus(rp.getCurrentCorpus());
                rp.setRestFee(rp.getCurrentFee());
                rp.setTotalCorpus(rp.getCurrentCorpus());
                rp.setYqRate(new BigDecimal(s.getRate()+""));
                rp.setRepaymentDate(s.getRepayDate());
                OOperaInfo oOperaInfo = omap.get(s.getOid());
                String serialNo = "RP" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + oOperaInfo == null ? null : oOperaInfo.getOsn()
                        + StringUtil.RadomCode(4, "other").toUpperCase() + "0001";
                rp.setSerialNo(serialNo);
                rp.setStatus(RepaymentPlanStatusEnum.UN_PAID.getKey());
                rp.setTotalFee(rp.getCurrentFee());
                rp.setTotalTimes(1);
                rp.setUid(s.getUid());
                rp.setCallRepayStatus("0");
                rplist.add(rp);
                int effCount = settlementDao.updateRdByDateAndUid(startDate, endDate, rp.getUid(), s.getId(),TradingTypeEnum.CREDIT.getKey());
                logger.error("更新交易记录表行数：[{}]", effCount);
            }
            settlementDao.batchSaveTbRepaymentPlan(rplist);
        }


    }

    @Override
    public List<MRedPacketSetting> findInvalidPacket(Integer cJob, Integer mJob, Integer startLimit, Integer endLimit) {
        return settlementDao.findInvalidPacket(cJob,mJob,startLimit,endLimit);
    }

    @Override
    public List<RepaymentDto> findUnRepaymentPage(Date startDate, Date endDate, Integer day, Integer cJob, Integer mJob) {
        return settlementDao.findUnRepaymentPage(startDate, endDate, day, cJob, mJob);
    }



    @Override
    public HashMap<String, Object> findSettlementListCount(SettlementLoopEnum settlementLoopEnum, Calendar c, int cJob, int mJob) {
        SettlementLoopEnum.SettlementDate sdate = settlementLoopEnum.getSettlementDate(c);
        HashMap<String,Object> resultMap =new HashMap<String,Object>();
        Integer uTotalCount = 0;
        Integer mTotalCount = 0;
        Date startDate = sdate.getStartDate();
        Date endDate = sdate.getEndDate();
        Date rdate = sdate.getRdate();
        // 查询商户结算
        Integer settlementLoop = settlementLoopEnum.getKey();
        String credit = TradingTypeEnum.CREDIT.getKey();


//        SettlementTestDto std = settlementDao.findSettlementByTest(settlementLoop);
//        if(std!=null)
//        {
//            startDate = std.getStartDate();
//            endDate = std.getEndDate();
//        }
          return settlementDao.queryTradingSettlementCount(startDate, endDate, credit,settlementLoop,cJob,mJob);
    }

    @Override
    public List<SettlementDto> findSettlementList(SettlementLoopEnum settlementLoopEnum, Calendar c, int cJob, int mJob, Integer startLimit, Integer endLimit) {
        SettlementLoopEnum.SettlementDate sdate = settlementLoopEnum.getSettlementDate(c);
        Date startDate = sdate.getStartDate();
        Date endDate = sdate.getEndDate();
        Date rdate = new Date();
        // 查询商户结算
        Integer settlementLoop = settlementLoopEnum.getKey();
        String credit = TradingTypeEnum.CREDIT.getKey();
        List<SettlementDto> settlementDetailList = settlementDao.queryTradingSettlementPage(startDate, endDate, credit,
                settlementLoop,cJob,mJob,startLimit,endLimit,rdate);
        System.out.println(settlementDetailList.size()+":"+cJob);
        return settlementDetailList;
    }

    @Override
    public void processSettlementList(List<SettlementDto> settlementDetailList) {
        // 查询商户结算
        Integer settlementLoop = settlementDetailList.get(0).getSettlementLoop();
        String credit = TradingTypeEnum.CREDIT.getKey();
        Date startDate = settlementDetailList.get(0).getStartDate();
        Date endDate = settlementDetailList.get(0).getEndDate();
        if (!CollectionUtils.isEmpty(settlementDetailList)) {
            List<TbSettlementDetail> dlist = new ArrayList<TbSettlementDetail>(settlementDetailList.size());
            int temp  = 1;
            for (SettlementDto sd : settlementDetailList) {
                if(Calculate.equalsZero(sd.getAmount()))
                {
                    logger.warn("Amount0:[mid={},amount={}] ", sd.getMid(), 0);
                    continue;
                }
                if (Calculate.equalsZero(sd.getAmount())) {
                    logger.warn("商户金额为0:[mid={},amount={}] ", sd.getMid(), 0);
                    continue;
                }

                TbSettlementDetail s = new TbSettlementDetail();
                s.setId(IdWorker.getIdStr());
                s.setTid(sd.getTid());
                s.setMid(sd.getMid());
                s.setOid(sd.getOid());
                String fid = settlementDao.queryMerchantFidList(sd.getMid());
                s.setFid(fid);// 商户对应的资金端ID
                BigDecimal tempMoney = Calculate.add(sd.getOperaShareFee(),sd.getOperatorShareFee()).getAmount();
                BigDecimal resultMoney = Calculate.add(tempMoney,sd.getOperatorDiscountFee()).getAmount();
                s.setAmount(Calculate.subtract(sd.getAmount(),resultMoney).getAmount());
                s.setOperaShareFee(sd.getOperaShareFee());
                s.setShareFee(sd.getShareFee());
                s.setDiscountFee(sd.getOperatorDiscountFee());
                s.setPlatformShareFee(sd.getPlatformShareFee());
                s.setStatus(SettlementStatusEnum.UNCONFIRMED.getKey());
                if(sd.getSettlementStatus().equals("0"))
                {
                    s.setOperStatus(OperStatusEnum.FOR_THE_ACCOUNT.getKey());
                    s.setPlatStatus(PlatStatusEnum.FOR_THE_ACCOUNT.getKey());
                }
                else
                {
                    s.setOperStatus(OperStatusEnum.UNCONFIRMED.getKey());
                    s.setPlatStatus(PlatStatusEnum.UNCONFIRMED.getKey());
                }


                s.setLoopStart(sd.getStartDate());
                s.setOutSettlementDate(sd.getRdate());
                s.setLoopEnd(sd.getEndDate());
                s.setCreateDate(new Date());
                String serialNo = "J" + DateUtil.format(new Date(), "yyyyMMddHHmmss")
                        + StringUtil.RadomCode(4, "other").toUpperCase() +"00"+ temp;
                temp++;
                s.setSerialNo(serialNo);
                dlist.add(s);
            }
            if (dlist.size() > 0) {
//                mTotalCount = mTotalCount+dlist.size();
                settlementDao.batchSaveTbSettlementDetail(dlist);
                logger.error("batchSaveTbSettlementDetail.. ================="+JSON.toJSONString(dlist)+"===============================");

                //修改钱包记录结算id和状态
                settlementDao.batchUpdateWalletIncome(dlist);
                logger.error("batchUpdateWalletIncome.. ");

                for (TbSettlementDetail sd : dlist) {

                    // 生成用户结算
//                    List<TbUserSharefeeSettlement> userList = settlementDao.createUserSettlement(sd.getMid(), sd.getId(), startDate, endDate);
//                    int userCount[] = settlementDao.batchSaveUserSharefeeDetail(userList, sd.getId());
                    //
                    // 更新交易记录表 结算id，结算状态
                    int[] effCount = settlementDao.updateSidByDateAndMid(sd.getId(),
                            sd.getTid());
                    logger.error("更新交易记录表行数：[{}]", effCount.length);
                }
            }
        } else {
            logger.error("查询商户交易结算为空,条件:[startDate={},endDate={},tradingType={},settlementLoopType={}]",
                    DateUtil.format(startDate), DateUtil.format(endDate), credit, settlementLoop);
        }

    }

    @Override
    public int[] updateRepaymentSlitSum(List<RepaymentSplitDto> list) {
        return settlementDao.updateRepaymentSlitSum(list);
    }

    @Autowired
    private SettlementDao settlementDao;

    @Override
    public List<RepaymentSplitDto> findRepaymentSplitSum(String id) {
        return settlementDao.findRepaymentSplitSum(id);
    }

    @Override
    public int[] updateRepaymentSlitList(List<TbRepaymentPlan> list) {
        return settlementDao.updateRepaymentSlitList(list);
    }

    @Override
    public List<TbRepaymentPlan> findRepaymentSplitList(Integer cJob, Integer mJob,Integer startDate,Integer endDate) {
        return settlementDao.findRepaymentSplitList(cJob,mJob,startDate,endDate);
    }

    @Override
    public List<HashMap<String, Object>> updateRepaymentSplit(Calendar c, Integer cJob) {
        return null;
    }

    @Override
    public void createUserSettlement(String mid,SettlementLoopEnum settlementLoopEnum, Calendar c) {
        SettlementLoopEnum.SettlementDate sdate = settlementLoopEnum.getSettlementDate(c);
        Date startDate = sdate.getStartDate();
        Date endDate = sdate.getEndDate();
    }

    @Override
    public List<HashMap<String, Object>> createOperaRepaymentDetail( Calendar c,int cJob) {
        List<HashMap<String,Object>> listResult = new ArrayList<HashMap<String,Object>>();
        List<TbRepaymentDetail> dlist = new ArrayList<TbRepaymentDetail>();
        HashMap<String,Object> resultMap =new HashMap<String, Object>();
        Calendar oldC = Calendar.getInstance();
        oldC.setTime(c.getTime());

        /** 首先查询所有出账日期为今天的消费用户 **/
        int day = c.get(Calendar.DAY_OF_MONTH);
        /** 获取上个月消费总额，以及当月需要还款的分期总额 **/
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MILLISECOND, -1);
        Date endDate = c.getTime();
        c.add(Calendar.MILLISECOND, 1);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);
        Date startDate = c.getTime();
        List<RepaymentDto>  list = settlementDao.findOperaRepayment(startDate,endDate,day,cJob);
        if(CollectionUtils.isEmpty(list)) {
            logger.error("商户job任务:"+cJob+" 获取上月消费为空,参数[startDate={},endDate={},ooaDate={}]",startDate, endDate, day);
            return listResult;
        }
        else
        {
            resultMap.put("tradeCount",dlist.size());
            listResult.add(resultMap);
        }
        for (RepaymentDto r : list) { // 循环所有还款数据
            logger.error("商户job任务:"+cJob+" 账单信息：" + JSON.toJSONString(r));
            if (Calculate.equalsZero(r.getAmount())) {
                continue;
            }

            RepaymentDto rdt= settlementDao.queryFidByOid(r.getOid());
            TbRepaymentRatioDict rrd = settlementDao.getTbRepaymentRatioDictByOidAndFid(r.getOid(),rdt.getFid());
            if (rrd == null) {
                continue;
            }
            OOperaInfo oInfo = settlementDao.getOOperaInfo(r.getOid());
            if (oInfo == null) {
                continue;
            }

            TbRepaymentDetail rd = new TbRepaymentDetail();
            rd.setId(IdWorker.getIdStr());
            rd.setUid(r.getUid());
            rd.setMid(r.getMid());
            rd.setRate(rrd == null ? BigDecimal.valueOf(0l) : rrd.getYqRate());
            rd.setOid(r.getOid());
            rd.setFid(rdt.getFid());
            rd.setTotalCorpus(r.getAmount());
            rd.setTotalFee(BigDecimal.valueOf(0));
            rd.setTotalLateFee(BigDecimal.valueOf(0));
            rd.setTotalTimes(1);
            rd.setCurrentTimes(1);
            rd.setStatus("0");
            rd.setIsSplit("0");
            rd.setRepayCorpus(BigDecimal.valueOf(0));
            rd.setRepayFee(BigDecimal.valueOf(0));
            rd.setRepayLateFee(BigDecimal.valueOf(0));
            rd.setLoopStart(startDate);
            rd.setLoopEnd(endDate);
            rd.setUtype("2");
            c = oldC;
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.DAY_OF_MONTH, r.getRepayDate() ==null? 1 : Integer.parseInt(r.getRepayDate()));
            rd.setRepayDate(c.getTime());
            rd.setOutDate(r.getOoaDate());
            String serialNo = "R" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + oInfo.getOsn()
                    + StringUtil.RadomCode(4, "other").toUpperCase() + "0001";
            rd.setRepaymentSn(serialNo);
            dlist.add(rd);
        }
        if (dlist.size() == 0){
            return listResult;
        }

        // 批量保存还款数据
        settlementDao.batchSaveTbRepaymentDetail(dlist);
        List<TbRepaymentPlan> rplist = new ArrayList<TbRepaymentPlan>();
        Map<String, OOperaInfo> omap = new HashMap<>();
        for (TbRepaymentDetail s : dlist) { // 批量生成还款计划
            TbRepaymentPlan rp = new TbRepaymentPlan();
            rp.setUtype("2");
            rp.setCurrentCorpus(s.getTotalCorpus());
            rp.setCurrentFee(s.getTotalFee());
            rp.setCurrentTimes(1);
            rp.setLateFee(BigDecimal.valueOf(0));
            rp.setRdId(s.getId());
            rp.setShowDate(s.getLoopEnd());
            rp.setRepaymentDate(s.getRepayDate());
            rp.setRestCorpus(rp.getCurrentCorpus());
            rp.setRestFee(rp.getCurrentFee());
            rp.setTotalCorpus(rp.getCurrentCorpus());
            rp.setYqRate(new BigDecimal(s.getRate()+""));
            rp.setRepaymentDate(s.getRepayDate());
            OOperaInfo oOperaInfo = omap.get(s.getOid());
            String serialNo = "RP" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + oOperaInfo == null ? null : oOperaInfo.getOsn()
                    + StringUtil.RadomCode(4, "other").toUpperCase() + "0001";
            rp.setSerialNo(serialNo);
            rp.setStatus(RepaymentPlanStatusEnum.UN_PAID.getKey());
            rp.setTotalFee(rp.getCurrentFee());
            rp.setTotalTimes(1);
            rp.setUid(s.getUid());
            rp.setCallRepayStatus("0");
            rplist.add(rp);
            int effCount = settlementDao.updateRdByDateAndUid(startDate, endDate, rp.getUid(), s.getId(),TradingTypeEnum.CREDIT.getKey());
            logger.error("更新交易记录表行数：[{}]", effCount);
        }
        settlementDao.batchSaveTbRepaymentPlan(rplist);
        resultMap.put("repaymentCount",rplist.size());

//        // 循环发送还款信息
//        for (TbRepaymentDetail list1 : dlist) {
//            MessageBean message = new TbRepaymentMessage(list1.getTotalCorpus().toString(),
//                    DateUtil.format(list1.getRepayDate(), "MM-dd"));
//            SmsClSupport.send(userInfoService.get(list1.getUid()).getMobile(), message.getContent());
//        }
        return listResult;
    }

    /**
     * 分页查询
     * @param queryString
     * @param startIndex
     * @param pageSize
     * @return
     */
    public String getPageData(String queryString, Integer startIndex, Integer pageSize)
    {
        String result = "";
        if (null != startIndex && null != pageSize)
        {
            result = queryString + " limit " + startIndex + "," + pageSize;
        } else if (null != startIndex && null == pageSize)
        {
            result = queryString + " limit " + startIndex;
        } else
        {
            result = queryString;
        }
        return result;
    }


    @Override
    @Transactional
    public List<HashMap<String, Object>> createRepaymentDetail(Calendar c,Integer cJob,String userType,Integer mJob) {
        List<HashMap<String,Object>> listResult = new ArrayList<HashMap<String,Object>>();
        Calendar oldC = Calendar.getInstance();
        oldC.setTime(c.getTime());

        /** 首先查询所有出账日期为今天的消费用户 **/
        int day = c.get(Calendar.DAY_OF_MONTH);
        /** 获取上个月消费总额，以及当月需要还款的分期总额 **/
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MILLISECOND, -1);
        Date endDate = c.getTime();
        c.add(Calendar.MILLISECOND, 1);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);
        Date startDate = c.getTime();

        // 获取上月消费总额
        List<RepaymentDto> list = settlementDao.findUnRepayment(startDate, endDate, day,cJob,mJob);
        List<TbRepaymentDetail> dlist = new ArrayList<TbRepaymentDetail>();
        List<String> oids = new ArrayList<String>(); // 保存所有的运营商id
        HashMap<String,Object> resultMap =new HashMap<String, Object>();


        if(CollectionUtils.isEmpty(list)) {
            logger.error("job任务:"+cJob+" 获取上月消费为空,参数[startDate={},endDate={},ooaDate={}]",startDate, endDate, day);
            return listResult;
        }
        else
        {
            resultMap.put("tradeCount",dlist.size());
            listResult.add(resultMap);
        }
        int temp =1;
        for (RepaymentDto r : list) { // 循环所有还款数据
            logger.error("job任务:"+cJob+" 账单信息：" + JSON.toJSONString(r));
            if (Calculate.equalsZero(r.getAmount())) {
                logger.error("r.getAmount() is 0");
                continue;
            }

            CUserRefCon info = settlementDao.getCUserRefCon(r.getUid(),r.getOid(),cJob);
            if (info == null) {
                logger.error("CUserRefCon is null");
                continue;
            }
//            FFundend f = settlementDao.getFFundend(info.getFid());
            TbRepaymentRatioDict rrd = settlementDao.getTbRepaymentRatioDictByOidAndFid(r.getOid(),r.getFid());
            if (rrd == null) {
                logger.error("TbRepaymentRatioDict is null");
                continue;
            }
            OOperaInfo oInfo = settlementDao.getOOperaInfo(r.getOid());
            if (oInfo == null) {
                logger.error("OOperaInfo is null");
                continue;
            }

            TbRepaymentDetail rd = new TbRepaymentDetail();
            rd.setId(IdWorker.getIdStr());
            rd.setUid(r.getUid());
            rd.setRate(rrd == null ? BigDecimal.valueOf(0l) : rrd.getYqRate());
            rd.setOid(r.getOid());
            rd.setFid(r.getFid());
            rd.setTotalCorpus(r.getAmount());
            rd.setTotalFee(BigDecimal.valueOf(0));
            rd.setTotalLateFee(BigDecimal.valueOf(0));
            rd.setTotalTimes(1);
            rd.setCurrentTimes(1);
            rd.setStatus("0");
            rd.setIsSplit("0");
            rd.setRepayCorpus(BigDecimal.valueOf(0));
            rd.setRepayFee(BigDecimal.valueOf(0));
            rd.setRepayLateFee(BigDecimal.valueOf(0));
            rd.setLoopStart(startDate);
            rd.setLoopEnd(endDate);
            rd.setUtype(userType);
            c = oldC;
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.DAY_OF_MONTH, info.getRepayDate() ==null? 1 : Integer.parseInt(info.getRepayDate()));
            rd.setRepayDate(c.getTime());
            rd.setOutDate(info.getOoaDate());
            String serialNo = "R" + DateUtil.format(new Date(), "yyyyMMddHHmmss")
                    + StringUtil.RadomCode(4, "other").toUpperCase() + "00"+temp;
            temp = temp+1;
            rd.setRepaymentSn(serialNo);
            dlist.add(rd);
            if (!oids.contains(r.getOid())) {
                oids.add(r.getOid());
            }
        }
        if (dlist.size() == 0){
            return listResult;
        }

        // 批量保存还款数据
        settlementDao.batchSaveTbRepaymentDetail(dlist);
        List<TbRepaymentPlan> rplist = new ArrayList<TbRepaymentPlan>();
        List<OOperaInfo> oinfos = settlementDao.queryOOperaInfoList(oids);
        Map<String, OOperaInfo> omap = new HashMap<>();
        for (OOperaInfo operaInfo : oinfos) {
            omap.put(operaInfo.getId(), operaInfo);
        }


        for (TbRepaymentDetail s : dlist) { // 批量生成还款计划
            TbRepaymentPlan rp = new TbRepaymentPlan();
            rp.setUtype(userType);
            rp.setCurrentCorpus(s.getTotalCorpus());
            rp.setCurrentFee(s.getTotalFee());
            rp.setCurrentTimes(1);
            rp.setLateFee(BigDecimal.valueOf(0));
            rp.setRdId(s.getId());
            rp.setShowDate(s.getLoopEnd());
            rp.setRepaymentDate(s.getRepayDate());
            rp.setRestCorpus(rp.getCurrentCorpus());
            rp.setRestFee(rp.getCurrentFee());
            rp.setTotalCorpus(rp.getCurrentCorpus());
            rp.setYqRate(new BigDecimal(s.getRate()+""));
            rp.setRepaymentDate(s.getRepayDate());
            OOperaInfo oOperaInfo = omap.get(s.getOid());
            String serialNo = "RP" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + oOperaInfo == null ? null : oOperaInfo.getOsn()
                    + StringUtil.RadomCode(4, "other").toUpperCase() + "0001";
            rp.setSerialNo(serialNo);
            rp.setStatus(RepaymentPlanStatusEnum.UN_PAID.getKey());
            rp.setTotalFee(rp.getCurrentFee());
            rp.setTotalTimes(1);
            rp.setUid(s.getUid());
            rp.setCallRepayStatus("0");
            rplist.add(rp);
            int effCount = settlementDao.updateRdByDateAndUid(startDate, endDate, rp.getUid(), s.getId(),TradingTypeEnum.CREDIT.getKey());
            logger.error("更新交易记录表行数：[{}]", effCount);
        }
        settlementDao.batchSaveTbRepaymentPlan(rplist);
        resultMap.put("repaymentCount",rplist.size());

//        // 循环发送还款信息
//        for (TbRepaymentDetail list1 : dlist) {
//            MessageBean message = new TbRepaymentMessage(list1.getTotalCorpus().toString(),
//                    DateUtil.format(list1.getRepayDate(), "MM-dd"));
//            SmsClSupport.send(userInfoService.get(list1.getUid()).getMobile(), message.getContent());
//        }
        return listResult;
    }

    public static void main(String [] args)
    {
        long arr[] ={0,1,2,3,4,5,6,7,8,9};
        long a = 2;
        long b = 3;
        System.out.print(a/b+"  "+a%b);
    }

}
