package com.tcxf.hbc.daemon.service.impl;

import com.tcxf.hbc.common.entity.TbRepay;
import com.tcxf.hbc.daemon.service.ITbRepayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tcxf.hbc.daemon.dao.SettlementDao;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
@Service
public class TbRepayServiceImpl implements ITbRepayService {

    @Autowired
    private SettlementDao settlementDao;
    @Override
    public List<TbRepay> findOverFifteenMsRepayBill() {
         return settlementDao.findOverFifteenMsRepayBill();
    }

    @Override
    public void updateById(List<TbRepay> list) {
        settlementDao.updateById(list);
    }
}
