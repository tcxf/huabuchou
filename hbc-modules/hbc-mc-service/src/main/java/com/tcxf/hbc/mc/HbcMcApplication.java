package com.tcxf.hbc.mc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhouyj
 * @date 2018/3/23
 */
@EnableAsync
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.tcxf.hbc.mc", "com.tcxf.hbc.common"})
public class HbcMcApplication {
    public static void main(String[] args) {
        SpringApplication.run(HbcMcApplication.class, args);
    }
}