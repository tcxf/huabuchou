package com.tcxf.hbc.mc.common.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TXInterfaceUtil {

    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    /**
     * 获取token
     * @return
     */
    /**
     * 通过okhttp发送post请求获取token值
     *
     * @return 授权码
     */
    public static String getAccessTokenByOkhttp(String account,String signature,String url) {
        //拼接请求体
        Map<String, String> params = new HashMap<>();
        params.put("account", account);
        params.put("signature", signature);
        //拼接请求头
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json;charset=UTF-8");
        String result = OkHttpUtil.postMethodHeader(url, params, headers);
        return result;
    }

    /**
     * 获取token
     * token有效时间为第一次获取后24小时内有效，并非每次都需要获取
     *
     * @return accessToken
     */
    private static String getTokenByOkHttp(String account,String signature,String url) {
        //获取token信息
        String accessToken =getAccessTokenByOkhttp(account,signature,url) ;
        JsonObject jsonObject = new JsonParser().parse(accessToken).getAsJsonObject();
        String token = jsonObject.get("data").getAsJsonObject().get("accessToken").getAsString();
        return token;
    }

    /**
     * 发送okHttp请求获得json结果
     *
     * @param params 参数
     * @return json结果
     */
    public static String getResultByOkHttp(HashMap<String, Object> params, int count)throws Exception {
        String result = null;
        //token并非每次都需要获取，可将未过期的token重复使用
        String token = getTokenByOkHttp(params.get("account").toString(), params.get("signature").toString(), params.get("tokenurl").toString());
//        String token = "";
        System.out.println("**************token :" + token + "**************");
        try {
            Response response = getResponseByOkHttp(params, token, params.get("url").toString());
            System.out.println("**************response :" + response + "**************");
            if (response.isSuccessful()) {
                result = response.body().string();
            } else {
                //如果token过期,重新获取token
                if (response.code() == 401) {
                    token = getTokenByOkHttp(params.get("account").toString(), params.get("signature").toString(), params.get("tokenurl").toString());
                    response = getResponseByOkHttp(params, token,params.get("url").toString());
                    result = response.body().string();
                } else {
                    Thread.sleep(500);
                    count ++;
                    if (count < 4)
                        getResultByOkHttp(params, count);

                    System.out.println("实际返回code为：" + response.code());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        JsonObject jsonObject = null;
//        String success = "false";
//        if(result != null && !"".equals(result)){
//            jsonObject = new JsonParser().parse(result).getAsJsonObject();
//            success = jsonObject.get("success").toString();
//        }

        return result;
    }

    /**
     * 发送okHttp请求获得Response
     *
     * @param params      参数
     * @param accessToken 授权码
     * @return 响应结果
     */
    public static Response getResponseByOkHttp(Map<String, Object> params, String accessToken,String url) {
        url = url + "&accessToken=" + accessToken;
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json;charset=UTF-8");
        Response response = OkHttpUtil.getMethodHeader(url, headers);
        return response;
    }
}
