package com.tcxf.hbc.mc.controller.amerchantPc.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.vo.AdminVo;
import com.tcxf.hbc.mc.service.IPAdminService;
import com.tcxf.hbc.mc.service.ITbPeopleRoleService;
import com.tcxf.hbc.mc.service.ITbRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 运营商端 管理员模块
 *
 */
@RestController
@RequestMapping("/mAdmin")
public class MAdminController {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    @Autowired
    public IPAdminService adminService;
    @Autowired
    public ITbRoleService iTbRoleService;

    @Autowired
    public ITbPeopleRoleService iTbPeopleRoleService;



    /**
     * 根据条件分页查询
     * @return
     */
    // @RequestMapping(value="/findAdminList",method ={RequestMethod.POST,RequestMethod.GET} )
    @RequestMapping(value = "/findAdminList/{mid}/{page}/{name}",method = RequestMethod.POST)
    public Page findAdminList(@PathVariable("mid") String mid, @PathVariable("page") String page,@PathVariable("name") String name){
        R r = new R();
        Map<String, Object> params = new HashMap<>();
        params.put("page",page);
        params.put("name",name);
        //获取登陆的id
        params.put("aid",mid);
        Page page1 = adminService.findAdminCondition(params);
//        r.setData(page1);
        return  page1;
    }

    /**
     * 添加管理员
     * @param admin
     * @return
     */
    @RequestMapping(value = "/insertAdmin",method = RequestMethod.POST)
    public R insertAdmin(@RequestBody PAdmin admin){
        R r = new R();
        try {
            admin.setCreateDate(new Date());
            admin.setModifyDate(new Date());
            String pwd =   ENCODER.encode(admin.getPwd());
            admin.setPwd(pwd);
            admin.setUiType(4);
            Boolean bo = adminService.insert(admin);
            if(bo){
                r.setMsg("添加成功");
            }else{
                r.setMsg("添加失败");
            }

        }catch (Exception e){
            r.setMsg("添加失败");
        }
        return r;
    }



    /**
     *跳转到修改管理员页面
     * @return
     */
    @RequestMapping(value = "/updateAdmin/{id}",method = RequestMethod.POST)
    public R updateAdmin(@PathVariable("id") String id){
        R r = new R();
        PAdmin admin = adminService.selectById(id);
        r.setData(admin);
        return r;
    }

    /**
     * 根据id修改管理信息
     * @param admin
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(@RequestBody PAdmin admin){
        R r = new R();
        if(admin.getPwd()==null){
         PAdmin   a =     adminService.selectById(admin.getId());
         admin.setPwd(a.getPwd());
        }else {
            String pwd = ENCODER.encode(admin.getPwd());
            admin.setPwd(pwd);
        }
        Boolean bo = adminService.updateById(admin);
        if(bo){
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return  r;
    }

    /**
     *
     * 根据id删除管理
     * @param
     * @return
     */
    @RequestMapping(value = "/deleteById/{id}",method = RequestMethod.POST)
    public R deleteById( @PathVariable("id") String id ){
        R r = new R();
        Boolean bo = adminService.deleteById(id);
        if(bo){
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 跳转到分配角色页面
     * @return
     */
    @RequestMapping(value = "/selectRole/{aid}",method = RequestMethod.POST)
    public R selectRole(@PathVariable("aid") String aid){
        //查询该默认管理员下的角色
        R r = new R();
        List<TbRole> list = iTbRoleService.selectList(new EntityWrapper<TbRole>().eq("uid",aid));
        r.setData(list);
        return r;
    }

    /**
     * 根据管理id查询该管理员的角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/selectAdminRoleStr/{id}",method = RequestMethod.POST)
    public R selectAdminRoleStr(@PathVariable("id") String id){
        R r = new R();
        List<TbPeopleRole> tList = iTbPeopleRoleService.selectList(new EntityWrapper<TbPeopleRole>().eq("people_user_id",id));
        r.setData(tList);
        return r;


    }

    /**
     * 根据管理员id分配角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/distribution/{id}/{ids}",method = RequestMethod.POST)
    public R distribution(@PathVariable("id") String id,@PathVariable("ids") String ids) {
        R r = new R();
        try {
            TbPeopleRole tbPeopleRole = iTbPeopleRoleService.selectOne(new EntityWrapper<TbPeopleRole>().eq("people_user_id", id));
            iTbPeopleRoleService.delete(new EntityWrapper<TbPeopleRole>().eq("people_user_id",id));
            String [] ids_ = ids.split(",");
            for (String str: ids_){
                TbPeopleRole tbPeopleRole1 = new TbPeopleRole();
                tbPeopleRole1.setPeopleUserId(id);
                tbPeopleRole1.setPeopleUserType("1");
                tbPeopleRole1.setRoleId(str);
                iTbPeopleRoleService.insert(tbPeopleRole1);
            }
            r.setMsg("分配角色成功");

        }catch (Exception e){
            r.setMsg("分配角色失败");
        }
        return  r;

    }
}
