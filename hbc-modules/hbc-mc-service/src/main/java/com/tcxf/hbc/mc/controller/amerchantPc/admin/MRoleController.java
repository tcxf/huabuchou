package com.tcxf.hbc.mc.controller.amerchantPc.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.service.IPAdminService;
import com.tcxf.hbc.mc.service.ITbResourcesService;
import com.tcxf.hbc.mc.service.ITbRoleResourcesService;
import com.tcxf.hbc.mc.service.ITbRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 运营商端 角色模块
 *
 */
@RestController
@RequestMapping("/mRole")
public class MRoleController {
    @Autowired
    public ITbRoleService iTbRoleService;

    @Autowired
    public ITbResourcesService iTbResourcesService;

    @Autowired
    public ITbRoleResourcesService iTbRoleResourcesService;

    @Autowired
    public IPAdminService adminService;



    @RequestMapping(value = "/roleManagerList/{page}/{aid}",method = RequestMethod.POST)
    public Page roleManagerList(@PathVariable("page")String page,@PathVariable("aid") String aid){
        R r = new R();
        //查询该管理员下的所有角色
        Page page1 = iTbRoleService.selectPage(page,aid);
        return  page1;
    }

    @RequestMapping(value = "/insertRole",method = RequestMethod.POST)
    public R insertRole(@RequestBody TbRole role){
        R r = new R();
        role.setUtype("1");
        role.setCreateDate(new Date());
        role.setModifyDate(new Date());
        Boolean bo = iTbRoleService.insert(role);
        if(bo){
            r.setMsg("添加成功");
        }else{
            r.setMsg("添加失败");
        }
        return r;
    }

    /**
     *
     * 跳转到修改角色管理页面
     * @return
     */
    @RequestMapping(value = "/updateRole/{id}",method = RequestMethod.POST)
    public R updateRole(@PathVariable("id") String id){
        R r = new R();
        TbRole role = iTbRoleService.selectById(id);
        r.setData(role);
        return  r;
    }

    /**
     * 根据id修改角色信息
     * @param role
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(@RequestBody TbRole role){
        R r = new R();
        TbRole role1 = iTbRoleService.selectById(role.getId());
        role1.setRoleName(role.getRoleName());
        role1.setRemark(role.getRemark());
        Boolean bo =iTbRoleService.updateById(role);
        if(bo){
            r.setMsg("修改成功");
        }else{
            r.setMsg("修改失败");
        }
        return  r;

    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById/{id}",method = RequestMethod.POST)
    public R deleteById(@PathVariable("id") String id){
        R r = new R();
        Boolean bo = iTbRoleService.deleteById(id);
        if(bo){
            r.setMsg("删除成功");
        }else{
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 进入分配资源页面
     * @return
     */
    @RequestMapping(value = "/selectResources",method = RequestMethod.POST)
    public  R selectResources(){
        R r = new R();
        Map<String,Object> map1 = new HashMap<>();
        map1.put("resource_type","2");
        map1.put("type","4");
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","1");
        map.put("type","4");
        //查询默认管理员下的所有资源路径和模块
        List<TbResources> pList  = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map1));
        List<TbResources> list  = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        Map<String,Object> mm = new HashMap<>();
        mm.put("list",list);
        mm.put("pList",pList);
        r.setData(mm);
        return r;
    }

    /**
     * 查询该管理员的资源权限
     * @param id
     * @return
     */
    @RequestMapping(value = "/loadRoleResourcesStr/{id}",method = RequestMethod.POST)
    public  R loadRoleResourcesStr(@PathVariable("id") String id){
        R r = new R();
        List<TbRoleResources> tList = iTbRoleResourcesService.selectList(new EntityWrapper<TbRoleResources>().eq("role_id",id));
        r.setData(tList);
        return  r;

    }
    @RequestMapping(value = "/distribution/{id}/{ids}",method = RequestMethod.POST)
    public R distribution(@PathVariable("id") String id,@PathVariable("ids") String ids){
        R r = new R();
        if(id == null){
            r.setMsg("不存在角色id");
        }else {
            if (ids == null ||"".equals(ids)) {
                r.setMsg("请选择权限");
                r.setCode(1);
            }else{
                //清楚所有的权限
                iTbRoleResourcesService.role(id,ids);
                r.setMsg("分配权限成功");
            }
        }
        return  r;
    }
}
