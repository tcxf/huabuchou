package com.tcxf.hbc.mc.controller.amerchantPc.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRoleResources;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.vo.ResourcesVo;
import com.tcxf.hbc.mc.service.ITbResourcesService;
import com.tcxf.hbc.mc.service.ITbRoleResourcesService;
import com.xiaoleilu.hutool.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sanxi
 * @Date :Created in 17:23 2018/6/15
 * 运营商 资源模块
 *
 */
@RestController
@RequestMapping("/mRe")
public class MTbResourcesController {
    @Autowired
    public ITbResourcesService iTbResourcesService;
    @Autowired
    public ITbRoleResourcesService iTbRoleResourcesService;

    @Autowired
    public FastFileStorageClient fastFileStorageClient;

    @Autowired
    public FdfsPropertiesConfig fdfsPropertiesConfig;

    /**
     * 跳转到资源管理页面
     * @return
     */
    @RequestMapping("/r")
    public R r(){
        R r = new R();
        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",4);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        r.setData(tList);
        return r;
    }

    /**
     * 根据条件查询资源分页
     * @return
     */
    @RequestMapping(value = "/findResources/{page}/{name}/{pid}",method = RequestMethod.POST)
    public Page findResources(@PathVariable("page") String page,@PathVariable("name") String name,@PathVariable("pid") String pid) {
        Map<String,Object> map = new HashMap<>();
        map.put("page",page);
        if(!"null".equals(pid)) {
            if ("0".equals(pid)) {
                map.put("resourceType", "2");
                map.put("parentId", null);
            } else {
                map.put("parentId", pid);
                map.put("resourceType", null);
            }
        }else {
            map.put("parentId", null);
            map.put("resourceType", null);
        }
        map.put("resourceName",name);
        map.put("type",4);
        Page page1 = iTbResourcesService.getPage(map);
        return page1;
    }

    /**
     * 根据id删除资源信息
     * @param
     * @return
     */
    @RequestMapping(value = "/deleteById/{id}",method = RequestMethod.POST)
    public R deleteById(@PathVariable("id") String id){
        R r = new R<>();
        List<TbRoleResources> ro =  iTbRoleResourcesService.selectList(new EntityWrapper<TbRoleResources>().eq("resource_id",id));
        if(ro.size()==0){
            Boolean se = iTbResourcesService.deleteById(id);
            r.setMsg("删除成功");
        }else{
            r.setMsg("有角色关联该资源路径，请先取消关联在来删除");
        }
        return r;
    }

    /**
     * 跳转到修改资源页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/updateResources/{id}",method = RequestMethod.POST)
    public R updateResources(@PathVariable("id") String id){
        R r = new R();
        //查询要修改的资源信息
        TbResources resources = iTbResourcesService.selectById(id);
        //查询所有模块信息

        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",4);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        Map<String,Object> map1 = new HashMap<>();
        map1.put("resources",resources);
        map1.put("tList",tList);
        r.setData(map1);
        return  r;
    }
    /**
     * 根据id修改资源信息
     * @param vo
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public R updateById(@RequestBody TbResources vo){
        R r = new R();
        ModelAndView mode = new ModelAndView();
        boolean se = iTbResourcesService.updateById(vo);

        return r;
    }

    /**
     * 跳转到资源添加页面
     * @return
     */
    @RequestMapping(value = "/AddResources",method = RequestMethod.POST)
    public R AddResources(){
        R r = new R();
        //查询所有模块
        Map<String,Object> map = new HashMap<>();
        map.put("resource_type","2");
        map.put("type",4);
        List<TbResources> tList = iTbResourcesService.selectList(new EntityWrapper<TbResources>().allEq(map));
        Map<String,Object> map1 = new HashMap<>();
        map1.put("tList",tList);
       r.setData(map1);
        return r;
    }

    /**
     * 添加资源
     * @param vo
     * @return
     */
    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public R insert(@RequestBody TbResources vo){

        vo.setCreateDate(new Date());
        vo.setModifyDate(new Date());
        R r = new R();
        ModelAndView mode = new ModelAndView();
        vo.setType(4);
        boolean se = iTbResourcesService.insert(vo);
        return r;
    }

    /**
     * 文件上传
     *
     * @param file
     * @return
     */

    @PostMapping("/otherInfo/upload")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
//            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }
}