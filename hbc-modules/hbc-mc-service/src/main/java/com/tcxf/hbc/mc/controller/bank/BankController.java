package com.tcxf.hbc.mc.controller.bank;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;
import com.tcxf.hbc.mc.service.ICUserOtherInfoService;
import com.tcxf.hbc.mc.service.ITbBindCardInfoService;
import com.tcxf.hbc.mc.service.ITbUserAccountService;
import com.tcxf.hbc.mc.service.ITbUserAccountTypeConService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bank")
public class BankController {

    @Autowired
    public ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    public ICUserOtherInfoService icUserOtherInfoService;
    @Autowired
    public ITbUserAccountService iTbUserAccountService;
    @Autowired
    public ITbUserAccountTypeConService iTbUserAccountTypeConService;
    @Autowired
    private ThreelementController threelementController;

    /**
     * 通过账户表ID查询所有绑卡信息 用户端
     * @param oid
     * @return
     */
    @RequestMapping(value = "/findUbank/{oid}/{type}" , method = RequestMethod.POST)
    public R<List<TbBindCardInfo>> findUbank(@PathVariable("oid") String oid,@PathVariable("type") String type){
        Map<String, Object> params = new HashMap<>();
        params.put("oid", oid);
        params.put("type", type);
        List<TbBindCardInfo> tbBindCardInfos = iTbBindCardInfoService.QueryUAllBank(params);
        return R.<List<TbBindCardInfo>>newOK(tbBindCardInfos);
    }

    /**
     * 通过账户表ID查询所有绑卡信息 商户PC端
     * @param oid
     * @return
     */
    @RequestMapping(value = "/findMbank/{oid}/{type}" , method = RequestMethod.POST)
    public Page findMbank(@PathVariable("oid") String oid,@PathVariable("type") String type){
        Map<String, Object> params = new HashMap<>();
        params.put("oid", oid);
        params.put("type", type);
        Page page = iTbBindCardInfoService.QueryMAllBank(params);
        return page;
    }

    /**
     * 通过绑卡表ID删除绑卡
     * @param id
     * @return
     */
    @RequestMapping(value = "/delbank/{id}" , method = RequestMethod.POST)
    public R delbank(@PathVariable("id") String id){
        R r = new R<>();
        boolean b = iTbBindCardInfoService.delete(new EntityWrapper<TbBindCardInfo>().eq("id", id));
        if (b){
            r.setCode(1);
            r.setMsg("删除成功");
        }else {
            r.setCode(0);
            r.setMsg("删除失败");
        }
        return r;
    }

    /**
     * 添加银行卡
     * @return
     */
    @RequestMapping("/savebank")
    @ResponseBody
    public R savebank(@RequestBody TbBindCardInfo tbBindCardInfo) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("oid", tbBindCardInfo.getOid());
        map.put("bankCard", tbBindCardInfo.getBankCard());
        TbBindCardInfo infos = iTbBindCardInfoService.QueryBank(map);
        if (!ValidateUtil.isEmpty(infos) && infos.getContractType().equals(TbBindCardInfo.CONTRACTTYPE_3)) {
            return R.newErrorMsg("不能重复绑卡");
        }
        if (!ValidateUtil.isEmpty(infos) && infos.getContractType().equals(tbBindCardInfo.getContractType())) {
            return R.newErrorMsg("不能重复绑卡");
        }
        if (!ValidateUtil.isEmpty(infos) && !infos.getContractType().equals(tbBindCardInfo.getContractType())) {
            infos.setContractType(TbBindCardInfo.CONTRACTTYPE_3);
            if(tbBindCardInfo.getContractType().equals(TbBindCardInfo.CONTRACTTYPE_2)){
                infos.setContractId(tbBindCardInfo.getContractId());
            }
            iTbBindCardInfoService.updateById(infos);
            return R.newOK();
        }

        R r = threelementController.threelementCheck(tbBindCardInfo);
        if (r.getCode()!=0){
            return r;
        }
        UserInfoDto userInfoDto = iTbUserAccountService.selectUserInfo(tbBindCardInfo.getOid());
        CUserOtherInfo userOtherInfo = icUserOtherInfoService.selectOne(new EntityWrapper<CUserOtherInfo>().eq("uid", userInfoDto.getcUserInfo().getId()));
        tbBindCardInfo.setOid(tbBindCardInfo.getOid());
        tbBindCardInfo.setUtype(userInfoDto.getcUserInfo().getUserAttribute());
        boolean b1 = iTbBindCardInfoService.insert(tbBindCardInfo);
        if (b1) {
            if (userOtherInfo.getBindCardStatus().equals("0")) {
                userOtherInfo.setBindCardStatus("1");
                icUserOtherInfoService.updateById(userOtherInfo);
            }
            return R.newOK("绑卡成功",tbBindCardInfo);
        }else{
            return R.newErrorMsg("绑卡失败");
        }
    }
}
