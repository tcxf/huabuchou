package com.tcxf.hbc.mc.controller.bank;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.mc.common.util.TXInterfaceUtil;
import com.tcxf.hbc.mc.service.IBaseInterfaceInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @program: Workspace2
 * @description: 银行卡三要素接口
 * @author: JinPeng
 * @create: 2018-08-04 19:14
 **/
@RestController
@RequestMapping("/threelement")
public class ThreelementController extends BaseController {

    @Autowired
    private IBaseInterfaceInfoService iBaseInterfaceInfoService;

    /**
     * @Description: 银行卡三要素检测
     * @Param: [creditVo]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/8/4
    */
    @RequestMapping(value = "/threelementCheck" , method = RequestMethod.POST)
    @ApiOperation(value = "银行卡三要素检测", notes = "银行卡三要素检测")
    public R threelementCheck(@RequestBody TbBindCardInfo tbBindCardInfo){
        try {
            String idCard = tbBindCardInfo.getIdCard();
            String userName = tbBindCardInfo.getName();
            String mobile = tbBindCardInfo.getMobile();
            //银行卡
            String accountNO = tbBindCardInfo.getBankCard();

            HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();

            String account = ibfismap.get("account").toString();
            logger.info("**************account:" + account + "**************");
            String signature = ibfismap.get("signature").toString();
            logger.info("**************signature:" + signature + "**************");
            String tokenurl = ibfismap.get("tianXingTokenUrl").toString();
            logger.info("**************tokenurl:" + tokenurl + "**************");

            //银行卡三要素验证
            String threelementp = "account=" + account + "&accountNO=" + accountNO + "&name=" + userName + "&idCard=" + idCard;
            String url = ibfismap.get("threelementUrl").toString();
            logger.info("**************threelementp:" + threelementp + "**************");
            String threelementurl = ibfismap.get("threelementUrl").toString() + threelementp;
            logger.info("**************threelementurl:" + threelementurl + "**************");

            HashMap<String, Object> threeMap = new HashMap<String, Object>();
            threeMap.put("account", account);
            threeMap.put("signature", signature);
            threeMap.put("url", threelementurl);
            threeMap.put("tokenurl", tokenurl);

            //银行卡三要素返回结果
            String threelementresult = TXInterfaceUtil.getResultByOkHttp(threeMap, 1);
            logger.info("银行卡三要素返回结果:" + threelementresult);

            if (ValidateUtil.isEmpty(threelementresult)) {
                return R.newErrorMsg("接口返回空");
            }
            JsonObject threelementresuljson = new JsonParser().parse(threelementresult).getAsJsonObject();

            //接口返回成功（这里我没做处理，你们自己根据返回的数据做处理）
            if ("false".equals(threelementresuljson.get("success").getAsString())) {
                return R.newError();
            }

            if ("验证失败,信息不匹配".equals(threelementresuljson.get("data").getAsJsonObject().get("result").getAsString())){
                return R.newErrorMsg(threelementresuljson.get("data").getAsJsonObject().get("result").getAsString());
            }

            return R.newOK();
        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newErrorMsg(e.getMessage());
        }
    }






    /**
     * @Description: 运营商三要素检测
     * @Param: [creditVo]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/8/4
     */
    @RequestMapping(value = "/operatorsCheck" , method = RequestMethod.POST)
    @ApiOperation(value = "运营商三要素检测", notes = "运营商三要素检测")
    public R operatorsCheck(@RequestBody CreditVo creditVo){

        try {
            String idCard = creditVo.getIdCard();
            String userName = creditVo.getUserName();
            String mobile = creditVo.getMobile();

            HashMap<String, Object> ibfismap = iBaseInterfaceInfoService.getBaseInterfaceInfoList();

            String account = ibfismap.get("account").toString();
            logger.info("**************account:" + account + "**************");
            String signature = ibfismap.get("signature").toString();
            logger.info("**************signature:" + signature + "**************");
            String tokenurl = ibfismap.get("tianXingTokenUrl").toString();
            logger.info("**************tokenurl:" + tokenurl + "**************");

            //运营商三要素验证
            String operatorsp = "account=" + account + "&mobile=" + mobile + "&name=" + userName + "&idCard=" + idCard;
            String operatorsurl = ibfismap.get("operatorsUrl") + operatorsp;
            logger.info("**************operatorsp:" + operatorsp + "**************");
            logger.info("**************operatorsurl:" + operatorsurl + "**************");

            HashMap<String, Object> threeMap = new HashMap<String, Object>();
            threeMap.put("account", account);
            threeMap.put("signature", signature);
            threeMap.put("url", operatorsurl);
            threeMap.put("tokenurl", tokenurl);

            //运营商三要素返回结果
            String operatorsresult = TXInterfaceUtil.getResultByOkHttp(threeMap, 1);
            logger.info("运营商三要素返回结果:" + operatorsresult);

            if (ValidateUtil.isEmpty(operatorsresult)) {
                return R.newErrorMsg("接口返回空");
            }
            JsonObject operatorsresuljson = new JsonParser().parse(operatorsresult).getAsJsonObject();

            //接口返回成功（这里我没做处理，你们自己根据返回的数据做处理）
            if ("false".equals(operatorsresuljson.get("success").getAsString())) {
                return R.newError();
            }
            return R.newOK();
        }catch (Exception e){
            logger.info(e.getMessage());
            return R.newErrorMsg(e.getMessage());
        }
    }

}
