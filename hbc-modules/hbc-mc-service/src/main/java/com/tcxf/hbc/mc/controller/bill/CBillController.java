package com.tcxf.hbc.mc.controller.bill;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.util.secure.DES;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;
import com.tcxf.hbc.mc.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author YWT_tai
 * @Date :Created in 20:43 2018/7/12
 */
@RestController
@RequestMapping("/consumerBill")
public class CBillController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ITbRepaymentPlanService iTbRepaymentPlanService;

    @Autowired
    private ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    private ITbTradingDetailService iTbTradingDetailService;

    @Autowired
    private ITbAccountInfoService iTbAccountInfoService;

    @Autowired
    private ITbRepayLogService iTbRepayLogService;

    @Autowired
    private ICUserRefConService iCUserRefConService;

    @Autowired
    private ITbRepayService iTbRepayService;

    @Autowired
    private IOOperaInfoService iOOperaInfoService;


    /**
     * @Description: 查询运营商姓名
     * @Param: [uid, oid]
     * @return: com.tcxf.hbc.common.util.R
     * @Author: JinPeng
     * @Date: 2018/7/20
     */
    @RequestMapping(value="/getOoperaInfoNameByid/{oid}",method ={RequestMethod.POST} )
    public R<String> getOoperaInfoNameByid(@PathVariable String oid){

            OOperaInfo oOperaInfo = iOOperaInfoService.selectById(oid);

            return R.newOK(oOperaInfo.getName());
    }



        /**
         * @Description: 查询代还款账单和未出帐账单所有信息
         * @Param: [uid, oid]
         * @return: com.tcxf.hbc.common.util.R
         * @Author: JinPeng
         * @Date: 2018/7/20
         */
    @RequestMapping(value="/findAvailableCreditInfo/{uid}/{oid}",method ={RequestMethod.POST} )
    public R<HashMap<String, Object>> findAvailableCreditInfo(@PathVariable String uid, @PathVariable String oid){

        HashMap<String, Object> resultMap = new HashMap<>();
        try {

            HashMap<String, Object> hmap = new HashMap<>();
            hmap.put("uid", uid);
            hmap.put("oid", oid);

            Map<String, Object> totalMoneymap = new HashMap<>();
            totalMoneymap.put("uid", uid);
            totalMoneymap.put("oid", oid);

            TbAccountInfo tbAccountInfo = iTbAccountInfoService.getTbAccountInfoByUid(hmap);
            String balance = "0";
            if (!ValidateUtil.isEmpty(tbAccountInfo)) {
                if (!ValidateUtil.isEmpty(tbAccountInfo.getBalance())) {
                    //获取可用额度
                    BigDecimal bl = new Calculate(DES.decryptdf(tbAccountInfo.getBalance())).format(2, RoundingMode.DOWN);
                    balance = bl.toString();
                }
            }

            //查询消费者待还总金额
            Map<String,Object> totalMoneyMap=iTbRepaymentPlanService.findTotalWaitPayMoney(totalMoneymap);

            Map<String, Object> paramMap = new HashMap<>();

            //查询本月未出账单的金额
            paramMap.put("currentMonth",DateUtil.getDateTimeforMonth(new Date()));
            paramMap.put("uid",uid);
            paramMap.put("oid", oid);

            BigDecimal totalMoney=iTbTradingDetailService.findUnOutTotalMoney(paramMap);

            resultMap.put("totalMoneyMap",totalMoneyMap);
            resultMap.put("totalMoney",totalMoney);
            resultMap.put("balance",balance);
        }catch (Exception e){
            return R.newErrorMsg(e.getMessage());
        }
        return R.newOK(resultMap);

    }


    /**findMineWaitPayInfo
     * @Author:YWT_tai
     * @Description
     * @Date: 11:26 2018/7/13
     * 查询消费者已出账单 待还所有信息
     */
    @RequestMapping(value="/findMineWaitPayInfo/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findMineWaitPayInfo(@PathVariable String uid,@PathVariable String oid){
        R r = new R();
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("uid",uid);
            paramMap.put("oid",oid);
            //分期按钮
            boolean planFlag=false;

            //立即还款按钮
            boolean payFlag=false;

            //查询该用户的最后还款日
            CUserRefCon cUserRefCon = iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid", uid).eq("oid", oid));

            //查询消费者待还总金额
            Map<String,Object> totalMoneyMap=iTbRepaymentPlanService.findTotalWaitPayMoney(paramMap);

            //查询用户逾期未还记录
            List<Map<String,Object>> list =iTbRepaymentPlanService.findExsitOverdue(paramMap);

            // 分期: 用户逾期记录 + 当月还款计划  (waitpay 0:未还 = 滞纳金+利息+本金 1:待还=利息+本金)
            List<Map<String,Object>> payList = new ArrayList<>();

            //未分期/未分期：已出账单的交易记录 以及分页信息
            List<TbTradingDetail> totalTradeList = new ArrayList<>();
            Page<TbTradingDetail> pageinfo = new Page<>();

            //查询当月还款分期计划
            paramMap.put("currentMonth",DateUtil.getDateTimeforMonth(new Date()));
            payList= iTbRepaymentPlanService.findCurrentMonthPayPlan(paramMap);
       /* if(null != cmMap){
            payList.add(cmMap);
        }*/
            if(null !=list && list.size()>0){  //有逾期未还记录
                for (Map<String,Object> mmp:list) {
                    paramMap.put("planId", mmp.get("planId"));
                    Map<String,Object> overduePlanMap=iTbRepaymentPlanService.findPlanOverDueBill(paramMap);  // 有分期/无分期  :查询逾期未还分期记录
                    if(null != overduePlanMap){
                        payList.add(overduePlanMap);
                    }
                }
            } else{  //无逾期未还记录
                String settlementId=iTbRepaymentPlanService.findCurrentMonthSetllmentId(paramMap); //查询当月还款表的主键id
                if (null !=settlementId && !"".equals(settlementId)){
                    //pageinfo = iTbTradingDetailService.selectPage(new Page<>(page, limit), new EntityWrapper<TbTradingDetail>().eq("rdid", settlementId)); //无分期:查询当月已出账单交易记录
                    //totalTradeList = pageinfo.getRecords();
                    totalTradeList = iTbTradingDetailService.selectList(new EntityWrapper<TbTradingDetail>().eq("rdid", settlementId));//无分期:

                    if(null!=totalTradeList && totalTradeList.size()>0){    //判断是否存在记录  是则存在分期按钮
                        planFlag=true;
                    }
                }
            }
            int TradeSize = totalTradeList.size();
            int paySize = payList.size();
            if(TradeSize>0 || paySize>0){  //判断大小是否大于0 是则存在立即还款按钮
                payFlag=true;
            }

            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("totalMoneyMap",totalMoneyMap);//待还总金额
            resultMap.put("payList",payList);//分期: 用户逾期记录 + 当月还款计划
            resultMap.put("totalTradeList",totalTradeList);//账单交易分页
            resultMap.put("planButton",planFlag);//是否存在分期按钮
            resultMap.put("payButton",payFlag);//是否存在还款按钮
            resultMap.put("totalSize",TradeSize+paySize);//总条数
            resultMap.put("lastPayDate",DateUtil.getDateTimeforSingleMonth(new Date())+"-"+cUserRefCon.getRepayDate());//最后还款日
            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findMineWaitPayInfo:"+e.getMessage());
            return R.newError();
        }
        return r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:26 2018/7/13
     * 点击待还或者逾期还款分期详情
     */
    @RequestMapping(value="/findPlanDetailInfo/{planId}/{waitpay}",method ={RequestMethod.POST} )
    @ResponseBody
    public R< Map<String,Object>> findPlanDetailInfo(@PathVariable String planId,@PathVariable String waitpay){
        R< Map<String,Object>> r = new R();
        try {
            Map<String,Object> resultMap=iTbRepaymentPlanService.findPlanDetailInfo(planId);
            if("0".equals(waitpay)){       //逾期:计算逾期的天数
                int margin = DateUtil.getMargin(DateUtil.getDateTime(), (String) resultMap.get("repaymentDate"));
                resultMap.put("lateDay",margin);
            }
            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findPlanDetailInfo:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 19:30 2018/7/16
     * 查询未出账单
     */
    @RequestMapping(value="/findUnOutBill/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findUnOutBill(@PathVariable String uid,@PathVariable String oid){
        R r = new R<>();
        try {
            Map<String, Object> resultMap = new HashMap<>();
            Map<String, Object> paramMap = new HashMap<>();

            //查询未出账单明细
            List<TbTradingDetail> unOutPageInfo = iTbTradingDetailService.
                    selectList( new EntityWrapper<TbTradingDetail>().
                            eq("uid", uid).eq("trading_type","0").eq("oid",oid).isNull("rdid").isNull("rid").like("trading_date", DateUtil.getDateTimeforMonth(new Date())));

            //查询提前还款明细
            List<TbTradingDetail> advancePageInfo = iTbTradingDetailService.
                    selectList( new EntityWrapper<TbTradingDetail>().
                            eq("uid", uid).eq("oid",oid).isNotNull("rid").like("trading_date", DateUtil.getDateTimeforMonth(new Date())));

            //查询本月未出账单的金额
            paramMap.put("currentMonth",DateUtil.getDateTimeforMonth(new Date()));
            paramMap.put("uid",uid);
            paramMap.put("oid",oid);
            BigDecimal totalMoney=iTbTradingDetailService.findUnOutTotalMoney(paramMap);

            //查询提前
            BigDecimal advanceMoney=iTbTradingDetailService.findAdvanceTotalMoney(paramMap);

            resultMap.put("unOutPageInfo",unOutPageInfo);
            resultMap.put("advancePageInfo",advancePageInfo);
            resultMap.put("totalMoney",totalMoney);
            resultMap.put("advanceMoney",advanceMoney);
            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findUnOutBill:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:07 2018/7/17
     * 查询分期明细
     */
    @RequestMapping(value="/findDivideDetail/{id}",method ={RequestMethod.POST} )
    public R findDivideDetail(@PathVariable String id) {
        R r = new R<>();
        try {
            Map<String, Object> paramMap = new HashMap<>();
            Map<String, Object> resultMap = new HashMap<>();
            //查询分期明细
            List<Map<String, Object>> list = iTbRepaymentPlanService.findDivideDetail(id);

            //查询剩余分期总额
            BigDecimal RemainPlanMoney=iTbRepaymentPlanService.findRemainPlanMoney(id) ;

            //查询本期应还
            paramMap.put("id",id);
            BigDecimal CurrentMonthMoney=iTbRepaymentPlanService.findCurrentMonthMoney(paramMap);
            CurrentMonthMoney=  CurrentMonthMoney==null ? new BigDecimal("0"):CurrentMonthMoney;

            resultMap.put("planList",list);
            resultMap.put("RemainPlanMoney",RemainPlanMoney);
            resultMap.put("CurrentMonthMoney",CurrentMonthMoney);
            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findDivideDetail:"+e.getMessage());
            return R.newError();
        }
        return r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 17:48 2018/7/13
     * 查询交易记录的详情
     */
    @RequestMapping(value="/findTradeDetailInfo/{id})",method ={RequestMethod.POST} )
    public R findTradeDetailInfo(@PathVariable String id){
        R r = new R<>();
        try {
            TbTradingDetail tbTradingDetail = iTbTradingDetailService.selectOne(new EntityWrapper<TbTradingDetail>().eq("id", id));
            r.setData(tbTradingDetail);
        } catch (Exception e) {
            logger.error("findTradeDetailInfo:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }



    /**
     * 查询分期账单
     * @param uid
     * @return
     */
    @RequestMapping(value="/findPlanIsSplitInfo/{uid})",method ={RequestMethod.POST} )
    public R findPlanIsSplitInfo(@PathVariable String uid){
        R r = new R();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, -1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date repaymentDate = c.getTime();
        HashMap<String,Object> map =new HashMap<String,Object>();
        map.put("uid",uid);
        map.put("repaymentDate",repaymentDate);
        List<TbRepaymentPlan> list=  iTbRepaymentPlanService.findExSplitRepayList(map);
        r.setData(list);
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:22 2018/7/19
     * 查询分期账单的年份
     */
    @RequestMapping(value="/findPlanYears/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findPlanYears(@PathVariable String uid,@PathVariable String oid){
        R r = new R<>();
        try {
            Map<String, Object> paramMap = new HashMap<>();
            Map<String, Object> resultMap = new HashMap<>();
            paramMap.put("uid",uid);
            paramMap.put("oid",oid);
            List<Map<String,Object>> list=  iTbRepaymentDetailService.findPlanYears(paramMap);
            //查询剩余分期总额以及当前应还总额
            Map<String,Object> mmp= iTbRepaymentPlanService.findRemainPlanAndCurrentMoney(paramMap);

            resultMap.put("list",list);
            resultMap.put("mmp",mmp);
            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findPlanYears:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:11 2018/7/19
     * 根据年份查询分期的总金额  findPlanBillByYear
     */
    @RequestMapping(value="/findPlanBillByYear/{dateYear}/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findPlanBillByYear(@PathVariable String dateYear,@PathVariable String uid,@PathVariable String oid){
        R r = new R<>();
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("dateYear",dateYear);
            map.put("uid",uid);
            map.put("oid",oid);
            List<Map<String,Object>> list=  iTbRepaymentDetailService.findPlanBillByYear(map);
            r.setData(list);
        } catch (Exception e) {
            logger.error("findPlanBillByYear:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:22 2018/7/19
     * 查询历史账单的年份
     */
    @RequestMapping(value="/findHistoryBillYears/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findHistoryBillYears(@PathVariable String uid,@PathVariable String oid){
        R r = new R<>();
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("uid",uid);
            paramMap.put("oid",oid);
            List<String> list=  iTbTradingDetailService.findUserTradeYear(paramMap);
            r.setData(list);
        } catch (Exception e) {
            logger.error("findHistoryBillYears:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 10:45 2018/7/19
     *根据年份查询历史账单
     */
    @RequestMapping(value="/findHistoryBillByYear/{dateYear}/{uid}/{oid}",method ={RequestMethod.POST} )
    public R findHistoryBillByYear(@PathVariable String dateYear,@PathVariable String uid,@PathVariable String oid){
        R r = new R<>();
        try {
            List<TbRepaymentDetailDto> NotExactLists= new ArrayList<TbRepaymentDetailDto>();//初始化值
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("uid",uid);
            paramMap.put("oid",oid);
            paramMap.put("currentYear",dateYear);
            //提前还款数量
            int advanceNum = iTbRepayLogService.selectCount(new EntityWrapper<TbRepayLog>().
                    eq("uid", uid).like("create_date", DateUtil.getDateTimeforYear(new Date())).eq("type","2"));

            //按期还款数量
           int ontimeNum = iTbRepaymentDetailService.selectCount(new EntityWrapper<TbRepaymentDetail>().
                    eq("uid", uid).eq("oid",oid).like("create_date", DateUtil.getDateTimeforYear(new Date())));
            /*int ontimeNum = iTbRepayService.selectCount(new EntityWrapper<TbRepay>().eq("status","1").
                    eq("pay_type","1").eq("uid", uid).eq("oid", oid).like("pay_date", DateUtil.getDateTimeforYear(new Date())));*/


            //判断该用户当前年份是否全是提前还款
            if(advanceNum>0 && ontimeNum==0){
                NotExactLists= iTbRepayLogService.findAllAdvancePay(paramMap);
            }
            //不全是提前
            else if(ontimeNum > 0){
                NotExactLists=iTbRepaymentDetailService.findNotExactlyAdvancePay(paramMap);
            }
            r.setData(NotExactLists);
        } catch (Exception e) {
            logger.error("findHistoryBillByYear:",e);
            return R.newError();
        }
        return  r;
    }


    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 15:56 2018/7/19
     * 查询历史账单的每月的详情
     */
    @RequestMapping(value="/findHistoryBillDetailInfo/{isSplit}/{uid}/{id}/{oid}",method ={RequestMethod.POST} )
    public R findHistoryBillDetailInfo(@PathVariable("isSplit")String isSplit, @PathVariable("uid")String uid, @PathVariable("id") String id,@PathVariable String oid){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        R r = new R<>();
        try {
            Map<String, Object> resultMap = new HashMap<>();

            //提前还款list
            List<TbRepayLog> advancePayList = new ArrayList<TbRepayLog>();

            //查询当月分期账单总和 已还清
            TbRepaymentDetail tbRepaymentDetail = new  TbRepaymentDetail();

            //查询未分期账单总和
            TbRepaymentPlan unPlantbRepaymentDetail = new  TbRepaymentPlan();

            //当月的交易记录
            List allList = new ArrayList<TbTradingDetail>();
            List<TbTradingDetail> tradeList = new ArrayList<TbTradingDetail>();

            //查询当月分期账单总和 已还清
            if("1".equals(isSplit) && !"0".equals(id)){
                BigDecimal bigDecimal = new BigDecimal("0");
                tbRepaymentDetail = iTbRepaymentDetailService.selectOne(new EntityWrapper<TbRepaymentDetail>().eq("id",id));
                if (tbRepaymentDetail!=null){
                    BigDecimal repayCorpus = tbRepaymentDetail.getRepayCorpus();
                    BigDecimal repayFee = tbRepaymentDetail.getRepayFee();
                    BigDecimal repayLateFee = tbRepaymentDetail.getRepayLateFee();
                    BigDecimal totalMoney = bigDecimal.add(repayCorpus==null ? new BigDecimal("0"):repayCorpus).add(repayFee==null ? new BigDecimal("0"):repayFee).add(repayLateFee==null ? new BigDecimal("0"):repayLateFee);
                    tbRepaymentDetail.setRepayCorpus(totalMoney);//分期还款总金额
                }
            }else if("0".equals(isSplit) && !"0".equals(id)){
                  BigDecimal bigDecimal = new BigDecimal("0");
                  TbRepaymentPlan  dbPlantbRepaymentDetail = iTbRepaymentPlanService.selectOne(new EntityWrapper<TbRepaymentPlan>().eq("rd_id",id));
                     if(dbPlantbRepaymentDetail!=null && "1".equals(dbPlantbRepaymentDetail.getStatus())){
                         BigDecimal repayCorpus = dbPlantbRepaymentDetail.getCurrentCorpus();
                         BigDecimal repayLateFee = dbPlantbRepaymentDetail.getLateFee();
                         BigDecimal totalMoney = bigDecimal.add(repayCorpus==null ? new BigDecimal("0"):repayCorpus).add(repayLateFee==null ? new BigDecimal("0"):repayLateFee);
                         unPlantbRepaymentDetail.setCurrentCorpus(totalMoney);//未分期还款总金额
                         unPlantbRepaymentDetail.setId(dbPlantbRepaymentDetail.getId());
                         unPlantbRepaymentDetail.setStatus(repayLateFee==null|| Calculate.equalsZero(repayLateFee)? "1":"0"); //1 :为非逾期未分期还款 0:逾期未分期还款
                     }
            }

            //查询当月的提前还款
            advancePayList = iTbRepaymentDetailService.findExRepayLogList(uid, DateUtil.getDateTimeforMonth(new Date()));

            //查询当月的提前还款对应的交易记录
            if(advancePayList.size()>0){
                for (TbRepayLog tbRepayLog:advancePayList) {
                    String ids = tbRepayLog.getTids();
                    String[] split = ids.split(",");
                    if(split.length!=0){
                        for (int i = 0; i < split.length; i++) {
                            String s = split[i];
                            if(!"".equals(s )){
                                List<TbTradingDetail> list=iTbTradingDetailService.selectList(new EntityWrapper<TbTradingDetail>().eq("id",s).orderBy("trading_date",false));
                                allList.addAll(list);
                            }
                        }
                    }
                }
            }

            //当月的交易记录
            tradeList = iTbTradingDetailService.selectList(new EntityWrapper<TbTradingDetail>().
                    eq("uid", uid).eq("oid",oid).eq("payment_status", 1).eq("rdid",id).orderBy("trading_date",false));

            allList.addAll(tradeList);

            Collections.sort(allList, new Comparator<TbTradingDetail>() {
                @Override
                public int compare(TbTradingDetail o1, TbTradingDetail o2){
                    int i = o2.getTradingDate().compareTo(o1.getTradingDate());
                    return i;
                }
            });
            resultMap.put("tbRepaymentDetail",tbRepaymentDetail);
            resultMap.put("unPlantbRepaymentDetail",unPlantbRepaymentDetail);
            resultMap.put("advancePayList",advancePayList);
            resultMap.put("tradeList",allList);

            r.setData(resultMap);
        } catch (Exception e) {
            logger.error("findHistoryBillDetailInfo:",e);
            return R.newError();
        }
        return  r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:09 2018/7/30
     * 立即还款
    */
    @RequestMapping(value="/PaybackMoney",method ={RequestMethod.POST} )
    public R PaybackMoney(@RequestBody PaybackMoneyDto paybackMoneyDto){
        R r = new R<>();
        try {
            //插入还款记录 tb_repay 以及 还款明细 tb_repay_log
            iTbRepayLogService.addinitPayBackInfo(paybackMoneyDto);


        } catch (Exception e) {
            logger.error("PaybackMontb_repay_logey:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 18:10 2018/8/8
     * 查询提前还款的金额
    */
    @RequestMapping(value="/findAdvancePaybackTotalMoney/{uid}/{oid}",method ={RequestMethod.POST} )
    public R<Map<String, Object>> findAdvancePaybackTotalMoney( @PathVariable("uid")String uid, @PathVariable("oid")String oid){
        try {
            Map<String, Object> map = new HashMap<>();
            //查询未出账单明细
            List<TbTradingDetail> unOutPageInfo = iTbTradingDetailService.
                    selectList( new EntityWrapper<TbTradingDetail>().
                            eq("uid", uid).eq("trading_type","0").eq("oid",oid).isNull("rdid").isNull("rid").like("trading_date", DateUtil.getDateTimeforMonth(new Date())));

            BigDecimal totalMoney = new BigDecimal("0");
            for (TbTradingDetail tbTradingDetail:unOutPageInfo) {
                totalMoney=tbTradingDetail.getActualAmount().add(totalMoney);
            }
            map.put("totalMoney",totalMoney);
            map.put("tradeList",unOutPageInfo);
            return R.newOK(map);
        } catch (Exception e) {
            logger.error("findAdvancePaybackTotalMoney:"+e.getMessage());
            return R.newError();
        }
    }




}
