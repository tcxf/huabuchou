package com.tcxf.hbc.mc.controller.cUserInfo;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.CUserInfoDto;
import com.tcxf.hbc.mc.service.ICUserInfoService;
import com.tcxf.hbc.mc.service.ITbAreaService;
import com.tcxf.hbc.mc.service.ITbUserAccountService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    private ICUserInfoService icUserInfoService;
    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    @Autowired
    private ITbAreaService iTbAreaService;
    @Autowired
    private MessageUtil messageUtil;

    @RequestMapping(value = "/getUserById/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "根据userId查询用户信息", notes = "根据userId查询用户信息")
    public R<CUserInfoDto> selectUserInfo(@PathVariable String id) {
        try {
            CUserInfoDto cUserInfoDto = icUserInfoService.selectUserInfo(id);
            TbArea tbArea = iTbAreaService.selectOne(new EntityWrapper<TbArea>().eq("id", cUserInfoDto.getArea()));
            if (ValidateUtil.isEmpty(tbArea)) {
                return R.<CUserInfoDto>newOK(cUserInfoDto);
            }
            cUserInfoDto.setDisPlayName(tbArea.getDisplayName());
            return R.<CUserInfoDto>newOK(cUserInfoDto);
        } catch (Exception ex) {
            return R.newError(ex.getMessage());
        }
    }

    @RequestMapping(value = "/updateUserInfo/{accountId}", method = RequestMethod.POST)
    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
    public R updateUserInfo(@PathVariable String accountId , @RequestBody CUserInfoDto cUserInfoDto) {
        try {
            boolean b = icUserInfoService.updateUserInfoDetal(accountId , cUserInfoDto);
        } catch (Exception e)
        {
            return R.newError(e.getMessage());
        }
        return R.newOK();
    }

    @RequestMapping(value = "/updatePaymentCode/{id}/{paymentCode}", method = RequestMethod.POST)
    @ApiOperation(value = "修改支付密码", notes = "修改支付密码")
    public R updatePaymentCode(@PathVariable String id, @PathVariable String paymentCode) {
        try {
            TbUserAccount tbUserAccount = iTbUserAccountService.selectById(id);
            tbUserAccount.setTranPwd(ENCODER.encode(paymentCode));
            iTbUserAccountService.updatePaymentCode(tbUserAccount);
        }catch (Exception e)
        {
            return R.newError(e.getMessage());
        }
        return R.newOK();
    }

    @RequestMapping(value = "/selectMobile/{id}/{mobile}/{yCode}", method = RequestMethod.POST)
    @ApiOperation(value = "修改支付密码短信验证", notes = "修改支付密码短信验证")
    public R selectMobile(@PathVariable String id ,@PathVariable String mobile , @PathVariable String yCode)
    {
        TbUserAccount tbUserAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("mobile" , mobile));
        if (ValidateUtil.isEmpty(tbUserAccount))
        {
            return R.newErrorMsg("手机号错误");
        }
        if (!tbUserAccount.getId().equals(id))
        {
            return R.newErrorMsg("手机号错误");
        }
        if (!messageUtil.valiCode(mobile, yCode, MessageUtil.YZM)) {
            return R.newErrorMsg("验证码错误");
        }
        return R.newOK();
    }

    @ApiOperation(value = "忘记密码验证码验证", notes = "忘记密码验证码验证")
    @RequestMapping(value = "/yzForgetPassword/{mobile}/{yzm}", method = RequestMethod.POST)
    public R yzForgetPassword(@PathVariable String mobile , @PathVariable String yzm)
    {
        if (!messageUtil.valiCode(mobile, yzm, MessageUtil.YZM)) {
            return R.newErrorMsg("验证码错误");
        }
        TbUserAccount tbUserAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("mobile" , mobile));
        if (ValidateUtil.isEmpty(tbUserAccount))
        {
            return R.newErrorMsg("手机号码错误");
        }
        return R.newOK();
    }

    @RequestMapping(value = "/userSetPassword/{id}/{password}/{oid}", method = RequestMethod.POST)
    @ApiOperation(value = "用户立即开店", notes = "用户立即开店")
    public R<String> userSetPassword(@PathVariable String id , @PathVariable String password , @PathVariable String oid)
    {
        try {
            String mid = iTbUserAccountService.userSetPassword(id, password , oid);
            if (mid.equals("请勿重复提交")){
                return R.newErrorMsg(mid);
            }
            return R.newOK(mid);
        }catch (Exception e){
            return R.newError();
        }
    }

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @ApiOperation(value = "忘记密码修改密码", notes = "忘记密码修改密码")
    @RequestMapping(value = "/updatePassword/{mobile}/{password}", method = RequestMethod.POST)
    public R updatePassword(@PathVariable String mobile , @PathVariable String password)
    {
        try {
            TbUserAccount tbUserAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("mobile", mobile));
            tbUserAccount.setPassword(ENCODER.encode(password));
            iTbUserAccountService.updateById(tbUserAccount);
        }catch (Exception e){
            return R.newErrorMsg("修改密码失败");
        }
        return R.newOK();
    }
}
