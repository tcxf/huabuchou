package com.tcxf.hbc.mc.controller.coupon;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.IMMerchantInfoService;
import com.tcxf.hbc.mc.service.IMMerchantOtherInfoService;
import com.tcxf.hbc.mc.service.IMRedPacketSettingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商户优惠券
 */
@RestController
@RequestMapping("/coupon")
public class MCouponController {
    @Autowired
    public IMRedPacketSettingService imRedPacketSettingService;

    @Autowired
    public IMMerchantInfoService imMerchantInfoService;

    @Autowired
    public IMMerchantOtherInfoService imMerchantOtherInfoService;

    /**
     * 分页查询商户优惠券
     *
     * @return
     */
    @RequestMapping(value = "/findMlist", method = {RequestMethod.POST})
    public Page findMlist(@RequestBody Map<String, Object> map) {
        Page mlist = imRedPacketSettingService.findMlist(map);
        return mlist;
    }

    @RequestMapping(value = "/mtype/{mid}",method = {RequestMethod.POST})
    public String mtype(@PathVariable String mid){
       MMerchantInfo  mm = imMerchantInfoService.selectById(mid);
        MMerchantOtherInfo mmo = imMerchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",mid));
        String type = "0";
        if("1".equals(mm.getStatus()) && "3".equals(mmo.getAuthExamine())){
             type = "1";
        }
        return type;
    }

    /**
     * 查询商户下的优惠券
     *
     * @return
     */
    @ApiOperation(value = "商户发布得优惠券", notes = "商户发布得优惠券",httpMethod ="POST")
    @RequestMapping(value = "/couponList/{mid}", method = {RequestMethod.POST})
    public R couponList(@PathVariable String mid) {
        R r = new R();
        try {
            List<RedSettingDto>   list  = imRedPacketSettingService.findList(mid);
            if(list.size()!=0) {
                for (int i = 0; i < list.size() - 1; i++) {
                    for (int j = list.size() - 1; j > i; j--) {
                        if (list.get(j).getId().equals(list.get(i).getId())) {
                            list.remove(j);
                        }
                    }
                }
            }
            for (RedSettingDto dto:list ) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateTime = sdf.format(date);
                date = DateUtil.stringDate(dateTime);
                String dateTime1 = sdf.format(dto.getEndDate());
                dto.setEndDate(DateUtil.stringDate(dateTime1)) ;
                if((date.getTime()-dto.getEndDate().getTime()>0?true:false)){
                    //失效
                    dto.setCouponType("0");

                }else{
                    //已过期
                    dto.setCouponType("1");
                }
            }
            r.setData(list);
            return r;

        }catch (Exception e){
            return R.newErrorMsg("暂无优惠券");
        }
//        1-10 *2 +5* 50 +1768  + 1769 - 出生年
    }

    /**
     * 创建优惠券
     *
     * @param mRedPacketSetting
     * @return
     */
    @ApiOperation(value = "创建优惠券", notes = "创建优惠券",httpMethod ="POST")
    @RequestMapping(value = "/couponInsert", method = {RequestMethod.POST})
    public R couponInsert(@RequestBody MRedPacketSetting mRedPacketSetting) {
        R r = new R();
        try {
            mRedPacketSetting.setCreateDate(new Date());
            MMerchantInfo mm = imMerchantInfoService.selectById(mRedPacketSetting.getMiId());
            mRedPacketSetting.setOid(mm.getOid());
            mRedPacketSetting.setEndDate(mRedPacketSetting.getModifyDate());
            if (mRedPacketSetting.getTimeType()!=1) {
                mRedPacketSetting.setEndDate(DateUtil.getDaydiffDate(mRedPacketSetting.getCreateDate(), mRedPacketSetting.getDays()));
            }
            if (imRedPacketSettingService.insert(mRedPacketSetting)) {
                r.setMsg("添加成功");
            } else {
                r.setMsg("添加失败");
            }
        } catch (Exception e) {
            return R.newErrorMsg("发生错误");
        }
        return r;
    }

    /**
     * 编辑优惠券页面
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑优惠券页面", notes = "编辑优惠券",httpMethod ="POST")
    @RequestMapping(value = "/couponUpdate/{id}", method = {RequestMethod.POST})
    public R couponUpdate(@PathVariable String id) {
        R r = new R();
        r.setData(imRedPacketSettingService.selectById(id));
        return r;

    }

    /**
     * 修改优惠券
     *
     * @param mRedPacketSetting
     * @return
     */
    @ApiOperation(value = "修改优惠券", notes = "修改优惠券",httpMethod ="POST")
    @RequestMapping(value = "/couponUpdateById", method = {RequestMethod.POST, RequestMethod.GET})
    public R couponUpdateById(@RequestBody MRedPacketSetting mRedPacketSetting) {
        R r = new R();
        MRedPacketSetting mme = imRedPacketSettingService.selectById(mRedPacketSetting.getId());
        if (mRedPacketSetting.getTimeType()!=1) {
            mme.setEndDate(DateUtil.getDaydiffDate(mme.getCreateDate(), mRedPacketSetting.getDays()));
        }else{
            mme.setEndDate(mRedPacketSetting.getModifyDate());
        }
        mme.setTimeType(mRedPacketSetting.getTimeType());
        mme.setName(mRedPacketSetting.getName());
        mme.setMoney(mRedPacketSetting.getMoney());
        mme.setFullMoney(mRedPacketSetting.getFullMoney());
        mme.setDays(mRedPacketSetting.getDays());
        try {
            if (imRedPacketSettingService.updateById(mme)) {
                r.setMsg("修改成功");
            } else {
                r.setMsg("修改失败");
            }
        } catch (Exception e) {
            return R.newErrorMsg("发生错误");
        }
        return r;
    }

    /**
     * 使优惠券失效
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "使优惠券失效", notes = "使优惠券失效",httpMethod ="POST")
    @RequestMapping(value = "/couponFailure/{id}", method = {RequestMethod.POST})
    public R couponFailure(@PathVariable String id) {
        R r = new R();
        try {
            MRedPacketSetting mRedPacketSetting = imRedPacketSettingService.selectById(id);
            mRedPacketSetting.setStatus("3");
            if (imRedPacketSettingService.updateById(mRedPacketSetting)) {
                r.setMsg("优惠券已失效");
            } else {
                r.setMsg("优惠券失效失败");
            }

        } catch (Exception e) {
            return R.newErrorMsg("发生错误");
        }
        return r;
    }

}
