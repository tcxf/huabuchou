package com.tcxf.hbc.mc.controller.coupon;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.dto.RedPacketDto;
import com.tcxf.hbc.mc.service.IMRedPacketService;
import com.tcxf.hbc.mc.service.IMRedPacketSettingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * 用户优惠券
 */
@RestController
@RequestMapping("/Ucoupon")
public class UCouponController {
    @Autowired
    public IMRedPacketService imRedPacketService;

    @Autowired
    public IMRedPacketSettingService imRedPacketSettingService;

    /**
     * 查询用户领取的优惠券
     * @param id
     * @return
     */
    @ApiOperation(value = "查询用户领取的优惠券", notes = "查询用户领取的优惠券",httpMethod ="POST")
    @RequestMapping(value = "/coupon/{id}",method = {RequestMethod.POST})
    public R findList(@PathVariable String id){
        R r = new R();
        List<RedPacketDto> list = imRedPacketService.findByUserId(id);
        for (RedPacketDto dto:list ) {
            Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateTime = sdf.format(date);
        date = DateUtil.stringDate(dateTime);
        String dateTime1 = sdf.format(dto.getEndDate());
        dto.setEndDate(DateUtil.stringDate(dateTime1)) ;
        if((date.getTime()-dto.getEndDate().getTime()>0?true:false)){
            //已过期
            dto.setCouponType("0");

        }else{
            //可使用
            dto.setCouponType("1");
        }
        }
        r.setData(list);
       return r;

    }

    /**
     * 用户领取优惠券
     * @param id
     * @param userId
     * @return
     */
    @ApiOperation(value = "用户领取优惠券", notes = "用户领取优惠券",httpMethod ="POST")
    @RequestMapping(value = "/getCoupon/{id}/{userId}",method = RequestMethod.POST)
    public R getCoupon(@PathVariable String id,@PathVariable  String userId){
        R r = new R();
        try {
             MRedPacketSetting redPacketSetting = imRedPacketSettingService.selectById(id);
            if(redPacketSetting!=null) {
                MRedPacket red = new MRedPacket();
                red.setRpsId(id);
                red.setName(redPacketSetting.getName());
                red.setMiId(redPacketSetting.getMiId());
                red.setSubTitle(redPacketSetting.getSubTitle());
                red.setIsUse("0");
                red.setUserId(userId);
                red.setOid(redPacketSetting.getOid());
                //计算失效时间
                if (1==redPacketSetting.getTimeType()) {
                    red.setEndDate(redPacketSetting.getEndDate());
                } else {
                    red.setEndDate(DateUtil.getDaydiffDate(redPacketSetting.getCreateDate(), redPacketSetting.getDays()));
                }
                red.setCreateDate(new Date());
                red.setMoney(redPacketSetting.getActualMoney());
                red.setFullMoney(redPacketSetting.getFullMoney());
                if (imRedPacketService.insert(red)) {
                    r.setMsg("领取成功");
                } else {
                    r.setMsg("领取失败");
                }

            }else{
                r.setMsg("优惠券不存在，领取失败");
            }
        }catch (Exception e){
            return R.newErrorMsg("发生错误");
        }
        return r;
    }
}
