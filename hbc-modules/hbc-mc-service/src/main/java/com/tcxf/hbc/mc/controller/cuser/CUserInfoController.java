package com.tcxf.hbc.mc.controller.cuser;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.Assert;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.mc.service.ICUserInfoService;
import com.tcxf.hbc.mc.service.ITbUserAccountService;
import com.xiaoleilu.hutool.io.FileUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-26
 */
@RestController
@RequestMapping("/cUserInfo")
public class CUserInfoController extends BaseController {
   /* @Autowired
    private AIBaseCreditConfigService baseCreditConfigService;*/
    @Autowired
    private ICUserInfoService cUserInfoService;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    @Autowired
    private ITbUserAccountService iTbUserAccountService;

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return CUserInfo
     */
    @GetMapping("/{id}")
    public CUserInfo get(@PathVariable Integer id)
    {
        return cUserInfoService.selectById(id);
    }

    @GetMapping("/getUserById/{id}")
    @ApiOperation(value = "根据userId查询用户信息", notes = "根据userId查询用户信息")
    @ResponseBody
    public CUserInfo getUserInfo(@PathVariable String id)
    {
        return cUserInfoService.getUserInfo(id);
    }

    @GetMapping("/delUserInfo/{id}")
    @ApiOperation(value = "根据userId删除用户信息", notes = "根据userId删除用户信息")
    @ResponseBody
    public R<Boolean> delUserInfo(@PathVariable String id)
    {
        cUserInfoService.delUserInfo(id);
        return new R<>(true);
    }

    @GetMapping("/selectUserInfoPage/{status}")
    @ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息")
    @ApiImplicitParam(name = "status", value = "用户状态", required = true, dataType = "String", paramType = "path")
    @ResponseBody
    public Page selectUserInfoPage(@PathVariable String status)
    {
        Page page = cUserInfoService.queryList(status);
       // List<BaseCreditConfig> list = baseCreditConfigService.getListByType("1");
        return page;
    }


    /**
     * 分页查询信息
     *
     * @param params 分页对象
     * @return 分页对象
     */
    @RequestMapping(value="/page",method = RequestMethod.POST)
    @ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息",httpMethod ="POST")
    public Page page(@RequestParam Map<String, Object> params)
    {
        // params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        return cUserInfoService.selectPage(new Query<CUserInfo>(params), new EntityWrapper <CUserInfo>());
    }

    /**
     * 添加用户信息
     * @param  cUserInfo  实体
     * @return success/false
     */
    @RequestMapping(value="/add",method = RequestMethod.POST)
    @ApiOperation(value = "新增用户信息", notes = "新增用户信息",httpMethod ="POST")
    public R<Boolean> add(@RequestBody CUserInfo cUserInfo)
    {
        Assert.validateEntity(cUserInfo);
        boolean result = cUserInfoService.insert(cUserInfo);
        return new R<>(result);
    }


    /**
     * 编辑
     * @param  cUserInfo  实体
     * @return success/false
     */
    @PutMapping
    public Boolean edit(@RequestBody CUserInfo cUserInfo)
    {
        cUserInfo.setModifyDate(new Date());
        return cUserInfoService.updateById(cUserInfo);
    }

    @PostMapping("/uploadFile")
    public Map<String, String> upload1() {
        File file = new File("d://timg.jpg");
        String fileExt = FileUtil.extName(file);
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
            ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
            System.out.println("Available bytes:" + in.available());
            byte[] temp = new byte[1024];
            int size = 0;
            while ((size = in.read(temp)) != -1) {
                out.write(temp, 0, size);
            }
            in.close();

            byte[] content = out.toByteArray();
            StorePath storePath = fastFileStorageClient.uploadFile(content, fileExt);

            resultMap.put("filename", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
        } catch (IOException e) {
            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }


    @PostMapping("/upload")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filename", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
        } catch (IOException e) {
            logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }


    @RequestMapping("/getOne")
    public CUserInfo getOne(@RequestBody CUserInfo cUserInfo){
        CUserInfo userInfo = cUserInfoService.selectOne(new EntityWrapper<CUserInfo>());
        return userInfo;
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 19:04 2018/7/23
     * 校验密码
    */
    @RequestMapping(value = "/checkPassWord/{accountId}/{passWord}",method = {RequestMethod.POST})
    public R checkPassWord(@PathVariable("accountId") String accountId,@PathVariable("passWord") String passWord){
        R r = new R<>();
        TbUserAccount tbUserAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("id", accountId));
        String passwordBydataBase = tbUserAccount.getTranPwd();
        PasswordEncoder ENCODER = new BCryptPasswordEncoder();
        boolean b = ENCODER.matches(passWord,passwordBydataBase);
        if(b){
            r.setMsg("分期成功");
        }else {
            r.setMsg("密码错误");
        }
        r.setData(b);
        return r;
    }
}
