package com.tcxf.hbc.mc.controller.divide;

import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.mc.service.ITbRepaymentDetailService;
import com.tcxf.hbc.mc.service.ITbRepaymentRatioDictService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 我要分期
 * jiangyong
 * 2018年7月21日16:50:08
 */
@RestController
@RequestMapping("/divide")
public class DivideController extends BaseController {

    @Autowired
    private ITbRepaymentDetailService iTbRepaymentDetailService;

    @Autowired
    private ITbRepaymentRatioDictService iTbRepaymentRatioDictService;

    /**
     * 查询分期金额
     * @param uid
     * @param oid
     * @param currentData
     * @return
     */
    @RequestMapping(value = "/Querydividemoeny/{uid}/{oid}/{currentData}",method = {RequestMethod.POST})
    @ApiOperation(value = "查询分期金额{uid}用户id/{oid}运营商ID",notes = "查询分期金额{uid}用户id/{oid}运营商ID")
    public R Querydividemoeny(@PathVariable("uid")String uid,@PathVariable("oid")String oid,@PathVariable("currentData")String currentData){
        try {
            Map<String,Object> map=new HashMap <String, Object>();
            map.put("uid",uid);
            map.put("oid",oid);
            map.put("currentData",currentData);
            TbRepaymentDetail repaymentDetailById = iTbRepaymentDetailService.findRepaymentDetailById(map);
            return R.newOK(repaymentDetailById);
        }catch (Exception e){
            if (e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

    /**
     * 更新还款账单状态
     * @return
     */
    @RequestMapping(value = "/updateSplitRepamentDetail/{totalTime}/{totalFee}",method = {RequestMethod.POST})
    public  R updateSplitRepamentDetail(@PathVariable("totalTime") String totalTime,@PathVariable("totalFee") String totalFee){
        R r=new R();
        try {
            Map<String,Object> map=new HashMap <String, Object>();
            iTbRepaymentDetailService.updateSplitRepamentDetail(map);
            r.setMsg("");
        }catch (Exception e){
            e.getMessage();
            r.setMsg("");
        }
        return r;
    }


    /**
     * 选择分期账单
     */
    @RequestMapping(value = "/ByStages/{uid}/{oid}/{currentData}/{fid}/{splitMonth}",method = RequestMethod.POST)
    @ApiOperation(value = "选择分期账单{uid}用户id/{oid}运营商ID/{currentData}时间/{fid}资金端ID/{splitMonth}分期数",notes = "选择分期账单{uid}用户id/{oid}运营商ID/{currentData}时间/{fid}资金端ID/{splitMonth}分期数")
    public R ByStages(@PathVariable("uid")String uid,@PathVariable("oid")String oid,@PathVariable("currentData")String currentData,@PathVariable("fid")String fid,@PathVariable("splitMonth") String splitMonth){
        R r = new R();
        try {
            Map<String,Object> map=new HashMap();
            map.put("uid",uid);
            map.put("oid",oid);
            map.put("currentData",currentData);
            map.put("fid",fid);
            map.put("splitMonth",splitMonth);
            r.setMsg("分期成功");
            r.setCode(R.SUCCESS);
            iTbRepaymentDetailService.updateSplitRepamentDetail(map);
        }catch (Exception e){
            r.setMsg("分期失败");
            r.setCode(R.FAIL);
            e.getMessage();
        }
        return r;
    }


    /**
     *查询分期利率
     */
    @RequestMapping(value = "/findDictMyOid/{oid}/{fid}",method = {RequestMethod.POST})
    public  R findDictMyOid(@PathVariable("oid")String oid,@PathVariable("fid")String fid){
        try {
            Map <String, Object> map = new HashMap <>();
            map.put("oid",oid);
            map.put("fid",fid);
            TbRepaymentRatioDict tbRepaymentRatioDict = iTbRepaymentRatioDictService.findDictMyOid(map);
            return R.newOK(tbRepaymentRatioDict);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

    /**
     * 分期手续费，每月应还
     * @return
     */
    @RequestMapping(value = "/getDiffSplitAmounts/{uid}/{oid}/{fid}/{spiltMon}",method = {RequestMethod.POST})
    @ApiOperation(value = "分期手续费，每月应还{uid}用户id/{oid}运营商ID/{fid}资金端ID/{splitMonth}分期数",notes = "分期手续费，每月应还{uid}用户id/{oid}运营商ID/{fid}资金端ID/{splitMonth}分期数")
    public R getDiffSplitAmounts(@PathVariable("uid") String uid,@PathVariable("oid") String oid,@PathVariable("fid") String fid, @PathVariable("spiltMon") String splitMon){
        try {
            List <TbRepaymentPlan> diffSplitAmount = iTbRepaymentDetailService.getDiffSplitAmount(uid,oid,fid,splitMon);
            return R.newOK(diffSplitAmount);
        }catch (Exception e){
            if (e instanceof  CheckedException){
                return  R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

}
