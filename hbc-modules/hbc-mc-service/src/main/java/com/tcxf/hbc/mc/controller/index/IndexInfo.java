package com.tcxf.hbc.mc.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.BannerDto;
import com.tcxf.hbc.mc.service.ICUserRefConService;
import com.tcxf.hbc.mc.service.IIndexBannerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/index")
public class IndexInfo {
    @Autowired
    private IIndexBannerService iIndexBannerService;
    @Autowired
    private ICUserRefConService icUserRefConService;


    /**
     * banner 图
     * @param id
     * @return
     */

    @RequestMapping(value = "/banner/{id}" , method = RequestMethod.POST)
    @ApiOperation(value = "banner图加载{id}/用户登陆id",notes = "banner图加载{id}/用户登陆id")
    public R QueryBanner(@PathVariable("id") String id){
        try {
            CUserRefCon user  = icUserRefConService.selectOne(new EntityWrapper <CUserRefCon>().eq("uid",id));
            System.out.println(user.getOid());
            List<IndexBanner> list = iIndexBannerService.selectList(new EntityWrapper <IndexBanner>().eq("oid",user.getOid()).orderDesc(Collections.singleton("sort")));
            return R.newOK(list);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return  R.newError();
        }
    }

    /**
     * 未登陆banner图加载
     */
    @RequestMapping(value = "/bannerss",method = {RequestMethod.POST})
    @ApiOperation(value = "未登陆banner图加载",notes = "未登陆banner图加载")
    public R Banners(){
        try {
            List <IndexBanner> sort = iIndexBannerService.selectList(new EntityWrapper <IndexBanner>().orderDesc(Collections.singleton("sort")));
            return R.newError(sort);
        }catch (Exception e){
            if(e instanceof  CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

}
