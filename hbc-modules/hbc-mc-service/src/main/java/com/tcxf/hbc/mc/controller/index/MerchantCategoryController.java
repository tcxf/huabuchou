package com.tcxf.hbc.mc.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.IMMerchantCategoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/merchantCategory")
public class MerchantCategoryController {
    @Autowired
    public IMMerchantCategoryService imMerchantCategoryService;

    /**
     * 查询更多商户行业分类
     * @return
     */
    @RequestMapping(value = "/QueryMerchantCategory",method ={RequestMethod.POST} )
    @ApiOperation(value = "查询更多商户行业分类",notes = "查询更多商户行业分类")
    public R QueryMerchantCategory(){
        try {
            List <MMerchantCategory> mMerchantCategories = imMerchantCategoryService.selectList(new EntityWrapper <MMerchantCategory>().orderAsc(Collections.singleton("order_list")));
            return R.newOK(mMerchantCategories);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

    /**
     * 查询更多商户行业分类
     * @return
     */
    @RequestMapping(value = "/QueryMerchantCategorys",method ={RequestMethod.POST} )
    @ApiOperation(value = "查询更多商户行业分类",notes = "查询更多商户行业分类")
    public R QueryMerchantCategorys(){
        try {
            Map<String, Object> paramMap = new HashMap<>();
            Page page = imMerchantCategoryService.QueryAll(new Query <MMerchantCategory>(paramMap));
            return R.newOK(page);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }
}
