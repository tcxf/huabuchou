package com.tcxf.hbc.mc.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.MHotSearchDto;
import com.tcxf.hbc.mc.service.IMHotSearchService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/hotsearch")
public class MerchantQueryHotsearch {
    @Autowired
    public IMHotSearchService imHotSearchService;

    /**
     * 热门搜索查询
     * @return
     */
    @RequestMapping(value = "/QueryAllHotsearch",method = {RequestMethod.POST})
    @ApiOperation(value = "热门搜索查询",notes = "热门搜索查询")
    public R QueryAllHotsearch(){ ;
        try {
            List <MHotSearch> mHotSearches = imHotSearchService.selectList(new EntityWrapper <MHotSearch>().orderAsc(Collections.singleton("order_list")));
           return R.newOK(mHotSearches);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return  R.newErrorMsg(e.getMessage());
            }
            return  R.newError();
        }
    }
    @RequestMapping(value = "/QueryAllHotsearchName/{name}",method ={RequestMethod.POST})
    @ApiOperation(value = "热门搜索查询/{name}/商户名称",notes = "热门搜索查询/{name}/商户名称")
    public R  QueryAllHotsearchName(@PathVariable("name")String name){
        try {
            Map map=new HashMap();
            map.put("name",name);
            Page page = imHotSearchService.grabbleHandlerNames(new Query <MHotSearchDto>(map));
           return R.newOK(page);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return  R.newErrorMsg(e.getMessage());
            }
                return R.newError();
        }
    }
}
