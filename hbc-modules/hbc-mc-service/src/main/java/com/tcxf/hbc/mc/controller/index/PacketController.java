package com.tcxf.hbc.mc.controller.index;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.mapper.MRedPacketSettingMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 优惠券
 */

@RestController
@RequestMapping("/packets")
public class PacketController {
    @Autowired
    public IMRedPacketSettingService imRedPacketSettingService;

    @Autowired
    public ICUserRefConService icUserRefConService;

    @Autowired
    public IOOperaOtherInfoService ioOperaOtherInfoService;

    @Autowired
    public IMMerchantInfoService imMerchantInfoService;

    @Autowired
    public MRedPacketSettingMapper mRedPacketSettingMapper;


    /**
     * 首页优惠券
     * @return
     */
    @RequestMapping(value = "/loadReadPckes",method ={RequestMethod.POST})
    @ApiOperation(value = "首页优惠券",notes = "首页优惠券")
    public R loadReadPcket(){
        try {
            Map<String, Object> paramMap = new HashMap <>();
            Page querypacket = imRedPacketSettingService.Querypacket(new Query<RedSettingDto>(paramMap));
            return R.newOK(querypacket);
        }catch (Exception e){
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

    /**
     * 首页更多精选优惠券
     * @param micId
     * @return
     */
    @RequestMapping(value = "/queryReadPckess/{micId}/{userId}/{page}",method ={RequestMethod.POST})
    @ApiOperation(value = "首页更多精选优惠券/{micId}商户分类ID/{userId}/用户ID",notes = "首页更多精选优惠券商户分类ID/{userId}/用户ID")
    public R queryReadPckess(@PathVariable("micId") String micId, @PathVariable("userId") String userId,@PathVariable("page") String page){
        try {
            Map<String, Object> paramMap = new HashMap <>();
            paramMap.put("micId",micId);
            paramMap.put("userId",userId);
            paramMap.put("page",page);
            Page querypackets = imRedPacketSettingService.Querypacketss(new Query<RedSettingDto>(paramMap));
            return R.newOK(querypackets);
        }catch (Exception e){
            if (e instanceof CheckedException){
                return  R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }


    /**
     * 未登率优惠卷展示
     */
    @RequestMapping(value = "/NOpacket",method = {RequestMethod.POST})
    @ApiOperation(value = "首页未登陆优惠券",notes = "首页未登陆优惠券")
    @AuthorizationNO
    public R wdlpack(){
        R r=new R();
        try {
            Map<String, Object> paramMap = new HashMap <>();
            Page querypackets = imRedPacketSettingService.Nopacket(new Query<RedSettingDto>(paramMap));
            r.setData(querypackets);
        }catch (Exception e){
            return R.newError(e.getMessage());
        }
        return r;
    }
}
