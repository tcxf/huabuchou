package com.tcxf.hbc.mc.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.LoginDto;
import com.tcxf.hbc.mc.model.dto.UserAccountDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;
import com.tcxf.hbc.mc.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登陆
 * 2018年7月5日14:57:32
 * liaozeyong
 */
@RestController
@RequestMapping("/clogin")
public class LoginController {

    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    @Autowired
    private ICUserInfoService icUserInfoService;
    @Autowired
    private ITbUserAccountTypeConService iTbUserAccountTypeConService;
    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    @Autowired
    private  IPAdminService ipAdminService;
    @Autowired
    private ITbBindCardInfoService iTbBindCardInfoService;

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Autowired
    private IMMerchantInfoService imMerchantInfoService;
    private Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 用户登陆
     * @param loginDto
     * @return
     */
    @RequestMapping(value = "/login" , method = RequestMethod.POST)
    @ApiOperation(value = "登陆", notes = "登陆")
    public R userLogin(@RequestBody LoginDto loginDto) {
        try {
            String id = iTbUserAccountService.login(loginDto);
            return R.newOK(id);
        } catch (Exception ex) {
            if (ex instanceof CheckedException) {
                return R.newErrorMsg(ex.getMessage());
            }
            log.error("MC controller 异常：" + ex);
            return R.newError();
        }
    }

    @RequestMapping(value = "/loginPc",method = RequestMethod.POST)
    @ApiOperation(value = "商户PC端登陆", notes = "商户PC端登陆")
    public R loginPc(@RequestBody LoginDto loginDto){
        try {
            R r = new R();
            String mobile = loginDto.getMobile();
            String pwd = loginDto.getPwd();
            Map<String,Object> map = new HashMap<>();
            MMerchantInfo m = imMerchantInfoService.selectOne(new EntityWrapper<MMerchantInfo>().eq("mobile",mobile));
            if (ValidateUtil.isEmpty(m) ||!ENCODER.matches(loginDto.getPwd(),m.getPwd())) {
                Map<String,Object> umap = new HashMap<>();
                umap.put("user_name",mobile);
                umap.put("ui_type",4);
                PAdmin admin = ipAdminService.selectOne(new EntityWrapper<PAdmin>().allEq(umap));
                if(ValidateUtil.isEmpty(admin) ||!ENCODER.matches(loginDto.getPwd(),admin.getPwd())){
                    r.setMsg("账号或密码错误");
                    r.setCode(1);
                }else {
                    //成功
                  TbUserAccountTypeCon t=  iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("con_id",admin.getAid()));
                  if(ValidateUtil.isEmpty(t)){
                      r.setMsg("账号或密码错误");
                      r.setCode(1);
                  }else {
                      map.put("id",t.getAcctId());
                      map.put("mobile",admin.getId());
                      r.setData(map);
                  }
                }
            }else {
               //成功
                if(!ValidateUtil.isEmpty(m)) {
                    TbUserAccountTypeCon t = iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("con_id", m.getId()));
                    if (ValidateUtil.isEmpty(t)) {
                        r.setMsg("账号或密码错误");
                        r.setCode(1);
                    } else {
                        map.put("id",t.getAcctId());
//                        Map<String,Object> ma = new HashMap<>();
//                        ma.put("type","1");
//                        ma.put("aid",m.getId());
                        map.put("mobile", m.getId());
//                        PAdmin a = ipAdminService.selectOne(new EntityWrapper<PAdmin>().allEq(ma));
//                        if(a==null){
//                            map.put("mobile","1019063742716977154");
//                        }else {
//                            map.put("mobile", a.getId());
//                        }
                        r.setData(map);
                    }
                }else {
                    r.setMsg("账号或密码错误");
                    r.setCode(1);
                }
            }
            return r;
        }catch (Exception e){
            R r= new R();
            r.setMsg("账号或密码错误");
            r.setCode(1);
            return  r;
        }
    }

    @RequestMapping(value = "/addOpenId/{openId}/{mobile}" , method = RequestMethod.POST)
    @ApiOperation(value = "登陆添加openId", notes = "登陆添加openId")
    public R<CUserInfo> AddOpenId(@PathVariable String openId , @PathVariable String mobile)
    {
        try {
            CUserInfo cUserInfo = icUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("mobile",mobile));
            cUserInfo.setWxOpenid(openId);
            icUserInfoService.updateById(cUserInfo);
            return R.<CUserInfo>newOK(cUserInfo);
        }catch (Exception ex){
            log.error("MC Controller 异常" + ex);
            if(ex instanceof CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newError();
        }
    }

    @RequestMapping(value = "/getOpenId/{openId}" , method = RequestMethod.GET)
    @ApiOperation(value = "获取openId", notes = "获取openId")
    public R<CUserInfo> getOpenId(@PathVariable String openId)
    {
        CUserInfo cUserInfo = icUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("wx_openid",openId));
        return R.<CUserInfo>newOK(cUserInfo);
    }

    @RequestMapping(value = "/logout/{accountId}/{userId}" , method = RequestMethod.POST)
    @ApiOperation(value = "退出登陆", notes = "退出登陆")
    public R logout(@PathVariable String accountId,@PathVariable String userId)
    {
        //清空openId
        try {
            iTbUserAccountService.logout(accountId ,userId);
        }catch (Exception ex){
            log.error("MC Controller 异常" + ex);
            if(ex instanceof CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newError();
        }
        return R.newOK();
    }

    @RequestMapping(value = "/getUserAccountInfoByUserId/{id}" , method = RequestMethod.POST)
    @ApiOperation(value = "根据userId获取登陆用户信息", notes = "根据userId获取登陆用户信息")
    public R<UserInfoDto> getUserAccountInfoByUserId(@PathVariable String id)
    {
        try{
            TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("con_id",id).eq("user_type",TbUserAccountTypeCon.USERTYPE_USER));
            if(ValidateUtil.isEmpty(tbUserAccountTypeCon)){
                return R.<UserInfoDto>newError();
            }
            UserInfoDto userInfoDto = iTbUserAccountService.selectUserInfo(tbUserAccountTypeCon.getAcctId());
            return R.<UserInfoDto>newOK(userInfoDto);
        }catch (Exception e){
            log.error("MC controller 异常:" + e);
            return R.newError();
        }
    }

    @RequestMapping(value = "/getUserAccountInfo/{id}" , method = RequestMethod.POST)
    @ApiOperation(value = "获取登陆用户信息", notes = "获取登陆用户信息")
    public R<UserInfoDto> getUserAccountInfo(@PathVariable String id)
    {
        try {
            UserInfoDto userInfoDto = iTbUserAccountService.selectUserInfo(id);
            return R.<UserInfoDto>newOK(userInfoDto);
        }catch (Exception ex){
            log.error("MC Controller 获取登陆用户信息 "+this.getClass()+"异常" + ex);
            if(ex instanceof CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newError();
        }
    }

    @RequestMapping(value = "/getOperaId/{osn:.+}" , method = RequestMethod.POST)
    @ApiOperation(value = "获取运营商信息", notes = "获取运营商信息")
    public R<OOperaInfo> getOperaInfo(@PathVariable String osn)
    {
        try {
            OOperaInfo operaInfo = ioOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("osn",osn));
            TbBindCardInfo tbBindCardInfo = iTbBindCardInfoService.selectTbBindCardInfoByOid(operaInfo.getId());
            return R.<OOperaInfo>newOK(operaInfo);
        }catch (Exception ex){
            log.error("获取运营商信息异常 "+this.getClass()+"" + ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newError();
        }
    }

    @RequestMapping(value = "/getTbBindCardInfoByOid/{id}" , method = RequestMethod.POST)
    @ApiOperation(value = "获取运营商一张绑卡信息", notes = "获取运营商一张绑卡信息")
    public R<TbBindCardInfo> getTbBindCardInfoByOid(@PathVariable String id)
    {
        try {
            TbBindCardInfo tbBindCardInfo = iTbBindCardInfoService.selectTbBindCardInfoByOid(id);
            return R.<TbBindCardInfo>newOK(tbBindCardInfo);
        }catch (Exception ex){
            log.error("获取运营商信息异常 "+this.getClass()+"" + ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newError();
        }
    }
}
