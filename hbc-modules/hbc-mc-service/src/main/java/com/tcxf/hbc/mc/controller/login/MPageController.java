package com.tcxf.hbc.mc.controller.login;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.service.IMMerchantInfoService;
import com.tcxf.hbc.mc.service.ITbResourcesService;
import com.tcxf.hbc.mc.service.ITbRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mPage")
public class MPageController {

    @Autowired
    private ITbRoleService iTbRoleService;
    @Autowired
    private ITbResourcesService iTbResourcesService;
    @Autowired
    private IMMerchantInfoService imMerchantInfoService;

    @RequestMapping(value = "/getRole/{mobile}/{type}" , method = RequestMethod.POST)
    @ApiOperation(value = "商户端获取权限", notes = "商户端获取权限")
    public R<List<TbRole>> getRole(@PathVariable String mobile ,  @PathVariable String type)
    {
        Map<String , Object> map = new HashMap<>();
        map.put("id" , mobile);
        map.put("type" , type);
        List<TbRole> list = iTbRoleService.selectRole(map);
        return R.<List<TbRole>>newOK(list);
    }

    @RequestMapping(value = "/index/{mid}" , method = RequestMethod.POST)
    @ApiOperation(value = "商户端获取权限", notes = "商户端获取权限")
    public R<Map<String , Object>> index(@PathVariable("mid") String mid,@RequestBody List<TbRole> tbRoles){
        R<Map<String , Object>> r = new R();
        Map<String , Object> map = new HashMap<>();
        ModelAndView modelAndView = new ModelAndView();
        MMerchantInfo loginAdmin = imMerchantInfoService.selectOne(new EntityWrapper<MMerchantInfo>().eq("id" , mid));
        //放入菜单
        List<TbResources> menuResources = new ArrayList<TbResources>();
        //放入资源
        Map<String, List<TbResources>> function = new HashMap<String, List<TbResources>>();
        //获取角色资源
        List<TbResources> tbResources = new ArrayList<TbResources>();
        for (TbRole tbRole : tbRoles) {
            //long强转为String
            String id = String.valueOf(tbRole.getId());
            tbResources = iTbResourcesService.selectResources(id,4);//连表查询所有菜单栏
            for (TbResources resources : tbResources)
            {
                //判断为2说明为菜单，1为资源
                if ("2".equals(resources.getResourceType()))
                {
                    menuResources.add(resources);
                } else {
                    List<TbResources> l;
                    //判断为不为模块，没有parentId说明是模块
                    if (resources.getParentId() != null)
                    {
                        l =  function.get(resources.getParentId());
                        if(null == l)
                        {
                            l = new ArrayList<TbResources>();
                        }
                        l.add(resources);
                        function.put(resources.getParentId().toString(),l);
                    }
                }
            }
        }
        map.put("loginAdmin",loginAdmin);
        map.put("functions", function);
        map.put("tbResources",tbResources);
        map.put("menuResources", menuResources);
        r.setData(map);
        return r;
    }
}
