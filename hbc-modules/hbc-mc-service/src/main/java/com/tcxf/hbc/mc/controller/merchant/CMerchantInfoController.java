package com.tcxf.hbc.mc.controller.merchant;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.tcxf.hbc.common.bean.config.FdfsPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.mc.model.dto.NearbyMerchantDto;
import com.tcxf.hbc.mc.model.vo.MerchantVo;
import com.tcxf.hbc.mc.service.*;
import com.xiaoleilu.hutool.io.FileUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
//import retrofit2.http.POST;

import java.awt.geom.Area;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * C端商户店铺资料
 * jangyong
 * 2018年7月12日19:37:54
 */
@RestController
@RequestMapping("/merchat")
public class CMerchantInfoController extends BaseController {
    @Autowired
    private  IMMerchantInfoService imMerchantInfoService;

    @Autowired
    private  IMMerchantCategoryService imMerchantCategoryService;


    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsPropertiesConfig fdfsPropertiesConfig;

    @Autowired
    private ITbUserAccountTypeConService iTbUserAccountTypeConService;

    @Autowired
    private ITbUserAccountService iTbUserAccountService;

    @Autowired
    private ICUserInfoService icUserInfoService;
    /**
     * 店铺资料查询
     * @param id
     * @return
     */
    @RequestMapping(value = "/QueryMerchatInfo/{id}",method = RequestMethod.POST)
    @ApiOperation(value = "店铺资料查询/{id}商户ID",notes = "店铺资料查询/{id}商户ID")
    public R<NearbyMerchantDto> QueryMerchatInfo(@PathVariable("id") String id){
        try {
            NearbyMerchantDto nearbyMerchantDto = imMerchantInfoService.QueryMerchantInfo(id);
            return R.newOK(nearbyMerchantDto)  ;
        }catch (Exception e){
            if(e instanceof CheckedException){
                return  R.newErrorMsg(e.getMessage());
            }
            return R.newError();
        }
    }

    /**
     * 行业类型
     * @return
     */
    @RequestMapping(value = "/QueryCategory",method = RequestMethod.POST)
    @ApiOperation(value = "行业类型",notes = "行业类型")
    public  R QueryCategory(){
        R r=new R() ;
        try {
            List <MMerchantCategory> mMerchantCategories = imMerchantCategoryService.selectList(new EntityWrapper <MMerchantCategory>());
            r.setData(mMerchantCategories);
        }catch (Exception e){
            if(e instanceof  CheckedException){
                return  R.newErrorMsg(e.getMessage()) ;
            }
            return  R.newError();
        }
        return  r;
    }

    /**
     * 文件上传
     * @param file
     * @return
     */

    @PostMapping("/upload")
    @ApiOperation(value = "文件上传",notes = "文件上传")
    public Map<String, String> upload(@RequestParam("file") MultipartFile file) {
        String fileExt = FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(1);
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getBytes(), fileExt);
            resultMap.put("filePath", fdfsPropertiesConfig.getFileHost() + storePath.getFullPath());
            System.out.println(storePath);
        } catch (IOException e) {
          logger.error("文件上传异常", e);
            throw new RuntimeException(e);
        }
        return resultMap;
    }

    /**
     * 修改店铺资料
     *
     */
    @RequestMapping(value = "/updateStore",method = RequestMethod.POST)
    @ApiOperation(value = "修改店铺资料",notes = "修改店铺资料")
    public R updateStore(@RequestBody MerchantVo m) {
        imMerchantInfoService.updateStore(m);
        return R.newOK();
    }

}
