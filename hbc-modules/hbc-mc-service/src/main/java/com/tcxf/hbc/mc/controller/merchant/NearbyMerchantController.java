package com.tcxf.hbc.mc.controller.merchant;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.bean.annotation.AuthorizationNO;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.dto.NearbyMerchantDto;
import com.tcxf.hbc.mc.service.IMMerchantInfoService;
import com.tcxf.hbc.mc.service.IMRedPacketSettingService;
import com.tcxf.hbc.mc.service.ITbAreaService;
import com.tcxf.hbc.mc.service.ITbCollectService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/nearby")
public class NearbyMerchantController {
    @Autowired
    IMMerchantInfoService imMerchantInfoService;

    @Autowired
    IMRedPacketSettingService imRedPacketSettingService;

    @Autowired
    ITbCollectService iTbCollectService;
    @Autowired
    ITbAreaService iTbAreaService;


    /**
     * 附件商家
     * @param lat
     * @param lng
     * @return
     */
    /**
     *查询附近商户
     * @return
     */
    @RequestMapping(value="/findList/{lat}/{lng}/{page}/{micId}",method ={RequestMethod.POST} )
    @ApiOperation(value = "查询附近商家", notes = "查询附近商家",httpMethod ="POST")
    public R findList(@PathVariable String lat, @PathVariable String lng,@PathVariable String page,@PathVariable String micId){
        R r  = new R();
        Map<String,Object> map = new HashMap<>();
        //经纬度为空下防止sql报错
        if("null".equals(lat) || "null".equals(lng)){
            map.put("lat", 0);
            map.put("lng", 0);
        }else {
            map.put("lat", lat);
            map.put("lng", lng);
        }
        map.put("page",page);
        if(!"null".equals(micId)){
            map.put("micId",micId);

        }else{
            map.put("micId",null);
        }
        Page page1 = imMerchantInfoService.findNearByList(map);
        List<NearbyMerchantDto> list =  page1.getRecords();
        if(list.size()!=0) {
            for (NearbyMerchantDto dto : list) {
                //添加最新的优惠券
                Map<String, Object> map1 = new HashMap<>();
                map1.put("mi_id", dto.getId());
                //审核通过的优惠券
                map1.put("status", 2);
                MRedPacketSetting red = imRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().allEq(map1).orderBy("create_date", false));
                if (red != null) {
                    dto.setFullMoney(red.getFullMoney());
                    dto.setMoney(red.getActualMoney());
                } else {
                    dto.setFullMoney(new BigDecimal("0"));
                    dto.setMoney(new BigDecimal("0"));
                }
                //经纬度不传时，返回距离为-1
                if ("null".equals(lat) || "null".equals(lng)) {
                    dto.setDistance("-1m");
                } else {
                    BigDecimal dis = new BigDecimal(dto.getDistance());
                    if (dis.compareTo(new BigDecimal("1000")) >= 0) {
                        dto.setDistance(dis.divide(new BigDecimal(1000), 1, BigDecimal.ROUND_DOWN).toString() + "km");
                    } else {
                        dto.setDistance(dis.setScale(1, BigDecimal.ROUND_DOWN).toString() + "m");
                    }
                }
            }
        }
        r.setData(page1);
        return r;
    }

    /**
     * 根据id查询商户详情
     * @param id
     * @return
     */
    @RequestMapping(value="/findById/{id}/{uid}",method = {RequestMethod.POST})
    @ApiOperation(value = "根据id查询商户", notes = "根据id查询商户",httpMethod ="POST")
    public R findById(@PathVariable String uid,@PathVariable String id){
        R r = new R();
        Map<String,Object> map = new HashMap<>();
        if(!"null".equals(uid)) {
            map.put("uid", uid);
        }
        map.put("mid",id);
        List<TbCollect> tbCollects = iTbCollectService.selectList(new EntityWrapper<TbCollect>().allEq(map));
       NearbyMerchantDto dto = imMerchantInfoService.findById(id);
       dto.setCtype(tbCollects.size());
       r.setData(dto);
       return r;
    }

    /**
     * 查询商户下的优惠券和优惠券的领取状态
     * @param userId
     * @return
     */
    @RequestMapping(value = "/findByUserId/{userId}/{miId}" ,method = RequestMethod.POST)
    @ApiOperation(value = "查询商户下的优惠券和优惠券的领取状态", notes = "查询商户下的优惠券和优惠券的领取状态",httpMethod ="POST")
    public R findByUserId(@PathVariable String userId,@PathVariable String miId){
        Map<String,Object> map = new HashMap<>();
        map.put("miId",miId);
        //未登陆的 userId 为空的情况下全部为领取
        if("null".equals(userId)){
            map.put("userId",null);
        }else {
            map.put("userId",userId);
        }
        R r = new R();
        r.setData(imRedPacketSettingService.findByUserId(map));
        return r;
    }
    @RequestMapping(value = "/dt/{mid}",method = RequestMethod.POST)
    public Map<String,Object> dt(@PathVariable String mid){
        MMerchantInfo m = imMerchantInfoService.selectById(mid);
        TbArea a = iTbAreaService.selectById(m.getArea());
        Map<String, Object> map = new HashMap<>();
        if(a!=null) {
            map.put("address", m.getAddress());
            map.put("name", a.getName());
        }else {
            map.put("address", "长沙市");
            map.put("name", "长沙市");
        }
        return  map;

    }
}
