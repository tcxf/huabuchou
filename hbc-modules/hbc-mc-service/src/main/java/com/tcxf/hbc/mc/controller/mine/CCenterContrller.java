package com.tcxf.hbc.mc.controller.mine;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.dto.TbCollectDto;
import com.tcxf.hbc.mc.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YWT_tai
 * @Date :Created in 15:20 2018/7/5
 */

@RestController
@RequestMapping("/consumerCenter")
public class CCenterContrller {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private  ITbCollectService iTbCollectService;

    @Autowired
    private ITbScoreLogService iTbScoreLogService;

    @Autowired
    private ICUserOtherInfoService iCUserOtherInfoService;

    @Autowired
    private ITbRepayLogService iTbRepayLogService;

    @Autowired
    private ICUserRefConService iCUserRefConService;

    @Autowired
    private IFFundendService iFFundendService;

    @Autowired
    private ICUserInfoService iCUserInfoService;

    @Autowired
    private IMRedPacketSettingService imRedPacketSettingService;

    @Autowired
    private ITbRepayService iTbRepayService;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:30 2018/7/5
     * 查询当前用户所有的收藏
    */
    @RequestMapping(value="/findMineCollects/{userId}",method ={RequestMethod.GET} )
    public R couponList(@PathVariable("userId")String userId){
        R r = new R();
        try {
            List<TbCollectDto> list = iTbCollectService.findAllcollects(userId);
            r.setData(list);
        } catch (Exception e) {
            log.error("findMineCollects:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:47 2018/7/6
     * 取消收藏
    */
    @RequestMapping(value="/deleteCollect/{id}",method ={RequestMethod.POST} )
    public R deleteCollect(@PathVariable String id){
        R r = new R();
        try {
            boolean b = iTbCollectService.delete(new EntityWrapper<TbCollect>().eq("id",id));
            r.setData(b);
        } catch (Exception e) {
            log.error("deleteCollect:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }
    /**
    * @Author:HuangJun
    * @Description
     * @Date: 13:45 2018/7/11
     *根据用户id和商户id取消收藏
    */
    @RequestMapping(value = "/deleteByMid/{uid}/{mid}",method = RequestMethod.POST)
    public R deleteByMid(@PathVariable String uid,@PathVariable String mid){
        R r = new R();
        try {
            Map<String,Object> map = new HashMap<>();
            map.put("uid",uid);
            map.put("mid",mid);
            if(iTbCollectService.delete(new EntityWrapper<TbCollect>().allEq(map))){
                r.setMsg("已取消收藏");
            }else {
                r.setMsg("取消收藏失败");
            }
        } catch (Exception e) {
            log.error("deleteByMid:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 19:56 2018/7/10
     * 添加收藏
    */
    @RequestMapping(value="/addCollect",method ={RequestMethod.POST} )
    public R addCollect(@RequestBody TbCollect tbCollect){
        R r = new R();
        try {
            tbCollect.setCreateDate(new Date());
            List<TbCollect> tbCollects = iTbCollectService.selectList(new EntityWrapper<TbCollect>().eq("mid", tbCollect.getMid()).eq("uid", tbCollect.getUid()));
            if(null != tbCollects && tbCollects.size()>0){
                r.setCode(R.FAIL);
                r.setMsg("请勿重复添加!");
            }
            Boolean add = iTbCollectService.insert(tbCollect);
            if(add){
                r.setCode(R.SUCCESS);
                r.setMsg("收藏成功!");
            }
        } catch (Exception e) {
            log.error("addCollect:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }
    /*================================我的积分===============================*/

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:11 2018/7/9
     * 查询我的积分
    */
    @RequestMapping(value="/findScoreInfo/{limit}/{page}/{direction}/{userId}",method ={RequestMethod.POST} )
    public R findScoreInfo(@PathVariable("limit")int limit,@PathVariable("page")int page,@PathVariable("direction")String direction,@PathVariable("userId")String userId){
        R r =new R();
        try {
            HashMap<String, Object> resultMap = new HashMap<>();
            Page pageInfo=iTbScoreLogService.selectPage(new Page <TbScoreLog>(page,limit), new EntityWrapper<TbScoreLog>().eq("direction", direction).eq("uid",userId));
            CUserOtherInfo userOtherInfo = iCUserOtherInfoService.selectOne(new EntityWrapper<CUserOtherInfo>().eq("uid", userId));
            resultMap.put("pageInfo",pageInfo);
            resultMap.put("score",userOtherInfo.getScore());
            r.setData(resultMap);
        } catch (Exception e) {
            log.error("findScoreInfo:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }

    /*================================我的还款===============================*/

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 11:11 2018/7/9
     * 查询我的还款
     */

    @RequestMapping(value="/findMyPaybackPageInfo/{limit}/{page}/{userId}",method ={RequestMethod.POST} )
    public R findMyPaybackPageInfo(@PathVariable("limit")int limit,@PathVariable("page")int page,@PathVariable("userId")String userId){
        R r =  new R();
        try {
            Page<TbRepay> pageInfo = iTbRepayService.selectPage(new Page<TbRepay>(page, limit), new EntityWrapper<TbRepay>().eq("uid", userId).ne("status","0").orderBy("create_date",false));
            //Page<TbRepayLog> pageInfo = iTbRepayLogService.selectPage(new Page<TbRepayLog>(page, limit), new EntityWrapper<TbRepayLog>().eq("uid", userId));
            r.setData(pageInfo);
        } catch (Exception e) {
            log.error("findMyPaybackPageInfo:",e);
            return R.newError();
        }
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:49 2018/7/9
     * 查询
    */
    @RequestMapping(value="/findMyRepayDetail/{id}/{userId}",method ={RequestMethod.POST} )
    public R findMyRepayDetail(@PathVariable("id")String id,@PathVariable("userId")String userId){
        R r = new R();
        try {
            HashMap<Object, Object> resultMap = new HashMap<>();
           // TbRepayLog tbRepayLog = iTbRepayLogService.selectOne(new EntityWrapper<TbRepayLog>().eq("id",id));
            TbRepay tbRepayLog = iTbRepayService.selectOne(new EntityWrapper<TbRepay>().eq("id",id));
            FFundend fFundend = iFFundendService.selectOne(new EntityWrapper<FFundend>().eq("id", iCUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid", userId)).getFid()));
            resultMap.put("tbRepayLog",tbRepayLog);
            resultMap.put("fName",fFundend.getFname());
            r.setData(resultMap);
        } catch (Exception e) {
            log.error("findMyRepayDetail:"+e.getMessage());
            return R.newError();
    }
        return  r;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:59 2018/7/10
     * 查询我的主页基本信息
    */
    @RequestMapping(value="/findUserInfo/{userId}",method ={RequestMethod.POST} )
    public R findUserInfo(@PathVariable("userId")String userId){
        R r = new R();
        try {
            CUserInfo userInfo = iCUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("id", userId));
            r.setData(userInfo);
        } catch (Exception e) {
            log.error("findUserInfo:"+e.getMessage());
            return R.newError();
        }
        return  r;
    }
}



