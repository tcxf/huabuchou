package com.tcxf.hbc.mc.controller.ooper;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.service.IOOperaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:运营商信息
 */
@RestController
@RequestMapping("/operaInfo")
public class OOperaInfoController {

    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    /**
     * 查找运营商信息 根据二级域名
     */
    @RequestMapping("/selectooperainfoByOsn")
    @ResponseBody
    public R<OOperaInfo> selectooperainfoByOsn(String osn){
        OOperaInfo oOperaInfo = ioOperaInfoService.selectooperainfoByOsn(osn);
        return R.<OOperaInfo>newOK(oOperaInfo);
    }
}
