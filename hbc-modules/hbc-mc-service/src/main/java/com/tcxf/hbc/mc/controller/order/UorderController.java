package com.tcxf.hbc.mc.controller.order;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.mc.model.dto.TradingDetaDto;
import com.tcxf.hbc.mc.model.dto.TradingStaticDto;
import com.tcxf.hbc.mc.service.ITbTradingDetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
* @Author:HuangJun
* @Description 我的订单
 * @Date: 11:46 2018/7/9

*/
@RestController
@RequestMapping("/uorder")
public class UorderController {
    @Autowired
    public ITbTradingDetailService iTbTradingDetailService;

    /**
     *用户商户订单查询通用
     * @param uid
     * @param type 1：查询用户和商户的支付订单  2：查询该商户下的商户订单
     * @return
     */
    @ApiOperation(value = "用户商户订单查询通用", notes = "用户商户订单查询通用",httpMethod ="POST")
    @RequestMapping(value = "/findByUid/{page}/{uid}/{type}",method = RequestMethod.POST)
    public Page findByUid(@PathVariable("page")String page,@PathVariable("uid") String uid,@PathVariable("type") String type){
        Map<String,Object> map = new HashMap<String, Object>();
        if("1".equals(type)) {
            map.put("uid", uid);
            map.put("mid", null);
        }else{
            map.put("mid",uid);
            map.put("uid", null);
        }
        map.put("page",page);
        Page page1 = iTbTradingDetailService.findByUid(map);

        return  page1;
    }

    /**
     *用户商户订单查询通用
     * @param uid *************************** app ********************
     * @param type 1：查询用户和商户的支付订单  2：查询该商户下的商户订单
     * @return
     */
    @ApiOperation(value = "用户商户订单查询通用", notes = "用户商户订单查询通用",httpMethod ="POST")
    @RequestMapping(value = "/findByUidApp/{page}/{uid}/{type}",method = RequestMethod.POST)
    public R findByUidApp(@PathVariable("page")String page,@PathVariable("uid") String uid,@PathVariable("type") String type){
        Map<String,Object> map = new HashMap<String, Object>();
        if("1".equals(type)) {
            map.put("uid", uid);
            map.put("mid", null);
        }else{
            map.put("mid",uid);
            map.put("uid", null);
        }
        map.put("page",page);
        Page page1 = iTbTradingDetailService.findByUid(map);
        R r = new R();
        r.setData(page1);

        return  r;
    }

    /**
     * 商户查询方法
     * @param map
     * @return
     */
    @ApiOperation(value = "商户查询订单方法", notes = "商户查询订单方法",httpMethod ="POST")
    @RequestMapping(value = "/findMByUid",method = RequestMethod.POST)
    public Page findbyuidm(@RequestBody Map<String, Object> map){
        Page page1 = iTbTradingDetailService.findByUid(map);
        return  page1;
    }


    /**
     * 商户查询方法 ************************* App *************************
     * @param map
     * @return
     */
    @ApiOperation(value = "商户查询订单方法App", notes = "商户查询订单方法",httpMethod ="POST")
    @RequestMapping(value = "/findMByUidApp",method = RequestMethod.POST)
    public R findbyuidmApp(@RequestBody Map<String, Object> map){
        Page page1 = iTbTradingDetailService.findByUid(map);
        R r = new R();
        r.setData(page1);
        return  r;
    }

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @ApiOperation(value = "查询订单详情", notes = "查询订单详情",httpMethod ="POST")
    @RequestMapping(value = "/findById/{id}",method = RequestMethod.POST)
    public R findById(@PathVariable("id") String id){
        R r = new R();
        TradingDetaDto dto = iTbTradingDetailService.findById(id);
        if(dto==null){
            TradingDetaDto  d = new TradingDetaDto();
            r.setData(d);
        }else {
            r.setData(dto);
        }
        return  r;
    }

    /**
     * 商户端统计
     * @param mid
     * @return
     */
    @ApiOperation(value = "商户端统计", notes = "商户端统计",httpMethod ="POST")
    @RequestMapping(value = "/findStati/{mid}",method = RequestMethod.POST)
    public R findStati(@PathVariable("mid") String mid) {
        R r = new R();
        try {
            TradingStaticDto dto = iTbTradingDetailService.findStati(mid);
            if(dto!=null) {
                r.setData(dto);
            }else {
                TradingStaticDto dto1 = new TradingStaticDto();
                dto1.setDayTradingCount("0");
                dto1.setDayTradingMoney(new BigDecimal("0"));
                dto1.setMonthTradingCount("0");
                dto1.setMonthTradingMoney(new BigDecimal("0"));
                dto1.setNoBill(new BigDecimal("0"));
                dto1.setSumTradingCount("0");
                dto1.setSumTradingMoney(new BigDecimal("0"));
                dto1.setYesSetMoney(new BigDecimal("0"));
                dto1.setNoSetMoney(new BigDecimal("0"));
                r.setData(dto1);
            }
           return r;
        }catch (Exception e){
            return R.newError("发生错误");
        }
    }


    /**
     * 交易详情
     * @return
     */
    @ApiOperation(value = "交易详情", notes = "交易详情",httpMethod ="POST")
    @RequestMapping(value = "/findTradingDeta/{page}/{mid}/{createDate}/{modifyDate}/{sid}",method = RequestMethod.POST)
    public  Page findTradingDeta (@PathVariable("page") String page,@PathVariable("mid") String mid,
                               @PathVariable("createDate") String createDate,@PathVariable("modifyDate")
                                           String modifyDate,@PathVariable("sid")String sid){
        Map<String,Object> map = new HashMap<>();
        map.put("mid",mid);
        map.put("createDate",createDate);
        map.put("modifyDate",modifyDate);
        map.put("sid",sid);
        map.put("page",page);
        Page page1 = iTbTradingDetailService.findTradingDeta(map);
        return page1;
    }
    /**
     * 交易详情 ************************* App *************************
     * @return
     */
    @ApiOperation(value = "交易详情APP接口", notes = "交易详情",httpMethod ="POST")
    @RequestMapping(value = "/findTradingDetaApp/{page}/{mid}/{createDate}/{modifyDate}/{sid}",method = RequestMethod.POST)
    public  R findTradingDetaApp (@PathVariable("page") String page,@PathVariable("mid") String mid,
                                  @PathVariable("createDate") String createDate,@PathVariable("modifyDate")
                                          String modifyDate,@PathVariable("sid")String sid){
        Map<String,Object> map = new HashMap<>();
        map.put("mid",mid);
        map.put("createDate",createDate);
        map.put("modifyDate",modifyDate);
        map.put("sid",sid);
        map.put("page",page);
        Page page1 = iTbTradingDetailService.findTradingDeta(map);
        R r = new R();
        r.setData(page1);
        return r;
    }

    /**
     * 结算订单
     * @param page
     * @param mid
     * @return
     */
    @ApiOperation(value = "结算订单", notes = "结算订单",httpMethod ="POST")
    @RequestMapping(value = "/findSettlementDeta/{page}/{mid}",method = RequestMethod.POST)
    public  Page findSettlementDeta (@PathVariable("page")String page,@PathVariable("mid") String mid){
        Map<String,Object> map = new HashMap<>();
        map.put("page",page);
        map.put("mid",mid);
        Page page1 = iTbTradingDetailService.findSettlementDeta(map);
        return  page1;
    }
    /**
     * 结算订单 ************************* App *************************
     * @param page
     * @param mid
     * @return
     */
    @ApiOperation(value = "结算订单APP", notes = "结算订单",httpMethod ="POST")
    @RequestMapping(value = "/findSettlementDetaApp/{page}/{mid}",method = RequestMethod.POST)
    public  R findSettlementDetaApp (@PathVariable("page")String page,@PathVariable("mid") String mid){
        Map<String,Object> map = new HashMap<>();
        map.put("page",page);
        map.put("mid",mid);
        R r = new R();
        Page page1 = iTbTradingDetailService.findSettlementDeta(map);
        r.setData(page1);
        return  r;
    }
    /**
     * 商户结算订单
     * @param
     * @param
     * @return
     */
    @ApiOperation(value = "商户结算订单", notes = "商户结算订单",httpMethod ="POST")
    @RequestMapping(value = "/findSettlementMDeta",method = RequestMethod.POST)
    public  Page findSettlementMDeta (@RequestBody Map<String, Object> map){
        Page page1 = iTbTradingDetailService.findSettlementDeta(map);
        return  page1;
    }

    /**
     * 商户结算订单 ************************* App *************************
     * @param
     * @param
     * @return
     */
    @ApiOperation(value = "商户结算订单App", notes = "商户结算订单",httpMethod ="POST")
    @RequestMapping(value = "/findSettlementMDetaApp",method = RequestMethod.POST)
    public  R findSettlementMDetaApp (@RequestBody Map<String, Object> map){
        Page page1 = iTbTradingDetailService.findSettlementDeta(map);
        R r = new R();
        r.setData(page1);
        return  r;
    }

    @RequestMapping(value = "/findByTraind/{mid}",method = RequestMethod.POST)
    public  R  findByTraind(@PathVariable("mid")String mid) { // 七天的交易记录
        Map<String, Object> omap = new HashMap<>();
        omap.put("mid", mid);
        List<Map<String, Object>> listMap = iTbTradingDetailService.findsevendaytrade(omap);
        //处理成数组
        ArrayList<String> timeList = new ArrayList<>();
        ArrayList<BigDecimal> tMoneyList = new ArrayList<>();
        dealArr(listMap, timeList, tMoneyList);
        String[] tm = new String[timeList.size()];
        BigDecimal[] tMon = new BigDecimal[tMoneyList.size()];
        String[] dtimeArr = timeList.toArray(tm);
        BigDecimal[] tMoneyArr = tMoneyList.toArray(tMon);
        R r = new R();
        Map<String,Object> map = new HashMap<>();
        map.put("dtimeArr",dtimeArr);
        map.put("tMoneyArr",tMoneyArr);
        r.setData(map);
        return  r;
    }
    //处理数组公共方法
    static void dealArr(List<Map<String, Object>> listMap, ArrayList<String> timeList, ArrayList<BigDecimal> tMoneyList) {
        for (Map<String,Object> map:listMap) {
            String dtime = (String) map.get("dateTime");
            String substring = dtime.substring(5);
            BigDecimal tMoney = (BigDecimal) map.get("totalMoney");
            timeList.add(substring);
            tMoneyList.add(tMoney);
        }
    }

}
