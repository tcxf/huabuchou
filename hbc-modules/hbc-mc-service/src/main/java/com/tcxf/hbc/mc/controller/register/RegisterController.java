package com.tcxf.hbc.mc.controller.register;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.model.dto.RegisterDto;
import com.tcxf.hbc.mc.model.dto.UserAccountDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;
import com.tcxf.hbc.mc.service.*;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private ICUserInfoService icUserInfoService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    private Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 注册，短信验证
     * 生成TbUserAccount表
     * 账户关联表
     * 钱包表
     * @param registerDto
     * @return
     */
    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    @ApiOperation(value = "用户注册", notes = "用户注册")
    public R<UserInfoDto> register(@RequestBody RegisterDto registerDto)
    {
        try {
            TbUserAccount tbUserAccount = icUserInfoService.addCUserInfo(registerDto);
            UserInfoDto userInfoDto = iTbUserAccountService.selectUserInfo(tbUserAccount.getId());
            return R.<UserInfoDto>newOK(userInfoDto);
        }catch (Exception ex){
            if(ex instanceof CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            log.error("MC controller 异常："+ex);
            return R.<UserInfoDto>newError();
        }
    }

    @RequestMapping(value = "/getYzm/{mobile}/{type}", method = RequestMethod.POST)
    @ApiOperation(value = "发送验证码", notes = "发送验证码")
    public R getYzm(@PathVariable String mobile,@PathVariable String type)
    {
        try {
            String code = messageUtil.Getmsmcode();
            String [] strArray = new String [20];
            strArray[0]=code;
            strArray[1]="5";
            messageUtil.sendSms(mobile,"",type,strArray);
            return R.newOK("发送成功",null);
        }catch (Exception ex){
            if(ex instanceof CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            log.error("MC controller 异常："+ex);
            return R.newError();
        }

    }

    @RequestMapping(value = "/AddTbUserAccount" , method = RequestMethod.POST)
    @ApiOperation(value = "注册添加关联表", notes = "注册添加关联表")
    public R AddTbUserAccount(@RequestBody RegisterDto registerDto)
    {
        R r = new R();
        boolean b = icUserInfoService.registerPlanTwo(registerDto);
        if (b)
        {
            r.setCode(0);
            r.setMsg("添加成功");
        }else {
            r.setCode(2);
            r.setMsg("添加失败");
        }
        return r;
    }
}
