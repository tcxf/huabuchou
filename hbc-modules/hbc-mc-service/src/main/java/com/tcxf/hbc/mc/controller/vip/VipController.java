package com.tcxf.hbc.mc.controller.vip;


import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.mapper.CUserInfoMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.model.dto.VipDto;
import com.tcxf.hbc.mc.service.ICUserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Vip")
public class VipController {
    @Autowired
    private ICUserInfoService icUserInfoService;


    @RequestMapping(value = "/QueryVip/{mid}/{realName}",method = RequestMethod.POST)
    @ApiOperation(value = "根据用户的名字查询商户下的VIP/{mid}商户id/{realName}/会员名字",notes = "根据用户的名字查询商户下的VIP/{mid}商户id/{realName}/会员名字")
    public Page QueryVip(@PathVariable("mid")String mid,@PathVariable("realName")String realName){
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("mid",mid);
            paramMap.put("realName",realName);
            Page page = icUserInfoService.QueryVip(new Query <VipDto>(paramMap));
            return page;
    }
    @RequestMapping(value = "/QueryVipList/{mid}/{realName}/{page}/{row}",method = RequestMethod.POST)
    @ApiOperation(value = "根据用户的名字查询商户下的VIP/{mid}商户id/{realName}/会员名字",notes = "根据用户的名字查询商户下的VIP/{mid}商户id/{realName}/会员名字")
    public R QueryVipList(@PathVariable("mid")String mid, @PathVariable("realName")String realName, @PathVariable("page")String page, @PathVariable("row")String row){
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mid",mid);
        paramMap.put("realName",realName);
        paramMap.put("page",page);
        paramMap.put("row",row);
        Page page1 = icUserInfoService.QueryVipList(new Query <VipDto>(paramMap));
        return R.newOK(page1);
    }
}
