package com.tcxf.hbc.mc.controller.wallet;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/wallet")
public class WalletController {
    @Autowired
    private ITbWalletService iTbWalletService;
    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    @Autowired
    private ITbWalletCashrateService iTbWalletCashrateService;
    @Autowired
    private ITbWalletIncomeService iTbWalletIncomeService;
    @Autowired
    protected ITbWalletWithdrawalsService iTbWalletWithdrawalsService;
    @Autowired
    protected ITbBindCardInfoService iTbBindCardInfoService;
    @Autowired
    protected MessageUtil messageUtil;

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> template;
    /**
     * 查询指定用户钱包-用户，商户
     *
     * @return
     */
    @RequestMapping(value = "/wallets/{oid}" , method = RequestMethod.POST)
    public R findwallet(@PathVariable("oid") String oid) {
        R<Object> r = new R<>();
        TbWallet wallet = iTbWalletService.getfindOWallet(oid);
        r.setData(wallet);
        return r;
    }
    /**
     * 查看指定钱包支出收入记录-用户端
     */
    @RequestMapping(value = "/findUWalletIncome/{utype}/{wid}", method = RequestMethod.POST)
    public R<List<TbWalletIncome>> findUWalletIncome(@PathVariable("utype") String utype,@PathVariable("wid") String wid) {
        Map<String, Object> params = new HashMap<>();
        R<List<TbWalletIncome>> r = new R<>();
        params.put("utype",utype);
        params.put("wid", wid);
        List<TbWalletIncome> walletIncomeList = iTbWalletIncomeService.getUWalletIncomeList(params);
        r.setData(walletIncomeList);
        return r;
    }
    /**
     * 查看指定钱包支出收入记录-用户端
     */
    @RequestMapping(value = "/findMWalletIncome/{utype}/{wid}/{page}/{limit}", method = RequestMethod.POST)
    public Page findMWalletIncome(@PathVariable("utype") String utype,@PathVariable("wid") String wid,@PathVariable("page") String page,@PathVariable("limit") String limit) {
        Map<String, Object> params = new HashMap<>();
        R<List<TbWalletIncome>> r = new R<>();
        params.put("utype",utype);
        params.put("wid", wid);
        params.put("page", page);
        params.put("limit", limit);
        Page list = iTbWalletIncomeService.getMWalletIncomeList(params);
        return list;
    }

    /**
     * 查看所有钱包提现记录-资金端
     */
    @RequestMapping(value ="/findWalletwithdrawalsList/{uid}", method = RequestMethod.POST)
    @ResponseBody
    public R findWalletwithdrawalsList(@PathVariable("uid") String uid) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("uid",uid);
        List<TbWalletWithdrawals> walletwithdrawalsList = iTbWalletWithdrawalsService.findWalletwithdrawalsList(params);
        r.setData(walletwithdrawalsList);
        return r;

    }

    /**
     * 查看所有钱包提现记录-商户PC
     */
    @RequestMapping(value ="/findMWalletwithdrawalsList/{uid}", method = RequestMethod.POST)
    @ResponseBody
    public Page findMWalletwithdrawalsList(@PathVariable("uid") String uid) {
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("uid",uid);
        Page mWalletwithdrawalsList = iTbWalletWithdrawalsService.findMWalletwithdrawalsList(params);
        return mWalletwithdrawalsList;

    }

    /**
     * 查询用户所有绑卡信息
     * @param oid
     * @return
     */
    @RequestMapping(value ="/findUBindCardinfo/{oid}/{type}", method = RequestMethod.POST)
    @ResponseBody
     public R findUBindCardinfo(@PathVariable("oid") String oid,@PathVariable("type") String type){
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("oid",oid);
        params.put("type", type);
        List<TbBindCardInfo> tbBindCardInfos = iTbBindCardInfoService.QueryUAllBank(params);
        r.setData(tbBindCardInfos);
        return r;
     }

    /**
     * 查询用户选择提现卡的信息
     * @param id
     * @return
     */
    @RequestMapping(value ="/findBind/{id}", method = RequestMethod.POST)
    @ResponseBody
    public R findBind(@PathVariable("id") String id){
        Map<String, Object> params = new HashMap<>();
        R r = new R<>();
        params.put("id",id);
        TbBindCardInfo info = iTbBindCardInfoService.selectOne(new EntityWrapper<TbBindCardInfo>().eq("id", id));
        r.setData(info);
        return r;
    }

    /**
     * 获取短信验证码
     *
     * @return
     */
    @RequestMapping("/getsys/{mobile}/{type}")
    public R getsys(@PathVariable("mobile") String mobile,@PathVariable("type") String type) {
        R<Object> objectR = new R<>();
        //String oid = (String) request.getSession().getAttribute("session_fund_id");
        //String mobile ="17621621521";
        String code = messageUtil.Getmsmcode();
        String [] strArray = new String [20];
        strArray[0]=code;
        strArray[1]="5";
        messageUtil.sendSms(mobile,"",type,strArray);
        objectR.setCode(1);
        return objectR;
    }

    /**
     * 钱包提现申请
     * @param
     */
    @RequestMapping(value="/walletdoReg", method = RequestMethod.POST)
    public R walletdoReg(@RequestBody Map<String, Object> map) {
        //设置缓存
        Long sendTime = null;
        if ( template.opsForValue().get("CODE_SEND_TIME"+map.get("uid"))!= null){
            sendTime = Long.parseLong(template.opsForValue().get("CODE_SEND_TIME"+map.get("uid")).toString());
        }
        Long now = new Date().getTime();
        if (sendTime != null && (now - sendTime) < 1000 * 60) {
            throw new CheckedException("请等待" + (60l - ((now - sendTime) / 1000)) + "秒后再次发送");
        }
        // 1分钟过期
        template.opsForValue().set(("CODE_SEND_TIME"+map.get("uid")), (new Date().getTime() + ""),60,TimeUnit.SECONDS);
        R<Object> objectR = new R<>();
        try {
            objectR = iTbWalletService.updateWalletInfo(map);
        } catch (Exception e) {
            if (e instanceof  CheckedException){
                objectR.setMsg("提现申请失败,请稍后再试");
                objectR.setCode(R.FAIL);
                return objectR;
            }
        }
        if(objectR.getCode()==R.SUCCESS){
            template.delete("CODE_SEND_TIME"+map.get("uid"));
        }
        return objectR;

       /* //查询提现需要的数据
        TbBindCardInfo info = iTbBindCardInfoService.selectOne(new EntityWrapper<TbBindCardInfo>().eq("id", map.get("bid")));
        TbWallet wallet = iTbWalletService.getfindOWallet(map.get("uid").toString());
        BigDecimal y =  Calculate.subtract(wallet.getTotalAmount(),new BigDecimal(map.get("txje").toString())).getAmount();
        //调用短信验证方法验证
        if (!messageUtil.valiCode(info.getMobile(),map.get("yCode").toString() ,map.get("type").toString() )) {
            objectR.setMsg("短信验证码错误");
            objectR.setCode(0);
            return objectR;
        }
        //验证钱包余额
        if (!Calculate.greaterOrEquals(wallet.getTotalAmount() , new BigDecimal(map.get("txje").toString()))){
            objectR.setMsg("钱包余额不足");
            objectR.setCode(0);
            return objectR;
        }
        //验证通过插入一条提现申请记录
        TbWalletWithdrawals w = new TbWalletWithdrawals();
        w.setUid(map.get("uid").toString());
        w.setState("1");
        w.setApplyTime(new Date());
        w.setUname(info.getName());
        w.setUmobile(info.getMobile());
        w.setBankCardNo(info.getBankCard());
        w.setKhh(info.getBankName());
        w.setTxAmount(new BigDecimal(map.get("txje").toString()));
        w.setDzAmount(new BigDecimal(map.get("sjtxje").toString()) );
        w.setWname(wallet.getWname());//钱包名称
        w.setServiceFee(new BigDecimal(map.get("txsxje").toString()));
        iTbWalletWithdrawalsService.insert(w);
        //改变钱包的余额
        wallet.setTotalAmount(y);
        boolean id1 = iTbWalletService.update(wallet, new EntityWrapper<TbWallet>().eq("id", wallet.getId()));
        //插入一条提现记录
        TbWalletIncome income = new TbWalletIncome();
        income.setAmount(new BigDecimal(map.get("txje").toString()));
        income.setWid(wallet.getId());
        income.setPlayid(map.get("uid").toString());
        income.setPname(wallet.getUname());
        income.setType("6");
        income.setTradeTotalAmount(new BigDecimal(map.get("txje").toString()));
        income.setPtepy("4");
        income.setPmobile(wallet.getUmobil());
        income.setCreateDate(new Date());
        income.setSettlementStatus("1");
        income.setTid(wallet.getId());
        boolean b = iTbWalletIncomeService.insert(income);*/
      /*  if (id1 == true && b == true){
            objectR.setMsg("提现申请成功");
            objectR.setCode(1);
            template.delete("CODE_SEND_TIME"+map.get("uid"));
        }else {
            objectR.setMsg("提现申请失败");
            objectR.setCode(0);
        }*/
    }

    /**
     * 查询用户提现费率
     * @param map
     * @return
     */
    @RequestMapping(value="/findBYuser", method = RequestMethod.POST)
    public R findBYuser(@RequestBody Map<String, Object> map) {
        R<Object> objectR = new R<>();
        TbWalletCashrate bYuser = iTbWalletCashrateService.findBYuser(map);
        objectR.setData(bYuser);
        return objectR;
    }
    /**
     * 查询商户提现费率
     * @param map
     * @return
     */
    @RequestMapping(value="/findBYmerchant", method = RequestMethod.POST)
    public R findBYmerchant(@RequestBody Map<String, Object> map) {
        R<Object> objectR = new R<>();
        List<TbWalletCashrate> bYmerchant = iTbWalletCashrateService.findBYmerchant(map);
        objectR.setData(bYmerchant);
        return objectR;
    }
    /**
     * 查询用户账户表信息
     */

    @RequestMapping(value="/findaccount/{id}", method = RequestMethod.POST)
    public TbUserAccount findaccount(@PathVariable("id") String id) {
        TbUserAccount account = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("id", id));
        return account;
    }
}
