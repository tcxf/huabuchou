package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提额申请审核记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface CUserCreditApplyMapper extends BaseMapper<CUserCreditApply> {

}
