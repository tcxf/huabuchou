package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.CUserInfoDto;
import com.tcxf.hbc.mc.model.dto.VipDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface CUserInfoMapper extends BaseMapper<CUserInfo> {

    public List<CUserInfo> queryUserInfoForList(Query<CUserInfo> query, Map<String, Object> condition);

    /**
     * 查询商户的Vip
     * @param query
     * @param condition
     * @return
     */
    public List<VipDto> QueryVip(Query<VipDto> query, Map<String, Object> condition);

    /**
     * 查询商户的Vip
     * @param condition
     * @return
     */
    public List<VipDto> QueryVipList(Query<VipDto> query,Map<String, Object> condition);

    /**
     * 查询详情信息
     * @param id
     * @return
     */
    CUserInfoDto selectUserInfoDetal(String id);

}
