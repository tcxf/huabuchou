package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 消费者用户其他信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface CUserOtherInfoMapper extends BaseMapper<CUserOtherInfo> {

}
