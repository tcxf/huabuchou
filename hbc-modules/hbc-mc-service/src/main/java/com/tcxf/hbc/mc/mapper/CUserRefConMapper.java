package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.CUserRefCon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface CUserRefConMapper extends BaseMapper<CUserRefCon> {

}
