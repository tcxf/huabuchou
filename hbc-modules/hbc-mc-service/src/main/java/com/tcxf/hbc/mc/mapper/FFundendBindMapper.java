package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 资金端、运营商绑定关系表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface FFundendBindMapper extends BaseMapper<FFundendBind> {

}
