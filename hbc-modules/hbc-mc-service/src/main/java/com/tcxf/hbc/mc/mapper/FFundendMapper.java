package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.FFundend;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 资金端信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface FFundendMapper extends BaseMapper<FFundend> {

}
