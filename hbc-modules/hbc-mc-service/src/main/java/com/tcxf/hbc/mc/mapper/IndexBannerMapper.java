package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.mc.model.dto.BannerDto;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 首页轮播图表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IndexBannerMapper extends BaseMapper<IndexBanner> {

    /**
     * c端首页banner图
     * @return
     */
    public BannerDto Querybanner(@Param("uid") String id);

}
