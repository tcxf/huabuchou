package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.MHotSearch;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.mc.model.dto.MHotSearchDto;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 热搜店铺表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MHotSearchMapper extends BaseMapper<MHotSearch> {
    /**
     * 更具热门搜索商户信息
     * @param condition
     * @return
     */
    List<MHotSearchDto> grabbleHandlerNames(Map<String,Object> condition);

    List<MHotSearchDto> QuerySHS(Map<String,Object> condition);

}
