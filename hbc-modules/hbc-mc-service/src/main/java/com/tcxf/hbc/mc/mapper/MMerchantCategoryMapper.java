package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商户行业分类表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MMerchantCategoryMapper extends BaseMapper<MMerchantCategory> {


    //未登陆优惠卷
    List<MMerchantCategory> QueryAll(Map<String,Object> condition);

}
