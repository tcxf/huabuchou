package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.NearbyMerchantDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商户信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MMerchantInfoMapper extends BaseMapper<MMerchantInfo> {

    public List<NearbyMerchantDto> findNearByList(Query<NearbyMerchantDto> query , Map<String,Object> map);

    public NearbyMerchantDto findById(@Param("id") String id);

    /**
     * C端店铺资料查询
     * @param id
     * @return
     */
    public NearbyMerchantDto QueryMerchantInfo(@Param("id") String id);

}
