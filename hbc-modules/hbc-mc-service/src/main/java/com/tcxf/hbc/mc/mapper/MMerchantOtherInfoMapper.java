package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商户其他信息 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MMerchantOtherInfoMapper extends BaseMapper<MMerchantOtherInfo> {

}
