package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.mc.model.dto.RedPacketDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * <p>
 * 用户领取优惠券记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MRedPacketMapper extends BaseMapper<MRedPacket> {

    public List<RedPacketDto> findByUserId(@Param("user_id") String id);

}
