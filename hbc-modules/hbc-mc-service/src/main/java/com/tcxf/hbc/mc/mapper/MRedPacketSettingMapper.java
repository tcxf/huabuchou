package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优惠券表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MRedPacketSettingMapper extends BaseMapper<MRedPacketSetting> {

    public List<RedSettingDto> findList(@Param("miid") String miid);
    public List<RedSettingDto> findMList(Query query,Map<String, Object> map);
    public List<RedSettingDto> findByUserId(Map<String,Object> map);
    //首页展示优惠券
    List<RedSettingDto> Querypacket(Map<String,Object> condition);

    //更多 更具查询所有优惠券
    List<RedSettingDto> Querypacketss(Query query,Map<String,Object> condition);

    //未登陆优惠卷
    List<RedSettingDto> Nopacket(Map<String,Object> condition);

}
