package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 运营商信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface OOperaInfoMapper extends BaseMapper<OOperaInfo> {

}
