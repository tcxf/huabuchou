package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.OOperaOtherInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 运营商其他信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface OOperaOtherInfoMapper extends BaseMapper<OOperaOtherInfo> {

}
