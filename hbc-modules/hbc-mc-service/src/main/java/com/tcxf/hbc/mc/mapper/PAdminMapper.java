package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.PAdmin;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台用户信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface PAdminMapper extends BaseMapper<PAdmin> {

    public List<PAdmin> findByConductor(Query query,Map<String, Object> map);
}
