package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.HashMap;

/**
 * <p>
 * 用户授信资金额度表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbAccountInfoMapper extends BaseMapper<TbAccountInfo> {

    public TbAccountInfo getTbAccountInfoByUid(HashMap<String, Object> hashMap);

}
