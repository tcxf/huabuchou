package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 授信资金流水 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbAccountTradingLogMapper extends BaseMapper<TbAccountTradingLog> {

}
