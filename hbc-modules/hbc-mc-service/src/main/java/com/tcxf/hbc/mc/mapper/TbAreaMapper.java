package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 地区表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbAreaMapper extends BaseMapper<TbArea> {

}
