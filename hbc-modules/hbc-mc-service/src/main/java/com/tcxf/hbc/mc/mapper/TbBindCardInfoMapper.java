package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 绑卡信息 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbBindCardInfoMapper extends BaseMapper<TbBindCardInfo> {
    /**
     * 查询所有银行卡信息
     */
    public List<TbBindCardInfo> QueryUAllBank(Map<String,Object> map);

    /**
     * 查询所有银行卡信息
     */
    public List<TbBindCardInfo> QueryMAllBank(Query query, Map<String,Object> map);

    /**
     *  绑卡时查询当前是否重复
     */
    TbBindCardInfo  QueryBank(Map<String,Object> map);

    /**
     * 根据oid查询运营商的一张卡的信息
     * @param id
     * @return
     */
    TbBindCardInfo selectTbBindCardInfoByOid(String id);
}
