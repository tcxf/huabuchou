package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.mc.model.dto.TbCollectDto;

import java.util.List;

/**
 * <p>
 * 我的收藏表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface TbCollectMapper extends BaseMapper<TbCollect> {

    List<TbCollectDto> findAllcollects(String uid);

    List<TbCollectDto> findNotCouponCollects(String uid);

    List<TbCollectDto> findAllcollectsNotPass(String uid);
}
