package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbImproveQuota;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 消费者用户提额授信结果表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbImproveQuotaMapper extends BaseMapper<TbImproveQuota> {

}
