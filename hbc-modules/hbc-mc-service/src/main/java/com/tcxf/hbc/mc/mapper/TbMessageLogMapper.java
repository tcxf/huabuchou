package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 短信记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbMessageLogMapper extends BaseMapper<TbMessageLog> {

}
