package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbOperationLogMapper extends BaseMapper<TbOperationLog> {

}
