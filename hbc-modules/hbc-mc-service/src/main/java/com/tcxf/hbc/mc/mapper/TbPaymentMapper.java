package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbPayment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 支付记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbPaymentMapper extends BaseMapper<TbPayment> {

}
