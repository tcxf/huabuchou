package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 后台用户角色关系表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbPeopleRoleMapper extends BaseMapper<TbPeopleRole> {

}
