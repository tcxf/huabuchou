package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepayLogMapper extends BaseMapper<TbRepayLog> {

    public List<TbRepayLog> findExRepayLogList(Map<String,Object> map);

    List<TbRepaymentDetailDto> findAllAdvancePay(Map<String,Object> paramMap);
}
