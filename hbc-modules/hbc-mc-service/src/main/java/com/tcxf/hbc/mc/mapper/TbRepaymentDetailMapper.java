package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepaymentDetailMapper extends BaseMapper<TbRepaymentDetail> {

    /**
     * 更新还款账单状态
     * @param map
     */
    public void updateSplitRepamentDetail(Map<String,Object> map);

    /**
     * 查询提前还款记录
     * @param map
     * @return
     */
    public List<TbRepaymentDetail> findExRepayList(Map<String,Object> map);

    /**
     * 查询
     * @param map
     * @return
     */
    public TbRepaymentDetail findExRepaymentIsSpilt(Map<String,Object> map);

    List<Map<String,Object>> findPlanYears(Map<String,Object> map);

    List<Map<String,Object>> findPlanBillByYear(Map<String,Object> mmp);

    List<TbRepaymentDetailDto> findNotExactlyAdvancePay(Map<String,Object> paramMap);

    public TbRepaymentDetail findRepaymentDetailById(Map<String, Object> map);
}
