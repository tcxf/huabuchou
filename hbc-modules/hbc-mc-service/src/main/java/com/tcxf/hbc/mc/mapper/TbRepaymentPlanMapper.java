package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款计划表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepaymentPlanMapper extends BaseMapper<TbRepaymentPlan> {

    Map<String,Object> findTotalWaitPayMoney(Map<String,Object> paramMap);

    List<Map<String,Object>> findExsitOverdue(Map<String,Object> paramMap);

    Map<String,Object> findPlanOverDueBill(Map<String,Object> paramMap);

    List<Map<String, Object>>  findCurrentMonthPayPlan(Map<String,Object> paramMap);

    String findCurrentMonthSetllmentId(Map<String,Object> paramMap);

    Map<String,Object> findPlanDetailInfo(String planId);

    List<Map<String,Object>> findDivideDetail(String id);

    BigDecimal findRemainPlanMoney(String id);

    BigDecimal findCurrentMonthMoney(Map<String,Object> paramMap);

    List<TbRepaymentPlan> findExRepayList(Map<String,Object> map);

    List<TbRepaymentPlan> findExSplitRepayList(Map<String,Object> map);

    Map<String,Object> findRemainPlanAndCurrentMoney(Map<String,Object> map);
}
