package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 还款比率字典表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepaymentRatioDictMapper extends BaseMapper<TbRepaymentRatioDict> {

   public TbRepaymentRatioDict findDictMyOid(Map<String,Object> map);
}
