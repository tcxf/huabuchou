package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbResources;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.TbResourcesDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资源信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbResourcesMapper extends BaseMapper<TbResources> {

    /**
     * 根据权限id和权限type查询资源
     * @param map
     * @return
     */
    public List<TbResources> selectResources(Map<String,Object> map);

    public List<TbResourcesDto> getList(Query<TbResourcesDto> query, Map<String,Object> map);
}
