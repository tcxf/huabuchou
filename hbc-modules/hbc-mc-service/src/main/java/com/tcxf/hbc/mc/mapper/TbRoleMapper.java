package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRoleMapper extends BaseMapper<TbRole> {
    /**
     * 获取角色
     * @return
     */
    List<TbRole> selectRole(Map<String,Object> map);
}
