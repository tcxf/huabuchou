package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbRoleResources;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 后台用户角色资源关系表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRoleResourcesMapper extends BaseMapper<TbRoleResources> {

}
