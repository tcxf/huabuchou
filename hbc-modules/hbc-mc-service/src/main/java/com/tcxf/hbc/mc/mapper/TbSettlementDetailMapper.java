package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  交易结算表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbSettlementDetailMapper extends BaseMapper<TbSettlementDetail> {

}
