package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbSettlementDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 结算方式比例字典表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbSettlementDictMapper extends BaseMapper<TbSettlementDict> {

}
