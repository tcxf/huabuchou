package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbSettlementLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  结算支付记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbSettlementLogMapper extends BaseMapper<TbSettlementLog> {

}
