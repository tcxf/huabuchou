package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbTradeProfitShare;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 交易利润分配表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbTradeProfitShareMapper extends BaseMapper<TbTradeProfitShare> {

}
