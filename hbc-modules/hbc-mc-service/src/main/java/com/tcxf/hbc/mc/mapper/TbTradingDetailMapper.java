package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.TradingDetaDto;
import com.tcxf.hbc.mc.model.dto.TradingStaticDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户交易记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbTradingDetailMapper extends BaseMapper<TbTradingDetail> {

    public List<Map<String,Object>> findUserTradeYear(Map<String, Object> map);

    public List<Map<String,Object>> findUserPaymentYear(Map<String, Object> map);

    public List<TradingDetaDto> findByUid(Query query, Map<String, Object> map);

    public TradingDetaDto findById(@Param("id") String id);

    BigDecimal findUnOutTotalMoney(Map<String,Object> mmp);

    BigDecimal findAdvanceTotalMoney(Map<String,Object> paramMap);

    public TbTradingDetail findExTradeByDate(HashMap<String,Object> map);

    public TbTradingDetail findExTradeByDateSum(HashMap<String,Object> map);

    public TradingStaticDto findStati(@Param("mid") String mid);

    public List<TradingDetaDto> findTradingDeta(Query query, Map<String, Object> map);

    public List<TradingDetaDto> findSettlementDeta(Query query,Map<String,Object> map);

    public List<TbTradingDetail> findExRepayTradeList(Map<String,Object> map);

    public Map<String,Object> findsevendaytrade(Map<String,Object> paramMap);
}
