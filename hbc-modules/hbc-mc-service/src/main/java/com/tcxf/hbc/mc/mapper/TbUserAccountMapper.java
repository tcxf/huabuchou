package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.mc.model.dto.UserAccountDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;

import java.util.List;

/**
 * <p>
 * 用户账户表（包含消费者、商户） Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbUserAccountMapper extends BaseMapper<TbUserAccount> {

    UserAccountDto selectUserInfo(String accountId);
}
