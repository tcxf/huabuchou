package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户类型表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbUserAccountTypeConMapper extends BaseMapper<TbUserAccountTypeCon> {

}
