package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
public interface TbWalletCashrateMapper extends BaseMapper<TbWalletCashrate> {

    /**
     * 查看用户提现费率信息
     */

    TbWalletCashrate findBYuser(Map<String, Object> condition);

    /**
     * 查看商户提现费率信息
     */

    List<TbWalletCashrate> findBYmerchant(Map<String, Object> condition);

}
