package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbWalletIncomeMapper extends BaseMapper<TbWalletIncome> {
    /**
     * 查询钱包的收入支出记录 商户PC
     */
    List<TbWalletIncome> getWalletIncomeList(Query query, Map<String, Object> condition);

    /**
     * 查询钱包的收入支出记录 用户
     */
    List<TbWalletIncome> getWalletIncome(Map<String, Object> condition);

}
