package com.tcxf.hbc.mc.mapper;

import com.tcxf.hbc.common.entity.TbWallet;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 资金钱包表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbWalletMapper extends BaseMapper<TbWallet> {

}
