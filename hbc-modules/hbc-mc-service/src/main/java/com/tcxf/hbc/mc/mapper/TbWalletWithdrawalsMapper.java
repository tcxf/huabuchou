package com.tcxf.hbc.mc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbWalletWithdrawalsMapper extends BaseMapper<TbWalletWithdrawals> {
    /**
     * 查询钱包的提现记录
     */
    public List<TbWalletWithdrawals> findWalletwithdrawalsList(Map<String, Object> condition);

    /**
     * 查询钱包的提现记录
     */
    public List<TbWalletWithdrawals> findWalletwithdrawals(Query query, Map<String, Object> condition);
}
