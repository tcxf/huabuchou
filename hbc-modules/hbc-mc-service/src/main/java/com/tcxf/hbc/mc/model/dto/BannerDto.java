package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.IndexBanner;

/**
 * 查询首页banner图的DTO
 */
public class BannerDto extends IndexBanner {
        private String ooid;/*运营商oid*/
        private String img;//banner图图片路径
        private String href;//banner图链接地址
        private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    @Override
    public String getImg() {
        return img;
    }

    @Override
    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String getHref() {
        return href;
    }

    @Override
    public void setHref(String href) {
        this.href = href;
    }
}
