package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.CUserInfo;

import java.util.Date;

public class CUserInfoDto extends CUserInfo {
    /**
     * 用户主键id
     */
    private String uid;
    /**
     * 绑卡状态（0-未绑卡 1-已绑卡）
     */
    private String bindCardStatus;
    /**
     * 账户类型 （1-个人 2-企业）
     */
    private String type;
    /**
     * 是否结婚
     */
    private String isMarry;
    /**
     * 地区
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 紧急联系人
     */
    private String ugrenName;
    /**
     * 紧急联系人电话
     */
    private String ugrenMobile;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 工作时长
     */
    private Integer workTime;
    /**
     * 工资
     */
    private Integer wages;
    /**
     * 公司电话
     */
    private String companyTel;
    /**
     * 公司职位
     */
    private String companyPosition;
    /**
     * 公司性质
     */
    private String companyType;
    /**
     * 公司地址
     */
    private String companyAddress;
    /**
     * 公司所在位置（如：湖南省长沙市岳麓区）
     */
    private String companyArea;
    /**
     *  积分数
     */
    private Integer score;
    /**
     * 0
     */
    private Integer versionCode;
    /**
     * 地区完整名字
     */
    private String disPlayName;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBindCardStatus() {
        return bindCardStatus;
    }

    public void setBindCardStatus(String bindCardStatus) {
        this.bindCardStatus = bindCardStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(String isMarry) {
        this.isMarry = isMarry;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUgrenName() {
        return ugrenName;
    }

    public void setUgrenName(String ugrenName) {
        this.ugrenName = ugrenName;
    }

    public String getUgrenMobile() {
        return ugrenMobile;
    }

    public void setUgrenMobile(String ugrenMobile) {
        this.ugrenMobile = ugrenMobile;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    public Integer getWages() {
        return wages;
    }

    public void setWages(Integer wages) {
        this.wages = wages;
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    public String getCompanyPosition() {
        return companyPosition;
    }

    public void setCompanyPosition(String companyPosition) {
        this.companyPosition = companyPosition;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyArea() {
        return companyArea;
    }

    public void setCompanyArea(String companyArea) {
        this.companyArea = companyArea;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getDisPlayName() {
        return disPlayName;
    }

    public void setDisPlayName(String disPlayName) {
        this.disPlayName = disPlayName;
    }
}
