package com.tcxf.hbc.mc.model.dto;

public class LoginDto {
    //用户登录
    public final static String TYPE_USER="1";
    private String mobile;//手机号
    private String type;//判断是用户登陆还是商户登陆
    private String pwd;//密码
    private String ycode;//短信验证码
    private String openId;//微信openID;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getYcode() {
        return ycode;
    }

    public void setYcode(String ycode) {
        this.ycode = ycode;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
