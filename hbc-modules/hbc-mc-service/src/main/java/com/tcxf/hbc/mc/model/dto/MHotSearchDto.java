package com.tcxf.hbc.mc.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.tcxf.hbc.common.entity.MHotSearch;

public class MHotSearchDto extends MHotSearch {

    /**
     * 商户简称
     */
    private String simpleName;
    /**
     * 详细地址
     */
    private String address;

    private int totals;

    private String mname;

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public int getTotals() {
        return totals;
    }

    public void setTotals(int totals) {
        this.totals = totals;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}