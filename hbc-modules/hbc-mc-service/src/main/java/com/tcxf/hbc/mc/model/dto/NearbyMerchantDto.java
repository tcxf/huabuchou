package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.MMerchantInfo;

import java.math.BigDecimal;

public class NearbyMerchantDto extends MMerchantInfo {
    private String distance;

    private BigDecimal fullMoney;

    private BigDecimal money;

    private String localPhoto;

    private String recommendGoods;

    private String hname;

    private String openDate;

    private  String licensePic;

    private  String authExamine;

    private String displayName;

    private String micId;

    private String areaId;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Override
    public String getMicId() {
        return micId;
    }

    @Override
    public void setMicId(String micId) {
        this.micId = micId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }


    public String getHname() {
        return hname;
    }

    public void setHname(String hname) {
        this.hname = hname;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    public String getRecommendGoods() {
        return recommendGoods;
    }

    public  Integer ctype;

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public void setRecommendGoods(String recommendGoods) {
        this.recommendGoods = recommendGoods;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }



    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public BigDecimal getFullMoney() {
        return fullMoney;
    }

    public void setFullMoney(BigDecimal fullMoney) {
        this.fullMoney = fullMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

}
