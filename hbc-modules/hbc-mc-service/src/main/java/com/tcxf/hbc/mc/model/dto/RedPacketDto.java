package com.tcxf.hbc.mc.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.common.util.DateUtil;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RedPacketDto extends MRedPacket {
    private String merchantName;

    private String mrStatus;

    private String timeType;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    private Date createDateStr;

    private String days;

    private String couponType;


    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }
    public String getCouponType() {
        return couponType;
    }

//    //判断优惠券是否可使用
//    public String getCouponType() {
//        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String dateTime = sdf.format(date);
//        date = DateUtil.stringDate(dateTime);
//        String dateTime1 = sdf.format(createDateStr);



//        createDateStr = DateUtil.stringDate(dateTime1);
//        if((date.getTime()-createDateStr.getTime()>0?true:false)){
//            //已过期
//            return "1";
//        }else{
//            //可使用
//            return "0";
//        }
//    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMrStatus() {
        return mrStatus;
    }

    public void setMrStatus(String mrStatus) {
        this.mrStatus = mrStatus;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public Date getCreateDateStr() {
        return createDateStr;
    }

    public void setCreateDateStr(Date createDateStr) {
        this.createDateStr = createDateStr;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }
}
