package com.tcxf.hbc.mc.model.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class RedSettingDto extends MRedPacketSetting {
    //用户领取数
    private Integer getSum;

    //用户使用数
    private Integer useSum;

    private Integer utype;


    private String micId;

    private String mname;

    private String couponType;//0过期 1可使用

    public Date getCreateDateStr() {
        return createDateStr;
    }

    public void setCreateDateStr(Date createDateStr) {
        this.createDateStr = createDateStr;
    }

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    @TableField("create_date")
    private Date createDateStr;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    private  Date endDates;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern ="yyyy-MM-dd" )
    private Date createDates;

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Date getCreateDates() {
        return createDates;
    }

    public void setCreateDates(Date createDates) {
        this.createDates = createDates;
    }

    public Date getEndDates() {
        return endDates;
    }

    public void setEndDates(Date endDates) {
        this.endDates = endDates;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getMicId() {
        return micId;
    }

    public void setMicId(String micId) {
        this.micId = micId;
    }

    public Integer getUtype() {
        return utype;
    }

    public void setUtype(Integer utype) {
        this.utype = utype;
    }

    private String localPhoto;

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }
    //用户领取状态 1领取 0未领取


    private   String oid;

    @Override
    public String getOid() {
        return oid;
    }


    private String mid;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    @Override
    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getGetSum() {
        return getSum;
    }

    public void setGetSum(Integer getSum) {
        this.getSum = getSum;
    }

    public Integer getUseSum() {
        return useSum;
    }

    public void setUseSum(Integer useSum) {
        this.useSum = useSum;
    }
}
