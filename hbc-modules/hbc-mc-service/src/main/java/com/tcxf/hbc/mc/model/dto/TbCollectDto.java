package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.TbCollect;

import java.math.BigDecimal;

/**
 * @author YWT_tai
 * @Date :Created in 15:58 2018/7/5
 */
public class TbCollectDto extends TbCollect {

    private String shopName;//店铺名字

    private BigDecimal fullMoney;//满多少

    private BigDecimal money;//减少多

    private String localPhoto;//店铺照片

    private String status;//状态为2则显示满减


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public BigDecimal getFullMoney() {
        return fullMoney;
    }

    public void setFullMoney(BigDecimal fullMoney) {
        this.fullMoney = fullMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }


}
