package com.tcxf.hbc.mc.model.dto;

import java.math.BigDecimal;

public class TradingStaticDto {
    private BigDecimal noSetMoney;//待结算金额

    private BigDecimal yesSetMoney;//已结算金额

    private String dayTradingCount;//日交易笔数

    private BigDecimal dayTradingMoney;//日交易金额

    private String monthTradingCount;//月交易笔数

    private BigDecimal monthTradingMoney;//月交易金额

    private String sumTradingCount;//总交易笔数

    private  BigDecimal sumTradingMoney;//总交易金额

    private BigDecimal noBill;//未出账单

    public BigDecimal getNoSetMoney() {
        return noSetMoney;
    }

    public void setNoSetMoney(BigDecimal noSetMoney) {
        this.noSetMoney = noSetMoney;
    }

    public BigDecimal getYesSetMoney() {
        return yesSetMoney;
    }

    public void setYesSetMoney(BigDecimal yesSetMoney) {
        this.yesSetMoney = yesSetMoney;
    }

    public String getDayTradingCount() {
        return dayTradingCount;
    }

    public void setDayTradingCount(String dayTradingCount) {
        this.dayTradingCount = dayTradingCount;
    }

    public BigDecimal getDayTradingMoney() {
        return dayTradingMoney;
    }

    public void setDayTradingMoney(BigDecimal dayTradingMoney) {
        this.dayTradingMoney = dayTradingMoney;
    }

    public String getMonthTradingCount() {
        return monthTradingCount;
    }

    public void setMonthTradingCount(String monthTradingCount) {
        this.monthTradingCount = monthTradingCount;
    }

    public BigDecimal getMonthTradingMoney() {
        return monthTradingMoney;
    }

    public void setMonthTradingMoney(BigDecimal monthTradingMoney) {
        this.monthTradingMoney = monthTradingMoney;
    }

    public String getSumTradingCount() {
        return sumTradingCount;
    }

    public void setSumTradingCount(String sumTradingCount) {
        this.sumTradingCount = sumTradingCount;
    }

    public BigDecimal getSumTradingMoney() {
        return sumTradingMoney;
    }

    public void setSumTradingMoney(BigDecimal sumTradingMoney) {
        this.sumTradingMoney = sumTradingMoney;
    }

    public BigDecimal getNoBill() {
        return noBill;
    }

    public void setNoBill(BigDecimal noBill) {
        this.noBill = noBill;
    }
}
