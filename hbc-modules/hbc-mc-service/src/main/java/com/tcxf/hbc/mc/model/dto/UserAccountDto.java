package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;

import java.util.List;

public class UserAccountDto extends TbUserAccount {
    private List<TbUserAccountTypeCon> tbUserAccountTypeConList;

    public List<TbUserAccountTypeCon> getTbUserAccountTypeConList() {
        return tbUserAccountTypeConList;
    }

    public void setTbUserAccountTypeConList(List<TbUserAccountTypeCon> tbUserAccountTypeConList) {
        this.tbUserAccountTypeConList = tbUserAccountTypeConList;
    }
}
