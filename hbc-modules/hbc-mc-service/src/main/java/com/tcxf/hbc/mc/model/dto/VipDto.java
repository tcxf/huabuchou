package com.tcxf.hbc.mc.model.dto;

import com.tcxf.hbc.common.entity.CUserInfo;

public class VipDto extends CUserInfo {
    private  String mid;

    private  String authStatus;

    private String page;

    private String row;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }
}
