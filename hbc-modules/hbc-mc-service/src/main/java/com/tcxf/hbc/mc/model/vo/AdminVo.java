package com.tcxf.hbc.mc.model.vo;

import com.tcxf.hbc.common.entity.PAdmin;

public class AdminVo extends PAdmin {
    private  String page;
     public void setPage(String page){
         this.page = page;
     }

     public String getPage(){
         return  page;
     }
}
