package com.tcxf.hbc.mc.model.vo;

import com.tcxf.hbc.common.entity.MMerchantInfo;

/**
 * c 端店铺资料
 * jiangyong
 * 2018年7月17日09:04:00
 */
public class MerchantVo extends MMerchantInfo {
    private String uid;
    private  String mmid;
    private String hname;
    private String openDate;
    private String displayName;
    private String recommendGoods;
    private String localPhoto;
    private String licensePic;
    private String areaId;
    private String dname;
    private String authExamine;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthExamine() {
        return authExamine;
    }

    public void setAuthExamine(String authExamine) {
        this.authExamine = authExamine;
    }

    public String getMmid() {
        return mmid;
    }

    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getAreaId() {
        return areaId;

    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getHname() {
        return hname;
    }

    public void setHname(String hname) {
        this.hname = hname;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getRecommendGoods() {
        return recommendGoods;
    }

    public void setRecommendGoods(String recommendGoods) {
        this.recommendGoods = recommendGoods;
    }

    public String getLocalPhoto() {
        return localPhoto;
    }

    public void setLocalPhoto(String localPhoto) {
        this.localPhoto = localPhoto;
    }

    public String getLicensePic() {
        return licensePic;
    }

    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }
}
