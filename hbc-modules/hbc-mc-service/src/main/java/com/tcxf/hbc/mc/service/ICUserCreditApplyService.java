package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提额申请审核记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserCreditApplyService extends IService<CUserCreditApply> {

}
