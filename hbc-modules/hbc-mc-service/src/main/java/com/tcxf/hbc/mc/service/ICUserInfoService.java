package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.CUserInfoDto;
import com.tcxf.hbc.mc.model.dto.VipDto;
import com.tcxf.hbc.mc.model.dto.RegisterDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserInfoService extends IService<CUserInfo> {

    public CUserInfo getUserInfo(String userId);

    public void delUserInfo(String userId);

    public void modifyUserInfo(CUserInfo cUserInfo);

    public void addUserInfo(CUserInfo cUserInfo);

    public Page selectUserInfoPage(Map<String, Object> params);

    CUserInfoDto selectUserInfo(String userId);

    /**
     * 分页查询用户信息
     * @param status
     * @return
     */
    public Page queryList(String status);

    /**
     * 分页查询用户信息
     * @param query
     * @return
     */
    public Page queryList( Query<CUserInfo> query);

    /**
     * 分页查询商户下的Vip
     * @param query
     * @return
     */
    public Page QueryVip( Query<VipDto> query);


    /**
     * 分页查询商户下的Vip
     * @return
     */
    public Page  QueryVipList(Query<VipDto> query);
    /**
     * 注册添加用户表
     * 添加邀请码
     * @return
     */
    TbUserAccount addCUserInfo(RegisterDto registerDto);

    /**
     * 注册第二步，添加支付密码
     * @param registerDto
     * @return
     */
    boolean registerPlanTwo(RegisterDto registerDto);

    /**
     * 修改用户详情信息
     * @param cUserInfoDto
     * @return
     */
    boolean updateUserInfoDetal(String accountId , CUserInfoDto cUserInfoDto);

}
