package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 消费者用户其他信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserOtherInfoService extends IService<CUserOtherInfo> {

}
