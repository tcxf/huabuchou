package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IFFundendBindService extends IService<FFundendBind> {

}
