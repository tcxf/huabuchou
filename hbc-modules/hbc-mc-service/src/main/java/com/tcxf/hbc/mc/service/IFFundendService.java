package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.FFundend;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 资金端信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IFFundendService extends IService<FFundend> {

}
