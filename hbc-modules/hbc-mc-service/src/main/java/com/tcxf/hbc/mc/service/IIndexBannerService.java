package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.mc.model.dto.BannerDto;

/**
 * <p>
 * 首页轮播图表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IIndexBannerService extends IService<IndexBanner> {
    /**
     * c端首页banner图
     * @return
     */
    public BannerDto Querybanner(String id);

}
