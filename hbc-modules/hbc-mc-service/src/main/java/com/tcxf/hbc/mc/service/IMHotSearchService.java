package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.MHotSearchDto;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 热搜店铺表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IMHotSearchService extends IService<MHotSearch> {

    /**
     * 热门搜索
     * @param query
     * @return
     */
    Page grabbleHandlerNames (Query<MHotSearchDto> query);

    Page QuerySHS (Query<MHotSearchDto> query);
}
