package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;

/**
 * <p>
 * 商户行业分类表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IMMerchantCategoryService extends IService<MMerchantCategory> {

    Page QueryAll(Query<MMerchantCategory> query);
}
