package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.mc.model.dto.NearbyMerchantDto;
import com.tcxf.hbc.mc.model.vo.MerchantVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * <p>
 * 商户信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IMMerchantInfoService extends IService<MMerchantInfo> {

    public Page findNearByList(Map<String,Object> map);

    public NearbyMerchantDto findById(String id);

    /**
     * C端店铺资料查询
     * @param id
     * @return
     */
    public NearbyMerchantDto QueryMerchantInfo(String id);

    void updateStore(@RequestBody MerchantVo m);

}
