package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.mc.model.dto.RedPacketDto;

import java.util.List;

/**
 * <p>
 * 用户领取优惠券记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IMRedPacketService extends IService<MRedPacket> {

    public List<RedPacketDto> findByUserId(String userId);
}
