package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.MRedPacketSettingMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优惠券表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IMRedPacketSettingService extends IService<MRedPacketSetting> {

    public Page selectPage(String mid);

    public List<RedSettingDto> findList(String miid);

    public List<RedSettingDto> findByUserId(Map<String,Object> map);

    /**
     * 商户端查询优惠卷
     * @param map
     * @return
     */
    Page findMlist(Map<String,Object> map);

    /**
     * 首页展示优惠券
     * @param mRedPacketSettingQuery
     * @return
     */
    Page Querypacket(Query<RedSettingDto> mRedPacketSettingQuery);
    

    /**
     * 首页更多查询商户分类优惠券
     * @param mRedPacketSettingQuery
     * @return
     */
    Page Querypacketss(Query<RedSettingDto> mRedPacketSettingQuery);


    /**
     * 未登陆优惠券
     * @param query
     * @return
     */
    Page Nopacket(Query<RedSettingDto> query);
}
