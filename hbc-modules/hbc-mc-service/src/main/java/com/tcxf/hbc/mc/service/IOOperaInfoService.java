package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.OOperaInfo;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbBindCardInfo;

/**
 * <p>
 * 运营商信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IOOperaInfoService extends IService<OOperaInfo> {

    OOperaInfo selectooperainfoByOsn(String osn);
}
