package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.OOperaOtherInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 运营商其他信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IOOperaOtherInfoService extends IService<OOperaOtherInfo> {

}
