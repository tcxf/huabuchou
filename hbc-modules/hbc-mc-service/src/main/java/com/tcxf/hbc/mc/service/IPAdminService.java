package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.PAdmin;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 平台用户信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface IPAdminService extends IService<PAdmin> {

    public Page findAdminCondition(Map<String,Object> map);
}
