package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.baomidou.mybatisplus.service.IService;

import java.util.HashMap;

/**
 * <p>
 * 用户授信资金额度表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbAccountInfoService extends IService<TbAccountInfo> {

    public TbAccountInfo getTbAccountInfoByUid(HashMap<String, Object> hashMap);
}
