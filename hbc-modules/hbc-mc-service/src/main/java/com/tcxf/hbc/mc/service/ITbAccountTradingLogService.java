package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 授信资金流水 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbAccountTradingLogService extends IService<TbAccountTradingLog> {

}
