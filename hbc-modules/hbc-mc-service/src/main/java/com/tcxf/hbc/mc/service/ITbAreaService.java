package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbArea;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地区表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbAreaService extends IService<TbArea> {

}
