package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbBindCardInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 绑卡信息 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbBindCardInfoService extends IService<TbBindCardInfo> {
    /**
     * 查询所有银行卡信息 商户PC
     */
    public Page QueryMAllBank(Map<String,Object> map);

    /**
     * 查询所有银行卡信息 商户PC
     */
    public List<TbBindCardInfo> QueryUAllBank(Map<String,Object> map);

    /**
     *  绑卡时查询当前是否重复
     */
    TbBindCardInfo  QueryBank(Map<String,Object> map);

    /**
     * 根据oid查询运营商的一张卡的信息
     * @param id
     * @return
     */
    TbBindCardInfo selectTbBindCardInfoByOid(String id);
}
