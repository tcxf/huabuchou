package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.mc.model.dto.TbCollectDto;

import java.util.List;

/**
 * <p>
 * 我的收藏表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
public interface ITbCollectService extends IService<TbCollect> {

    List<TbCollectDto> findAllcollects(String uid);
}
