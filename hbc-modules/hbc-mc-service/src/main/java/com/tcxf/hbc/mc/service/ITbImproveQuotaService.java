package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbImproveQuota;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 消费者用户提额授信结果表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbImproveQuotaService extends IService<TbImproveQuota> {

}
