package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 短信记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbMessageLogService extends IService<TbMessageLog> {

}
