package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbOperationLogService extends IService<TbOperationLog> {

}
