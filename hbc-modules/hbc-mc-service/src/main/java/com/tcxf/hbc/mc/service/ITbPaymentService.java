package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbPayment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 支付记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbPaymentService extends IService<TbPayment> {

}
