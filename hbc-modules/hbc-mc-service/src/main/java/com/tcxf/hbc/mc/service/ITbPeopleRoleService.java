package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 后台用户角色关系表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbPeopleRoleService extends IService<TbPeopleRole> {

}
