package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRepayLogService extends IService<TbRepayLog> {

    List<TbRepaymentDetailDto> findAllAdvancePay( Map<String,Object> paramMap);

    void addinitPayBackInfo(PaybackMoneyDto paybackMoneyDto);
}
