package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;


import com.tcxf.hbc.common.entity.TbRepaymentPlan;

import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRepaymentDetailService extends IService<TbRepaymentDetail> {

    /**
     * 获取分期计划费用
     * @param uid
     * @param oid
     * @param fid
     * @param splitMonth
     * @return
     */
    public List<TbRepaymentPlan> getDiffSplitAmount(String uid,String oid,String fid,String splitMonth);


    /**
     * 更新还款账单状态
     * @param map
     */
    public void updateSplitRepamentDetail(Map<String,Object> map);

    /**
     * 查询用户当前账单
     * @param map
     * @return
     */
    public TbRepaymentDetail findRepaymentDetailById(Map<String,Object> map);

    /**
     * 查询提前还款记录
     * @param map
     * @return
     */
    public List<TbRepaymentDetail> findExRepayList(Map<String,Object> map);
    /**
     * 查询历史账单中的分期账单
     * @param uid
     * @param currentData
     * @return
     */
    public TbRepaymentDetail findExRepaymentIsSpilt(String uid,String oid, String currentData);

    /**
     * 查询历史账单中提前还款账单
     * @param uid
     * @param currentData
     * @return
     */
    public List<TbRepayLog> findExRepayLogList(String uid,String currentData);

    /**
     * 查询历史账单中的交易记录
     * @param uid
     * @param currentData
     * @return
     */
    public List<TbTradingDetail> findExRepayTradeList(String uid,String currentData);

    List<Map<String,Object>> findPlanYears(Map<String,Object> mmp);

    List<Map<String,Object>> findPlanBillByYear(Map<String,Object> mmp);

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:46 2018/7/20
     * 查询用户当年不全是提前还款记录
    */
    List<TbRepaymentDetailDto> findNotExactlyAdvancePay(Map<String,Object> paramMap);
}
