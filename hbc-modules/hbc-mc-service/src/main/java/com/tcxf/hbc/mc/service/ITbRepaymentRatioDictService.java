
package com.tcxf.hbc.mc.service;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 还款比率字典表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRepaymentRatioDictService extends IService<TbRepaymentRatioDict> {

    public TbRepaymentRatioDict findDictMyOid (Map<String,Object> map);


}
