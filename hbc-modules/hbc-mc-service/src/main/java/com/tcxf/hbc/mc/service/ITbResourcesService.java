package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资源信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbResourcesService extends IService<TbResources> {

    /**
     * 根据角色id和type查询权限
     * @param id
     * @return
     */
    public List<TbResources> selectResources(String id, int type);

    public Page getPage(Map<String,Object> map);
}
