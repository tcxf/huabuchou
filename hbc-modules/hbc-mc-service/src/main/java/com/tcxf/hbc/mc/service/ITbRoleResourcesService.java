package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbRoleResources;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 后台用户角色资源关系表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRoleResourcesService extends IService<TbRoleResources> {

    public void role(String id,String ids);
}
