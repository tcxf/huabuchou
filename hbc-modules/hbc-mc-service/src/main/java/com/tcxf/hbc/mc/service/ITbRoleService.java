package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbRole;
import com.baomidou.mybatisplus.service.IService;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户角色表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRoleService extends IService<TbRole> {

    public Page selectPage(String page,String aid);

    /**
     * 查询角色
     * @param
     * @return
     */
    public List<TbRole> selectRole(Map<String,Object> map);
}
