package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbScoreLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户积分明细表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbScoreLogService extends IService<TbScoreLog> {

}
