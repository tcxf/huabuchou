package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  交易结算表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbSettlementDetailService extends IService<TbSettlementDetail> {

}
