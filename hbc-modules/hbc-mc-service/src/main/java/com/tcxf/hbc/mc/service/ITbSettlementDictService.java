package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbSettlementDict;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 结算方式比例字典表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbSettlementDictService extends IService<TbSettlementDict> {

}
