package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 三级分销利润分配表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbThreeSaleProfitShareService extends IService<TbThreeSaleProfitShare> {

}
