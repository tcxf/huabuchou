package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbThreeSalesRef;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 三级分销用户关系绑定表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbThreeSalesRefService extends IService<TbThreeSalesRef> {

}
