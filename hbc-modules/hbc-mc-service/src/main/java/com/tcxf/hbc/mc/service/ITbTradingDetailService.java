package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.mc.model.dto.TradingDetaDto;
import com.tcxf.hbc.mc.model.dto.TradingStaticDto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户交易记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbTradingDetailService extends IService<TbTradingDetail> {

    public List<String> findUserTradeYear(Map<String,Object> map);

    public Page findByUid(Map<String,Object> map);

    public TradingDetaDto findById(String id);

    BigDecimal findUnOutTotalMoney(Map<String, Object> mmp);

    BigDecimal findAdvanceTotalMoney(Map<String,Object> paramMap);

    public TbTradingDetail findExTradeByDate(Date date,String uid);

    public TradingStaticDto findStati(String mid);

    public Page findTradingDeta (Map<String,Object> map);

    public Page findSettlementDeta(Map<String,Object> map);

    public List<Map<String,Object>> findsevendaytrade(Map<String,Object> map );


}
