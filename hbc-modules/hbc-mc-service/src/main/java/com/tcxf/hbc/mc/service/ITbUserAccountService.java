package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.mc.model.dto.LoginDto;
import com.tcxf.hbc.mc.model.dto.UserAccountDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;

import java.util.List;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbUserAccountService extends IService<TbUserAccount> {

    /**
     * 登陆
     * @param loginDto
     * @return
     */
    String login(LoginDto loginDto);

    /**
     * 缓存取值，根据accountId查询账户表里面的一系列信息
     * @param accountId
     * @return
     */
    UserInfoDto selectUserInfo(String accountId);

    /**
     * 修改支付密码的方法
     * @param tbUserAccount
     * @return
     */
    boolean updatePaymentCode(TbUserAccount tbUserAccount);

    /**
     * 用户立即开店
     * @param accountId
     * @param password
     * @param oid
     */
    String userSetPassword(String accountId , String password , String oid);

    /**
     * 修改店铺资料的时候清除缓存
     * @param accountId
     */
    void updateMerchantdeleteRedies(String accountId);

    void logout(String accountId,String userId);
}
