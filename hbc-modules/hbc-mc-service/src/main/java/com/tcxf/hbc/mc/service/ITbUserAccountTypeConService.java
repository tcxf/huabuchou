package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户类型表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbUserAccountTypeConService extends IService<TbUserAccountTypeCon> {

}
