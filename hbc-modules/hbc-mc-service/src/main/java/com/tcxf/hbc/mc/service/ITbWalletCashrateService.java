package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbWalletCashrate;
import com.baomidou.mybatisplus.service.IService;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
public interface ITbWalletCashrateService extends IService<TbWalletCashrate> {

    /**
     * 查看用户提现费率信息
     */

    TbWalletCashrate findBYuser(Map<String, Object> condition);

    /**
     * 查看商户提现费率信息
     */

    List<TbWalletCashrate> findBYmerchant(Map<String, Object> condition);
}
