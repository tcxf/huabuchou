package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbWalletIncome;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbWalletIncomeService extends IService<TbWalletIncome> {

    /**
     * 查询钱包的收入支出记录
     */
     Page getMWalletIncomeList(Map<String,Object> map);

    /**
     * 查询钱包的收入支出记录
     */
    List<TbWalletIncome> getUWalletIncomeList(Map<String,Object> map);

}
