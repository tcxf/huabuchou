package com.tcxf.hbc.mc.service;

import com.tcxf.hbc.common.entity.TbWallet;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.util.R;

import java.util.Map;

/**
 * <p>
 * 资金钱包表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbWalletService extends IService<TbWallet> {
    /**
     * 查询指定钱包
     */
    public TbWallet getfindWallet(String id);

    /**
     * 查询指定钱包
     */
    public TbWallet getfindOWallet(String oid);

    /**
     * 添加钱包
     * * 参数：ID 用户的ID(如果注册的用户是消费者或者是商户，插入的ID就是账户表的ID)
     * type 用户的类型1：用户，2：商户，3：运营商，4：资金端,
     * name 当前开通钱包用户的名字(用户类型是普通用户名字就是自己的名字。用户类型是商户称or运营商or资金端就是自己的姓名)
     * wname 钱包的名字(用户姓名or商户名称or运营商名称or资金端名称)
     * mobil 用户手机
     */
    public int savewallet(String id,int type,String name,String mobil,String wname);

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:36 2018/8/30
     * 提现插入记录以及修改表
    */
    R updateWalletInfo(Map<String,Object> map);
}
