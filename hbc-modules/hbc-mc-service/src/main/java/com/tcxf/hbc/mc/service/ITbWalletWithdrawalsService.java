package com.tcxf.hbc.mc.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbWalletWithdrawalsService extends IService<TbWalletWithdrawals> {
    /**
     * 钱包提现查询 用户
     */
    public List<TbWalletWithdrawals> findWalletwithdrawalsList(Map<String,Object> map);

    /**
     * 钱包提现查询 商户PC
     */
    Page findMWalletwithdrawalsList(Map<String,Object> map);

    /**
     * 钱包提现
     */
    public int savaWallet(TbWalletWithdrawals t);
}
