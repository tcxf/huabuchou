package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.CUserCreditApply;
import com.tcxf.hbc.mc.mapper.CUserCreditApplyMapper;
import com.tcxf.hbc.mc.service.ICUserCreditApplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提额申请审核记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class CUserCreditApplyServiceImpl extends ServiceImpl<CUserCreditApplyMapper, CUserCreditApply> implements ICUserCreditApplyService {

}
