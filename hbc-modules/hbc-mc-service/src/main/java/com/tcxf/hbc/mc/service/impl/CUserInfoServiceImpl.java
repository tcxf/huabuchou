package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tcxf.hbc.common.constant.CommonConstant;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.YQCodeUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.mapper.CUserInfoMapper;
import com.tcxf.hbc.mc.model.dto.CUserInfoDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;
import com.tcxf.hbc.mc.model.dto.VipDto;
import com.tcxf.hbc.mc.service.ICUserInfoService;
import com.tcxf.hbc.mc.model.dto.RegisterDto;
import com.tcxf.hbc.mc.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.OpenOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消费者用户表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
@Slf4j
public class CUserInfoServiceImpl extends ServiceImpl<CUserInfoMapper, CUserInfo> implements ICUserInfoService {

    @Autowired
    private  CUserInfoMapper cUserInfoMapper;
    @Autowired
    private ITbThreeSalesRefService iTbThreeSalesRefService;
    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    @Autowired
    private ITbUserAccountTypeConService iTbUserAccountTypeConService;
    @Autowired
    private ITbWalletService iTbWalletService;
    @Autowired
    private IMMerchantInfoService imMerchantInfoService;
    @Autowired
    private ICUserRefConService icUserRefConService;
    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    @Autowired
    private ICUserOtherInfoService icUserOtherInfoService;
    @Autowired
    private IMMerchantOtherInfoService imMerchantOtherInfoService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private ITbPeopleRoleService iTbPeopleRoleService;
    @Autowired
    private IOOperaOtherInfoService ioOperaOtherInfoService;
    @Autowired
    private IFFundendBindService ifFundendBindService;

    /**
     * 根据用户id查询用户信息
     * @param userId
     * @return
     */
    @Override
    @Cacheable(value = "user_info", key = "#userId  + '_user_info'")
    public CUserInfo getUserInfo(String userId) {
        return this.selectById(userId);
    }

    /**
     * 根据用户id删除用户信息
     * @param userId
     */
    @Override
    @CacheEvict(value = "user_info", key = "#userId + '_user_info'")
    public void delUserInfo(String userId) {
        this.deleteById(userId);
    }

    /**
     * 根据用户id更新用户信息
     * @param cUserInfo
     */
    @Override
    @CachePut(value = "user_info", key = "#userId + '_user_info'")
    public void modifyUserInfo(CUserInfo cUserInfo)
    {
        this.updateById(cUserInfo);
    }

    /**
     * 新增用户
     * @param cUserInfo
     */
    public void addUserInfo(CUserInfo cUserInfo)
    {
        this.insert(cUserInfo);
    }

    /**
     * 分页查询用户列表
     * @param params
     * @return
     */
    @Override
    public Page selectUserInfoPage( Map<String, Object> params)
    {
        params.put("status",CommonConstant.STATUS_NORMAL);
       return this.selectPage(new Query<CUserInfo>(params),new EntityWrapper<CUserInfo>());
    }

    /**
     * 分页查询用户信息
     * @param status
     * @return
     */
    @Override
    public Page queryList(String status)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        Query<CUserInfo> query = new  Query(params);
        return  this.queryList(query);
    }

    /**
     * 分页查询用户信息
     * @param query
     * @return
     */
    @Override
    public Page queryList( Query<CUserInfo> query)
    {
        List<CUserInfo> list = cUserInfoMapper.queryUserInfoForList(query, query.getCondition());
        query.setRecords(list);
        return query;
    }

    @Override
    public Page QueryVip(Query <VipDto> query) {
        List <VipDto> list = cUserInfoMapper.QueryVip(query, query.getCondition());
        query.setRecords(list);
        return query;
    }

    @Override
    public Page  QueryVipList(Query <VipDto> query) {
        List <VipDto> list = cUserInfoMapper.QueryVip(query, query.getCondition());
        query.setRecords(list);
        return query;
    }




    /**
     * 添加用户表
     * @param registerDto
     * @return
     */
    private CUserInfo addUser(RegisterDto registerDto)
    {
        CUserInfo c = new CUserInfo();
        c.setMobile(registerDto.getMobile());
        c.setStatus(CUserInfo.STATUS_Y);
        c.setCreateDate(new Date());
        c.setUserAttribute(registerDto.getType());
        c.setWxOpenid(registerDto.getOpenId());
        /**
         * 默认会员头像为 c.setHeadImg("images/uhead.png");
         */
        c.setHeadImg("images/uhead.png");
        /**
         * 默认添加会员名称为手机号码
         */
        c.setRealName(registerDto.getMobile().substring(0,6));
        cUserInfoMapper.insert(c);
        //根据id生成自己的邀请码
        c.setInvateCode(YQCodeUtil.toSerialCode(Long.valueOf(c.getId())));
        cUserInfoMapper.updateById(c);
        //生成CUserOtherInfo
        CUserOtherInfo cUserOtherInfo = new CUserOtherInfo();
        cUserOtherInfo.setUid(c.getId());
        cUserOtherInfo.setCreateDate(new Date());
        cUserOtherInfo.setBindCardStatus(CUserOtherInfo.BINDCARDSTATUS_N);
        icUserOtherInfoService.insert(cUserOtherInfo);
        return c;

    }

    /**
     * 添加三级分销用户关系绑定表
     * @param cUserInfo
     * @param invateCode
     */
    private void addTbThreeSalesRef(CUserInfo cUserInfo , String invateCode)
    {
        if(ValidateUtil.isEmpty(invateCode)){
            return;
        }
        //查询上级用户信息
        CUserInfo parentUserInfo = selectOne(new EntityWrapper<CUserInfo>().eq("invate_code",invateCode));
        if(ValidateUtil.isEmpty(parentUserInfo)){
            throw new CheckedException("邀请码错误，请核对");
        }
        //添加三级分销用户关系绑定表
        TbThreeSalesRef tbThreeSalesRef = new TbThreeSalesRef();
        tbThreeSalesRef.setUserId(cUserInfo.getId());
        tbThreeSalesRef.setParentUserId(parentUserInfo.getId());
        if (parentUserInfo.getUserAttribute().equals("1")){
            tbThreeSalesRef.setUserType(TbThreeSalesRef.USERTYPE_USER);
            tbThreeSalesRef.setParentUserType(TbThreeSalesRef.PARENT_USERTYPE_USER);
        }else{
            tbThreeSalesRef.setUserType(TbThreeSalesRef.USERTYPE_MERCHANT);
            tbThreeSalesRef.setParentUserType(TbThreeSalesRef.PARENT_USERTYPE_MERCHANT);
        }
        iTbThreeSalesRefService.insert(tbThreeSalesRef);
    }

    /**
     * 添加TbUserAccount表和关联表
     * @param c
     */
    private TbUserAccount CAddAcount(CUserInfo c){
        //添加TbUserAccount表
        TbUserAccount tbUserAccount = new TbUserAccount();
        tbUserAccount.setMobile(c.getMobile());
        tbUserAccount.setAccount(c.getMobile());
        tbUserAccount.setUserTypeId(TbUserAccount.USERTYPEID_USER);
        tbUserAccount.setCreateDate(new Date());
        iTbUserAccountService.insert(tbUserAccount);
        //添加关联表
        TbUserAccountTypeCon tbUserAccountTypeCon = new TbUserAccountTypeCon();
        tbUserAccountTypeCon.setAcctId(tbUserAccount.getId());
        tbUserAccountTypeCon.setUserType(TbUserAccountTypeCon.USERTYPE_USER);
        tbUserAccountTypeCon.setConId(c.getId());
        iTbUserAccountTypeConService.insert(tbUserAccountTypeCon);
        return tbUserAccount;
    }

    /**
     * 生成用户关系绑定表
     * @param userId 用户id
     */
    private void CAddCUserRefCon(String userId,String oid){
        OOperaOtherInfo oOperaOtherInfo = ioOperaOtherInfoService.selectOne(new EntityWrapper<OOperaOtherInfo>().eq("oid",oid));
        FFundendBind fFundendBind = ifFundendBindService.selectOne(new EntityWrapper<FFundendBind>().eq("oid",oOperaOtherInfo.getOid()).eq("status" , "1"));
        CUserRefCon cUserRefCon = new CUserRefCon();
        cUserRefCon.setUid(userId);
        cUserRefCon.setAuthStatus(CUserRefCon.AUTHSTATUS_N);
        cUserRefCon.setAuthExamine(CUserRefCon.AUTHEXAMINE_N);
        cUserRefCon.setCreateDate(new Date());
        cUserRefCon.setOid(oid);
        cUserRefCon.setFid(fFundendBind.getFid());
        cUserRefCon.setOoaDate(oOperaOtherInfo.getOoaDate());
        cUserRefCon.setRepayDate(oOperaOtherInfo.getRepayDate());
        icUserRefConService.insert(cUserRefCon);
    }

    /**
     * 生成商户信息表
     * @param mobile 手机号
     * @param password 密码
     * @return
     */
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    public MMerchantInfo addmMerchantInfo(String mobile , String password,String oId){
        MMerchantInfo mMerchantInfo = new MMerchantInfo();
        mMerchantInfo.setOid(oId);
        mMerchantInfo.setMobile(mobile);
        mMerchantInfo.setPwd(ENCODER.encode(password));
        mMerchantInfo.setStatus(MMerchantInfo.status_C);
        mMerchantInfo.setCreateDate(new Date());
        imMerchantInfoService.insert(mMerchantInfo);
        MMerchantOtherInfo mMerchantOtherInfo = new MMerchantOtherInfo();
        mMerchantOtherInfo.setMid(mMerchantInfo.getId());
        mMerchantOtherInfo.setZlStatus(MMerchantOtherInfo.ZLSTATUS_N);
        mMerchantOtherInfo.setSyauthExamine("0");
        imMerchantOtherInfoService.insert(mMerchantOtherInfo);
        //生成商户角色中间表
        TbPeopleRole tbPeopleRole = new TbPeopleRole();
        tbPeopleRole.setCreateDate(new Date());
        tbPeopleRole.setPeopleUserId(mMerchantInfo.getId());
        tbPeopleRole.setRoleId("5");
        tbPeopleRole.setPeopleUserType("1");
        iTbPeopleRoleService.insert(tbPeopleRole);
        return mMerchantInfo;
    }

    /**
     * 添加TbUserAccount表和关联表
     * @param c
     */
    private TbUserAccount MAddAcount(CUserInfo c,MMerchantInfo m,String password){
        //添加TbUserAccount表
        TbUserAccount tbUserAccount = new TbUserAccount();
        tbUserAccount.setMobile(c.getMobile());
        tbUserAccount.setAccount(c.getMobile());
        tbUserAccount.setUserTypeId(TbUserAccount.USERTYPEID_MERCHANT);
        tbUserAccount.setPassword(ENCODER.encode(password));
        tbUserAccount.setCreateDate(new Date());
        iTbUserAccountService.insert(tbUserAccount);
        //添加用户关联表
        TbUserAccountTypeCon tbUserAccountTypeCon = new TbUserAccountTypeCon();
        tbUserAccountTypeCon.setAcctId(tbUserAccount.getId());
        tbUserAccountTypeCon.setUserType(TbUserAccountTypeCon.USERTYPE_USER);
        tbUserAccountTypeCon.setConId(c.getId());
        iTbUserAccountTypeConService.insert(tbUserAccountTypeCon);
        //添加商户关联表
        TbUserAccountTypeCon userAccountTypeCon = new TbUserAccountTypeCon();
        userAccountTypeCon.setAcctId(tbUserAccount.getId());
        userAccountTypeCon.setUserType(TbUserAccountTypeCon.USERTYPE_MERCHANT);
        userAccountTypeCon.setConId(m.getId());
        iTbUserAccountTypeConService.insert(userAccountTypeCon);
        return tbUserAccount;
    }

    /**
     * 添加用户表
     * 根据有没有填写邀请码生成用户关系表
     * @param registerDto
     * @return
     */
    @Override
    @Transactional
    public TbUserAccount addCUserInfo(RegisterDto registerDto)
    {
        vaalidateaddCUserInfo(registerDto);
        //根据type值，1为添加用户
        //添加用户信息表
        CUserInfo c = addUser(registerDto);
            //填写了邀请码添加用户关系绑定表，没填就不加
        addTbThreeSalesRef(c,registerDto.getInvateCode());

        OOperaInfo operaInfo = ioOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("osn",registerDto.getOpsn1()));
        if(ValidateUtil.isEmpty(operaInfo)){
            throw new CheckedException("未找到运营商信息，暂不能注册");
        }
        if (RegisterDto.TYPE_USER.equals(registerDto.getType())){
            //添加TbUserAccount表和关联表
            TbUserAccount userAccount = CAddAcount(c);
            //生成钱包
            iTbWalletService.savewallet(userAccount.getId(),1,"",userAccount.getMobile(),"");
            //生成用户关系绑定表
            CAddCUserRefCon(c.getId(),operaInfo.getId());
            return userAccount;
        }
        //商户生成merchant表
        MMerchantInfo m = addmMerchantInfo(registerDto.getMobile() , registerDto.getPassword(),operaInfo.getId());
        //用户表和商户表和关联表关联
        TbUserAccount userAccount = MAddAcount(c , m,registerDto.getPassword());
        //生成钱包
        iTbWalletService.savewallet(userAccount.getId(),2,"",userAccount.getMobile(),"");
        //生成用户关系绑定表
        CAddCUserRefCon(c.getId(),operaInfo.getId());;
        return userAccount;
    }

    /**
     * 注册数据验证
     * @param registerDto
     */
    private void vaalidateaddCUserInfo(RegisterDto registerDto) {
        //判断验证码
        if (!messageUtil.valiCode(registerDto.getMobile(), registerDto.getYcode(), MessageUtil.YZM))
        {
            throw new CheckedException("验证码错误");
        }
        //判断手机号有没有注册
        CUserInfo info = selectOne(new EntityWrapper<CUserInfo>().eq("mobile",registerDto.getMobile()));
        if(!ValidateUtil.isEmpty(info)){
            throw new CheckedException("该手机号码已被注册");
        }
    }

    /**
     * 修改支付密码
     * @param registerDto
     * @return
     */
    @Override
    @Transactional
    public boolean registerPlanTwo(RegisterDto registerDto) {
        //第三步修改支付密码
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("con_id",registerDto.getId()));
        TbUserAccount tbUserAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("id",tbUserAccountTypeCon.getAcctId()));
        String password = ENCODER.encode(registerDto.getNewpwd());
        tbUserAccount.setTranPwd(password);
        boolean b = iTbUserAccountService.updateById(tbUserAccount);
        return b;
    }

    /**
     * 修改用户详情信息
     * @param cUserInfoDto
     * @return
     */
    @Override
    @Transactional
    @CacheEvict(value = "account_info", key = "#accountId  + '_account_info'")
    public boolean updateUserInfoDetal(String accountId , CUserInfoDto cUserInfoDto) {
        CUserInfo cUserInfo = new CUserInfo();
        cUserInfo.setId(cUserInfoDto.getId());
        cUserInfo.setRealName(cUserInfoDto.getRealName());
        cUserInfo.setHeadImg(cUserInfoDto.getHeadImg());
        cUserInfo.setSex(cUserInfoDto.getSex());
        cUserInfo.setBirthday(cUserInfoDto.getBirthday());
        cUserInfo.setModifyDate(new Date());
        cUserInfoMapper.updateById(cUserInfo);
        CUserOtherInfo cUserOtherInfo = icUserOtherInfoService.selectOne(new EntityWrapper<CUserOtherInfo>().eq("uid",cUserInfoDto.getId()));
        cUserOtherInfo.setArea(cUserInfoDto.getArea());
        cUserOtherInfo.setAddress(cUserInfoDto.getAddress());
        TbWallet tbWallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid" , accountId));
        tbWallet.setUname(cUserInfoDto.getRealName());
        tbWallet.setWname(cUserInfoDto.getRealName());
        iTbWalletService.updateById(tbWallet);
        icUserOtherInfoService.updateById(cUserOtherInfo);
        return true;
    }

    /**
     * 查询用户详情其他信息
     * @param userId
     * @return
     */
    @Override
    public CUserInfoDto selectUserInfo(String userId) {
        CUserInfoDto cUserInfoDto =cUserInfoMapper.selectUserInfoDetal(userId);
        return cUserInfoDto;
    }
}
