package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.CUserOtherInfo;
import com.tcxf.hbc.mc.mapper.CUserOtherInfoMapper;
import com.tcxf.hbc.mc.service.ICUserOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消费者用户其他信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class CUserOtherInfoServiceImpl extends ServiceImpl<CUserOtherInfoMapper, CUserOtherInfo> implements ICUserOtherInfoService {

}
