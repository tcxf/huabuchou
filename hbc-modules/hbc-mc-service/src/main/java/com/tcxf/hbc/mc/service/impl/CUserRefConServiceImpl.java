package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.tcxf.hbc.mc.mapper.CUserRefConMapper;
import com.tcxf.hbc.mc.model.dto.RegisterDto;
import com.tcxf.hbc.mc.service.ICUserRefConService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.mc.service.IOOperaInfoService;
import com.tcxf.hbc.mc.service.ITbUserAccountService;
import com.tcxf.hbc.mc.service.ITbUserAccountTypeConService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class CUserRefConServiceImpl extends ServiceImpl<CUserRefConMapper, CUserRefCon> implements ICUserRefConService {

}
