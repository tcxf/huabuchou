package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.mc.mapper.FFundendBindMapper;
import com.tcxf.hbc.mc.service.IFFundendBindService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金端、运营商绑定关系表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class FFundendBindServiceImpl extends ServiceImpl<FFundendBindMapper, FFundendBind> implements IFFundendBindService {

}
