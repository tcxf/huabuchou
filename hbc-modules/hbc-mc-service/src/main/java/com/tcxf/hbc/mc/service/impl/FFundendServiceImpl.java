package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.FFundend;
import com.tcxf.hbc.mc.mapper.FFundendMapper;
import com.tcxf.hbc.mc.service.IFFundendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金端信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class FFundendServiceImpl extends ServiceImpl<FFundendMapper, FFundend> implements IFFundendService {

}
