package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.IndexBanner;
import com.tcxf.hbc.mc.mapper.IndexBannerMapper;
import com.tcxf.hbc.mc.model.dto.BannerDto;
import com.tcxf.hbc.mc.service.IIndexBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页轮播图表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class IndexBannerServiceImpl extends ServiceImpl<IndexBannerMapper, IndexBanner> implements IIndexBannerService {

    @Autowired
    public IndexBannerMapper indexBannerMapper;


    @Override
    public BannerDto Querybanner(String id) {
        return indexBannerMapper.Querybanner(id);
    }
}
