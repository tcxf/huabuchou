package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MHotSearch;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.MHotSearchMapper;
import com.tcxf.hbc.mc.model.dto.MHotSearchDto;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.IMHotSearchService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 热搜店铺表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MHotSearchServiceImpl extends ServiceImpl<MHotSearchMapper, MHotSearch> implements IMHotSearchService {
    @Autowired
    public MHotSearchMapper mHotSearchMapper;

    @Override
    public Page grabbleHandlerNames(Query<MHotSearchDto> query) {
        List<MHotSearchDto> list=mHotSearchMapper.grabbleHandlerNames(query.getCondition());
        List<MHotSearchDto> list2=mHotSearchMapper.QuerySHS(query.getCondition());
        list.addAll(list2);
        return query.setRecords(list);
    }

    @Override
    public Page QuerySHS(Query <MHotSearchDto> query) {
        List<MHotSearchDto> list=mHotSearchMapper.grabbleHandlerNames(query.getCondition());
        return query.setRecords(list);
    }
}
