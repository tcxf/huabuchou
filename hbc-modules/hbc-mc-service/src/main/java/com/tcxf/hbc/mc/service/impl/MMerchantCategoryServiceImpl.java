package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MMerchantCategory;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.MMerchantCategoryMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.IMMerchantCategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商户行业分类表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MMerchantCategoryServiceImpl extends ServiceImpl<MMerchantCategoryMapper, MMerchantCategory> implements IMMerchantCategoryService {

    @Autowired
    private MMerchantCategoryMapper mMerchantCategoryMapper;
    @Override
    public Page QueryAll(Query<MMerchantCategory> query) {
        List<MMerchantCategory> list=mMerchantCategoryMapper.QueryAll(query.getCondition());
        return query.setRecords(list);
    }
}
