package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.CreditVo;
import com.tcxf.hbc.mc.controller.bank.ThreelementController;
import com.tcxf.hbc.mc.mapper.MMerchantInfoMapper;
import com.tcxf.hbc.mc.model.dto.NearbyMerchantDto;
import com.tcxf.hbc.mc.model.vo.MerchantVo;
import com.tcxf.hbc.mc.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商户信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MMerchantInfoServiceImpl extends ServiceImpl<MMerchantInfoMapper, MMerchantInfo> implements IMMerchantInfoService {

    @Autowired
    MMerchantInfoMapper mMerchantInfoMapper;

    @Autowired
    private  IMMerchantInfoService imMerchantInfoService;

    @Autowired
    private IMMerchantCategoryService imMerchantCategoryService;

    @Autowired
    private IMMerchantOtherInfoService imMerchantOtherInfoService;

    @Autowired
    private ITbUserAccountTypeConService iTbUserAccountTypeConService;

    @Autowired
    private ITbUserAccountService iTbUserAccountService;

    @Autowired
    private ICUserInfoService icUserInfoService;

    @Autowired
    private ThreelementController threelementController;

    @Autowired
    private ITbWalletService iTbWalletService;

    /**
     * 附近商家
     * @param map
     * @return
     */
    @Override
    public Page findNearByList(Map<String,Object> map) {
        Map<String, Object> params = new HashMap<>();
        params.put("status", "0");
        params.put("lat",map.get("lat"));
        params.put("lng",map.get("lng"));
        params.put("page",map.get("page"));
        params.put("micId",map.get("micId"));
        Query query = new Query(params);
        List<NearbyMerchantDto> resources = mMerchantInfoMapper.findNearByList(query, params);
        query.setRecords(resources);
        return query;
    }

    /**
     * 查询商户详情
     * @param id
     * @return
     */
    @Override
    public  NearbyMerchantDto findById (String id){
        return mMerchantInfoMapper.findById(id);
    }

    /**
     * C 端查询商户店铺资料
     * @param id
     * @return
     */
    @Override
    public NearbyMerchantDto QueryMerchantInfo(String id) {
        return mMerchantInfoMapper.QueryMerchantInfo(id);
    }

    /**
     * 修改、添加店铺资料
     * @param m
     */
    @Override
    @Transactional
    public void updateStore(MerchantVo m) {
        boolean bo = imMerchantInfoService.updateById(m);
        MMerchantInfo merchant = imMerchantInfoService.selectOne(new EntityWrapper <MMerchantInfo>().eq("id", m.getId()));
        MMerchantOtherInfo merchantother = imMerchantOtherInfoService.selectOne(new EntityWrapper <MMerchantOtherInfo>().eq("mid", m.getId()));
        MMerchantCategory merchantcategory = imMerchantCategoryService.selectOne(new EntityWrapper <MMerchantCategory>().eq("id", m.getMicId()));
        //在用户表里面修改数据
        CUserInfo cUserInfo = icUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("id",m.getUid()));

        CreditVo creditCto = new CreditVo();
        creditCto.setMobile(m.getMobile());
        creditCto.setIdCard(m.getIdCard());
        creditCto.setUserName(m.getLegalName());
        R r2 = threelementController.operatorsCheck(creditCto);
        if (r2.getCode() == 1){
            throw new CheckedException("请输入正确的法人信息！");
        }
        cUserInfo.setRealName(m.getSimpleName());
        //编辑店铺基本信息
        merchant.setSimpleName(m.getSimpleName());        //店铺简称
        merchant.setName(m.getDname());        //店铺名称
        merchant.setIdCard(m.getIdCard());//法人身份证
        merchant.setLegalName(m.getLegalName());//法人名称.

        merchant.setLat(m.getLat());//设置经纬度
        merchant.setLng(m.getLng());

        merchant.setMicId(m.getMicId());//行业类型

        merchantother.setOpenDate(m.getOpenDate());//开店时间
        merchant.setMobile(m.getMobile());//店铺电话
        merchant.setArea(m.getArea());//地区
        merchant.setAddress(m.getAddress());//详细地址
        merchantother.setRecommendGoods(m.getRecommendGoods());//店铺介绍
        merchantother.setLocalPhoto(m.getLocalPhoto());
        merchantother.setLicensePic(m.getLicensePic());
        if(merchantother.getAuthExamine().equals("2") || merchantother.getAuthExamine().equals("3")){
            imMerchantOtherInfoService.updateById(merchantother);
        }else {
            merchantother.setAuthExamine("1");
            imMerchantOtherInfoService.updateById(merchantother);
        }
        icUserInfoService.updateById(cUserInfo);
        imMerchantCategoryService.updateById(merchantcategory);
        imMerchantInfoService.updateById(merchant);
        TbUserAccountTypeCon tbUserAccountTypeCon = iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("con_id" , merchant.getId()).eq("user_type",TbUserAccountTypeCon.USERTYPE_MERCHANT));
        TbWallet tbWallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid" , tbUserAccountTypeCon.getAcctId()));
        tbWallet.setUname(m.getLegalName());
        tbWallet.setWname(m.getName());
        iTbWalletService.updateById(tbWallet);
        //修改店铺资料清除缓存
        iTbUserAccountService.updateMerchantdeleteRedies(tbUserAccountTypeCon.getAcctId());
    }
}
