package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.mc.mapper.MMerchantOtherInfoMapper;
import com.tcxf.hbc.mc.service.IMMerchantOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户其他信息 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MMerchantOtherInfoServiceImpl extends ServiceImpl<MMerchantOtherInfoMapper, MMerchantOtherInfo> implements IMMerchantOtherInfoService {

}
