package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.mc.mapper.MRedPacketMapper;
import com.tcxf.hbc.mc.model.dto.RedPacketDto;
import com.tcxf.hbc.mc.service.IMRedPacketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户领取优惠券记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MRedPacketServiceImpl extends ServiceImpl<MRedPacketMapper, MRedPacket> implements IMRedPacketService {

    @Autowired
    MRedPacketMapper mRedPacketMapper;

    @Override
    public List<RedPacketDto> findByUserId(String userId){
        return  mRedPacketMapper.findByUserId(userId);
    }
}
