package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.common.entity.TbRole;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.MRedPacketSettingMapper;
import com.tcxf.hbc.mc.model.dto.RedSettingDto;
import com.tcxf.hbc.mc.service.IMRedPacketSettingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 优惠券表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class MRedPacketSettingServiceImpl extends ServiceImpl<MRedPacketSettingMapper, MRedPacketSetting> implements IMRedPacketSettingService {

    @Autowired
    private MRedPacketSettingMapper mRedPacketSettingMapper;

    @Override
    public Page selectPage(String mid){
        Map<String,Object> map = new HashMap<>();
        map.put("status", "0");
        Query query = new Query(map);
        List<MRedPacketSetting> resources = mRedPacketSettingMapper.selectPage(query, new EntityWrapper<MRedPacketSetting>().eq("mi_id",mid));
        query.setRecords(resources);
        return query;

    }

    /**
     * 查询商户发布的优惠券，及领取信息
     * @param miid
     * @return
     */
    @Override
    public List<RedSettingDto> findList(String miid){
        return mRedPacketSettingMapper.findList(miid);
    }

    /**
     * 查询商户下的优惠券，及领取状态
     * @param map
     * @return
     */
    @Override
    public List<RedSettingDto> findByUserId(Map<String,Object> map){
        return  mRedPacketSettingMapper.findByUserId(map);
    }
    /**
     * 查询商户下的优惠券，及领取状态
     * @param map
     * @return
     */
    @Override
    public Page findMlist(Map<String, Object> map) {
        Map<String,Object> maps = new HashMap<>();
        maps.put("page",map.get("page"));
        maps.put("limit",map.get("limit"));
        Query query = new Query(maps);
        List<RedSettingDto> mList = mRedPacketSettingMapper.findMList(query, map);
        query.setRecords(mList);
        return query;
    }

    /**
     * 首页展示优惠券
     * @param query
     * @return
     */
    @Override
    public Page Querypacket(Query <RedSettingDto> query) {
       List<RedSettingDto> list=mRedPacketSettingMapper.Querypacket(query.getCondition());
        return query.setRecords(list);
    }

    /**
     * 首页更多更具商户精选优惠券
     * @param query
     * @return
     */
    @Override
    public Page Querypacketss(Query <RedSettingDto> query) {
        List<RedSettingDto> list=mRedPacketSettingMapper.Querypacketss(query,query.getCondition());
        return query.setRecords(list);
    }

    /**
     * 未等路优惠券
     * @param query
     * @return
     */
    @Override
    public Page Nopacket(Query <RedSettingDto> query) {
        List<RedSettingDto> list=mRedPacketSettingMapper.Nopacket(query.getCondition());
        return query.setRecords(list);
    }


}
