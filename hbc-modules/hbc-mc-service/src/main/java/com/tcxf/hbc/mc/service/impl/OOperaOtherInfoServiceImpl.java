package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.OOperaOtherInfo;
import com.tcxf.hbc.mc.mapper.OOperaOtherInfoMapper;
import com.tcxf.hbc.mc.service.IOOperaOtherInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商其他信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class OOperaOtherInfoServiceImpl extends ServiceImpl<OOperaOtherInfoMapper, OOperaOtherInfo> implements IOOperaOtherInfoService {

}
