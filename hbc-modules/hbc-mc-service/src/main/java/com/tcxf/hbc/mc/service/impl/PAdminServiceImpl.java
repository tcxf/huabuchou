package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.PAdmin;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.PAdminMapper;
import com.tcxf.hbc.mc.service.IPAdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台用户信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class PAdminServiceImpl extends ServiceImpl<PAdminMapper, PAdmin> implements IPAdminService {
    @Autowired
    private  PAdminMapper pAdminMapper;

    @Override
    public Page findAdminCondition(Map<String,Object> map) {
        Map<String, Object> params = new HashMap<>();
        params.put("status","0");
        params.put("page",map.get("page"));
        Query<PAdmin> query = new  Query(map);
        String name = null;
        Map<String,Object> map1 = new HashMap<>();
        map1.put("adminName",map.get("name"));
        map1.put("aid",map.get("aid"));
        List<PAdmin> admin= pAdminMapper.findByConductor(query,map1);
        query.setRecords(admin);
        return query;
    }
}
