package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.mc.mapper.TbAccountInfoMapper;
import com.tcxf.hbc.mc.service.ITbAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 用户授信资金额度表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbAccountInfoServiceImpl extends ServiceImpl<TbAccountInfoMapper, TbAccountInfo> implements ITbAccountInfoService {

    @Autowired
    TbAccountInfoMapper tbAccountInfoMapper;

    public TbAccountInfo getTbAccountInfoByUid(HashMap<String, Object> hashMap){
        return tbAccountInfoMapper.getTbAccountInfoByUid(hashMap);
    }
}
