package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.tcxf.hbc.mc.mapper.TbAccountTradingLogMapper;
import com.tcxf.hbc.mc.service.ITbAccountTradingLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信资金流水 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbAccountTradingLogServiceImpl extends ServiceImpl<TbAccountTradingLogMapper, TbAccountTradingLog> implements ITbAccountTradingLogService {

}
