package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbArea;
import com.tcxf.hbc.mc.mapper.TbAreaMapper;
import com.tcxf.hbc.mc.service.ITbAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbAreaServiceImpl extends ServiceImpl<TbAreaMapper, TbArea> implements ITbAreaService {

}
