package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.TbBindCardInfoMapper;
import com.tcxf.hbc.mc.service.ITbBindCardInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 绑卡信息 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbBindCardInfoServiceImpl extends ServiceImpl<TbBindCardInfoMapper, TbBindCardInfo> implements ITbBindCardInfoService {


@Autowired
public TbBindCardInfoMapper  tbBindCardInfoMapper;

    @Override
    public Page QueryMAllBank(Map<String, Object> map) {
        Query<TbBindCardInfo> query = new Query<>(map);
        List<TbBindCardInfo> tbBindCardInfos = tbBindCardInfoMapper.QueryMAllBank(query, query.getCondition());
        query.setRecords(tbBindCardInfos);
        return query;
    }

    @Override
    public List<TbBindCardInfo>  QueryUAllBank(Map<String,Object> map) {
        List<TbBindCardInfo> tbBindCardInfos = tbBindCardInfoMapper.QueryUAllBank(map);
        return tbBindCardInfos;
    }

    @Override
    public TbBindCardInfo QueryBank(Map<String, Object> map) {
        TbBindCardInfo cardInfo = tbBindCardInfoMapper.QueryBank(map);
        return cardInfo;
    }

    @Override
    public TbBindCardInfo selectTbBindCardInfoByOid(String id) {
        return tbBindCardInfoMapper.selectTbBindCardInfoByOid(id);
    }

}
