package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbCollect;
import com.tcxf.hbc.mc.mapper.TbCollectMapper;
import com.tcxf.hbc.mc.model.dto.TbCollectDto;
import com.tcxf.hbc.mc.service.ITbCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 我的收藏表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-05
 */
@Service
public class TbCollectServiceImpl extends ServiceImpl<TbCollectMapper, TbCollect> implements ITbCollectService {
    @Autowired
    private TbCollectMapper tbCollectMapper;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 13:48 2018/7/6
     * 查询当前用户收藏
    */
    @Override
    public List<TbCollectDto> findAllcollects(String uid) {
        ArrayList<TbCollectDto> allList = new ArrayList<>();
        List<TbCollectDto> collects = tbCollectMapper.findAllcollects(uid);//查询有优惠券的商户并且通过的
        List<TbCollectDto> allCollects = tbCollectMapper.findNotCouponCollects(uid);//查询无优惠券的商户
        List<TbCollectDto> allCollectsNotPass = tbCollectMapper.findAllcollectsNotPass(uid);//查询有优惠券的商户没有通过的


        if(collects !=null && collects.size()>0 &&allCollectsNotPass !=null && allCollectsNotPass.size()>0){
            for (int i = 0; i < collects.size(); i++) {
                TbCollectDto tbCollectDto1 = collects.get(i);
                //商户即发布已通过的也有未没通的  删除未通过的
                for (int j = 0; j <allCollectsNotPass.size() ; j++) {
                    TbCollectDto tbCollectDto = allCollectsNotPass.get(j);
                    if(tbCollectDto1.getMid().equals(tbCollectDto.getMid()) && !tbCollectDto1.getStatus().equals(tbCollectDto.getStatus())){
                        allCollectsNotPass.remove(j);
                    }
                }
            }

        }

        if(null != collects && collects.size()>0){
            allList.addAll(collects);
        }
        if(null != allCollects && allCollects.size()>0){
            allList.addAll(allCollects);
        }
        if(null != allCollectsNotPass && allCollectsNotPass.size()>0){
            allList.addAll(allCollectsNotPass);
        }


        return  allList;
    }
}
