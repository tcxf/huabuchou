package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbImproveQuota;
import com.tcxf.hbc.mc.mapper.TbImproveQuotaMapper;
import com.tcxf.hbc.mc.service.ITbImproveQuotaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消费者用户提额授信结果表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbImproveQuotaServiceImpl extends ServiceImpl<TbImproveQuotaMapper, TbImproveQuota> implements ITbImproveQuotaService {

}
