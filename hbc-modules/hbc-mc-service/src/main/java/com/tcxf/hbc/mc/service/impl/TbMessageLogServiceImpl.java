package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbMessageLog;
import com.tcxf.hbc.mc.mapper.TbMessageLogMapper;
import com.tcxf.hbc.mc.service.ITbMessageLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 短信记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbMessageLogServiceImpl extends ServiceImpl<TbMessageLogMapper, TbMessageLog> implements ITbMessageLogService {

}
