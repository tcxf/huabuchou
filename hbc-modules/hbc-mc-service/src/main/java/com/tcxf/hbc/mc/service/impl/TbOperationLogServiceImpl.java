package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbOperationLog;
import com.tcxf.hbc.mc.mapper.TbOperationLogMapper;
import com.tcxf.hbc.mc.service.ITbOperationLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbOperationLogServiceImpl extends ServiceImpl<TbOperationLogMapper, TbOperationLog> implements ITbOperationLogService {

}
