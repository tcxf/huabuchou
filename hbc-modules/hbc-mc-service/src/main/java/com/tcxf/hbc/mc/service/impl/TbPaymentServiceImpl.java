package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbPayment;
import com.tcxf.hbc.mc.mapper.TbPaymentMapper;
import com.tcxf.hbc.mc.service.ITbPaymentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbPaymentServiceImpl extends ServiceImpl<TbPaymentMapper, TbPayment> implements ITbPaymentService {

}
