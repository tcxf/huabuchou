package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbPeopleRole;
import com.tcxf.hbc.mc.mapper.TbPeopleRoleMapper;
import com.tcxf.hbc.mc.service.ITbPeopleRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户角色关系表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbPeopleRoleServiceImpl extends ServiceImpl<TbPeopleRoleMapper, TbPeopleRole> implements ITbPeopleRoleService {

}
