package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.mc.common.util.DateUtil;
import com.tcxf.hbc.mc.mapper.TbRepayLogMapper;
import com.tcxf.hbc.mc.mapper.TbRepayMapper;
import com.tcxf.hbc.mc.mapper.TbRepaymentDetailMapper;
import com.tcxf.hbc.mc.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;
import com.tcxf.hbc.mc.service.ITbRepayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepayLogServiceImpl extends ServiceImpl<TbRepayLogMapper, TbRepayLog> implements ITbRepayLogService {

    @Autowired
    private TbRepayLogMapper  tbRepayLogMapper;

    @Autowired
    private TbRepayMapper tbRepayMapper;

    @Autowired
    private TbRepaymentDetailMapper tbRepaymentDetailMapper;

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:58 2018/7/20
     * 查询当前用户提前还款记录
    */
    @Override
    public List<TbRepaymentDetailDto> findAllAdvancePay(Map<String, Object> paramMap) {
        return tbRepayLogMapper.findAllAdvancePay(paramMap);
    }

    @Override
    @Transactional
    public void addinitPayBackInfo(PaybackMoneyDto paybackMoneyDto) {
       /* List<TbRepayLog> paybackMoneys = paybackMoneyDto.getPaybackMoney(); //分期还款
        String rdid = paybackMoneyDto.getRdid(); //未分期账单id
        String oid = paybackMoneyDto.getOid();  //运营商id
        String uid = paybackMoneyDto.getUid(); //用户id


        BigDecimal totalMoney = new BigDecimal("0");
        BigDecimal unplanTotalMoney = new BigDecimal("0");
        BigDecimal planTotalMoney = new BigDecimal("0");

        if(!"0".equals(rdid) || paybackMoneys.size()>0){

            //计算还款总金额
            if(!"0".equals(rdid)){
                TbRepaymentPlan paramdto = new TbRepaymentPlan();
                paramdto.setRdId(rdid);
                TbRepaymentPlan tbRepaymentPlan = tbRepaymentPlanMapper.selectOne(paramdto);
                unplanTotalMoney = tbRepaymentPlan.getTotalCorpus();
            }
            if(paybackMoneys.size()>0){
                for (TbRepayLog tbRepayLog:paybackMoneys) {
                    planTotalMoney = tbRepayLog.getAmount().add(planTotalMoney);
                }
            }
            totalMoney=planTotalMoney.add(unplanTotalMoney);

            //插入主表
            TbRepay tbRepay = new TbRepay();
            tbRepay.setTotalMoney(totalMoney);
            tbRepay.setRepaySn("");//生成编号
            tbRepay.setUid(uid);
            tbRepayMapper.insert(tbRepay);
            String id = tbRepay.getId();

            //repay_log表里面添加明细
            if(!"0".equals(rdid)){
                TbRepaymentPlan paramDto = new TbRepaymentPlan();
                TbRepaymentDetail paramVo= new TbRepaymentDetail();
                paramDto.setRdId(rdid);
                paramVo.setId(rdid);
                TbRepaymentPlan tbRepaymentPlan = tbRepaymentPlanMapper.selectOne(paramDto);
                TbRepaymentDetail tbRepaymentDetail = tbRepaymentDetailMapper.selectOne(paramVo);

                TbRepayLog tbRepayLog = new TbRepayLog();
                tbRepayLog.setAmount(tbRepaymentPlan.getTotalCorpus());
                tbRepayLog.setPlanId(tbRepaymentPlan.getId());
                tbRepayLog.setUid(uid);
                tbRepayLog.setBillDate(DateUtil.getDateTimeforMonth(tbRepaymentPlan.getRepaymentDate()));
                tbRepayLog.setRepaySn(tbRepaymentDetail.getRepaymentSn());
                tbRepayLog.setType("2");//账单还款
                tbRepayLog.setCreateDate(new Date());
                tbRepayLog.setPaymentType("");//支付类型(支付宝/网银/微信)
                tbRepayLog.setUtype("");//商户、用户

                tbRepayLogMapper.insert(tbRepayLog);
            }

            //repay_log表里面添加明细
            if(paybackMoneys.size()>0){
                for (TbRepayLog tbRepayLog:paybackMoneys) {
                    tbRepayLogMapper.insert(tbRepayLog);
                }
            }

        }*/

    }



}
