package com.tcxf.hbc.mc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.mc.mapper.*;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;
import com.tcxf.hbc.mc.service.ITbRepaymentDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 还款表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepaymentDetailServiceImpl extends ServiceImpl<TbRepaymentDetailMapper, TbRepaymentDetail> implements ITbRepaymentDetailService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private TbRepaymentDetailMapper tbRepaymentDetailMapper;
    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;
    @Autowired
    private TbRepayLogMapper tbRepayLogMapper;
    @Autowired
    private TbTradingDetailMapper tbTradingDetailMapper;
    @Autowired
    private TbRepaymentRatioDictMapper tbRepaymentRatioDictMapper;

    @Override
    public List<TbRepaymentPlan> getDiffSplitAmount(String uid,String oid,String fid,String splitMon) {
        List<TbRepaymentPlan> list = new ArrayList<TbRepaymentPlan>();
        HashMap<String,Object> map = new HashMap<String,Object>();
        map.put("uid",uid);
        map.put("oid",oid);
        map.put("fid",fid);
        TbRepaymentDetail rd = tbRepaymentDetailMapper.findRepaymentDetailById(map);
        TbRepaymentRatioDict tr = tbRepaymentRatioDictMapper.findDictMyOid(map);
        BigDecimal fee= Calculate.multiply(tr.getServiceFee(),BigDecimal.valueOf(0.01)).getAmount() ;//分期利率
        BigDecimal splitMonth = BigDecimal.valueOf(Long.parseLong(splitMon));
        BigDecimal totalCorpus = rd.getTotalCorpus();
        Calculate ca[]=  Calculate.divideAndRemainder(totalCorpus,splitMonth);//本金
        BigDecimal oneCorpus = Calculate.add(ca[0].getAmount(),ca[1].getAmount()).getAmount();//第一期本金
//        Calculate c[]=  Calculate.divideAndRemainder(Calculate.multiply(totalCorpus,fee).getAmount(),splitMonth);//利息
        BigDecimal totalFeeDetail = Calculate.multiply(Calculate.multiply(totalCorpus,fee).getAmount(),splitMonth).getAmount() ;
        BigDecimal oneFee = Calculate.multiply(totalCorpus,fee).getAmount();//第一期利息
//        tbRepaymentPlanMapper.delete(new EntityWrapper<TbRepaymentPlan>().eq("rd_id",rd.getId()));
        for (int i = 0; i < splitMonth.intValue(); i++) {
            TbRepaymentPlan t = new TbRepaymentPlan();
            t.setId(IdWorker.getIdStr());
            t.setUid(map.get("uid").toString());
            t.setUtype("1");
            t.setTotalTimes(splitMonth.intValue());
            t.setCurrentTimes(i+1);
            t.setTotalCorpus(rd.getTotalCorpus());
            t.setTotalFee(totalFeeDetail);
            t.setRestCorpus(totalCorpus);
            BigDecimal restFee = Calculate.subtract(totalCorpus,Calculate.subtract(totalCorpus,Calculate.multiply(totalCorpus,fee).getAmount()).getAmount()).getAmount();
            t.setRestFee(restFee);
            if(i==0)
            {
                t.setCurrentCorpus(oneCorpus);
                t.setCurrentFee(oneFee);
            }
            else
            {
                t.setCurrentCorpus(ca[0].getAmount());
                t.setCurrentFee(oneFee);
            }
            t.setLateFee(BigDecimal.valueOf(0l));
            Calendar cal = Calendar.getInstance();
            cal.setTime(rd.getRepayDate());
            cal.add(Calendar.MONTH, i);

            Date date = cal.getTime();
            t.setRepaymentDate(date);

            t.setRdId(rd.getId());
            t.setStatus("2");
            t.setYqRate(tr.getYqRate());
            t.setCreateDate(new Date());
            list.add(t);
        }

        logger.error("==================分期账单信息:===================="+JSON.toJSONString(list));
        return list;
    }

    @Override
    @Transactional
    public void updateSplitRepamentDetail(Map<String, Object> map) {
        try {
            TbRepaymentDetail rd = tbRepaymentDetailMapper.findRepaymentDetailById(map);
            TbRepaymentRatioDict tr = tbRepaymentRatioDictMapper.findDictMyOid(map);
            BigDecimal fee= Calculate.multiply(tr.getServiceFee(),BigDecimal.valueOf(0.01)).getAmount() ;//分期利率
            BigDecimal splitMonth = BigDecimal.valueOf(Integer.parseInt(map.get("splitMonth").toString()));
            BigDecimal totalCorpus = rd.getTotalCorpus();
            Calculate ca[]=  Calculate.divideAndRemainder(totalCorpus,splitMonth);//本金
            BigDecimal oneCorpus = Calculate.add(ca[0].getAmount(),ca[1].getAmount()).getAmount();//第一期本金
//            Calculate c[]=  Calculate.divideAndRemainder(Calculate.multiply(totalCorpus,fee).getAmount(),splitMonth);//利息
            BigDecimal oneFee = Calculate.multiply(totalCorpus,fee).getAmount();//第一期利息
            List<TbRepaymentPlan> list = new ArrayList<TbRepaymentPlan>();
            tbRepaymentPlanMapper.delete(new EntityWrapper<TbRepaymentPlan>().eq("rd_id",rd.getId()));
            int temp=1;
            BigDecimal totalFeeDetail = Calculate.multiply(Calculate.multiply(totalCorpus,fee).getAmount(),splitMonth).getAmount() ;
            for (int i = 0; i < splitMonth.intValue(); i++) {
                TbRepaymentPlan t = new TbRepaymentPlan();
                t.setId(IdWorker.getIdStr());
                t.setUid(map.get("uid").toString());
                t.setTotalTimes(splitMonth.intValue());
                t.setUtype("1");
                t.setCurrentTimes(i+1);
                t.setTotalCorpus(rd.getTotalCorpus());
                t.setTotalFee(totalFeeDetail);
                t.setRestCorpus(totalCorpus);
                t.setSerialNo("R" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + StringUtil.RadomCode(4, "other").toUpperCase() + "00"+temp++);
                BigDecimal restFee = Calculate.subtract(totalCorpus,Calculate.subtract(totalCorpus,Calculate.multiply(totalCorpus,fee).getAmount()).getAmount()).getAmount();
                t.setRestFee(restFee);
                if(i==0)
                {
                    t.setCurrentCorpus(oneCorpus);
                    t.setCurrentFee(oneFee);
                }
                else
                {
                    t.setCurrentCorpus(ca[0].getAmount());
                    t.setCurrentFee(oneFee);
                }
                t.setLateFee(BigDecimal.valueOf(0l));
                Calendar cal = Calendar.getInstance();
                cal.setTime(rd.getRepayDate());
                cal.add(Calendar.MONTH, i);
                Date date = cal.getTime();
                t.setRepaymentDate(date);
                t.setRdId(rd.getId());
                t.setStatus("2");
                t.setYqRate(tr.getYqRate());
                t.setCreateDate(new Date());
                tbRepaymentPlanMapper.insert(t);
            }
            rd.setIsSplit("1");
            rd.setCurrentTimes(1);
            rd.setSplitDate(new Date());
            rd.setTotalTimes(Integer.parseInt(map.get("splitMonth").toString()));
            rd.setTotalFee(totalFeeDetail);
            tbRepaymentDetailMapper.updateById(rd);

        } catch (NumberFormatException e) {
            logger.error("分期失败！:"+e.getMessage());
            throw  new RuntimeException();
        }


    }

    /**
     * 查询用户当月待还账单
     * @param map
     * @return
     */
    @Override
    public TbRepaymentDetail findRepaymentDetailById(Map<String, Object> map) {
        return  tbRepaymentDetailMapper.findRepaymentDetailById(map);
    }

    /**
     * 查询历史账单交易记录
     * @param uid
     * @param currentData
     * @return
     */
    @Override
    public List<TbTradingDetail> findExRepayTradeList(String uid, String currentData) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("uid",uid);
        map.put("currentData",currentData);
        return tbTradingDetailMapper.findExRepayTradeList(map);
    }

    @Override
    public List<TbRepaymentDetail> findExRepayList(Map<String, Object> map) {
        return null;
    }
    /**
     * 查询历史账单中的分期账单
     * @param uid
     * @param currentData
     * @return
     */
    public TbRepaymentDetail findExRepaymentIsSpilt(String uid,String oid ,String currentData)
    {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("uid",uid);
        map.put("oid",oid);
        map.put("currentData",currentData);
        return tbRepaymentDetailMapper.findExRepaymentIsSpilt(map);
    }

    /**
     * 查询历史账单中的提前还款记录
     * @param uid
     * @param currentData
     * @return
     */
    @Override
    public List<TbRepayLog> findExRepayLogList(String uid, String currentData) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("uid",uid);
        map.put("currentData",currentData);
        return tbRepayLogMapper.findExRepayLogList(map);
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:25 2018/7/19
     * 查询分期账单的年份
    */
    @Override
    public List<Map<String,Object>> findPlanYears(Map<String,Object> map) {
        return tbRepaymentDetailMapper.findPlanYears(map);
    }


    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:44 2018/7/19
     * 根据月份查询分期账单
    */
    @Override
    public List<Map<String, Object>> findPlanBillByYear(Map<String,Object> mmp) {
        return tbRepaymentDetailMapper.findPlanBillByYear(mmp);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:47 2018/7/20
     * 查询用户当前不全是提前还款的记录
    */
    @Override
    public List<TbRepaymentDetailDto> findNotExactlyAdvancePay(Map<String, Object> paramMap) {
        return tbRepaymentDetailMapper.findNotExactlyAdvancePay(paramMap);
    }

}
