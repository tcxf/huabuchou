package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.mc.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.mc.model.dto.TbRepaymentDetailDto;
import com.tcxf.hbc.mc.service.ITbRepaymentPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款计划表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepaymentPlanServiceImpl extends ServiceImpl<TbRepaymentPlanMapper, TbRepaymentPlan> implements ITbRepaymentPlanService {
    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * 查询历史账单所有的年
     * @param uid
     * @return
     */
    public List<TbRepaymentDetailDto> findExRepaymentYear(String uid)
    {
        String sql = "" +
                " select distinct s.dataYear from (" +
                " select s1.dataYear from (" +
                " select distinct DATE_FORMAT(t.create_date, '%Y') as dataYear from" +
                " tb_repayment_detail t where t.uid=?" +
                " ) s1" +
                " union" +
                " select s2.dataYear from " +
                " (" +
                " select distinct DATE_FORMAT(b.pay_date, '%Y') as dataYear from tb_repay_log b" +
                " where b.uid=?" +
                " ) s2) as s";
        return jdbcTemplate.query(sql,new Object[]{uid,uid},new BeanPropertyRowMapper(TbRepaymentDetailDto.class));
    }

    /**
     * 查询历史账单
     * @param uid
     * @return
     */
    public List<TbRepaymentDetailDto> findExRepaymentList(String uid,String year)
    {
        String sql="select  aa.* from" +
                " (select sum(total_amount) as total_amount,tt.dataYear,tt.dataMonth,tt.uid,tt.id,tt.is_split,tt.status from (" +
                " select q.total_amount,q.dataMonth,q.dataYear,q.uid,q.id,q.is_split,q.status as status from " +
                " (" +
                " SELECT" +
                " DATE_FORMAT(t.create_date, '%Y') AS dataYear," +
                " DATE_FORMAT(t.create_date, '%m') AS dataMonth," +
                " t. STATUS," +
                " t.uid," +
                " t.id," +
                " t.is_split," +
                " sum(" +
                " IFNULL(t.total_corpus, 0) + IFNULL(t.total_fee, 0) + IFNULL(t.total_late_fee, 0)" +
                " ) AS total_amount" +
                " FROM" +
                " tb_repayment_detail t" +
                " " +
                " WHERE" +
                " t.uid = ? and DATE_FORMAT(t.create_date, '%Y')=?" +
                "  # and s.uid=? " +
                "  #and DATE_FORMAT(t.create_date, '%Y') = s.B_Year" +
                "  #and DATE_FORMAT(t.create_date, '%m') = s.B_Month" +
                " GROUP BY" +
                " DATE_FORMAT(t.create_date, '%Y')," +
                " DATE_FORMAT(t.create_date, '%m')" +
                " ORDER BY" +
                " DATE_FORMAT(t.create_date, '%Y')," +
                " DATE_FORMAT(t.create_date, '%m') DESC" +
                " )as q" +
                " union" +
                " select s.actual_amount as total_amount,s.B_Month as dataMonth,s.B_Year as dataYear,s.uid, '' as id,'' as is_split,'' as status   from" +
                " (" +
                " SELECT" +
                " a.actual_amount as actual_amount," +
                " a.uid," +
                " DATE_FORMAT(b.pay_date, '%Y') AS B_Year," +
                " DATE_FORMAT(b.pay_date, '%m') AS B_Month" +
                " FROM" +
                " tb_trading_detail a,tb_repay_log b" +
                " WHERE" +
                " a.payment_status = '1'" +
                " AND a.uid = ? AND b.uid=? " +
                " AND a.rid = b.id" +
                " ) as s  " +
                " ) as tt " +
                " group by tt.dataYear,tt.dataMonth ) as aa, tb_repayment_detail bb" +
                " where aa.uid = bb.uid group by aa.id,aa.status,aa.is_split,aa.dataYear,aa.dataMonth order by aa.dataYear,aa.dataMonth desc" +
                "  ";
        return jdbcTemplate.query(sql,new Object[]{uid,year,uid,uid,uid},new BeanPropertyRowMapper(TbRepaymentDetailDto.class));
    }

    /**
     * 查询分期账单
     * @param map
     * @return
     */
    public List<TbRepaymentPlan> findExSplitRepayList(Map<String,Object> map )
    {
        return tbRepaymentPlanMapper.findExSplitRepayList(map);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:27 2018/7/21
     *分期账单 查询剩余分期总额以及当前应还总额
    */
    @Override
    public Map<String, Object> findRemainPlanAndCurrentMoney(Map<String,Object> map) {
        return tbRepaymentPlanMapper.findRemainPlanAndCurrentMoney(map);
    }

    /**
     * 查询提前还款账单
     * @param map
     * @return
     */
    @Override
    public List<TbRepaymentPlan> findExRepayList(Map<String, Object> map) {
        return tbRepaymentPlanMapper.findExRepayList(map);
    }



    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:46 2018/7/13
     *查询待还总金额
    */
    @Override
    public Map<String, Object> findTotalWaitPayMoney(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findTotalWaitPayMoney(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 8:55 2018/7/13
     * 判断是否存在逾期未还记录
    */
    @Override
    public List<Map<String, Object>> findExsitOverdue(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findExsitOverdue(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:23 2018/7/13
     * 如果存在分期逾期记录 查询用户的逾期记录
    */
    @Override
    public Map<String, Object> findPlanOverDueBill(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findPlanOverDueBill(paramMap);
    }

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 9:45 2018/7/13
     * 查询当月分期还款记录
     */
    @Override
    public List<Map<String, Object>>  findCurrentMonthPayPlan(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findCurrentMonthPayPlan(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 10:55 2018/7/13
     * 查询当月的还款表主键id
    */
    @Override
    public String findCurrentMonthSetllmentId(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findCurrentMonthSetllmentId(paramMap);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 17:08 2018/7/13
     * 查询逾期或者待还详情
    */
    @Override
    public Map<String, Object> findPlanDetailInfo(String planId) {
        return tbRepaymentPlanMapper.findPlanDetailInfo(planId);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:23 2018/7/17
     * 查询分期明细
    */
    @Override
    public List<Map<String, Object>> findDivideDetail(String id) {
        return tbRepaymentPlanMapper.findDivideDetail(id);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:08 2018/7/17
     * 剩余分期金额
    */
    @Override
    public BigDecimal findRemainPlanMoney(String id) {
        return tbRepaymentPlanMapper.findRemainPlanMoney(id);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 14:08 2018/7/17
     * 本期应还金额
    */
    @Override
    public BigDecimal findCurrentMonthMoney(Map<String, Object> paramMap) {
        return tbRepaymentPlanMapper.findCurrentMonthMoney(paramMap);
    }




}
