package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbRepaymentRatioDict;
import com.tcxf.hbc.mc.mapper.TbRepaymentRatioDictMapper;
import com.tcxf.hbc.mc.service.ITbRepaymentRatioDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 还款比率字典表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepaymentRatioDictServiceImpl extends ServiceImpl<TbRepaymentRatioDictMapper, TbRepaymentRatioDict> implements ITbRepaymentRatioDictService {

    @Autowired
    private TbRepaymentRatioDictMapper tbRepaymentRatioDictMapper;
    @Override
    public TbRepaymentRatioDict findDictMyOid(Map<String, Object> map) {
        return tbRepaymentRatioDictMapper.findDictMyOid(map);
    }
}
