package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbResources;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.TbResourcesMapper;
import com.tcxf.hbc.mc.model.dto.TbResourcesDto;
import com.tcxf.hbc.mc.service.ITbResourcesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资源信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbResourcesServiceImpl extends ServiceImpl<TbResourcesMapper, TbResources> implements ITbResourcesService {

    @Autowired
    TbResourcesMapper resourcesMapper;

    /**
     * 根据角色id查询权限
     * @param id
     * @return
     */
    @Override
    public List<TbResources> selectResources(String id,int type) {
        Map<String , Object> map = new HashMap<>();
        map.put("id",id);
        map.put("type",type);
        List<TbResources> tbResources = resourcesMapper.selectResources(map);
        return tbResources;
    }

    /**
     * 根据条件分页查询
     * @param pmap
     * @return
     */
    @Override
    public Page getPage(Map<String,Object> pmap){
        Map<String,Object> map = new HashMap<>();
        map.put("status","0");
        map.put("page",pmap.get("page"));
        Query<TbResourcesDto> query = new Query(map);
        List<TbResourcesDto> tb= resourcesMapper.getList(query,pmap);
        query.setRecords(tb);
        return query;
    }
}
