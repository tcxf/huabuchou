package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbScoreLog;
import com.tcxf.hbc.mc.mapper.TbScoreLogMapper;
import com.tcxf.hbc.mc.service.ITbScoreLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户积分明细表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbScoreLogServiceImpl extends ServiceImpl<TbScoreLogMapper, TbScoreLog> implements ITbScoreLogService {

}
