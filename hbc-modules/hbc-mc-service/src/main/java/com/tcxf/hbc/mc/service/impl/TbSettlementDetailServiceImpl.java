package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbSettlementDetail;
import com.tcxf.hbc.mc.mapper.TbSettlementDetailMapper;
import com.tcxf.hbc.mc.service.ITbSettlementDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  交易结算表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbSettlementDetailServiceImpl extends ServiceImpl<TbSettlementDetailMapper, TbSettlementDetail> implements ITbSettlementDetailService {

}
