package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbSettlementLog;
import com.tcxf.hbc.mc.mapper.TbSettlementLogMapper;
import com.tcxf.hbc.mc.service.ITbSettlementLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  结算支付记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbSettlementLogServiceImpl extends ServiceImpl<TbSettlementLogMapper, TbSettlementLog> implements ITbSettlementLogService {

}
