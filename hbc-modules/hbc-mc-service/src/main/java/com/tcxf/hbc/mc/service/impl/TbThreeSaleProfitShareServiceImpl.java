package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.tcxf.hbc.mc.mapper.TbThreeSaleProfitShareMapper;
import com.tcxf.hbc.mc.service.ITbThreeSaleProfitShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销利润分配表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbThreeSaleProfitShareServiceImpl extends ServiceImpl<TbThreeSaleProfitShareMapper, TbThreeSaleProfitShare> implements ITbThreeSaleProfitShareService {

}
