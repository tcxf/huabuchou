package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.tcxf.hbc.mc.mapper.TbThreeSalesRateMapper;
import com.tcxf.hbc.mc.service.ITbThreeSalesRateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销利润分配比例表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbThreeSalesRateServiceImpl extends ServiceImpl<TbThreeSalesRateMapper, TbThreeSalesRate> implements ITbThreeSalesRateService {

}
