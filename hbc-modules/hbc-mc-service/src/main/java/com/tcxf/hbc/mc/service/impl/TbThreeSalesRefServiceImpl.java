package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbThreeSalesRef;
import com.tcxf.hbc.mc.mapper.TbThreeSalesRefMapper;
import com.tcxf.hbc.mc.service.ITbThreeSalesRefService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销用户关系绑定表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbThreeSalesRefServiceImpl extends ServiceImpl<TbThreeSalesRefMapper, TbThreeSalesRef> implements ITbThreeSalesRefService {

}
