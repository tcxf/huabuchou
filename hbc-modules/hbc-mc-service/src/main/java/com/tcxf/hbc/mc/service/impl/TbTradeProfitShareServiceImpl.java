package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbTradeProfitShare;
import com.tcxf.hbc.mc.mapper.TbTradeProfitShareMapper;
import com.tcxf.hbc.mc.service.ITbTradeProfitShareService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易利润分配表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbTradeProfitShareServiceImpl extends ServiceImpl<TbTradeProfitShareMapper, TbTradeProfitShare> implements ITbTradeProfitShareService {

}
