package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.common.util.DateUtil;
import com.tcxf.hbc.mc.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.mc.mapper.TbTradingDetailMapper;
import com.tcxf.hbc.mc.model.dto.TradingDetaDto;
import com.tcxf.hbc.mc.model.dto.TradingStaticDto;
import com.tcxf.hbc.mc.service.ITbTradingDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 用户交易记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbTradingDetailServiceImpl extends ServiceImpl<TbTradingDetailMapper, TbTradingDetail> implements ITbTradingDetailService {

    private static final String DATE_FORMAT      = "yyyy-MM-dd";

    @Autowired
    TbTradingDetailMapper tbTradingDetailMapper;
    @Autowired
    TbRepaymentPlanMapper tbRepaymentPlanMapper;

    
    /**
    * @Author:
    * @Description
    * @Date: 14:30 2018/7/20
     * 查询历史账单的年份 ----new
    */
    @Override
    public List<String> findUserTradeYear(Map<String, Object> map) {

        List<Map<String,Object>> listT =  tbTradingDetailMapper.findUserTradeYear(map);
        List<Map<String,Object>> listD = tbTradingDetailMapper.findUserPaymentYear(map);
        List<String> list = new ArrayList<String>();
        if(null !=listT && listT.size()>0){
            for (Map<String,Object> mmp:listT) {
                list.add((String) mmp.get("serialNo"));
            }
        }
        if(null !=listD && listD.size()>0){
            for (Map<String,Object> mmp:listD) {
                list.add((String) mmp.get("serialNo"));
            }
        }
        List newList = new ArrayList(new TreeSet(list));//去重复
        newList.sort(Comparator.reverseOrder());
        return newList;
    }

    public static void main(String []args )
    {
        List<String> list = new ArrayList<String>();
        list.add("2018");
        list.add("2016");
        list.add("2017");
        list.add("2018");
        list.add("2019");
        List newList = new ArrayList(new TreeSet(list));
        newList.sort(Comparator.reverseOrder());
        System.out.print(newList.get(0));
    }

    /**
     * 查询用户历史账单
     * @param map
     * @return
     */
    public List<TbRepaymentPlan> findExRepayList(Map<String,Object> map)
    {
        return tbRepaymentPlanMapper.findExRepayList(map);
    }

    /**
     * 查询用户未出账单
     * @param date
     * @param uid
     * @return
     */
    public TbTradingDetail findExTradeByDate(Date date,String uid)
    {
        HashMap<String,Object> map =new HashMap<String, Object>();
        map.put("date",date);
        map.put("uid",uid);
        return tbTradingDetailMapper.findExTradeByDate(map);
    }

    /**
     * 查询用户未出账单总金额
     * @param date
     * @param uid
     * @return
     */
    public TbTradingDetail findExTradeByDateSum(Date date,String uid)
    {
        HashMap<String,Object> map =new HashMap<String, Object>();
        map.put("date",date);
        map.put("uid",uid);
        return tbTradingDetailMapper.findExTradeByDateSum(map);
    }

    /**
     * 查询用户订单
     * @param map
     * @return
     */
    public Page findByUid(Map<String,Object> map){
        map.put("status","0");
        Query query = new Query(map);
        List<TradingDetaDto> list = tbTradingDetailMapper.findByUid(query,map);
        query.setRecords(list);
        return  query;
    }

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    public TradingDetaDto findById(String id){
        return  tbTradingDetailMapper.findById(id);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 19:50 2018/7/16
     * 查询本月未出账单金额
    */
    @Override
    public BigDecimal findUnOutTotalMoney(Map<String, Object> mmp) {
        return tbTradingDetailMapper.findUnOutTotalMoney(mmp);
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:36 2018/7/17
     * 查询提前还款总金额
    */
    @Override
    public BigDecimal findAdvanceTotalMoney(Map<String, Object> paramMap) {
        return tbTradingDetailMapper.findAdvanceTotalMoney(paramMap);
    }


    public TradingStaticDto findStati(String mid){return  tbTradingDetailMapper.findStati(mid);}

    public Page findTradingDeta(Map<String,Object> map){
        map.put("status","0");
        Query query = new Query(map);
        List<TradingDetaDto> list = tbTradingDetailMapper.findTradingDeta(query,map);
        query.setRecords(list);
        return  query;
    }

    public Page findSettlementDeta(Map<String ,Object> map){
        map.put("statuc","0");
        Query query = new Query(map);
        List<TradingDetaDto> list  = tbTradingDetailMapper.findSettlementDeta(query,query.getCondition());
        query.setRecords(list);
        return  query;
    }


    @Override
    public List<Map<String, Object>> findsevendaytrade( Map<String, Object> mmp) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, -6);
        Date startDate = c.getTime();

        //开始时间
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String startT= sdf.format(startDate);
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> paramMap = new HashMap<>();
        for (int i = 0; i <7 ; i++) {
            String daytime = DateUtil.addDay(startT, i);
            paramMap.put("daytime",daytime);
            paramMap.put("mid",mmp.get("mid"));
            Map<String,Object> map=tbTradingDetailMapper.findsevendaytrade(paramMap);//写sqli
            list.add(map);
        }
        return list;
    }
}
