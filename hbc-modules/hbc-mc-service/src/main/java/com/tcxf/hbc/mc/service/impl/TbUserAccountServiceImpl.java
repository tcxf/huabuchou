package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.mapper.TbUserAccountMapper;
import com.tcxf.hbc.mc.model.dto.LoginDto;
import com.tcxf.hbc.mc.model.dto.UserAccountDto;
import com.tcxf.hbc.mc.model.dto.UserInfoDto;
import com.tcxf.hbc.mc.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbUserAccountServiceImpl extends ServiceImpl<TbUserAccountMapper, TbUserAccount> implements ITbUserAccountService {
    @Autowired
    private TbUserAccountMapper tbUserAccountMapper;
    @Autowired
    private ICUserInfoService icUserInfoService;
    @Autowired
    private IMMerchantInfoService imMerchantInfoService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private ICUserRefConService icUserRefConService;
    @Autowired
    private IOOperaInfoService ioOperaInfoService;
    @Autowired
    private IMMerchantOtherInfoService imMerchantOtherInfoService;
    @Autowired
    private ITbUserAccountTypeConService iTbUserAccountTypeConService;
    @Autowired
    private ITbPeopleRoleService iTbPeopleRoleService;
    @Autowired
    private ITbUserAccountService iTbUserAccountService;
    @Autowired
    private ITbWalletService iTbWalletService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    /**login
     * 登陆
     * @param loginDto
     * @return
     */
    @Override
    public String login(LoginDto loginDto)
    {
        if (LoginDto.TYPE_USER.equals(loginDto.getType()))
        {
            if (!messageUtil.valiCode(loginDto.getMobile(), loginDto.getYcode(), MessageUtil.YZM)) {
                throw  new CheckedException("验证码错误");
            }
            TbUserAccount tbUserAccount = new TbUserAccount();
            tbUserAccount.setAccount(loginDto.getMobile());
            TbUserAccount userAccount = tbUserAccountMapper.selectOne(tbUserAccount);
            if(!ValidateUtil.isEmpty(userAccount)){
                return userAccount.getId();
            }
            throw  new CheckedException("账号不存在，请点击下方立即注册进行注册");
        }
        TbUserAccount tbUserAccount = new TbUserAccount();
        tbUserAccount.setAccount(loginDto.getMobile());
        tbUserAccount.setUserTypeId(TbUserAccount.USERTYPEID_MERCHANT);
        TbUserAccount userAccount = tbUserAccountMapper.selectOne(tbUserAccount);
        if(ValidateUtil.isEmpty(userAccount)||!ENCODER.matches(loginDto.getPwd(),userAccount.getPassword())){
            throw new CheckedException("账号或密码错误");
        }
        return userAccount.getId();
    }

    /**
     * 登陆取值
     * @param accountId
     * @return
     */
    @Override
    @Cacheable(value = "account_info", key = "#accountId  + '_account_info'")
    public UserInfoDto selectUserInfo(String accountId) {
        UserAccountDto userAccountDtos = tbUserAccountMapper.selectUserInfo(accountId);
        UserInfoDto userInfoDto = new UserInfoDto();
        TbUserAccount tbUserAccount = new TbUserAccount();
        tbUserAccount.setId(userAccountDtos.getId());
        tbUserAccount.setAccount(userAccountDtos.getAccount());
        tbUserAccount.setPassword(userAccountDtos.getPassword());
        tbUserAccount.setUserTypeId(userAccountDtos.getUserTypeId());
        tbUserAccount.setMobile(userAccountDtos.getMobile());
        tbUserAccount.setTranPwd(userAccountDtos.getTranPwd());
        tbUserAccount.setCreateDate(userAccountDtos.getCreateDate());
        tbUserAccount.setModifyDate(userAccountDtos.getModifyDate());
        userInfoDto.setTbUserAccount(tbUserAccount);
        List<TbUserAccountTypeCon> list = userAccountDtos.getTbUserAccountTypeConList();
        for (TbUserAccountTypeCon tbUserAccountTypeCon:list) {
            if (TbUserAccountTypeCon.USERTYPE_USER.equals(tbUserAccountTypeCon.getUserType()))
            {
                CUserInfo cUserInfo = icUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("id",tbUserAccountTypeCon.getConId()));
                if (ValidateUtil.isEmpty(cUserInfo)){
                    throw new CheckedException("系统异常");
                }
                userInfoDto.setcUserInfo(cUserInfo);

                CUserRefCon cUserRefCon = icUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid",tbUserAccountTypeCon.getConId()));
                if (ValidateUtil.isEmpty(cUserRefCon)){
                    throw new CheckedException("系统异常");
                }
                userInfoDto.setcUserRefCon(cUserRefCon);
            }else {
                MMerchantInfo mMerchantInfo = imMerchantInfoService.selectOne(new EntityWrapper<MMerchantInfo>().eq("id",tbUserAccountTypeCon.getConId()));
                if (ValidateUtil.isEmpty(mMerchantInfo)){
                    throw new CheckedException("系统异常");
                }
                userInfoDto.setmMerchantInfo(mMerchantInfo);
                MMerchantOtherInfo mMerchantOtherInfo = imMerchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",mMerchantInfo.getId()));
                if (ValidateUtil.isEmpty(mMerchantOtherInfo)){
                    throw new CheckedException("系统异常");
                }
                userInfoDto.setmMerchantOtherInfo(mMerchantOtherInfo);
            }
        }
        return userInfoDto;
    }

    /**
     * 修改支付密码,删除缓存
     * @param tbUserAccount
     * @return
     */
    @Override
    @CacheEvict(value = "account_info", key = "#accountId  + '_account_info'")
    public boolean updatePaymentCode(TbUserAccount tbUserAccount) {
        tbUserAccountMapper.updateById(tbUserAccount);
        return true;
    }

    /**
     * 用户立即开店
     * @param accountId
     * @param password
     * @param oid
     */
    @Override
    @Transactional
    @CacheEvict(value = "account_info", key = "#accountId  + '_account_info'")
    public String userSetPassword(String accountId, String password , String oid) {
        try {
            //修改accountInfo表
            TbUserAccount userAccount = iTbUserAccountService.selectOne(new EntityWrapper<TbUserAccount>().eq("id",accountId));
            //做一个防重复提交
            if (TbUserAccount.USERTYPEID_MERCHANT.equals(userAccount.getUserTypeId()))
            {
                return "请勿重复提交";
            }
            TbWallet tbWallet = iTbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid",accountId));
            tbWallet.setUtype("2");
            iTbWalletService.updateById(tbWallet);
            TbUserAccount tbUserAccount = selectOne(new EntityWrapper<TbUserAccount>().eq("id" , accountId));
            tbUserAccount.setId(accountId);
            tbUserAccount.setPassword(ENCODER.encode(password));
            tbUserAccount.setUserTypeId(TbUserAccount.USERTYPEID_MERCHANT);
            updateById(tbUserAccount);
            //查询运营商
            OOperaInfo operaInfo = ioOperaInfoService.selectOne(new EntityWrapper<OOperaInfo>().eq("id" , oid));
            //添加商户表
            MMerchantInfo mMerchantInfo = new MMerchantInfo();
            mMerchantInfo.setOid(operaInfo.getId());
            mMerchantInfo.setMobile(tbUserAccount.getMobile());
            mMerchantInfo.setPwd(ENCODER.encode(password));
            mMerchantInfo.setStatus(MMerchantInfo.status_C);
            mMerchantInfo.setCreateDate(new Date());
            imMerchantInfoService.insert(mMerchantInfo);
            MMerchantOtherInfo mMerchantOtherInfo = new MMerchantOtherInfo();
            mMerchantOtherInfo.setMid(mMerchantInfo.getId());
            mMerchantOtherInfo.setZlStatus(MMerchantOtherInfo.ZLSTATUS_N);
            mMerchantOtherInfo.setSyauthExamine("0");
            imMerchantOtherInfoService.insert(mMerchantOtherInfo);
            TbPeopleRole tbPeopleRole = new TbPeopleRole();
            tbPeopleRole.setCreateDate(new Date());
            tbPeopleRole.setPeopleUserId(mMerchantInfo.getId());
            tbPeopleRole.setRoleId("5");
            tbPeopleRole.setPeopleUserType("1");
            iTbPeopleRoleService.insert(tbPeopleRole);
            //添加商户和accountInfo关联表
            TbUserAccountTypeCon tbUserAccountTypeCon = new TbUserAccountTypeCon();
            tbUserAccountTypeCon.setAcctId(tbUserAccount.getId());
            tbUserAccountTypeCon.setUserType(TbUserAccountTypeCon.USERTYPE_MERCHANT);
            tbUserAccountTypeCon.setConId(mMerchantInfo.getId());
            iTbUserAccountTypeConService.insert(tbUserAccountTypeCon);
            //修改CUserInfo表属性
            TbUserAccountTypeCon tbUserAccountTypeCon1 = iTbUserAccountTypeConService.selectOne(new EntityWrapper<TbUserAccountTypeCon>().eq("acct_id",tbUserAccount.getId()).eq("user_type",TbUserAccountTypeCon.USERTYPE_USER));
            CUserInfo cUserInfo = new CUserInfo();
            cUserInfo.setId(tbUserAccountTypeCon1.getConId());
            cUserInfo.setUserAttribute(CUserInfo.USERATTRIBUTE_MERCHANT);
            deleteredis(cUserInfo.getId());
            icUserInfoService.updateById(cUserInfo);
            return mMerchantInfo.getId();
        }catch (Exception e){
            throw new CheckedException("立即开店错误");
        }
    }

    @CacheEvict(value = "user_info", key = "#userId  + '_user_info'")
    public void deleteredis(String userId){

    }

    @Override
    @CacheEvict(value = "account_info", key = "#accountId  + '_account_info'")
    public void updateMerchantdeleteRedies(String accountId) {

    }

    /**
     * 退出登陆，清除缓存，清除openId
     * @param accountId
     * @param userId
     */
    @Override
    @CacheEvict(value = "account_info", key = "#accountId  + '_account_info'")
    public void logout(String accountId, String userId) {
        CUserInfo cUserInfo = icUserInfoService.selectOne(new EntityWrapper<CUserInfo>().eq("id",userId));
        cUserInfo.setWxOpenid("");
        icUserInfoService.updateById(cUserInfo);
    }
}
