package com.tcxf.hbc.mc.service.impl;

import com.tcxf.hbc.common.entity.TbUserAccountTypeCon;
import com.tcxf.hbc.mc.mapper.TbUserAccountTypeConMapper;
import com.tcxf.hbc.mc.service.ITbUserAccountTypeConService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户类型表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbUserAccountTypeConServiceImpl extends ServiceImpl<TbUserAccountTypeConMapper, TbUserAccountTypeCon> implements ITbUserAccountTypeConService {

}
