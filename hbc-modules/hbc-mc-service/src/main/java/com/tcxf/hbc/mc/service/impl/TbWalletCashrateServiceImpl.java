package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWalletCashrate;

import com.tcxf.hbc.mc.mapper.TbWalletCashrateMapper;
import com.tcxf.hbc.mc.mapper.TbWalletIncomeMapper;
import com.tcxf.hbc.mc.service.ITbWalletCashrateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-08-09
 */
@Service
public class TbWalletCashrateServiceImpl extends ServiceImpl<TbWalletCashrateMapper, TbWalletCashrate> implements ITbWalletCashrateService {
    @Autowired
    private TbWalletCashrateMapper tbWalletCashrateMapper;

    @Override
    public TbWalletCashrate findBYuser(Map<String, Object> condition) {
        TbWalletCashrate bYuser = tbWalletCashrateMapper.findBYuser(condition);
        return bYuser;
    }

    @Override
    public List<TbWalletCashrate> findBYmerchant(Map<String, Object> condition) {
        List<TbWalletCashrate> bYmerchant = tbWalletCashrateMapper.findBYmerchant(condition);
        return bYmerchant;
    }
}
