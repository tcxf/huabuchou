package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.TbWalletIncomeMapper;
import com.tcxf.hbc.mc.service.ITbWalletIncomeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包收支明细表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbWalletIncomeServiceImpl extends ServiceImpl<TbWalletIncomeMapper, TbWalletIncome> implements ITbWalletIncomeService {
    @Autowired
    private TbWalletIncomeMapper tbWalletIncome;

    @Override
    public Page getMWalletIncomeList(Map<String, Object> map) {
        Query<TbWalletIncome> query = new Query<>(map);
        List<TbWalletIncome> list= tbWalletIncome.getWalletIncomeList(query,query.getCondition());
        query.setRecords(list);
        return query;

    }

    @Override
    public List<TbWalletIncome> getUWalletIncomeList(Map<String, Object> map) {
        List<TbWalletIncome> walletIncome = tbWalletIncome.getWalletIncome(map);
        return walletIncome;
    }
}
