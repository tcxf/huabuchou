package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbBindCardInfo;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.message.MessageUtil;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.RandomUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.mc.mapper.TbWalletMapper;
import com.tcxf.hbc.mc.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 资金钱包表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbWalletServiceImpl extends ServiceImpl<TbWalletMapper, TbWallet> implements ITbWalletService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TbWalletMapper tbWalletMapper;

    @Autowired
    protected MessageUtil messageUtil;

    @Autowired
    private ITbWalletService iTbWalletService;

    @Autowired
    protected ITbBindCardInfoService iTbBindCardInfoService;

    @Autowired
    private ITbWalletIncomeService iTbWalletIncomeService;

    @Autowired
    protected ITbWalletWithdrawalsService iTbWalletWithdrawalsService;

    @Override
    public TbWallet getfindWallet(String id)
    {
        return tbWalletMapper.selectById(id);
    }

    @Override
    public TbWallet getfindOWallet(String oid) {
        TbWallet t= new TbWallet();
        t.setUid(oid);
        return tbWalletMapper.selectOne(t);
    }

    @Override
    public int savewallet(String id,int type,String name,String mobil,String wname )
    {
        TbWallet t= new TbWallet();
        t.setUid(id);
        t.setTotalAmount(new BigDecimal("0"));
        t.setWname(wname);
        t.setUname(name);
        t.setUmobil(mobil);
        t.setUtype(String.valueOf(type));
        t.setStuta(1);
        t.setCardNum(RandomUtil.generateLowerStr(14));
        t.setWithdrawFeeFlag("1");
        t.setSxje(new BigDecimal("2"));
        return tbWalletMapper.insert(t);
    }

    @Override
    @Transactional
    public R updateWalletInfo(Map<String, Object> map) {
        R<Object> objectR = new R<>();
        //查询提现需要的数据
        TbBindCardInfo info = iTbBindCardInfoService.selectOne(new EntityWrapper<TbBindCardInfo>().eq("id", map.get("bid")));
        TbWallet wallet = iTbWalletService.getfindOWallet(map.get("uid").toString());
        BigDecimal y =  Calculate.subtract(wallet.getTotalAmount(),new BigDecimal(map.get("txje").toString())).getAmount();

        //调用短信验证方法验证
        if (!messageUtil.valiCode(info.getMobile(),map.get("yCode").toString() ,map.get("type").toString() )) {
            objectR.setMsg("短信验证码错误");
            objectR.setCode(R.FAIL);
            return objectR;
        }

        //验证钱包余额
        if (!Calculate.greaterOrEquals(wallet.getTotalAmount() , new BigDecimal(map.get("txje").toString()))){
            objectR.setMsg("钱包余额不足");
            objectR.setCode(R.FAIL);
            return objectR;
        }

        //验证通过插入一条提现申请记录
        try {
            TbWalletWithdrawals w = new TbWalletWithdrawals();
            w.setUid(map.get("uid").toString());
            w.setState("1");
            w.setApplyTime(new Date());
            w.setUname(info.getName());
            w.setUmobile(info.getMobile());
            w.setBankCardNo(info.getBankCard());
            w.setKhh(info.getBankName());
            w.setTxAmount(new BigDecimal(map.get("txje").toString()));
            w.setDzAmount(new BigDecimal(map.get("sjtxje").toString()) );
            w.setWname(wallet.getWname());//钱包名称
            w.setServiceFee(new BigDecimal(map.get("txsxje").toString()));
            iTbWalletWithdrawalsService.insert(w);

            //改变钱包的余额
            wallet.setTotalAmount(y);
            iTbWalletService.update(wallet, new EntityWrapper<TbWallet>().eq("id", wallet.getId()));

            //插入一条提现记录
            TbWalletIncome income = new TbWalletIncome();
            income.setAmount(new BigDecimal(map.get("txje").toString()));
            income.setWid(wallet.getId());
            income.setPlayid(map.get("uid").toString());
            income.setPname(wallet.getUname());
            income.setType("6");
            income.setTradeTotalAmount(new BigDecimal(map.get("txje").toString()));
            income.setPtepy(wallet.getUtype());
            income.setPmobile(wallet.getUmobil());
            income.setCreateDate(new Date());
            income.setSettlementStatus("1");
            income.setTid(wallet.getId());
            iTbWalletIncomeService.insert(income);

            objectR.setMsg("提现申请成功");
            objectR.setCode(R.SUCCESS);
        } catch (Exception e) {
            logger.error("提现申请失败",e);
            throw new CheckedException("提现申请失败");
        }
        return objectR;
    }
}
