package com.tcxf.hbc.mc.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.tcxf.hbc.common.entity.TbWalletWithdrawals;
import com.tcxf.hbc.common.util.Query;
import com.tcxf.hbc.mc.mapper.TbWalletWithdrawalsMapper;
import com.tcxf.hbc.mc.service.ITbWalletWithdrawalsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 资金钱包提现表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbWalletWithdrawalsServiceImpl extends ServiceImpl<TbWalletWithdrawalsMapper, TbWalletWithdrawals> implements ITbWalletWithdrawalsService {
    @Autowired
    private TbWalletWithdrawalsMapper tbWalletWithdrawalsMapper;

    /**
     * 钱包提现查询
     */
    @Override
    public List<TbWalletWithdrawals> findWalletwithdrawalsList(Map<String, Object> map) {
        List<TbWalletWithdrawals> list= tbWalletWithdrawalsMapper.findWalletwithdrawalsList(map);
        return list;
    }
    /**
     * 钱包提现分页查询
     */
    @Override
    public Page findMWalletwithdrawalsList(Map<String, Object> map) {
        Query<TbWalletWithdrawals> query = new Query<>(map);
        List<TbWalletWithdrawals> walletwithdrawals = tbWalletWithdrawalsMapper.findWalletwithdrawals(query, query.getCondition());
        query.setRecords(walletwithdrawals);
        return query;
    }

    @Override
    public int savaWallet(TbWalletWithdrawals t) {
        return tbWalletWithdrawalsMapper.insert(t);
    }

}
