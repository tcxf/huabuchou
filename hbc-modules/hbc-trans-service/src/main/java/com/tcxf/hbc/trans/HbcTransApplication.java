package com.tcxf.hbc.trans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhouyj
 * @date 2018/3/23
 */
@EnableAsync
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.tcxf.hbc.trans", "com.tcxf.hbc.common.bean"})
public class HbcTransApplication {
    public static void main(String[] args) {
        SpringApplication.run(HbcTransApplication.class, args);
    }
}