package com.tcxf.hbc.trans.controller.cuser;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.trans.service.ICUserInfoService;
import com.tcxf.hbc.trans.service.ITbUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 消费者账户信息
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-26
 */
@RestController
@RequestMapping("/userAccount")
public class CUserAccountController extends BaseController {


    @Autowired
    private ITbUserAccountService tbUserAccountService;
    /**
     * 查找用户账户信息信息
     */
    @RequestMapping("/selectUserAccount")
    @ResponseBody
    public R<TbUserAccount> selectUserAccount(String userId){
        TbUserAccount tbUserAccount = tbUserAccountService.selectUserInfoByUserId(userId);
        return R.<TbUserAccount>newOK(tbUserAccount);
    }

}
