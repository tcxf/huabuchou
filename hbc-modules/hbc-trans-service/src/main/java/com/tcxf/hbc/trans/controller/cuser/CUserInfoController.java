package com.tcxf.hbc.trans.controller.cuser;

import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.trans.service.ICUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 消费者用户表 前端控制器
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-26
 */
@RestController
@RequestMapping("/cUserInfo")
public class CUserInfoController extends BaseController {

    @Autowired private ICUserInfoService cUserInfoService;
    /**
     * 查找用户信息
     */
    @RequestMapping("/selectUserInfoByOpenId")
    @ResponseBody
    public R<CUserInfo> selectUserInfoByOpenId(String openId){
        CUserInfo cUserInfo = cUserInfoService.selectUserInfoByOpenId(openId);
        return R.<CUserInfo>newOK(cUserInfo);
    }

    /**
     * 查找用户信息
     */
    @RequestMapping("/selectUserInfoById")
    @ResponseBody
    public R<CUserInfo> selectUserInfoById(String userId){
        CUserInfo cUserInfo = cUserInfoService.selectById(userId);
        return R.<CUserInfo>newOK(cUserInfo);
    }

}
