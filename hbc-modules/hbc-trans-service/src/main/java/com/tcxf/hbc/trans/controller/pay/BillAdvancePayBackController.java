package com.tcxf.hbc.trans.controller.pay;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.common.entity.TbRepay;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.trans.service.ITbRepayLogService;
import com.tcxf.hbc.trans.service.ITbRepayService;
import com.tcxf.hbc.trans.service.ITbRepaymentPlanService;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmResData;
import com.tcxf.hbc.trans.util.token.TokenManager;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.TreeMap;

/**
 * @author YWT_tai
 * @Date :Created in 15:48 2018/8/3
 */

@RestController
@RequestMapping("/billAndAdvance")
public class BillAdvancePayBackController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ITbRepayLogService iTbRepayLogService;

    @Autowired
    private ITbRepayService iTbRepayService;

    @Autowired
    private ITbRepaymentPlanService iTbRepaymentPlanService;

    @Autowired
    private TokenManager tokenManager;

    /**
     * @Author:YWT_tai
     * @Description
     * @Date: 14:09 2018/7/30
     * 已出账单还款支付申请
     */
    @RequestMapping(value="/PaybackMoney",method ={RequestMethod.POST} )
    public R PaybackMoney(@RequestBody PaybackMoneyDto paybackMoneyDto){
        try {
            //插入还款记录 tb_repay 以及 还款明细 tb_repay_log
            PayagreeconfirmDto payagreeconfirmDto = iTbRepayLogService.addinitPayBackInfo(paybackMoneyDto);
            return R.newOK(payagreeconfirmDto);
        } catch (Exception e) {
            logger.error("PaybackMoney:",e);
            if(e instanceof CheckedException){
                return R.newErrorMsg(e.getMessage());
            }
            return R.newErrorMsg("还款失败，请稍后再试");
        }
    }

        /**
        * @Author:YWT_tai
        * @Description
        * @Date: 13:53 2018/8/6
         * 提前还款支付申请
        */
        @RequestMapping(value="/AdvancePaybackMoney",method ={RequestMethod.POST} )
        public R AdvancePaybackMoney(@RequestBody PaybackMoneyDto paybackMoneyDto){
            try {
                //插入还款记录 tb_repay 以及 还款明细 tb_repay_log
                PayagreeconfirmDto payagreeconfirmDto = iTbRepayLogService.advancePaybackMoney(paybackMoneyDto);
                return R.newOK(payagreeconfirmDto);
            } catch (Exception e) {
                logger.error("AdvancePaybackMoney:",e);
                if(e instanceof CheckedException){
                    return R.newErrorMsg(e.getMessage());
                }
                return R.newErrorMsg("还款失败，请稍后再试");
            }
        }


        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 13:53 2018/8/6
         * 提前还款支付成功回调 更新记录
         */
        @RequestMapping(value="/updateTradingInfo",method ={RequestMethod.POST} )
        public R updateTradingInfo(@RequestBody TreeMap<String, String> map){
            try {
                TbRepay tbRepay = iTbRepayService.selectOne(new EntityWrapper<TbRepay>().eq("id", map.get("cusorderid")));
                //支付失败
                if(!map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_0000)&&
                        !map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)&&
                        !map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                    tokenManager.deleteToken("TbRepay"+tbRepay.getUid());
                    return  R.newOK();
                }

                if(map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)||
                        map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                    tokenManager.createToken("TbRepay"+tbRepay.getUid(),map.get("trxstatus"),900L);
                    return  R.newOK();
                }

                String status = tbRepay.getStatus();
                if(StringUtils.equals(status,"1")){
                    return R.newOK();
                }
                PayagreeconfirmDto payagreeconfirmDto = new PayagreeconfirmDto();
                //CUserInfo  _cuserInfo = new CUserInfo();
                CUserInfo cUserInfo = new CUserInfo().selectOne(new EntityWrapper().eq("id", tbRepay.getUid()));
                payagreeconfirmDto.setOrderid(map.get("cusorderid"));
                payagreeconfirmDto.setTrxid(map.get("trxid"));
                payagreeconfirmDto.setOid(tbRepay.getOid());
                payagreeconfirmDto.setFid(tbRepay.getFid());
                payagreeconfirmDto.setUid(tbRepay.getUid());
                payagreeconfirmDto.setUtype(cUserInfo.getUserAttribute());
                iTbRepayLogService.updateTradingInfo(payagreeconfirmDto);
                tokenManager.deleteToken("TbRepay"+tbRepay.getUid());
                return R.newOK();
            } catch (Exception e) {
                logger.error("AdvancePaybackMoney:",e);
                return R.newErrorMsg("还款失败");
            }
        }

        /**
         * @Author:YWT_tai
         * @Description
         * @Date: 13:53 2018/8/6
         * 账单还款支付成功回调 更新记录
         */
        @RequestMapping(value="/updateNormalPay",method ={RequestMethod.POST} )
        public R updateNormalPay(@RequestBody TreeMap<String, String> map){
            try {
                TbRepay tbRepay = iTbRepayService.selectOne(new EntityWrapper<TbRepay>().eq("id", map.get("cusorderid")));

                //支付失败
                if(!map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_0000)&&
                        !map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)&&
                        !map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                    tokenManager.deleteToken("tbOutRepay"+tbRepay.getUid());
                    return  R.newOK();
                }
                if(map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)||
                        map.get("trxstatus").equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                    tokenManager.createToken("tbOutRepay"+tbRepay.getUid(),map.get("trxstatus"),900L);
                    return  R.newOK();
                }

                String status = tbRepay.getStatus();
                if(StringUtils.equals(status,"1")){
                    return R.newOK();
                }
                CUserInfo cUserInfo = new CUserInfo().selectOne(new EntityWrapper().eq("id", tbRepay.getUid()));
                PayagreeconfirmDto payagreeconfirmDto = new PayagreeconfirmDto();
                payagreeconfirmDto.setOrderid( map.get("cusorderid"));
                payagreeconfirmDto.setTrxid(map.get("trxid"));
                payagreeconfirmDto.setOid(tbRepay.getOid());
                payagreeconfirmDto.setFid(tbRepay.getFid());
                payagreeconfirmDto.setUid(tbRepay.getUid());
                payagreeconfirmDto.setUtype(cUserInfo.getUserAttribute());
                payagreeconfirmDto.setRdId(tbRepay.getRdid());
                payagreeconfirmDto.setSerialNo(tbRepay.getSerialNo());
                iTbRepayLogService.updateNormalPay(payagreeconfirmDto);
                tokenManager.deleteToken("tbOutRepay"+tbRepay.getUid());
                return R.newOK();
            } catch (Exception e) {
                logger.error("AdvancePaybackMoney:",e);
                return R.newErrorMsg("还款失败");
            }
        }


        /**
        * @Author:YWT_tai
        * @Description
        * @Date: 16:37 2018/8/8
         * 查询未分期的金额
        */
        @RequestMapping(value="/findRepaymentPlanByRdId/{rdid}",method ={RequestMethod.POST} )
        public R<TbRepaymentPlan> findRepaymentPlanByRdId(@PathVariable("rdid")String rdid){
            try {
                TbRepaymentPlan repaymentPlan = iTbRepaymentPlanService.selectOne(new EntityWrapper<TbRepaymentPlan>().eq("rd_id", rdid));
                return  R.newOK(repaymentPlan);
            } catch (Exception e) {
                logger.error("findRepaymentPlanByRdId:",e);
                return R.newError();
            }
        }




}
