package com.tcxf.hbc.trans.controller.pay;

import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.service.MMerchantInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:商户信息
 */
@RestController
@RequestMapping("/merchantInfo")
public class MMerchantInfoController {

    @Autowired
    private MMerchantInfoService mMerchantInfoService;

    /**
     * 查找商户信息
     */
    @RequestMapping("/selectmMerchantInfo")
    @ResponseBody
    public R<MMerchantInfo> selectmMerchantInfo(String mId){
        MMerchantInfo mMerchantInfo = mMerchantInfoService.selectById(mId);
        return R.<MMerchantInfo>newOK(mMerchantInfo);
    }
}
