package com.tcxf.hbc.trans.controller.pay;

import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.service.IMMerchantOtherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:商户其他信息
 */
@RestController
@RequestMapping("/merchantOtherInfo")
public class MMerchantOtherInfoController {

    @Autowired
    private IMMerchantOtherInfoService iMMerchantOtherInfoService;

    /**
     * 查找商户其他信息
     */
    @RequestMapping("/selectMMerchantOtherInfo")
    @ResponseBody
    public R<MMerchantOtherInfo> selectMMerchantOtherInfo(String mId){
        MMerchantOtherInfo selectMMerchantOtherInfo = iMMerchantOtherInfoService.selectByMId(mId);
        return R.<MMerchantOtherInfo>newOK(selectMMerchantOtherInfo);
    }
}
