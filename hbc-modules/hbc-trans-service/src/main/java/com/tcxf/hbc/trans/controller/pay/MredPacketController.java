package com.tcxf.hbc.trans.controller.pay;

import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.model.dto.MRedPacketDto;
import com.tcxf.hbc.trans.service.MredPacketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:优惠券
 */
@RestController
@RequestMapping("/mredPacket")
public class MredPacketController {

    @Autowired
    private MredPacketService mredPacketService;
    /**
     * 查找可用的优惠券
     */
    @RequestMapping("/selectRedPacketAvailable")
    @ResponseBody
    public R<List<MRedPacketDto>> selectRedPacketAvailable(String userId, String mId, String amount){
        List<MRedPacketDto> list = mredPacketService.selectRedPacketAvailable(userId,mId,amount);
        return R.<List<MRedPacketDto>>newOK(list);
    }
}
