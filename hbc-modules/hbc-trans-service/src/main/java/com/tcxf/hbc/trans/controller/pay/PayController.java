package com.tcxf.hbc.trans.controller.pay;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.Pair;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.trans.model.dto.PayDto;
import com.tcxf.hbc.trans.model.dto.QuickPayAgreeapplyDto;
import com.tcxf.hbc.trans.model.dto.WalletDto;
import com.tcxf.hbc.trans.service.ITbWalletIncomeService;
import com.tcxf.hbc.trans.service.ITbWalletService;
import com.tcxf.hbc.trans.service.PayService;
import com.tcxf.hbc.trans.tlPay.business.*;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.*;
import com.tcxf.hbc.trans.tlPay.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:支付
 */
@RestController
@RequestMapping("/pay")
public class PayController {

    @Autowired
    private PayService payService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private R quickPayAgreeapplyReqDataR;

    //钱包
    @Autowired
    private ITbWalletService tbWalletService;
    //钱包记录收支明细信息
    @Autowired
    private ITbWalletIncomeService tbWalletIncomeService;
    /**
     * 支付操作
     * @param payDto
     * @return
     */
    @RequestMapping("/onPay")
    @ResponseBody
    public R onPay(@RequestBody PayDto payDto){
        try {
            String payinfo = payService.onPay(payDto);
            return R.newOK(payinfo);
        }catch (Exception ex){
            log.error("trans controller 异常"+ ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newErrorMsg("支付失败");
        }
    }


    /**
     * 支付确认
     * @param payagreeconfirmDto
     * @return
     */
    @RequestMapping("/onPayagreeconfirm")
    @ResponseBody
    public R onPayagreeconfirm(@RequestBody PayagreeconfirmDto payagreeconfirmDto){

        try {

            QuickPayPayagreeconfirmReqData quickPayPayagreeconfirmReqData =
                    new QuickPayPayagreeconfirmReqData(payagreeconfirmDto.getOrderid(),
                            payagreeconfirmDto.getAgreeid(),payagreeconfirmDto.getSmscode(),payagreeconfirmDto.getThpinfo());
            new QuikPayPayagreeconfirmBusiness().run(quickPayPayagreeconfirmReqData, new QuikPayPayagreeconfirmBusiness.ResultListener() {
                @Override
                public void onQuickPayPayagreeconfirmFail(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData) {
                    Util.log(QuikPayPayagreeconfirmBusiness.result);
                    quickPayAgreeapplyReqDataR = R.newErrorMsg(ValidateUtil.isEmpty(quickPayPayagreeconfirmResData.getErrmsg())?"支付失败，请稍后再试":quickPayPayagreeconfirmResData.getErrmsg());
                }
                @Override
                public void onQuickPayPayagreeconfirmSuccess(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData) {
                    quickPayAgreeapplyReqDataR = R.newOK(quickPayPayagreeconfirmResData);
                }
            });

        } catch (Exception ex) {
            log.error("trans controller 异常"+ ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newErrorMsg("支付失败，请稍后再试");
        }
        return quickPayAgreeapplyReqDataR;
    }

    /**
     * 签约申请
     * @param quickPayAgreeapplyDto
     * @return
     */
    @RequestMapping("/onQuickPayAgreeapply")
    @ResponseBody
    public R onQuickPayAgreeapply(@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto){

        try {
            QuickPayAgreeapplyReqData quickPayAgreeapplyReqData =new QuickPayAgreeapplyReqData(quickPayAgreeapplyDto.getMeruserid(),
                    quickPayAgreeapplyDto.getAcctno(),quickPayAgreeapplyDto.getIdno(),quickPayAgreeapplyDto.getAcctname(),
                    quickPayAgreeapplyDto.getMobile());
            new QuickPayAgreeapplyBusiness().run(quickPayAgreeapplyReqData, new QuickPayAgreeapplyBusiness.ResultListener() {
                /**
                 * 签约申请失败处理
                 * @param quickPayAgreeapplyResData
                 */
                @Override
                public void onQuickPayAgreeapplyFail(QuickPayAgreeapplyResData quickPayAgreeapplyResData) {
                    Util.log(QuickPayAgreeapplyBusiness.result);
                    if(quickPayAgreeapplyResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)){
                        quickPayAgreeapplyReqDataR = R.newErrorMsg(quickPayAgreeapplyResData.getErrmsg());
                        return;
                    }
                    quickPayAgreeapplyReqDataR = R.newError();
                }
                /**
                 * 签约申请成功处理
                 * @param quickPayAgreeapplyResData
                 */
                @Override
                public void onQuickPayAgreeapplySuccess(QuickPayAgreeapplyResData quickPayAgreeapplyResData) {
                    quickPayAgreeapplyReqDataR = R.newOK(quickPayAgreeapplyResData.getThpinfo());
                }
            });

        } catch (Exception ex) {
            log.error("trans controller 异常"+ ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newErrorMsg("绑定失败");
        }
        return quickPayAgreeapplyReqDataR;
    }

    /**
     * 签约确认
     * @param quickPayAgreeapplyDto
     * @return
     */
    @RequestMapping("/onAgreeconfirm")
    @ResponseBody
    public R<QuickPayAgreeconfirmResData> onAgreeconfirm(@RequestBody QuickPayAgreeapplyDto quickPayAgreeapplyDto){

        try {
            QuickPayAgreeconfirmReqData quickPayAgreeconfirmReqData =new QuickPayAgreeconfirmReqData(quickPayAgreeapplyDto.getMeruserid(),
                    quickPayAgreeapplyDto.getAcctno(),quickPayAgreeapplyDto.getIdno(),quickPayAgreeapplyDto.getAcctname(),
                    quickPayAgreeapplyDto.getMobile(),quickPayAgreeapplyDto.getSmscode());
            new QuickPayAgreeconfirmBusiness().run(quickPayAgreeconfirmReqData, new QuickPayAgreeconfirmBusiness.ResultListener() {
                /**
                 * 签约确认失败处理
                 * @param quickPayAgreeconfirmResData
                 */
                @Override
                public void onQuickPayAgreeconfirmFail(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData) {
                    Util.log(QuickPayAgreeconfirmBusiness.result);
                    if(quickPayAgreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)){
                        quickPayAgreeapplyReqDataR = R.newErrorMsg(quickPayAgreeconfirmResData.getErrmsg());
                        return;
                    }
                    quickPayAgreeapplyReqDataR = R.newErrorMsg(ValidateUtil.isEmpty(quickPayAgreeconfirmResData.getRetmsg())?
                            "开通失败,请稍后再试":"开通失败，失败原因:"+quickPayAgreeconfirmResData.getRetmsg());
                }
                /**
                 * 签约确认成功处理
                 * @param quickPayAgreeconfirmResData
                 */
                @Override
                public void onQuickPayAgreeconfirmSuccess(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData) {
                    quickPayAgreeapplyReqDataR = R.newOK(quickPayAgreeconfirmResData);
                }
            });

        } catch (Exception ex) {
            log.error("trans controller 异常"+ ex);
            if(ex instanceof  CheckedException){
                return R.newErrorMsg(ex.getMessage());
            }
            return R.newErrorMsg("开通快捷支付失败");
        }
        return quickPayAgreeapplyReqDataR;
    }


    /**
     * 支付成功回调
     * @param map
     */
    @RequestMapping("/onpayNotify")
    @ResponseBody
    public R onpayNotify(@RequestBody TreeMap<String, String> map ){
        //VSP501：微信支付
        //VSP301-快捷支付
        try {
            String paytype = map.get("trxcode").equals(BaseQuickPayReqData.VSP501)?PayDto.PAYTYPE_WX:
                    map.get("trxcode").equals(BaseQuickPayReqData.VSP301)?PayDto.PAYTYPE_YL:"";
            payService.onpayNotify(map.get("cusorderid"),paytype,map,"");
            return R.newOK();
        }catch (Exception ex){
            log.error("trans controller 异常"+ ex);
            log.error("回调处理失败");
            return R.newError();
        }
    }


    /**
     * 钱包数据验证
     */
    @RequestMapping("/onCheckTbWallet")
    @ResponseBody
    public R onCheckTbWallet() {
        //查询所有钱包
        List<TbWallet> listTbWallet = tbWalletService.selectList(new EntityWrapper<>());
        //查询所有钱包明细
        List<TbWalletIncome> listTbWalletIncome = tbWalletIncomeService.selectList(new EntityWrapper<>());


        //根据wid分组 钱包明细
        Map<String,List<TbWalletIncome>> mapTbWalletIncome = new HashMap<String, List<TbWalletIncome>>();
        for(TbWalletIncome tbWalletIncome : listTbWalletIncome){
            if(!mapTbWalletIncome.containsKey(tbWalletIncome.getWid())){
                List<TbWalletIncome> tbWalletIncomeList = new ArrayList<TbWalletIncome>();
                tbWalletIncomeList.add(tbWalletIncome);
                mapTbWalletIncome.put(tbWalletIncome.getWid(),tbWalletIncomeList);
                continue;
            }
            mapTbWalletIncome.get(tbWalletIncome.getWid()).add(tbWalletIncome);
        }

        //根据wid分组 钱包
        Map<String,List<TbWallet>> mapTbWallet = new HashMap<String, List<TbWallet>>();
        for(TbWallet tbWallet : listTbWallet){
            if(!mapTbWallet.containsKey(tbWallet.getId())){
                List<TbWallet> tbWalletList = new ArrayList<TbWallet>();
                tbWalletList.add(tbWallet);
                mapTbWallet.put(tbWallet.getId(),tbWalletList);
                continue;
            }
            mapTbWallet.get(tbWallet.getId()).add(tbWallet);
        }


        //统计每个钱包明细已结算的入金额 和 出金额
        Map<String,Pair<String,String>> map = new HashMap<String,Pair<String,String>>();
        for(Map.Entry<String,List<TbWalletIncome>> ent : mapTbWalletIncome.entrySet()){
            BigDecimal addmount = new BigDecimal(0);
            BigDecimal subtractmount = new BigDecimal(0);
            for(TbWalletIncome t : ent.getValue()){
                if((t.getType().equals(TbWalletIncome.TYPE_1)||
                        t.getType().equals(TbWalletIncome.TYPE_2)||
                        t.getType().equals(TbWalletIncome.TYPE_3)||
                        t.getType().equals(TbWalletIncome.TYPE_4)||
                        t.getType().equals(TbWalletIncome.TYPE_5))&&t.getSettlementStatus().equals("1")){
                    addmount =  Calculate.add(addmount,t.getTradeTotalAmount()).getAmount();
                }
                if((t.getType().equals(TbWalletIncome.TYPE_6)||
                        t.getType().equals(TbWalletIncome.TYPE_7)||
                        t.getType().equals(TbWalletIncome.TYPE_8))&&t.getSettlementStatus().equals("1")){
                    subtractmount = Calculate.add(subtractmount,t.getTradeTotalAmount()).getAmount();
                }
            }
            map.put(ent.getKey(),new Pair<String,String>(addmount.toString(),subtractmount.toString()));

        }

        //数据统计
        List<WalletDto> list = new ArrayList<WalletDto>();
        for(Map.Entry<String,List<TbWallet>> ent : mapTbWallet.entrySet()){
            Pair<String,String> pair = map.get(ent.getKey());
            //钱包明细为空 钱包金额为0
            if(ValidateUtil.isEmpty(pair) && Calculate.equalsZero(ent.getValue().get(0).getTotalAmount())){
                continue;
            }
            WalletDto walletDto = new WalletDto();
            TbWallet tbWallet  =  tbWalletService.selectById(ent.getKey());
            //钱包明细为空 钱包金额不为0 则为异常
            if(ValidateUtil.isEmpty(pair) && !Calculate.equalsZero(ent.getValue().get(0).getTotalAmount())){
                walletDto.setWalletType(tbWallet.getUtype());
                walletDto.setuId(tbWallet.getUid());
                walletDto.setWalletId(ent.getKey());
                walletDto.setWalletMoney(ent.getValue().get(0).getTotalAmount());
                walletDto.setAddamount(new BigDecimal(0));
                walletDto.setSubtractmount(new BigDecimal(0));
                list.add(walletDto);
                continue;
            }
            //入金额 - 减金额 不等于钱包金额
            BigDecimal addamount =  new BigDecimal(pair.getLeft());
            BigDecimal subtractmount =  new BigDecimal(pair.getRight());
            BigDecimal allamount = Calculate.subtract(addamount,subtractmount).getAmount();
            if(!Calculate.equals(ent.getValue().get(0).getTotalAmount(),allamount)){
                walletDto.setWalletType(tbWallet.getUtype());
                walletDto.setuId(tbWallet.getUid());
                walletDto.setWalletId(ent.getKey());
                walletDto.setWalletMoney(ent.getValue().get(0).getTotalAmount());
                walletDto.setAddamount(addamount);
                walletDto.setSubtractmount(subtractmount);
                list.add(walletDto);
            }
        }
        return R.newOK(list);
        }

}
