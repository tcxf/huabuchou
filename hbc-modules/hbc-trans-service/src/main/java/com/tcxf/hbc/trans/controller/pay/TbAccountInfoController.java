package com.tcxf.hbc.trans.controller.pay;

import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.service.ITbAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:用户授信额度
 */
@RestController
@RequestMapping("/accountInfo")
public class TbAccountInfoController {

    @Autowired
    private ITbAccountInfoService iTbAccountInfoService;
    /**
     * 查找用户授信额度
     */
    @RequestMapping("/selectTbAccountInfo")
    @ResponseBody
    public R<TbAccountInfo> selectTbAccountInfo(String userId, String oId){
        TbAccountInfo selectTbAccountInfo = iTbAccountInfoService.selectTbAccountInfo(userId,oId);
        return R.<TbAccountInfo>newOK(selectTbAccountInfo);
    }
}
