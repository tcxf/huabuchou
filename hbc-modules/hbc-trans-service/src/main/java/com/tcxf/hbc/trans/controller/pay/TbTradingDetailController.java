package com.tcxf.hbc.trans.controller.pay;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.service.TbTradingDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:交易记录
 */
@RestController
@RequestMapping("/tbTradingDetail")
public class TbTradingDetailController {

    @Autowired
    private TbTradingDetailService tbTradingDetailService;
    /**
     * 查找交易记录信息
     */
    @RequestMapping("/selecbTradingDetailBySerialNo")
    @ResponseBody
    public R<TbTradingDetail> selectooperainfoBymId(String serialNo){
        TbTradingDetail selectTbTradingDetail = tbTradingDetailService.selectOne(new EntityWrapper<TbTradingDetail>().eq("serial_no",serialNo));
        return R.<TbTradingDetail>newOK(selectTbTradingDetail);
    }
}
