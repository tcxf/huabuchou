package com.tcxf.hbc.trans.controller.pay;

import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.trans.service.ITbWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 10:33
 * @Description:用户钱包
 */
@RestController
@RequestMapping("/wallet")
public class TbWalletController {

    @Autowired
    private ITbWalletService walletService;
    /**
     * 查找用户钱包
     */
    @RequestMapping("/selectUserWallet")
    @ResponseBody
    public R<TbWallet> selectUserWallet(String userId){
        TbWallet selectTbwallet = walletService.selectUserTbWallet(userId);
        return R.<TbWallet>newOK(selectTbwallet);
    }
}
