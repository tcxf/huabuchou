package com.tcxf.hbc.trans.controller.settlement;

import com.tcxf.hbc.common.util.MapUtil;
import com.tcxf.hbc.common.util.R;
import com.tcxf.hbc.common.web.BaseController;
import com.tcxf.hbc.trans.service.ISettlementInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 线下结算
 * jiangyong
 */
@RestController
@RequestMapping("/settlement")
public class SettlementConoller extends BaseController {
    @Autowired
    private ISettlementInfoService iSettlementInfoService;

    @RequestMapping(value = "/doSettlement/{sid}",method = {RequestMethod.POST})
    public R<Boolean> Settlement(@PathVariable("sid")String sid){
        R<Boolean> r=new R<Boolean>();
        HashMap <String, Object> map = iSettlementInfoService.settlementAllUser(sid);
        if(map!=null && StringUtils.equals(map.get("result").toString(),"success"))
        {
            r.setData(true);
        }
        else
        {
            r.setData(false);
        }
        return  r;
    }
}
