package com.tcxf.hbc.trans.dao;

import com.alibaba.fastjson.JSON;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.vo.PaybackMoney;
import com.tcxf.hbc.trans.model.dto.SettlementInfoDto;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.spring.web.json.Json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SettlementDao {
    private static final Logger logger = LoggerFactory.getLogger(SettlementDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 账单还款
     * @param ids
     * @param repaymentType
     * @param serialNo
     * @return
     */
    public boolean updateUserRepaymentList(List<TbRepayLog> ids, String rdId, String repaymentType, String serialNo)
    {
        boolean b = false;
        try
        {
            //已出账单id  未分期
            if(!rdId.equals("0"))
            {
                String updateTpSql = "update tb_repayment_plan set status=?,repayment_type=?,serial_no=?," +
                        "process_date=now(),modify_date=now() where rd_id=? ";
                jdbcTemplate.update(updateTpSql,"1",repaymentType,serialNo,rdId);
                logger.info("已出账单_交易记录:更新plan");
                String updateTdSql = "update tb_repayment_detail set status=1,repay_date=now(),modify_date=now() where id = ?";
                jdbcTemplate.update(updateTdSql,rdId);
                logger.info("已出账单_交易记录:更新detail");
            }

            //已分期/未分期
            for (int i = 0; i < ids.size(); i++) {
                String sql = "select * from tb_repayment_plan t where t.id = ?";
                TbRepaymentPlan t = (TbRepaymentPlan)jdbcTemplate.queryForObject(sql,new Object[]{ids.get(i).getPlanId()},new BeanPropertyRowMapper(TbRepaymentPlan.class));

                String sqlTd = "select * from tb_repayment_detail where id = ? ";
                TbRepaymentDetail td = (TbRepaymentDetail)jdbcTemplate.queryForObject(sqlTd,new Object[]{t.getRdId()},new BeanPropertyRowMapper(TbRepaymentDetail.class));



                String updateTpSql = "update tb_repayment_plan set status=?,repayment_type=?,serial_no=?," +
                        "process_date=now(),modify_date=now() where id=? ";
                jdbcTemplate.update(updateTpSql,"1",repaymentType,ids.get(i).getRepaySn(),ids.get(i).getPlanId());
                logger.info("已出账单_分期:更新plan");

                if(td.getTotalTimes()!=1 && td.getTotalTimes()==(td.getCurrentTimes()+1))
                {
                    String sqlUpdateTds = "updaet tb_repayment_detail set status=1,repay_date=now(),modify_date=now() where id=? ";
                    jdbcTemplate.update(sqlUpdateTds,td.getId());
                    logger.info("已出账单_分期:更新detail");
                }
                else
                {
                    String updateTdSql="";
                    if(td.getTotalTimes()!=1){
                        updateTdSql = "update tb_repayment_detail set current_times = ? where id = ?";
                        jdbcTemplate.update(updateTdSql,td.getCurrentTimes()+1,t.getRdId());
                        logger.info("已出账单_分期:更新detail");
                    }else{
                        updateTdSql = "update tb_repayment_detail set status=1 where id = ?";
                        jdbcTemplate.update(updateTdSql,t.getRdId());
                        logger.info("已出账单_未分期:更新detail");
                    }

                }


                /**
                 * 修改已还本金、利息、滞纳金
                 */
                BigDecimal bd_corpus =  Calculate.add(td.getRepayCorpus(),t.getCurrentCorpus()).getAmount();
                BigDecimal dc_fee = Calculate.add(td.getRepayFee(),t.getCurrentFee()).getAmount();
                BigDecimal dc_late_fee = Calculate.add(td.getRepayLateFee(),t.getLateFee()).getAmount();
                String updateMoney = "update tb_repayment_detail set repay_corpus=?,repay_fee=?,repay_late_fee=? where id =?";
                jdbcTemplate.update(updateMoney,bd_corpus,dc_fee,dc_late_fee,td.getId());
                b = true;
            }
        }
        catch(Exception e)
        {
            logger.error("还款失败:"+e.getMessage());
            throw  new RuntimeException(e);
        }
        return b;
    }


    /**
     * 获取商户结算单
     * @param settlementId
     * @return
     */
    public SettlementInfoDto getOpratorSettlementById(String settlementId)
    {
        String sql="SELECT * FROM tb_settlement_detail where id = ? ";
        return (SettlementInfoDto)jdbcTemplate.queryForObject(sql,new Object[]{settlementId},new BeanPropertyRowMapper(SettlementInfoDto.class));
    }

    /**
     * 查询用户结算单
     * @param settlementId
     * @return
     */
    public List<TbUserSharefeeSettlement> getUserSettlementById(String settlementId)
    {
        String sql="SELECT * FROM tb_user_sharefee_settlement where settlement_id = ? ";
//        return (TbUserSharefeeDetails)jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper(TbUserSharefeeDetails.class));
        List<TbUserSharefeeSettlement> list = jdbcTemplate.query(sql.toString(),new Object[]{settlementId},new BeanPropertyRowMapper(TbUserSharefeeSettlement.class));
        return list;
    }

    public List<TbWalletIncome> findAllMoneyBySid(String sid)
    {
        String sql =" SELECT " +
                " sum(w.amount) AS amount, " +
                " w.wid as wid " +
                " FROM " +
                " tb_wallet_income as w " +
                " WHERE " +
                " w.settlement_id = ? " +
                " AND w.settlement_status = '0' " +
                " AND type IN (3, 4) group by w.wid ";
        List<TbWalletIncome> list= jdbcTemplate.query(sql,new Object[]{sid}
                ,new BeanPropertyRowMapper(TbWalletIncome.class));
        return list;
    }

    /**
     * 钱包需入账总金额
     * @param sid
     * @param walletId
     * @return
     */
    public TbWalletIncome findTotalMoneyByParam(String sid,String walletId)
    {
        String sql =" SELECT" +
                " sum(w.amount) AS amount," +
                " w.wid" +
                " FROM" +
                " tb_wallet_income w" +
                " WHERE" +
                " w.wid = ?" +
                " AND w.settlement_id = ?" +
                " AND w.settlement_status = '0'" +
                " AND type IN (3, 4)";
        return (TbWalletIncome)jdbcTemplate.queryForObject(sql,new Object[]{walletId,sid}
        ,new BeanPropertyRowMapper(TbWalletIncome.class));
    }

    /**
     * 查询用户结算总金额(钱包金额已累加)
     * @param sid
     * @return
     */
    public List<TbWallet> findAllUserMoney(String sid)
    {
        String sql = " SELECT s.wid as id,sum(t.total_amount+s.amount) as total_amount from " +
                " (" +
                " SELECT" +
                " w.wid," +
                " sum(w.amount) as amount" +
                " FROM" +
                " tb_wallet_income w" +
                " WHERE" +
                " w.ptepy = '1'" +
                " AND w.settlement_id = ?" +
                " AND w.settlement_status = '0'" +
                " GROUP BY w.wid " +
                " ) s,tb_wallet t where s.wid = t.id GROUP BY s.wid";
        List<TbWallet> list =  (List<TbWallet>)jdbcTemplate.query(sql,new Object[]{sid}
                ,new BeanPropertyRowMapper(TbWallet.class));
        return list;
    }

    /**
     * 平台抽成运营商总费用
     * @param sid
     * @param walletId
     * @return
     */
    public TbWalletIncome findTotalMoneyByOid(String sid,String walletId)
    {
        String sql =" SELECT" +
                " sum(w.amount) AS amount," +
                " w.wid" +
                " FROM" +
                " tb_wallet_income w" +
                " WHERE" +
                " w.wid = ?" +
                " AND w.settlement_id = ?" +
                " AND w.settlement_status = '0'" +
                " AND type='8'";
        return (TbWalletIncome)jdbcTemplate.queryForObject(sql,new Object[]{walletId,sid}
                ,new BeanPropertyRowMapper(TbWalletIncome.class));
    }

    /**
     * 用户分润总金额
     * @param settlementId
     * @return
     */
    public TbUserSharefeeSettlement getTotalUserMoney(String settlementId)
    {
        String sql="SELECT sum(total_money) as totalMoney FROM tb_user_sharefee_settlement where settlement_id= ? ";
        return (TbUserSharefeeSettlement)jdbcTemplate.queryForObject(sql,new Object[]{settlementId},new BeanPropertyRowMapper(TbUserSharefeeSettlement.class));
    }

    /**
     * 更新结算状态
     * @param settlementId
     */
    public void updateUserWalletStatus(String settlementId)
    {
        String sql ="update tb_wallet_income set settlement_status= 1 where settlement_id = ?";
        jdbcTemplate.update(sql,new Object[]{settlementId});
    }

    /**
     * 查询账户信息
     * @param id
     * @param userType
     * @return
     */
    public TbWallet getUserWallet(String id,String userType)
    {
        String sql = "select * from tb_user_account_type_con t where t.con_id = ? and t.user_type = ? ";
        TbUserAccountTypeCon ta = (TbUserAccountTypeCon)jdbcTemplate.queryForObject(sql,new Object[]{id,userType},new BeanPropertyRowMapper(TbUserAccountTypeCon.class));
        if("2".equals(userType))
        {
            String sqlA = "select * from tb_user_account_type_con t where t.acct_id = ? and user_type= ? ";
            TbUserAccountTypeCon tt =   (TbUserAccountTypeCon)jdbcTemplate.queryForObject(sqlA,new Object[]{ta.getAcctId(),"1"},new BeanPropertyRowMapper(TbUserAccountTypeCon.class));
            String walletSql = "select * from tb_wallet t where t.uid = ?";
            return (TbWallet)jdbcTemplate.queryForObject(walletSql,new Object[]{tt.getAcctId()},new BeanPropertyRowMapper(TbWallet.class));
        }
        else
        {
            String walletSql = "select * from tb_wallet t where t.id = ?";
            return (TbWallet)jdbcTemplate.queryForObject(walletSql,new Object[]{ta.getAcctId()},new BeanPropertyRowMapper(TbWallet.class));
        }

    }
    public TbWallet getUserWalletByOid(String oid)
    {
        String walletSql = "select * from tb_wallet t where t.uid = ?";
        return (TbWallet)jdbcTemplate.queryForObject(walletSql,new Object[]{oid},new BeanPropertyRowMapper(TbWallet.class));
    }

    public TbWallet getUserWalletById(String id)
    {
        String walletSql = "select * from tb_wallet t where t.id = ?";
        return (TbWallet)jdbcTemplate.queryForObject(walletSql,new Object[]{id},new BeanPropertyRowMapper(TbWallet.class));
    }


    /**
     * 查询钱包金额与结算金额的总和
     * @param settlementId
     */
    public List<TbWallet>  getUserWalletSum(String settlementId)
    {
        String sql="select sum(w.total_amount + f.money) as totalAmount,w.uid as uid from tb_wallet w,(select c.acct_id,sum(s.total_money) as money " +
                "from tb_user_sharefee_settlement s ,tb_user_account_type_con c where s.settlement_id = ? AND s.uid= c.con_id) f " +
                "where w.uid = f.acct_id GROUP BY w.uid";
        List<TbWallet> list = (List<TbWallet>)jdbcTemplate.query(sql,new Object[]{settlementId},new BeanPropertyRowMapper(TbWallet.class));
        return list;
    }

    /**
     * 更新用户钱包金额
     * @param list
     */
    public void updateUserWalletSum(List<TbWallet> list)
    {
        String sql = "update tb_wallet t set t.total_amount = ?,t.modify_date=now() where uid = ?";
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            params.add(new Object[]{list.get(i).getTotalAmount(),list.get(i).getUid()});
        }
        jdbcTemplate.batchUpdate(sql,params);
    }

    /**
     * 更新钱包金额
     * @param amount
     * @param id
     */
    public void updateWalletById(BigDecimal amount, String id )
    {
        String sql ="update tb_wallet t set t.total_amount = ? where id = ? ";
        jdbcTemplate.update(sql,amount,id);
    }

    /**
     * 更新分润表结算状态
     * @param list
     */
    public void updateTradeProfitShare(List<TbWalletIncome> list)
    {
        String sql = "update tb_trade_profit_share t set t.is_settlement=1,t.modify_date=now() where trade_id= ?";
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i <list.size() ; i++) {
            params.add(new Object[]{list.get(i).getTid()});
        }
        if(params.size()>0)
        {
            jdbcTemplate.batchUpdate(sql,params);
        }

    }

    /**
     * 获取结算的钱包交易记录
     * @param settlementId
     * @return
     */
    public List<TbWalletIncome> getWalletIncomeList(String settlementId)
    {
        String sql ="select * from tb_wallet_income t where t.settlement_id = ? ";
        List<TbWalletIncome> list =jdbcTemplate.query(sql,new Object[]{settlementId},new BeanPropertyRowMapper(TbWalletIncome.class));
        return list;
    }

    public void addSettlementLog()
    {
        String sql = "insert into tb_settlement_log " +
                "(id,payment_type,settlement_sn,oid,amount,status,pay_date,sid,repay_sn,create_date,modify_date) " +
                "values(?,?,?,?,?,?,?,?,?,now(),now()) ";

    }


    /**
     * 更新钱包记录结算状态
     * @param settlementId
     */
    public void updateWalletIncomeSettlementStatus(String settlementId)
    {
        String sql ="update tb_wallet_income t set t.settlement_status = 1 where settlement_id = ? ";
        jdbcTemplate.update(sql,settlementId);
    }

    /**
     * 用户钱包分润总金额
     * @param settlementId
     * @return
     */
    public List<TbWallet> getWalletSumAmount(String settlementId)
    {
        String sql="SELECT " +
                "sum(w.total_amount + f.money) AS total_amount,w.uid as uid " +
                "FROM " +
                "tb_wallet w, " +
                "( " +
                "SELECT " +
                "c.acct_id, " +
                "sum(s.total_money) AS money,c.con_id " +
                "FROM " +
                "tb_user_sharefee_settlement s, " +
                "tb_user_account_type_con c " +
                "WHERE " +
                "s.uid = c.con_id and s.settlement_id = ? " +
                ") f " +
                "WHERE " +
                "w.uid = f.acct_id group by w.uid";
        List<TbWallet> list= jdbcTemplate.query(sql,new Object[]{settlementId},new BeanPropertyRowMapper());
        return list;

    }
}
