package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.MMerchantInfo;

/**
 * <p>
 * 商户信息表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MMerchantInfoMapper extends BaseMapper<MMerchantInfo> {

}
