package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.MRedPacketSetting;

/**
 * <p>
 * 优惠券表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface MRedPacketSettingMapper extends BaseMapper<MRedPacketSetting> {
}
