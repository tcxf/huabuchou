package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.trans.model.dto.MRedPacketDto;

import java.util.List;
import java.util.Map;

public interface MredPacketMapper extends BaseMapper<MRedPacket> {

    List<MRedPacketDto> selectRedPacketAvailable(Map<String,Object> map);
}
