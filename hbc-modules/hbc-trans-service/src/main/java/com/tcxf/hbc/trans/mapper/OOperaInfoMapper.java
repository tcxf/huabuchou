package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.OOperaInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 运营商信息表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface OOperaInfoMapper extends BaseMapper<OOperaInfo> {

    OOperaInfo selectooperainfoBymId(@Param("mId") String mId);
}
