package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.trans.model.entity.Pay;

public interface PayMapper extends BaseMapper<Pay> {

}
