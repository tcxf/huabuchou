package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbAccountInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户授信资金额度表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbAccountInfoMapper extends BaseMapper<TbAccountInfo> {
    TbAccountInfo selectTbAccountInfo(@Param("userId") String userId, @Param("oId") String oId);
}
