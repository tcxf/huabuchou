package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbAccountTradingLog;

/**
 * <p>
 * 授信资金流水 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbAccountTradingLogMapper extends BaseMapper<TbAccountTradingLog> {

}
