package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbPayment;

public interface TbPaymentMapper extends BaseMapper<TbPayment> {

}
