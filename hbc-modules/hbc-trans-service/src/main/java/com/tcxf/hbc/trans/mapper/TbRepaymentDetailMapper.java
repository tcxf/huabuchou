package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;


/**
 * <p>
 * 还款表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepaymentDetailMapper extends BaseMapper<TbRepaymentDetail> {

}
