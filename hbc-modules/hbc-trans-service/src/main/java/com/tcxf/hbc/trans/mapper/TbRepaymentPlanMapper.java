package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;


/**
 * <p>
 * 还款计划表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbRepaymentPlanMapper extends BaseMapper<TbRepaymentPlan> {

}
