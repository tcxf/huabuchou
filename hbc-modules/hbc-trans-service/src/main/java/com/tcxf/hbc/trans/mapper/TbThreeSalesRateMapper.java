package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbThreeSalesRate;

/**
 * <p>
 * 三级分销利润分配比例表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbThreeSalesRateMapper extends BaseMapper<TbThreeSalesRate> {

}
