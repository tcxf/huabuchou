package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbThreeSalesRef;

/**
 * <p>
 * 三级分销用户关系绑定表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbThreeSalesRefMapper extends BaseMapper<TbThreeSalesRef> {
}
