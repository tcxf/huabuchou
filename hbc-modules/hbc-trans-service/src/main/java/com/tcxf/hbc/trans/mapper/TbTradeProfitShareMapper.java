package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbTradeProfitShare;

public interface TbTradeProfitShareMapper extends BaseMapper<TbTradeProfitShare> {
}