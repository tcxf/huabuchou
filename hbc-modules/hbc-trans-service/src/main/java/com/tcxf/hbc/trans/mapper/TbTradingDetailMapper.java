package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbTradingDetail;

/**
 * <p>
 * 用户交易记录表 Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbTradingDetailMapper extends BaseMapper<TbTradingDetail> {
}
