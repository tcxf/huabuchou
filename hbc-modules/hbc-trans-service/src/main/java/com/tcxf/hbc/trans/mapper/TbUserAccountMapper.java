package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbUserAccount;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户账户表（包含消费者、商户） Mapper 接口
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface TbUserAccountMapper extends BaseMapper<TbUserAccount> {

    TbUserAccount selectUserInfoByUserId(@Param("userId") String userId);
}
