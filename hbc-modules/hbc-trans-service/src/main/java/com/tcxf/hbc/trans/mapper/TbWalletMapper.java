package com.tcxf.hbc.trans.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcxf.hbc.common.entity.TbWallet;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 资金钱包表 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface TbWalletMapper extends BaseMapper<TbWallet> {

    TbWallet selectUserTbWallet(@Param("userId") String userId);

    TbWallet selectopTbWallet(@Param("oId")String oId);
}
