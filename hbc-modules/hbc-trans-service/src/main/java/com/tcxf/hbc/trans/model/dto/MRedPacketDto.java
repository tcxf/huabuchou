package com.tcxf.hbc.trans.model.dto;

import com.tcxf.hbc.common.entity.MRedPacket;

/**
 * @Auther: liuxu
 * @Date: 2018/7/10 08:51
 * @Description:
 */
public class MRedPacketDto extends MRedPacket {
    /**
     * 已使用
     */
    public static final String IS_USE_ES = "1";
    /**
     * 未使用
     */
    public static final String IS_USE_NO = "0";
    /**
     * 商户名称
     */
    private String miName;

    public String getMiName() {
        return miName;
    }

    public void setMiName(String miName) {
        this.miName = miName;
    }
}
