package com.tcxf.hbc.trans.model.dto;

import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.common.entity.TbWalletIncome;

import java.math.BigDecimal;
import java.util.List;

/**
 * 钱包数据验证
 */
public class OnCheckWalletDto {
    //钱包
    private WalletDto walletDto;
    /**
     * 钱包明细
     */
    private List<TbWalletIncome> tbWalletIncomeList;

}
