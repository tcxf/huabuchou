package com.tcxf.hbc.trans.model.dto;

/**
 * @Auther: liuxu
 * @Date: 2018/7/5 17:11
 * @Description: 快捷支付申请模块DTO
 */
public class PayAgreeapplyDto {

    /**
     * 订单号
     */
    private String orderid;
    /**
     * 银行卡协议号
     */
    private String agreeid;
    /**
     * 支付金额 单位为分
     */
    private String amount;
    /**
     *订单内容	订单的展示标题
     */
    private String subjec;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSubjec() {
        return subjec;
    }

    public void setSubjec(String subjec) {
        this.subjec = subjec;
    }
}
