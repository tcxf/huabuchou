package com.tcxf.hbc.trans.model.dto;

import com.tcxf.hbc.common.entity.TbTradingDetail;

import java.math.BigDecimal;

/**
 * @Auther: liuxu
 * @Date: 2018/7/5 17:11
 * @Description: 支付模块DTO
 */
public class PayDto {
    /**
     * 授信支付
     */
    public static final  String PAYTYPE_SX="1";
    /**
     * 钱包支付
     */
    public static final  String PAYTYPE_QB="2";
    /**
     * 微信支付
     */
    public static final  String PAYTYPE_WX="3";
    /**
     * 银联支付
     */
    public static final  String PAYTYPE_YL="4";

    /**
     * 商户ID
     */
    private String mId;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户微信openID
     */
    private String openId;
    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 支付类型
     */
    private String payType;
    /**
     * 优惠券ID
     */
    private String mredmarketId;

    /**
     * 支付密码
     */
    private String passWord;

    /**
     * 快捷支付银行卡协议号
     */
    private String agreeid;

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getMredmarketId() {
        return mredmarketId;
    }

    public void setMredmarketId(String mredmarketId) {
        this.mredmarketId = mredmarketId;
    }

    public String checkTradingType(){
        if(PAYTYPE_SX.equals(this.getPayType())){
            return TbTradingDetail.TRADINGTYPE_SX_YES;
        }
        return TbTradingDetail.TRADINGTYPE_SX_NO;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
