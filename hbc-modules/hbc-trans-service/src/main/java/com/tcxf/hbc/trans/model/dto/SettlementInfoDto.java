package com.tcxf.hbc.trans.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SettlementInfoDto implements Serializable {
    private BigDecimal amount;                                  // 结算单结算总额
    private BigDecimal        shareFee;                                // 结算单商家让利金额
    private BigDecimal        platformShareFee;                        // 结算单平台让利金额
    private String            mid;
    private String            oid;
    private String            name;
    private Integer           settlementLoop;
    private String            osn;
    private BigDecimal operatorShareFee;                                //运营商交易抽成费
    private BigDecimal operaShareFee;                                   //运营商收取商户通道费用
    private BigDecimal discountFee;
    private String fid;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public BigDecimal getPlatformShareFee() {
        return platformShareFee;
    }

    public void setPlatformShareFee(BigDecimal platformShareFee) {
        this.platformShareFee = platformShareFee;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSettlementLoop() {
        return settlementLoop;
    }

    public void setSettlementLoop(Integer settlementLoop) {
        this.settlementLoop = settlementLoop;
    }

    public String getOsn() {
        return osn;
    }

    public void setOsn(String osn) {
        this.osn = osn;
    }

    public BigDecimal getOperatorShareFee() {
        return operatorShareFee;
    }

    public void setOperatorShareFee(BigDecimal operatorShareFee) {
        this.operatorShareFee = operatorShareFee;
    }

    public BigDecimal getOperaShareFee() {
        return operaShareFee;
    }

    public void setOperaShareFee(BigDecimal operaShareFee) {
        this.operaShareFee = operaShareFee;
    }

    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }
}
