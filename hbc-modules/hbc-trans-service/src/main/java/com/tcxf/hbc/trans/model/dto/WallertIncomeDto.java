package com.tcxf.hbc.trans.model.dto;

import com.tcxf.hbc.common.entity.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 钱包明细
 */
public class WallertIncomeDto {

    /**
     * 钱包明细ID
     */
    private String id;

    /**
     * 钱包收支类型 收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），
     * 5.充值,6.提现支出，7.消费支出 8 平台抽成费扣取（只针对运营商钱包）',
     */
    private String type;
    /**
     * 金额
     */
    private BigDecimal trade_total_amount;

    /**
     * 交易记录ID
     */
    private String tid;

    /**
     * 结算单ID
     */
    private String settlement_id;

    /**
     * 结算单状态
     */
    private String settlement_status;

    /**
     * 交易记录
     */
    private TbTradingDetail tbTradingDetail;

    /**
     * 交易分润
     */
    private TbTradeProfitShare tbTradeProfitShare;

    /**
     * 三级分销
     */
    private TbThreeSaleProfitShare tbThreeSaleProfitShare;

    /**
     * 还款单
     */
    private TbRepay tbRepay;

    /**
     * 提现表
     */
    private TbWalletWithdrawals tbWalletWithdrawals;


}
