package com.tcxf.hbc.trans.model.dto;

import com.tcxf.hbc.common.entity.TbTradingDetail;

import java.math.BigDecimal;

/**
 * @Auther: liuxu
 * @Date: 2018/7/5 17:11
 * @Description: 钱包数据验证
 */
public class WalletDto {

    /**
     * 钱包类型  用户 商户  运营商 资金端
     */
    private String walletType;
    /**
     * 用户account_id
     */
    private String uId;

    /**
     * 钱包ID
     */
    private String walletId;
    /**
     * 钱包总金额
     */
    private BigDecimal walletMoney;

    /**
     * 总入金额
     */
    private BigDecimal addamount;

    /**
     * 总出金额
     */
    private BigDecimal subtractmount;

    public BigDecimal getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(BigDecimal walletMoney) {
        this.walletMoney = walletMoney;
    }

    public BigDecimal getAddamount() {
        return addamount;
    }

    public void setAddamount(BigDecimal addamount) {
        this.addamount = addamount;
    }

    public BigDecimal getSubtractmount() {
        return subtractmount;
    }

    public void setSubtractmount(BigDecimal subtractmount) {
        this.subtractmount = subtractmount;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }
}
