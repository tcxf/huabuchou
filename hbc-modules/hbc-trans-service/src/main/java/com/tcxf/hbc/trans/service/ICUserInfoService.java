package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CUserInfo;

/**
 * <p>
 * 消费者用户表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserInfoService extends IService<CUserInfo> {

    CUserInfo selectUserInfoByOpenId(String openId);
}
