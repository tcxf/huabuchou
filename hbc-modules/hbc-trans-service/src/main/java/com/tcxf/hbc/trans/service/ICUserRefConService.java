package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.CUserRefCon;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ICUserRefConService extends IService<CUserRefCon> {

}
