package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.MRedPacketSetting;

/**
 * <p>
 * 优惠券表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IMRedPacketSettingService extends IService<MRedPacketSetting> {

}
