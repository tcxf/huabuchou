package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.OOperaOtherInfo;

/**
 * <p>
 * 运营商其他信息表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface IOOperaOtherInfoService extends IService<OOperaOtherInfo> {
    /**
     * 查询今天所注册的运营商
     * @param obj  根据时间
     * @return
     */
     public int CreateQueryAllDat(String strat, String end);
}
