package com.tcxf.hbc.trans.service;

import java.util.HashMap;

public interface ISettlementInfoService {
    /**
     * 根据结算单结算所有费用
     * @param settlementId
     * @return
     */
    public HashMap<String,Object> settlementAllUser(String settlementId);
}
