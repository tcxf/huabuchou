package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbAccountTradingLog;

/**
 * <p>
 * 授信资金流水 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbAccountTradingLogService extends IService<TbAccountTradingLog> {

}
