package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepayLog;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;


/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRepayLogService extends IService<TbRepayLog> {

    PayagreeconfirmDto addinitPayBackInfo(PaybackMoneyDto paybackMoneyDto);

    PayagreeconfirmDto advancePaybackMoney(PaybackMoneyDto paybackMoneyDto);

    void updateTradingInfo(PayagreeconfirmDto payagreeconfirmDto);

    void updateNormalPay(PayagreeconfirmDto payagreeconfirmDto);
}
