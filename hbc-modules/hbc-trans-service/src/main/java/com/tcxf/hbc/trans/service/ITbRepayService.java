package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepay;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
public interface ITbRepayService extends IService<TbRepay> {

}
