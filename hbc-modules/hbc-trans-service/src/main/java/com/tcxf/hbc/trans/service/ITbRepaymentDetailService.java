package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbRepaymentDetail;


/**
 * <p>
 * 还款表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbRepaymentDetailService extends IService<TbRepaymentDetail> {
}
