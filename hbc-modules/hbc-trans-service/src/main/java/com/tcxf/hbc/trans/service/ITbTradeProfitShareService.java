package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbTradeProfitShare;

/**
 * <p>
 * 交易利润分配表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbTradeProfitShareService extends IService<TbTradeProfitShare> {

}
