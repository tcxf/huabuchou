package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbUserAccount;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface ITbUserAccountService extends IService<TbUserAccount> {

    TbUserAccount selectUserInfoByUserId(String userId);

}
