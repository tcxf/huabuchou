package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbWalletIncome;


public interface ITbWalletIncomeService extends IService<TbWalletIncome> {

}
