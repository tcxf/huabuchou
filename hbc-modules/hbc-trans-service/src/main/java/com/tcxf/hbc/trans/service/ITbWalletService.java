package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbWallet;


/**
 * <p>
 * 资金钱包表 服务类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
public interface ITbWalletService extends IService<TbWallet> {

    TbWallet selectUserTbWallet(String userId);

    TbWallet selectopTbWallet(String oid);
}
