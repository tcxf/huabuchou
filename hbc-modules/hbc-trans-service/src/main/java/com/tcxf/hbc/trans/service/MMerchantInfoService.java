package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.MMerchantInfo;

/**
 * <p>
 * 商户信息表 服务类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
public interface MMerchantInfoService extends IService<MMerchantInfo> {

}
