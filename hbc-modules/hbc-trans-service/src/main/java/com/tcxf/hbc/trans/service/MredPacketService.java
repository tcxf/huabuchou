package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.trans.model.dto.MRedPacketDto;

import java.util.List;

/**
 * @Auther: liuxu
 * @Date: 2018/7/10 08:38
 * @Description:
 */
public interface MredPacketService extends IService<MRedPacket> {

    List<MRedPacketDto>  selectRedPacketAvailable (String userId, String mId, String amount);
}
