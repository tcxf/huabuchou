package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.trans.model.dto.PayDto;
import com.tcxf.hbc.trans.model.entity.Pay;

import java.util.TreeMap;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 15:09
 * @Description:
 */
public interface PayService extends IService<Pay> {
    /**
     * 支付操作 包含 授信支付 钱包支付 微信支付 银联支付
     * @param payDto
     * @return
     */
    String onPay(PayDto payDto) throws Exception;

    /**
     * 支付回调
     * @param tbTradingDetailId 交易单ID
     */
    void onpayNotify(String tbTradingDetailId,String payType,TreeMap<String, String> map,String onpayNotify);

}
