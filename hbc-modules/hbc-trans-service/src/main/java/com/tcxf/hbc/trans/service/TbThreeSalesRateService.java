package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbThreeSalesRate;

/**
 * 三级分销比例表
 * @Auther: liuxu
 * @Date: 2018/7/10 17:23
 * @Description:
 */

public interface TbThreeSalesRateService extends IService<TbThreeSalesRate> {

}
