package com.tcxf.hbc.trans.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcxf.hbc.common.entity.TbThreeSalesRef;

/**
 * 三级分销用户绑定关系表
 * @Auther: liuxu
 * @Date: 2018/7/10 17:23
 * @Description:
 */
public interface TbThreeSalesRefService extends IService<TbThreeSalesRef> {
}
