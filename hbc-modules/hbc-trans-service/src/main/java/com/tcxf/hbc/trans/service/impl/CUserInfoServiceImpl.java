package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.CUserInfo;
import com.tcxf.hbc.trans.mapper.CUserInfoMapper;
import com.tcxf.hbc.trans.service.ICUserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消费者用户表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
@Slf4j
public class CUserInfoServiceImpl extends ServiceImpl<CUserInfoMapper, CUserInfo> implements ICUserInfoService {

    @Override
    public CUserInfo selectUserInfoByOpenId(String openId) {
        return selectOne(new EntityWrapper<CUserInfo>().eq("wx_openid",openId));
    }
}
