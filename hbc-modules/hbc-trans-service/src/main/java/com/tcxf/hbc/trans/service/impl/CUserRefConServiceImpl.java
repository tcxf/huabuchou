package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.CUserRefCon;
import com.tcxf.hbc.trans.mapper.CUserRefConMapper;
import com.tcxf.hbc.trans.service.ICUserRefConService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户，运营商，资金端，商户关系表(用于用户捆绑各端) 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class CUserRefConServiceImpl extends ServiceImpl<CUserRefConMapper, CUserRefCon> implements ICUserRefConService {

}
