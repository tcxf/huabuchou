package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.MMerchantInfo;
import com.tcxf.hbc.trans.mapper.MMerchantInfoMapper;
import com.tcxf.hbc.trans.service.MMerchantInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户信息表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MMerchantInfoServiceImpl extends ServiceImpl<MMerchantInfoMapper, MMerchantInfo> implements MMerchantInfoService {


}
