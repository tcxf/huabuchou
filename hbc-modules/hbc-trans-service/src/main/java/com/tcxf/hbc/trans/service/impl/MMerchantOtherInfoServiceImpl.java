package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.MMerchantOtherInfo;
import com.tcxf.hbc.trans.mapper.MMerchantOtherInfoMapper;
import com.tcxf.hbc.trans.service.IMMerchantOtherInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户其他信息 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MMerchantOtherInfoServiceImpl extends ServiceImpl<MMerchantOtherInfoMapper, MMerchantOtherInfo> implements IMMerchantOtherInfoService {

    @Override
    public MMerchantOtherInfo selectByMId(String mId) {
        return selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid",mId));
    }
}
