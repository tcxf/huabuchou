package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.MRedPacketSetting;
import com.tcxf.hbc.trans.mapper.MRedPacketSettingMapper;
import com.tcxf.hbc.trans.service.IMRedPacketSettingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class MRedPacketSettingServiceImpl extends ServiceImpl<MRedPacketSettingMapper, MRedPacketSetting> implements IMRedPacketSettingService {

}
