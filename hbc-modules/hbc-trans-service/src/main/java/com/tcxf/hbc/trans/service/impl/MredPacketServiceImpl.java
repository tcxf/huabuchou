package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.FFundendBind;
import com.tcxf.hbc.common.entity.MRedPacket;
import com.tcxf.hbc.common.util.DateUtil;
import com.tcxf.hbc.trans.mapper.MredPacketMapper;
import com.tcxf.hbc.trans.model.dto.MRedPacketDto;
import com.tcxf.hbc.trans.service.MredPacketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: liuxu
 * @Date: 2018/7/10 08:38
 * @Description:
 */
@Service
public class MredPacketServiceImpl extends ServiceImpl<MredPacketMapper,MRedPacket> implements MredPacketService {

    @Autowired
    MredPacketMapper mredPacketMapper;

    @Override
    public List<MRedPacketDto> selectRedPacketAvailable(String userId,String mId,String amount) {
        Map<String,Object> entity = new HashMap<String,Object>();
        entity.put("userId",userId);
        entity.put("isuse",MRedPacket.IS_USE_NO);
        entity.put("mId",mId);
        entity.put("fullMoney",amount);
        entity.put("endDate",DateUtil.format(new Date(),DateUtil.FORMAT_YYYY_MM_DD));
        return mredPacketMapper.selectRedPacketAvailable(entity);

    }
}
