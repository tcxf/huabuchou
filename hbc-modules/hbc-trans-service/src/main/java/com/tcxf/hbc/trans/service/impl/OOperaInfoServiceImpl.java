package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.OOperaInfo;
import com.tcxf.hbc.trans.mapper.MredPacketMapper;
import com.tcxf.hbc.trans.mapper.OOperaInfoMapper;
import com.tcxf.hbc.trans.service.IOOperaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营商信息表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class OOperaInfoServiceImpl extends ServiceImpl<OOperaInfoMapper, OOperaInfo> implements IOOperaInfoService {

    @Autowired
    OOperaInfoMapper oOperaInfoMapper;

    @Override
    public OOperaInfo selectooperainfoBymId(String mId) {
        return  oOperaInfoMapper.selectooperainfoBymId(mId);
    }
}
