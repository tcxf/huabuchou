package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.bean.config.PayStatusPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.Pair;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.util.secure.DES;
import com.tcxf.hbc.trans.mapper.PayMapper;
import com.tcxf.hbc.trans.model.dto.PayDto;
import com.tcxf.hbc.trans.model.entity.Pay;
import com.tcxf.hbc.trans.service.*;
import com.tcxf.hbc.trans.tlPay.business.QuickPayPayapplyagreeBusiness;
import com.tcxf.hbc.trans.tlPay.business.UnifiedorderBusiness;
import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeResData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.util.Util;
import com.tcxf.hbc.trans.util.SnowflakeIdWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @Auther: liuxu
 * @Date: 2018/7/9 15:10
 * @Description:
 */
@Service
public class PayServiceImpl extends ServiceImpl<PayMapper,Pay> implements PayService {

    @Autowired
    private PayStatusPropertiesConfig payStatusPropertiesConfig;
    //交易记录
    @Autowired
    private TbTradingDetailService tbTradingDetailService;
    //商户信息
    @Autowired
    private MMerchantInfoService mMerchantInfoService;
    //用户信息
    @Autowired
    private ICUserInfoService cuserinfoservice;
    //优惠券信息
    @Autowired
    private MredPacketService mredPacketService;
    //优惠券设置信息
    @Autowired
    private IMRedPacketSettingService mRedPacketSettingService;
    //三级分销用户绑定关系
    @Autowired
    private TbThreeSalesRefService tbThreeSalesRefService;
    //三级分销比例表
    @Autowired
    private TbThreeSalesRateService tbThreeSalesRateService;
    // 三级分销利润分配
    @Autowired
    private ITbThreeSaleProfitShareService tbThreeSaleProfitShareService;
    //钱包
    @Autowired
    private ITbWalletService tbWalletService;
    //钱包记录收支明细信息
    @Autowired
    private ITbWalletIncomeService iTbWalletIncomeService;
    //支付记录
    @Autowired
    private ITbPaymentService paymentService;
    //商户其他信息
    @Autowired
    private IMMerchantOtherInfoService merchantOtherInfoService;
    //运营商其他信息
    @Autowired
    private IOOperaOtherInfoService operaOtherInfoService;
    //交易利润分配
    @Autowired
    private ITbTradeProfitShareService iTbTradeProfitShareService;
    //授信额度
    @Autowired
    private ITbAccountInfoService iTbAccountInfoService;
    // 授信资金流水
    @Autowired
    private ITbAccountTradingLogService iTbAccountTradingLogService;

    @Autowired
    private ITbUserAccountService tbUserAccountService;

    @Autowired
    private ICUserRefConService cUserRefConService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 支付返回信息
     * 1、授信支付 钱包支付 返回的是 支付成功的跳转界面路径
     * 2、微信支付 返回的是 JS支付API提交的JSON参数结构
     * 3、快捷支付 返回的是 支付申请的接口响应参数
     */
    String payinfo;

    /**
     * 支付
     *
     * @param payDto
     * @return
     */
    @Override
    @Transactional
    public String onPay(PayDto payDto) throws Exception {
        //获取相关用户信息 商户信息 优惠券信息
        Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> pair = selectObjectData(payDto);
        //1、验证支付数据的真伪性，以及数据的合法性
        validateonPay(pair, payDto);
        //1、创建交易订单信息
        TbTradingDetail tbTradingDetail = addTbTradingDetail(payDto, pair);
        //2、创建三级分销利润分配
        TbThreeSaleProfitShare tbThreeSaleProfitShare = addTbThreeSaleProfitShare(payDto.getMredmarketId(), pair);
        //创建交易分润
        TbTradeProfitShare tbTradeProfitShare = addTbTradeProfitShare(payDto, tbTradingDetail, tbThreeSaleProfitShare, pair);
        //如果是授信支付或者钱包支付 则直接进行支付回调
        if (PayDto.PAYTYPE_SX.equals(payDto.getPayType()) || PayDto.PAYTYPE_QB.equals((payDto.getPayType()))) {
            onpayNotify(tbTradingDetail.getSerialNo(), payDto.getPayType(),new TreeMap<>(),payDto.getOpenId());
            payinfo =  "/pay/goToPayOk?mId=" + payDto.getmId() + "&userId=" + payDto.getUserId();
            return payinfo;
        }
        //微信支付
        if (PayDto.PAYTYPE_WX.equals(payDto.getPayType())) {
            MRedPacket mRedPacket = pair.getRight();
            BigDecimal mRedPacketMoney = ValidateUtil.isEmpty(mRedPacket) ? new BigDecimal(0) : mRedPacket.getMoney();
            String amount = Calculate.multiply_100(Calculate.subtract(payDto.getAmount(),mRedPacketMoney).getAmount()).format(0).toString();
            if(payStatusPropertiesConfig.getStatus().equals(Configure.PAY_STATUS_0)){
                amount = "1";
            }
            gotoOnPay(amount, tbTradingDetail.getSerialNo(), pair.getLeft().getRight().getName(), payDto.getOpenId());
            return payinfo;
        }
        //快捷支付
        if (PayDto.PAYTYPE_YL.equals(payDto.getPayType())) {
            MRedPacket mRedPacket = pair.getRight();
            BigDecimal mRedPacketMoney = ValidateUtil.isEmpty(mRedPacket) ? new BigDecimal(0) : mRedPacket.getMoney();
            String amount = Calculate.multiply_100(Calculate.subtract(payDto.getAmount(),mRedPacketMoney).getAmount()).format(0).toString();
            if(payStatusPropertiesConfig.getStatus().equals(Configure.PAY_STATUS_0)){
                amount = "1";
            }
            quickPayment(tbTradingDetail.getSerialNo(),payDto.getAgreeid(),amount,pair.getLeft().getRight().getName());
        }
        return payinfo;

    }

    /**
     * 快捷支付
     */
    private void quickPayment(String orderid,String agreeid,String amount,String subject) {
        try {
            QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData =
                    new QuickPayPayapplyagreeReqData(orderid,agreeid,amount,subject,Configure.NOTIFY_URL_TYPE);
            new QuickPayPayapplyagreeBusiness().run(quickPayPayapplyagreeReqData, new QuickPayPayapplyagreeBusiness.ResultListener() {
                @Override
                public void onQuickPayPayapplyagreeFail(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                    Util.log(QuickPayPayapplyagreeBusiness.result);
                    throw new CheckedException(ValidateUtil.isEmpty(quickPayPayapplyagreeResData.getErrmsg())?"支付失败，请稍后再试":quickPayPayapplyagreeResData.getErrmsg());
                }
                @Override
                public void onQuickPayPayapplyagreeSuccess(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                    Util.log("支付申请成功，得到支付信息："+quickPayPayapplyagreeResData);
                    payinfo = JsonTools.getJsonFromObject(quickPayPayapplyagreeResData);
                }
            });

        } catch (Exception e) {
          throw new CheckedException("支付失败，请稍后再试");
        }

    }

    /**
     * 通联微信支付
     *
     * @param trxamt 金额 单位为分
     * @param reqsn  商户交易单号 商户的交易订单号 32位
     * @param body   商品名称
     * @param acct   支付平台用户标识 JS支付时使用 微信支付-用户的微信openid
     */
    private void gotoOnPay(String trxamt, String reqsn, String body, String acct) throws Exception {

        UnifiedorderReqData unifiedorderReqData = new UnifiedorderReqData(trxamt, reqsn, body, acct);
        new UnifiedorderBusiness().run(unifiedorderReqData, new UnifiedorderBusiness.ResultListener() {
            @Override
            public void onUnifiedorderFail(UnifiedorderResData unifiedorderResData) {
                logger.error(UnifiedorderBusiness.result);
                throw new CheckedException(ValidateUtil.isEmpty(unifiedorderResData.getErrmsg())?"支付失败":unifiedorderResData.getErrmsg());
            }
            @Override
            public void onUnifiedorderSuccess(UnifiedorderResData unifiedorderResData) {
                logger.error("预支付成功，得到支付信息：" + unifiedorderResData.getPayinfo());
                payinfo = unifiedorderResData.getPayinfo();
            }
        });
    }


    /**
     * 支付数据验证
     *
     * @param payDto
     */
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    private void validateonPay(Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> pair, PayDto payDto) {
        //支付数据验证
        if (ValidateUtil.isEmpty(payDto.getAmount())) {
            throw new CheckedException("金额不能为空");
        }
        String regExp = "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"; // 判断小数点后2位的数字的正则表达式
        Pattern p = Pattern.compile(regExp);
        if (!p.matcher(payDto.getAmount().toString()).matches()) {
            throw new CheckedException("金额输入不合法");
        }
        TbUserAccount tbUserAccount = tbUserAccountService.selectUserInfoByUserId(pair.getLeft().getLeft().getId());
        if ((PayDto.PAYTYPE_QB.equals(payDto.getPayType())
                || PayDto.PAYTYPE_SX.equals(payDto.getPayType())||PayDto.PAYTYPE_YL.equals(payDto.getPayType())
        ) && !ENCODER.matches(payDto.getPassWord(), tbUserAccount.getTranPwd())) {
            throw new CheckedException("支付密码错误");
        }
        //使用的钱包 或者 授信额度时需要核对一下额度和余额是否足够支付 支付密码是否正确
        TbWallet selectTbWallet = getTbWallet(pair.getLeft().getLeft().getId());
        if (PayDto.PAYTYPE_QB.equals(payDto.getPayType()) && ValidateUtil.isEmpty(selectTbWallet)) {
            throw new CheckedException("钱包异常，请重新再试");
        }
        //1、优惠券是否符合使用条件 比如 有效期  金额是否满足减
        if (!ValidateUtil.isEmpty(pair.getRight()) && Calculate.greaterThan(pair.getRight().getFullMoney(), payDto.getAmount())) {
            throw new CheckedException("不能使用该优惠券，请重新选择优惠券");
        }
        if (!ValidateUtil.isEmpty(pair.getRight()) && pair.getRight().getIsUse().equals(MRedPacket.IS_USE_YES)) {
            throw new CheckedException("该优惠券已被使用，请重新选择优惠券");
        }

        if (PayDto.PAYTYPE_QB.equals(payDto.getPayType())) {
            BigDecimal tempMoney = Calculate.subtract(payDto.getAmount(),ValidateUtil.isEmpty(pair.getRight())?new BigDecimal("0"):
                    pair.getRight().getMoney()).getAmount();
            if(Calculate.lessThan(selectTbWallet.getTotalAmount(), tempMoney))
            {
                throw new CheckedException("钱包余额不足，请重新再试");
            }
        }

        //3、授信支付 授信额度是否被冻结
        TbAccountInfo tbaccountinfo = iTbAccountInfoService.selectTbAccountInfo(pair.getLeft().getLeft().getId(), pair.getLeft().getRight().getOid());
        if (PayDto.PAYTYPE_SX.equals(payDto.getPayType()) && ValidateUtil.isEmpty(tbaccountinfo)) {
            throw new CheckedException("无授信额度，请先进行授信");
        }
        if (PayDto.PAYTYPE_SX.equals(payDto.getPayType()) && TbAccountInfo.IS_FREEZE_YES.equals(tbaccountinfo.getIsFreeze())) {
            throw new CheckedException("授信额度已被冻结，请更改其他支付方式");
        }
        if (PayDto.PAYTYPE_SX.equals(payDto.getPayType())) {
            BigDecimal tempMoney = Calculate.subtract(payDto.getAmount(),ValidateUtil.isEmpty(pair.getRight())?new BigDecimal("0"):
                    pair.getRight().getMoney()).getAmount();
            if(Calculate.lessThan(new BigDecimal(DESdecryptdf(tbaccountinfo.getBalance())), tempMoney))
            {
                throw new CheckedException("授信额度不足，请重新再试");
            }

        }
    }

    /**
     * 支付成功回调 （包括现金支付支付通道的异步回调，以及钱包 授信的同步回调 都是调用这个方法）
     *
     * @param serialNo 交易订单信息单号
     * @param payType  支付类型
     */
    @Override
    @Transactional
    public void onpayNotify(String serialNo, String payType, TreeMap<String,String> map,String openId) {
        //交易记录
        TbTradingDetail selectTbTradingDetail = tbTradingDetailService.selectOne(new EntityWrapper<TbTradingDetail>().eq("serial_no", serialNo));
        //如果状态已经支付 则直接返回
        if(selectTbTradingDetail.getPaymentStatus().equals(TbTradingDetail.PAYMENT_STATUS_YES)){
            return;
        }
        //交易分润
        TbTradeProfitShare selectTbTradeProfitShare = iTbTradeProfitShareService.selectOne(new EntityWrapper<TbTradeProfitShare>().eq("trade_id", selectTbTradingDetail.getId()));
        //3、三级分销钱包处理
        HandleTbThreeSaleProfitShareWallet(selectTbTradeProfitShare);
        //4、分润钱包处理
        HandleTbTradeProfitShareWallet(selectTbTradeProfitShare);
        //5、如果使用了优惠券 则需要将优惠券设置为已使用
        if (!ValidateUtil.isEmpty(selectTbTradingDetail.getRpId())) {
            updateMRedPacket(selectTbTradingDetail.getRpId());
        }
        //区分是授信回调与非授信回调 --授信支付 分润时只添加钱包记录，不做钱包操作
        String isSettlement = TbTradingDetail.ISSETTLEMENT_YES;
        if (PayDto.PAYTYPE_SX.equals(payType)&&TbTradingDetail.TRADINGTYPE_SX_YES.equals(selectTbTradingDetail.getTradingType())) {
            //、扣除授信额度钱包
            updateTbaccountinfo(selectTbTradingDetail);
            isSettlement = TbTradingDetail.ISSETTLEMENT_NO;
        }
        //钱包支付扣除钱包余额
        if (PayDto.PAYTYPE_QB.equals(payType)) {
            TbWallet tbWallet = getTbWallet(selectTbTradingDetail.getUid());
            updateWallet(tbWallet, selectTbTradingDetail.getActualAmount(), TbWalletIncome.TYPE_7, selectTbTradeProfitShare);
        }
        //1、将交易记录 已支付 授信消费是否已结算 改为已结算
        updateTbTradingDetailStatus(selectTbTradingDetail, isSettlement);
        //2、修改交易利润分配单 结算单状态
        updateTbTradeProfitShareStatus(selectTbTradeProfitShare, isSettlement);
        //6、创建支付记录
        addPayment(selectTbTradingDetail, payType,map,openId);
        //7 创建三级分销关系
        addTbThreeSalesRef(selectTbTradingDetail);
        //8 修改用户 商户关系绑定
        addCuserrefcon(selectTbTradingDetail);
    }


    /**
     * 修改用户 商户关系绑定
     *
     * @param selectTbTradingDetail
     */
    private void addCuserrefcon(TbTradingDetail selectTbTradingDetail) {
        //首先查询用户是否有绑定关系
        CUserRefCon selectCUserRefCon = cUserRefConService.selectOne(new EntityWrapper<CUserRefCon>().eq("uid", selectTbTradingDetail.getUid()));
        if (ValidateUtil.isEmpty(selectCUserRefCon) || !ValidateUtil.isEmpty(selectCUserRefCon.getMid())) {
            return;
        }
        selectCUserRefCon.setMid(selectTbTradingDetail.getMid());
        cUserRefConService.updateById(selectCUserRefCon);
    }

    /**
     * 创建三级分销关系
     * @param selectTbTradingDetail
     */
    private void addTbThreeSalesRef(TbTradingDetail selectTbTradingDetail) {
        //首先查询用户是否有上级绑定关系
        TbThreeSalesRef selectTbThreeSalesRef = tbThreeSalesRefService.selectOne(new EntityWrapper<TbThreeSalesRef>().eq("user_id", selectTbTradingDetail.getUid()));
        if (!ValidateUtil.isEmpty(selectTbThreeSalesRef)) {
            return;
        }
        TbThreeSalesRef tbThreeSalesRef = new TbThreeSalesRef();
        tbThreeSalesRef.setUserId(selectTbTradingDetail.getUid());
        tbThreeSalesRef.setParentUserType(TbThreeSalesRef.PARENT_USERTYPE_MERCHANT);
        tbThreeSalesRef.setParentUserId(selectTbTradingDetail.getMid());
        tbThreeSalesRef.setUserType(selectTbTradingDetail.getUtype());
        tbThreeSalesRefService.insert(tbThreeSalesRef);
    }

    /**
     * 授信额度钱包操作
     */
    private void updateTbaccountinfo(TbTradingDetail selectTbTradingDetail) {
        //1、金额的扣除
        TbAccountInfo tbaccountinfo = iTbAccountInfoService.selectTbAccountInfo(selectTbTradingDetail.getUid(), selectTbTradingDetail.getOid());
        //金额解密
        String balanceMoney = DESdecryptdf(tbaccountinfo.getBalance());
        //金额加密
        String totlMoney = Calculate.subtract(new BigDecimal(balanceMoney), selectTbTradingDetail.getActualAmount()).toString();
        tbaccountinfo.setBalance(DESencryptdf(totlMoney));
        iTbAccountInfoService.updateById(tbaccountinfo);
        //2、操作记录
        addTbaccounttradinglog(tbaccountinfo, selectTbTradingDetail.getActualAmount(), selectTbTradingDetail.getId());
    }

    /**
     * 金额的解密
     */
    private String DESdecryptdf(final String banlance) {
        String balanceMoney = null;
        try {
            balanceMoney = DES.decryptdf(banlance);
            return balanceMoney;
        } catch (Exception e) {
            throw new CheckedException("授信支付失败");
        }
    }

    /**
     * 金额加密
     *
     * @return
     */
    private String DESencryptdf(final String banlance) {
        try {
            return DES.encryptdf(banlance);
        } catch (Exception e) {
            throw new CheckedException("授信支付失败");
        }
    }

    private void addTbaccounttradinglog(TbAccountInfo tbaccountinfo, BigDecimal money, String resId) {
        String balanceMoney = DESdecryptdf(tbaccountinfo.getBalance());
        TbAccountTradingLog tbAccountTradingLog = new TbAccountTradingLog();
        tbAccountTradingLog.setAid(tbaccountinfo.getId());
        tbAccountTradingLog.setDirection(TbAccountTradingLog.DIRECTION_0);
        tbAccountTradingLog.setBizType("授信额度消费");
        tbAccountTradingLog.setAmount(money);
        tbAccountTradingLog.setBalance(new BigDecimal(balanceMoney));
        tbAccountTradingLog.setResId(resId);
        tbAccountTradingLog.setCreateDate(new Date());
        iTbAccountTradingLogService.insert(tbAccountTradingLog);
    }

    /**
     * 处理三级分销分润处理
     *
     * @param tbTradeProfitShare 交易分润
     */
    private void HandleTbThreeSaleProfitShareWallet(TbTradeProfitShare tbTradeProfitShare) {
        //如果没有三级分销则直接返回
        if (ValidateUtil.isEmpty(tbTradeProfitShare.getThreeSaleShareId())) {
            return;
        }
        TbThreeSaleProfitShare selectTbThreeSaleProfitShare = tbThreeSaleProfitShareService.selectById(tbTradeProfitShare.getThreeSaleShareId());
        //直接上级
        TbWallet redirectTbWallet = getTbWallet(selectTbThreeSaleProfitShare.getRedirectUserId());
        updateWallet(redirectTbWallet, selectTbThreeSaleProfitShare.getRedirectShareAmount(), TbWalletIncome.TYPE_4, tbTradeProfitShare);
        //存在间接上级的情况
        if (!ValidateUtil.isEmpty(selectTbThreeSaleProfitShare.getInderectUserId())) {
            TbWallet inderectTbWallet = getTbWallet(selectTbThreeSaleProfitShare.getInderectUserId());
            updateWallet(inderectTbWallet, selectTbThreeSaleProfitShare.getInderectShareAmount(), TbWalletIncome.TYPE_4, tbTradeProfitShare);
        }
    }

    /**
     * 往用户或者商户钱包加钱 或者 扣钱 授信支付只添加记录
     *
     * @param money              金额
     * @param tbWalletIncomeType 资金收支类型
     * @param tbTradeProfitShare 交易利润分配
     */
    private void updateWallet(TbWallet selectTbWallet, BigDecimal money, String tbWalletIncomeType, TbTradeProfitShare tbTradeProfitShare) {
        //当金额为0 直接return
        if(Calculate.equalsZero(money)){
            return;
        }
        //结算状态 默认为未结算 如果不是授信支付 则改为 已结算
        String isSettlement = TbTradingDetail.ISSETTLEMENT_NO;
        //非授信支付需要直接做钱包结算
        if (!TbTradingDetail.TRADINGTYPE_SX_YES.equals(tbTradeProfitShare.getTradeType())) {
            updateWallet(selectTbWallet, money, tbWalletIncomeType);
            isSettlement = TbTradingDetail.ISSETTLEMENT_YES;
        }
        //3、创建钱包收支记录
        addWalletincome(selectTbWallet, money, tbWalletIncomeType,
                isSettlement ,tbTradeProfitShare);
    }

    /**
     * 钱包收款 扣款 操作
     *
     * @param selectTbWallet     钱包
     * @param money              金额
     * @param TbWalletIncomeType 操作类型 入账 消费等
     */
    private void updateWallet(TbWallet selectTbWallet, BigDecimal money, String TbWalletIncomeType) {
        //根据资金收支类型来觉得钱包的加减
        //收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出',
        if (TbWalletIncome.TYPE_1.equals(TbWalletIncomeType) ||
                TbWalletIncome.TYPE_2.equals(TbWalletIncomeType) ||
                TbWalletIncome.TYPE_3.equals(TbWalletIncomeType) ||
                TbWalletIncome.TYPE_4.equals(TbWalletIncomeType) ||
                TbWalletIncome.TYPE_5.equals(TbWalletIncomeType)) {
            selectTbWallet.setTotalAmount(Calculate.add(selectTbWallet.getTotalAmount(), money).getAmount());
        }
        if (TbWalletIncome.TYPE_6.equals(TbWalletIncomeType) || TbWalletIncome.TYPE_7.equals(TbWalletIncomeType)
                || TbWalletIncome.TYPE_8.equals(TbWalletIncomeType)) {
            selectTbWallet.setTotalAmount(Calculate.subtract(selectTbWallet.getTotalAmount(), money).getAmount());
        }
        tbWalletService.updateById(selectTbWallet);

    }

    /**
     * 根据用户ID 或者商户ID 查询账户表  能到对应的账户ID在关联钱包表进行查询
     *
     * @param userId
     * @return
     */
    private TbWallet getTbWallet(String userId) {
        //用户或者商户钱包
        return tbWalletService.selectUserTbWallet(userId);

    }

    private TbWallet getOOperaInfoTbWallet(String oid) {
        //运营商 or 平台
        return tbWalletService.selectOne(new EntityWrapper<TbWallet>().eq("uid", oid));
    }

    /**
     * 添加钱包收支明细
     *
     */
    private void addWalletincome(TbWallet selectTbWallet, BigDecimal money, String type,
                                 String settlement_status,TbTradeProfitShare tbTradeProfitShare) {

        TbTradingDetail selectTbTradingDetail = tbTradingDetailService.selectById(tbTradeProfitShare.getTradeId());
        CUserInfo selectCuserInfo = cuserinfoservice.selectById(selectTbTradingDetail.getUid());
        TbWalletIncome tbWalletIncome = new TbWalletIncome();
        tbWalletIncome.setWid(selectTbWallet.getId());
        tbWalletIncome.setPlayid(selectTbTradingDetail.getUid());
        tbWalletIncome.setPtepy(selectTbTradingDetail.getUtype());
        tbWalletIncome.setType(type);
        tbWalletIncome.setPname(selectCuserInfo.getRealName());
        tbWalletIncome.setPmobile(selectCuserInfo.getMobile());
        //这里有疑问 两个金额不知道代表啥 先统一都为交易金额
        tbWalletIncome.setTradeTotalAmount(money);
        tbWalletIncome.setAmount(money);
        tbWalletIncome.setCreateDate(new Date());
        tbWalletIncome.setTid(tbTradeProfitShare.getTradeId());
        tbWalletIncome.setOid(tbTradeProfitShare.getOid());
        tbWalletIncome.setSettlementStatus(settlement_status);
        iTbWalletIncomeService.insert(tbWalletIncome);
    }

    /**
     * 分润处理 将分润往各个角色钱包上操作
     */
    private void HandleTbTradeProfitShareWallet(TbTradeProfitShare selectTbTradeProfitShare) {

        //授信支付 统一收支类型为 授信金结算
        String bWalletIncomeType = TbWalletIncome.TYPE_2;
        if (TbTradingDetail.TRADINGTYPE_SX_YES.equals(selectTbTradeProfitShare.getTradeType())) {
            bWalletIncomeType = TbWalletIncome.TYPE_3;
        }
        //1、交易商户入账金额
        TbWallet mTbWallet = getTbWallet(selectTbTradeProfitShare.getMid());
        updateWallet(mTbWallet, selectTbTradeProfitShare.getMerchantInhomeAmount(),
                bWalletIncomeType, selectTbTradeProfitShare);
        //运营商 运营商交易抽成费（优惠卷中设置的抽取折扣金额） 入账金额
        TbWallet oTbWallet = getOOperaInfoTbWallet(selectTbTradeProfitShare.getOid());
        updateWallet(oTbWallet, selectTbTradeProfitShare.getOperatorShareFee(),
                TbWalletIncome.TYPE_4, selectTbTradeProfitShare);

        //运营商 运营商通道费（商户让利费）入账金额
        TbWallet oTbWallet3 = getOOperaInfoTbWallet(selectTbTradeProfitShare.getOid());
        updateWallet(oTbWallet, selectTbTradeProfitShare.getOperaShareFee(),
                bWalletIncomeType, selectTbTradeProfitShare);

        //运营商 扣减平台抽成费
        TbWallet oTbWallet2 = getOOperaInfoTbWallet(selectTbTradeProfitShare.getOid());
        updateWallet(oTbWallet, selectTbTradeProfitShare.getPlatfromShareFee(),
                TbWalletIncome.TYPE_8, selectTbTradeProfitShare);

    }

    /**
     * 修改交易利润分配状态为 已结算
     *
     * @param selectTbTradeProfitShare //交易利润分配
     */
    private void updateTbTradeProfitShareStatus(TbTradeProfitShare selectTbTradeProfitShare, String isSettlement) {
        //根据交易单查询交易分润单
        selectTbTradeProfitShare.setIsSettlement(isSettlement);
        iTbTradeProfitShareService.updateById(selectTbTradeProfitShare);
    }

    /**
     * 修改交易记录单 为 已支付 已结算
     *
     * @param selecttbTradingDetail
     */
    private void updateTbTradingDetailStatus(TbTradingDetail selecttbTradingDetail, String isSettlement) {
        selecttbTradingDetail.setPaymentStatus(TbTradingDetail.PAYMENT_STATUS_YES);
        selecttbTradingDetail.setIsSettlement(isSettlement);
        tbTradingDetailService.updateById(selecttbTradingDetail);
    }

    /**
     * 将优惠券设置为已使用
     *
     * @param mRedPacketId
     */
    public void updateMRedPacket(String mRedPacketId) {
        if (ValidateUtil.isEmpty(mRedPacketId)) {
            return;
        }
        MRedPacket mRedPacket = new MRedPacket();
        mRedPacket.setId(mRedPacketId);
        mRedPacket.setIsUse(MRedPacket.IS_USE_YES);
        mredPacketService.updateById(mRedPacket);
    }

    /**
     * 支付记录
     */
    private void addPayment(TbTradingDetail selectTbTradingDetail,String payType,TreeMap<String,String> map,String openId) {
        logger.error("通联交易单号："+map.get("trxid"));
        TbPayment payment = new TbPayment();
        payment.setPaymentSn(map.isEmpty()?UUID.randomUUID().toString().substring(0, 28):map.get("trxid"));
        payment.setMiId(selectTbTradingDetail.getMid());
        payment.setStatus(selectTbTradingDetail.getPaymentStatus());
        payment.setOiId(selectTbTradingDetail.getSerialNo());
        payment.setMoney(selectTbTradingDetail.getActualAmount());
        payment.setOid(selectTbTradingDetail.getOid());
        payment.setCreateDate(new Date());
        payment.setIp("127.0.0.1");
        payment.setPayDate(new Date());
        payment.setPaymentType(payType);
        payment.setOpenid(openId);
        paymentService.insert(payment);
    }


    /**
     *  交易利润分配
     * @param payDto 界面入参
     * @param tbTradingDetail 交易记录
     * @param tbThreeSaleProfitShare 三级分销信息
     * @param pair 根据页面入参 查询到的相关信息 比如用户信息 商户信息 优惠券信息
     * @return
     */
    private TbTradeProfitShare addTbTradeProfitShare(PayDto payDto, TbTradingDetail tbTradingDetail,
                                                     TbThreeSaleProfitShare tbThreeSaleProfitShare,
                                                     Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> pair) {
        //商户信息
        MMerchantInfo mMerchantInfo = pair.getLeft().getRight();
        //优惠券信息
        MRedPacket mRedPacket = pair.getRight();
        //商户其他信息
        MMerchantOtherInfo mMerchantOtherInfo = merchantOtherInfoService.selectOne(new EntityWrapper<MMerchantOtherInfo>().eq("mid", mMerchantInfo.getId()));
        //运营商信息
        OOperaOtherInfo oOperaOtherInfo = operaOtherInfoService.selectOne(new EntityWrapper<OOperaOtherInfo>().eq("oid", mMerchantInfo.getOid()));
        //三级分销比列分配
        TbThreeSalesRate tbThreeSalesRate = tbThreeSalesRateService.selectOne(new EntityWrapper<TbThreeSalesRate>().eq("oid", mMerchantInfo.getOid()));

        //交易利润分配
        TbTradeProfitShare tbTradeProfitShare = new TbTradeProfitShare();
        //优惠券设置信息
        MRedPacketSetting mRedPacketSetting = null;
        //如果使用优惠券
        if (!ValidateUtil.isEmpty(mRedPacket)) {
            mRedPacketSetting = mRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", mRedPacket.getRpsId()));
            tbTradeProfitShare.setIsUseDiscout(TbTradingDetail.ISUSEDISCOUT_YES);
        } else {
            tbTradeProfitShare.setIsUseDiscout(TbTradingDetail.ISUSEDISCOUT_NO);
        }
        //交易记录ID
        tbTradeProfitShare.setTradeId(tbTradingDetail.getId());
        //交易类型 0-授信消费 1-非授信消费
        tbTradeProfitShare.setTradeType(tbTradingDetail.getTradingType());
        //运营商ID
        tbTradeProfitShare.setOid(mMerchantInfo.getOid());
        //商户ID
        tbTradeProfitShare.setMid(mMerchantInfo.getId());
        //用户原始支付的金额（不含优惠券金额 比如满100 减20 这里还是100块钱，真正扣钱的时候会去计算扣除这个优惠券的20块钱）
        BigDecimal payAmount = payDto.getAmount();
        //优惠券优惠金额（这里是指真正的优惠金额，比如商家设定的是100 减 20 运营商抽取了10块钱做为三级分销，这里的优惠金额就是10块钱）
        BigDecimal mRedPacketMoney = ValidateUtil.isEmpty(mRedPacket) ? new BigDecimal(0) : mRedPacket.getMoney();
        //三级分销总金额 = 运营商抽取优惠金额(优惠券设置里面，这里包含了运营商收取的优惠券折扣费 运营商+上级+间接上级)
        BigDecimal operatorDiscountFee = ValidateUtil.isEmpty(mRedPacketSetting) ? new BigDecimal(0) : mRedPacketSetting.getOpMoney();
        //商户设置的原始优惠金额
        BigDecimal mRedPacketSettingMoney = ValidateUtil.isEmpty(mRedPacketSetting) ? new BigDecimal(0) : mRedPacketSetting.getMoney();
        //运营商的商户让利费率（百分之）
        BigDecimal shareFee = ValidateUtil.isEmpty(mMerchantOtherInfo.getShareFee()) ? new BigDecimal(0) : mMerchantOtherInfo.getShareFee();
        //平台抽取运营商服务费率（百分之）
        BigDecimal platfromShareRatio = ValidateUtil.isEmpty(oOperaOtherInfo.getPlatfromShareRatio()) ? new BigDecimal(0) : oOperaOtherInfo.getPlatfromShareRatio();
        //金额计算
        Map<String, BigDecimal> map = calculation(payAmount, mRedPacketMoney, shareFee, platfromShareRatio,
                operatorDiscountFee,tbThreeSalesRate.getOpaRate());

        // 交易总金额
        tbTradeProfitShare.setTotalTradeAmount(map.get("totalTradeAmount"));
        //运营商通道费
        tbTradeProfitShare.setOperaShareFee(map.get("operaShareFee"));
        //交易商户入账金额
        tbTradeProfitShare.setMerchantInhomeAmount(map.get("merchantInhomeAmount"));
        //平台抽成运营商交易费用  (平台抽取运营商服务费率 *运营商交易抽成费)
        tbTradeProfitShare.setPlatfromShareFee(map.get("platfromsharefee"));
        //运营商的抽取的优惠券的折扣费
        tbTradeProfitShare.setOperatorShareFee(map.get("operatorSharefee"));
        //用于三级分销的商户让利折扣比例  运营商抽取优惠金额/商户设置的原始优惠金额
        BigDecimal operatorDiscountRatio = Calculate.multiply_100(ValidateUtil.isEmpty(mRedPacketSetting) ? new BigDecimal("0") :
                Calculate.division(operatorDiscountFee, mRedPacketSettingMoney).getAmount()).getAmount();
        tbTradeProfitShare.setOperatorDiscountRatio(operatorDiscountRatio.longValue());
        //真正的三级分销总金额(三级分销总金额（含运营商抽取的折扣费-运营商抽取的折扣费） 这里是指真正的三级分销金额 只含有上级金额+间接上级金额)
        tbTradeProfitShare.setOperatorDiscountFee(Calculate.subtract(operatorDiscountFee,map.get("operatorSharefee")).getAmount());
        //平台抽成运营商交易费用比率  = 平台抽取运营商服务费率
        tbTradeProfitShare.setPlatfromShareRatio(oOperaOtherInfo.getPlatfromShareRatio());
        tbTradeProfitShare.setIsSettlement(TbTradingDetail.ISSETTLEMENT_NO);
        //如果有三级分销 则回写三级分销ID
        if (!ValidateUtil.isEmpty(tbThreeSaleProfitShare)) {
            tbTradeProfitShare.setThreeSaleShareId(tbThreeSaleProfitShare.getId());
        }
        tbTradeProfitShare.setTradingDate(tbTradingDetail.getCreateDate());
        tbTradeProfitShare.setCreateDate(new Date());
        iTbTradeProfitShareService.insert(tbTradeProfitShare);
        return tbTradeProfitShare;
    }

    /**
     * 交易分润计算
     * @param payAmount  用户原始支付的金额（不含优惠券金额 比如满100 减20 这里还是100块钱，真正扣钱的时候会去计算扣除这个优惠券的20块钱）
     * @param mRedPacketMoney 优惠券优惠金额（这里是指真正的优惠金额，比如商家设定的是100 减 20 运营商抽取了10块钱做为三级分销，这里的优惠金额就是10块钱）
     * @param shareFee  运营商的商户让利费率（百分之）
     * @param platfromShareRatio 平台抽取运营商服务费率（百分之）
     * @param operatorDiscountFee  三级分销总金额（运营商+上级+间接上级） = 运营商抽取优惠金额(优惠券设置里面，这里包含了运营商收取的优惠券折扣费)
     * @param opaRate 三级分销比列分配
     * @return
     */
    private Map<String, BigDecimal> calculation(BigDecimal payAmount, BigDecimal mRedPacketMoney,
                                                BigDecimal shareFee, BigDecimal platfromShareRatio,
                                                BigDecimal operatorDiscountFee , Integer opaRate) {

        //真实的交易总金额 = 交易总金额-减去优惠券优惠的金额
        BigDecimal totalTradeAmount = Calculate.subtract(payAmount, mRedPacketMoney).getAmount();

        //运营商抽取通道费 （交易总金额*运营商的商户让利）
        BigDecimal operaShareFee = Calculate.multiply(totalTradeAmount, Calculate.division_100(shareFee).getAmount()).getAmount();

        //平台抽成费 = 真实的交易总金额 * 平台抽取运营商服务费率
        BigDecimal platfromsharefee = Calculate.multiply(totalTradeAmount, Calculate.division_100(platfromShareRatio).getAmount()).getAmount();

        //交易商户入账金额 交易总金额（原始）-三级分销总金额(优惠卷金额)-运营商通道费
        BigDecimal merchantInhomeAmount = Calculate.subtract(Calculate.subtract(totalTradeAmount,
                operatorDiscountFee).getAmount(), operaShareFee).getAmount();

        //运营商的抽取的优惠券的折扣费（根据优惠券设置信息中的 运营商抽取比列*优惠券总金额）
        BigDecimal operatorSharefee =Calculate.multiply(operatorDiscountFee,Calculate.division_100(new BigDecimal(opaRate)).getAmount()).getAmount();

        Map<String, BigDecimal> map = new HashMap<>();

        //真实的交易总金额 = 交易总金额-减去优惠券优惠的金额
        map.put("totalTradeAmount", totalTradeAmount);
        //运营商抽取通道费
        map.put("operaShareFee", operaShareFee);
        ///交易商户入账金额
        map.put("merchantInhomeAmount", merchantInhomeAmount);
        //平台抽成费
        map.put("platfromsharefee", platfromsharefee);
        //运营商的抽取的优惠券的折扣费
        map.put("operatorSharefee", operatorSharefee);

        return map;
    }

    public static void main(String[] args) {

        System.out.println(Calculate.multiply_100(Calculate.subtract(new BigDecimal("0.01"), new BigDecimal("0")).getAmount()).format(0).toString());
        //System.out.println(Calculate.subtract(Calculate.multiply_100(new BigDecimal("0.01")).format(0),new BigDecimal("0")).format(0));
        //原始总金额
        BigDecimal payAmount = new BigDecimal("100");
        //用户真正优惠券优惠金额（这里是指真正的优惠金额，比如商家设定的是100 减 20 运营商抽取了10块钱做为三级分销，这里的优惠金额就是10块钱）
        BigDecimal mRedPacketMoney = new BigDecimal("10");
        //运营商的商户让利费率（百分之）(商户信息里面设置 )
        BigDecimal shareFee = new BigDecimal(1);
        //平台抽取运营商服务费率（百分之）（运营商里面设置）
        BigDecimal platfromShareRatio = new BigDecimal(1);
        //三运营级分销总金额 = 商抽取优惠金额(优惠券设置里面)
        BigDecimal operatorDiscountFee = new BigDecimal("10");
        //运营商抽取优惠券折扣的比率
        Integer opaRate =50;

        PayServiceImpl payServiceImpl = new PayServiceImpl();
        Map<String, BigDecimal> map = payServiceImpl.calculation(payAmount, mRedPacketMoney, shareFee,
                platfromShareRatio, operatorDiscountFee,opaRate);
        System.out.println("交易总金额 =" + map.get("totalTradeAmount"));
        System.out.println("运营商交易折扣抽成费 =" + map.get("operatorSharefee"));
        System.out.println("交易商户入账金额 =" + map.get("merchantInhomeAmount"));
        System.out.println("平台抽成费 =" + map.get("platfromsharefee"));
        System.out.println("运营商通道抽成费 =" + map.get("operaShareFee"));
    }


    /**
     * 获取用户信息，商户信息，优惠券信息
     *
     * @param payDto
     * @return
     */
    private Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> selectObjectData(PayDto payDto) {
        //查询用户信息
        CUserInfo cUserInfo = cuserinfoservice.selectById(payDto.getUserId());
        //查询商户信息
        MMerchantInfo mMerchantInfo = mMerchantInfoService.selectById(payDto.getmId());
        //查询优惠券信息
        MRedPacket mRedPacket = null;
        if (!ValidateUtil.isEmpty(payDto.getMredmarketId())) {
            mRedPacket = mredPacketService.selectById(payDto.getMredmarketId());
        }
        return new Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket>(new Pair<CUserInfo, MMerchantInfo>(cUserInfo,
                mMerchantInfo), mRedPacket);
    }

    /**
     * 创建交易订单
     *
     * @param payDto
     */
    private TbTradingDetail addTbTradingDetail(PayDto payDto, Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> pair) {
        CUserInfo cUserInfo = pair.getLeft().getLeft();
        MMerchantInfo mMerchantInfo = pair.getLeft().getRight();
        MRedPacket mRedPacket = pair.getRight();
        TbTradingDetail tbTradingDetail = new TbTradingDetail();
        //订单号
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker (1, 1);
        tbTradingDetail.setSerialNo(String.valueOf(idWorker.nextId()));
        //商户ID
        tbTradingDetail.setMid(mMerchantInfo.getId());
        //运营商ID
        tbTradingDetail.setOid(mMerchantInfo.getOid());
        //用户ID
        tbTradingDetail.setUid(cUserInfo.getId());
        //用户类型
        tbTradingDetail.setUtype(cUserInfo.getUserAttribute());
        //交易类型 交易类型（0-授信消费 1-非授信消费）
        tbTradingDetail.setTradingType(payDto.checkTradingType());
        //支付状态
        tbTradingDetail.setPaymentStatus(TbTradingDetail.PAYMENT_STATUS_NO);
        //交易原始总金额
        tbTradingDetail.setTradeAmount(payDto.getAmount());

        if (!ValidateUtil.isEmpty(mRedPacket)) {
            //是否使用了优惠券
            tbTradingDetail.setIsUseDiscout(TbTradingDetail.ISUSEDISCOUT_YES);
            //优惠金额
            tbTradingDetail.setDiscountAmount(mRedPacket.getMoney());
            //优惠券ID
            tbTradingDetail.setRpId(mRedPacket.getId());
            //实际交易总金额=交易总金额-优惠金额(使用了优惠券的情况)
            tbTradingDetail.setActualAmount(Calculate.subtract(payDto.getAmount(),
                    mRedPacket.getMoney()).getAmount());
        } else {
            tbTradingDetail.setIsUseDiscout(TbTradingDetail.ISUSEDISCOUT_NO);
            //实际交易总金额=交易总金额 (未使用了优惠券的情况)
            tbTradingDetail.setActualAmount(payDto.getAmount());
        }
        //交易时间
        tbTradingDetail.setTradingDate(new Date());
        //交易ID 待获取
        tbTradingDetail.setIp("127.0.0.1");
        //授信消费是否已结算
        tbTradingDetail.setIsSettlement(TbTradingDetail.ISSETTLEMENT_NO);
        //创建时间
        tbTradingDetail.setCreateDate(new Date());
        tbTradingDetailService.insert(tbTradingDetail);
        return tbTradingDetail;
    }


    private TbThreeSaleProfitShare addTbThreeSaleProfitShare(String mredmarketId, Pair<Pair<CUserInfo, MMerchantInfo>, MRedPacket> pair) {
        //未使用优惠券 则不涉及到三级分销
        if (ValidateUtil.isEmpty(mredmarketId)) {
            return null;
        }
        //用户信息
        CUserInfo cUserInfo = pair.getLeft().getLeft();
        //商户信息
        MMerchantInfo mMerchantInfo = pair.getLeft().getRight();
        //获取直接上级 间接上级
        Pair<Pair<String, String>, Pair<String, String>> tTbThreeSalesRefPair = getTbThreeSalesRef(cUserInfo,mMerchantInfo);
        //查询三级分销分润比例
        TbThreeSalesRate tbThreeSalesRate = tbThreeSalesRateService.selectOne(new EntityWrapper<TbThreeSalesRate>().eq("oid", mMerchantInfo.getOid()));

        TbThreeSaleProfitShare tbThreeSaleProfitShare = new TbThreeSaleProfitShare();

        tbThreeSaleProfitShare.setRedirectUserId(tTbThreeSalesRefPair.getLeft().getLeft());
        tbThreeSaleProfitShare.setRedirectUserType(tTbThreeSalesRefPair.getLeft().getRight());
        tbThreeSaleProfitShare.setInderectUserId(tTbThreeSalesRefPair.getRight().getLeft());
        tbThreeSaleProfitShare.setInderectUserType(tTbThreeSalesRefPair.getRight().getRight());
        //优惠券信息
        MRedPacket mRedPacket = pair.getRight();
        //查询优惠券配置信息
        MRedPacketSetting mRedPacketSetting = mRedPacketSettingService.selectOne(new EntityWrapper<MRedPacketSetting>().eq("id", mRedPacket.getRpsId()));
        //计算三级分销利润分配
        //三级分销金额等于 优惠券中抽取用于三级分销的金额*分润比例
        tbThreeSaleProfitShare.setInderectShareAmount(Calculate.multiply(mRedPacketSetting.getOpMoney(),
                Calculate.division_100(new BigDecimal(tbThreeSalesRate.getIndirectRate())).getAmount()).getAmount());
        tbThreeSaleProfitShare.setRedirectShareAmount(Calculate.multiply(mRedPacketSetting.getOpMoney(),
                Calculate.division_100(new BigDecimal(tbThreeSalesRate.getRedirectRate())).getAmount()).getAmount());
        tbThreeSaleProfitShare.setOid(mMerchantInfo.getOid());
//        tbThreeSaleProfitShare.setOpaShareAmount(Calculate.multiply(mRedPacketSetting.getOpMoney(),
//                Calculate.division_100(new BigDecimal(tbThreeSalesRate.getOpaRate())).getAmount()).getAmount());
        tbThreeSaleProfitShareService.insert(tbThreeSaleProfitShare);
        return tbThreeSaleProfitShare;
    }

    /**
     * 获取直接上级 间接上级 关系
     *
     * @return
     */
    private Pair<Pair<String, String>, Pair<String, String>> getTbThreeSalesRef(CUserInfo cUserInfo, MMerchantInfo mMerchantInfo) {
        //查询直接上级ID
        TbThreeSalesRef oneParentTbThreeSalesRef = tbThreeSalesRefService.selectOne(new EntityWrapper<TbThreeSalesRef>().eq("user_id", cUserInfo.getId()));
        //没有直接上级 直接上级 间接上级为支付用户自己
        if (ValidateUtil.isEmpty(oneParentTbThreeSalesRef)) {
            return new Pair<Pair<String, String>, Pair<String, String>>(
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER),
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER));
        }
        //如果直接上级为商户分两种情况 第一种 如果本次消费的商户跟直接上级相同，则直接上级收益 和 则间接上级收益 都为支付用户自己
        if (TbThreeSalesRef.PARENT_USERTYPE_MERCHANT.equals(oneParentTbThreeSalesRef.getParentUserType())&&mMerchantInfo.getId().equals(oneParentTbThreeSalesRef.getParentUserId())) {
            return new Pair<Pair<String, String>, Pair<String, String>>(
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER),
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER));

        }
        //如果直接上级为商户 第二种情况 直接上级不是本次消费商户则 只是上级就是给直接上级 间接上级给支付用户自己
        if (TbThreeSalesRef.PARENT_USERTYPE_MERCHANT.equals(oneParentTbThreeSalesRef.getParentUserType())&&
                !mMerchantInfo.getId().equals(oneParentTbThreeSalesRef.getParentUserId())) {
            return new Pair<Pair<String, String>, Pair<String, String>>(
                    Pair.create(oneParentTbThreeSalesRef.getParentUserId(), oneParentTbThreeSalesRef.getParentUserType()),
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER));

        }
        //查询间接上级
        TbThreeSalesRef twoParentTbThreeSalesRef = tbThreeSalesRefService.selectOne(new EntityWrapper<TbThreeSalesRef>().eq("user_id", oneParentTbThreeSalesRef.getParentUserId()));
        //间接上级为空 则将间接上级设定为支付用户自己
        if(ValidateUtil.isEmpty(twoParentTbThreeSalesRef)) {
            return new Pair<Pair<String, String>, Pair<String, String>>(
                    Pair.create(oneParentTbThreeSalesRef.getParentUserId(), oneParentTbThreeSalesRef.getParentUserType()),
                    Pair.create(cUserInfo.getId(), TbThreeSalesRef.PARENT_USERTYPE_USER));
        }
        //正常的三级关系
        return new Pair<Pair<String, String>, Pair<String, String>>(
                Pair.create(oneParentTbThreeSalesRef.getParentUserId(), oneParentTbThreeSalesRef.getParentUserType()),
                Pair.create(twoParentTbThreeSalesRef.getParentUserId(), twoParentTbThreeSalesRef.getParentUserType()));
    }

}
