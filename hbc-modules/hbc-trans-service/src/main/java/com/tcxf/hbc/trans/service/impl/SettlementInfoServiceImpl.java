package com.tcxf.hbc.trans.service.impl;

import com.alibaba.fastjson.JSON;
import com.tcxf.hbc.common.entity.TbUserSharefeeSettlement;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.dao.SettlementDao;
import com.tcxf.hbc.trans.model.dto.SettlementInfoDto;
import com.tcxf.hbc.trans.service.ISettlementInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@Service
public class SettlementInfoServiceImpl  implements ISettlementInfoService {
    private static final Logger logger = LoggerFactory.getLogger(SettlementInfoServiceImpl.class);
    @Autowired
    SettlementDao settlementDao;

    /**
     * 结算
     * 钱包入账
     * @param settlementId
     * @return
     */
    @Override
    @Transactional
    public HashMap<String, Object> settlementAllUser(String settlementId) {
        HashMap<String,Object> map = new HashMap<String,Object>();
            logger.error("=============================结算id:========================"+ settlementId);
            SettlementInfoDto sd = settlementDao.getOpratorSettlementById(settlementId);
            String oid = sd.getOid();//运营商id

            List<TbWalletIncome> listAllIncome= settlementDao.findAllMoneyBySid(settlementId);
            logger.error("================listAllIncome:=============="+JSON.toJSONString(listAllIncome));

            for (int i = 0; i <listAllIncome.size() ; i++) {

                TbWallet tw = settlementDao.getUserWalletById(listAllIncome.get(i).getWid());
                logger.error("钱包本金："+tw.getTotalAmount());
                logger.error("钱包入金:"+listAllIncome.get(i).getAmount());
                BigDecimal bd = Calculate.add(tw.getTotalAmount(),listAllIncome.get(i).getAmount()).getAmount();
                settlementDao.updateWalletById(bd,listAllIncome.get(i).getWid());
            }
            //获取运营商钱包
            TbWallet oWallet = settlementDao.getUserWalletByOid(oid);
            //平台抽成运营商总费用
            logger.error("settlementId,:"+settlementId+"  oWallet.getId():"+oWallet.getId());
            TbWalletIncome  ti= settlementDao.findTotalMoneyByOid(settlementId,oWallet.getId());
            logger.error("平台钱包本金："+oWallet.getTotalAmount());
            logger.error("平台出金:"+JSON.toJSONString(ti));
            if(ti!=null && ti.getAmount()!=null && !Calculate.equalsZero(ti.getAmount()))
            {
                BigDecimal obd = Calculate.subtract(oWallet.getTotalAmount(),ti.getAmount()).getAmount();
                logger.error("平台钱包入金："+obd);
                settlementDao.updateWalletById(obd,oWallet.getId());
            }


            //获取钱包结算交易记录
            List<TbWalletIncome> incomeList = settlementDao.getWalletIncomeList(settlementId);
            //更新分润表结算状态
            if(incomeList!=null && incomeList.size()>0)
            {
                logger.error("更新分润表结算状态:"+ JSON.toJSONString(incomeList));
                settlementDao.updateTradeProfitShare(incomeList);
            }
            //更新钱包结算状态
            logger.error("更新钱包结算状态:"+ settlementId);
            settlementDao.updateUserWalletStatus(settlementId);
            map.put("result","success");
//        List<TbUserSharefeeSettlement> tsdList = settlementDao.getUserSettlementById(settlementId);
        return map;
    }
}
