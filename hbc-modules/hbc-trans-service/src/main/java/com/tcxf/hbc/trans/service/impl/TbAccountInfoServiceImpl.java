package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbAccountInfo;
import com.tcxf.hbc.trans.mapper.TbAccountInfoMapper;
import com.tcxf.hbc.trans.service.ITbAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户授信资金额度表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbAccountInfoServiceImpl extends ServiceImpl<TbAccountInfoMapper, TbAccountInfo> implements ITbAccountInfoService {

    @Autowired
    TbAccountInfoMapper tbAccountInfoMapper;

    @Override
    public TbAccountInfo selectTbAccountInfo(String userId, String oId) {
        return tbAccountInfoMapper.selectTbAccountInfo(userId,oId);
    }
}
