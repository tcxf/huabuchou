package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbAccountTradingLog;
import com.tcxf.hbc.trans.mapper.TbAccountTradingLogMapper;
import com.tcxf.hbc.trans.service.ITbAccountTradingLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信资金流水 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbAccountTradingLogServiceImpl extends ServiceImpl<TbAccountTradingLogMapper, TbAccountTradingLog> implements ITbAccountTradingLogService {

}
