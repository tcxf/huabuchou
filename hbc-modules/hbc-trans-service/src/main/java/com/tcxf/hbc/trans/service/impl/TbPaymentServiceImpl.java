package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbPayment;
import com.tcxf.hbc.trans.mapper.TbPaymentMapper;
import com.tcxf.hbc.trans.service.ITbPaymentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付记录表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service

public class TbPaymentServiceImpl extends ServiceImpl<TbPaymentMapper, TbPayment> implements ITbPaymentService {

}
