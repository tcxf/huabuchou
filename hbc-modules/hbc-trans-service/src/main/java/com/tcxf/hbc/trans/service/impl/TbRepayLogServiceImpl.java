package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.bean.config.PayStatusPropertiesConfig;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.common.util.*;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.common.util.secure.DES;
import com.tcxf.hbc.common.vo.PayagreeconfirmDto;
import com.tcxf.hbc.common.vo.PaybackMoneyDto;
import com.tcxf.hbc.trans.dao.SettlementDao;
import com.tcxf.hbc.trans.mapper.*;
import com.tcxf.hbc.trans.service.ITbRepayLogService;
import com.tcxf.hbc.trans.service.TbTradingDetailService;
import com.tcxf.hbc.trans.tlPay.business.QuickPayPayapplyagreeBusiness;
import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmResData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeResData;
import com.tcxf.hbc.trans.tlPay.util.Util;
import com.tcxf.hbc.trans.util.token.RedisTokenManager;
import com.tcxf.hbc.trans.util.token.TokenModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepayLogServiceImpl extends ServiceImpl<TbRepayLogMapper, TbRepayLog> implements ITbRepayLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PayStatusPropertiesConfig payStatusPropertiesConfig;
    @Autowired
    private TbRepayLogMapper  tbRepayLogMapper;

    @Autowired
    private TbRepayMapper tbRepayMapper;

    @Autowired
    private TbRepaymentDetailMapper tbRepaymentDetailMapper;

    @Autowired
    private TbRepaymentPlanMapper tbRepaymentPlanMapper;

    @Autowired
    private SettlementDao settlementDao;

    @Autowired
    private CUserRefConMapper cUserRefConMapper;

    @Autowired
    private TbAccountInfoMapper tbAccountInfoMapper;

    @Autowired
    private TbTradingDetailMapper tbTradingDetailMapper;

    @Autowired
    private TbWalletMapper tbWalletMapper;

    @Autowired
    private TbWalletIncomeMapper tbWalletIncomeMapper;

    @Autowired
    private CUserInfoMapper cUserInfoMapper;

    @Autowired
    private RedisTokenManager redisTokenManager;

    private String thpinfo;

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:55 2018/8/6
     * 已出账单立即还款
    */
    @Override
    @Transactional
    public PayagreeconfirmDto addinitPayBackInfo(PaybackMoneyDto paybackMoneyDto) {

        //1、验证缓存里面是否存在还款动作
        if(redisTokenManager.checkToken(new TokenModel("tbOutRepay"+paybackMoneyDto.getUid()))){
            TokenModel value = redisTokenManager.getToken("tbOutRepay"+paybackMoneyDto.getUid());
            if(value.getTrIds().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)||value.getTrIds().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                throw new CheckedException("您有正在处理的还款单，请稍后再试");
            }
            throw new CheckedException("您操作过于频繁，请稍后再试");
        }

          List<TbRepayLog> paybackMoneys = paybackMoneyDto.getPaybackMoney(); //分期还款


         String rdid = paybackMoneyDto.getRdid();                            //未分期账单id
         String oid = paybackMoneyDto.getOid();                              //运营商id
         String uid = paybackMoneyDto.getUid();                              //用户id
         String fid = paybackMoneyDto.getFid();                              //资金端id
         String uType = paybackMoneyDto.getuType();                           //用户类型
         BigDecimal totalMoney = new BigDecimal("0");
         BigDecimal unplanTotalMoney = new BigDecimal("0");
         BigDecimal planTotalMoney = new BigDecimal("0");
         BigDecimal dbPlanTotalMoney = new BigDecimal("0");
        if(!"0".equals(rdid) || ( null!=paybackMoneys && paybackMoneys.size()>0) ){
            //计算还款总金额
            if(!"0".equals(rdid)){
                TbRepaymentPlan paramdto = new TbRepaymentPlan();
                paramdto.setRdId(rdid);
                TbRepaymentDetail tbRepaymentDetail = tbRepaymentDetailMapper.selectById(rdid);
                if(tbRepaymentDetail.getStatus().equals("1")){
                    throw new CheckedException("请勿重复还款，请稍后再试");
                }
                unplanTotalMoney = tbRepaymentDetail.getTotalCorpus().add(tbRepaymentDetail.getTotalLateFee());
            }
            if(null!=paybackMoneys && paybackMoneys.size()>0){
                for (TbRepayLog tbRepayLog:paybackMoneys) {
                    TbRepaymentPlan tbRepaymentPlan = tbRepaymentPlanMapper.selectById(tbRepayLog.getPlanId());
                    if(tbRepaymentPlan.getStatus().equals("1")){
                        throw new CheckedException("请勿重复还款，请稍后再试");
                    }
                    planTotalMoney = tbRepayLog.getAmount().add(planTotalMoney);

                    dbPlanTotalMoney= tbRepaymentPlan.getCurrentCorpus().add(tbRepaymentPlan.getCurrentFee()).add(tbRepaymentPlan.getLateFee()).add(dbPlanTotalMoney);

                }
            }

            //校验数据库金额与传输金额
           if(!Calculate.equals(planTotalMoney,dbPlanTotalMoney)){
               throw new CheckedException("非法请求!");
           }

            totalMoney=planTotalMoney.add(unplanTotalMoney);

                //插入主表
                TbRepay tbRepay = new TbRepay();
                tbRepay.setTotalMoney(totalMoney);
                tbRepay.setRepaySn(  "P" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + StringUtil.RadomCode(4, "other").toUpperCase());//生成编号
                tbRepay.setUid(uid);
                tbRepay.setFid(fid);
                tbRepay.setOid(oid);
                tbRepay.setRdid(rdid);
                tbRepay.setPayType("1");      //2:提前还款 1:账单还款
                tbRepay.setCreateDate(new Date()); //添加创建时间
                tbRepayMapper.insert(tbRepay);
                String repayId = tbRepay.getId();

                //repay_log表里面添加明细
                String serialNo ="0";
                if(!"0".equals(rdid)){
                    TbRepaymentPlan paramDto = new TbRepaymentPlan();
                    TbRepaymentDetail paramVo= new TbRepaymentDetail();
                    paramDto.setRdId(rdid);
                    paramVo.setId(rdid);
                    TbRepaymentPlan tbRepaymentPlan = tbRepaymentPlanMapper.selectOne(paramDto);
                    TbRepaymentDetail tbRepaymentDetail = tbRepaymentDetailMapper.selectOne(paramVo);
                    serialNo = tbRepaymentPlan.getSerialNo();
                    TbRepayLog tbRepayLog = new TbRepayLog();
                    tbRepayLog.setAmount(tbRepaymentPlan.getTotalCorpus());
                    tbRepayLog.setPlanId(tbRepaymentPlan.getId());
                    tbRepayLog.setUid(uid);
                    tbRepayLog.setOid(oid);
                    tbRepayLog.setFid(fid);
                    tbRepayLog.setRepayId(repayId);
                    tbRepayLog.setBillDate(DateUtil.getDateTimeforMonth(tbRepaymentPlan.getRepaymentDate()));
                    tbRepayLog.setRepaySn(tbRepaymentDetail.getRepaymentSn());
                    tbRepayLog.setType("1");                                 //2:提前还款 1:账单还款
                    tbRepayLog.setCreateDate(new Date());
                    tbRepayLog.setPaymentType(paybackMoneyDto.getPayWay());//支付类型 1：网银  2：微信
                    tbRepayLog.setUtype(paybackMoneyDto.getuType());       //用户类型 1:消费者 2：商户
                    tbRepayLogMapper.insert(tbRepayLog);
                }
                tbRepay.setSerialNo(serialNo);
                tbRepayMapper.updateById(tbRepay); //更新repay中的serialNo

                //repay_log表里面添加明细
                if(null!=paybackMoneys && paybackMoneys.size()>0){
                    for (TbRepayLog tbRepayLog:paybackMoneys) {
                        tbRepayLog.setRepayId(repayId);                //repay表中主键id
                        tbRepayLog.setOid(oid);
                        tbRepayLog.setFid(fid);
                        tbRepayLog.setUid(uid);
                        tbRepayLog.setCreateDate(new Date());           //添加创建时间
                        tbRepayLog.setPaymentType("1");                 //'1 '网银' '2'  '支付宝''3'  '微信'
                        tbRepayLog.setUtype(uType);
                        tbRepayLogMapper.insert(tbRepayLog);
                    }
                }

                //调用支付申请接口
                String orderid=repayId;
                String agreeid=paybackMoneyDto.getContractId();//银行卡通联协议号
                String subjec="还款";

                String  payMoney = Calculate.multiply_100(new Calculate(totalMoney).format(2)).format(0).toString(); //还款总金额
                if(payStatusPropertiesConfig.getStatus().equals(Configure.PAY_STATUS_0)){
                    payMoney = "1";
                }
                //回调支付接口
                try {
                    QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData =
                            new QuickPayPayapplyagreeReqData(orderid,agreeid
                                    ,payMoney,subjec,"3");
                    new QuickPayPayapplyagreeBusiness().run(quickPayPayapplyagreeReqData, new QuickPayPayapplyagreeBusiness.ResultListener() {
                        @Override
                        public void onQuickPayPayapplyagreeFail(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                            Util.log(QuickPayPayapplyagreeBusiness.result);
                            throw new CheckedException(ValidateUtil.isEmpty(quickPayPayapplyagreeResData.getErrmsg())?"支付失败,请稍后再试":"支付失败，失败原因："+quickPayPayapplyagreeResData.getErrmsg());
                        }
                        @Override
                        public void onQuickPayPayapplyagreeSuccess(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                            Util.log("支付申请成功，得到支付信息："+quickPayPayapplyagreeResData);
                            thpinfo = quickPayPayapplyagreeResData.getThpinfo();
                        }
                    });
                } catch (Exception e) {
                    if(e instanceof CheckedException){
                        throw new CheckedException(e.getMessage());
                    }
                    throw new CheckedException("支付失败,请稍后再试");
                }
                PayagreeconfirmDto payagreeconfirmDto = new PayagreeconfirmDto();
                payagreeconfirmDto.setAgreeid(agreeid);
                payagreeconfirmDto.setOrderid(orderid);
                payagreeconfirmDto.setThpinfo(thpinfo);
                payagreeconfirmDto.setSerialNo(serialNo);
                payagreeconfirmDto.setRdId(rdid);
                redisTokenManager.createToken("tbOutRepay"+paybackMoneyDto.getUid(),"tbRepay");
                return payagreeconfirmDto;
        }
        return null;
    }

    public static void main(String[] args) {
        String  payMoney = Calculate.multiply_100(new Calculate("1265.56").format(2)).format(0).toString();
        System.out.println(payMoney);

    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 15:55 2018/8/6
     * 未出账单提前还款
    */
    @Override
    @Transactional
    public PayagreeconfirmDto advancePaybackMoney(PaybackMoneyDto paybackMoneyDto) {

        //未还款的交易记录
        List<TbTradingDetail> TradingDetails = paybackMoneyDto.getTradingDetails();
        if(ValidateUtil.isEmpty(TradingDetails)||TradingDetails.isEmpty()){
            throw new CheckedException("请勿重复还款，请稍后再试");
        }
        String tids = listToStringSlipStr(TradingDetails, ",");
        //1、验证缓存里面是否存在未完成的交易记录
        if(redisTokenManager.checkToken(new TokenModel("TbRepay"+paybackMoneyDto.getUid()))){
            TokenModel value = redisTokenManager.getToken("TbRepay"+paybackMoneyDto.getUid());
            if(value.getTrIds().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)||value.getTrIds().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)){
                throw new CheckedException("您有正在处理的还款单，请稍后再试");
            }
            throw new CheckedException("您操作过于频繁，请稍后再试");
        }
        //2 验证交易单是否已经被还款
        for(TbTradingDetail tbTradingDetail : TradingDetails){
            TbTradingDetail selectTbTradingDetail  = tbTradingDetailMapper.selectById(tbTradingDetail.getId());
            if(!ValidateUtil.isEmpty(selectTbTradingDetail.getRid())){
                throw new CheckedException("请勿重复还款");
            }
        }
        String oid = paybackMoneyDto.getOid();                              //运营商id
        String uid = paybackMoneyDto.getUid();                              //用户id
        String fid = paybackMoneyDto.getFid();                              //资金端id

        BigDecimal totalMoney = new BigDecimal("0");

        for (TbTradingDetail tbTradingDetail:TradingDetails) {
            totalMoney=tbTradingDetail.getActualAmount().add(totalMoney);
        }
            TbRepay tbRepay = new TbRepay();
            tbRepay.setTotalMoney(totalMoney);
            tbRepay.setRepaySn(  "P" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + StringUtil.RadomCode(4, "other").toUpperCase());//生成编号
            tbRepay.setUid(uid);
            tbRepay.setFid(fid);
            tbRepay.setOid(oid);
            tbRepay.setPayType("2");      //2:提前还款 1:账单还款
            tbRepay.setCreateDate(new Date()); //添加创建时间   2018 8.28
            tbRepayMapper.insert(tbRepay);
            String repayId = tbRepay.getId();
            TbRepayLog tbRepayLog = new TbRepayLog();
            tbRepayLog.setAmount(totalMoney);//
            tbRepayLog.setUid(uid);
            tbRepayLog.setOid(oid);
            tbRepayLog.setFid(fid);
            tbRepayLog.setTids(tids);                               //交易记录id
            tbRepayLog.setRepayId(repayId);
            tbRepayLog.setType("2");                                //2:提前还款 1:账单还款
            tbRepayLog.setCreateDate(new Date());
            tbRepayLog.setPaymentType(paybackMoneyDto.getPayWay());//支付类型 1：网银  2：微信
            tbRepayLog.setUtype(paybackMoneyDto.getuType());       //用户类型 1:消费者 2：商户
            tbRepayLogMapper.insert(tbRepayLog);
            //调用支付申请接口
            String orderid=repayId;
            String agreeid=paybackMoneyDto.getContractId();//银行卡通联协议号
            String subjec="还款";

            String  payMoney = Calculate.multiply_100(new Calculate(totalMoney).format(2)).format(0).toString();//还款总金额
            if(payStatusPropertiesConfig.getStatus().equals(Configure.PAY_STATUS_0)){
                payMoney = "1";
            }

        try {
            QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData =
                    new QuickPayPayapplyagreeReqData(orderid,agreeid
                            ,payMoney,subjec,"2");
            new QuickPayPayapplyagreeBusiness().run(quickPayPayapplyagreeReqData, new QuickPayPayapplyagreeBusiness.ResultListener() {
                @Override
                public void onQuickPayPayapplyagreeFail(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                    Util.log(QuickPayPayapplyagreeBusiness.result);
                    throw new CheckedException(ValidateUtil.isEmpty(quickPayPayapplyagreeResData.getErrmsg())?"支付失败,请稍后再试":"支付失败，失败原因："+quickPayPayapplyagreeResData.getErrmsg());
                }
                @Override
                public void onQuickPayPayapplyagreeSuccess(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
                    Util.log("支付申请成功，得到支付信息："+quickPayPayapplyagreeResData);
                    thpinfo = quickPayPayapplyagreeResData.getThpinfo();
                }
            });
        } catch (Exception ex) {
            if(ex instanceof CheckedException){
                throw new CheckedException(ex.getMessage());
            }
            throw new CheckedException("支付失败,请稍后再试");
        }
        PayagreeconfirmDto payagreeconfirmDto = new PayagreeconfirmDto();
        payagreeconfirmDto.setAgreeid(agreeid);
        payagreeconfirmDto.setOrderid(orderid);
        payagreeconfirmDto.setThpinfo(thpinfo);
        redisTokenManager.createToken("TbRepay"+paybackMoneyDto.getUid(),"tbRepay"); //2、每次申请还款支付的时候，将交易记录ID 放入缓存锁定，不允许重复操作 时间过期30秒
        return payagreeconfirmDto;
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 9:13 2018/8/11
     * 提前还款成功更新记录
    */
    @Override
    @Transactional
    public void updateTradingInfo(PayagreeconfirmDto payagreeconfirmDto) {

        String repayId = payagreeconfirmDto.getOrderid();
        String uid = payagreeconfirmDto.getUid();
        String fid = payagreeconfirmDto.getFid();
        String oid = payagreeconfirmDto.getOid();
        String uType = payagreeconfirmDto.getUtype();
        //回调成功
        //1 ：修改主表数据
        try {
            TbRepay tbRepay = new TbRepay();
            tbRepay.setPaymentSn(payagreeconfirmDto.getTrxid());                      //还款成功返回支付编号
            tbRepay.setPayDate(new Date());                         //支付时间
            tbRepay.setId(repayId);
            tbRepay.setStatus("1");                                 //支付状态 1:成功 0：待支付
            tbRepayMapper.updateById(tbRepay);

            logger.info("更新repay表成功");

            //2: 还款记录表数据
            TbRepayLog updateTbRepayLog = new TbRepayLog();
            updateTbRepayLog.setPayDate(new Date());                        //支付时间
            updateTbRepayLog.setStatus("1");                               //1:成功 0:待支付
            tbRepayLogMapper.update(updateTbRepayLog,new EntityWrapper<TbRepayLog>().eq("repay_id",repayId));

            logger.info("更新repay_log表成功");

            //更新交易记录的提前还款id
            TbRepayLog findTbRepayLog = new TbRepayLog();
            findTbRepayLog.setRepayId(repayId);
            TbRepayLog tbRepayLog = tbRepayLogMapper.selectOne(findTbRepayLog);
            String tids = tbRepayLog.getTids();//交易记录id
            String rid = tbRepayLog.getId();
            BigDecimal totalMoney = tbRepayLog.getAmount();

            String[] split = tids.split(",");
            if(split.length>0){
                for (int i = 0; i <split.length ; i++) {
                    TbTradingDetail tbTradingDetail = new TbTradingDetail();
                    tbTradingDetail.setRid(rid);
                    tbTradingDetailMapper.update(tbTradingDetail,new EntityWrapper<TbTradingDetail>().eq("id",split[i]));
                }
            }

            logger.info("更新trade表成功");

            //4：修改账户表里面的金额
            TbAccountInfo tbAccountMoneyInfo = new TbAccountInfo();
            tbAccountMoneyInfo.setOid(uid);
            TbAccountInfo moneyInfo = tbAccountInfoMapper.selectOne(tbAccountMoneyInfo);
            String dbBalanceMoney =DES.decryptdf(moneyInfo.getBalance());
            BigDecimal balanceMoney = new BigDecimal(dbBalanceMoney).add(totalMoney);

            tbAccountMoneyInfo.setBalance(DES.encryptdf(balanceMoney.toString()));
            tbAccountMoneyInfo.setModifyDate(new Date());
            tbAccountInfoMapper.update(tbAccountMoneyInfo,new EntityWrapper<TbAccountInfo>().eq("oid",uid));

            logger.info("更新TbAccountInfo表成功");

            //7.修改资金端钱包的金额
            TbWallet tbWallet = new TbWallet();
            tbWallet.setUid(fid);
            TbWallet dbWallet = tbWalletMapper.selectOne(tbWallet);   //查询资金端的钱包
            String wid = dbWallet.getId();                            //钱包id
            BigDecimal totalAmount = dbWallet.getTotalAmount();
            dbWallet.setTotalAmount(totalAmount.add(totalMoney));
            dbWallet.setModifyDate(new Date());
            tbWalletMapper.updateById(dbWallet);                      //修改资金端的钱包总金额

            logger.info("更新TbWallet表成功");

            //8.tb_wallet_income添加记录
            CUserInfo cUserInfo = cUserInfoMapper.selectById(uid);
            TbWalletIncome tbWalletIncome = new TbWalletIncome();
            tbWalletIncome.setWid(wid);
            tbWalletIncome.setPlayid(uid);                             //资金来源用户id
            tbWalletIncome.setPtepy(uType);                            //用户类型
            tbWalletIncome.setType("1");                               //收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出 8 平台抽成费扣取（只针对运营商钱包
            tbWalletIncome.setPname(cUserInfo.getRealName());
            tbWalletIncome.setPmobile(cUserInfo.getMobile());
            tbWalletIncome.setTradeTotalAmount(totalMoney);             //交易总金额
            tbWalletIncome.setAmount(totalMoney);                       //出、入金总额
            tbWalletIncome.setCreateDate(new Date());
            tbWalletIncome.setOid(oid);                                 //运营商id
            tbWalletIncome.setSettlementStatus("1");                    //结算状态 0:未结算 1:已结算
            tbWalletIncomeMapper.insert(tbWalletIncome);
            tbWalletIncome.setTid(repayId);                             //还款tid为repay表中的主键id

            logger.info("添加tb_wallet_income表成功");

        } catch (Exception e) {
            logger.error("还款更新记录失败",e);
            throw new RuntimeException("更新记录失败");
        }
    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:37 2018/8/11
     * 正常还款中回调修改记录
    */
    @Override
    @Transactional
    public void  updateNormalPay(PayagreeconfirmDto payagreeconfirmDto) {
        String repayId = payagreeconfirmDto.getOrderid();
        String uid = payagreeconfirmDto.getUid();
        String fid = payagreeconfirmDto.getFid();
        String oid = payagreeconfirmDto.getOid();
        String uType = payagreeconfirmDto.getUtype();
        String rdId=payagreeconfirmDto.getRdId();
        String serialNo=payagreeconfirmDto.getSerialNo();
        //回调成功
        //1 ：修改主表数据

        try {
            TbRepay tbRepay = new TbRepay();
            tbRepay.setPaymentSn(payagreeconfirmDto.getTrxid());                      //还款成功返回支付编号
            tbRepay.setPayDate(new Date());                         //支付时间
            tbRepay.setStatus("1");                                 //支付状态 1:成功 0：待支付
            tbRepay.setId(repayId);
            tbRepayMapper.updateById(tbRepay);

            logger.info("更新repay表成功");

            //1 查询tbReypay表中的数据
            TbRepay findTbRepay = new TbRepay();
            findTbRepay.setId(repayId);
            TbRepay dbTbRepay = tbRepayMapper.selectOne(findTbRepay);
            BigDecimal totalMoney = dbTbRepay.getTotalMoney();

            // 查询tbReypay_log表中的数据
            List<TbRepayLog> paybackMoneys = tbRepayLogMapper.selectList(new EntityWrapper<TbRepayLog>().eq("repay_id", repayId));


            //查询每笔分期/未分期的本金累加
            BigDecimal sxTotalMoney = new BigDecimal("0");
            if (paybackMoneys!=null && paybackMoneys.size()>0){
                for (TbRepayLog tbRepayLog:paybackMoneys) {
                    TbRepaymentPlan tbRepaymentPlan = tbRepaymentPlanMapper.selectById(tbRepayLog.getPlanId());
                    sxTotalMoney=sxTotalMoney.add(tbRepaymentPlan.getCurrentCorpus());
                }
            }

            //2: 还款记录表数据
            TbRepayLog tbRepayLog = new TbRepayLog();
            tbRepayLog.setPayDate(new Date());                        //支付时间
            tbRepayLog.setStatus("1");                                //1:成功 0:待支付
            tbRepayLogMapper.update(tbRepayLog,new EntityWrapper<TbRepayLog>().eq("repay_id",repayId));

            logger.info("更新repay_log表成功");

            //3：CUserRefCon 修改授信状态
            CUserRefCon cUserRefCon = new CUserRefCon();
            cUserRefCon.setAuthStatus("1");                           //授信状态（0-未授信 1-已授信 2-已冻结）
            cUserRefConMapper.update(cUserRefCon,new EntityWrapper<CUserRefCon>().eq("uid",uid).eq("oid",oid));

            logger.info("更新CUserRefCon授信状态成功");

            //4：tb_account_info 修改授信状态
            TbAccountInfo tbAccountInfo = new TbAccountInfo();
            tbAccountInfo.setIsFreeze("0");                          //授信是否冻结 0:未冻结 1:冻结
            tbAccountInfoMapper.update(tbAccountInfo,new EntityWrapper<TbAccountInfo>().eq("oid",uid)); //资源主键oid（user_info表中主键id）

            logger.info("更新tb_account_info授信状态成功");

            //5 ：调用接口修改还款表 以及 还款计划表数据
            settlementDao.updateUserRepaymentList(paybackMoneys,rdId,"1",serialNo);

            logger.info("更新repayment_detail以及repayment_plan数据成功");

            //6：修改账户表里面的金额
            TbAccountInfo tbAccountMoneyInfo = new TbAccountInfo();
            tbAccountMoneyInfo.setOid(uid);
            TbAccountInfo moneyInfo = tbAccountInfoMapper.selectOne(tbAccountMoneyInfo);
            String dbBalanceMoney =DES.decryptdf(moneyInfo.getBalance());
            BigDecimal balanceMoney = new BigDecimal(dbBalanceMoney).add(sxTotalMoney);     //添加的是本金不含利息以及逾期金额
            tbAccountMoneyInfo.setBalance(DES.encryptdf(balanceMoney.toString()));
            tbAccountInfoMapper.update(tbAccountMoneyInfo,new EntityWrapper<TbAccountInfo>().eq("oid",uid));

            logger.info("更新TbAccountInfo表成功");

            //7.修改资金端钱包的金额
            TbWallet tbWallet = new TbWallet();
            tbWallet.setUid(fid);
            TbWallet dbWallet = tbWalletMapper.selectOne(tbWallet);   //查询资金端的钱包
            String wid = dbWallet.getId();                            //钱包id
            BigDecimal totalAmount = dbWallet.getTotalAmount();
            dbWallet.setTotalAmount(totalAmount.add(totalMoney));
            tbWalletMapper.updateById(dbWallet);                      //修改资金端的钱包总金额

            logger.info("更新TbWallet表成功");

            //8.tb_wallet_income添加记录
            CUserInfo cUserInfo = cUserInfoMapper.selectById(uid);
            TbWalletIncome tbWalletIncome = new TbWalletIncome();
            tbWalletIncome.setWid(wid);
            tbWalletIncome.setPlayid(uid);                             //资金来源用户id
            tbWalletIncome.setPtepy(uType);       //用户类型
            tbWalletIncome.setType("1");                               //收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出 8 平台抽成费扣取（只针对运营商钱包
            tbWalletIncome.setPname(cUserInfo.getRealName());
            tbWalletIncome.setPmobile(cUserInfo.getMobile());
            tbWalletIncome.setTradeTotalAmount(totalMoney);             //交易总金额
            tbWalletIncome.setAmount(totalMoney);                       //出、入金总额
            tbWalletIncome.setCreateDate(new Date());
            tbWalletIncome.setOid(oid);                                 //运营商id
            tbWalletIncome.setSettlementStatus("1");                    //结算状态 0:未结算 1:已结算
            tbWalletIncome.setTid(repayId);                             //还款tid为repay表中的主键id
            tbWalletIncomeMapper.insert(tbWalletIncome);

            logger.info("添加tb_wallet_income表成功");
        } catch (Exception e) {
            logger.error("还款更新记录失败",e);
            throw new RuntimeException("还款失败!");
        }

    }

    /**
    * @Author:YWT_tai
    * @Description
    * @Date: 11:37 2018/8/11
     * list转String
    */
    public static String listToStringSlipStr( List<TbTradingDetail> list, String slipStr) {
        StringBuffer returnStr = new StringBuffer();
        if (list != null && list.size() > 0) {
            for (TbTradingDetail tbTradingDetail: list) {
                returnStr.append(tbTradingDetail.getId()).append(slipStr);
            }
        }
        if (returnStr.toString().length() > 0){
            return returnStr.toString().substring(0,
                    returnStr.toString().lastIndexOf(slipStr));
        }
        else{
            return "";
        }
    }


}
