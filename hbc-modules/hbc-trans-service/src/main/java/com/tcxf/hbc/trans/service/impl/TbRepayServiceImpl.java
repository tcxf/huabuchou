package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepay;
import com.tcxf.hbc.trans.mapper.TbRepayMapper;
import com.tcxf.hbc.trans.service.ITbRepayService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-07-31
 */
@Service
public class TbRepayServiceImpl extends ServiceImpl<TbRepayMapper, TbRepay> implements ITbRepayService {

}
