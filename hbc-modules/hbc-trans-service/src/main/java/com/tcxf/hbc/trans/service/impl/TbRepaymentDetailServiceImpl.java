package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.*;
import com.tcxf.hbc.trans.mapper.TbRepaymentDetailMapper;
import com.tcxf.hbc.trans.service.ITbRepaymentDetailService;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 还款表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepaymentDetailServiceImpl extends ServiceImpl<TbRepaymentDetailMapper, TbRepaymentDetail> implements ITbRepaymentDetailService {

}
