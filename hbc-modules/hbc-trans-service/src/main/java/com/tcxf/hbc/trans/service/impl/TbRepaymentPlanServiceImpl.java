package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbRepaymentPlan;
import com.tcxf.hbc.trans.mapper.TbRepaymentPlanMapper;
import com.tcxf.hbc.trans.service.ITbRepaymentPlanService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款计划表 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbRepaymentPlanServiceImpl extends ServiceImpl<TbRepaymentPlanMapper, TbRepaymentPlan> implements ITbRepaymentPlanService {


}
