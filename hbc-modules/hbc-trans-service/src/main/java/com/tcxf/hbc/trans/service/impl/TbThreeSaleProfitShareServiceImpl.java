package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbThreeSaleProfitShare;
import com.tcxf.hbc.trans.mapper.TbThreeSaleProfitShareMapper;
import com.tcxf.hbc.trans.service.ITbThreeSaleProfitShareService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 三级分销利润分配表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbThreeSaleProfitShareServiceImpl extends ServiceImpl<TbThreeSaleProfitShareMapper, TbThreeSaleProfitShare> implements ITbThreeSaleProfitShareService {

}
