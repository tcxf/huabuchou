package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbThreeSalesRate;
import com.tcxf.hbc.trans.mapper.TbThreeSalesRateMapper;
import com.tcxf.hbc.trans.service.TbThreeSalesRateService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/10 17:55
 * @Description:
 */
@Service
public class TbThreeSalesRateServiceImpl extends ServiceImpl<TbThreeSalesRateMapper,TbThreeSalesRate> implements TbThreeSalesRateService {

}
