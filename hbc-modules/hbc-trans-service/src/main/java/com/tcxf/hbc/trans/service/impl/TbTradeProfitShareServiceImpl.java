package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbTradeProfitShare;
import com.tcxf.hbc.trans.mapper.TbTradeProfitShareMapper;
import com.tcxf.hbc.trans.service.ITbTradeProfitShareService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易利润分配表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbTradeProfitShareServiceImpl extends ServiceImpl<TbTradeProfitShareMapper, TbTradeProfitShare> implements ITbTradeProfitShareService {

}
