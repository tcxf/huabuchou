package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbTradingDetail;
import com.tcxf.hbc.trans.mapper.TbTradingDetailMapper;
import com.tcxf.hbc.trans.service.TbTradingDetailService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liuxu
 * @Date: 2018/7/10 14:36
 * @Description:
 */
@Service
public class TbTradingDetailServiceImpl extends ServiceImpl<TbTradingDetailMapper,TbTradingDetail> implements TbTradingDetailService {

}
