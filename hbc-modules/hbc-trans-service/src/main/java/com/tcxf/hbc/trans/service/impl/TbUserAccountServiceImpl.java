package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbUserAccount;
import com.tcxf.hbc.trans.mapper.TbUserAccountMapper;
import com.tcxf.hbc.trans.service.ITbUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户表（包含消费者、商户） 服务实现类
 * </p>
 *
 * @author zhouyinjun
 * @since 2018-05-29
 */
@Service
public class TbUserAccountServiceImpl extends ServiceImpl<TbUserAccountMapper, TbUserAccount> implements ITbUserAccountService {

    @Autowired
    TbUserAccountMapper tbUserAccountMapper;


    @Override
    public TbUserAccount selectUserInfoByUserId(String userId) {
        return tbUserAccountMapper.selectUserInfoByUserId(userId);

    }
}
