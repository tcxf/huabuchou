package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWalletIncome;
import com.tcxf.hbc.trans.mapper.TbWalletIncomeMapper;
import com.tcxf.hbc.trans.service.ITbWalletIncomeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金钱包收支明细表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbWalletIncomeServiceImpl extends ServiceImpl<TbWalletIncomeMapper, TbWalletIncome> implements ITbWalletIncomeService{

}
