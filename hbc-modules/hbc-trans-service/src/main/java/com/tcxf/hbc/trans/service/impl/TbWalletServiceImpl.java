package com.tcxf.hbc.trans.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tcxf.hbc.common.entity.TbWallet;
import com.tcxf.hbc.trans.mapper.TbThreeSalesRefMapper;
import com.tcxf.hbc.trans.mapper.TbWalletMapper;
import com.tcxf.hbc.trans.service.ITbWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资金钱包表 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2018-05-30
 */
@Service
public class TbWalletServiceImpl extends ServiceImpl<TbWalletMapper, TbWallet> implements ITbWalletService {
    @Autowired
    TbWalletMapper tbWalletMapper;

    @Override
    public TbWallet selectUserTbWallet(String userId) {
        return tbWalletMapper.selectUserTbWallet(userId);
    }

    @Override
    public TbWallet selectopTbWallet(String oId) {
        return tbWalletMapper.selectopTbWallet(oId);
    }
}
