package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.Calculate;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.BaseQuickPayReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeapplyReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeapplyResData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqJsData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.service.QuickPayAgreeapplyService;
import com.tcxf.hbc.trans.tlPay.service.UnifiedorderService;
import com.tcxf.hbc.trans.tlPay.util.Util;

import java.math.RoundingMode;


/**
 * 签约申请业务处理器
 * @author liuxu
 * @creation 2018年8月6日
 */
public class QuickPayAgreeapplyBusiness {

	private QuickPayAgreeapplyService quickPayAgreeapplyService;

	/**
	 * 执行结果
	 */
	public static String result;


	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 签约申请失败
		 *
		 * @param quickPayAgreeapplyResData
		 */
		public void onQuickPayAgreeapplyFail(QuickPayAgreeapplyResData quickPayAgreeapplyResData);

		/**
		 * 签约申请成功
		 *
		 * @param quickPayAgreeapplyResData
		 */
		public void onQuickPayAgreeapplySuccess(QuickPayAgreeapplyResData quickPayAgreeapplyResData);

	}

	public static void main(String[] args) {
		String meruserid ="测试绑定";
		String acctno ="6230200550966092";
		String idno ="433025199110106625";
		String acctname="田方";
		String mobile="13918410871";
		try {
			QuickPayAgreeapplyReqData quickPayAgreeapplyReqData =new QuickPayAgreeapplyReqData(meruserid,acctno,idno,acctname,mobile);
			new QuickPayAgreeapplyBusiness().run(quickPayAgreeapplyReqData, new QuickPayAgreeapplyBusiness.ResultListener() {
				@Override
				public void onQuickPayAgreeapplyFail(QuickPayAgreeapplyResData quickPayAgreeapplyResData) {
					Util.log(QuickPayAgreeapplyBusiness.result);
				}
				@Override
				public void onQuickPayAgreeapplySuccess(QuickPayAgreeapplyResData quickPayAgreeapplyResData) {
					Util.log(QuickPayAgreeapplyBusiness.result);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public QuickPayAgreeapplyBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		quickPayAgreeapplyService = new QuickPayAgreeapplyService();
    }
    
    /**
     * 直接执行签约申请业务逻辑
     * @param quickPayAgreeapplyReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(QuickPayAgreeapplyReqData quickPayAgreeapplyReqData, ResultListener resultListener) throws Exception {
    	String quickPayAgreeapplyResDataString = quickPayAgreeapplyService.request(quickPayAgreeapplyReqData);
		Util.log("申请签约响应参数："+quickPayAgreeapplyResDataString);
		QuickPayAgreeapplyResData quickPayAgreeapplyResData = JsonTools.parseObject(quickPayAgreeapplyResDataString,QuickPayAgreeapplyResData.class);
		if (ValidateUtil.isEmpty(quickPayAgreeapplyResData)|| ValidateUtil.isEmpty(quickPayAgreeapplyResData.getRetcode())) {
			String str ="申请签约失败，API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问";
			setResult(str);
			resultListener.onQuickPayAgreeapplyFail(quickPayAgreeapplyResData);
			return;
		}
		if (quickPayAgreeapplyResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			String str ="申请签约失败,失败原因:"+quickPayAgreeapplyResData.getRetmsg()+",请检测Post给API的数据是否规范合法";
			setResult(str);
			resultListener.onQuickPayAgreeapplyFail(quickPayAgreeapplyResData);
			return;
		}
		if (quickPayAgreeapplyResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				!quickPayAgreeapplyResData.getTrxstatus().equals(QuickPayAgreeapplyResData.TRXSTATUS_1999)) {
			String str="申请签约失败,失败原因：" + quickPayAgreeapplyResData.getErrmsg();
			setResult(str);
			resultListener.onQuickPayAgreeapplyFail(quickPayAgreeapplyResData);
			return;
		}
		if (quickPayAgreeapplyResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				quickPayAgreeapplyResData.getTrxstatus().equals(QuickPayAgreeapplyResData.TRXSTATUS_1999)) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(quickPayAgreeapplyResDataString)){
				setResult("申请签约失败,失败原因:签名认证不通过");
				resultListener.onQuickPayAgreeapplyFail(quickPayAgreeapplyResData);
				return;
			}
			setResult("申请签约成功");
			resultListener.onQuickPayAgreeapplySuccess(quickPayAgreeapplyResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		QuickPayAgreeapplyBusiness.result = result;
	}
    
}
