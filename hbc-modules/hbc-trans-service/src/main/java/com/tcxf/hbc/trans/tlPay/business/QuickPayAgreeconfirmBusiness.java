package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.BaseQuickPayReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeconfirmReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeconfirmResData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.service.QuickPayAgreeconfirmService;
import com.tcxf.hbc.trans.tlPay.util.Util;


/**
 * 签约确认业务处理器
 * @author liuxu
 * @creation 2018年8月6日
 */
public class QuickPayAgreeconfirmBusiness {

	private QuickPayAgreeconfirmService quickPayAgreeconfirmService;

	/**
	 * 执行结果
	 */
	public static String result;


	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 签约申请失败
		 *
		 * @param quickPayAgreeconfirmResData
		 */
		public void onQuickPayAgreeconfirmFail(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData);

		/**
		 * 签约申请成功
		 *
		 * @param quickPayAgreeconfirmResData
		 */
		public void onQuickPayAgreeconfirmSuccess(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData);

	}

	public static void main(String[] args) {
		String meruserid ="测试绑定";
		String acctno ="6230200550966092";
		String idno ="433025199110106625";
		String acctname="田方";
		String mobile="13918410871";
		String smscode="242591";
		try {
			QuickPayAgreeconfirmReqData quickPayAgreeconfirmReqData =new QuickPayAgreeconfirmReqData(meruserid,acctno,idno,acctname,mobile,smscode);
			new QuickPayAgreeconfirmBusiness().run(quickPayAgreeconfirmReqData, new QuickPayAgreeconfirmBusiness.ResultListener() {
				@Override
				public void onQuickPayAgreeconfirmFail(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData) {
					Util.log(QuickPayAgreeconfirmBusiness.result);
				}
				@Override
				public void onQuickPayAgreeconfirmSuccess(QuickPayAgreeconfirmResData quickPayAgreeconfirmResData) {
					Util.log(QuickPayAgreeconfirmBusiness.result);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public QuickPayAgreeconfirmBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		quickPayAgreeconfirmService = new QuickPayAgreeconfirmService();
    }
    
    /**
     * 直接执行签约确认业务逻辑
     * @param quickPayAgreeconfirmReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(QuickPayAgreeconfirmReqData quickPayAgreeconfirmReqData, ResultListener resultListener) throws Exception {
    	String quickPayAgreeapplyResDataString = quickPayAgreeconfirmService.request(quickPayAgreeconfirmReqData);
		Util.log("签约确认响应参数："+quickPayAgreeapplyResDataString);
		QuickPayAgreeconfirmResData quickPayAgreeconfirmResData = JsonTools.parseObject(quickPayAgreeapplyResDataString,QuickPayAgreeconfirmResData.class);
		if (ValidateUtil.isEmpty(quickPayAgreeconfirmResData)|| ValidateUtil.isEmpty(quickPayAgreeconfirmResData.getRetcode())) {
			setResult("签约确认，API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问");
			resultListener.onQuickPayAgreeconfirmFail(quickPayAgreeconfirmResData);
			return;
		}
		if (quickPayAgreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			setResult("签约确认失败，失败原因:"+quickPayAgreeconfirmResData.getRetmsg()+",请检测Post给API的数据是否规范合法");
			resultListener.onQuickPayAgreeconfirmFail(quickPayAgreeconfirmResData);
			return;
		}
		if (quickPayAgreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				!quickPayAgreeconfirmResData.getTrxstatus().equals(QuickPayAgreeconfirmResData.TRXSTATUS_0000)) {
			setResult("签约确认失败,失败原因：" + quickPayAgreeconfirmResData.getErrmsg());
			resultListener.onQuickPayAgreeconfirmFail(quickPayAgreeconfirmResData);
			return;
		}
		if (quickPayAgreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				quickPayAgreeconfirmResData.getTrxstatus().equals(QuickPayAgreeconfirmResData.TRXSTATUS_0000)) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(quickPayAgreeapplyResDataString)){
				setResult("签约确认失败,失败原因:签名认证不通过");
				resultListener.onQuickPayAgreeconfirmFail(quickPayAgreeconfirmResData);
				return;
			}
			setResult("签约确认成功");
			resultListener.onQuickPayAgreeconfirmSuccess(quickPayAgreeconfirmResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		QuickPayAgreeconfirmBusiness.result = result;
	}
    
}
