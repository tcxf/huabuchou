package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.BaseQuickPayReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeResData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqJsData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.service.QuickPayPayapplyagreeService;
import com.tcxf.hbc.trans.tlPay.service.UnifiedorderService;
import com.tcxf.hbc.trans.tlPay.util.Util;


/**
 * 快捷支付申请业务处理器
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayPayapplyagreeBusiness {

	private QuickPayPayapplyagreeService quickPayPayapplyagreeService;

	/**
	 * 执行结果
	 */
	public static String result;


	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 快捷支付申请失败
		 *
		 * @param quickPayPayapplyagreeResData
		 */
		public void onQuickPayPayapplyagreeFail(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData);

		/**
		 * 快捷支付申请成功
		 *
		 * @param quickPayPayapplyagreeResData
		 */
		public void onQuickPayPayapplyagreeSuccess(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData);

	}

	public static void main(String[] args) {
        String amount="1";
        String orderid=RandomStringGenerator.getRandomStringByLength(12);
        String agreeid="201808281400306258";
        String subject ="测试快捷支付";
        try {
			QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData =
					new QuickPayPayapplyagreeReqData(orderid,agreeid,amount,subject,Configure.NOTIFY_URL_TYPE);
			new QuickPayPayapplyagreeBusiness().run(quickPayPayapplyagreeReqData, new QuickPayPayapplyagreeBusiness.ResultListener() {
				@Override
				public void onQuickPayPayapplyagreeFail(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
					Util.log(QuickPayPayapplyagreeBusiness.result);
					throw new CheckedException("支付失败");
				}
				@Override
				public void onQuickPayPayapplyagreeSuccess(QuickPayPayapplyagreeResData quickPayPayapplyagreeResData) {
					Util.log("预支付成功，得到支付信息："+quickPayPayapplyagreeResData);
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    public QuickPayPayapplyagreeBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		quickPayPayapplyagreeService = new QuickPayPayapplyagreeService();
    }
    
    /**
     * 直接执行快捷支付申请业务逻辑
     * @param quickPayPayapplyagreeReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData, ResultListener resultListener) throws Exception {
    	String quickPayPayapplyagreeResDataString = quickPayPayapplyagreeService.request(quickPayPayapplyagreeReqData);
		Util.log("支付申请响应参数："+quickPayPayapplyagreeResDataString);
		QuickPayPayapplyagreeResData quickPayPayapplyagreeResData = JsonTools.parseObject(quickPayPayapplyagreeResDataString,QuickPayPayapplyagreeResData.class);
    	if (ValidateUtil.isEmpty(quickPayPayapplyagreeResData)|| ValidateUtil.isEmpty(quickPayPayapplyagreeResData.getRetcode())) {
			setResult("快捷支付申请失败,API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问");
			resultListener.onQuickPayPayapplyagreeFail(quickPayPayapplyagreeResData);
			return;
		}
		if (quickPayPayapplyagreeResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			setResult("快捷支付申请失败，失败原因:"+quickPayPayapplyagreeResData.getRetmsg()+",请检测Post给API的数据是否规范合法");
			resultListener.onQuickPayPayapplyagreeFail(quickPayPayapplyagreeResData);
			return;
		} 
		if (!quickPayPayapplyagreeResData.getTrxstatus().equals(QuickPayPayapplyagreeResData.TRXSTATUS_0000)
				&&!quickPayPayapplyagreeResData.getTrxstatus().equals(QuickPayPayapplyagreeResData.TRXSTATUS_1999)) {
			setResult("快捷支付申请失败,失败原因：" + quickPayPayapplyagreeResData.getErrmsg());
			resultListener.onQuickPayPayapplyagreeFail(quickPayPayapplyagreeResData);
			return;
		}

		if (quickPayPayapplyagreeResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				(quickPayPayapplyagreeResData.getTrxstatus().equals(QuickPayPayapplyagreeResData.TRXSTATUS_0000)||
						quickPayPayapplyagreeResData.getTrxstatus().equals(QuickPayPayapplyagreeResData.TRXSTATUS_1999))) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(quickPayPayapplyagreeResDataString)){
				setResult("快捷支付申请失败,失败原因:签名认证不通过");
				resultListener.onQuickPayPayapplyagreeFail(quickPayPayapplyagreeResData);
				return;
			}
			setResult("快捷支付申请成功");
			resultListener.onQuickPayPayapplyagreeSuccess(quickPayPayapplyagreeResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		QuickPayPayapplyagreeBusiness.result = result;
	}
    
}
