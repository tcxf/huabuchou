package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.*;
import com.tcxf.hbc.trans.tlPay.service.QuickPayPayagreeconfirmService;
import com.tcxf.hbc.trans.tlPay.service.QuickPayQueryService;
import com.tcxf.hbc.trans.tlPay.util.Util;


/**
 * 快捷支付交易查询处理器
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuikPayPayQueryBusiness {

	private QuickPayQueryService quickPayQueryService;

	/**
	 * 执行结果
	 */
	public static String result;


	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 快捷支付交易查询失败
		 *
		 * @param quickPayQueryResData
		 */
		public void onQuickPayQueryResDataFail(QuickPayQueryResData quickPayQueryResData);

		/**
		 * 快捷支付交易查询成功
		 *
		 * @param quickPayQueryResData
		 */
		public void onQuickPayQueryResDataSuccess(QuickPayQueryResData quickPayQueryResData);

	}

	public static void main(String[] args) {
        try {
        	String orderId = "1033296549238059009";
        	String trxid = "";
			QuickPayQueryReqData quickPayPayagreeconfirmReqData = new QuickPayQueryReqData(orderId,trxid);
			new QuikPayPayQueryBusiness().run(quickPayPayagreeconfirmReqData, new ResultListener() {
				@Override
				public void onQuickPayQueryResDataFail(QuickPayQueryResData quickPayQueryResData) {
					Util.log(QuikPayPayQueryBusiness.result);
					throw new CheckedException("快捷支付交易查询失败");
				}
				@Override
				public void onQuickPayQueryResDataSuccess(QuickPayQueryResData quickPayQueryResData) {
					Util.log("快捷支付交易查询成功，得到查询信息："+quickPayQueryResData);
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    public QuikPayPayQueryBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		quickPayQueryService = new QuickPayQueryService();
    }
    
    /**
     * 直接执行快捷支付确认业务逻辑
     * @param quickPayQueryReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(QuickPayQueryReqData quickPayQueryReqData, ResultListener resultListener) throws Exception {
    	String qquickPayQueryReqDataString = quickPayQueryService.request(quickPayQueryReqData);
		Util.log("快捷支付交易查询响应参数："+qquickPayQueryReqDataString);
		QuickPayQueryResData quickPayQueryResData = JsonTools.parseObject(qquickPayQueryReqDataString,QuickPayQueryResData.class);
    	if (ValidateUtil.isEmpty(quickPayQueryResData)|| ValidateUtil.isEmpty(quickPayQueryResData.getRetcode())) {
			setResult("快捷支付交易查询失败，API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问");
			resultListener.onQuickPayQueryResDataFail(quickPayQueryResData);
			return;
		}
		if (quickPayQueryResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			setResult("快捷支付交易查询失败，失败原因:"+quickPayQueryResData.getRetmsg()+",请检测Post给API的数据是否规范合法");
			resultListener.onQuickPayQueryResDataFail(quickPayQueryResData);
			return;
		}

		if (quickPayQueryResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(qquickPayQueryReqDataString)){
				setResult("快捷支付交易查询失败,失败原因:签名认证不通过");
				resultListener.onQuickPayQueryResDataFail(quickPayQueryResData);
				return;
			}
			setResult("快捷支付交易查询成功");
			resultListener.onQuickPayQueryResDataSuccess(quickPayQueryResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		QuikPayPayQueryBusiness.result = result;
	}
    
}
