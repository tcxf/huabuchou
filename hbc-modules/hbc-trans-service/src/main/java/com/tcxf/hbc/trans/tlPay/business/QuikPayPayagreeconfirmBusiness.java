package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.StringUtil;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.BaseQuickPayReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmResData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeResData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.service.QuickPayPayagreeconfirmService;
import com.tcxf.hbc.trans.tlPay.util.Util;


/**
 * 快捷支付确认业务处理器
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuikPayPayagreeconfirmBusiness {

	private QuickPayPayagreeconfirmService quickPayPayagreeconfirmService;

	/**
	 * 执行结果
	 */
	public static String result;


	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 快捷支付确认失败
		 *
		 * @param quickPayPayagreeconfirmResData
		 */
		public void onQuickPayPayagreeconfirmFail(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData);

		/**
		 * 快捷支付确认成功
		 *
		 * @param quickPayPayagreeconfirmResData
		 */
		public void onQuickPayPayagreeconfirmSuccess(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData);

	}

	public static void main(String[] args) {
		String orderid = "keqbm6cm1sv2";
		String agreeid ="201808281400306258";
		String smscode="735843";
		String thpinfo ="{\"sign\":\"\",\"tphtrxcrtime\":\"\",\"tphtrxid\":0,\"trxflag\":\"trx\",\"trxsn\":\"\"}";
        try {
			QuickPayPayagreeconfirmReqData quickPayPayagreeconfirmReqData =
					new QuickPayPayagreeconfirmReqData(orderid,agreeid,smscode,thpinfo);
			new QuikPayPayagreeconfirmBusiness().run(quickPayPayagreeconfirmReqData, new ResultListener() {
				@Override
				public void onQuickPayPayagreeconfirmFail(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData) {
					Util.log(QuikPayPayagreeconfirmBusiness.result);
					throw new CheckedException("支付失败");
				}
				@Override
				public void onQuickPayPayagreeconfirmSuccess(QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData) {
					Util.log("支付成功，得到支付信息："+quickPayPayagreeconfirmResData);
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    public QuikPayPayagreeconfirmBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		quickPayPayagreeconfirmService = new QuickPayPayagreeconfirmService();
    }
    
    /**
     * 直接执行快捷支付确认业务逻辑
     * @param quickPayPayagreeconfirmReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(QuickPayPayagreeconfirmReqData quickPayPayagreeconfirmReqData, ResultListener resultListener) throws Exception {
    	String quickPayPayagreeconfirmResDataString = quickPayPayagreeconfirmService.request(quickPayPayagreeconfirmReqData);
		Util.log("快捷支付确认响应参数："+quickPayPayagreeconfirmResDataString);
		QuickPayPayagreeconfirmResData quickPayPayagreeconfirmResData = JsonTools.parseObject(quickPayPayagreeconfirmResDataString,QuickPayPayagreeconfirmResData.class);
    	if (ValidateUtil.isEmpty(quickPayPayagreeconfirmResData)|| ValidateUtil.isEmpty(quickPayPayagreeconfirmResData.getRetcode())) {
			setResult("快捷支付确认失败，API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问");
			resultListener.onQuickPayPayagreeconfirmFail(quickPayPayagreeconfirmResData);
			return;
		}
		if (quickPayPayagreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			setResult("快捷支付确认失败，失败原因:"+quickPayPayagreeconfirmResData.getRetmsg()+",请检测Post给API的数据是否规范合法");
			resultListener.onQuickPayPayagreeconfirmFail(quickPayPayagreeconfirmResData);
			return;
		} 
		if (!quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_0000) &&
				!quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000) &&
				!quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008)) {
			String str="快捷支付确认失败,失败原因：" + quickPayPayagreeconfirmResData.getErrmsg();
			setResult(str);
			resultListener.onQuickPayPayagreeconfirmFail(quickPayPayagreeconfirmResData);
			return;
		}

		if (quickPayPayagreeconfirmResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				(quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayapplyagreeResData.TRXSTATUS_0000)||
						quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2000)||
								quickPayPayagreeconfirmResData.getTrxstatus().equals(QuickPayPayagreeconfirmResData.TRXSTATUS_2008))) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(quickPayPayagreeconfirmResDataString)){
				setResult("快捷支付确认失败,失败原因:签名认证不通过");
				resultListener.onQuickPayPayagreeconfirmFail(quickPayPayagreeconfirmResData);
				return;
			}
			setResult("快捷支付确认成功");
			resultListener.onQuickPayPayagreeconfirmSuccess(quickPayPayagreeconfirmResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		QuikPayPayagreeconfirmBusiness.result = result;
	}
    
}
