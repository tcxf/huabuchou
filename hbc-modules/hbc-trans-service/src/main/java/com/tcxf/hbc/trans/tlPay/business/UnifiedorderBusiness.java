package com.tcxf.hbc.trans.tlPay.business;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.common.util.exception.CheckedException;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.BaseQuickPayReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqJsData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import com.tcxf.hbc.trans.tlPay.service.UnifiedorderService;
import com.tcxf.hbc.trans.tlPay.util.Util;


/**
 * 微信支付统一下单业务处理器
 * @author liuxu
 * @creation 2018年8月2日
 */
public class UnifiedorderBusiness {
	
	private UnifiedorderService unifiedorderService;

	/**
	 * 执行结果
	 */
	public static String result;
	
	
	/**
	 * 事件监听接口
	 */
	public interface ResultListener {
		/**
		 * 预支付订单失败
		 * 
		 * @param unifiedorderResData
		 */
		public void onUnifiedorderFail(UnifiedorderResData unifiedorderResData);

		/**
		 * 预支付订单成功
		 * 
		 * @param unifiedorderResData
		 */
		public void onUnifiedorderSuccess(UnifiedorderResData unifiedorderResData);

	}

	public static void main(String[] args) {
        String trxamt="100";
        String reqsn=RandomStringGenerator.getRandomStringByLength(12);
        String body="测试商户收款";
        String acct="obxVUt5Fb8r_w2u4_ohKygBsAn08";//obxVUt5Fb8r_w2u4_ohKygBsAn08
        try {
			UnifiedorderReqData unifiedorderReqData =new UnifiedorderReqData(trxamt,reqsn,body,acct);
			new UnifiedorderBusiness().run(unifiedorderReqData, new UnifiedorderBusiness.ResultListener() {
				@Override
				public void onUnifiedorderFail(UnifiedorderResData unifiedorderResData) {
					Util.log(UnifiedorderBusiness.result);
					throw new CheckedException("支付失败");
				}
				@Override
				public void onUnifiedorderSuccess(UnifiedorderResData unifiedorderResData) {
					Util.log("预支付成功，得到支付信息："+unifiedorderResData.getPayinfo());
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
    public UnifiedorderBusiness() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
    	unifiedorderService = new UnifiedorderService();
    }
    
    /**
     * 直接执行统一下单业务逻辑
     * @param unifiedorderReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @param resultListener 统一监听接口 业务逻辑可能触发的各种分支事件，并做好合理的响应处理
     * @throws Exception
     */
    public void run(UnifiedorderReqData unifiedorderReqData, ResultListener resultListener) throws Exception {

    	String unifiedorderServiceResponseString = unifiedorderService.request(unifiedorderReqData);
		Util.log("微信预支付响应参数："+unifiedorderServiceResponseString);
		UnifiedorderResData unifiedorderResData = JsonTools.parseObject(unifiedorderServiceResponseString,UnifiedorderResData.class);
		unifiedorderResData.setUnifiedorderReqJsData(JsonTools.parseObject(unifiedorderResData.getPayinfo(),UnifiedorderReqJsData.class));
    	if (ValidateUtil.isEmpty(unifiedorderResData)|| ValidateUtil.isEmpty(unifiedorderResData.getRetcode())) {
			setResult("微信预支付失败，API请求错误，请仔细检测传过去的每一个参数是否合法，或是看API能否被正常访问");
			resultListener.onUnifiedorderFail(unifiedorderResData);
			return;
		}
		if (unifiedorderResData.getRetcode().equals(BaseQuickPayReqData.FAIL)) {
			setResult("微信预支付失败，失败原因:"+unifiedorderResData.getRetmsg()+",请检测Post给API的数据是否规范合法");
			resultListener.onUnifiedorderFail(unifiedorderResData);
			return;
		} 
		if (!unifiedorderResData.getTrxstatus().equals(UnifiedorderResData.TRXSTATUS_0000)) {
			setResult("微信预支付失败,失败原因：" + unifiedorderResData.getErrmsg());
			resultListener.onUnifiedorderFail(unifiedorderResData);
			return;
		}
		if (unifiedorderResData.getRetcode().equals(BaseQuickPayReqData.SUCCESS)&&
				unifiedorderResData.getTrxstatus().equals(UnifiedorderResData.TRXSTATUS_0000)) {
			//进行签名认证
			if(!Signature.checkIsSignValidFromResponseString(unifiedorderServiceResponseString)){
				setResult("微信预支付失败,失败原因:签名认证不通过");
				resultListener.onUnifiedorderFail(unifiedorderResData);
				return;
			}
			setResult("微信预支付成功");
			resultListener.onUnifiedorderSuccess(unifiedorderResData);
		}
    }
    
	public static String getResult() {
		return result;
	}
	public static void setResult(String result) {
		Util.log(result);
		UnifiedorderBusiness.result = result;
	}
    
}
