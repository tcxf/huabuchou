package com.tcxf.hbc.trans.tlPay.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.tcxf.hbc.common.bean.config.NotifyUrlPropertiesConfig;
import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqJsData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderResData;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * 这里放置各种配置数据
 * @author liuxu
 *
 */
@Service
public class Configure {


	private static NotifyUrlPropertiesConfig notifyUrlPropertiesConfig;

	//全额支付
	public final  static String PAY_STATUS_1 = "1";

	//1分钱支付
	public final  static String PAY_STATUS_0 = "0";

	//支付通道分配的key
	public final  static String MCHKEY = "huabuchoukey2018";
	//支付通道分配的 appId
	public final static String APPID = "00097631";
	//支付通道分配的商户号ID
	public final static String CUSID = "5515510481658ZE";

	/**
	 * 支付路径回调地址
	 */
	public final static String NOTIFY_URL_TYPE="1";

	/**
	 * 提前还款回调路径类型
	 */
	public final static String NOTIFY_BEFORE_PAY_URL_TYPE="2";

	/**
	 * 正常（已出账）还款回调路径 类型
	 */
	public final static String NOTIFY_NORMAL_PAY_URL_TYPE="3";

	//回调路径
	public  static String NORMAL_PAY_URL= "";

	//支付类型
	//微信JS支付
	public final static String PAYTYPE_W02 = "W02";
	//支付限制
	public final static String LIMIT_PAY = "no_credit";

	//统一下单URL
	public final static String UNIFIEDORDER_API = "https://vsp.allinpay.com/apiweb/unitorder/pay";

	//签约申请 URL
	public final static String AGREEAPPLY_API = "https://vsp.allinpay.com/apiweb/qpay/agreeapply";

	//签约申请确认 URL
	public final static String AGREECONFIRM_API = "https://vsp.allinpay.com/apiweb/qpay/agreeconfirm";

	//重发签约短信验证码 URL
	public final static String AGREESMS_API = "https://vsp.allinpay.com/apiweb/qpay/agreesms";

	//商户支付申请 URL
	public final static String PAYAPPLYAGREE_API = "https://vsp.allinpay.com/apiweb/qpay/payapplyagree";

	//支付确认 URL
	public final static String PAYAGREECONFIRM_API = "https://vsp.allinpay.com/apiweb/qpay/payagreeconfirm";

	//重新获取支付短信 URL
	public final static String PAYSMSAGREE_API = "https://vsp.allinpay.com/apiweb/qpay/paysmsagree";

	//支付交易查询
	public final static String PAYQUERY_API = "https://vsp.allinpay.com/apiweb/qpay/query";

	/**
	 * 获取支付回调路径
	 * @param type
	 * @return
	 */
	public static String getNotifyUrl(String type){
		if(StringUtils.equals(Configure.NOTIFY_URL_TYPE,type))
		{
			NORMAL_PAY_URL = notifyUrlPropertiesConfig.getRj_notify_url();
		}
		else if (StringUtils.equals(Configure.NOTIFY_BEFORE_PAY_URL_TYPE,type))
		{
			NORMAL_PAY_URL = notifyUrlPropertiesConfig.getNotify_before_pay_url();
		}
		else  if (StringUtils.equals(Configure.NOTIFY_NORMAL_PAY_URL_TYPE,type))
		{
			NORMAL_PAY_URL  = notifyUrlPropertiesConfig.getNotify_normal_pay_url();
		}
		return NORMAL_PAY_URL;
	}

	public  NotifyUrlPropertiesConfig getNotifyUrlPropertiesConfig() {
		return notifyUrlPropertiesConfig;
	}

	@Autowired
	public  void setNotifyUrlPropertiesConfig(NotifyUrlPropertiesConfig notifyUrlPropertiesConfig) {
		Configure.notifyUrlPropertiesConfig = notifyUrlPropertiesConfig;
	}
}
