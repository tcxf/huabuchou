package com.tcxf.hbc.trans.tlPay.common;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.trans.tlPay.service.IServiceRequest;
import com.tcxf.hbc.trans.tlPay.util.SSLUtil;
import com.tcxf.hbc.trans.tlPay.util.Util;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.HashMap;
import java.util.Map;

/**
 * HttpsRequest
 * @author liuxu
 *
 * @creation 2018年8月2日
 */
public class HttpsRequest implements IServiceRequest {

    private HttpURLConnection conn;
    private String connectUrl;

    public HttpsRequest(String connectUrl) throws Exception {
        this.connectUrl = connectUrl;
        init();
    }

    private void init() throws Exception{
        URL url = new URL(connectUrl);
        System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                return urlHostName.equals(session.getPeerHost());
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
        URLConnection conn = url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setReadTimeout(60000);
        conn.setConnectTimeout(30000);
        if (conn instanceof HttpsURLConnection){
            HttpsURLConnection httpsConn = (HttpsURLConnection)conn;
            httpsConn.setSSLSocketFactory(SSLUtil.getInstance().getSSLSocketFactory());
        } else if (conn instanceof HttpURLConnection){
            HttpURLConnection httpConn = (HttpURLConnection)conn;
        } else {
            throw new Exception("不是http/https协议的url");
        }
        this.conn = (HttpURLConnection)conn;
        initDefaultPost();
    }

    @Override
    public String sendPost(Object jsonObj) throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Util.log("接口提交数据:"+JsonTools.getJsonFromObject(jsonObj));
        Map<String, String> params = JsonTools.parseJSON2Map(JsonTools.getJsonFromObject(jsonObj));
        StringBuilder outBuf = new StringBuilder();
        boolean isNotFirst = false;
        for (Map.Entry<String, String> entry: params.entrySet()){
            if (isNotFirst)
                outBuf.append('&');
            isNotFirst = true;
            outBuf.append(entry.getKey())
                    .append('=')
                    .append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        byte[] bys = postParams(outBuf.toString());
        return new String(bys,"UTF-8");
    }

    public byte[] postParams(String message) throws IOException {
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(message.getBytes("UTF-8"));
        out.close();
        return readBytesFromStream(conn.getInputStream());
    }

    private byte[] readBytesFromStream(InputStream is) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int readLen;
        byte[] tmpBuf = new byte[4096];
        while ((readLen = is.read(tmpBuf)) > 0)
            baos.write(tmpBuf, 0, readLen);
        is.close();
        return baos.toByteArray();
    }

    private void initDefaultPost() throws Exception{
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");
        conn.setUseCaches(false);
        conn.setInstanceFollowRedirects(true);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    }
}
