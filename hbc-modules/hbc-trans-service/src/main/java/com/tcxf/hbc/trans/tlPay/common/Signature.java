package com.tcxf.hbc.trans.tlPay.common;

import com.tcxf.hbc.common.util.JsonTools;
import com.tcxf.hbc.common.util.ValidateUtil;
import com.tcxf.hbc.trans.tlPay.util.Util;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 *  签名
 * @author liuxu
 *
 * @creation 2018年8月2日
 */
public class Signature {

    /**
     * 签名
     * @param object
     * @return
     * @throws Exception
     */
    public static String sign(Object object){
        TreeMap<String,String> params = JsonTools.parseJSON2TreeMap(JsonTools.toJson(object));
        if(params.containsKey("sign")){
            //签名明文组装不包含sign字段
            params.remove("sign");
        }
        params.put("key", Configure.MCHKEY);
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String, String> entry:params.entrySet()){
            if(entry.getValue()!=null&&entry.getValue().length()>0){
                sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        if(sb.length()>0){
            sb.deleteCharAt(sb.length()-1);
        }
        String sign = MD5.MD5Encode(sb.toString()).toUpperCase();//记得是md5编码的加签
        params.remove("key");
        return sign;
    }

    /**
     * 检验API返回的数据里面的签名是否合法，避免数据在传输的过程中被第三方篡改
     * @param responseString API返回的XML数据字符串
     * @return API签名是否合法
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static boolean checkIsSignValidFromResponseString(String responseString) throws Exception {
        TreeMap<String,String> map = JsonTools.parseJSON2TreeMap(responseString);
        Util.log(map.toString());
        Object signFromAPIResponse = map.get("sign");
        if(ValidateUtil.isEmpty(signFromAPIResponse)){
            Util.log("API返回的数据签名数据不存在，有可能被第三方篡改!!!");
            return false;
        }
        Util.log("服务器回包里面的签名是:" + signFromAPIResponse.toString());
        //清掉返回数据对象里面的Sign数据（不能把这个数据也加进去进行签名），然后用签名算法进行签名
        map.put("sign","");
        //将API返回的数据根据用签名算法进行计算新的签名，用来跟API返回的签名进行比较
        String signForAPIResponse = sign(map);
        Util.log("自己的签名是"+signForAPIResponse.toString());
        if(!signForAPIResponse.equals(signFromAPIResponse.toString())){
            //签名验不过，表示这个API返回的数据有可能已经被篡改了
            Util.log("API返回的数据签名验证不通过，有可能被第三方篡改!!!");
            return false;
        }
        Util.log("API返回的数据签名验证通过!!!");
        return true;
    }
}
