package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

import java.lang.reflect.Field;
import java.util.TreeMap;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 快捷支付公共参数
 */
public class BaseQuickPayReqData {

    /**
     * result_code 返回值
     */
    public  final static String SUCCESS="SUCCESS";
    /**
     * result_code 返回值
     */
    public  final static String FAIL="FAIL";

    /**
     * 快捷支付
     */
    public final static String VSP301="VSP301";

    /**
     * 微信支付
     */
    public final static String VSP501="VSP501";

    /**
     * 商户号 通联分配的商户号 必填Y
     */
    private String cusid;
    /**
     * 应用ID平台分配的APPID
     */
    private String appid;
    /**
     *版本号	接口版本号 默认填11
     */
    private String version = "11";
    /**
     * 请求ip
     */
    private String reqip;
    /**
     * 本次请求时间 yyyyMMddHHmmss
     */
    private String reqtime;
    /**
     * 随机字符串	商户自行生成的随机字符串 必填 32位
     */
    private String randomstr;
    /**
     * sign	签名 32位 必填
     */
    private String sign;

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReqip() {
        return reqip;
    }

    public void setReqip(String reqip) {
        this.reqip = reqip;
    }

    public String getReqtime() {
        return reqtime;
    }

    public void setReqtime(String reqtime) {
        this.reqtime = reqtime;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
