package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 签约申请提交数据结构
 */
public class QuickPayAgreeapplyReqData extends BaseQuickPayReqData {
    /**
     *商户用户号		否	32
     */
    private String  meruserid;
    /**
     * 卡类型	00:借记卡 02:信用卡	否
     */
    private String accttype ="00";
    /**
     *银行卡号		否	20
     */
    private String acctno;
    /**
     *证件号		否	22
     */
    private String idno;
    /**
     *户名		否	20
     */
    private String acctname;
    /**
     * 手机号码		否	11
     */
    private String mobile;
    /**
     * 有效期	MMyy	是	4	信用卡不能为空
     */
    private String validdate;
    /**
     * Cvv2		是	4	信用卡不能为空
     */
    private String cvv2;

    public QuickPayAgreeapplyReqData(String meruserid,String acctno,
                                     String idno,String acctname,String mobile)  {

        setCusid(Configure.CUSID);
        setAppid(Configure.APPID);
        setMeruserid(meruserid);
        setAcctno(acctno);
        setAcctname(acctname);
        setIdno(idno);
        setMobile(mobile);
        setRandomstr(RandomStringGenerator.getRandomStringByLength(32));
        String sign = Signature.sign(this);
        setSign(sign);
    }

    public String getMeruserid() {
        return meruserid;
    }

    public void setMeruserid(String meruserid) {
        this.meruserid = meruserid;
    }

    public String getAccttype() {
        return accttype;
    }

    public void setAccttype(String accttype) {
        this.accttype = accttype;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getValiddate() {
        return validdate;
    }

    public void setValiddate(String validdate) {
        this.validdate = validdate;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

}
