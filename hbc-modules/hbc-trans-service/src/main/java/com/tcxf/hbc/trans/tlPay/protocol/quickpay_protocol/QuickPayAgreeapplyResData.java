package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 协议申请响应数据结构
 */
public class QuickPayAgreeapplyResData {

    /**
     * 1999: 短信验证码已发送,请调用申请确认完成签约
     * 3004: 卡号错误(无法识别卡bin)
     * 3051: 协议已存在,请勿重复签约
     * 3999: 签约失败
     */
    public  final static String TRXSTATUS_1999="1999";

    /**
     * 返回状态码  SUCCESS/FAIL
     */
    private String retcode;

    /**
     * 返回信息 返回信息，如非空，为错误原因
     */
    private String retmsg;

    /**
     *随机生成的字符串
     */
    private String randomstr;
    /**
     * 签名
     */
    private String sign;

    //-------------业务参数 当retcode为SUCCESS时有返回-----------
    private String  trxstatus;

    /**
     * 错误信息
     */
    private  String  errmsg;

    /**
     * 交易透传信息 如果不为空,则签约申请确认(或者重新触发短信)请原样带上
     */
    private  String  thpinfo;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTrxstatus() {
        return trxstatus;
    }

    public void setTrxstatus(String trxstatus) {
        this.trxstatus = trxstatus;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}
