package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 重发签约短信验证码提交数据结构
 */
public class QuickPayAgreesmsReqData extends BaseQuickPayReqData {

    /**
     *商户用户号		否	32
     */
    private String  meruserid;

    /**
     * 卡类型	00:借记卡   02:信用卡
     */
    private String  accttype;

    /**
     * 银行卡号
     */
    private String  acctno;

    /**
     * 证件号
     */
    private String  idno;

    /**
     * 户名
     */
    private String  acctname;

    /**
     * 手机号码
     */
    private String  mobile;

    /**
     * 有效期	MMyy 信用卡不能为空
     */
    private String  validdate;

    /**
     * Cvv2 信用卡不能为空
     */
    private String  cvv2;
    /**
     * 交易透传信息	签约申请时返回,如果不为空,则原样带上
     */
    private String  thpinfo;

    public String getMeruserid() {
        return meruserid;
    }

    public void setMeruserid(String meruserid) {
        this.meruserid = meruserid;
    }

    public String getAccttype() {
        return accttype;
    }

    public void setAccttype(String accttype) {
        this.accttype = accttype;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getValiddate() {
        return validdate;
    }

    public void setValiddate(String validdate) {
        this.validdate = validdate;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}
