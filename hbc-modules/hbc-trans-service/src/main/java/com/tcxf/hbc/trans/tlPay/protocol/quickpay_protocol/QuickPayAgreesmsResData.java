package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 重发签约短信验证码响应数据结构
 */
public class QuickPayAgreesmsResData  {

    /**
     * 返回码 SUCCESS/FAIL
     */
    private String retcode;
    /**
     * 返回码说明		是	100
     */
    private String retmsg;
    /**
     * 随机字符串	随机生成的字符串	否	是
     */
    private String randomstr;
    /**
     * 签名
     */
    private String sign;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
