package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 支付确认提交数据结构
 */
public class QuickPayPayagreeconfirmReqData extends BaseQuickPayReqData {

    /**
     * 订单号	商户订单号	否
     */
    private String orderid;
    /**
     * 协议编号
     */
    private String agreeid;
    /**
     * 短信验证码
     */
    private String smscode;
    /**
     * 交易透传信息		是	-	支付申请或者错误码为1999返回的thpinfo原样带上
     */
    private String thpinfo;

    public QuickPayPayagreeconfirmReqData(String orderid,String agreeid,String smscode,String thpinfo){
        setOrderid(orderid);
        setAgreeid(agreeid);
        setSmscode(smscode);
        setThpinfo(thpinfo);
        setCusid(Configure.CUSID);
        setAppid(Configure.APPID);
        setRandomstr(RandomStringGenerator.getRandomStringByLength(32));
        String sign = Signature.sign(this);
        setSign(sign);
    }



    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getSmscode() {
        return smscode;
    }

    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }

}