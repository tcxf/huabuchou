package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;
import org.apache.commons.lang.StringUtils;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 商户支付申请提交数据结构
 */
public class QuickPayPayapplyagreeReqData extends BaseQuickPayReqData {

    /**
     * 商户订单号
     */
    private String orderid;
    /**
     * 协议编号 签约返回
     */
    private String agreeid;
    /**
     * 订单金额 单位分
     */
    private String amount;
    /**
     * 币种		否	8	暂只支持CNY
     */
    private String currency ="CNY";
    /**
     *订单内容	订单的展示标题
     */
    private String subject;
    /**
     * 有效时间	订单有效时间	是	-	最大1440分钟
     */
    private String validtime;
    /**
     * 交易备注	用于用户订单个性化信息
     * 交易完成通知会带上本字段 160字符
     */
    private String trxreserve;
    /**
     * 交易结果通知地址	接收交易结果通知回调地址，通知url必须为直接可访问的url，不能携带参数。
     */
    private String notifyurl;

    /**
     *
     * @param orderid
     * @param agreeid
     * @param amount
     * @param subject
     * @param type  2：正常还款，3：提前还款
     */
    public QuickPayPayapplyagreeReqData(String orderid,String agreeid,
                                        String amount,String subject,String type){
        setOrderid(orderid);
        setAgreeid(agreeid);
        setAmount(amount);
        setSubject(subject);
        setNotifyurl(Configure.getNotifyUrl(type));
        setCusid(Configure.CUSID);
        setAppid(Configure.APPID);
        setRandomstr(RandomStringGenerator.getRandomStringByLength(32));
        String sign = Signature.sign(this);
        setSign(sign);
    }



    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getValidtime() {
        return validtime;
    }

    public void setValidtime(String validtime) {
        this.validtime = validtime;
    }

    public String getTrxreserve() {
        return trxreserve;
    }

    public void setTrxreserve(String trxreserve) {
        this.trxreserve = trxreserve;
    }

    public String getNotifyurl() {
        return notifyurl;
    }

    public void setNotifyurl(String notifyurl) {
        this.notifyurl = notifyurl;
    }
}
