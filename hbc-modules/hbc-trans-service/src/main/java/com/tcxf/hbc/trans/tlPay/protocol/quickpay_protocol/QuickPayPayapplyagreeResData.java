package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 商户支付申请响应数据结构
 */
public class QuickPayPayapplyagreeResData  {

    /**
     * 0000:交易成功,交易流程完成,无需支付确认
     * 1999: 短信验证码已发送,请继续调用支付确认接口完成支付
     * 2008/2000:交易处理中,请查询交易
     * 其他3开头,交易失败
     */
    public  final static String TRXSTATUS_0000="0000";

    public  final static String TRXSTATUS_1999="1999";



    /**
     * 返回码	SUCCESS/FAIL
     */
    private String retcode;
    /**
     * 返回码说明
     */
    private String retmsg;
    /**
     * 随机字符串	随机生成的字符串	否	是	SUCCESS返回
     */
    private String randomstr;
    /**
     * 签名 SUCCESS返回
     */
    private String sign;

//--------------业务参数 当retcode为SUCCESS时有返回
    /**
     * 商户订单号
     */
    private String orderid;
    /**
     * 交易状态	交易的状态,
     * 0000:交易成功,交易流程完成
     */
    private String trxstatus;
    /**
     * 错误原因	失败的原因说明
     */
    private String errmsg;
    /**
     * 交易单号	平台的交易流水号	否	20	交易成功返回
     */
    private String trxid;
    /**
     * 渠道平台交易单号		是	50	交易成功返回
     */
    private String chnltrxid;
    /**
     * 交易完成时间	yyyyMMddHHmmss	是	14	交易成功返回
     */
    private String fintime;
    /**
     * 交易透传信息		是	-	支付确认时需原样带上本字段信息
     */
    private String thpinfo;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getTrxstatus() {
        return trxstatus;
    }

    public void setTrxstatus(String trxstatus) {
        this.trxstatus = trxstatus;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getChnltrxid() {
        return chnltrxid;
    }

    public void setChnltrxid(String chnltrxid) {
        this.chnltrxid = chnltrxid;
    }

    public String getFintime() {
        return fintime;
    }

    public void setFintime(String fintime) {
        this.fintime = fintime;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}
