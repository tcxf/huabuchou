package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 重新获取支付短信提交数据结构
 */
public class QuickPayPaysmsagreeReqData extends BaseQuickPayReqData {

    /**
     * 商户订单号
     */
    private String orderid;
    /**
     * 协议编号	 协议支付必填
     */
    private String agreeid;
    /**
     * thpinfo	交易透传信息		是	-	提交支付的时候,接口返回,请原样带上
     */
    private String thpinfo;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAgreeid() {
        return agreeid;
    }

    public void setAgreeid(String agreeid) {
        this.agreeid = agreeid;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}