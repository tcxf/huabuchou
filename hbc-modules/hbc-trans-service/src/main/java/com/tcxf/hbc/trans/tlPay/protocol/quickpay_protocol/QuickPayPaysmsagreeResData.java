package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/6 16:47
 * @Description: 重新获取支付短信响应数据结构
 */
public class QuickPayPaysmsagreeResData extends BaseQuickPayReqData {

    /**
     * 返回码	SUCCESS/FAIL
     */
    private String retcode;
    /**
     * 返回码说明
     */
    private String retmsg;
    /**
     * 随机字符串	随机生成的字符串 SUCCESS返回
     */
    private String randomstr;
    /**
     * 签名 SUCCESS返回
     */
    private String sign;

    //------------业务参数 retcode为SUCCESS时有返回-------------
    /**
     * 交易透传信息		是	-	下一步提交请原样带上
     */
    private String thpinfo;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    @Override
    public String getRandomstr() {
        return randomstr;
    }

    @Override
    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    @Override
    public String getSign() {
        return sign;
    }

    @Override
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getThpinfo() {
        return thpinfo;
    }

    public void setThpinfo(String thpinfo) {
        this.thpinfo = thpinfo;
    }
}