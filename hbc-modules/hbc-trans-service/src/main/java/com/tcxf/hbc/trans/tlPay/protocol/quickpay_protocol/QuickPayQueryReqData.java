package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;

/**
 * @Auther: liuxu
 * @Date: 2018/8/28 16:47
 * @Description: 交易查询提交数据结构
 */
public class QuickPayQueryReqData extends BaseQuickPayReqData {

    /**
     * 商户订单号
     */
    private String orderid;
    /**
     * 支付的收银宝平台流水
     * 如果不为空,则必须为大于0的整数
     * orderid和trxid必填其一
     * 建议:商户如果同时拥有trxid和orderid,优先使用trxid
     */
    private String trxid;

    public QuickPayQueryReqData(String orderid,String trxid){
        setOrderid(orderid);
        setTrxid(trxid);
        setCusid(Configure.CUSID);
        setAppid(Configure.APPID);
        setRandomstr(RandomStringGenerator.getRandomStringByLength(32));
        String sign = Signature.sign(this);
        setSign(sign);
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }
}