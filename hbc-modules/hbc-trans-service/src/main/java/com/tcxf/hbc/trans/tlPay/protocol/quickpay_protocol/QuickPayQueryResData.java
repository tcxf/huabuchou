package com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol;

/**
 * @Auther: liuxu
 * @Date: 2018/8/28 16:47
 * @Description: 交易查询 响应数据结构
 */
public class QuickPayQueryResData {


    /**
     * 返回码	SUCCESS/FAIL
     */
    private String retcode;

    /**
     * 返回码说明
     */
    private String retmsg;

    /**
     * 随机字符串	随机生成的字符串 SUCCESS返回
     */
    private String randomstr;

    /**
     * 签名 SUCCESS返回
     */
    private String sign;

    //------------业务参数 retcode为SUCCESS时有返回-------------

    /**
     * 商户号	平台分配的商户号
     */
    private String cusid;
    /**
     * 商户号	平台分配的商户号
     */
    private String appid;
    /**
     * 交易单号	平台的交易流水号
     */
    private String trxid;
    /**
     * 支付渠道交易单号	如支付宝,微信平台的交易单号
     */
    private String chnltrxid;
    /**
     * 商户订单号	商户的交易订单号
     */
    private String orderid;
    /**
     * 交易类型	交易类型
     */
    private String trxcode;
    /**
     * 交易金额	单位为分
     */
    private String trxamt;
    /**
     * 交易状态	交易的状态
     */
    private String trxstatus;
    /**
     * 支付账号
     */
    private String acct;
    /**
     * 交易完成时间	yyyyMMddHHmmss
     */
    private String fintime;
    /**
     * 错误原因	失败的原因说明
     */
    private String errmsg;

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public String getRetmsg() {
        return retmsg;
    }

    public void setRetmsg(String retmsg) {
        this.retmsg = retmsg;
    }

    public String getRandomstr() {
        return randomstr;
    }

    public void setRandomstr(String randomstr) {
        this.randomstr = randomstr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getChnltrxid() {
        return chnltrxid;
    }

    public void setChnltrxid(String chnltrxid) {
        this.chnltrxid = chnltrxid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getTrxcode() {
        return trxcode;
    }

    public void setTrxcode(String trxcode) {
        this.trxcode = trxcode;
    }

    public String getTrxamt() {
        return trxamt;
    }

    public void setTrxamt(String trxamt) {
        this.trxamt = trxamt;
    }

    public String getTrxstatus() {
        return trxstatus;
    }

    public void setTrxstatus(String trxstatus) {
        this.trxstatus = trxstatus;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getFintime() {
        return fintime;
    }

    public void setFintime(String fintime) {
        this.fintime = fintime;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}