package com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.common.RandomStringGenerator;
import com.tcxf.hbc.trans.tlPay.common.Signature;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 统一下单，提交数据结构
 */
public class UnifiedorderReqData {
	/**
	 * 商户号 通联支付分配的商户号 必填 Y
	 */
	private String cusid = "";
	/**
	 * 应用ID 平台分配的APPID  必填 Y
	 */
	private String appid = "";
	/**
	 * 版本号 默认填11
	 */
	private String version = "11";
	/**
	 * 交易金额	单位为分 必填 Y
	 */
	private String trxamt = "";
	/**
	 * 商户交易单号 商户的交易订单号 32位  必填 Y
	 */
	private String reqsn = "";
	/**
	 *  交易方式 必填
	 * W02：微信JS支付
	 * A02：支付宝JS支付
	 * Q02：QQ钱包JS支付
	 */
	private String paytype = "";
	/**
	 * 随机字符串 32 位 必填 Y
	 */
	private String randomstr = "";
	/**
	 * 订单标题 订单商品名称，为空则以商户名作为商品名称
	 */
	private String body = "";
	/**
	 * 备注信息
	 */
	private String remark = "";
	/**
	 * 订单有效时间，以分为单位，不填默认为15分钟 最大60分钟
	 */
	private String validtime = "";
	/**
	 * 支付平台用户标识
	 * JS支付时使用 微信支付-用户的微信openid
	 */
	private String acct = "";
	/**
	 * 交易结果通知地址 接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
	 */
	private String notify_url = "";
	/**
	 * 支付限制	no_credit--指定不能使用信用卡支付 暂时只对微信支付有效,仅支持no_credit
	 */
	private String limit_pay = "";
	/**
	 *分账信息
	 */
	private String asinfo = "";
	/**
	 * 签名
	 */
	private String sign = "";


	/**
	 * @param trxamt 金额 单位为分
	 * @param reqsn  商户交易单号 商户的交易订单号 32位
	 * @param body 商品名称
	 * @param acct 支付平台用户标识 JS支付时使用 微信支付-用户的微信openid
	 */
	public UnifiedorderReqData(String trxamt,String reqsn,String body,String acct) throws Exception {
		setCusid(Configure.CUSID);
		setAppid(Configure.APPID);
		setTrxamt(trxamt);
		setReqsn(reqsn);
		setPaytype(Configure.PAYTYPE_W02);
		setRandomstr(RandomStringGenerator.getRandomStringByLength(32));
		setBody(body);
		setAcct(acct);
		setNotify_url(Configure.getNotifyUrl(Configure.NOTIFY_URL_TYPE));
		setLimit_pay(Configure.LIMIT_PAY);
		String sign = Signature.sign(this);
        setSign(sign);
	}
	public String getCusid() {
		return cusid;
	}

	public void setCusid(String cusid) {
		this.cusid = cusid;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTrxamt() {
		return trxamt;
	}

	public void setTrxamt(String trxamt) {
		this.trxamt = trxamt;
	}

	public String getReqsn() {
		return reqsn;
	}

	public void setReqsn(String reqsn) {
		this.reqsn = reqsn;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getRandomstr() {
		return randomstr;
	}

	public void setRandomstr(String randomstr) {
		this.randomstr = randomstr;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getValidtime() {
		return validtime;
	}

	public void setValidtime(String validtime) {
		this.validtime = validtime;
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getLimit_pay() {
		return limit_pay;
	}

	public void setLimit_pay(String limit_pay) {
		this.limit_pay = limit_pay;
	}

	public String getAsinfo() {
		return asinfo;
	}

	public void setAsinfo(String asinfo) {
		this.asinfo = asinfo;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
}
