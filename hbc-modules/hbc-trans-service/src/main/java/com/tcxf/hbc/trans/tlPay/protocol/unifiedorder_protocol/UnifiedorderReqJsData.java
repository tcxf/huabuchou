package com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 支付JS，提交数据结构
 */
public class UnifiedorderReqJsData {
	/**
	 * 公众账号ID  微信分配的公众账号ID 必填 Y 
	 */
	private String appId = "";
	/**
	 * 时间戳 10位的
	 */
	private String timeStamp = "";
	
	/**
	 * 随机字符串 随机字符串，不长于32位  必填 Y
	 */
	private String nonceStr = "";
	/**
	 * 订单详情扩展字符串 统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
	 */
	private String payPackage = "";
	
	/**
	 * 签名方式 MD5
	 */
	private String signType = "";
	
	/**
	 * 签名  必填 Y
	 */
	private String paySign = "";

	/**
	 * 支付ID
	 */
	private String prepay_id = "";

	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public String getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getPayPackage() {
		return payPackage;
	}
	
	public void setPayPackage(String payPackage) {
		this.payPackage = payPackage;
	}
	
	public String getSignType() {
		return signType;
	}
	
	public void setSignType(String signType) {
		this.signType = signType;
	}
	
	public String getPaySign() {
		return paySign;
	}
	
	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getPrepay_id() {
		return prepay_id;
	}

	public void setPrepay_id(String prepay_id) {
		this.prepay_id = prepay_id;
	}
}
