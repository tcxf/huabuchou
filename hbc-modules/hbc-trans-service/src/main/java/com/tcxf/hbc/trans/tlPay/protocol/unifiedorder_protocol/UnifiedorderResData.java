package com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol;

/**
 * 统一下单，返回数据结构
 */
public class UnifiedorderResData {
	/**
	 * 状态码
	 */
	public  final static String TRXSTATUS_0000="0000";
	//--------------------返回结果-------------------------
	/**
	 * 返回状态码  SUCCESS/FAIL
	 * 此字段是通信标识，非交易结果，交易是否成功需要查看trxstatus来判断
	 */
    private String retcode = "";
    /**
     * 返回信息 返回信息，如非空，为错误原因
	 * 签名失败
	 * 参数格式校验错误
     */
    private String retmsg = "";
    //----------------------------------------------
    //----------------以下字段在retcode为SUCCESS的时候有返回--------------------------
	/**
	 * 商户号	平台分配的商户号
	 */
    private String cusid = "";
    /**
	 * 应用ID	平台分配的APPID
	 */
	private String appid = "";
	/**
	 * 交易单号	收银宝平台的交易流水号
	 */
	private String trxid = "";
	/**
	 * 渠道平台交易单号	例如微信,支付宝平台的交易单号
	 */
	private String chnltrxid = "";
	/**
	 * 商户交易单号	商户的交易订单号
	 */
	private String reqsn = "";
	/**
	 *随机生成的字符串
	 */
	private String randomstr = "";
	/**
	 * 交易的状态,
	 * 对于刷卡支付，该状态表示实际的支付结果，其他为下单状态
	 */
	private String trxstatus = "";
	/**
	 * 交易完成时间	yyyyMMddHHmmss
	 */
	private String fintime = "";
	/**
	 * errmsg	错误原因	失败的原因说明	是	100
	 */
	private String errmsg = "";
	/**
	 * 微信公众号JS支付则返回json字符串。
	 * 支付宝服务窗返回JSAPI支付接口参数tradeNO。
	 * QQ钱包的的公众号JS支付返回支付的链接,消费者只需跳转到此链接即可完成支付
	 */
	private String payinfo;

	private UnifiedorderReqJsData unifiedorderReqJsData;


	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

	public String getCusid() {
		return cusid;
	}

	public void setCusid(String cusid) {
		this.cusid = cusid;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getTrxid() {
		return trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}

	public String getChnltrxid() {
		return chnltrxid;
	}

	public void setChnltrxid(String chnltrxid) {
		this.chnltrxid = chnltrxid;
	}

	public String getReqsn() {
		return reqsn;
	}

	public void setReqsn(String reqsn) {
		this.reqsn = reqsn;
	}

	public String getRandomstr() {
		return randomstr;
	}

	public void setRandomstr(String randomstr) {
		this.randomstr = randomstr;
	}

	public String getTrxstatus() {
		return trxstatus;
	}

	public void setTrxstatus(String trxstatus) {
		this.trxstatus = trxstatus;
	}

	public String getFintime() {
		return fintime;
	}

	public void setFintime(String fintime) {
		this.fintime = fintime;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public UnifiedorderReqJsData getUnifiedorderReqJsData() {
		return unifiedorderReqJsData;
	}

	public void setUnifiedorderReqJsData(UnifiedorderReqJsData unifiedorderReqJsData) {
		this.unifiedorderReqJsData = unifiedorderReqJsData;
	}

	public String getPayinfo() {
		return payinfo;
	}

	public void setPayinfo(String payinfo) {
		this.payinfo = payinfo;
	}
}
