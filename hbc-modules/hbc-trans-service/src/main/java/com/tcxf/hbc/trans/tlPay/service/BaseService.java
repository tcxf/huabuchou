package com.tcxf.hbc.trans.tlPay.service;


import com.tcxf.hbc.trans.tlPay.common.HttpsRequest;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;


/**
 * 服务的基类
 * @author liuxu
 * @creation 2018年8月2日
 */
public class BaseService{


    //发请求的HTTPS请求器
    private IServiceRequest serviceRequest;

	public BaseService(String connectUrl) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        try {
			serviceRequest = new HttpsRequest(connectUrl);
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String sendPost(Object jsonObj) throws UnrecoverableKeyException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return serviceRequest.sendPost(jsonObj);
    }

    /**
     * 供商户想自定义自己的HTTP请求器用
     * @param request 实现了IserviceRequest接口的HttpsRequest
     */
    public void setServiceRequest(IServiceRequest request){
        serviceRequest = request;
    }
}
