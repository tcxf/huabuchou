package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeapplyReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;


/**
 * 签约申请接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayAgreeapplyService extends BaseService{

	public QuickPayAgreeapplyService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.AGREEAPPLY_API);
	}
	
	  /**
     * 请求签约申请服务
     * @param quickPayAgreeapplyReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(QuickPayAgreeapplyReqData quickPayAgreeapplyReqData) throws Exception {
        String responseString = sendPost(quickPayAgreeapplyReqData);
        return responseString;
    }

}
