package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeapplyReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayAgreeconfirmReqData;


/**
 * 签约确认接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayAgreeconfirmService extends BaseService{

	public QuickPayAgreeconfirmService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.AGREECONFIRM_API);
	}
	
	  /**
     * 请求签约确认服务
     * @param quickPayAgreeconfirmReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(QuickPayAgreeconfirmReqData quickPayAgreeconfirmReqData) throws Exception {
        String responseString = sendPost(quickPayAgreeconfirmReqData);
        return responseString;
    }

}
