package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeReqData;


/**
 * 快捷支付确认接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayPayagreeconfirmService extends BaseService{

	public QuickPayPayagreeconfirmService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.PAYAGREECONFIRM_API);
	}
	
	  /**
     * 快捷支付确认服务
     * @param quickPayPayagreeconfirmReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(QuickPayPayagreeconfirmReqData quickPayPayagreeconfirmReqData) throws Exception {
        String responseString = sendPost(quickPayPayagreeconfirmReqData);
        return responseString;
    }

}
