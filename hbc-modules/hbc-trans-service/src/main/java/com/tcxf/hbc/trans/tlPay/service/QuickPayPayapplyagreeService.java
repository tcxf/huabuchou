package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayapplyagreeReqData;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;


/**
 * 快捷支付申请接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayPayapplyagreeService extends BaseService{

	public QuickPayPayapplyagreeService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.PAYAPPLYAGREE_API);
	}
	
	  /**
     * 快捷支付申请服务
     * @param quickPayPayapplyagreeReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(QuickPayPayapplyagreeReqData quickPayPayapplyagreeReqData) throws Exception {
        String responseString = sendPost(quickPayPayapplyagreeReqData);
        return responseString;
    }

}
