package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayPayagreeconfirmReqData;
import com.tcxf.hbc.trans.tlPay.protocol.quickpay_protocol.QuickPayQueryReqData;


/**
 * 快捷支付交易查询接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class QuickPayQueryService extends BaseService{

	public QuickPayQueryService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.PAYQUERY_API);
	}
	
	  /**
     * 快捷支付交易查询服务
     * @param quickPayQueryReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(QuickPayQueryReqData quickPayQueryReqData) throws Exception {
        String responseString = sendPost(quickPayQueryReqData);
        return responseString;
    }

}
