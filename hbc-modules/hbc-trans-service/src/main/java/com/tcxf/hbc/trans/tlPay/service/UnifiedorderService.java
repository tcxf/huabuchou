package com.tcxf.hbc.trans.tlPay.service;

import com.tcxf.hbc.trans.tlPay.common.Configure;
import com.tcxf.hbc.trans.tlPay.protocol.unifiedorder_protocol.UnifiedorderReqData;


/**
 * 统一下单接口调用
 * @author liuxu
 * @creation 2018年8月2日
 */
public class UnifiedorderService  extends BaseService{

	public UnifiedorderService() throws ClassNotFoundException,IllegalAccessException, InstantiationException {
		super(Configure.UNIFIEDORDER_API);
	}
	
	  /**
     * 请求统一下单服务
     * @param unifiedorderReqData 这个数据对象里面包含了API要求提交的各种数据字段
     * @return API返回的数据
     * @throws Exception
     */
    public String request(UnifiedorderReqData unifiedorderReqData) throws Exception {
        String responseString = sendPost(unifiedorderReqData);
        return responseString;
    }

}
