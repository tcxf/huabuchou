package com.tcxf.hbc.trans.util.token;

import com.tcxf.hbc.common.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedisTokenManager implements TokenManager {

	private static final Long TIME =120L;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public TokenModel createToken(String userId,String trIds) {
		return createToken(userId,trIds,TIME);

	}

	@Override
	public TokenModel createToken(String userId,String trIds,Long time) {
		TokenModel model = new TokenModel(userId, trIds);
		//存储到redis并设置过期时间
		redisUtil.set(userId, trIds, time);
		return model;
	}

	@Override
	public TokenModel getToken(String userId) {
		if (ValidateUtil.isEmpty(userId)) {
			return null;
		}
		Object object = redisUtil.get(userId);
		if(ValidateUtil.isEmpty(object)){
			return null;
		}
		return new TokenModel(userId, (String) object);
	}

	@Override
	public boolean checkToken(TokenModel model) {
		if (ValidateUtil.isEmpty(model)) {
			return false;
		}
		Object value = redisUtil.get(model.getUserId());
		if (ValidateUtil.isEmpty(value)) {
			return false;
		}
		return true;
	}

	@Override
	public void deleteToken(String userId) {
		redisUtil.remove(userId);
	}
}
