package com.tcxf.hbc.trans.util.token;

public interface TokenManager {

	 /**
     * 创建一个token关联上指定用户
     */
    public TokenModel createToken(String userIds, String trIds);


    /**
     * 创建一个token关联上指定用户
     */
    public TokenModel createToken(String userIds, String trIds,Long time);

    /**
     * 检查token是否有效
     */
    public boolean checkToken(TokenModel model);

    /**
     * 从字符串中解析token
     */
    public TokenModel getToken(String userId);

    /**
     * 清除token
     */
    public void deleteToken(String userId);

}

