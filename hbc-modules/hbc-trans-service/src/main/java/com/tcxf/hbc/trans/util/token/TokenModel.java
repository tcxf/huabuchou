package com.tcxf.hbc.trans.util.token;

/**
 * Token的Model类，可以增加字段提高安全性，例如时间戳、url签名
 *
 */
public class TokenModel {

    //用户userId
    private String  userId;

    //存储value
    private String  trIds;
    
    public TokenModel(String userId,String trIds) {
        this.trIds = trIds;
        this.userId = userId;
    }
    public TokenModel(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTrIds() {
        return trIds;
    }

    public void setTrIds(String trIds) {
        this.trIds = trIds;
    }
}