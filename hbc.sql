﻿/*
Navicat MySQL Data Transfer

Source Server         : 172.31.30.82
Source Server Version : 50640
Source Host           : 172.31.30.82:3306
Source Database       : hbc

Target Server Type    : MYSQL
Target Server Version : 50640
File Encoding         : 65001

Date: 2018-06-28 21:00:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_credit_config
-- ----------------------------
DROP TABLE IF EXISTS `base_credit_config`;
CREATE TABLE `base_credit_config` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `parentId` varchar(50) NOT NULL,
  `type` varchar(3) NOT NULL,
  `content` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for base_interface_info
-- ----------------------------
DROP TABLE IF EXISTS `base_interface_info`;
CREATE TABLE `base_interface_info` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL,
  `type` varchar(10) NOT NULL,
  `company` varchar(50) NOT NULL,
  `content` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for c_credit_user_info
-- ----------------------------
DROP TABLE IF EXISTS `c_credit_user_info`;
CREATE TABLE `c_credit_user_info` (
  `id` varchar(50) NOT NULL,
  `type` varchar(3) NOT NULL COMMENT '类型',
  `typeName` varchar(50) NOT NULL COMMENT '类型名称',
  `jsonStr` varchar(5000) NOT NULL COMMENT 'json数据',
  `uid` varchar(50) NOT NULL COMMENT '用户id',
  `createTime` datetime NOT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for c_user_credit_apply
-- ----------------------------
DROP TABLE IF EXISTS `c_user_credit_apply`;
CREATE TABLE `c_user_credit_apply` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `uid` varchar(20) DEFAULT NULL COMMENT '用户id',
  `uname` varchar(100) DEFAULT NULL COMMENT '用户姓名',
  `auth_amount` bigint(20) DEFAULT NULL COMMENT '授信金额',
  `auth_status` char(1) DEFAULT NULL COMMENT '审核状态',
  `opera_name` varchar(100) DEFAULT NULL COMMENT '审批人',
  `auth_time` datetime DEFAULT NULL COMMENT '审核时间',
  `examine_fail_reason` varchar(100) DEFAULT NULL COMMENT '反馈意见',
  `application_date` datetime DEFAULT NULL COMMENT '申请时间',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提额申请审核记录表';

-- ----------------------------
-- Table structure for c_user_info
-- ----------------------------
DROP TABLE IF EXISTS `c_user_info`;
CREATE TABLE `c_user_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `real_name` varchar(15) DEFAULT NULL COMMENT '用户姓名',
  `mobile` varchar(15) DEFAULT NULL COMMENT '用户手机号码',
  `status` char(1) DEFAULT NULL COMMENT '状态（0-启用 1-禁用）',
  `user_attribute` char(1) DEFAULT NULL COMMENT '用户属性 1.正常用户 2.商户用户',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '最后登录ip',
  `head_img` varchar(100) DEFAULT 'uhead.png' COMMENT '用户头像资源访问路径',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证号',
  `wx_openid` varchar(50) DEFAULT NULL COMMENT '微信公众号openid',
  `wx_unionid` varchar(50) DEFAULT NULL COMMENT '微信唯一id',
  `sex` char(1) DEFAULT NULL COMMENT '性别 1-男 2-女',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消费者用户表';

-- ----------------------------
-- Table structure for c_user_other_info
-- ----------------------------
DROP TABLE IF EXISTS `c_user_other_info`;
CREATE TABLE `c_user_other_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  `uid` varchar(20) NOT NULL COMMENT '用户主键id',
  `bind_card_status` char(1) DEFAULT NULL COMMENT '绑卡状态（0-未绑卡 1-已绑卡）',
  `type` char(1) DEFAULT NULL COMMENT '账户类型 （1-个人 2-企业）',
  `is_marry` char(1) DEFAULT NULL COMMENT '是否结婚',
  `area` varchar(32) DEFAULT NULL COMMENT '地区',
  `address` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `ugren_name` varchar(15) DEFAULT NULL COMMENT '紧急联系人',
  `ugren_mobile` varchar(18) DEFAULT NULL COMMENT '紧急联系人电话',
  `company_name` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `work_time` int(10) DEFAULT NULL COMMENT '工作时长',
  `wages` int(10) DEFAULT NULL COMMENT '工资',
  `company_tel` varchar(20) DEFAULT NULL COMMENT '公司电话',
  `company_position` varchar(10) DEFAULT NULL COMMENT '公司职位',
  `company_type` varchar(10) DEFAULT NULL COMMENT '公司性质',
  `company_address` varchar(50) DEFAULT NULL COMMENT '公司地址',
  `company_area` varchar(32) DEFAULT NULL COMMENT '公司所在位置（如：湖南省长沙市岳麓区）',
  `score` int(10) DEFAULT '0' COMMENT ' 积分数',
  `version_code` int(100) DEFAULT '0' COMMENT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消费者用户其他信息表';

-- ----------------------------
-- Table structure for c_user_ref_con
-- ----------------------------
DROP TABLE IF EXISTS `c_user_ref_con`;
CREATE TABLE `c_user_ref_con` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `uid` varchar(20) NOT NULL COMMENT '用户id',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `mid` varchar(20) NOT NULL COMMENT '绑定商户id',
  `fid` varchar(20) NOT NULL COMMENT '资金端id',
  `auth_status` char(1) NOT NULL COMMENT '授信状态（0-未授信 1-已授信）',
  `auth_max` decimal(10,0) NOT NULL COMMENT '授信额度',
  `auth_date` datetime NOT NULL COMMENT '授信日期',
  `ooa_date` varchar(2) NOT NULL COMMENT '出账日期',
  `repay_date` varchar(2) NOT NULL COMMENT '还款日期',
  `auth_examine` char(1) NOT NULL COMMENT '0-初始状态 1-快速授信成功 2- 快速授信失败 3-提额申请中',
  `examine_fail_reason` varchar(100) DEFAULT NULL COMMENT '最近一次授信审核失败原因（包括快速授信和提额）',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户，运营商，资金端，商户关系表(用于用户捆绑各端)';

-- ----------------------------
-- Table structure for f_fundend
-- ----------------------------
DROP TABLE IF EXISTS `f_fundend`;
CREATE TABLE `f_fundend` (
  `id` varchar(20) NOT NULL COMMENT '资金端id',
  `fname` varchar(50) DEFAULT NULL COMMENT '资金端名称',
  `name` varchar(50) DEFAULT NULL COMMENT '资金端法人姓名',
  `mobile` varchar(11) DEFAULT NULL COMMENT '资金端手机号',
  `address` varchar(255) DEFAULT NULL COMMENT '资金端所在地址',
  `account` varchar(50) DEFAULT NULL COMMENT '资金端账号',
  `pwd` varchar(50) DEFAULT NULL COMMENT '资金端登录密码',
  `cardid` varchar(18) DEFAULT NULL COMMENT '资金端法人身份证',
  `bankcard` varchar(25) DEFAULT NULL COMMENT '资金端银行卡',
  `status` char(1) DEFAULT NULL COMMENT '状态 1：可用，0：不可用',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金端信息表';

-- ----------------------------
-- Table structure for f_fundend_bind
-- ----------------------------
DROP TABLE IF EXISTS `f_fundend_bind`;
CREATE TABLE `f_fundend_bind` (
  `id` varchar(20) NOT NULL COMMENT '资金端绑定表id',
  `fid` varchar(20) DEFAULT NULL COMMENT '资金端id',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商id',
  `status` varchar(10) DEFAULT NULL COMMENT '启用状态 （1：启用，0：不启用 ）',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金端、运营商绑定关系表';

-- ----------------------------
-- Table structure for index_banner
-- ----------------------------
DROP TABLE IF EXISTS `index_banner`;
CREATE TABLE `index_banner` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `sort` int(5) DEFAULT NULL COMMENT '排序号',
  `img` varchar(255) DEFAULT NULL COMMENT 'banner图',
  `href` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `type` char(1) DEFAULT NULL COMMENT '图片类型 1-商户banner 2-商品banner  3-酒店banner 其他为定',
  `link_name` varchar(20) DEFAULT NULL COMMENT '链接名字',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='首页轮播图表';

-- ----------------------------
-- Table structure for job_execution_log
-- ----------------------------
DROP TABLE IF EXISTS `job_execution_log`;
CREATE TABLE `job_execution_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `sharding_item` int(11) NOT NULL,
  `execution_source` varchar(20) NOT NULL,
  `failure_cause` varchar(4000) DEFAULT NULL,
  `is_success` int(11) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `complete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for job_status_trace_log
-- ----------------------------
DROP TABLE IF EXISTS `job_status_trace_log`;
CREATE TABLE `job_status_trace_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `original_task_id` varchar(255) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `slave_id` varchar(50) NOT NULL,
  `source` varchar(50) NOT NULL,
  `execution_type` varchar(20) NOT NULL,
  `sharding_item` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `creation_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TASK_ID_STATE_INDEX` (`task_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for m_hot_search
-- ----------------------------
DROP TABLE IF EXISTS `m_hot_search`;
CREATE TABLE `m_hot_search` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '热门搜索名称',
  `type` char(1) DEFAULT NULL COMMENT '类型 0-商家 1-商品',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商id',
  `order_list` int(10) DEFAULT NULL COMMENT '排序编号 ，越小越排前，order by orderlist asc',
  `rid` varchar(20) DEFAULT NULL COMMENT '资源id，可为商品id也可为商家id，根据type类型而定',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='热搜店铺表';

-- ----------------------------
-- Table structure for m_merchant_category
-- ----------------------------
DROP TABLE IF EXISTS `m_merchant_category`;
CREATE TABLE `m_merchant_category` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `name` varchar(50) DEFAULT NULL COMMENT '行业名称',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '分类父id，顶层为0',
  `grade` int(11) DEFAULT NULL COMMENT '层级',
  `order_list` int(11) DEFAULT NULL COMMENT '排序id',
  `oid` varchar(32) DEFAULT NULL COMMENT '运营商id （保留字段，暂时不用）',
  `char_color` varchar(7) DEFAULT NULL COMMENT '前端界面用户饼图对应颜色',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户行业分类表';

-- ----------------------------
-- Table structure for m_merchant_info
-- ----------------------------
DROP TABLE IF EXISTS `m_merchant_info`;
CREATE TABLE `m_merchant_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商主键id',
  `name` varchar(20) DEFAULT NULL COMMENT '商户名称',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机号码',
  `simple_name` varchar(10) DEFAULT NULL COMMENT '商户简称',
  `legal_name` varchar(10) DEFAULT NULL COMMENT '法人名称',
  `id_card` varchar(18) DEFAULT NULL COMMENT '法人身份证',
  `area` varchar(32) DEFAULT NULL COMMENT '地区',
  `address` varchar(500) DEFAULT NULL COMMENT '详细地址',
  `pwd` varchar(50) DEFAULT NULL COMMENT '商户登录密码',
  `status` char(1) NOT NULL COMMENT '商户状态0-禁用 1-启用 2-待审核',
  `examine_date` datetime DEFAULT NULL COMMENT '审核时间',
  `mic_id` varchar(32) DEFAULT NULL COMMENT '行业类型',
  `lat` varchar(15) DEFAULT NULL COMMENT '商家经纬度',
  `lng` varchar(15) DEFAULT NULL COMMENT '商家经纬度',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户信息表';

-- ----------------------------
-- Table structure for m_merchant_other_info
-- ----------------------------
DROP TABLE IF EXISTS `m_merchant_other_info`;
CREATE TABLE `m_merchant_other_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `mid` varchar(20) NOT NULL COMMENT '商户主键id，唯一',
  `nature` varchar(50) DEFAULT NULL COMMENT '商户性质',
  `oper_year` int(11) DEFAULT NULL COMMENT '品牌经营年限',
  `oper_area` int(11) DEFAULT NULL COMMENT '经营面积（㎡）',
  `reg_money` int(11) DEFAULT NULL COMMENT '注册资金（万）',
  `local_photo` varchar(100) DEFAULT NULL COMMENT '场地照片资源路径（c端显示图片）',
  `license_pic` varchar(100) DEFAULT NULL COMMENT '营业执照图片资源路径',
  `legal_photo` varchar(100) DEFAULT NULL COMMENT '法人身份照片资源路径',
  `normal_card` varchar(100) DEFAULT NULL COMMENT '借记卡照片资源路径',
  `credit_card` varchar(100) DEFAULT NULL COMMENT '信用卡照片资源路径',
  `lagal_face_photo` varchar(100) DEFAULT NULL COMMENT '法人正面照资源路径',
  `settlement_type` char(1) DEFAULT NULL COMMENT '结算方式 0- t+1 , 1 - t+0',
  `auth_max_flag` char(1) DEFAULT NULL COMMENT '授信是否有上限',
  `auth_max` bigint(20) DEFAULT NULL COMMENT '授信上限值',
  `settlement_loop` char(1) DEFAULT NULL COMMENT '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年）',
  `clear_day` int(11) DEFAULT NULL COMMENT '清算日期（结算日）',
  `share_flag` int(1) DEFAULT '1' COMMENT '''运营商抽取商户服务费率（百分之）''',
  `share_fee` int(2) DEFAULT '1' COMMENT '商户让利费率（百分之）',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  `open_date` varchar(255) DEFAULT NULL COMMENT '开店时间（运营商审核的时间）',
  `auth_examine` char(1) DEFAULT '0' COMMENT '0-未提交 1-待审核 2-审核失败 3-审核成功',
  `examine_fail_reason` varchar(100) DEFAULT NULL COMMENT '审核失败原因',
  `phone_number` varchar(15) DEFAULT NULL COMMENT '商家电话',
  `recommend_goods` varchar(255) DEFAULT NULL COMMENT '店铺推荐商品简介',
  `syauth_examine` char(1) DEFAULT NULL COMMENT ' 授信状态 0-未提交 1-待审核 2-审核中 3-审核成功 4审核失败5线下审核',
  `syexamine_fail_reason` varchar(100) DEFAULT NULL COMMENT '授信审核失败原因',
  `sy_date` datetime DEFAULT NULL COMMENT '申请授信时间',
  `zl_status` char(1) DEFAULT NULL COMMENT '商家资料填写状态 0.未完成 1.已完成',
  `ooa_date` varchar(2) DEFAULT NULL COMMENT ' 出账日期',
  `repay_date` varchar(2) DEFAULT NULL COMMENT '还款日期',
  `borrowing_multi_json` longtext COMMENT '多头借贷接口json',
  `borrowing_multi_status` char(1) DEFAULT NULL COMMENT '多头借贷接口状态（0失败 1成功）',
  `investment_tenure_json` longtext COMMENT '个人投资任职接口json',
  `investment_tenure_status` char(1) DEFAULT NULL COMMENT '个人投资任职接口状态（0失败 1成功）',
  `bank_card_json` longtext COMMENT '银行卡接口json',
  `bank_card_status` char(1) DEFAULT NULL COMMENT '银行卡接口状态（0失败 1成功）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户其他信息';

-- ----------------------------
-- Table structure for m_red_packet
-- ----------------------------
DROP TABLE IF EXISTS `m_red_packet`;
CREATE TABLE `m_red_packet` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '优惠券名称',
  `mi_id` varchar(100) DEFAULT NULL COMMENT '所属商户id',
  `money` bigint(20) DEFAULT NULL COMMENT '优惠金额',
  `sub_title` varchar(100) DEFAULT NULL COMMENT '优惠券副标题',
  `full_money` bigint(20) DEFAULT NULL COMMENT '订单金额需要达到该金额才可以使用',
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户id',
  `is_use` char(1) DEFAULT NULL COMMENT '是否使用',
  `end_date` datetime DEFAULT NULL COMMENT '有效期',
  `oid` varchar(20) DEFAULT NULL COMMENT '所属运营商id',
  `modify_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `rps_id` varchar(20) DEFAULT NULL COMMENT '优惠券配置信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户领取优惠券记录表';

-- ----------------------------
-- Table structure for m_red_packet_setting
-- ----------------------------
DROP TABLE IF EXISTS `m_red_packet_setting`;
CREATE TABLE `m_red_packet_setting` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '优惠券名称',
  `mi_id` varchar(20) DEFAULT NULL COMMENT '所属商户id',
  `money` bigint(20) DEFAULT NULL COMMENT '商户设置的原始优惠金额',
  `actual_money` bigint(20) DEFAULT NULL COMMENT '实际优惠金额（运营商抽成后的优惠金额）',
  `op_money` bigint(20) DEFAULT NULL COMMENT '运营商抽取优惠金额(用于三级分销分润)',
  `sub_title` varchar(100) DEFAULT NULL COMMENT '优惠券副标题',
  `full_money` bigint(20) DEFAULT NULL COMMENT '订单金额需要达到该金额才可以使用',
  `num` int(10) DEFAULT NULL COMMENT '每次领取的数量',
  `time_type` int(10) DEFAULT NULL COMMENT '类型 1-固定截止时间 2-可用天数',
  `end_date` datetime DEFAULT NULL COMMENT '有效期',
  `days` int(10) DEFAULT NULL COMMENT '有效期时间',
  `oid` varchar(100) DEFAULT NULL COMMENT '所属运营商id',
  `modify_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` char(1) DEFAULT '1' COMMENT '1 审核中，2：审核通过，3：失效，4：审核不通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券表';

-- ----------------------------
-- Table structure for o_opera_info
-- ----------------------------
DROP TABLE IF EXISTS `o_opera_info`;
CREATE TABLE `o_opera_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `name` varchar(15) DEFAULT NULL COMMENT '运营商名称',
  `pwd` varchar(500) DEFAULT NULL COMMENT '运营商密码',
  `osn` varchar(6) DEFAULT NULL COMMENT '运营商编码 即二级域名',
  `appid` varchar(20) DEFAULT NULL COMMENT '运营商微信公众号appid',
  `appsecret` varchar(32) DEFAULT NULL COMMENT '运营商微信公众号appsecret',
  `status` char(1) DEFAULT NULL COMMENT '运营商状态0-未开通 1-可用 2-禁用',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机号码（默认作为登录帐号）',
  `acount` varchar(15) DEFAULT NULL COMMENT '账号（默认用手机号）',
  `simple_name` varchar(20) NOT NULL COMMENT '运营商简称',
  `legal_name` varchar(10) DEFAULT NULL COMMENT '法人名称',
  `id_card` varchar(18) DEFAULT NULL COMMENT '法人身份证',
  `address` varchar(50) DEFAULT NULL COMMENT '详细地址',
  `area` varchar(32) DEFAULT NULL COMMENT '运营商地区',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营商信息表';

-- ----------------------------
-- Table structure for o_opera_other_info
-- ----------------------------
DROP TABLE IF EXISTS `o_opera_other_info`;
CREATE TABLE `o_opera_other_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商主键id，唯一',
  `oper_year` int(10) DEFAULT NULL COMMENT '品牌经营年限',
  `oper_area` int(10) DEFAULT NULL COMMENT ' 经营面积（㎡）',
  `license_pic` varchar(100) DEFAULT NULL COMMENT '营业执照图片资源路径',
  `normal_card` varchar(100) DEFAULT NULL COMMENT '借记卡照片资源路径',
  `platfrom_share_ratio` int(2) DEFAULT NULL COMMENT '''平台抽取运营商服务费率（百分之）''',
  `legal_photo` varchar(100) DEFAULT NULL COMMENT '场地照片资源路径',
  `credit_card` varchar(100) DEFAULT NULL COMMENT ' 信用卡照片资源路径',
  `nature` varchar(100) DEFAULT NULL COMMENT '运营商性质',
  `local_photo` varchar(100) DEFAULT NULL COMMENT '场地照片资源路径',
  `reg_money` bigint(10) DEFAULT NULL COMMENT '注册资金（万）',
  `lagal_face_photo` varchar(100) DEFAULT NULL COMMENT '法人正面照资源路径',
  `message_fee` bigint(20) DEFAULT NULL COMMENT '短信费用/条',
  `score_percent` int(5) DEFAULT '0' COMMENT '积分返利比例 %（根据消费金额获取积分）',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `ooa_date` varchar(2) DEFAULT NULL COMMENT '出账日',
  `repay_date` varchar(2) DEFAULT NULL COMMENT '还款日',
  `type` int(1) DEFAULT '0' COMMENT '修改出账日和还款日的状态 0未修改 1以修改',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营商其他信息表';

-- ----------------------------
-- Table structure for p_admin
-- ----------------------------
DROP TABLE IF EXISTS `p_admin`;
CREATE TABLE `p_admin` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `status` char(1) DEFAULT NULL COMMENT '状态(0:启用 1:停用 )',
  `reg_email` varchar(100) DEFAULT NULL COMMENT '注册邮箱',
  `head_uri` varchar(100) DEFAULT NULL COMMENT '管理员头像',
  `admin_name` varchar(100) DEFAULT NULL COMMENT '用户姓名',
  `pwd` varchar(500) DEFAULT NULL COMMENT '密码,md5加密',
  `user_name` varchar(100) DEFAULT NULL COMMENT '帐号',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  `type` int(1) DEFAULT NULL COMMENT '1-默认管理员 0-普通管理员',
  `aid` varchar(20) DEFAULT NULL COMMENT '默认管理员id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台用户信息表';

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `area_code` varchar(100) NOT NULL COMMENT '区域编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(100) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(100) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `area_name` varchar(100) NOT NULL COMMENT '区域名称',
  `area_type` char(1) DEFAULT NULL COMMENT '区域类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `city_level` char(4) DEFAULT NULL COMMENT '城市级别',
  PRIMARY KEY (`area_code`),
  KEY `idx_sys_area_pc` (`parent_code`),
  KEY `idx_sys_area_ts` (`tree_sort`),
  KEY `idx_sys_area_status` (`status`),
  KEY `idx_sys_area_pcs` (`parent_codes`),
  KEY `idx_sys_area_tss` (`tree_sorts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行政区划';

-- ----------------------------
-- Table structure for tb_account_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_info`;
CREATE TABLE `tb_account_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) NOT NULL COMMENT '资源主键id（用户id或者商户id）',
  `utype` char(1) DEFAULT NULL COMMENT '所属用户类型（1-用户 2-商户）',
  `auth_type` char(1) DEFAULT NULL COMMENT '授信类型（1-消费者授信 2-商户授信）',
  `balance` varchar(500) DEFAULT NULL COMMENT '授信可用余额（des256加密）',
  `frezz_money` varchar(500) DEFAULT NULL COMMENT '冻结金额（des256加密）',
  `version_code` int(10) DEFAULT NULL COMMENT '资金版本号（乐观锁）',
  `max_money` varchar(500) DEFAULT NULL COMMENT '授信最大限额（des256加密）',
  `is_freeze` char(1) DEFAULT NULL COMMENT '授信是否冻结',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户授信资金额度表';

-- ----------------------------
-- Table structure for tb_account_trading_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_trading_log`;
CREATE TABLE `tb_account_trading_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `aid` varchar(20) NOT NULL COMMENT '资金账户主键id',
  `direction` int(11) DEFAULT NULL COMMENT '收支类型（0-支出 1-收入）',
  `biz_type` varchar(20) DEFAULT NULL COMMENT '业务类型编码',
  `amount` bigint(20) DEFAULT NULL COMMENT '金额',
  `balance` bigint(20) DEFAULT NULL COMMENT '账户结余',
  `res_id` varchar(20) DEFAULT NULL COMMENT '资源id（交易明细id、还款计划id等）',
  `create_date` datetime NOT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信资金流水';

-- ----------------------------
-- Table structure for tb_area
-- ----------------------------
DROP TABLE IF EXISTS `tb_area`;
CREATE TABLE `tb_area` (
  `id` varchar(100) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '地区名称',
  `path` varchar(100) DEFAULT NULL COMMENT '层级路径',
  `display_name` varchar(100) DEFAULT NULL COMMENT '地区完整名称',
  `parent_id` varchar(100) DEFAULT NULL COMMENT '上级地区id',
  `grade` int(10) DEFAULT NULL COMMENT '层级',
  `order_list` int(10) DEFAULT NULL COMMENT '排序id',
  `modify_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `create_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区表';

-- ----------------------------
-- Table structure for tb_bind_card_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_bind_card_info`;
CREATE TABLE `tb_bind_card_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) NOT NULL COMMENT '资源主键id（用户id或者商户id或运营商id或资金端id）',
  `utype` char(1) DEFAULT NULL COMMENT '所属用户类型（1-用户 2-商户 3-运营商 4-资金端）',
  `card_type` char(1) DEFAULT NULL COMMENT '卡片类型（0-储蓄卡 ）',
  `bank_sn` varchar(50) DEFAULT NULL COMMENT '银行卡编码',
  `mobile` varchar(15) DEFAULT NULL COMMENT '持卡人手机号',
  `name` varchar(15) DEFAULT NULL COMMENT '持卡人姓名',
  `id_card` varchar(20) DEFAULT NULL COMMENT '持卡人证件号',
  `bank_card` varchar(20) DEFAULT NULL COMMENT '银行卡号',
  `vali_date` varchar(4) DEFAULT NULL COMMENT '有效期',
  `cvv` varchar(4) DEFAULT NULL COMMENT 'cvv码',
  `is_default` char(5) DEFAULT NULL COMMENT '是否为主卡',
  `bank_name` varchar(50) DEFAULT NULL COMMENT '开户银行',
  `contract_id` varchar(32) DEFAULT NULL COMMENT '支付通道协议号',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='绑卡信息';

-- ----------------------------
-- Table structure for tb_improve_quota
-- ----------------------------
DROP TABLE IF EXISTS `tb_improve_quota`;
CREATE TABLE `tb_improve_quota` (
  `id` varchar(20) NOT NULL,
  `uid` varchar(20) NOT NULL,
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `sesame_credit_json` longtext COMMENT '芝麻信用接口信息',
  `sesame_credit_status` char(1) DEFAULT NULL COMMENT '芝麻信用接口状态',
  `credit_card_json` longtext COMMENT '信用卡接口信息',
  `credit_card_status` char(1) DEFAULT NULL COMMENT '信用卡接口状态',
  `student_info_json` longtext COMMENT '学信接口信息',
  `student_info_status` char(1) DEFAULT NULL COMMENT '学信接口状态',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `yys_json` longtext COMMENT '电话运营商接口（6个月同行记录信息）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消费者用户提额授信结果表';

-- ----------------------------
-- Table structure for tb_message_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_message_log`;
CREATE TABLE `tb_message_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) DEFAULT NULL COMMENT '商户id所属运营商(短信归属运营商)',
  `content` varchar(255) DEFAULT NULL COMMENT '消息内容',
  `mid` varchar(32) DEFAULT NULL COMMENT '接收消息的商户id',
  `uid` varchar(32) DEFAULT NULL COMMENT '接收消息的用户id',
  `type` varchar(20) DEFAULT NULL COMMENT '1-短信 2-站内信',
  `biz_type` varchar(10) DEFAULT NULL COMMENT '短信业务类型（如注册、登录、体现、还款、授信结果等）',
  `fee` bigint(20) DEFAULT NULL COMMENT '短信费用',
  `ip` varchar(20) DEFAULT NULL COMMENT '请求发送的ip地址',
  `mobile` varchar(15) DEFAULT NULL COMMENT '用户目标电话',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信记录表';

-- ----------------------------
-- Table structure for tb_message_template
-- ----------------------------
DROP TABLE IF EXISTS `tb_message_template`;
CREATE TABLE `tb_message_template` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `content` varchar(5000) DEFAULT NULL COMMENT '内容',
  `utype` varchar(100) DEFAULT NULL COMMENT '标识字段',
  `explan` varchar(5000) DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信模板表';

-- ----------------------------
-- Table structure for tb_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_operation_log`;
CREATE TABLE `tb_operation_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `desc_info` longtext COMMENT '操作内容',
  `type` varchar(20) DEFAULT NULL COMMENT '操作类型',
  `operation_user` varchar(32) DEFAULT NULL COMMENT '操作用户（商户id、用户id、管理员id、运营商管理员id）',
  `user_type` char(1) DEFAULT NULL COMMENT '操作用户类型（0-商户 1-用户 2-平台管理员 3-运营商管理员）',
  `operation_ip` varchar(20) DEFAULT NULL COMMENT '操作ip地址',
  `operation_area` varchar(15) DEFAULT NULL COMMENT '操作地区名称',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `request_param` longtext COMMENT '请求参数',
  `create_date` datetime NOT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志表';

-- ----------------------------
-- Table structure for tb_payment
-- ----------------------------
DROP TABLE IF EXISTS `tb_payment`;
CREATE TABLE `tb_payment` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `type` char(1) DEFAULT NULL,
  `mi_id` varchar(20) DEFAULT NULL COMMENT '所属商户id',
  `status` char(1) DEFAULT NULL COMMENT '支付状态 0-待支付 1-支付成功',
  `oi_id` varchar(20) DEFAULT NULL COMMENT ' 订单编号',
  `payment_sn` varchar(32) DEFAULT NULL COMMENT '支付编号',
  `money` bigint(20) DEFAULT NULL COMMENT '支付金额',
  `oid` varchar(20) DEFAULT NULL COMMENT '所属运营商id',
  `modify_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL COMMENT '支付产品类型',
  `pay_date` datetime DEFAULT NULL COMMENT '支付时间',
  `openid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付记录表';

-- ----------------------------
-- Table structure for tb_people_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_people_role`;
CREATE TABLE `tb_people_role` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `people_user_type` char(1) DEFAULT NULL COMMENT '用户类型1：商户，2：运营商，3：平台',
  `people_user_id` varchar(20) DEFAULT NULL COMMENT '用户id,用户表主键',
  `role_id` varchar(20) DEFAULT NULL COMMENT '角色id,角色表主键',
  `create_date` datetime DEFAULT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色关系表';

-- ----------------------------
-- Table structure for tb_repayment_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_repayment_detail`;
CREATE TABLE `tb_repayment_detail` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `mid` varchar(20) DEFAULT NULL COMMENT '商户id',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `uid` varchar(20) NOT NULL COMMENT '用户id',
  `utype` char(1) DEFAULT NULL COMMENT '用户类型：1-消费者，2-商户',
  `fid` varchar(20) NOT NULL COMMENT '资金端id',
  `total_times` int(11) DEFAULT NULL COMMENT '还款总期次',
  `current_times` int(11) DEFAULT NULL COMMENT '当前还款期次',
  `total_corpus` bigint(20) NOT NULL COMMENT '还款总本金',
  `total_fee` bigint(20) NOT NULL COMMENT '还款总利息',
  `total_late_fee` bigint(20) DEFAULT NULL COMMENT '滞纳金总额（产生滞纳金后累计到此处）',
  `repayment_sn` varchar(30) DEFAULT NULL COMMENT '还款详情流水号',
  `status` char(1) DEFAULT NULL COMMENT '还款状态 0-待还 1-已还',
  `repay_corpus` bigint(20) DEFAULT NULL COMMENT '已还本金（如果分期则累计）',
  `repay_fee` bigint(20) DEFAULT NULL COMMENT '已还利息（如果分期则累计）',
  `repay_late_fee` bigint(20) DEFAULT NULL COMMENT ' 已还滞纳金（如果分期则累计）',
  `out_date` varchar(2) DEFAULT NULL COMMENT '出账日（运营商指定的出账日）',
  `repay_date` datetime DEFAULT NULL COMMENT '还款日期',
  `loop_start` datetime DEFAULT NULL COMMENT '账期开始时间',
  `loop_end` datetime DEFAULT NULL COMMENT '账期结束时间',
  `rate` int(2) DEFAULT NULL COMMENT '月利率（根据月利率算利息）',
  `is_split` char(1) DEFAULT NULL COMMENT '是否分期（当用收到账单后，选择是否分期，绝对此字段）',
  `service_fee_rate` int(2) DEFAULT NULL COMMENT '分期手续费利率',
  `service_fee` bigint(20) DEFAULT NULL COMMENT '分期手续费',
  `ip` varchar(18) DEFAULT NULL COMMENT '操作分期时候的ip地址',
  `split_date` datetime DEFAULT NULL COMMENT '分期日期',
  `show_date` datetime DEFAULT NULL COMMENT '在账单中显示的时间',
  `create_date` datetime NOT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='还款表';

-- ----------------------------
-- Table structure for tb_repayment_plan
-- ----------------------------
DROP TABLE IF EXISTS `tb_repayment_plan`;
CREATE TABLE `tb_repayment_plan` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `uid` varchar(20) NOT NULL COMMENT '用户id',
  `utype` char(1) DEFAULT NULL COMMENT '用户类型：1-消费者，2-商户',
  `total_times` int(11) DEFAULT NULL COMMENT '还款总期次',
  `current_times` int(11) DEFAULT NULL COMMENT '当前还款期次',
  `total_corpus` bigint(20) NOT NULL COMMENT '还款总本金',
  `total_fee` bigint(20) NOT NULL COMMENT '还款总利息',
  `rest_corpus` bigint(20) DEFAULT NULL COMMENT '剩余还款本金',
  `rest_fee` bigint(20) DEFAULT NULL COMMENT '剩余还款利息',
  `current_corpus` bigint(20) DEFAULT NULL COMMENT '当期还款本金',
  `current_fee` bigint(20) DEFAULT NULL COMMENT '当期还款利息',
  `late_fee` bigint(20) DEFAULT NULL COMMENT '当期逾期滞纳金  每日逾期滞纳金 = （当期还款本金+当期还款利息+逾期滞纳金）* 滞纳金费率',
  `repayment_date` datetime DEFAULT NULL COMMENT '还款日',
  `process_date` datetime DEFAULT NULL COMMENT '用户实际进行还款的日期',
  `rd_id` varchar(20) DEFAULT NULL COMMENT '还款详情id',
  `serial_no` varchar(30) DEFAULT NULL COMMENT '当前分期还款流水号（r+还款日期（8位）+2位运营商码+4位随机码+主机编号（4位） 长度 19位）',
  `status` char(1) DEFAULT NULL COMMENT '状态 1-已还 2-未还',
  `repayment_type` char(1) DEFAULT NULL COMMENT '还款方式 1-网银 2-微信 3-支付宝',
  `yq_rate` decimal(11,2) DEFAULT NULL COMMENT '逾期利率',
  `loop_start` datetime DEFAULT NULL,
  `loop_end` datetime DEFAULT NULL,
  `show_date` datetime DEFAULT NULL COMMENT '分期纪录，在账单显示的日期',
  `call_repay_status` char(2) NOT NULL DEFAULT '-1' COMMENT '催收狀態 -1 不催收 0-短信催收 1-电话催收 2-上门催收',
  `create_date` datetime NOT NULL COMMENT '纪录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '纪录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='还款计划表';

-- ----------------------------
-- Table structure for tb_repayment_ratio_dict
-- ----------------------------
DROP TABLE IF EXISTS `tb_repayment_ratio_dict`;
CREATE TABLE `tb_repayment_ratio_dict` (
  `id` varchar(20) NOT NULL,
  `oid` varchar(20) NOT NULL COMMENT '运营端id',
  `fid` varchar(20) NOT NULL COMMENT '资金端id',
  `yq_rate` int(2) NOT NULL COMMENT '逾期还款费率（百分比）',
  `service_fee` int(2) NOT NULL COMMENT '手续费利率（分期才有手续费）',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  `create_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='还款比率字典表';

-- ----------------------------
-- Table structure for tb_repay_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_repay_log`;
CREATE TABLE `tb_repay_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `payment_type` varchar(100) DEFAULT NULL COMMENT '还款-支付类型',
  `amount` bigint(20) DEFAULT NULL COMMENT '还款-支付金额',
  `status` char(1) DEFAULT NULL COMMENT '还款-支付状态',
  `uid` varchar(20) DEFAULT NULL,
  `utype` char(1) DEFAULT NULL COMMENT '用户类型：1-消费者，2-商户',
  `pay_date` datetime DEFAULT NULL COMMENT '还款-成功支付时间',
  `repay_sn` varchar(100) DEFAULT NULL COMMENT '还款-支付编号(对应还款计划表中的流水号)',
  `bill_date` varchar(100) DEFAULT NULL COMMENT '还款-账单年月',
  `tids` longtext COMMENT '如果是提前还款，此处存储所有的交易明细id',
  `type` char(1) DEFAULT NULL COMMENT '还款类型 1-账单还款 2-提前还款',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='还款记录表';

-- ----------------------------
-- Table structure for tb_resources
-- ----------------------------
DROP TABLE IF EXISTS `tb_resources`;
CREATE TABLE `tb_resources` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `resource_name` varchar(100) DEFAULT NULL COMMENT '资源名称',
  `resource_type` varchar(1) DEFAULT NULL COMMENT '资源类型(1:模块 2:普通菜单 )',
  `resource_url` varchar(100) DEFAULT NULL COMMENT '资源路径',
  `img_url` varchar(100) DEFAULT NULL COMMENT '图标路径',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '父id',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  `type` int(1) DEFAULT NULL COMMENT '1-平台端 2-运营商端 3-资金端',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源信息表';

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `utype` char(1) DEFAULT NULL COMMENT '用户类型1：商户，2：运营商，3：平台',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `uid` varchar(20) DEFAULT NULL COMMENT '用户的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Table structure for tb_role_resources
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_resources`;
CREATE TABLE `tb_role_resources` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `role_id` varchar(20) DEFAULT NULL COMMENT '角色id,角色表主键',
  `resource_id` varchar(20) DEFAULT NULL COMMENT '资源id,资源 表主键',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色资源关系表';

-- ----------------------------
-- Table structure for tb_score_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_score_log`;
CREATE TABLE `tb_score_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商id',
  `biz_type` varchar(100) DEFAULT NULL COMMENT '业务类型',
  `direction` char(1) DEFAULT NULL COMMENT '收支类型 0-支付 1-收入',
  `uid` varchar(20) DEFAULT NULL COMMENT '用户id',
  `descinfo` varchar(100) DEFAULT NULL COMMENT '收支类型 0-支付 1-收入',
  `score` int(10) DEFAULT NULL COMMENT '获得积分',
  `modify_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户积分明细表';

-- ----------------------------
-- Table structure for tb_settlement_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_settlement_detail`;
CREATE TABLE `tb_settlement_detail` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `mid` varchar(20) DEFAULT NULL COMMENT '结算对应商户主键id',
  `oid` varchar(20) NOT NULL COMMENT '结算商户对应运营商id',
  `fid` varchar(20) NOT NULL COMMENT '资金方id',
  `settlement_time` datetime DEFAULT NULL COMMENT '结算时间',
  `status` char(1) DEFAULT NULL COMMENT '结算状态 （0-待确认 1-待结算 2-已结算）',
  `amount` bigint(20) NOT NULL COMMENT '实际结算总金额',
  `share_fee` bigint(20) DEFAULT NULL COMMENT '运营商收取商户让利总费用+三级分销费用（根据分润表统计）',
  `serial_no` varchar(30) DEFAULT NULL COMMENT '结算单号即流水号',
  `out_settlement_date` datetime DEFAULT NULL COMMENT '结算单出账时间(对应商户的清算日期)',
  `loop_start` datetime DEFAULT NULL COMMENT '结算的交易记录开始时间',
  `loop_end` datetime DEFAULT NULL COMMENT '结算的交易记录结束时间',
  `role_stime` datetime DEFAULT NULL,
  `platform_share_fee` bigint(20) DEFAULT NULL COMMENT '平台收取运营商通道总费用（根据分润表统计）',
  `discount_fee` bigint(20) DEFAULT NULL COMMENT '运营商抽取的折扣总费用（根据分润表统计）',
  `oper_status` char(1) DEFAULT NULL COMMENT '运营商结算状态 （0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）',
  `plat_status` char(1) DEFAULT NULL COMMENT '平台结算状态 （0-待确认 1-待结算 2-已结算 3-未发送 4-未生成 5-遗漏交易）',
  `create_date` datetime NOT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '记录修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' 交易结算表';

-- ----------------------------
-- Table structure for tb_settlement_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_settlement_log`;
CREATE TABLE `tb_settlement_log` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `payment_type` bigint(20) DEFAULT NULL COMMENT '结算记录-支付类型',
  `settlement_sn` varchar(100) DEFAULT NULL COMMENT '结算记录-支付编号',
  `oid` varchar(20) DEFAULT NULL COMMENT '结算记录-运营商id',
  `amount` bigint(20) DEFAULT NULL COMMENT '结算记录-结算金额',
  `status` char(1) DEFAULT NULL COMMENT '结算记录-支付状态',
  `pay_date` datetime DEFAULT NULL COMMENT ' 结算记录-成功支付时间',
  `sid` varchar(20) DEFAULT NULL COMMENT '结算记录-结算单id',
  `repay_sn` varchar(100) DEFAULT NULL COMMENT '结算记录-支付编号',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' 结算支付记录表';

-- ----------------------------
-- Table structure for tb_three_sales_rate
-- ----------------------------
DROP TABLE IF EXISTS `tb_three_sales_rate`;
CREATE TABLE `tb_three_sales_rate` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `oid` varchar(20) NOT NULL COMMENT '运营商ID',
  `mid` varchar(20) DEFAULT NULL COMMENT '商户id',
  `redirect_rate` int(2) NOT NULL COMMENT '直接上级分润比率',
  `indirect_rate` int(2) NOT NULL COMMENT '间接上级分润比率',
  `opa_rate` bigint(20) NOT NULL COMMENT '运营商抽取三级分销分润比例（redirect_rate、indirect_rate、opa_rate三者之和为100%）',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `MODIFY_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='三级分销利润分配比例表';

-- ----------------------------
-- Table structure for tb_three_sales_ref
-- ----------------------------
DROP TABLE IF EXISTS `tb_three_sales_ref`;
CREATE TABLE `tb_three_sales_ref` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `user_id` varchar(20) NOT NULL COMMENT '三级用户id',
  `user_type` char(1) NOT NULL COMMENT ' 用户类型（1-商户，2-消费者）',
  `parent_user_id` bigint(20) NOT NULL COMMENT '上级用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='三级分销用户关系绑定表';

-- ----------------------------
-- Table structure for tb_three_sale_profit_share
-- ----------------------------
DROP TABLE IF EXISTS `tb_three_sale_profit_share`;
CREATE TABLE `tb_three_sale_profit_share` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `redirect_user_id` varchar(20) NOT NULL COMMENT '直接上级userId',
  `redirect_user_type` char(1) NOT NULL COMMENT '直接上级userType',
  `redirect_share_amount` bigint(20) NOT NULL COMMENT '直接上级利润分配金额',
  `inderect_user_id` varchar(20) NOT NULL COMMENT '间接上级userid',
  `inderect_user_type` char(1) NOT NULL COMMENT '间接上级userType',
  `inderect_share_amount` bigint(20) NOT NULL COMMENT '间接上级利润分配金额',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `opa_share_amount` bigint(20) NOT NULL COMMENT '运营商抽取三级分销利润金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='三级分销利润分配表';

-- ----------------------------
-- Table structure for tb_trade_profit_share
-- ----------------------------
DROP TABLE IF EXISTS `tb_trade_profit_share`;
CREATE TABLE `tb_trade_profit_share` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `trade_id` varchar(20) DEFAULT NULL COMMENT '消费交易id',
  `trade_type` char(1) NOT NULL COMMENT '交易类型（0-授信消费 1-非授信消费）',
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `mid` varchar(20) NOT NULL COMMENT '商户id',
  `total_trade_amount` bigint(20) NOT NULL COMMENT '交易总金额',
  `merchant_inhome_amount` bigint(20) NOT NULL COMMENT '交易商户入账金额',
  `operator_share_fee` bigint(20) NOT NULL COMMENT '运营商交易抽成费',
  `operator_discount_fee` bigint(20) NOT NULL COMMENT '三级分销总金额',
  `operator_discount_ratio` bigint(20) NOT NULL COMMENT '用于三级分销的商户让利折扣比例',
  `is_use_discout` char(1) NOT NULL COMMENT '是否使用优惠券',
  `three_sale_share_id` bigint(20) NOT NULL COMMENT '三级分销利润分配id',
  `platfrom_share_fee` bigint(20) NOT NULL COMMENT '平台抽成运营商交易费用（按交易总金额百分比抽成）',
  `platfrom_share_ratio` bigint(20) NOT NULL COMMENT '平台抽成运营商交易费用比率（按交易总金额百分比）',
  `is_settlement` char(1) NOT NULL COMMENT '授信消费是否已结算 0:未结算，1：已结算',
  `sid` varchar(20) NOT NULL,
  `trading_date` datetime DEFAULT NULL COMMENT '交易记录时间',
  `create_date` datetime NOT NULL COMMENT '记录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '结算时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交易利润分配表';

-- ----------------------------
-- Table structure for tb_trading_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_trading_detail`;
CREATE TABLE `tb_trading_detail` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `serial_no` varchar(30) DEFAULT NULL COMMENT '业务单号 (t+年月日时分秒（14位）+2位运营商码+4位随机码+主机编号（4位） 长度 25位)',
  `mid` varchar(20) NOT NULL COMMENT '商户id',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商id',
  `uid` varchar(20) NOT NULL COMMENT '用户id',
  `utype` char(1) DEFAULT NULL COMMENT '用户类型：1-消费者，2-商户',
  `trading_type` char(1) DEFAULT NULL COMMENT '交易类型（0-授信消费 1-非授信消费）',
  `payment_status` char(1) DEFAULT NULL COMMENT '支付状态（0-未支付 1-已支付）',
  `trade_amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '交易原始总金额',
  `discount_amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '优惠金额（消费者使用优惠券）',
  `actual_amount` bigint(20) NOT NULL COMMENT '实际交易总金额',
  `trading_date` datetime DEFAULT NULL COMMENT '交易时间',
  `rdid` bigint(20) DEFAULT NULL COMMENT ' 所属还款主键id',
  `ip` varchar(18) DEFAULT NULL COMMENT '交易地点ip',
  `is_use_discout` char(1) DEFAULT NULL COMMENT '是否使用优惠券',
  `rp_id` varchar(20) DEFAULT NULL COMMENT '优惠券id',
  `rid` varchar(20) DEFAULT NULL COMMENT '提前还款纪录id',
  `is_settlement` bigint(20) NOT NULL COMMENT '授信消费是否已结算 0:未结算，1：已结算',
  `sid` varchar(20) DEFAULT NULL COMMENT '结算单id（生成结算单时插入此字段）',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户交易记录表';

-- ----------------------------
-- Table structure for tb_user_account
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_account`;
CREATE TABLE `tb_user_account` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `account` bigint(20) NOT NULL COMMENT '登录账号（手机号）',
  `mobile` bigint(20) NOT NULL COMMENT '手机号',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `tran_pwd` varchar(50) NOT NULL COMMENT '支付密码',
  `user_type_id` bigint(20) NOT NULL COMMENT '用户类型id',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户账户表（包含消费者、商户）';

-- ----------------------------
-- Table structure for tb_user_account_type_con
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_account_type_con`;
CREATE TABLE `tb_user_account_type_con` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `acct_id` varchar(20) NOT NULL COMMENT '账户id',
  `user_type` char(1) NOT NULL COMMENT '用户类型（1：消费者，2商户）',
  `con_id` varchar(20) NOT NULL COMMENT ' 关联id （如果user_type=1,则为c_user_info表的id ，否则为m_merchant_info的id）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户类型表';

-- ----------------------------
-- Table structure for tb_wallet
-- ----------------------------
DROP TABLE IF EXISTS `tb_wallet`;
CREATE TABLE `tb_wallet` (
  `id` varchar(20) NOT NULL,
  `uid` varchar(20) DEFAULT NULL COMMENT '用户id（如果用户是消费者或商户，则此id为tb_user_account表对应的主键id）',
  `card_num` varchar(30) DEFAULT NULL COMMENT '用户钱包卡号',
  `total_amount` bigint(20) DEFAULT NULL COMMENT '总金额,单位（分）',
  `utype` char(1) DEFAULT NULL COMMENT '用户类型1：用户，2：商户，3：运营商，4：资金端',
  `uname` varchar(10) DEFAULT NULL COMMENT '用户名字',
  `umobil` varchar(20) DEFAULT NULL COMMENT '用户手机',
  `stuta` int(5) DEFAULT NULL COMMENT '用户状态',
  `withdraw_fee_flag` char(1) DEFAULT NULL COMMENT '提现续费开关',
  `sxje` bigint(20) DEFAULT NULL COMMENT '手续费',
  `wname` varchar(30) DEFAULT NULL COMMENT '钱包名称(用户姓名or商户名称or运营商or资金端)',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金钱包表';

-- ----------------------------
-- Table structure for tb_wallet_income
-- ----------------------------
DROP TABLE IF EXISTS `tb_wallet_income`;
CREATE TABLE `tb_wallet_income` (
  `id` varchar(20) NOT NULL,
  `wid` varchar(20) DEFAULT NULL COMMENT '钱包id（收入、支出）',
  `playid` varchar(50) DEFAULT NULL COMMENT '资金来源用户id（收入、支出）',
  `ptepy` varchar(10) DEFAULT NULL COMMENT '用户类型1：消费者用户，2：商户，3：运营商，4：资金端',
  `type` int(2) DEFAULT NULL COMMENT '收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出',
  `pname` varchar(10) DEFAULT NULL COMMENT '用户名字',
  `pmobile` varchar(20) DEFAULT NULL COMMENT '用户手机',
  `trade_total_amount` bigint(20) DEFAULT NULL COMMENT '交易总金额',
  `amount` bigint(20) DEFAULT NULL COMMENT '出、入金总额',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `tid` varchar(20) DEFAULT NULL COMMENT '交易记录id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金钱包收支明细表';

-- ----------------------------
-- Table structure for tb_wallet_withdrawals
-- ----------------------------
DROP TABLE IF EXISTS `tb_wallet_withdrawals`;
CREATE TABLE `tb_wallet_withdrawals` (
  `id` varchar(20) NOT NULL,
  `uid` varchar(20) DEFAULT NULL COMMENT '提现用户id',
  `uname` varchar(30) DEFAULT NULL COMMENT '提现用户姓名',
  `umobile` varchar(30) DEFAULT NULL COMMENT '提现用户手机',
  `khh` varchar(30) DEFAULT NULL COMMENT '提现用户开户行',
  `bank_card_no` varchar(30) DEFAULT NULL COMMENT '提现用户银行卡号',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `arrive_time` datetime DEFAULT NULL COMMENT '到账时间',
  `tx_amount` bigint(20) DEFAULT NULL COMMENT '提现金额',
  `dz_amount` bigint(20) DEFAULT NULL COMMENT '到账金额',
  `service_fee` bigint(20) DEFAULT NULL COMMENT '手续费',
  `wallet_balance` bigint(20) DEFAULT NULL COMMENT '钱包余额',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `state` char(1) DEFAULT NULL COMMENT '交易状态（1审核中 2提现成功 3提现失败）',
  `wname` varchar(30) DEFAULT NULL COMMENT '提现钱包名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金钱包提现表';


/* 修改还款表 还款时间修改为dateTime*/ 
ALTER TABLE tb_repayment_detail MODIFY repay_date dateTime default NULL comment '还款日期';

/* 删除表tb_trade_profit_share列名为sid  */
alter table tb_trade_profit_share drop column sid;

/* 在表o_opera_other_info添加结算周期字段  */
alter table o_opera_other_info add settlement_loop char(1) not null comment '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年）';

/* 在表o_opera_other_info 修改platfrom_share_ratio不为空  */
alter table o_opera_other_info modify platfrom_share_ratio int(2) not null default 0 comment '平台抽取运营商服务费率';

/* 在表c_user_info 添加邀请码字段(invate_code)  */
alter table c_user_info add invate_code varchar(32) not null comment '邀请码';

/* 修改tb_user_account 中account、mobile、user_type_id类型为varchar */
ALTER TABLE tb_user_account MODIFY account varchar(20) not null comment '登录账号（手机号）';
ALTER TABLE tb_user_account MODIFY mobile varchar(20) not null comment '手机号';
ALTER TABLE tb_user_account MODIFY user_type_id varchar(20) not null comment '用户类型id';

/* 修改tb_three_sales_rate 中opa_rate类型为int(2)以及注释改为:'运营商抽取三级分销分润比例' */
ALTER TABLE tb_three_sales_rate MODIFY opa_rate int(2) not null comment '运营商抽取三级分销分润比例';

/* 在表c_user_info 修改邀请码字段(invate_code)默认为空  */
alter table c_user_info MODIFY invate_code varchar(32) default null comment '邀请码';

/* 删除表m_merchant_other_info列名为share_flag  */
alter table m_merchant_other_info drop column share_flag;

/* 在表tb_settlement_detail 添加:运营商收取商户通道费用字段(opera_share_fee)  */
alter table tb_settlement_detail add opera_share_fee  bigint(20) not null comment '运营商收取商户通道费用';

/* 在表tb_settlement_detail 修改share_fee 的注释:运营商用于三级分销总费用*/
alter table tb_settlement_detail modify share_fee  bigint(20) not null  comment '运营商用于三级分销总费用';


/* 在表tb_trade_profit_share 添加:运营商收取商户通道费用字段(opera_share_fee)  */
alter table tb_trade_profit_share add opera_share_fee  bigint(20) not null comment '运营商收取商户通道费用';

/*==============================================================*/
/* Table: tb_user_sharefee_settlement                                 */
/*==============================================================*/
DROP TABLE IF EXISTS tb_user_sharefee_settlement;
create table tb_user_sharefee_settlement 
(
   id                   bigint not null,
   uid                  bigint not null comment '用户id',
   settlement_id        varchar(20) not null comment '结算id',
   total_money          bigint(20)  not null comment '结算分润总金额',
   primary key (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户分润结算表';

alter table tb_user_sharefee_settlement MODIFY id varchar(20) not null comment '主键id';
alter table tb_user_sharefee_settlement MODIFY uid varchar(20) not null comment '用户id';
alter table tb_user_sharefee_settlement add is_settlement char(1) not null COMMENT '是否结算(1:已结算 0:待结算)';

/*==============================================================*/
/* Table: tb_user_sharefee_detail                                 */
/*==============================================================*/
DROP TABLE IF EXISTS tb_user_sharefee_details;
create table tb_user_sharefee_details 
(
   id                   bigint not null,
   uid                  bigint not null comment '用户id',
   trade_id		bigint not null comment '交易id',	
   share_money          bigint(20)  not null comment '分润金额',
   primary key (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户分润明细表';

alter table tb_user_sharefee_details MODIFY id varchar(20) not null comment '主键id';
alter table tb_user_sharefee_details MODIFY uid varchar(20) not null comment '用户id';
alter table tb_user_sharefee_details MODIFY trade_id varchar(20) not null comment '交易id';

alter table tb_user_sharefee_details add create_time datetime DEFAULT null COMMENT '创建时间';
alter table tb_user_sharefee_details add modify_time datetime DEFAULT null COMMENT '修改时间';
alter table tb_user_sharefee_details add is_settlement char(1) not null COMMENT '是否结算';

/*==============================================================*/
/* Table: tb_collect                                            */
/*==============================================================*/
DROP TABLE IF EXISTS tb_collect;
create table tb_collect 
(
   id                   varchar(20) not null,
   uid                  varchar(20) not null comment '当前登陆的id',
   mid		        varchar(20) not null comment '商户id',	
   create_date          datetime DEFAULT NULL COMMENT '创建时间',
   primary key (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的收藏表';


/* 在表c_user_ref_con 修改字段类型  */
alter table c_user_ref_con MODIFY mid varchar(20) DEFAULT NULL comment '绑定商户id';
alter table c_user_ref_con MODIFY fid varchar(20) DEFAULT NULL comment '资金端id';
alter table c_user_ref_con MODIFY auth_max decimal(10,0) DEFAULT NULL COMMENT '授信额度';
alter table c_user_ref_con MODIFY auth_date datetime DEFAULT NULL COMMENT '授信日期';
alter table c_user_ref_con MODIFY ooa_date varchar(2) DEFAULT NULL COMMENT '出账日期';
alter table c_user_ref_con MODIFY repay_date varchar(2) DEFAULT NULL COMMENT '还款日期';

/* 在表m_red_packet 修改时间类型为date 刘旭 2018/7/10*/
alter table m_red_packet MODIFY end_date date DEFAULT NULL comment '有效期';

/* f_fundend修改密码长度  YWT_tai 2018/7/10 */
alter table f_fundend MODIFY pwd varchar(255) DEFAULT NULL comment '资金端登录密码';

/* 添加授信提额表  pj 2018/7/11 */
DROP TABLE IF EXISTS tb_up_money;
create table tb_up_money 
(
   id                       varchar(20) not null,
   parent_id                varchar(20) not null comment '当前登陆的id',
   grade 		    char(1)    not null comment '级别',
   name			    varchar(20) not null comment '一级名称/二级名称',
   code			    varchar(20) not null comment '英文标识',
   uid			    varchar(20) not null comment '用户id',
   primary key (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信提额表';

/* 修改交易记录表  lx 2018/7/11 */
alter table tb_trading_detail MODIFY is_settlement char(1) not null comment '授信消费是否已结算 0:未结算，1：已结算';

/* f_fundend 添加字段  lzy 2018/7/11 */
alter table f_fundend add address_info varchar(255) DEFAULT NULL COMMENT '资金端所在地址详情';

/* tb_user_account 修改支付密码默认为空  lzy 2018/7/11 */
alter table tb_user_account MODIFY tran_pwd varchar(50) DEFAULT NULL COMMENT '支付密码';

/* tb_up_money 添加字段  pj 2018/7/11 */
alter table tb_up_money add value varchar(20) DEFAULT NULL COMMENT '二级信息';

/* tb_up_money 添加字段  pj 2018/7/11 */
alter table tb_up_money MODIFY parent_id varchar(20) DEFAULT null comment '父id';

/* tb_up_money 添加字段  pj 2018/7/11 */
alter table tb_up_money add status char(1) DEFAULT NULL COMMENT '是否完善 0:未完善 1:已完善';

/* tb_wallet_income 添加字段  lzs 2018/7/11 */
alter table tb_wallet_income add settlement_id varchar(20) DEFAULT NULL COMMENT '结算id';

/* tb_wallet_income 添加字段  lzs 2018/7/11 */
alter table tb_wallet_income add oid varchar(20) DEFAULT NULL COMMENT '运营商id';

/* tb_wallet_income 添加字段  lzs 2018/7/11 */
alter table tb_wallet_income add settlement_status char(1) DEFAULT NULL COMMENT '结算状态 0:未结算 1:已结算';

/* tb_wallet 添加注释  lzy 2018/7/12 */
alter table tb_wallet MODIFY `utype` char(1) DEFAULT NULL COMMENT '用户类型1：用户，2：商户，3：运营商，4：资金端 5:平台端';

/* tb_user_account 添加注释  lzy 2018/7/12 */
alter table tb_user_account MODIFY  `user_type_id` char(1) NOT NULL COMMENT '0:用户 1:商户';


/* tb_trading_detail 修改还款主键id类型  ywt 2018/7/13 */
alter table tb_trading_detail MODIFY  rdid varchar(20)  DEFAULT NULL COMMENT ' 所属还款主键id';

/* tb_up_money 添加字段  pj 2018/7/13 */
alter table tb_up_money add amount  bigint(20) DEFAULT NULL COMMENT '额度';

/* tb_up_money 删除amount  pj 2018/7/13 */
alter table tb_up_money drop column amount;

/* tb_up_money 添加字段  pj 2018/7/13 */
alter table tb_up_money add token  varchar(20) DEFAULT NULL COMMENT 'token';

/* tb_payment 修改字段  ywt 2018/7/16 */
alter table tb_payment MODIFY `oi_id` varchar(30) DEFAULT NULL COMMENT ' 订单编号(交易表serial_no)';

/* tb_repay_log 修改字段  ywt 2018/7/16 */
alter table tb_repay_log MODIFY  `status` char(1) not null default 1 COMMENT '还款-支付状态 默认为1:成功';

/* tb_account_info 添加字段 bj 2018/7/17 */
alter table tb_account_info add grade  varchar(20) DEFAULT NULL COMMENT '（评分）';

/* tb_user_account 修改字段 LZY 2018/7/17 */
alter table tb_user_account MODIFY `password` varchar(300) DEFAULT NULL COMMENT '密码';

/* tb_user_account 修改字段 LZY 2018/7/17 */
alter table tb_user_account MODIFY `tran_pwd` varchar(300) DEFAULT NULL COMMENT '支付密码';

/* m_merchant_info 修改字段 LZY 2018/7/17 */
alter table m_merchant_info MODIFY   `pwd` varchar(300) DEFAULT NULL COMMENT '商户登录密码';

/* c_user_ref_con 添加 BJ 2018/7/17 */
alter table c_user_ref_con add nextauth_date  datetime DEFAULT NULL COMMENT '下次授信日期';  

/* c_user_ref_con 添加 BJ 2018/7/18 */
ALTER TABLE tb_up_money MODIFY token varchar(100) COMMENT 'token';  

/* tb_up_money 添加 BJ 2018/7/20 */
ALTER TABLE tb_up_money ADD COLUMN oid varchar(20) DEFAULT NULL COMMENT '运营商id';

/* c_user_ref_con 添加 BJ 2018/7/20 */
ALTER TABLE c_user_ref_con ADD accid varchar(20) DEFAULT NULL COMMENT '授信额度表id';

/* o_opera_info 修改 LZY 2018/7/23 */
ALTER TABLE o_opera_info MODIFY `osn` varchar(100) DEFAULT NULL COMMENT '运营商编码 即二级域名';


/*  修改所有表中的金额字段 XX 2018/7/18 */
alter table c_user_credit_apply modify column auth_amount decimal(20,6) COMMENT '授信金额';
alter table c_user_ref_con modify column auth_max decimal(20,6) COMMENT '授信额度';
alter table m_red_packet modify column money decimal(20,6) COMMENT '优惠金额';
alter table m_red_packet modify column full_money decimal(20,6) COMMENT '订单金额需要达到该金额才可以使用';
alter table m_red_packet_setting modify column money decimal(20,6) COMMENT '商户设置的原始优惠金额';
alter table m_red_packet_setting modify column actual_money decimal(20,6) COMMENT '实际优惠金额（运营商抽成后的优惠金额）';
alter table m_red_packet_setting modify column op_money decimal(20,6) COMMENT '运营商抽取优惠金额(用于三级分销分润)';
alter table m_red_packet_setting modify column full_money decimal(20,6) COMMENT '订单金额需要达到该金额才可以使用';
alter table tb_account_trading_log modify column amount decimal(20,6) COMMENT '金额';
alter table tb_account_trading_log modify column balance decimal(20,6) COMMENT '账户结余';
alter table tb_payment modify column money decimal(20,6) COMMENT '支付金额';
alter table tb_repay_log modify column amount decimal(20,6) COMMENT '还款-支付金额';
alter table tb_repayment_detail modify column total_corpus decimal(20,6) COMMENT '还款总本金';
alter table tb_repayment_detail modify column total_fee decimal(20,6) COMMENT '还款总利息';
alter table tb_repayment_detail modify column total_late_fee decimal(20,6) COMMENT '滞纳金总额（产生滞纳金后累计到此处）';
alter table tb_repayment_detail modify column repay_corpus decimal(20,6) COMMENT '已还本金（如果分期则累计）';
alter table tb_repayment_detail modify column repay_fee decimal(20,6) COMMENT '已还利息（如果分期则累计）';
alter table tb_repayment_detail modify column repay_late_fee decimal(20,6) COMMENT '已还滞纳金（如果分期则累计）';
alter table tb_repayment_detail modify column service_fee decimal(20,6) COMMENT '分期手续费';
alter table tb_repayment_plan modify column total_corpus decimal(20,6) COMMENT '还款总本金';
alter table tb_repayment_plan modify column total_fee decimal(20,6) COMMENT '还款总利息';
alter table tb_repayment_plan modify column rest_corpus decimal(20,6) COMMENT '剩余还款本金';
alter table tb_repayment_plan modify column rest_fee decimal(20,6) COMMENT '剩余还款利息';
alter table tb_repayment_plan modify column current_corpus decimal(20,6) COMMENT '当期还款本金';
alter table tb_repayment_plan modify column current_fee decimal(20,6) COMMENT '当期还款利息';
alter table tb_repayment_plan modify column late_fee decimal(20,6) COMMENT '当期逾期滞纳金  每日逾期滞纳金 = （当期还款本金+当期还款利息+逾期滞纳金）* 滞纳金费率';
alter table tb_settlement_detail modify column amount decimal(20,6) COMMENT '实际结算总金额';
alter table tb_settlement_detail modify column share_fee decimal(20,6) COMMENT '运营商用于三级分销总费用';
alter table tb_settlement_detail modify column platform_share_fee decimal(20,6) COMMENT '平台收取运营商通道总费用（根据分润表统计）';
alter table tb_settlement_detail modify column discount_fee decimal(20,6) COMMENT '运营商抽取的折扣总费用（根据分润表统计）';
alter table tb_settlement_detail modify column opera_share_fee decimal(20,6) COMMENT '运营商收取商户通道费用';
alter table tb_settlement_log modify column amount decimal(20,6) COMMENT '结算记录-结算金额';
alter table tb_three_sale_profit_share modify column redirect_share_amount decimal(20,6) COMMENT '直接上级利润分配金额';
alter table tb_three_sale_profit_share modify column inderect_share_amount decimal(20,6) COMMENT '间接上级利润分配金额';
alter table tb_three_sale_profit_share modify column opa_share_amount decimal(20,6) COMMENT '运营商抽取三级分销利润金额';
alter table tb_trade_profit_share modify column total_trade_amount decimal(20,6) COMMENT '交易总金额';
alter table tb_trade_profit_share modify column merchant_inhome_amount decimal(20,6) COMMENT '交易商户入账金额';
alter table tb_trade_profit_share modify column operator_share_fee decimal(20,6) COMMENT '运营商交易抽成费';
alter table tb_trade_profit_share modify column operator_discount_fee decimal(20,6) COMMENT '三级分销总金额';
alter table tb_trade_profit_share modify column platfrom_share_fee decimal(20,6) COMMENT '平台抽成运营商交易费用（按交易总金额百分比抽成）';
alter table tb_trade_profit_share modify column opera_share_fee decimal(20,6) COMMENT '运营商收取商户通道费用';
alter table tb_trading_detail modify column trade_amount decimal(20,6) COMMENT '交易原始总金额';
alter table tb_trading_detail modify column discount_amount decimal(20,6) COMMENT '优惠金额（消费者使用优惠券）';
alter table tb_trading_detail modify column actual_amount decimal(20,6) COMMENT '实际交易总金额';
alter table tb_user_sharefee_settlement modify column total_money decimal(20,6) COMMENT '结算分润总金额';
alter table tb_wallet modify column total_amount decimal(20,6) COMMENT '总金额';
alter table tb_wallet modify column sxje decimal(20,6) COMMENT '手续费';
alter table tb_wallet_income modify column trade_total_amount decimal(20,6) COMMENT '交易总金额';
alter table tb_wallet_income modify column amount decimal(20,6) COMMENT '出、入金总额';
alter table tb_wallet_withdrawals modify column tx_amount decimal(20,6) COMMENT '提现金额';
alter table tb_wallet_withdrawals modify column dz_amount decimal(20,6) COMMENT '到账金额';
alter table tb_wallet_withdrawals modify column service_fee decimal(20,6) COMMENT '手续费';
alter table tb_wallet_withdrawals modify column wallet_balance decimal(20,6) COMMENT '钱包余额';

/* o_opera_other_info 修改 JY 2018/7/25 */
ALTER TABLE o_opera_other_info MODIFY `oper_area` varchar(50) DEFAULT NULL COMMENT ' 经营面积（㎡）';

/* c_user_info 修改 YWT 2018/7/27 */
ALTER TABLE c_user_info MODIFY  `head_img` varchar(100) DEFAULT 'images/uhead.png' COMMENT '用户头像资源访问路径';

/* tb_trade_profit_share 修改 LX 2018/7/27 */
ALTER TABLE tb_trade_profit_share MODIFY  `three_sale_share_id` bigint(20) DEFAULT NULL COMMENT '三级分销利润分配id';

/* tb_repayment_detail 修改 LZS 2018/7/27 */
ALTER TABLE tb_repayment_detail MODIFY `repayment_sn` varchar(100) DEFAULT NULL COMMENT '还款详情流水号';

/* tb_account_info 修改 jy 2018/7/27 */
ALTER TABLE tb_account_info add  `rapid_Money` varchar(500) DEFAULT NULL COMMENT '快速授信额度（des256加密）';

ALTER TABLE tb_account_info add  `up_Money` varchar(500) DEFAULT NULL COMMENT '授信提额（des256加密）';

ALTER TABLE tb_account_info add  `total_Money` varchar(500) DEFAULT NULL COMMENT '授信总额度（des256加密）';



/* tb_repay_log 修改 ywt 2018/7/30 */
ALTER TABLE tb_repay_log MODIFY `status` char(1) NOT NULL DEFAULT '0' COMMENT '还款-支付状态 默认为0:待支付  1:成功';

ALTER TABLE tb_repay_log add `planId` varchar(20) DEFAULT NULL COMMENT '分期planId( repaymentPlan表 主键id)';



/* tb_repay 创建 ywt 2018/7/31 */
DROP TABLE IF EXISTS tb_repay;
create table tb_repay 
(
   id                       varchar(20) NOT NULL,
   status		    char(1) NOT NULL DEFAULT '0' COMMENT '还款-支付状态 默认为0:待支付  1:已支付',
   repay_sn 		    varchar(100)    NOT NULL comment '生成还款支付单号',
   payment_sn		    varchar(100)    DEFAULT null comment '已支付返回支付单号',
   totalMoney		    decimal(20,6) DEFAULT NULL COMMENT '还款-支付金额',
   pay_date		    datetime DEFAULT NULL COMMENT '支付时间',
   uid			    varchar(20) DEFAULT NULL COMMENT '用户id',
   primary key (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付主表';

/* c_user_ref_con 修改 yx 2018/7/31 */
ALTER TABLE c_user_ref_con MODIFY  `auth_status` char(1) NOT NULL COMMENT '授信状态（0-未授信 1-已授信 2-已冻结）';

/* c_user_ref_con 修改 yx 2018/7/31 */
ALTER TABLE m_red_packet_setting MODIFY `num` int(10) NOT NULL DEFAULT '1' COMMENT '每次领取的数量';

/* tb_repayment_ratio_dict 修改 lzy 2018/8/1 */
ALTER TABLE tb_repayment_ratio_dict MODIFY  `yq_rate` decimal(6,3) DEFAULT NULL COMMENT '逾期还款费率（百分比）';

/* tb_repayment_ratio_dict 修改 lzy 2018/8/1 */
ALTER TABLE tb_repayment_ratio_dict MODIFY  `service_fee` decimal(6,3) DEFAULT NULL COMMENT '手续费利率（分期才有手续费）';

/* tb_settlement_detail 修改 LZS 2018/8/1 */
ALTER TABLE tb_settlement_detail MODIFY `serial_no` varchar(100) DEFAULT NULL COMMENT '结算单号即流水号';


/* tb_settlement_init_test 添加 LZS 2018/8/1 */
DROP TABLE IF EXISTS tb_settlement_init_test;
CREATE TABLE `tb_settlement_init_test` (
  `id` varchar(100) NOT NULL,
  `startDate` datetime DEFAULT NULL COMMENT '开始时间',
  `endDate` datetime DEFAULT NULL COMMENT '结束时间',
  `settlementLoop` varchar(100) DEFAULT NULL COMMENT '结算周期',
  `isCheck` tinyint(2) DEFAULT NULL COMMENT '开关（0：关闭，1：开启）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* o_opera_info 添加字段 JY 2018/8/2 */
ALTER TABLE o_opera_info add  `settlement_status` char(1) NOT NULL DEFAULT '1' COMMENT '审核结算单0-不开通(运营商和平台不需要审核 直接资金端) 1-开通(运营商可以审核结算单 审核完到平台审核)';


/* tb_business_credit 添加 pj 2018/8/3 */
DROP TABLE IF EXISTS tb_business_credit;
CREATE TABLE tb_business_credit (
	id    varchar(20) NOT NULL COMMENT '主键',
	uid   varchar(20) NOT NULL COMMENT  '用户id' ,         
	oid   varchar(20) NOT NULL COMMENT  '运营商id',             
	status		char(1)	 NOT NULL COMMENT   '状态（0，未通过，1，已通过，2，已报名）',        
	create_date     datetime DEFAULT NULL COMMENT '创建时间',
	modify_date	datetime DEFAULT NULL COMMENT '修改时间',
	auth_max        varchar(100) NOT NULL COMMENT '授信额度',
	total_times  varchar(6) NOT NULL COMMENT '分期数',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商业授信表';


/* `tb_business_credit_checklist` 添加 jy 2018/8/3 */
DROP TABLE IF EXISTS `tb_business_credit_checklist`;
CREATE TABLE `tb_business_credit_checklist` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `bid` varchar(20) NOT NULL COMMENT '资源主键id（商业授信ID）',
  `status` char(1) NOT NULL COMMENT '审核结果 0审核失败 1审核成功',
   approver_name varchar(32) NOT NULL COMMENT '审批人（当前登录运营商姓名）',
   mobile varchar(32) NOT NULL COMMENT '审批人手机号码（当前登录运营商手机号码）',
   end_moeny varchar(100) NOT NULL COMMENT '最后给予授信额度（不能大于授信额度）',
  `create_date` datetime NOT NULL COMMENT '纪录创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '纪录修改时间',	
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商业授信审核表';


/* tb_repay_log 添加字段 YWT 2018/8/3 */
ALTER TABLE tb_repay_log add `repay_id` varchar(20) NOT NULL  COMMENT 'tb_repay表中的主键id';

/* c_user_ref_con 添加字段 PJ 2018/8/4 */
ALTER TABLE c_user_ref_con ADD credit_count varchar(2) NOT NULL DEFAULT '0' COMMENT '授信计数';

/* tb_business_credit 添加字段 PJ 2018/8/4 */
ALTER TABLE tb_business_credit ADD mid varchar(20) DEFAULT NULL COMMENT '绑定商户id';

/* tb_three_sales_ref 添加字段 LX 2018/8/4 */
alter table tb_three_sales_ref add column parent_user_type char(1) NOT NULL COMMENT '上级用户类型（0 (用户,商户用户 对应的c_user_info表），1为商户（对应的 m_merchant_info表）';


/* tb_account_info 修改 YWT 2018/8/4*/
ALTER TABLE tb_account_info MODIFY  `is_freeze` char(1) DEFAULT NULL COMMENT '授信是否冻结 0:未冻结 1:冻结';

/* tb_repay_log 添加字段 YWT 2018/8/6 */
ALTER TABLE tb_repay_log add `oid` varchar(20) NOT NULL  COMMENT '运营商id';


/* tb_repay_log 添加字段 YWT 2018/8/6 */
ALTER TABLE tb_repay_log add `fid` varchar(20) NOT NULL  COMMENT '资金端id';


/* tb_payment 添加字段 HJ 2018/8/7 */
ALTER TABLE tb_payment MODIFY `payment_type` varchar(100) DEFAULT NULL COMMENT '支付产品类型:(1 授信 2 钱包 3 微信 4 快捷)';


/* tb_repay 添加字段 YWT 2018/8/7 */
ALTER TABLE tb_repay add `oid` varchar(20) NOT NULL  COMMENT '运营商id';


/* tb_repay 添加字段 YWT 2018/8/7 */
ALTER TABLE tb_repay add `fid` varchar(20) NOT NULL  COMMENT '资金端id';


/* tb_bind_card_info 添加字段 YX 2018/8/7 */
ALTER TABLE tb_bind_card_info add `contract_type` char(1) DEFAULT NULL COMMENT '支付银行卡状态 1:提现 2:支付 3:提现加支付';


/* `tb_credit_grade` 添加表 pj 2018/8/7 */
DROP TABLE IF EXISTS `tb_credit_grade`;
CREATE TABLE `tb_credit_grade` (
  `id` varchar(20) NOT NULL,
  `details` varchar(100) DEFAULT NULL COMMENT '详情',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `code` varchar(20) NOT NULL COMMENT '英文标识',
  `uid` varchar(20) NOT NULL COMMENT '用户id',
  `value` varchar(20) DEFAULT NULL COMMENT '评分',
  `status` char(1) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
  `oid` varchar(20) DEFAULT NULL COMMENT '运营商id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='快速授信评分项';


/* tb_wallet 添加字段 liaozeyong 2018/8/8 */
ALTER TABLE tb_wallet MODIFY `sxje` DECIMAL (20, 6) NOT NULL DEFAULT 0 COMMENT '手续费';


/* tb_trading_detail 修改 ywt 2018/8/8 */
alter table tb_trading_detail modify column `discount_amount` decimal(20,6) DEFAULT '0.000000' COMMENT '优惠金额（消费者使用优惠券）';


/* tb_credit_grade 修改 PJ 2018/8/8 */
ALTER TABLE tb_credit_grade MODIFY code varchar(100) NOT NULL COMMENT '英文标识';

/* tb_settlement_detail 添加 jy 2018/8/9*/
ALTER TABLE tb_settlement_detail add `is_pay` char(1) DEFAULT NULL COMMENT '是否还款 1:已还款 0:未还款';

/* tb_wallet_income 修改 lx 2018/8/9*/
alter table tb_wallet_income modify column type int(2) DEFAULT NULL COMMENT '收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.充值,6.提现支出，7.消费支出 8 平台抽成费扣取（只针对运营商钱包）';


/* `tb_wallet_cashrate` 修改 yx 2018/8/9*/

DROP TABLE IF EXISTS `tb_wallet_cashrate`;
CREATE TABLE `tb_wallet_cashrate` (
  `id` varchar(20) NOT NULL,
  `utype` char(1) DEFAULT NULL COMMENT '用户类型：1用户 2商户 3运营商 4资金端',
  `present_type` char(1) NOT NULL COMMENT '提现费率类型 1为百分比 2.元',
  `present_content` varchar(20) NOT NULL COMMENT '提现类型费率',
  `ladder_class` char(1) DEFAULT NULL COMMENT '提现阶梯等级',
  `minimum_interval_value` varchar(20) DEFAULT NULL COMMENT '阶梯区间值最低',
  `maximum_interval_value` varchar(20) DEFAULT NULL COMMENT '阶梯区间值最高',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* tb_trade_profit_share 修改 lx 22018/8/11*/
ALTER TABLE tb_trade_profit_share modify column platfrom_share_ratio decimal(20,6) NOT NULL COMMENT '平台抽成运营商交易费用比率（按交易总金额百分比）';


/* tb_repay 修改 ywt 22018/8/11*/
alter table tb_repay add column rdid varchar(20) DEFAULT NULL COMMENT '还款表主键id';


/* tb_repay 修改 ywt 22018/8/11*/
alter table tb_repay add column serial_no varchar(30) DEFAULT NULL COMMENT '当前分期还款流水号（r+还款日期（8位）+2位运营商码+4位随机码+主机编号（4位） 长度 19位）';


/* tb_repay 修改 ywt 2018/8/12*/
alter table tb_repay add column pay_type char(1) DEFAULT NULL COMMENT '还款类型 1-账单还款 2-提前还款';


/* tb_repay 修改 ywt 2018/8/12*/
alter table tb_repay add column `payment_type` char(1) NOT NULL DEFAULT 1 COMMENT '还款-支付类型 网银支付';

/* m_merchant_other_info 修改 jy 2018/8/12*/
alter table m_merchant_other_info modify column `share_fee` decimal(20,0) DEFAULT '1' COMMENT '商户让利费率（百分之）';

/* o_opera_other_info 修改 jy 2018/8/12*/
alter table o_opera_other_info modify column `platfrom_share_ratio` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '平台抽取运营商服务费率';

/* tb_wallet_income 修改 lx 2018/8/12*/
ALTER TABLE tb_wallet_income MODIFY COLUMN pname VARCHAR(25) NOT NULL COMMENT '用户名字';


/* p_admin 添加字段 HX 2018/8/13*/
ALTER TABLE p_admin add column `ui_type` int(1) DEFAULT NULL COMMENT '1-平台 2运营商 3-资金端 4-商户pc';


/* o_opera_info 修改字段 JY 2018/8/16*/
ALTER TABLE o_opera_info  MODIFY `appid` varchar(50) DEFAULT NULL COMMENT '运营商微信公众号appid';


/* o_opera_info 修改字段 JY 2018/8/16*/
ALTER TABLE o_opera_info  MODIFY `appsecret` varchar(50) DEFAULT NULL COMMENT '运营商微信公众号appsecret';


/* tb_bind_card_info 修改字段 JY 2018/8/16*/
ALTER TABLE tb_bind_card_info  MODIFY  `bank_card` varchar(32) DEFAULT NULL COMMENT '银行卡号';

  
/* `base_credit_info` 修改 PJ 2018/8/16*/
DROP TABLE IF EXISTS `base_credit_info` ;
CREATE TABLE `base_credit_info` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `mobile` varchar(50) NOT NULL COMMENT '授信手机号码',
  `username` varchar(50) NOT NULL COMMENT '授信用户名',
  `idcard` varchar(50) NOT NULL COMMENT '授信用户名',
  `accountno` varchar(50) NOT NULL COMMENT '主键id',
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信四要素表';

/* tb_wallet_income 修改字段 yx 2018/8/18*/
ALTER TABLE tb_wallet_income MODIFY `type` int(2) DEFAULT NULL COMMENT '收支类型 1.还款入金(资金端)，2.现金消费入金（商户），3.授信金结算， 4.返佣入金（消费者、商户），5.申请驳回入金,6.提现支出，7.消费支出 8 平台抽成费扣取（只针对运营商钱包）';

/* tb_wallet_income 修改字段 lx 2018/8/18*/
ALTER TABLE tb_wallet_income MODIFY  `settlement_status` char(1)  NOT NULL COMMENT '结算状态 0:未结算 1:已结算';

/* c_credit_user_info 修改字段 pj 2018/8/20*/
ALTER TABLE c_credit_user_info MODIFY jsonStr LONGTEXT DEFAULT NULL COMMENT 'json数据';

/*  添加索引 lzs 2018/8/20*/
alter table tb_repayment_detail add index idx_tb_repayment_detail_mid (mid) ;
alter table tb_repayment_detail add index idx_tb_repayment_detail_oid (oid) ;
alter table tb_repayment_detail add index idx_tb_repayment_detail_uid (uid) ;
alter table tb_repayment_plan add index idx_tb_repayment_plan_uid (uid) ;
alter table tb_repayment_plan add index idx_tb_repayment_plan_rd_id (rd_id) ;
alter table tb_settlement_detail add index idx_tb_settlement_detail_mid (mid) ;
alter table tb_settlement_detail add index idx_tb_settlement_detail_oid (oid) ;
alter table tb_user_sharefee_settlement add index idx_tb_user_sharefee_settlement_uid (uid) ;
alter table tb_user_sharefee_settlement add index idx_tb_user_sharefee_settlement_settlement_id (settlement_id) ;


/*  添加索引 lx 2018/8/21*/
alter table tb_wallet_income add index idx_tb_wallet_income_wid (wid) ;
alter table tb_wallet add index idx_tb_wallet_uid (uid) ;
alter table tb_user_sharefee_details add index idx_tb_user_sharefee_details_trade_id (trade_id) ;
alter table tb_user_sharefee_details add index idx_tb_user_sharefee_details_uid (uid) ;
alter table tb_trading_detail add index idx_tb_trading_detail_mid (mid) ;
alter table tb_trading_detail add index idx_tb_trading_detail_uid (uid) ;
alter table tb_trading_detail add index idx_tb_trading_detail_rp_id (rp_id) ;
alter table tb_trading_detail add index idx_tb_trading_detail_rid (rid);
alter table tb_trading_detail add index idx_tb_trading_detail_sid (sid);
alter table tb_trading_detail add index idx_tb_trading_detail_oid (oid);
alter table tb_trade_profit_share add index idx_tb_trade_profit_share_trade_id (trade_id);
alter table tb_trade_profit_share add index idx_tb_trade_profit_share_mid (mid);
alter table tb_trade_profit_share add index idx_tb_trade_profit_share_oid (oid);
alter table tb_trade_profit_share add index idx_tb_trade_profit_share_three_sale_share_id (three_sale_share_id);

alter table tb_three_sales_ref add index idx_tb_three_sales_ref_user_id (user_id);
alter table tb_three_sales_ref add index idx_tb_three_sales_ref_parent_user_id (parent_user_id);
alter table tb_three_sales_rate add index idx_tb_three_sales_rate_oid (oid);
alter table tb_three_sales_rate add index idx_tb_three_sales_rate_mid (mid);
alter table tb_three_sale_profit_share add index idx_tb_three_sale_profit_share_redirect_user_id (redirect_user_id);
alter table tb_three_sale_profit_share add index idx_tb_three_sale_profit_share_inderect_user_id (inderect_user_id);
alter table tb_three_sale_profit_share add index idx_tb_three_sale_profit_share_oid (oid);
alter table tb_payment add index idx_tb_payment_mi_id (mi_id);

alter table c_user_other_info add index idx_c_user_other_info_uid (uid) ;
alter table f_fundend_bind add index idx_f_fundend_bind_fid (fid);
alter table f_fundend_bind add index idx_f_fundend_bind_oid (oid);
alter table m_hot_search add index idx_m_hot_search_oid (oid);
alter table m_hot_search add index dx_m_hot_search_rid (rid);
alter table m_merchant_other_info add index idx_m_merchant_other_info_mid (mid);
alter table o_opera_other_info add index idx_o_opera_other_info_oid (oid);



/*  添加索引 pj 2018/8/21*/
ALTER TABLE `c_credit_user_info` ADD index idx_c_credit_user_info_uid (uid);
ALTER TABLE `c_user_ref_con` ADD index idx_c_user_ref_con_uid (uid);
ALTER TABLE `c_user_ref_con` ADD index idx_c_user_ref_con_oid (oid);
ALTER TABLE `c_user_ref_con` ADD index idx_c_user_ref_con_mid (mid);
ALTER TABLE `c_user_ref_con` ADD index idx_c_user_ref_con_fid (fid);
ALTER TABLE `c_user_ref_con` ADD index idx_c_user_ref_con_accid (accid);
ALTER TABLE `tb_account_info` ADD index idx_tb_account_info_oid (oid);
ALTER TABLE `tb_business_credit` ADD index idx_tb_business_credit_uid (uid);
ALTER TABLE `tb_business_credit` ADD index idx_tb_business_credit_oid (oid);
ALTER TABLE `tb_credit_grade` ADD index idx_tb_credit_grade_uid (uid);
ALTER TABLE `tb_credit_grade` ADD index idx_tb_credit_grade_oid (oid);
ALTER TABLE `tb_up_money` ADD index idx_tb_up_money_uid (uid);
ALTER TABLE `tb_up_money` ADD index idx_tb_up_money_oid (oid);


/*  添加索引 ywt 2018/8/21*/
ALTER TABLE tb_repay ADD index idx_tb_repay_uid (uid);
ALTER TABLE tb_repay ADD index idx_tb_repay_oid (oid);
ALTER TABLE tb_repay ADD index idx_tb_repay_fid (fid);
ALTER TABLE tb_repay_log ADD index idx_tb_repay_log_fid (fid);
ALTER TABLE tb_repay_log ADD index idx_tb_repay_log_oid (oid);
ALTER TABLE tb_repay_log ADD index idx_tb_repay_log_repay_id (repay_id);
alter table tb_collect add index idx_tb_collect_uid (uid);
alter table tb_collect add index idx_tb_collect_mid (mid);


/*  添加索引 hj 2018/8/21*/
alter table m_red_packet_setting add index idx_m_red_packet_setting_mi_id (mi_id);
alter table m_red_packet_setting add index idx_m_red_packet_setting_oid (oid);
alter table m_red_packet add index idx_m_red_packet_mi_id (mi_id);
alter table m_red_packet add index idx_m_red_packet_oid (oid);
alter table m_red_packet add index idx_m_red_packet_rps_id (rps_id);
alter table m_red_packet add index idx_m_red_packet_user_id (user_id);


/*  添加索引 ywt 2018/8/23*/
ALTER TABLE tb_repay ADD index idx_tb_repay_status (status);


/* tb_repay 添加字段 ywt 2018/8/27*/
ALTER TABLE tb_repay add column `create_date` datetime DEFAULT NULL COMMENT '创建时间';


/* base_credit_info 添加/修改字段 pj 2018/8/27*/
ALTER TABLE base_credit_info MODIFY idcard varchar(50) NOT NULL COMMENT '授信身份证号';

ALTER TABLE base_credit_info MODIFY accountno varchar(50) NOT NULL COMMENT '授信银行卡号';

ALTER TABLE base_credit_info add column `uid` varchar(50) NOT NULL COMMENT '用户id';

ALTER TABLE base_credit_info add column `status` varchar(50) NOT NULL COMMENT '1.快速授信流程走完第一步，2.快速授信流程走完';


/* 修改字段 JY 2018/8/27*/
ALTER TABLE o_opera_other_info  MODIFY  `platfrom_share_ratio` decimal(20,6) NOT NULL DEFAULT '0' COMMENT '平台抽取运营商服务费率';

/* 修改字段 JY 2018/8/27*/
ALTER TABLE o_opera_other_info  MODIFY    `score_percent` decimal(20,6) DEFAULT '0' COMMENT '积分返利比例 %（根据消费金额获取积分）';

/* 修改字段 JY 2018/8/27*/
ALTER TABLE m_merchant_other_info  MODIFY    `share_fee` decimal(20,6) DEFAULT '1' COMMENT '商户让利费率（百分之）';



/* 修改字段 JY 2018年9月5日09:22:32*/
ALTER TABLE o_opera_other_info  MODIFY   `settlement_loop` char(1) NOT NULL COMMENT '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年 7-半月）';

/* 修改字段 JY 2018年9月5日09:22:32*/
ALTER TABLE m_merchant_other_info MODIFY    `settlement_loop` char(1) DEFAULT NULL COMMENT '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年 7-半月）';



/* `f_money_info` 添加表 pj 2018年9月5日11:40:04 */
DROP TABLE IF EXISTS `f_money_info`;
CREATE TABLE `f_money_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `fid` varchar(20) NOT NULL COMMENT '资金方id',
  `money` decimal(20,6) DEFAULT NULL COMMENT '总金额',
  `day_money` decimal(20,6) DEFAULT NULL COMMENT '每天授信金额',
  `status` char(1) DEFAULT NULL COMMENT '状态（0.未开启，1.开启）',
  `create_date` datetime DEFAULT NULL COMMENT'创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资金端授信上限表';


/* `f_money_other_info` 添加表 pj 2018年9月5日11:40:04 */

DROP TABLE IF EXISTS `f_money_other_info`;
CREATE TABLE `f_money_other_info` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `fmid` varchar(20) NOT NULL COMMENT '资金方授信上限表id',
  `fid` varchar(20) NOT NULL COMMENT '资金方id',
  `money` decimal(20,6) DEFAULT NULL COMMENT '总金额',
  `uid` varchar(20) DEFAULT NULL COMMENT '用户id',
  `type` char(1) DEFAULT NULL COMMENT '类型（0.出金，1.入金）',
  `create_date` datetime DEFAULT NULL COMMENT'创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信总金额出入明细表';

/* `f_money_info` 修改表 pj 2018年9月5日11:40:04 */
ALTER TABLE f_money_info add column `residue_money` decimal(20,6) DEFAULT NULL COMMENT '每天剩余授信金额';


/*运营商开关表 2018/9/10 ywt*/
DROP TABLE IF EXISTS `c_opera_switch`;

DROP TABLE IF EXISTS `o_opera_switch`;
CREATE TABLE `o_opera_switch` (
  `id` varchar(20) NOT NULL,
  `oid` varchar(20) NOT NULL COMMENT '运营商id',
  `phone_state` char(1) DEFAULT NULL COMMENT '手机归属地状态是否与运营商一致（0.关，1.开）',
  `ip_state` char(1) DEFAULT NULL COMMENT 'ip归属地状态是否与运营商一致（0.关，1.开）',
  `idcard_state` char(1) DEFAULT NULL COMMENT '省份证归属地状态是否与运营商一致（0.关，1.开）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营商开关表';


/* 修改字段 JY 2018年9月10日15:57:01*/
ALTER TABLE f_money_info MODIFY  `money` decimal(20,6) DEFAULT NULL  DEFAULT '0' COMMENT '总金额';

/* 修改字段 JY 2018年9月10日15:57:08*/
ALTER TABLE f_money_info MODIFY   `day_money` decimal(20,6) DEFAULT NULL  DEFAULT '0' COMMENT '每天授信金额';



/* 添加字段 JY 2018年9月26日16:09:36*/
alter table m_merchant_other_info add column minmoney decimal(20,6) DEFAULT NULL COMMENT '最低提现金额 (元)';
alter table m_merchant_other_info add column latestday varchar(50)  DEFAULT NULL COMMENT  '最迟提现时间 (天)';

/* ==================================风控系统开始=================================================*/

/* 新增通知表(存多条，取最新一条展示) lzs 2018/9/11*/
DROP TABLE IF EXISTS `p_notice`;
CREATE TABLE `p_notice` (
  id varchar(50) NOT NULL,
  plan_id varchar(50) NOT NULL COMMENT '还款计划表id',  
  three_day char(1) DEFAULT NULL COMMENT '前三日类型(0:未通知,1:已通知,2:占线,3:空号,4:停机)',
  one_day char(1) DEFAULT NULL COMMENT '前一日类型(0:未通知,1:已通知,2:占线,3:空号,4:停机)',
  create_time datetime COMMENT '创建时间',
  update_time datetime COMMENT '修改时间',
  plan_time datetime COMMENT '预计还款时间',
  content VARCHAR(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='还款分控催收记录表';



/*新增授信日报表  ywt 2018/9/17*/
DROP TABLE IF EXISTS `credit_daily`;
CREATE TABLE `credit_daily` (
  id varchar(50) NOT NULL,
  oid varchar(50) NOT NULL COMMENT '运营商id',
  fid varchar(50) NOT NULL COMMENT '资金端id',
  sx_number_people varchar(10) NOT NULL COMMENT '授信人数', 
  upmoney_number_people varchar(10) NOT NULL COMMENT '申请提额人数',
  percapita_up_money decimal(20,6) NOT NULL  COMMENT '人均提额金额',
  percapita_sx_money decimal(20,6) NOT NULL  COMMENT '人均授信金额',
  credit_line decimal(20,6) NOT NULL  COMMENT '授信额度',
  passing_rate varchar(5) NOT NULL  COMMENT '通过率', 
  create_time datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='授信日报表';


/* 添加字段 JY 2018年9月18日14:26:45*/
alter table c_credit_user_info add column operator_type char(1) COMMENT '运营商类型 0:移动,1:联通,2:电信';

/*新增还款统计表  ywt 2018/9/19*/
DROP TABLE IF EXISTS `tb_repay_statistical`;
CREATE TABLE `tb_repay_statistical` (
  id varchar(50) NOT NULL,
  oid varchar(50) DEFAULT NULL COMMENT '运营商id',
  fid varchar(50) DEFAULT NULL COMMENT '资金端id',
  should_pay_people varchar(10) NOT NULL COMMENT '应还人数', 
  already_pay_people varchar(10) NOT NULL COMMENT '已还人数',
  pay_rate varchar(5) DEFAULT NULL  COMMENT '按时还款率',
  repay_rate varchar(5) NOT NULL  COMMENT '回款率', 
  should_pay_money decimal(20,6) NOT NULL  COMMENT '应还金额',
  actual_repay_money decimal(20,6) NOT NULL  COMMENT '实际回款金额',
  unusual_number varchar(10) DEFAULT NULL  COMMENT '中期异常人数', 
  create_time datetime COMMENT '还款日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='还款统计表';


/* 修改`credit_daily`字段 yx 2018/9/21*/
alter table `credit_daily` MODIFY column oid varchar(50) DEFAULT NULL COMMENT '运营商id';
alter table `credit_daily` MODIFY column fid varchar(50) DEFAULT NULL COMMENT '资金端id';

/* 修改`c_user_ref_con`字段 ywt 2018/9/21*/
alter table `c_user_ref_con` MODIFY column `auth_status` char(1) NOT NULL COMMENT '授信状态（0-未授信 1-已授信 2-已冻结 3-已拒绝）';



/*授信月份统计 lzs 2018/9/25*/
DROP TABLE IF EXISTS `rm_creditmonth_statistical`;
CREATE TABLE `rm_creditmonth_statistical`(
  id varchar(50) NOT NULL,
  oid varchar(50) DEFAULT NULL COMMENT '运营商id',
  fid varchar(50) DEFAULT NULL COMMENT '资金端id',
  month varchar(10) NOT NULL COMMENT '月份', 
  sx_people varchar(10) NOT NULL COMMENT '授信人数',
  sx_money decimal(20,6) NOT NULL  COMMENT '授信额度(元)',
  avg_sx_money decimal(20,6) NOT NULL  COMMENT '平均授信额度(元)',
  pass_rate varchar(5) DEFAULT NULL  COMMENT '通过率',
  using_money_rate varchar(5) NOT NULL  COMMENT '额度使用率',
  break_contract_rate varchar(5) NOT NULL  COMMENT '违约率',
  unhealthy_rate varchar(5) DEFAULT NULL  COMMENT '不良率', 
  bad_debt_rate varchar(5) DEFAULT NULL  COMMENT '坏账率', 
  create_time datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='授信月份统计表';





/* 修改`settlement_loop`字段 JY 2018年9月26日16:21:40*/
alter table `o_opera_other_info` MODIFY column `settlement_loop` char(1) DEFAULT NULL COMMENT '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年 7-半月 8-额度结算）';
alter table `m_merchant_other_info` MODIFY column `settlement_loop` char(1) DEFAULT NULL COMMENT '结算周期（1-日  2-周 3-月 4-季 5-半年 6-一年 7-半月 8-额度结算）';


/* 添加 tb_credit_grade 字段注释 pj 2018/10/8*/
alter table `tb_credit_grade` add column `version_id` varchar(20) DEFAULT NULL COMMENT '版本id';

/*删除`rm_creditmonth_statistical`表字段 ywt 2018/10/10*/
alter table rm_creditmonth_statistical drop COLUMN break_contract_rate;
alter table rm_creditmonth_statistical drop COLUMN unhealthy_rate;
alter table rm_creditmonth_statistical drop COLUMN bad_debt_rate;

/*授信月份统计 ywt 2018/10/10*/
DROP TABLE IF EXISTS `rm_repayment_situation`;
CREATE TABLE `rm_repayment_situation`(
  id varchar(20) NOT NULL,
  oid varchar(20) DEFAULT NULL COMMENT '运营商id',
  fid varchar(20) DEFAULT NULL COMMENT '资金端id',
  break_contract_rate varchar(5) DEFAULT NULL  COMMENT '违约率',
  unhealthy_rate varchar(5) DEFAULT NULL  COMMENT '不良率', 
  bad_debt_rate varchar(5) DEFAULT NULL  COMMENT '坏账率', 
  repayment_time datetime COMMENT '还款周期',
  create_time datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='还款情况表';



/*pj 2018/10/10*/
alter table c_user_ref_con add column credit_date datetime DEFAULT NULL COMMENT '快速授信时间';

/*pj 2018/10/10*/
alter table c_user_ref_con add column upmoney_date datetime DEFAULT NULL COMMENT '授信提额时间';

/*pj 2018/10/10*/
alter table c_user_ref_con add column up_Money decimal(20,4) DEFAULT NULL COMMENT '提额额度';

/*pj 2018/10/10*/
alter table c_user_ref_con add column residue_Money decimal(20,4) DEFAULT NULL COMMENT '剩余授信额度';

/*pj 2018/10/10*/
alter table c_user_ref_con add column refusing_code varchar(20) DEFAULT NULL COMMENT '拒绝授信code';

/*pj 2018/10/10*/
alter table c_user_ref_con add column twoapplications varchar(3) DEFAULT NULL COMMENT '是否同意二次申请';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_base_credit_initial`;
CREATE TABLE `rm_base_credit_initial` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `name` varchar(50) NOT NULL COMMENT '条件描述',
  `code` varchar(50) NOT NULL COMMENT '条件代码',
  `status` varchar(3) NOT NULL COMMENT '条件状态（0.禁用,1.启用）',
  `value` varchar(50) NOT NULL COMMENT '权重',
  `typeId` varchar(50) NOT NULL COMMENT '类别表id',
  `versionId` varchar(50) DEFAULT NULL COMMENT '版本id',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  `minScore` decimal(20,0) DEFAULT NULL COMMENT '最小分值',
  `maxScore` decimal(20,0) DEFAULT NULL COMMENT '最大分值',
  `number` int(11) DEFAULT NULL COMMENT '条件约束',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='初始授信条件模板表';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_base_credit_upmoney`;
CREATE TABLE `rm_base_credit_upmoney` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `name` varchar(50) NOT NULL COMMENT '条件描述',
  `code` varchar(50) NOT NULL COMMENT '条件代码',
  `status` varchar(3) NOT NULL COMMENT '条件状态（0.禁用,1.启用）',
  `value` varchar(50) NOT NULL COMMENT '权重',
  `typeId` varchar(50) NOT NULL COMMENT '类别表id',
  `versionId` varchar(50) DEFAULT NULL COMMENT '版本id',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提额授信条件模板表';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_refusing_version`;
CREATE TABLE `rm_refusing_version` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `fId` varchar(50) NOT NULL COMMENT '资金端id',
  `type` varchar(50) NOT NULL COMMENT '授信类型 1拒绝授信 2初始授信 3授信提额 4授信提额综合分值',
  `status` varchar(3) NOT NULL COMMENT '版本状态 1 为当前版本 0 为过期版本',
  `version` varchar(50) DEFAULT NULL COMMENT '版本号',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyDdate` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信条件版本表';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_base_credit_type`;
CREATE TABLE `rm_base_credit_type` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `name` varchar(50) NOT NULL COMMENT '条件描述',
  `code` varchar(50) NOT NULL COMMENT '条件代码',
  `status` varchar(3) NOT NULL COMMENT '条件状态（0.初始授信类别,1.授信提额类别,2.初始授信内置类别,3.冗余字段）',
  `value` varchar(50) NOT NULL COMMENT '权重',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信条件类别表';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_credit_score`;
CREATE TABLE `rm_credit_score` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `min_score` decimal(20,0) NOT NULL COMMENT '最小分值',
  `max_score` decimal(20,0) NOT NULL COMMENT '最大分值',
  `amount` decimal(20,0) NOT NULL COMMENT '额度',
  `version_id` varchar(50) DEFAULT NULL COMMENT '版本id',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `seq` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信分值金额对应表';

/*pj 2018/10/10*/
DROP TABLE IF EXISTS `rm_refusing_credit`;
CREATE TABLE `rm_refusing_credit` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `name` varchar(50) NOT NULL COMMENT '条件描述',
  `code` varchar(50) NOT NULL COMMENT '条件代码',
  `status` varchar(3) NOT NULL COMMENT '条件状态（0.禁用,1.启用）',
  `number` int(11) NOT NULL COMMENT '条件约束',
  `condition` varchar(50) NOT NULL COMMENT '条件描述',
  `version_id` varchar(50) DEFAULT NULL COMMENT '版本id',
  `twoapplications` varchar(2) DEFAULT NULL COMMENT '是否同意二次申请',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='拒绝授信条件模板表';

/* 修改 rm_base_credit_type 字段注释 ywt 2018/10/8*/
alter table `rm_base_credit_type` MODIFY column `status` varchar(3) NOT NULL COMMENT '条件状态（0.初始授信类别,1.授信提额类别,2.初始授信内置类别,3.冗余字段(数据分析:类别)）';


/*pj 2018/10/11*/
alter table base_interface_info add column price decimal(20,4) DEFAULT NULL COMMENT '接口费用';


/*base_interface_info 修改字段 pj */
alter table base_interface_info add column price decimal(20,4) DEFAULT NULL COMMENT '接口费用';

/*pj 2018/10/16*/
alter table base_credit_info add column `grade` varchar(20) DEFAULT NULL COMMENT '（评分）';


/*新建 rm_sxtemplate_relationship 表 ywt 2018/10/19*/
DROP TABLE IF EXISTS `rm_sxtemplate_relationship`;
CREATE TABLE `rm_sxtemplate_relationship` (
  `id` varchar(20) NOT NULL COMMENT '主键id',
  `version` varchar(50) DEFAULT NULL COMMENT '版本号',
  `fid` varchar(50) DEFAULT NULL COMMENT '资金端id',
  `sx_refusing_id` varchar(20) NOT NULL COMMENT '拒绝授信版本id',
  `sx_init_id` varchar(20) NOT NULL COMMENT '初始授信版本id',
  `sx_upmoney_id` varchar(20) NOT NULL COMMENT '授信提额版本id',
  `sx_upmoney_focus_id` varchar(50) NOT NULL COMMENT '授信提额综合分值版本id',
  `update_module` varchar(50) DEFAULT NULL COMMENT '更新模块 json字符串',
  `status` char(1) DEFAULT NULL COMMENT '是否当前版本 1:是 0:否',
  `is_publish` char(1) DEFAULT NULL COMMENT '是否发布 1:是 0:否',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `forbidden_date` datetime DEFAULT NULL COMMENT '禁用时间',
  `publish_date` datetime DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授信模板关系表';

/*tb_credit_grade 添加字段 ywt 2018/10/19*/
alter table tb_credit_grade add column sx_state char(1) DEFAULT NULL COMMENT '初始授信状态 1:已完成 0:未完成';

/*credit_daily 修改字段 ywt 2018/10/19*/
alter table `credit_daily` MODIFY column `passing_rate` varchar(100) NOT NULL COMMENT '通过率';


/*c_user_ref_con添加字段 pj 2018/10/22*/
alter table c_user_ref_con add column version_id varchar(20) DEFAULT NULL COMMENT '主版本号';


/*rm_creditmonth_statistical 修改字段 ywt 2018/10/22*/
alter table rm_creditmonth_statistical MODIFY column `pass_rate` varchar(100) DEFAULT NULL COMMENT '通过率';
alter table rm_creditmonth_statistical MODIFY column `using_money_rate` varchar(100) NOT NULL COMMENT '额度使用率';

/*rm_refusing_credit 修改字段 pj 2018/10/26*/
ALTER TABLE `rm_refusing_credit` CHANGE `condition` `content` varchar(50) NOT NULL COMMENT '条件描述';


/*rm_refusing_version 修改字段 pj 2018/10/30*/
ALTER TABLE rm_refusing_version DROP COLUMN status;